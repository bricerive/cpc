#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file osconsole.h - CPC console under MacOS

#include <MacTypes.h>
#include <Palettes.h>
#include <Displays.h>
#include <Navigation.h>
#include <stdio.h> // for FILE *
#include "cpcguiconsole.h"
#include "macutils.h"
#include "monitor.h"
#include <vector>

class CpcBase;

typedef enum {
    ERR_CON_DEFAULT,
    ERR_CON_PRNTOPEN,
    ERR_CON_BADPREF,
    ERR_CON_USAGE,
    MSG_CON_KEYPRESS,
    MSG_CON_KEYASSIGN,
    MSG_CON_CPCKEYSHOW,
    MSG_CON_CPCKEYFREE,
    MSG_CON_MACKEYSSHOW,
    MSG_CON_MACKEYFREE,
    MSG_CON_ABOUT,
    MSG_CON_CONFIRMOVERLOAD,
    ERR_CON_KEYMAPPEDTWICE,
    MSG_CON_OPEN_PROMPT_DSK,
    MSG_CON_OPEN_TITLE_DSK,
    MSG_CON_OPEN_PROMPT_SNA,
    MSG_CON_OPEN_TITLE_SNA,
    MSG_CON_OPEN_PROMPT_ROM,
    MSG_CON_OPEN_TITLE_ROM,
    MSG_CON_OPEN_PROMPT_ANY,
    MSG_CON_OPEN_TITLE_ANY
} MsgId;


/// CPC console under MacOS
class OsConsole:public CpcGuiConsole, public MacUtils {

public:
    OsConsole(int argc, char **argv);
    virtual ~OsConsole();
    virtual void ConnectCpc();

    // CpcConsole implementation
    virtual void FrameReady();
    virtual void DiskActivity(int which, int state);
    virtual void PutPrinter(BYTE val);
    virtual void MonitorEventLoop();
    virtual void BuffDone();

    // CpcGuiConsole implementation
    virtual void CheckEvents(bool monitFlag);
    virtual void CheckKbd();
    virtual void Update();
    virtual void SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed)const;
    virtual void StartSound();
    virtual void StopSound();
    virtual void WaitForSound(bool val) {}
    virtual void OsVol(int val);
    void SleepUs(UINT32 us)const;
    unsigned long TicksMs()const;
    unsigned long GetCrtUs()const;

    static bool FilterFile(const std::string &path, TypeFilter filter);

    // OcConsole implementation
    virtual std::string GetStr(CpcGuiMsgId errId);
    virtual void PutError(const std::string &str);

    virtual bool GetHexValue(const char *prompt, int &val);
    virtual void DrawAmstradText(CpcDib32 dst, int x, int y, unsigned long color, const char *str);

private:

    std::string GetStr(MsgId errId, int block=129);
    void ErrReport(MsgId errId, ...);

    OsConsole &operator=(const OsConsole&);
    OsConsole(const OsConsole&);

    virtual void CmdChecked(CpcCmd cmd, bool checked);
    virtual void CmdEnabled(CpcCmd cmd, bool enabled);
    virtual void WindowTitle(const std::string &title);
    virtual void LaunchUrl(const std::string &url);
    virtual bool SelectFile(std::string &path, FileType &type, int &option);
    virtual void PlayCpcSound(CpcSound sound);
    virtual void EmptyRunMenus();
    virtual void AddItemToRunMenu(int drive, const std::string &item, bool enabled);
    virtual void SaveSnapshot(const std::string &path, const BYTE * buff);
    virtual bool GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path);
    virtual void SetScreenSmall(bool val);
    virtual void SetScreenFull(bool val);
    virtual void SetScreenSkip(bool val);
    virtual void SetIndicators(bool val);
    virtual void SetMonochrome(bool val);
    virtual void CopyToClipboard();
    virtual void SavePICTFile();
    virtual int ConfirmDlg(CpcGuiMsgId msgId, ...);
    virtual void DoAboutDialog();
    virtual void DoRomDialog();
    virtual void DoKeyboardDialog();
    virtual void DoStartupDialog();
    virtual void SetFileType(const std::string &path, FileType type);
    virtual bool FilterFile(const std::string &path, FileType type);
    virtual void CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size);

    virtual void MonitToggle();
    virtual void MonitOff();

    virtual std::string GetClipboardString();
    virtual bool CanSound()const;
    virtual bool CanStereo()const;

    void HandleUpdate(WindowPtr window, DialogPtr crtDialog=0);

    // Attributes
    FILE *prOut;
    bool screenSmall;
    bool screenFull;
    bool screenSkip;
    bool indicators;
    bool hold;
    bool inBackground;
    int startupFlag;
    unsigned long kbdState[4];
    // Windows
    WindowPtr mainWin;
    // Main window coordinates in global space
    int mainWinW, mainWinH, mainWinR, mainWinC;
    // CPC screen coordinates in mainWin space
    int cpcScrW, cpcScrH, cpcScrR, cpcScrC;
    // Blit clipping in CPC space
    int blitX0,blitX1, blitY0,blitY1;
    // Timers
    unsigned long startTime;
    unsigned long crtTick;
    char lastSna[128];
    // Mac screen access
    Rect dirtyRect;
    // Full screen
    int lastScreenMode;
    // Indicators bar
    Rect barRect;
    GWorldPtr barGWorld;
    // Dynamic RUN menus
    int runMenuItemsCount[2];

    // Event handling
    void DoEvent(EventRecord *event);
    void DoMouseDownEvent(EventRecord *event);
    void DoMenuCommand(int menuResult);
    void DoOSEvent(EventRecord *event);
    void DoHLEvent(EventRecord *event);
    static pascal OSErr OpenApplHdlr(const AppleEvent *event,AppleEvent *reply,long refCon);
    static pascal OSErr OpenDocsHdlrGlue(const AppleEvent *event,AppleEvent *reply,long refCon);
    OSErr OpenDocsHdlr(const AppleEvent *event);
    static pascal OSErr QuitApplHdlr(const AppleEvent *event,AppleEvent *reply,long refCon);
    static int displayChanged;
    static pascal void DisplayChanged(AppleEvent *theEvent);
    void AsciiUpDown(unsigned char charCode, unsigned char keyCode, bool down);

    AEEventHandlerUPP openAppUPP, openDocUPP, quitAppUPP;
    DMNotificationUPP displayChangedUPP;

    // Dialog routines
    static pascal Boolean StdDialogFilter(DialogPtr theDialog, EventRecord *theEvent, short *itemHit);
    void SetUsageStr(DialogPtr dialog, int item);
    static pascal Boolean AboutDlgFilter(DialogPtr dialog, EventRecord *event, short *itemHitP);
    void PrepareAboutText(GWorldPtr &newGWorld,PixMapPtr *txtPixmapP, int &lastLine);
    static void ScrollText(DialogPtr dialog, const BitMap *dlgPixmapP);
    static void OutlineKey(DialogPtr theDialog, short which);
    static pascal Boolean KbdDlgFilter(DialogPtr dialog, EventRecord *event, short *itemHitP);
    int GetSingleMacKey();
    void SetItemRom(DialogPtr dialog, int offsetCheckBox, CpcConfig::Rom &rom, std::string &path);

    void DrawLongText(const std::string &myText, unsigned char fore, int &crtRow);


    static pascal Boolean ConfirmAlertFilter(DialogPtr dialog,EventRecord *event,short *itemHit);
    static pascal Boolean myFilterProcGlue(AEDesc* theItem,void* info, NavCallBackUserData callBackUD, NavFilterModes filterMode);
    Boolean myFilterProc(AEDesc* theItem,void* info, NavFilterModes filterMode);
    static pascal void myEventProcGlue(NavEventCallbackMessage callBackSelector, NavCBRecPtr callBackParms, NavCallBackUserData callBackUD);
    void myEventProc(NavEventCallbackMessage callBackSelector, NavCBRecPtr callBackParms);

    void HandleCustomMouseDown(NavCBRecPtr callBackParms);
    bool wantDriveSelection;
    int driveSelection;
    Handle customDitl;
    TypeFilter _filter;

    void UpdateWindow(CpcBase::RenderMode rMode);

    void CmdToMenu(int cmd, int &menu, int &item);

    // Attribute chages methods
    void SetWindow();
    void SetRenderingInfo(CpcBase::RenderMode rMode, CpcBase::RenderInfo &renderInfo)const;
    void InBackground(bool val);

    // OS tools
    void LoadConfig();
    Boolean SaveConfig();

    // Customized GetFile for floppies
    int crtGetFloppyFileType;
    int crtGetSnaFileType;
    int crtGetRomFileType;
    DialogPtr currentDialog;

    enum {SNA,DSK} fileType;

    // Events Handling
    void UpdateKeyMap();

    // Screen exports
    void FillBlock(int top,int left,int h,int w,int color)const;
    void FillText(int top,int left,int color0,int color1,int color2,const char *text)const;

    /// Draw a rectangle in the current window
    void RectFill(const Rect &rect, unsigned long color)const;
    /// Draw a string of Amstrad font text in the current window
    void DrawAmstradText(int x, int y, unsigned long color, const char *str) const;
    PixMapHandle GetLockCrtPixmap()const;
    GWorldPtr fontGWorld;
    Rect fontRect;
    PixMapHandle fontPixmap;

    Monitor monit;
    // Monitor Window
    WindowPtr monitWin;
    bool monitVisible;
    void MonitUpdate();

    // sound
    static const int WAIT_SIZE=256;
    static const int NBUFFS=6;
    int buffNSamples;
    UINT8 *psgBuff[NBUFFS];
    volatile bool psgBuffFull[NBUFFS];
    int crtPsgBuff;
    int crtMacBuff;
    volatile int requestEODB;
    // Common sound manager
    SndChannelPtr sndChan;
    // This is called asynchronously by the sound manager
    // when it is done playing a buffer
    void SndCallBack();
    static pascal void SndCallBackGlue(SndChannelPtr sndChan, SndCommand *sndCmd);
    SndCallBackUPP sndCallBackUPP;
    void SendBuff();
    void PlaySilence();
    void PlayBuffer(int buffIdx);
    void SendSoundCmd(long cmd, long param);
    ExtSoundHeaderPtr sndHdr;
    void GetHardwareSettings(SndChannelPtr chan, SoundComponentData *hardwareInfo);
    bool stereoCapable;
    SoundComponentData hardwareInfo;
};
