// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// OsConsole.cc - CPC console under MacOS

#pragma GCC diagnostic ignored "-Wdeprecated"

#include <string.h>
#include <ctype.h>

#include <fstream>

#include "cpcerror.h"
#include "osconsole.h"
#include "cpcConfig.h"
#include "Z80.h"
#include "Cpc.h"
#include "Pio.h"
#include "centro.h"
#include "ga.h"
#include "version.h"
#include "amstrad_text.h"
#include "console_shared.h"

#include "unzip.h"

#include "cocoa_write_image.h"

using namespace std;

static OsConsole *gCon; // for dialog filters


#pragma mark • Resources •


// GUI resources
static const int kMainWin=128;
static const int kMenuBar=128;
static const int kMenuBarX=129;
static const int kMonitWin=129;

// Indicators bar
static const int kBarPICT=131;
static const int kBarH=16;

// Pictures
static const int kAboutPICT=133;

static const int kAmstradFontPict=129;

// Sounds
static const int kSndInsert=8192;
static const int kSndEject=8193;
enum {kDialogOk=1, kDialogCancel};

// Menus
enum {
    kAppleMenu=128,kFileMenu,kConfigMenu,kDisplayMenu,kOptionsMenu,kControlMenu=134,
    kDriveAMenu=140,kDriveBMenu,kSoundMenu,kMonitorMenu,kModelMenu,kKeyboardMenu,kPrinterMenu,kDrivesdMenu,
    kFrameRateMenu=150,kVolumeMenu,kSpeedLimitMenu,kConstructorMenu,kCrtcMenu,
    kRunMenu=157,kRunMenuB};
// Apple menu
enum {kAbout=1,kWebURL=kAbout+2,kMailURL,kRegisterURL=kMailURL+2,kRegister};
// File menu
enum {kOpen=1,kSaveSna=kOpen+4,kQuickLoad,kQuickSave,kPageSetup=kQuickSave+2,kPrintScr,kQuit=kPrintScr+2};
// Control Menu
enum {kPause=1,kReset,kGhostType};
// Config Menu
enum {kRomSelect=8};
// Disk Menu
enum {kSelectDsk=1,kSaveDsk,kSaveAsDsk,kFlip=kSaveAsDsk+2,kEjectDsk};
// Sound Menu
enum {kEnabled=1,kStereo,kChannelA=5,kChannelB,kChannelC};
// Monitor Menu
enum {kMonochrome=1};
// Model Menu
enum {k6128_512=1,k6128,k664,k464,kCPM=8};
// Keyboard Menu
enum {kAscii=1,kRaw,kRawDefine=4};
// Printer Menu
enum {kPrtNone=1,kPrtFile,kDigiblaster};
// Display Menu
enum {kRefresh=1,kScrSmall=3,kScrFull,kSkipOdd,kIndicators,kSaveScr=10,kCopyScr};
// Options Menu
enum {kStartup=1,kForget,kNoLimit=4,kRealTimeFdc=7,kMonitor=8};

// Dialogs
enum {kStartupDialog=129,kRomsDialog,kAboutDialog,kKeyboardDialog,kRomItems,kConfirm=136};

// Splash
enum {kSplashRegister=kDialogOk,kSplashNotYet,kSplashLicense,kSplashUsage,kSplashText=6};
// Startup
enum {kStartupOk=kDialogOk,kStartupCancel,kStartupDiskA,kStartupDiskB,kStartupSnapshot,kStartupCommand,
    kStartupDiskASelect,kStartupDiskBSelect,kStartupSnapshotSelect,kStartupDiskAClear,
    kStartupDiskBClear,kStartupSnapshotClear,kStartupCommClear};
// ROMs
enum {kRomOk=kDialogOk,kRomCancel,kRomActive0,kRomSlot0,kRomSelect0,kRomDesc0,kRomClear0};
// About
enum {kAboutOk=kDialogOk,kAboutLicense,kAboutUsage,kAboutText=5};
// Keyboard
enum {kKeyboardOk=kDialogOk,kKeyboardCancel,kKeyboardDefault,kKeyboardMsg,kKeyboardMapping,kFirstCpcKey};
// Confirm/Remember
enum {kConfirmOk=kDialogOk,kConfirmCancel,kConfirmText,kConfirmRemember};

#pragma mark • Constants •


// Map Mac keyboard to CPC keys (default)
// Gives a CPC key for each Mac key (or FF)
static BYTE keyConv0[128]={
    0x45,0x3C,0x3D,0x35,0x2C,0x34,0x47,0x3F, // 00
    0x3E,0x37,0xFF,0x36,0x43,0x3B,0x3A,0x32, // 08
    0x2B,0x33,0x40,0x41,0x39,0x38,0x30,0x31, // 10
    0x18,0x21,0x29,0x19,0x28,0x20,0x11,0x22, // 18
    0x2A,0x1A,0x23,0x1B,0x12,0x24,0x2D,0x1C, // 20
    0x25,0x1D,0x16,0x27,0x1E,0x2E,0x26,0x1F, // 28
    0x44,0x2F,0x13,0x4F,0xFF,0x42,0xFF,0x4C, // 30
    0x15,0x46,0x09,0x17,0xFF,0xFF,0xFF,0xFF, // 38
    0xFF,0x07,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 40
    0xFF,0xFF,0xFF,0xFF,0x06,0xFF,0xFF,0xFF, // 48
    0xFF,0xFF,0x0F,0x0D,0x0E,0x05,0x14,0x0C, // 50
    0x04,0x0A,0xFF,0x0B,0x03,0xFF,0xFF,0xFF, // 58
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 60
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 68
    0xFF,0xFF,0x10,0x48,0xFF,0x4A,0xFF,0x49, // 70
    0xFF,0x4B,0xFF,0x08,0x01,0x02,0x00,0xFF  // 78
};
// Names for all the Mac virtual keys
static const char *macKeyStr[128]={
    "A","S","D","F","H","G","Z","X", // 00
    "C","V","NoName0A","B","Q","W","E","R", // 08
    "Y","T","1","2","3","4","6","5", // 10
    "=","9","7","-","8","0","]","O", // 18
    "U","[","I","P","Enter","L","J","'", // 20
    "K",";","\\",",","/","N","M",".", // 28
    "Tab","Space","`","Del","VTab","Esc","Power","Command", // 30
    "Shift","Caps","Option","Control","Shift2","Option2","Control2","NoName3F", // 38
    "NoName40","Kpd.","NoName42","Kpd*","NoName44","Kpd+","NoName46","KpdClear", // 40
    "NoName48","NoName49","NoName4A","Kpd/","KpdEnter","NoName4D","Kpd-","NoName4F", // 48
    "NoName50","Kpd=","Kpd0","Kpd1","Kpd2","Kpd3","Kpd4","Kpd5", // 50
    "Kpd6","Kpd7","NoName5A","Kpd8","Kpd9","NoName5D","NoName5E","NoName5F", // 58
    "F5","F6","F7","F3","F8","F9","NoName66","F11", // 60
    "NoName68","F13","NoName6A","F14","NoName6C","F10","NoName6E","F12", // 68
    "NoName70","F15","Help","Home","PgUp","Del","F4","End", // 70
    "F2","PgDn","F1","Left","Right","Down","Up","NoName7F" // 78
};
// Names for all the CPC keys
//    | 0      1      2      3      4      5      6      7
//-----------------------------------------------------------
//  0 | UP     RIGHT  DOWN   {9     {6     {3     {ENTER {.
//  8 | LEFT   COPY   {7     {8     {5     {1     {2     {0
// 16 | CLR    [      ENTER  ]      {4     SHIFT  \      CTRL
// 24 | |      -      @      P      ;      :      /      .
// 32 | 0      9      O      I      L      K      M      ,
// 40 | 8      7      U      Y      H      J      N      SPACE
// 48 | 6      5      R      T      G      F      B      V
//    |[UP    [DOWN  [LEFT  [RIGHT [FIRE2 [FIRE1 [SPARE
// 56 | 4      3      E      W      S      D      C      X
// 64 | 1      2      ESC    Q      TAB    A      CAPS   Z
// 72 | (UP    (DOWN  (LEFT  (RIGHT (FIRE2 (FIRE1 (SPARE DEL
static const char *cpcKeyStr[]={
    "Up","Right","Down","Kpd 9","Kpd 6","Kpd 3","Kpd Enter","Kpd .",
    "Left","Copy","Kpd 7","Kpd 8","Kpd 5","Kpd 1","Kpd 2","Kpd 0",
    "Clr","[","Enter","]","Kpd 4","Shift","\\","Crtl",
    "|","-","@","P",";",":","/",".",
    "0","9","O","I","L","K","M",",",
    "8","7","U","Y","H","J","N","Space",
    "6","5","R","T","G","F","B","V",
    "4","3","E","W","S","D","C","X",
    "1","2","Esc","Q","Tab","A","Caps","Z",
    "Joy Up","Joy Down","Joy Left","Joy Right","Joy Fire2","Joy Fire1","Joy Spare","Del"
};

// In CPU hog mode switch to FullEventCheck for that many ticks when we think something
// new can happen. Thiss increases responsivness
static const int SNEAK_COUNT=16;

// Resource ID of preview PICTS in snapshots
static const int kPreviewPictId=256;

// Indicators colors
enum {BLUE=0, YELLOW, RED, DARK_BLUE, BLACK, DARK_GREY, LIGHT_GREY};
static RGBColor colors[] = {
    {0x0000,0x0000,0xFFFF}, // Blue
    {0xFFFF,0xFFFF,0x0000}, // Yellow
    {0xFFFF,0x0000,0x0000}, // Red
    {0x0000,0x0000,0x8000}, // Dark Blue
    {0x0000,0x0000,0x0000}, // Black
    {0x3300,0x3300,0x3300}, // Dark Grey
    {0x7700,0x7700,0x7700}, // Light Grey
};

/// Map menu commands to CpcGuiConsole commands
static struct {
    CpcCmd cmd;
    int menu;
    int item;
} cmdMap[] = {

    {kCmdAbout,kAppleMenu,kAbout},
    {kCmdWebUrl,kAppleMenu,kWebURL},
    {kCmdMailUrl,kAppleMenu,kMailURL},

    {kCmdSaveSnapshot,kFileMenu,kSaveSna},
    {kCmdQuickLoad,kFileMenu,kQuickLoad},
    {kCmdQuickWrite,kFileMenu,kQuickSave},
    {kCmdQuit,kFileMenu,kQuit},
    {kCmdOpenAny,kFileMenu,kOpen},

    {kCmdPause,kControlMenu,kPause},
    {kCmdReset,kControlMenu,kReset},
    {kCmdGhostType,kControlMenu,kGhostType},
    {kCmdRoms,kConfigMenu,kRomSelect},

    {kCmdLoadA,kDriveAMenu,kSelectDsk},
    {kCmdSaveA,kDriveAMenu,kSaveDsk},
    {kCmdSaveAsA,kDriveAMenu,kSaveAsDsk},
    {kCmdFlipA,kDriveAMenu,kFlip},
    {kCmdEjectA,kDriveAMenu,kEjectDsk},

    {kCmdLoadB,kDriveBMenu,kSelectDsk},
    {kCmdSaveB,kDriveBMenu,kSaveDsk},
    {kCmdSaveAsB,kDriveBMenu,kSaveAsDsk},
    {kCmdFlipB,kDriveBMenu,kFlip},
    {kCmdEjectB,kDriveBMenu,kEjectDsk},

    {kCmdSoundOn,kSoundMenu,kEnabled},
    {kCmdStereoOn,kSoundMenu,kStereo},
    {kCmdChannelA,kSoundMenu,kChannelA},
    {kCmdChannelB,kSoundMenu,kChannelB},
    {kCmdChannelC,kSoundMenu,kChannelC},
    {kCmdVolume0,kVolumeMenu,1},
    {kCmdVolume1,kVolumeMenu,2},
    {kCmdVolume2,kVolumeMenu,3},
    {kCmdVolume3,kVolumeMenu,4},
    {kCmdVolume4,kVolumeMenu,5},
    {kCmdVolume5,kVolumeMenu,6},
    {kCmdVolume6,kVolumeMenu,7},
    {kCmdVolume7,kVolumeMenu,8},

    {kCmdMonochrome,kMonitorMenu,kMonochrome},

    {kCmd6128_512,kModelMenu,k6128_512},
    {kCmd6128,kModelMenu,k6128},
    {kCmd664,kModelMenu,k664},
    {kCmd464,kModelMenu,k464},
    {kCmdCpmBoot,kModelMenu,kCPM},
    {kCmdIsp,kConstructorMenu,1},
    {kCmdTriumph,kConstructorMenu,2},
    {kCmdSaisho,kConstructorMenu,3},
    {kCmdSolavox,kConstructorMenu,4},
    {kCmdAwa,kConstructorMenu,5},
    {kCmdSchneider,kConstructorMenu,6},
    {kCmdOrion,kConstructorMenu,7},
    {kCmdAmstrad,kConstructorMenu,8},

    {kCmdAscii,kKeyboardMenu,kAscii},
    {kCmdRaw,kKeyboardMenu,kRaw},
    {kCmdDefineRaw,kKeyboardMenu,kRawDefine},

    {kCmdPrinterNone,kPrinterMenu,kPrtNone},
    {kCmdPrinterFile,kPrinterMenu,kPrtFile},
    {kCmdDigiblaster,kPrinterMenu,kDigiblaster},

    {kCmdRefresh,kDisplayMenu,kRefresh},
    {kCmdHalfSize,kDisplayMenu,kScrSmall},
    {kCmdFullScreen,kDisplayMenu,kScrFull},
    {kCmdSkip,kDisplayMenu,kSkipOdd},
    {kCmdIndicators,kDisplayMenu,kIndicators},
    {kCmdSaveScreen,kDisplayMenu,kSaveScr},
    {kCmdCopyScreen,kDisplayMenu,kCopyScr},
    {kCmdFRate1,kFrameRateMenu,1},
    {kCmdFRate2,kFrameRateMenu,2},
    {kCmdFRate4,kFrameRateMenu,3},
    {kCmdFRate8,kFrameRateMenu,4},
    {kCmdStartup,kOptionsMenu,kStartup},
    {kCmdForget,kOptionsMenu,kForget},
    {kCmdSpeedNone,kOptionsMenu,kNoLimit},
    {kCmdDebugger,kOptionsMenu,kMonitor},
    {kCmdRealTimeFdc,kOptionsMenu,kRealTimeFdc},
    {kCmdSpeed25,kSpeedLimitMenu,1},
    {kCmdSpeed50,kSpeedLimitMenu,2},
    {kCmdSpeed75,kSpeedLimitMenu,3},
    {kCmdSpeed100,kSpeedLimitMenu,4},
    {kCmdSpeed125,kSpeedLimitMenu,5},
    {kCmdSpeed150,kSpeedLimitMenu,6},
    {kCmdSpeed175,kSpeedLimitMenu,7},
    {kCmdSpeed200,kSpeedLimitMenu,8},
    {kCmdEndOfList,0,0}
};

#pragma mark • Events handling •

void OsConsole::HandleUpdate(WindowPtr window, DialogPtr crtDialog)
{
    BeginUpdate(window);
    if (window == mainWin)
        Update();
    else if (window==monitWin)
        MonitUpdate();
    else if (window==GetDialogWindow(crtDialog)) {
        SetPortDialogPort(crtDialog);
        DrawDialog(crtDialog);
    }
    EndUpdate(window);
}

// Do some MacOS specific mapping to handle some special keys
void OsConsole::AsciiUpDown(unsigned char charCode, unsigned char keyCode, bool down)
{
    unsigned char asciiCode = charCode;
    if (keyCode==0x33 && charCode==0x08) asciiCode = 127; // DEL
    if (keyCode==0x7B && charCode==0x1C) asciiCode = 131; // Left
    if (keyCode==0x7C && charCode==0x1D) asciiCode = 130; // Right
    if (keyCode==0x7E && charCode==0x1E) asciiCode = 128; // Up
    if (keyCode==0x7D && charCode==0x1F) asciiCode = 129; // Down
    if (keyCode==0x4C && charCode==0x03) asciiCode = 135; // Enter

    if (down)
        AsciiDown(asciiCode);
    else
        AsciiUp(asciiCode);
}

void OsConsole::DoEvent(EventRecord *event)
{

    unsigned char charCode, keyCode;
    int menuResult;

    switch(event->what) {
        case mouseDown:
            DoMouseDownEvent(event);
            break;
        case keyDown:
            charCode = event->message & charCodeMask;
            keyCode = (event->message & keyCodeMask)>>8;
            if ((event->modifiers & cmdKey)!=0) {
                SetRunMenus();
                menuResult=MenuKey(charCode);
                if (!menuResult)
                    HiliteMenu(0);
                else {
                    HiliteMenu(HiWord(menuResult));
                    DoMenuCommand(menuResult);
                }
            }
            if (FrontWindow()==monitWin) {
                if (monit.KeyDown(charCode))
                    SetWindow();

            } else
                if (!gConfig.kbdRawMode)
                    AsciiUpDown(charCode, keyCode, true);
            break;
        case keyUp:
            charCode = event->message & charCodeMask;
            keyCode = (event->message & keyCodeMask)>>8;
            if (!gConfig.kbdRawMode)
                AsciiUpDown(charCode, keyCode, false);
            break;
        case updateEvt:
            HandleUpdate((WindowPtr)event->message);
            break;
        case osEvt:
            DoOSEvent(event);
            break;
        case kHighLevelEvent:
            DoHLEvent(event);
            break;
        default:
            break;
    }
}

void OsConsole::DoMouseDownEvent(EventRecord *event)
{
    WindowPtr window;
    Rect clip;
    int moved=0;

    if (screenFull) ReleaseFullScreen();

    int part = FindWindow(event->where,&window);

    if (window==monitWin && part!=inSysWindow && part!=inMenuBar) {

        bool run=false;
        if (FrontWindow() != monitWin) {
            monit.Refresh();
            SelectWindow(monitWin);
        }
        switch (part) {
            case inGoAway:
                MonitToggle();
                break;
            case inDrag:
                BitMap screenBits;
                GetQDGlobalsScreenBits(&screenBits);
                DragWindow(monitWin, event->where, &screenBits.bounds);
                break;
            case inContent:
                // Convert to local coordinates
                GrafPtr savePort;
                GetPort(&savePort);
                SetPort(GetWindowPort(monitWin));
                GlobalToLocal(&event->where);
                run = monit.MouseDown(event->where.h, event->where.v);
                SetPort(savePort);
                break;
        }
        MonitUpdate();
        if (run)
            SetWindow();
        return;
    }

    switch (part) {
        case inGoAway:
            DoQuit();
            break;
        case inZoomIn:
            SetScreenSmall(true);
            break;
        case inZoomOut:
            SetScreenSmall(false);
            SetWindow();
            break;
        case inSysWindow:
            break;
        case inMenuBar:
            SetRunMenus();
            DoMenuCommand(MenuSelect(event->where));
            break;
        case inContent:
        case inDrag:
            if (!screenFull) {
                BitMap screenBits;
                GetQDGlobalsScreenBits(&screenBits);
                clip.top = screenBits.bounds.top+20+event->where.v-mainWinR;
                clip.left = screenBits.bounds.left+event->where.h-mainWinC;
                clip.bottom = screenBits.bounds.bottom-mainWinH+event->where.v-mainWinR+1;
                clip.right = screenBits.bounds.right-mainWinW+event->where.h-mainWinC+1;
                //DragWindow(window, event->where, &clip);
                DragWindow(window, event->where, 0);
                SetPortWindowPort(mainWin);
                Point winPos = {0, 0};
                SetPt(&winPos,0,0);
                LocalToGlobal(&winPos);
                //winPos.h = (winPos.h+4)&0xFFF8;
                if (screenSmall) {
                    if (guiPrefs.sWinX!=winPos.h || guiPrefs.sWinY!=winPos.v) moved=1;
                    guiPrefs.sWinX = winPos.h;
                    guiPrefs.sWinY = winPos.v;
                } else {
                    if (guiPrefs.bWinX!=winPos.h || guiPrefs.bWinY!=winPos.v) moved=1;
                    guiPrefs.bWinX = winPos.h;
                    guiPrefs.bWinY = winPos.v;
                }
                if (moved) SetWindow();

                // Give the click position to the monitor for CRT debugging
                int x=event->where.h,y=event->where.v;
                x = (x-mainWinC-cpcScrC);
                y = (y-mainWinR-cpcScrR);
                if (screenSmall) {
                    x<<=1;
                    y<<=1;
                }
                WindowClick(x,y);
            }
            break;
    }
    //while (GetCurrentEventButtonState()) {}
    if (screenFull) GetFullScreen();
}

void OsConsole::DoOSEvent(EventRecord *event)
{
    switch((event->message >> 24) & 0xFF) {
        case suspendResumeMessage:
            if (event->message & resumeFlag) {
                InBackground(false);
                Resume();
                hold=false;
            } else {
                // Do that in Carbon so that the window buffer is up-to-date
                // and the menus' alpha is correct
                // Flush the window buffer
                RgnHandle visibleRgn=NewRgn();
                GetPortVisibleRegion(GetWindowPort(mainWin),visibleRgn);
                QDFlushPortBuffer(GetWindowPort(mainWin),visibleRgn);
                DisposeRgn(visibleRgn);
                //InBackground(true);
                //Suspend();
                //hold=true;
            }
            break;
    }
}

void OsConsole::DoHLEvent(EventRecord *event)
{
    AEProcessAppleEvent(event);
}

// Handle OpenAppl Apple Event
pascal OSErr OsConsole::OpenApplHdlr(const AppleEvent * /*event*/, AppleEvent * /*reply*/, long /*refCon*/)
{
    return noErr;
}

OSErr OsConsole::OpenDocsHdlr(const AppleEvent *event)
{
    AEDescList docList;
    long itemsInList;
    FSRef myFsRef;
    Size actualSize;
    AEKeyword keywd;
    DescType returnedType;
    int item;

    try {
        // get the direct parameter--a descriptor list--and put it into docList
        ASSERT_THROW(AEGetParamDesc(event, keyDirectObject, typeAEList, &docList)==noErr)("Cannot get ODOC event parameters.");

        // count the number of descriptor records in the list
        ASSERT_THROW(AECountItems(&docList, &itemsInList)==noErr)("Cannot get ODOC params count.");

        for (item=0; item<itemsInList; item++) {
            // now get each descriptor record from the list,
            // coerce the returned data to an FSSpec record, and open the associated file
            ASSERT_THROW(AEGetNthPtr(&docList, item+1, typeFSRef, &keywd, &returnedType, &myFsRef, sizeof(myFsRef),&actualSize)==noErr)("cannot get ODOC file descriptor.");
            std::string fName;
            GetFullPath(myFsRef, fName);
            if (FilterFile(fName, FileDisk))
                LoadDisk(fName,item&1);
            if (FilterFile(fName, FileSnapshot))
                LoadSnapshot(fName);
        }
    } catch (Err &err) {
        PutError(err.What().c_str());
    }
    return noErr;
}

// Handle OpenDoc Apple Event
pascal OSErr OsConsole::OpenDocsHdlrGlue(const AppleEvent *event, AppleEvent */*reply*/, long refCon)
{
    OsConsole *console = (OsConsole *)refCon;
    return console->OpenDocsHdlr(event);
}

// Handle QuitAppl Apple Event
pascal OSErr OsConsole::QuitApplHdlr(const AppleEvent */*event*/, AppleEvent */*reply*/, long refCon)
{
    OsConsole *console = (OsConsole *)refCon;
    console->DoQuit(false); // in OSX, Quit is always an appleEvent
    return noErr;
}

void OsConsole::DoMenuCommand(int menuResult)
{
    int menuID,menuItem;

    menuID=HiWord(menuResult);
    menuItem=LoWord(menuResult);
    if (menuID==kRunMenu || menuID==kRunMenuB) {
        RunProg(menuID==kRunMenu?0:1,menuItem-1);
    } else
        for (int i=0; cmdMap[i].cmd!=kCmdEndOfList; i++) {
            if (cmdMap[i].menu==menuID && cmdMap[i].item==menuItem) {
                DoCommand(cmdMap[i].cmd);
                break;
            }
        }
    HiliteMenu(0);
}

void OsConsole::CmdToMenu(int cmd, int &menu, int &item)
{
    for (int i=0; cmdMap[i].cmd!=kCmdEndOfList; i++) {
        if (cmdMap[i].cmd == cmd) {
            menu = cmdMap[i].menu;
            item=cmdMap[i].item;
            break;
        }
    }
}

#pragma mark • CPC Functions •
static bool IsZipFile(const std::string &path);

// Uncompress the indexth item that passes the filter
// If there is such an item, data is a newly allocated buffer containing the uncompressed item
// otherwise data is zero
void OsConsole::CheckCompressed(const std::string &path, FileType type, int index, BYTE *&data, int &size)
{
    int found=0;
    data=0;
    size=0;

    if (!::IsZipFile(path)) return;

    unzFile zFile = unzOpen(path.c_str());
    ASSERT_THROW(zFile)("Unknown error in UnZip.");
    unz_global_info info;
    unzGetGlobalInfo(zFile, &info);
    for (unsigned int zIdx=0; zIdx<info.number_entry; zIdx++) {
        unz_file_info fInfo;
        char zfpath[128];
        char zeField[128];
        char zComm[128];
        unzGetCurrentFileInfo(zFile, &fInfo, zfpath,128,zeField,128,zComm,128);

        BYTE *buff = new BYTE[fInfo.uncompressed_size];
        if (buff==0) {
            unzClose(zFile);
            ERR_THROW("Memory full.");
        }
        if (unzOpenCurrentFile(zFile)!=UNZ_OK) {
            unzClose(zFile);
            ERR_THROW("Problem with the ZIP file.");
        }
        if (unzReadCurrentFile(zFile, buff, fInfo.uncompressed_size)!=static_cast<int>(fInfo.uncompressed_size)) {
            unzCloseCurrentFile(zFile);
            unzClose(zFile);
            ERR_THROW("Problem with the ZIP file.");
        }
        unzCloseCurrentFile(zFile);

        if (IsFileType(zfpath, buff, type)) found++;

        if (found == index+1) {
            size = fInfo.uncompressed_size;
            data = buff;
            break;
        }
        delete[] buff;
        unzGoToNextFile(zFile);
    }
    unzClose(zFile);
}

#pragma mark • Dialogs •

pascal Boolean OsConsole::StdDialogFilter(DialogPtr theDialog, EventRecord *theEvent, short *itemHit)
{
    switch (theEvent->what) {
        case keyDown: {
            char theChar = (char)(theEvent->message & charCodeMask);
            if ( ((theEvent->modifiers&cmdKey)&&theChar=='.') || (theChar==27)) {
                *itemHit = kDialogCancel;
                HiliteButton(theDialog,kDialogCancel,1);
                return true;
            }
            if ( theChar==13 || theChar==3 ) {
                *itemHit = kDialogOk;
                HiliteButton(theDialog,kDialogOk,1);
                return true;
            }
        } break;
        case updateEvt:
            gCon->HandleUpdate((WindowPtr)theEvent->message,theDialog);
            break;
    }
    return false;
}

#pragma mark - About

void OsConsole::SetUsageStr(DialogPtr dialog, int item)
{
    char usageStr[128];
    int nbDays,nbHours,nbMins,nbSecs;
    unsigned long usage, newTime;
    DateTimeRec date;

    GetDateTime(&newTime);
    usage = guiPrefs.usageTime + (newTime-startTime);
    SecondsToDate(usage,&date);
    nbDays = usage / (60 * 60 * 24);
    usage -= nbDays * (60 * 60 * 24);
    nbHours = usage / (60 * 60);
    usage -= nbHours * (60 * 60);
    nbMins = usage / 60;
    usage -= nbMins * 60;
    nbSecs = usage;
    std::string format = GetStr(ERR_CON_USAGE);
    sprintf(usageStr, format.c_str(), nbDays, nbHours, nbMins, nbSecs);
    SetItemText(dialog,item,usageStr);
}

// Could not find a way to pass a handle to the filter function,
// so store in it global (would that work in 68K ?)
static struct {
    OsConsole *console;
    const BitMap *dlgPixmapP;
} gAbout;

static struct {
    int line;
    int lastPixRow;
    GWorldPtr txtWorld;
    PixMapPtr txtPixmapP;
    Rect txtRect;
} gScrollText;

pascal Boolean OsConsole::AboutDlgFilter(DialogPtr dialog, EventRecord *event, short *itemHit)
{
    char theChar;

    switch (event->what) {
        case keyDown:
            theChar = (char)(event->message & charCodeMask);
            if ( theChar==13 || theChar==3 || theChar==27) {
                *itemHit = kDialogOk;
                HiliteButton(dialog,kDialogOk,1);
                return true;
            }break;
        case updateEvt:
            gCon->HandleUpdate((WindowPtr)event->message,dialog);
            break;
    }
    ScrollText(dialog, gAbout.dlgPixmapP);

    return false; // Pass event
}

void OsConsole::PrepareAboutText(GWorldPtr &newGWorld, PixMapPtr *txtPixmapP, int &lastLine)
{
    // Draw splash screen text background PICT
    DrawPictIntoNewGWorld(kAboutPICT, newGWorld,0);

    // Prepare for text drawing
    GWorldPtr saveWorld;
    GDHandle saveDevice;
    GetGWorld(&saveWorld, &saveDevice);
    SetGWorld(newGWorld, nil);

    Rect rect;
    SetRect(&rect,8,184,9,185);
    RGBForeColor(&colors[DARK_BLUE]);
    PaintRect(&rect);

    // Draw the Copyrith years
    static const int copyrightRow = 90;
    DrawAmstradText(8, copyrightRow, P32()[BrightYellow], "1996-2013");

    static const int aboutTextRow = 120;
    gScrollText.lastPixRow=aboutTextRow;

    // Draw the version info
    string CL = string(versionString).substr(7,3);
    string versionInfo = string("Version ")+CL+" \n";
    DrawLongText(versionInfo, BrightYellow, gScrollText.lastPixRow);
    gScrollText.lastPixRow+=16;

    // Draw the scrolling text centered
    // Do line feeds for \r or more than 32 chars
    std::string myText = GetStr(MSG_CON_ABOUT);
    DrawLongText(myText, BrightYellow, gScrollText.lastPixRow);

    UnlockPixels(GetGWorldPixMap(newGWorld));

    SetGWorld(saveWorld, saveDevice);

    // Lock the Pixmap for dialogFilterProc scrolling
    PixMapHandle txtPixmapH;
    txtPixmapH = GetGWorldPixMap(newGWorld);
    HLockHi((Handle)txtPixmapH);
    LockPixels(txtPixmapH);
    *txtPixmapP = *txtPixmapH;
}

void OsConsole::ScrollText(DialogPtr dialog, const BitMap *dlgPixmapP)
{
    RGBColor foreColor,backColor;
    static unsigned int lastTick=0;
    Rect srcRect = gScrollText.txtRect;
    bool drawText=false;

    if (gScrollText.line==0 || TickCount() > lastTick+1) {
        gScrollText.line += 1;
        if (gScrollText.line>gScrollText.lastPixRow)
            gScrollText.line=0;
        lastTick = TickCount();
        drawText=true;
    }
    // Draw Scrolling Text
    OffsetRect(&srcRect, -srcRect.left, gScrollText.line-srcRect.top);
    SetPortDialogPort(dialog);
    GetBackColor(&backColor);
    GetForeColor(&foreColor);
    ForeColor(blackColor);  // Prevents colorizing mode
    BackColor(whiteColor);  // Prevents colorizing mode
    (**(gScrollText.txtPixmapP->pmTable)).ctSeed = (**((**((**(GetGDevice())).gdPMap)).pmTable)).ctSeed;
    CopyBits((BitMapPtr)gScrollText.txtPixmapP, (BitMapPtr)dlgPixmapP,
             &srcRect, &gScrollText.txtRect, srcCopy, nil);
    RGBForeColor(&foreColor);
    RGBBackColor(&backColor);

    CGrafPtr thePort = GetDialogPort(dialog);
    // flush part of the port
    if (QDIsPortBuffered(thePort)) {
        RgnHandle theRgn;
        theRgn = NewRgn();
        RectRgn(theRgn, &gScrollText.txtRect);
        QDFlushPortBuffer(thePort, theRgn);
        DisposeRgn(theRgn);
    }
}

void OsConsole::DoAboutDialog()
{
    DialogPtr dialog;
    int done=0;

    if (!(dialog=GetNewDialog(kAboutDialog,0,(WindowPtr) -1)))
        return;

    SetItemText(dialog, kAboutLicense, "");
    SetUsageStr(dialog, kAboutUsage);
    ShowWindow(GetDialogWindow(dialog));
    DrawDialog(dialog);
    SetDialogDefaultItem(dialog,kAboutOk);

    // Prepare test scrolling
    PrepareAboutText(gScrollText.txtWorld,&gScrollText.txtPixmapP, gScrollText.lastPixRow);
    gAbout.dlgPixmapP = GetPortBitMapForCopyBits(GetDialogPort(dialog));

    GetItemRect(dialog, kAboutText, &gScrollText.txtRect);
    gScrollText.txtRect.right-=1;
    gAbout.console=this;
    gScrollText.line=0;

    gCon=this;
    while (!done) {
        EventRecord theEvent;
        WaitNextEvent ( everyEvent - diskMask, &theEvent, 5, nil );
        SInt16      itemHit;
        if (AboutDlgFilter(dialog, &theEvent, &itemHit)==false) {
            if ( IsDialogEvent ( &theEvent ) ) {
                DialogRef   whichDialog;
                if ( DialogSelect ( &theEvent, &whichDialog, &itemHit ) ) {
                    if ( whichDialog == dialog && itemHit == kAboutOk )
                        done=1;
                }
            }
        }
        else
            done=1;
    }

    DisposeGWorld(gScrollText.txtWorld);
    DisposeDialog(dialog);
}

#pragma mark - ROMs
static Str255 tmpStrs[(NB_ROMSLOTS+1)*2];
static std::string paths[NB_ROMSLOTS+1];

void SetRomItem(DialogPtr dialog, int j, int crtItem, int dy, int dx, int active, const Str255 &label, const Str255 &romDesc)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    GetDialogItem(dialog, crtItem, &itemType, &itemHandle, &itemRect);
    itemRect.top += dy;
    itemRect.bottom += dy;
    itemRect.left += dx;
    itemRect.right += dx;
    SetDialogItem(dialog, crtItem, itemType, itemHandle, &itemRect);

    switch (j) {
        case 0:
            SetControlValue((ControlHandle)itemHandle, active);
            break;
        case 1:
            SetDialogItemText(itemHandle, label);
            break;
        case 3:
            SetDialogItemText(itemHandle,romDesc);
            break;
    }
}

void SetRomItem(DialogPtr dialog, int crtItem, int i)
{
    // Setup the dialog items
    const int nbItemPerRom=5;
    const int itemDx = 348;
    const int itemDy = 29;

    int iCol = i>=NB_ROMSLOTS/2;
    int iRow = i%(NB_ROMSLOTS/2);

    char b[32];
    sprintf(b,"%d",i);
    Str255 &label(tmpStrs[i*2]);
    CopyCStringToPascal(b, label);
    paths[i] = gConfig.upperRoms[i].path;
    Str255 &romDesc(tmpStrs[i*2+1]);
    CopyCStringToPascal(gConfig.upperRoms[i].description, romDesc);

    // reposition the items
    for (int j=0; j<nbItemPerRom; j++) {
        SetRomItem(dialog, j, crtItem+j, itemDy*iRow, itemDx*iCol, gConfig.upperRoms[i].active, label, romDesc);
    }
}

void OsConsole::SetItemRom(DialogPtr dialog, int offsetCheckBox, CpcConfig::Rom &rom, std::string &path)
{
    GetItemVal(dialog, offsetCheckBox, &rom.active);
    std::string description;
    GetItemText(dialog, offsetCheckBox+3, description);
    strncpy(rom.description, description.c_str(), 32);
    strncpy(rom.path, path.c_str(), PATH_BYTES-1);
    //try {
    //    SetTypeCreator(rom.path, FileRom);
    //} catch (...) {}

}

void OsConsole::DoRomDialog()
{
    DialogPtr dialog;
    if (!(dialog=GetNewDialog(kRomsDialog,0,(WindowPtr) -1)))
        return;

    const int nbItemPerRom=5;


    int firstNewItem=CountDITL(dialog)+1;
    const int offsetCheckBox=firstNewItem;
    const int offsetLabel=offsetCheckBox+1;
    const int offsetSelect=offsetLabel+1;
    const int offsetInfo=offsetSelect+1;
    //const int offsetClear=offsetInfo+1;

    int crtItem = firstNewItem;
    for (int i=0; i<NB_ROMSLOTS; i++) {
        // Get a copy of the items list and append it to the dialog's DITL
        AppendDITL(dialog, GetResource('DITL', kRomItems), overlayDITL);
        // reposition the items
        SetRomItem(dialog, crtItem, i);
        crtItem+=nbItemPerRom;
    }

    AppendDITL(dialog, GetResource('DITL',kRomItems), overlayDITL);
    Str255 &label(tmpStrs[NB_ROMSLOTS*2]);
    CopyCStringToPascal("LowerRom", label);
    paths[NB_ROMSLOTS] = gConfig.lowerRom.path;
    Str255 &romDesc(tmpStrs[NB_ROMSLOTS*2+1]);
    CopyCStringToPascal(gConfig.lowerRom.description, romDesc);
    // reposition the items
    for (int j=0; j<nbItemPerRom; j++) {
        SetRomItem(dialog, j, crtItem, 16*29+10, 20, gConfig.lowerRom.active, label, romDesc);
        crtItem++;
    }

    SetDialogDefaultItem(dialog,kDialogOk);

    DrawDialog(dialog);
    ShowWindow(GetDialogWindow(dialog));

    gCon=this;
    ModalFilterUPP filter=NewModalFilterUPP(StdDialogFilter);
    int done=-1;
    while (done==-1) {
        short itemHit;
        ModalDialog(filter,&itemHit);

        short iKind;
        Handle iHandle;
        Rect iRect;
        // get the control handle for the popup
        GetDialogItem ( dialog, kStartupDiskASelect, &iKind, &iHandle, &iRect) ;
        InvalWindowRect(GetDialogWindow(dialog), &iRect);
        switch(itemHit) {
            case kRomOk:
                for (int i=0; i<NB_ROMSLOTS; i++) {
                    SetItemRom(dialog, offsetCheckBox+i*nbItemPerRom, gConfig.upperRoms[i], paths[i]);
                }
                SetItemRom(dialog, offsetCheckBox+NB_ROMSLOTS*nbItemPerRom, gConfig.lowerRom, paths[NB_ROMSLOTS]);
                done=1;
                break;
            case kRomCancel:
                done=0;
                break;
            default: {
                int romNb = (itemHit-firstNewItem)/nbItemPerRom;
                int action = (itemHit-firstNewItem)-nbItemPerRom*romNb;
                switch (action) {
                    case 0: // Active toggle
                        ToggleItemVal(dialog,firstNewItem+romNb*nbItemPerRom);
                        break;
                    case 2: // Select
                    {
                        std::string path;
                        std::string desc;
                        currentDialog=dialog;
                        if (SelectRom(path,desc,romNb)) {
                            SetItemText(dialog, offsetInfo+romNb*nbItemPerRom, desc.c_str());
                            paths[romNb] = path;
                            SetItemVal(dialog, offsetCheckBox+romNb*nbItemPerRom, 1);
                            //SetTypeCreator(path.c_str(),FileRom);
                        }
                        currentDialog=0;
                    } break;
                    case 4: // Clear
                        SetItemText(dialog, offsetInfo+romNb*nbItemPerRom, "");
                        paths[romNb].clear();
                        SetItemVal(dialog, offsetCheckBox+romNb*nbItemPerRom, 0);
                        break;
                }
            }break;
        }
        DrawDialog(dialog);
    }
    DisposeModalFilterUPP(filter);
    DisposeDialog(dialog);
    for (int i=0; i<NB_ROMSLOTS; i++) {
    }
}

#pragma mark - Startup
void OsConsole::DoStartupDialog()
{
    DialogPtr dialog;
    int done=-1;
    short itemHit;
    std::string str;

    if (!(dialog=GetNewDialog(kStartupDialog,0,(WindowPtr) -1)))
        return;

    SetItemText(dialog,kStartupDiskA,gConfig.DriveADiskPath);
    SetItemText(dialog,kStartupDiskB,gConfig.DriveBDiskPath);
    SetItemText(dialog,kStartupSnapshot,gConfig.SnapshotPath);
    SetItemText(dialog,kStartupCommand,gConfig.StartupComm);
    SetDialogDefaultItem(dialog,kStartupOk);
    SelectDialogItemText(dialog, kStartupCommand, 0, 32767);

    ShowWindow(GetDialogWindow(dialog));
    DrawDialog(dialog);

    gCon=this;
    ModalFilterUPP filter=NewModalFilterUPP(StdDialogFilter);
    while (done==-1) {
        ModalDialog(filter,&itemHit);

        short iKind;
        Handle iHandle;
        Rect iRect;
        // get the control handle for the popup
        GetDialogItem ( dialog, kStartupDiskASelect, &iKind, &iHandle, &iRect) ;
        InvalWindowRect(GetDialogWindow(dialog), &iRect);

        switch(itemHit) {
            case kStartupDiskASelect:
                currentDialog=dialog;
                if (SelectFloppyImage(str,0))
                    SetItemText(dialog,kStartupDiskA,str.c_str());
                currentDialog=0;
                break;
            case kStartupDiskAClear:
                SetItemText(dialog,kStartupDiskA,"");
                break;
            case kStartupDiskBSelect:
                currentDialog=dialog;
                if (SelectFloppyImage(str,1))
                    SetItemText(dialog,kStartupDiskB,str.c_str());
                currentDialog=0;
                break;
            case kStartupDiskBClear:
                SetItemText(dialog,kStartupDiskB,"");
                break;
            case kStartupSnapshotSelect:
                currentDialog=dialog;
                if (SelectSnapshot(str))
                    SetItemText(dialog,kStartupSnapshot,str.c_str());
                currentDialog=0;
                break;
            case kStartupSnapshotClear:
                SetItemText(dialog,kStartupSnapshot,"");
                break;
            case kStartupCommClear:
                SetItemText(dialog,kStartupCommand,"");
                break;
            case kStartupOk: {
                std::string temp;
                GetItemText(dialog,kStartupDiskA, temp);
                strncpy(gConfig.DriveADiskPath, temp.c_str(), PATH_BYTES);
                GetItemText(dialog,kStartupDiskB, temp);
                strncpy(gConfig.DriveBDiskPath, temp.c_str(), PATH_BYTES);
                GetItemText(dialog,kStartupSnapshot, temp);
                strncpy(gConfig.SnapshotPath, temp.c_str(), PATH_BYTES);
                GetItemText(dialog,kStartupCommand, temp);
                strncpy(gConfig.StartupComm, temp.c_str(), PATH_BYTES);
                done=1;
            } break;
            case kStartupCancel:
                done=0;
                break;
        }
        DrawDialog(dialog);
    }
    DisposeModalFilterUPP(filter);
    DisposeDialog(dialog);
    return;
}

#pragma mark - Keyboard

// Direct read of Mac Keyboard
// Returns key code >= 0 iif a single Mac key is pressed
int OsConsole::GetSingleMacKey()
{
    int macKey=-1;

    KeyMap keyMap;
    GetKeys(keyMap); // Get map of Mac keys state
    unsigned long newState[4];
    for (int i=0; i<4; i++)
        newState[i] = ntohl(keyMap[i].bigEndianValue);

    for (int i=0; i<4; i++) {
        int newBit = newState[i];
        for (int j=0; j<32; j++) {
            if (newBit & 1) {
                if (macKey >=0) // Allow only one at a time
                    return -1;
                macKey = 32*i + 8 * (3-(j/8)) + (j%8);
            }
            newBit >>= 1;
        }
    }
    return macKey;
}

void OsConsole::OutlineKey(DialogPtr dialog, short which)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    GetDialogItem(dialog, which, &itemType, &itemHandle, &itemRect);
    SetPortDialogPort(dialog);
    InsetRect(&itemRect, -2, -2);
    PenSize(2, 2);
    RGBForeColor(&colors[RED]);
    FrameRect(&itemRect);
    CGrafPtr thePort = GetDialogPort(dialog);
    // flush part of the port
    if (QDIsPortBuffered(thePort)) {
        RgnHandle theRgn;
        theRgn = NewRgn();
        RectRgn(theRgn, &itemRect);
        QDFlushPortBuffer(thePort, theRgn);
        DisposeRgn(theRgn);
    }
}


// Don't know how to pass a handle to a filter proc
// so put it in global (does it work with 68K?)
struct FilterProcData {
    BYTE temp[128];
} KbdDialogData;
pascal Boolean OsConsole::KbdDlgFilter(DialogPtr dialog, EventRecord *event, short */*itemHit*/)
{
    static int lastMacKey=-1;
    int macKey,cpcKey;

    // When a Mac key is pressed, we highlight the CPC key
    // Scan mac kdb to see if a key is pressed
    if ((macKey=gCon->GetSingleMacKey())>=0) {
        // Get the corresponding CPC key
        cpcKey = KbdDialogData.temp[macKey];
        if (cpcKey!=0xFF) {
            // Get the corresponding dialog item
            int item = kFirstCpcKey + cpcKey;
            // Highlight it
            OutlineKey(dialog, item);
            if (cpcKey==78) OutlineKey(dialog,kFirstCpcKey+21); // Second Shift key
            if (cpcKey==21) OutlineKey(dialog,kFirstCpcKey+78); // Second Shift key
            char buff[256];
            std::string buff2 = gCon->GetStr(MSG_CON_MACKEYSSHOW);
            sprintf(buff,buff2.c_str(), macKeyStr[macKey], cpcKeyStr[cpcKey]);
            gCon->SetItemText(dialog,kKeyboardMapping,buff);
        } else {
            char buff[256];
            std::string buff2 = gCon->GetStr(MSG_CON_MACKEYFREE);
            sprintf(buff,buff2.c_str(), macKeyStr[macKey]);
            gCon->SetItemText(dialog,kKeyboardMapping,buff);
        }
    }
    if (macKey!=lastMacKey) {
        DrawDialog(dialog); // did not work anymore without it lately 991216
        lastMacKey=macKey;
    }

    switch(event->what) {
        case keyDown: case keyUp: case autoKey: // Filter out kbd events
            return true;
        case updateEvt: // Handle refresh
            gCon->HandleUpdate((WindowPtr)event->message,dialog);
            return true;
        default:
            return false;
    }
}

void OsConsole::DoKeyboardDialog()
{
    DialogPtr dialog;
    int done=-1;
    short itemHit;
    UInt32 i;
    int cpcKey, macKey, prevMacKey;

    if (!(dialog=GetNewDialog(kKeyboardDialog,0,(WindowPtr) -1))) return;
    char buff[256];
    SetItemText(dialog,kKeyboardMsg,GetStr(MSG_CON_KEYPRESS).c_str());
    ShowWindow(GetDialogWindow(dialog));

    // setup a temporary map
    for (i=0; i<128; i++)
        KbdDialogData.temp[i] = guiPrefs.keyConv[i];
    gCon=this;
    ModalFilterUPP filter=NewModalFilterUPP(KbdDlgFilter);
    while (done==-1) {
        ModalDialog(filter,&itemHit);
        switch(itemHit) {
            case kKeyboardOk:
                for (i=0; i<128; i++)
                    guiPrefs.keyConv[i] = KbdDialogData.temp[i];
                done=1;
                break;
            case kKeyboardCancel:
                done=0;
                break;
            case kKeyboardDefault:
                for (i=0; i<128; i++)
                    KbdDialogData.temp[i] = keyConv0[i];
                break;
            default:
                cpcKey = itemHit-kFirstCpcKey;
                if (cpcKey==78) cpcKey=21;  // Both shift keys are the same
                if (cpcKey>=0 && cpcKey<=79) {
                    macKey = prevMacKey = -1;
                    for (i=0; i<128; i++) // Find current mapping for this key
                        if (KbdDialogData.temp[i]==cpcKey) {
                            if (prevMacKey!=-1)
                                ErrReport(ERR_CON_KEYMAPPEDTWICE);
                            prevMacKey=i;
                        }
                    macKey=GetSingleMacKey();
                    if (macKey < 0) goto noKey;
                    if (prevMacKey>=0)
                        KbdDialogData.temp[prevMacKey] = 0xFF; // Unset previous mapping
                    KbdDialogData.temp[macKey] = cpcKey; // Set mapping
                    sprintf(buff,GetStr(MSG_CON_KEYASSIGN).c_str(), cpcKeyStr[cpcKey], macKeyStr[macKey]);
                    SetItemText(dialog,kKeyboardMapping,buff);
                    break;
                noKey:
                    if (prevMacKey < 0) goto noKey2;
                    sprintf(buff,GetStr(MSG_CON_CPCKEYSHOW).c_str(), cpcKeyStr[cpcKey], macKeyStr[prevMacKey]);
                    SetItemText(dialog,kKeyboardMapping,buff);
                    break;
                noKey2:
                    sprintf(buff,GetStr(MSG_CON_CPCKEYFREE).c_str(), cpcKeyStr[cpcKey]);
                    SetItemText(dialog,kKeyboardMapping,buff);
                    break;
                }
                break;
        }
    }
    DisposeModalFilterUPP(filter);
    DisposeDialog(dialog);
}

#pragma mark - File Open
static bool IsRomImage(const std::string &path, const BYTE *data)
{
    if (CpcGuiConsole::IsRomImage(path, data)) return true;
    if (MacUtils::IsFinderFileType(path, CpcGuiConsole::FileRom)) return true;
    return false;
}

static bool IsDiskImage(const std::string &path, const BYTE *data)
{
    if (CpcGuiConsole::IsDiskImage(path, data)) return true;
    if (MacUtils::IsFinderFileType(path, CpcGuiConsole::FileDisk)) return true;
    return false;
}

static bool IsSnapshot(const std::string &path, const BYTE *data)
{
    if (CpcGuiConsole::IsSnapshot(path, data)) return true;
    if (MacUtils::IsFinderFileType(path, CpcGuiConsole::FileSnapshot)) return true;
    return false;
}

static bool IsZipFile(const std::string &path)
{
    if (CpcGuiConsole::IsZipFile(path)) return true;
    if (MacUtils::IsFinderFileType(path, CpcGuiConsole::FileZip)) return true;
    return false;
}

bool OsConsole::FilterFile(const std::string &path, FileType type)
{
    TypeFilter filter = GetFilter(type);
    return FilterFile(path, filter);
}

bool OsConsole::FilterFile(const std::string &path, TypeFilter filter)
{
    // See if it looks OK based on the path
    if ((*filter)(path,0)) return true;

    // If it's an archive look for a file in there
    if (::IsZipFile(path)) {
        unzFile zFile = unzOpen(path.c_str());
        ASSERT_THROW(zFile);
        unz_global_info info;
        unzGetGlobalInfo(zFile, &info);
        for (unsigned int zIdx=0; zIdx<info.number_entry; zIdx++) {
            unz_file_info fInfo;
            char zfpath[128];
            char zeField[128];
            char zComm[128];
            unzGetCurrentFileInfo(zFile, &fInfo, zfpath,128,zeField,128,zComm,128);
            if ((*filter)(zfpath,0)) {
                unzClose(zFile);
                return true;
            }
            BYTE buff[32];
            if (unzOpenCurrentFile(zFile)!=UNZ_OK) {
                unzClose(zFile);
                return false;
            }
            if (unzReadCurrentFile(zFile, buff, 32)!=32) {
                unzCloseCurrentFile(zFile);
                unzClose(zFile);
                return false;
            }
            unzCloseCurrentFile(zFile);
            if ((*filter)("",buff)) {
                unzClose(zFile);
                return true;
            }
            unzGoToNextFile(zFile);
        }
        unzClose(zFile);
    }

    // Look at the data
    ifstream in(path.c_str());
    BYTE data[128];
    in.read(reinterpret_cast<char *>(data), 128);
    if (!in) return false;
    if ((*filter)("",data)) return true;

    return false; // else reject
}

pascal Boolean OsConsole::myFilterProcGlue(AEDesc* theItem,void* info, NavCallBackUserData callBackUD, NavFilterModes filterMode)
{
    OsConsole *This = (OsConsole *)callBackUD;
    return This->myFilterProc(theItem,info,filterMode);
}

Boolean OsConsole::myFilterProc(AEDesc* theItem,void* info, NavFilterModes /*filterMode*/)
{
    try {
        if (theItem->descriptorType!=typeFSS) return true; // only mask Files

        NavFileOrFolderInfo* theInfo = (NavFileOrFolderInfo*)info;
        if (theInfo->isFolder) return true; // don't mask folders

        FSSpec *myFSS = *(FSSpec **)theItem->dataHandle;
        std::string path;
        GetFullPath(*myFSS, path);
        return FilterFile(path, _filter);
    } catch (Err &err) {
        LOG_STATUS(err.What().c_str());
        return false;
    }
}

void OsConsole::HandleCustomMouseDown(NavCBRecPtr callBackParms)
{
    OSErr theErr = noErr;
    ControlHandle whichControl;
    Point where = callBackParms->eventData.eventDataParms.event->where;
    short theItem = 0;
    UInt16 firstItem = 0;
    short realItem = 0;
    short partCode = 0;
    WindowPtr window = callBackParms->window;
    DialogRef dialog = GetDialogFromWindow(window);
    //DialogPtr *dialog = (DialogPtr *)callBackParms->window;

    GlobalToLocal(&where);
    theItem = FindDialogItem(dialog, where);    // get the item number of the control
    partCode = FindControl(where,callBackParms->window,&whichControl);  // get the control itself

    // ask NavServices for the first custom control's ID:
    if (callBackParms->context != 0) {  // always check to see if the context is correct
        theErr = NavCustomControl(callBackParms->context,kNavCtlGetFirstControlID,&firstItem);
        realItem = theItem - firstItem + 1;     // map it to our DITL constants:
    }

    if (realItem == 1) // That's our checkbox
        SetControlValue(whichControl,driveSelection^=1);
}

// Navigation service event proc
pascal void OsConsole::myEventProcGlue(NavEventCallbackMessage callBackSelector, NavCBRecPtr callBackParms, NavCallBackUserData callBackUD)
{
    OsConsole *This = (OsConsole *)callBackUD;
    This->myEventProc(callBackSelector, callBackParms);
}

// Manage the DriveB check box extra control in file selection dialog
void OsConsole::myEventProc(NavEventCallbackMessage callBackSelector, NavCBRecPtr callBackParms)
{
    WindowPtr window = callBackParms->window;
    DialogRef dialog = GetDialogFromWindow(window);
    //DialogPtr *dialog = (DialogPtr *)callBackParms->window;

    switch (callBackSelector) {
        case kNavCBCustomize: // Get a customization rectangle
            if (wantDriveSelection) {
                Rect &rect = callBackParms->customRect;
                if (rect.right-rect.left<300 || rect.bottom-rect.top<20) {
                    rect.right = rect.left+300;
                    rect.bottom = rect.top+20;
                }
            }
            break;
        case kNavCBStart: // Install our custom control
            if (wantDriveSelection) {
                customDitl = GetResource('DITL',139);
                NavCustomControl(callBackParms->context, kNavCtlAddControlList, customDitl);
                UInt16  firstItem = 0;
                NavCustomControl(callBackParms->context,kNavCtlGetFirstControlID,&firstItem);   // ask NavServices for our first control ID

                // set the current drive selection
                short realItem = firstItem + 1;
                short itemType;
                Rect itemRect;
                Handle itemH;
                GetDialogItem(dialog,realItem,&itemType,&itemH,&itemRect);
                SetControlValue((ControlHandle)itemH,driveSelection);
            }
            break;
        case kNavCBTerminate: // release our resource
            if (wantDriveSelection)
                ReleaseResource(customDitl);
            break;
        case kNavCBEvent:
            EventRecord *event = callBackParms->eventData.eventDataParms.event;
            switch (event->what) {
                case updateEvt:
                    HandleUpdate((WindowPtr)event->message);
                    break;
                case mouseDown:
                    if (wantDriveSelection)
                        HandleCustomMouseDown(callBackParms);
                    break;
            }
            break;
    }
}

// Select a file
bool OsConsole::SelectFile(std::string &path, FileType &type, int &option)
{
    if (screenFull) ReleaseFullScreen();

    NavDialogOptions dialogOptions;
    NavEventUPP eventProc = NewNavEventUPP(myEventProcGlue);
    NavObjectFilterUPP filterProc;

    if (NavGetDefaultDialogOptions(&dialogOptions)!=noErr) return false; // Get default options
    dialogOptions.dialogOptionFlags &= ~kNavAllowMultipleFiles; // Only one file at a time

    char prompt[256];
    std::string title;
    wantDriveSelection=false;
    driveSelection=0;
    if (type==FileDisk) {
        title = GetStr(MSG_CON_OPEN_TITLE_DSK);
        sprintf(prompt,GetStr(MSG_CON_OPEN_PROMPT_DSK).c_str(),option?'B':'A');
        _filter = ::IsDiskImage;
        dialogOptions.dialogOptionFlags ^= kNavAllowPreviews;
    } else if (type==FileSnapshot) {
        title = GetStr(MSG_CON_OPEN_TITLE_SNA);
        strcpy(prompt, GetStr(MSG_CON_OPEN_PROMPT_SNA).c_str());
        _filter = ::IsSnapshot;
    } else if (type==FileRom) {
        title = GetStr(MSG_CON_OPEN_TITLE_ROM);
        sprintf(prompt,GetStr(MSG_CON_OPEN_PROMPT_ROM).c_str(),option);
        _filter = ::IsRomImage;
        dialogOptions.dialogOptionFlags ^= kNavAllowPreviews;
    } else {
        title = GetStr(MSG_CON_OPEN_TITLE_ANY);
        strcpy(prompt, GetStr(MSG_CON_OPEN_PROMPT_ANY).c_str());
        _filter = IsAnyFile;
        wantDriveSelection=true;
    }
    CopyCStringToPascal(prompt,dialogOptions.message); // Prompt string
    CopyCStringToPascal(title.c_str(),dialogOptions.windowTitle); // Window title

    NavReplyRecord reply;
    filterProc = NewNavObjectFilterUPP(myFilterProcGlue);

    // Call NavGetFile() with specified options and declare our app-defined functions and type list
    if (NavGetFile (0, &reply, &dialogOptions, eventProc, 0, filterProc, 0, this)!=noErr) return false;
    if (!reply.validRecord) return false;

    AEKeyword theKeyword;
    DescType actualType;
    Size actualSize;
    FSSpec documentFSSpec;

    // Get a pointer to selected file
    if (AEGetNthPtr(&(reply.selection), 1, typeFSS, &theKeyword, &actualType,&documentFSSpec,sizeof(documentFSSpec), &actualSize)!=noErr) return false;

    std::string fName;
    GetFullPath(documentFSSpec,fName);
    path = fName;
    if (type == FileDisk || type==FileAny)
        option = driveSelection;

    //  Dispose of NavReplyRecord, resources, descriptors
    NavDisposeReply(&reply);
    if (screenFull) GetFullScreen();
    DisposeNavEventUPP(eventProc);
    DisposeNavObjectFilterUPP(filterProc);
    return true;
}



#pragma mark - File Save

bool OsConsole::GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path)
{
    std::string promptStr= GetStr(prompt);
    std::string name = path.empty()? GetStr(defaultName): path;

    if (screenFull) ReleaseFullScreen();
    NavReplyRecord theReply;
    NavDialogOptions dialogOptions;
    NavEventUPP eventProc = NewNavEventUPP(myEventProcGlue);

    wantDriveSelection=false;
    NavGetDefaultDialogOptions (&dialogOptions);
    dialogOptions.dialogOptionFlags |= (kNavDontAddTranslateItems + kNavNoTypePopup);

    CopyCStringToPascal(promptStr.c_str(), dialogOptions.windowTitle);
    CopyCStringToPascal(name.c_str(), dialogOptions.savedFileName);
    CopyCStringToPascal("CPC++", dialogOptions.clientName);

    if (NavPutFile(0, &theReply, &dialogOptions, eventProc, 'ttxt', 'TEXT', this)!=noErr) goto Error;

    if (!theReply.validRecord) goto Error;

    AEKeyword theKeyword;
    DescType actualType;
    Size actualSize;
    {
        FSSpec documentFSSpec;

        // Get a pointer to selected file
        if (AEGetNthPtr(&(theReply.selection), 1, typeFSS, &theKeyword, &actualType,&documentFSSpec,
                        sizeof(documentFSSpec), &actualSize)!=noErr) goto Error;
        GetFullPath(documentFSSpec, path);
    }
    NavDisposeReply(&theReply);
    DisposeNavEventUPP(eventProc);
    if (screenFull) GetFullScreen();
    return true;
Error:
    DisposeNavEventUPP(eventProc);
    if (screenFull) GetFullScreen();
    return false;
}

#pragma mark • Attributes Change •

void OsConsole::SetWindow()
{
    // Deal with full screen mode
    if (screenFull != lastScreenMode) {
        if (screenFull)
            GetFullScreen();
        else
            ReleaseFullScreen();
        lastScreenMode=screenFull;
    }
    int menuHeight = screenFull? 0: GetMBarHeight();
    int barHeight = indicators? kBarH: 0;

    // Get available screen size
    BitMap screenBits;
    GetQDGlobalsScreenBits(&screenBits);

    int macScrW = screenBits.bounds.right-screenBits.bounds.left;
    int macScrH = screenBits.bounds.bottom-screenBits.bounds.top-menuHeight;

    // Select cpc screen size
    cpcScrW = Ga::CPC_SCR_W;
    cpcScrH = Ga::CPC_SCR_H;

    // Desired window size
    if (screenFull) {
        mainWinH = macScrH;
        mainWinW = macScrW;
    } else if (screenSmall) {
        mainWinH = cpcScrH/2+barHeight;
        mainWinW = cpcScrW/2;
    } else {
        mainWinH = cpcScrH+barHeight;
        mainWinW = cpcScrW;
    }

    // clip window to screen
    MINIMIZE(mainWinH, macScrH);
    MINIMIZE(mainWinW, macScrW);

    // cpc screen in window
    if (screenSmall) {
        cpcScrC = (mainWinW-cpcScrW/2)/2;
        cpcScrR = (mainWinH-barHeight-cpcScrH/2)/2+barHeight;
        // Set blitting clip in cpc coordinates
        blitX0 = -cpcScrC;
        blitX1 = mainWinW*2-cpcScrC-1;
        blitY0 = -(cpcScrR-barHeight);
        blitY1 = mainWinH*2-cpcScrR-1;
    } else {
        cpcScrC = (mainWinW-cpcScrW)/2;
        cpcScrR = (mainWinH-barHeight-cpcScrH)/2+barHeight;
        // Set blitting clip in cpc coordinates
        blitX0 = -cpcScrC;
        blitX1 = mainWinW-cpcScrC-1;
        blitY0 = -(cpcScrR-barHeight);
        blitY1 = mainWinH-cpcScrR-1;
    }

    if (blitX0<0) blitX0=0;
    if (blitX1>Ga::CPC_SCR_W) blitX1=Ga::CPC_SCR_W;
    if (blitY0<0) blitY0=0;
    if (blitY1>Ga::CPC_SCR_H) blitY1=Ga::CPC_SCR_H;

    // get desired window position
    if (screenSmall) {
        mainWinC = guiPrefs.sWinX;
        mainWinR = guiPrefs.sWinY;
    } else {
        mainWinC = guiPrefs.bWinX;
        mainWinR = guiPrefs.bWinY;
    }

    // Position window on screen
    if (screenFull) {
        if (mainWinC+mainWinW>macScrW)
            mainWinC = macScrW-mainWinW;
        if (mainWinC<0)
            mainWinC=0;
        if (menuHeight+mainWinR+mainWinH>macScrH)
            mainWinR = menuHeight+macScrH-mainWinH;
        if (mainWinR<menuHeight)
            mainWinR=menuHeight;
    }

    // Update menus

    // Apply window parameters
    SizeWindow(mainWin,mainWinW,mainWinH,true);
    MoveWindow(mainWin,mainWinC,mainWinR,false);
    ShowWindow(mainWin);
    SelectWindow(mainWin);
    SetPortWindowPort(mainWin);

    Update();
}

void OsConsole::SetRenderingInfo(CpcBase::RenderMode rMode, CpcBase::RenderInfo &renderInfo)const
{
    PixMap **mainWinPixmap = GetPortPixMap(GetWindowPort(mainWin));
    Rect &bounds = (*mainWinPixmap)->bounds;
    int mainWinBPR = (*mainWinPixmap)->rowBytes & 0x3fff;
    Ptr mainWinPt = GetPixBaseAddr(mainWinPixmap) - bounds.left * 32/8 - bounds.top * mainWinBPR;
    renderInfo.base = (unsigned char *)mainWinPt + cpcScrR * mainWinBPR + cpcScrC * 32/8;
    renderInfo.bytesPerRow = mainWinBPR;
    renderInfo.clip.Set(blitY0,blitX0,blitY1-1,blitX1-1);
    renderInfo.renderMode = rMode;
    renderInfo.lut = gConfig.Monochrome? M32(): P32();
}


void OsConsole::InBackground(bool val)
{
    inBackground = val;
    if (val && screenFull) ReleaseFullScreen();
    if (!val && screenFull) GetFullScreen();
    //  Update(); - generates access errors at startup call from wire
}

int OsConsole::displayChanged=0;
pascal void OsConsole::DisplayChanged(AppleEvent * /*theEvent*/)
{
    displayChanged=1;
}

#pragma mark - ctor/dtor

// Initialize the console:
// -Initialize managers (set display to 8 bits)
// -Create windows (main,message,kMonitor)
// -Create colormap
// -Create off-screen buffer (or screen direct buffer)
//  Note that this buffer will be scaled by two in vertical direction
//  for correct display
OsConsole::OsConsole(int argc, char **argv)
: CpcGuiConsole(argc, argv)
, prOut(0)
, screenSmall(false)
, screenFull(false)
, screenSkip(false)
, indicators(false)
, hold(false)
, inBackground(false)
, startupFlag(false)
, mainWin(0)
, mainWinW(0), mainWinH(0), mainWinR(0), mainWinC(0)
, cpcScrW(0), cpcScrH(0), cpcScrR(0), cpcScrC(0)
, blitX0(0), blitX1(0), blitY0(0), blitY1(0)
, startTime(0)
, crtTick(0)
, dirtyRect()
, lastScreenMode(0)
, barRect()
, barGWorld(0)
, openAppUPP(0), openDocUPP(0), quitAppUPP(0), displayChangedUPP(0)
, wantDriveSelection(false)
, driveSelection(0)
, customDitl(0)
, _filter(0)
, crtGetFloppyFileType(0)
, crtGetSnaFileType(0)
, crtGetRomFileType(0)
, currentDialog(0)
, fileType(DSK)
, fontGWorld(0), fontRect()
, monit(this)
, monitVisible(false)
{
    // Appearance manager
    long result;
    if (Gestalt(gestaltAppearanceAttr, &result)==noErr)
        RegisterAppearanceClient();

    SetEventMask(everyEvent); // Allow keyUp events

    // Check for resource override
    SetupResourcePath();

    // Note starting time
    GetDateTime(&startTime);
    crtTick=0; // Counter for demo timeout

    // Check beta expiration date
    static int endYear=2100, endMonth=11, endDay=30;
    int endCount = endYear*400+endMonth*35+endDay;
    DateTimeRec dtr;
    SecondsToDate(startTime, &dtr);
    int crtCount = dtr.year*400+dtr.month*35+dtr.day;
    ASSERT_THROW(crtCount<=endCount)("This beta version expired on %04d/%02d/%02d", endYear, endMonth, endDay);

    // Init attributes
    memset((char *)kbdState, 0, sizeof(kbdState));
    crtGetFloppyFileType=2;
    crtGetSnaFileType=2;
    crtGetRomFileType=2;
    startupFlag=1;
    lastScreenMode=screenFull=0;
    runMenuItemsCount[0]=runMenuItemsCount[1]=0;

    // Get config
    LoadConfig();

    // Do Menu Bar
    SetMenuBar(GetNewMBar(kMenuBarX));

    InsertMenu(GetMenu(kDriveAMenu), -1);
    InsertMenu(GetMenu(kDriveBMenu), -1);
    InsertMenu(GetMenu(kSoundMenu), -1);
    InsertMenu(GetMenu(kMonitorMenu), -1);
    InsertMenu(GetMenu(kModelMenu), -1);
    InsertMenu(GetMenu(kVolumeMenu), -1);
    InsertMenu(GetMenu(kCrtcMenu), -1);
    InsertMenu(GetMenu(kConstructorMenu), -1);
    InsertMenu(GetMenu(kKeyboardMenu), -1);
    InsertMenu(GetMenu(kPrinterMenu), -1);
    InsertMenu(GetMenu(kFrameRateMenu), -1);
    InsertMenu(GetMenu(kSpeedLimitMenu), -1);
    InsertMenu(GetMenu(kRunMenu), -1);
    InsertMenu(GetMenu(kRunMenuB), -1);
    DrawMenuBar();

    // Create windows
    ASSERT_THROW((mainWin=GetNewCWindow(kMainWin,0,(WindowPtr) -1)))("Missing resource in console init.");

    SetPortWindowPort(mainWin);

    // Handle Display Manager events
    ProcessSerialNumber psn;
    GetCurrentProcess(&psn);
    displayChangedUPP = NewDMNotificationUPP(DisplayChanged);
    DMRegisterNotifyProc(displayChangedUPP,&psn);

    // Setup bar
    DrawPictIntoNewGWorld(kBarPICT, barGWorld, &barRect);

    // Init events handling
    FlushEvents(everyEvent,0);
    openAppUPP = NewAEEventHandlerUPP(OpenApplHdlr);
    openDocUPP = NewAEEventHandlerUPP(OpenDocsHdlrGlue);
    quitAppUPP = NewAEEventHandlerUPP(QuitApplHdlr);
    AEInstallEventHandler(kCoreEventClass, kAEOpenApplication, openAppUPP, (long)this, false);
    AEInstallEventHandler(kCoreEventClass, kAEOpenDocuments, openDocUPP, (long)this, false);
    AEInstallEventHandler(kCoreEventClass, kAEQuitApplication, quitAppUPP, (long)this, false);

    Cursor arrow;
    SetCursor (GetQDGlobalsArrow(&arrow));

    // Load the CPC font picture
    MacUtils::DrawPictIntoNewGWorld(kAmstradFontPict,fontGWorld,&fontRect);
    ASSERT_THROW(fontGWorld);
    fontPixmap = GetGWorldPixMap(fontGWorld);

    // Monitor
    ASSERT_THROW((monitWin=GetNewCWindow(kMonitWin,0,(WindowPtr) -1)))("Monitor window creation error");
    CpcRect monitWinRect = monit.WinRect();
    HideWindow(monitWin); // In case rsrc if wrong
    SizeWindow(monitWin,monitWinRect.Width(),monitWinRect.Height(),true);
    MoveWindow(monitWin,monitWinRect.Left(),monitWinRect.Top(),false);

    // Sound

    // Check for stereo
    stereoCapable = false;
    long features;
    if (Gestalt(gestaltSoundAttr, &features)==noErr)
    {
        stereoCapable = BitTst(&features,1<<gestaltStereoCapability);
    }

    // Create sound channel
    sndChan=0;
    sndCallBackUPP = NewSndCallBackUPP(SndCallBackGlue);
    ASSERT_THROW(SndNewChannel(&sndChan, sampledSynth,initNoInterp + (stereoCapable? initStereo: initMono),sndCallBackUPP)==noErr)("Sound channel creation failed")("Sound init failed");

    // Get the sound hardware info for this Mac
    GetHardwareSettings(sndChan,&hardwareInfo);

    // I tried changing the Mac rate to a multiple of the CPC actual rates
    // (like 125000) but it does not sound that good.
    // Note that the default mac rate is 44100Hz (CD rate).
    // However the CPC's AY, with a base rate of 125KHz (1MHz/8) can potentialy
    // generate tones up to 62.5KHz. These are not hearable, but they can create
    // lower frequency beats with the subsampling frequency.
    // This needs more testing and tuning
    //     hardwareInfo.sampleRate = 125000*65536.0;

    // this is key to the rendering quality.
    // Too short and we endanger the synchronization
    // Too long and we have noticeable delay
    // Also, it shoud be a value that divides the sampleRate cleanly.
    static const float BUFF_TIME=0.05;

    // Compute the buffer sizes

    // Init Psg double buffering
    buffNSamples = static_cast<int>(BUFF_TIME * hardwareInfo.sampleRate/65536);
    const int bytesPerSample = (stereoCapable?2:1) * (hardwareInfo.sampleSize==16? 2: 1);

    const int buffNBytes = buffNSamples*bytesPerSample;
    for (int i=0; i<NBUFFS; i++) {
        ASSERT_THROW(psgBuff[i] = new UINT8[buffNBytes])("Cannot allocate PSG sound buff 1")("Sound init failed");
    }

    // Allocate a sound header with a silence buffer at the end
    ASSERT_THROW(sndHdr=(ExtSoundHeaderPtr)NewPtrClear(sizeof(ExtSoundHeader)+WAIT_SIZE*bytesPerSample))("Cannot allocate snd buffer 0")("Sound init failed");
    sndHdr->numChannels = stereoCapable? 2: 1;
    sndHdr->sampleRate = hardwareInfo.sampleRate;
    sndHdr->encode = extSH; // extended sound header
    sndHdr->baseFrequency = 1;
    sndHdr->sampleSize = hardwareInfo.sampleSize;
}

void OsConsole::ConnectCpc()
{
    // Setup interface for Psg
    CpcBase::SoundInfo sndInfo;
    sndInfo.encodeBased = true;
    sndInfo.canStereo = CanStereo();
    sndInfo.precision = (hardwareInfo.sampleSize==16)?2:1;
    sndInfo.frequencyHz = (int)(hardwareInfo.sampleRate/65536.0);
    cpc.SetSoundInfo(sndInfo);
    
    CpcGuiConsole::ConnectCpc();
    SetWindow();
    ShowWindow(mainWin);
    InBackground(false);
    monit.SetCpc(cpc);
}

// Free up/clean up everything created at init time
OsConsole::~OsConsole()
{
    SaveConfig();
    if (screenFull) ReleaseFullScreen();
    if (mainWin) DisposeWindow(mainWin);
    if (prOut) fclose(prOut);
    if (barGWorld) DisposeGWorld(barGWorld);
    if (fontGWorld) DisposeGWorld(fontGWorld);

    DisposeDMNotificationUPP(displayChangedUPP);
    DisposeAEEventHandlerUPP(openAppUPP);
    DisposeAEEventHandlerUPP(openDocUPP);
    DisposeAEEventHandlerUPP(quitAppUPP);
}

#pragma mark • CpcConsole •

std::string OsConsole::GetStr(CpcGuiMsgId errId)
{
    return GetStr(static_cast<MsgId>(errId), 128);
}

std::string OsConsole::GetStr(MsgId errId, int block)
{
    Str255 tmpStr;
    char tmpStr2[256];
    GetIndString(tmpStr, block, errId+1);
    if (!tmpStr[0])
        GetIndString(tmpStr, block, ERR_CON_DEFAULT+1);
    if (!tmpStr[0])
        return "Unknown error";
    CopyPascalStringToC(tmpStr, tmpStr2);
    return tmpStr2;
}

void OsConsole::ErrReport(MsgId errId, ...)
{
    va_list argP;
    va_start(argP, errId);
    std::string format = GetStr(errId);
    char str[128];
    vsprintf(str, format.c_str(), argP);
    va_end(argP);
    PutError(str);
}


void OsConsole::PutPrinter(BYTE val)
{
    if (!prOut) {
        if (!(prOut = fopen(gConfig.PrinterFile,"wb")))
            ErrReport(ERR_CON_PRNTOPEN);
    }
    if (prOut) {
        fwrite((char *)&val,1,1,prOut);
        fflush(prOut); // we need flushing or we will not see the end of it
    }
}

pascal Boolean OsConsole::ConfirmAlertFilter(DialogPtr dialog, EventRecord *event, short *itemHit)
{
    switch (event->what) {
        case keyDown:
            char theChar = (char)(event->message & charCodeMask);
            if ( ((event->modifiers&cmdKey)&&theChar=='.') ||
                (theChar==27)) {
                *itemHit = kDialogCancel;
                HiliteButton(dialog,kDialogCancel,1);
                return true;
            }
            if ( theChar==13 || theChar==3 ) {
                *itemHit = kDialogOk;
                HiliteButton(dialog,kDialogOk,1);
                return true;
            }break;
    }
    return false;
}

int OsConsole::ConfirmDlg(CpcGuiMsgId msgId, ...)
{
    // Look if we remember
    for (int i=0; i<guiPrefs.nbConfirms; i++) {
        if (guiPrefs.confirmIds[i]==msgId)
            return guiPrefs.confirmVal[i];
    }

    va_list argP;
    char str[128];
    va_start(argP, msgId);
    vsprintf(str, GetStr(msgId).c_str(), argP);
    va_end(argP);

    short itemHit;
    bool done=false,ok,remember=false;
    DialogPtr dialog;

    if (!(dialog=GetNewDialog(kConfirm,0,(WindowPtr) -1))) return 0;
    SetItemText(dialog,kConfirmText,str);

    if (screenFull) ReleaseFullScreen();
    ShowWindow(GetDialogWindow(dialog));
    SetDialogDefaultItem(dialog,kConfirmOk);

    gCon=this;
    ModalFilterUPP filter=NewModalFilterUPP(ConfirmAlertFilter);
    while (!done) {
        ModalDialog(filter,&itemHit);
        switch(itemHit) {
            case kConfirmOk:
                ok=true;
                done=true;
                break;
            case kConfirmCancel:
                ok=false;
                done=true;
                break;
            case kConfirmRemember:
                SetItemVal(dialog,kConfirmRemember,remember=remember?false:true);
                break;
        }
    }
    DisposeModalFilterUPP(filter);
    DisposeDialog(dialog);
    if (screenFull) GetFullScreen();
    if (remember && ok) {
        if (guiPrefs.nbConfirms>=MAX_CONFIRMS) {
            ErrReport(MSG_CON_CONFIRMOVERLOAD);
        } else {
            guiPrefs.confirmIds[guiPrefs.nbConfirms]=msgId;
            guiPrefs.confirmVal[guiPrefs.nbConfirms]=ok;
            guiPrefs.nbConfirms++;
        }
    }
    return ok;
}

// Refresh windows
void OsConsole::Update()
{
    PixMapHandle barPixmapH;
    Rect srcRect, dstRect;
    bool lastState=false;

    if (FrontWindow() != mainWin) {
        lastState = inBackground;
        InBackground(true);
    }

    SetPortWindowPort(mainWin);
    if (screenFull) {
        RGBForeColor(&colors[BLACK]);
        Rect windowBox;
        GetPortBounds(GetWindowPort(mainWin), &windowBox);
        PaintRect(&windowBox);
    }

    UpdateWindow(screenSmall? CpcBase::SMALL: screenSkip? CpcBase::SKIP: CpcBase::FULL);
    // Flush the window buffer
    RgnHandle visibleRgn=NewRgn();
    GetPortVisibleRegion(GetWindowPort(mainWin),visibleRgn);
    QDFlushPortBuffer(GetWindowPort(mainWin),visibleRgn);
    DisposeRgn(visibleRgn);

    if (indicators) {
        barPixmapH = GetGWorldPixMap(barGWorld);
        LockPixels(barPixmapH);
        srcRect = barRect;
        dstRect=srcRect;
        if (!screenSmall||screenFull)
            dstRect.right = dstRect.left+2*(dstRect.right-dstRect.left);
        CopyBits((BitMapPtr)*barPixmapH, GetPortBitMapForCopyBits(GetWindowPort(mainWin)), &srcRect, &dstRect, srcCopy, nil);
        UnlockPixels(barPixmapH);
    }
    if (FrontWindow() != mainWin)
        InBackground(lastState);
}

#pragma mark - Blitting

void OsConsole::UpdateWindow(CpcBase::RenderMode rMode)
{
    // Render current frame into the window buffer
    CpcBase::RenderInfo renderInfo;
    LockPortBits(GetWindowPort(mainWin));
    SetRenderingInfo(rMode,renderInfo);
    cpc.RenderFrame(renderInfo, false);
    UnlockPortBits(GetWindowPort(mainWin));
}

// Synchronize
void OsConsole::FrameReady()
{
    UpdateWindow(screenSmall? CpcBase::SMALL: screenSkip? CpcBase::SKIP: CpcBase::FULL);
    RgnHandle visibleRgn=NewRgn();
    GetPortVisibleRegion(GetWindowPort(mainWin),visibleRgn);
    QDFlushPortBuffer(GetWindowPort(mainWin),visibleRgn);
    DisposeRgn(visibleRgn);
}

#pragma mark - Keyboard

void OsConsole::UpdateKeyMap()
{
    UInt32 i,j;
    int virtualKey;

    KeyMap keyMap;
    GetKeys(keyMap); // Get map of Mac keys state
    unsigned long newState[4];
    for (i=0; i<4; i++)
        newState[i] = ntohl(keyMap[i].bigEndianValue);

    for (i=0; i<4; i++) { // for lines of 32 cols in map (4 int32)
        if (kbdState[i]!=newState[i]) { // look for change in this line
            int kbdBit = kbdState[i];
            int newBit = newState[i];

            for (j=0; j<32; j++) {
                if ((kbdBit & ~newBit) &1) { // key has been released
                    virtualKey = 32*i + 8 * (3-(j/8)) + (j%8);
                    if (guiPrefs.keyConv[virtualKey]!=0xFF) {
                        CpcKeyUp(guiPrefs.keyConv[virtualKey]);
                        kbdState[i] ^= kbdState[i] & (1<<j);
                        goto Done;
                    }
                }
                if ((newBit & ~kbdBit) &1) { // key has been pressed
                    virtualKey = 32*i + 8 * (3-(j/8)) + (j%8);
                    if (guiPrefs.keyConv[virtualKey]!=0xFF) {
                        CpcKeyDown(guiPrefs.keyConv[virtualKey]);
                        kbdState[i] ^= newState[i] & (1<<j);
                        goto Done;
                    }
                }
                kbdBit >>= 1;
                newBit >>= 1;
            }
        }
    }
Done:;
}

void OsConsole::CheckKbd()
{
    if (gConfig.kbdRawMode)
        UpdateKeyMap();
    else
        CheckEvents(false);
}

void OsConsole::MonitorEventLoop()
{
    CheckEvents(true);
}

// Handle events and update keyMap
void OsConsole::CheckEvents(bool monitFlag)
{
    if (gConfig.kbdRawMode) UpdateKeyMap();
    if (startupFlag) {
        DoStartupEvents();
        startupFlag=0;
    }

    if (monitFlag)
    {
        if (FrontWindow() != monitWin) {
            SelectWindow(monitWin);
            monit.Refresh();
            MonitUpdate();
        }
    }

    // Do that in Carbon so that the window buffer is up-to-date
    // and the menus' alpha is correct
    //UpdateWindow(screenSmall? SMALL: screenSkip? SKIP: FULL);
    do {
        EventRecord event;
        SetEventMask(everyEvent); // Allow keyUp events
        while (WaitNextEvent(everyEvent,&event,60*(IsPaused()|inBackground|hold),0))
            DoEvent(&event);
        if (displayChanged) { // Catch display manager events
            displayChanged=0;
            SetWindow(); // Do screenSmall
        }
    } while (!IsQuit() && (IsPaused() || hold || IsWindowCollapsed(mainWin)||inBackground || FrontWindow()!=mainWin || Button()));
}

#pragma mark - Preferences

// Load up the config data
void OsConsole::LoadConfig()
{
    // OS specific - defaults (X-platform defaults have been set by ctor)
    for (int i=0; i<128; i++) guiPrefs.keyConv[i] = keyConv0[i];
    guiPrefs.usageTime=0;
    guiPrefs.sWinX=guiPrefs.cWinX=guiPrefs.bWinX=100;
    guiPrefs.sWinY=guiPrefs.cWinY=guiPrefs.bWinY=100;
    guiPrefs.indicators=1;
    guiPrefs.nbConfirms=0;

    string path = GetConfigPath();
    ifstream in(path.c_str());
    if (!in) return;

    std::streampos byteCount = in.tellg();
    in.seekg(0, std::ios::end);
    byteCount = in.tellg() - byteCount;
    in.seekg(0, std::ios::beg);

    long confBytes = sizeof(CpcConfig);
    long prefBytes = sizeof(GuiPrefs);
    if (byteCount != confBytes+prefBytes) {
        ErrReport(ERR_CON_BADPREF);
        return;
    }

    in.read(reinterpret_cast<char *>(&gConfig), confBytes);
    in.read(reinterpret_cast<char *>(&guiPrefs), prefBytes);

    gConfig.Check(); // X-platform check
    guiPrefs.indicators&=1; // OS values check
}

// Save the preference file
Boolean OsConsole::SaveConfig()
{
    string path = GetConfigPath();
    ofstream out(path.c_str());
    if (!out) return false;
    out.write(reinterpret_cast<const char *>(&gConfig), sizeof(CpcConfig));
    unsigned long newTime;
    GetDateTime(&newTime);
    guiPrefs.usageTime += (newTime-startTime);
    startTime=newTime;
    out.write(reinterpret_cast<const char *>(&guiPrefs), sizeof(GuiPrefs));
    return true;
}

#pragma mark - Indicators

void OsConsole::FillText(
                         int top,
                         int left,
                         int color0, // background color
                         int color1, // shadow color
                         int color2, // text color
                         const char *text
                         )const
{
    if (!screenSmall||screenFull)
        left*=2;
    GWorldPtr tempGWorld=0;
    Rect box;
    SetRect(&box,0,0,8*strlen(text)+1,9);
    Rect dstBox;
    SetRect(&dstBox,left,top,left+8*strlen(text)+1,top+9);
    if (NewGWorld(&tempGWorld, 32, &box, 0, nil, 0)!=noErr) return;
    GWorldPtr saveWorld;
    GDHandle saveDevice;
    GetGWorld(&saveWorld, &saveDevice);
    SetGWorld (tempGWorld, nil);
    // fill background
    RGBForeColor(&colors[color0]);
    PaintRect(&box);
    // draw shadow
    DrawAmstradText(1,1,P32()[Black],text);
    // draw text
    DrawAmstradText(0,0,P32()[White],text);
    SetGWorld(saveWorld, saveDevice);
    CopyBits(GetPortBitMapForCopyBits(tempGWorld), GetPortBitMapForCopyBits(saveWorld), &box, &dstBox, srcCopy, (RgnHandle) nil );
    DisposeGWorld(tempGWorld);
}

void OsConsole::FillBlock(int top, int left, int h, int w, int color)const
{
    if (!screenSmall||screenFull) {
        left*=2;
        w*=2;
    }
    Rect rect;
    RGBColor foreColor;

    SetRect(&rect,left,top,left+w,top+h);
    GetForeColor(&foreColor);
    RGBForeColor(&colors[color]);
    PaintRect(&rect);
    RGBForeColor(&foreColor);
}

void OsConsole::SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed)const
{
    static const int MAX_SPEED_COEFF=10;
    static const int SPEED_X=4;
    static const int SPEED_Y=11;
    static const int SPEED_W=60;
    static const int SPEED_H=2;
    static const int SPEED_REPORT_US=100000;
    static const int SPEED_STR_X=70;
    static const int SPEED_STR_Y=6;

    static unsigned long totalExpected=0;
    static unsigned long totalExecuted=0;
    static unsigned long totalElapsed=0;

    if (!indicators) return;
    SetPortWindowPort(mainWin);
    totalExpected += expected;
    totalExecuted += executed;
    totalElapsed += elapsed;
    if (totalElapsed > SPEED_REPORT_US) {
        int l1,l2,i;

        l1 = (SPEED_W*totalExpected)/(totalElapsed*MAX_SPEED_COEFF);
        l2 = (SPEED_W*totalExpected)/(totalExecuted*MAX_SPEED_COEFF);
        if (l1>SPEED_W) l1=SPEED_W;
        if (l2>SPEED_W) l2=SPEED_W;

        // Draw the used speed
        FillBlock(SPEED_Y,SPEED_X,SPEED_H,l1,YELLOW);
        // Draw the wasted speed
        if (l2>l1) FillBlock(SPEED_Y,SPEED_X+l1,SPEED_H,l2-l1,RED);
        // Draw the rest
        FillBlock(SPEED_Y,SPEED_X+l2,SPEED_H,SPEED_W-l2,BLUE);
        // Draw the separator bars
        for (i=1; i<MAX_SPEED_COEFF; i++)
            FillBlock(SPEED_Y,SPEED_X+(SPEED_W/MAX_SPEED_COEFF)*i-1,SPEED_H,1,DARK_BLUE);

        char str[12];
        sprintf(str,"%3d%%",(int)(100*totalExpected/totalElapsed));
        FillText(SPEED_STR_Y,SPEED_STR_X,DARK_GREY,BLACK,LIGHT_GREY,str);
        totalExpected=totalExecuted=totalElapsed=0;
    }
}

void OsConsole::DiskActivity(int which, int state)
{
    static const int DRIVE_X=110;
    static const int DRIVE_X2=DRIVE_X+28;
    static const int DRIVE_Y=11;
    static const int DRIVE_W=30;
    static const int DRIVE_H=2;

    if (!indicators) return;
    SetPortWindowPort(mainWin);
    FillBlock(DRIVE_Y, which?DRIVE_X2:DRIVE_X, DRIVE_H, DRIVE_W, state?RED:BLUE);
}

#pragma mark - Clipboard
void OsConsole::CopyToClipboard()
{
    vector<unsigned long> image;
    RenderToBuffer(image, screenSmall);
    cocoa_image_to_clipboard(image);
}

std::string OsConsole::GetClipboardString()
{
    ScrapRef scrap;
    ASSERT_THROW(GetCurrentScrap(&scrap)==noErr);

    ScrapFlavorFlags scrapFlavorFlags;
    ASSERT_THROW(GetScrapFlavorFlags(scrap,kScrapFlavorTypeText, &scrapFlavorFlags)==noErr);
    Size byteCount;
    ASSERT_THROW(GetScrapFlavorSize(scrap, kScrapFlavorTypeText, &byteCount)==noErr);
    std::vector<char> buff(byteCount);
    GetScrapFlavorData(scrap, kScrapFlavorTypeText, &byteCount, (void *)&buff[0]);
    return std::string(buff.begin(), buff.end());
}

void OsConsole::SavePICTFile()
{
    std::string name;
    if (!GetSavePath(MSG_CON_SAVEPICT, MSG_CON_UNTITLED_PICT, name)) return;
    vector<unsigned long> image;
    RenderToBuffer(image, screenSmall);
    cocoa_write_image(image, name);
}

void OsConsole::CmdEnabled(CpcCmd cmd, bool enabled)
{
    int menuId;
    int menuItem;
    CmdToMenu(cmd, menuId, menuItem);
    MacUtils::MenuItemEnabled(menuId, menuItem, enabled);
}

void OsConsole::CmdChecked(CpcCmd cmd, bool checked)
{
    int menuId;
    int menuItem;
    CmdToMenu(cmd, menuId, menuItem);
    MacUtils::MenuItemChecked(menuId, menuItem, checked);
}

void OsConsole::WindowTitle(const std::string &title)
{
    string CL = string(versionString).substr(7,3);
    string newTitle = "CPC++ [b"+CL+"] - "+title;
    MacUtils::SetWindowTitle(mainWin,newTitle);
}

void OsConsole::LaunchUrl(const std::string &url)
{
    MacUtils::LaunchURL(url);
}

void OsConsole::PlayCpcSound(CpcSound sound)
{
    if (sound==Insert)
        MacUtils::PlaySound(kSndInsert);
    else if (sound==Eject)
        MacUtils::PlaySound(kSndEject);
}


void OsConsole::EmptyRunMenus()
{
    for (int i=0; i<2; i++) {
        MenuHandle menu = GetMenuHandle(kRunMenu+i);
        for (int j=0; j<runMenuItemsCount[i]; j++)
            DeleteMenuItem(menu,1);
        runMenuItemsCount[i]=0;
    }
}

void OsConsole::AddItemToRunMenu(int drive, const std::string &item_, bool enabled)
{
    std::string item;
    if (item_[0]==0)
        item="-";
    else if (item_[0]=='-')
        item = std::string(" ") + item_;
    else
        item = item_;

    MenuHandle menu = GetMenuHandle(kRunMenu+drive);
    Str255 pascalStr;
    CopyCStringToPascal(item.c_str(),pascalStr);
    InsertMenuItem(menu, pascalStr, runMenuItemsCount[drive]);
    if (enabled)
        EnableMenuItem(menu,++runMenuItemsCount[drive]);
    else
        DisableMenuItem(menu,++runMenuItemsCount[drive]);
}

void OsConsole::SaveSnapshot(const std::string &path, const BYTE * buff)
{
    Handle handle = GetResource('icns', 128);
    //gMsg(NONE, "GetResource error: %d", ResError());
    DetachResource(handle);

    // Write the snapshot to the data fork
    ofstream out(path.c_str());
    if (out)
    {
        long byteCount = 256 + ((buff[0x6b]==64)?64:128)*1024; // Compute snapshot size based on RAM size
        out.write(reinterpret_cast<const char*>(buff), byteCount);

        //SetTypeCreator(path, FileSnapshot);

        vector<unsigned long> image;
        RenderToBuffer(image);
        cocoa_set_image_as_icon(image, path);
    }
}

void OsConsole::SetScreenSmall(bool val)
{
    CpcGuiConsole::SetScreenSmall(val);
    screenSmall = val;
    SetWindow();
}

void OsConsole::SetScreenFull(bool val)
{
    CpcGuiConsole::SetScreenFull(val);
    screenFull = val;
    SetWindow();
}

void OsConsole::SetScreenSkip(bool val)
{
    CpcGuiConsole::SetScreenSkip(val);
    screenSkip = val;
    SetWindow();
}

void OsConsole::SetIndicators(bool val)
{
    CpcGuiConsole::SetIndicators(val);
    indicators = val;
    SetWindow();
}

void OsConsole::SetMonochrome(bool val)
{
    CpcGuiConsole::SetMonochrome(val);
    SetWindow();
}

void OsConsole::SetFileType(const std::string &path, FileType type)
{
    //SetTypeCreator(path.c_str(), type);
}

void OsConsole::MonitToggle()
{
    if (monitVisible) {
        monitVisible=false;
        HideWindow(monitWin);
    } else {
        monitVisible=true;
        ShowWindow(monitWin);
    }
    monit.Refresh();
    MonitUpdate();
}

void OsConsole::MonitOff()
{
    if (monitVisible)
        MonitToggle();
}

void OsConsole::MonitUpdate()
{
    if (!monitVisible) return;

    LockPortBits(GetWindowPort(monitWin));
    PixMap **monitPixmap = GetPortPixMap(GetWindowPort(monitWin));
    ASSERT_THROW((*monitPixmap)->pixelSize==32);
    ASSERT_THROW((*monitPixmap)->pixelFormat=='BGRA');
    LockPixels(monitPixmap);
    Rect &bounds = (*monitPixmap)->bounds;
    int dstBPR = (*monitPixmap)->rowBytes & 0x3fff;
    Ptr monitWinPt = GetPixBaseAddr(monitPixmap) - bounds.left * 32/8 - bounds.top * dstBPR;

    CpcDib32 monitDib = monit.Dib();
    CpcRect srcRect = monit.WinRect();

    for (int row=0; row<srcRect.Height(); row++)
    {
        RAW32 *dstPt = (RAW32 *)(monitWinPt + dstBPR * row);
        RAW32 *srcPt = monitDib.pt + monitDib.bpr * row / 4;
        memcpy(dstPt, srcPt, dstBPR);
    }
    UnlockPixels(monitPixmap);
    UnlockPortBits(GetWindowPort(monitWin));

    RgnHandle visibleRgn=NewRgn();
    GetPortVisibleRegion(GetWindowPort(monitWin),visibleRgn);
    QDFlushPortBuffer(GetWindowPort(monitWin),visibleRgn);
    DisposeRgn(visibleRgn);

}

// Hexa value selection dialog
static const int kMonitOK=3;
static const int kMonitCancel=4;
static const int kMonitDialog=134;
static const int kMonitPrompt=1;
static const int kMonitData=2;

static pascal Boolean GetHexValFilter(DialogPtr theDialog,EventRecord *theEvent,short *itemHit)
{
    char theChar;

    switch (theEvent->what) {
        case keyDown:
            theChar = (char)(theEvent->message & charCodeMask);
            if ( ((theEvent->modifiers&cmdKey)&&theChar=='.') ||
                (theChar==27)) {
                *itemHit = kMonitCancel;
                MacUtils::HiliteButton(theDialog,kMonitCancel,1);
                return true;
            }
            if ( theChar==13 || theChar==3 ) {
                *itemHit = kMonitOK;
                MacUtils::HiliteButton(theDialog,kMonitOK,1);
                return true;
            }break;
    }
    return false;
}

bool OsConsole::GetHexValue(const char *prompt, int &val)
{
    DialogPtr dialog;
    int done=-1;
    short itemHit;
    unsigned int value;

    if (!(dialog=GetNewDialog(kMonitDialog,0,(WindowPtr)-1)))
        return 0;
    MacUtils::SetItemText(dialog,kMonitPrompt,prompt);
    MacUtils::OutlineButton(dialog,kMonitOK);
    ShowWindow(GetDialogWindow(dialog));

    gCon=this;
    ModalFilterUPP filter=NewModalFilterUPP(GetHexValFilter);
    while (done==-1) {
        ModalDialog(filter,&itemHit);
        switch(itemHit) {
            case kMonitOK:
            {
                std::string str;
                MacUtils::GetItemText(dialog, kMonitData, str);
                if (sscanf(str.c_str(),"%04X",&value)) {
                    val=value;
                    done=1;
                } else
                    done=0;
            } break;
            case kMonitCancel:
                done=0;
                break;
        }
    }
    DisposeModalFilterUPP(filter);
    DisposeDialog(dialog);
    return done;
}

void OsConsole::DrawAmstradText(CpcDib32 dst, int x, int y, unsigned long color, const char *str)
{
    AmstradText amstradText(CpcDib32((RAW32 *)GetPixBaseAddr(fontPixmap), (*fontPixmap)->rowBytes&0x7FFF));
    amstradText.DrawAmstradText(dst, x, y, color, str);
}

void OsConsole::DrawLongText(const string &myText, unsigned char fore, int &crtRow)
{
    char str[64],*pt;
    pt=str;
    for (unsigned int i=0; i<=myText.size(); i++) {
        if (myText[i]=='\r' || myText[i]=='\n' || (pt-str)>20 || myText[i]==0) {
            *pt=0;
            DrawAmstradText(8+(19-strlen(str))*4, crtRow, P32()[fore], str);
            pt=str;
            crtRow+=8;
        } else
            *pt++ = myText[i];
    }
}

PixMapHandle OsConsole::GetLockCrtPixmap()const
{
    GrafPtr crtPort=GetWindowPort(monitWin);
    GetPort(&crtPort);
    PixMapHandle pixmap = GetPortPixMap(crtPort);
    ASSERT_THROW((*pixmap)->pixelSize==32);
    LockPixels(pixmap);
    return pixmap;
}

void OsConsole::RectFill(const Rect &rect, unsigned long color)const
{
    PixMapHandle pixmap = GetLockCrtPixmap();
    AmstradText::RectFill(CpcDib32((RAW32 *)GetPixBaseAddr(pixmap), (*pixmap)->rowBytes&0x7FFF), CpcRect(rect.top, rect.left, rect.bottom, rect.right), color);
    UnlockPixels(pixmap);
}

void OsConsole::DrawAmstradText(int x, int y, unsigned long color, const char *str)const
{
    std::swap(*(char *)&color, *((char*)(&color)+3));
    std::swap(*((char *)&color+1), *((char*)(&color)+2));
    PixMapHandle pixmap = GetLockCrtPixmap();

    AmstradText amstradText(CpcDib32((RAW32 *)GetPixBaseAddr(fontPixmap), (*fontPixmap)->rowBytes&0x7FFF));
    amstradText.DrawAmstradText(CpcDib32((RAW32 *)GetPixBaseAddr(pixmap), (*pixmap)->rowBytes&0x7FFF), x, y, color, str);

    UnlockPixels(pixmap);
}

// Put up an error message
void OsConsole::PutError(const std::string &str)
{
    Str255 tmpStr;
    CopyCStringToPascal(str.c_str(), tmpStr);

    ParamText(tmpStr,"\p","\p","\p");
    if (screenFull) ReleaseFullScreen();
    SInt16 outItemHit;
    StandardAlert(kAlertStopAlert,tmpStr,0,0,&outItemHit);
    if (screenFull) GetFullScreen();
}

#pragma mark - Sound

void OsConsole::BuffDone()
{
    psgBuffFull[crtPsgBuff] = true; // Current buffer is ready for playback
    int nextBuff = (crtPsgBuff+1)%NBUFFS; // pick the next buffer

    // If the next buff is sill being played (CPC faster than Mac),
    // we start dropping sound frames (cf. Psg.cc)
    if (psgBuffFull[nextBuff])
        cpc.DropOn(true);

    // Select next buffer for the PSG to render
    crtPsgBuff = nextBuff;
    cpc.SndBuff(psgBuff[crtPsgBuff],buffNSamples);
}

// This starts double buffer sound
void OsConsole::StartSound()
{
    requestEODB=0;
    for (int i=0; i<NBUFFS; i++)
        psgBuffFull[i] = false;
    crtPsgBuff = 0;
    crtMacBuff = NBUFFS-1;
    psgBuffFull[NBUFFS-1]=true;
    cpc.DropOn(false);
    cpc.SndBuff(psgBuff[crtPsgBuff],buffNSamples); // Give our buffer to the PSG
    PlaySilence();
}

void OsConsole::PlaySilence()
{
    sndHdr->numFrames = WAIT_SIZE;
    sndHdr->samplePtr = 0; // use sampleArea (i.e. silence)
    SendBuff();
}

void OsConsole::PlayBuffer(int buffIdx)
{
    sndHdr->numFrames = buffNSamples;
    sndHdr->samplePtr = (char *)psgBuff[buffIdx];
    SendBuff();
    crtMacBuff = buffIdx; // goto next buffer
}

void OsConsole::SendBuff()
{
    // Send a buffer of data to be played by the sound channel
    SendSoundCmd(bufferCmd,(long)sndHdr);
    // Ask the sound channel to call back our routine when it's done with playing
    SendSoundCmd(callBackCmd,(long)this);
}

// Send a command to the sound channel
void OsConsole::SendSoundCmd(long cmd, long param)
{
    SndCommand newCmd;
    newCmd.cmd = cmd;
    newCmd.param1 = 0;
    newCmd.param2 = param;
    ASSERT_THROW(SndDoCommand(sndChan, &newCmd, true)==noErr);
}

// This stops asynchronous sound play
void OsConsole::StopSound()
{
    requestEODB=1; // Request end of double buffer play
    cpc.DropOn(true);
    while (requestEODB) {} // Wait for end of double buffer acknowledge
}

void OsConsole::OsVol(int val)
{
    SndCommand sndComm;

    sndComm.cmd=volumeCmd;
    sndComm.param1=0;
    sndComm.param2=((val+1)<<5) | ((val+1)<<21); // 0x01000100 is full volume
    ASSERT_THROW(SndDoImmediate(sndChan,&sndComm)==noErr);
}

void OsConsole::GetHardwareSettings(SndChannelPtr chan, SoundComponentData *hardwareInfo)
{
    ASSERT_THROW(!SndGetInfo(chan, siNumberChannels, &hardwareInfo->numChannels));
    ASSERT_THROW(!SndGetInfo(chan, siSampleRate, &hardwareInfo->sampleRate));
    ASSERT_THROW(!SndGetInfo(chan, siSampleSize, &hardwareInfo->sampleSize));

    if (hardwareInfo->sampleSize == 16)
        hardwareInfo->format = k16BitLittleEndianFormat;
    else
        hardwareInfo->format = k8BitOffsetBinaryFormat;

    LOG_STATUS("HW Sound: Channels=%d Rate=%d Size=%d Format=%s"
                ,hardwareInfo->numChannels,(int)(hardwareInfo->sampleRate/65536.0)
                ,hardwareInfo->sampleSize,(hardwareInfo->format==kOffsetBinary? "Normal": "Based"));
}


pascal void OsConsole::SndCallBackGlue(SndChannelPtr /*sndChan*/, SndCommand *sndCmd)
{
    OsConsole *psg = (OsConsole *)sndCmd->param2;
    psg->SndCallBack();
}

void OsConsole::SndCallBack()
{
    // if we reauested End Of Double Buffer play, just notify here
    if (requestEODB) {
        requestEODB=0;
        return;
    }

    // if we were in drop mode, we can exit that as we are done
    cpc.DropOn(false); // exit drop mode

    // Next buffer from the PSG
    int nextBuff = (crtMacBuff+1)%NBUFFS;

    if (psgBuffFull[nextBuff]) { // is the next buffer from the PSG ready?
        psgBuffFull[crtMacBuff]=false; // Not optimum
        PlayBuffer(nextBuff);
    } else { // No data available to play
        PlaySilence();
    }
}

void OsConsole::SleepUs(UINT32 us)const
{
    usleep(us);
}

unsigned long OsConsole::TicksMs()const
{
    return TickCount()*1000/60;
}

unsigned long OsConsole::GetCrtUs()const
{
    UnsignedWide crtTime;
    Microseconds(&crtTime);
    return crtTime.lo;
}

bool OsConsole::CanStereo()const
{return stereoCapable;}
bool OsConsole::CanSound()const
{return true;}
