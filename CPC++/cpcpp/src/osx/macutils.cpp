// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// MacUtils.cc - abstract layer to include Mac Gui tools
#pragma GCC diagnostic ignored "-Wdeprecated"
#include <MacTypes.h>
#include <Palettes.h>
#include <Sound.h>
#include <Files.h>
#include <string>
#include <stdio.h>
#include <TextUtils.h>
#include <Resources.h>
#include <Gestalt.h>
#include <Folders.h>
#include <LowMem.h> // for LMSetMBarHeight

#include "cpcerror.h"
#include "macutils.h"
//#include "cpcguiconsole.h"

using namespace std;

static void ConvertStringToPascal(const string &str, Str255 &pascalStr)
{
    CopyCStringToPascal(str.c_str(), pascalStr);
}

static void ConvertPascalToString(const Str255 &pascalStr, string &str)
{
    char *temp = new char[pascalStr[0]+1];
    CopyPascalStringToC(pascalStr, temp);
    str = temp;
    delete[] temp;
}
static void ConvertPascalToString(const Str63 &pascalStr, string &str)
{
    char *temp = new char[pascalStr[0]+1];
    CopyPascalStringToC(pascalStr, temp);
    str = temp;
    delete[] temp;
}

MacUtils::MacUtils()
: menuBarHidden(false), menuBarHeight(0), oldGrayRgn(0), mainResFile(0)
{
}

MacUtils::~MacUtils()
{
    InternetConfig_Shutdown();
}

void MacUtils::GetItemRect(
    DialogPtr dialog,
    int itemId,
    Rect *rectP
)
{
    short itemType;
    Handle itemHandle;

    GetDialogItem(dialog,itemId,&itemType,&itemHandle,rectP);
}

void MacUtils::SetItemText(
    DialogPtr dialog,
    int itemId,
    const string &text
)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    Str255 tmpStr;

    ConvertStringToPascal(text, tmpStr);
    GetDialogItem(dialog,itemId,&itemType,&itemHandle,&itemRect);
    SetDialogItemText(itemHandle,tmpStr);
    DrawDialog(dialog); // did not work anymore without it lately 991216
}

void MacUtils::GetItemText(DialogPtr dialog, int itemId, string &text)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    GetDialogItem(dialog, itemId, &itemType, &itemHandle, &itemRect);

    Str255 tmpStr;
    GetDialogItemText(itemHandle,tmpStr);
    ConvertPascalToString(tmpStr, text);
}

void MacUtils::SetItemInt(DialogPtr dialog, int itemId, int val)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    Str255 tmpStr;

    GetDialogItem(dialog,itemId,&itemType,&itemHandle,&itemRect);
    sprintf((char *)tmpStr+1,"%d",val);
    tmpStr[0]=strlen((char *)tmpStr+1);
    SetDialogItemText(itemHandle, tmpStr);
    DrawDialog(dialog); // did not work anymore without it lately 991216
}

void MacUtils::GetItemInt(DialogPtr dialog, int itemId, int *val)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    Str255 tmpStr;

    GetDialogItem(dialog,itemId,&itemType,&itemHandle,&itemRect);
    GetDialogItemText(itemHandle,tmpStr);
    tmpStr[tmpStr[0]+1]=0;
    sscanf((char *)tmpStr+1,"%d",val);
}

void MacUtils::SetItemVal(
    DialogPtr dialog,
    int itemId,
    int val
)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;

    GetDialogItem(dialog,itemId,&itemType,&itemHandle,&itemRect);
    SetControlValue((ControlHandle)itemHandle,val);
}

void MacUtils::GetItemVal(
    DialogPtr dialog,
    int itemId,
    int *val
)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;

    GetDialogItem(dialog,itemId,&itemType,&itemHandle,&itemRect);
    *val = GetControlValue((ControlHandle)itemHandle);
}

void MacUtils::ToggleItemVal(
    DialogPtr dialog,
    int itemId
)
{
    int val;

    GetItemVal(dialog, itemId, &val);
    SetItemVal(dialog, itemId, !val);
}

void MacUtils::OutlineButton(DialogPtr theDialog, short which)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;
    GrafPtr curPort;
    PenState curPen;

    GetDialogItem(theDialog, which, &itemType, &itemHandle, &itemRect);
    GetPort(&curPort);
    SetPort(GetDialogPort(theDialog));
    GetPenState(&curPen);
    InsetRect(&itemRect, -4, -4);
    PenSize(3, 3);
    Pattern myPat;
    StuffHex(&myPat,"\p0202020404040808");
    PenPat(&myPat);
    PenMode(patCopy);
    FrameRoundRect(&itemRect, 16, 16);
    SetPenState(&curPen);
    SetPort(curPort);
}

void MacUtils::HiliteButton(DialogPtr dialog, short item, int state)
{
    short itemType;
    Handle itemHandle;
    Rect itemRect;

    GetDialogItem(dialog, item, &itemType, &itemHandle, &itemRect);
    HiliteControl((ControlHandle)itemHandle, state?0:10);
}

void MacUtils::MenuItemChecked(int menu, int item, bool checked)
{
    CheckMenuItem(GetMenuHandle(menu), item, checked);
}

void MacUtils::MenuItemEnabled(int menu, int item, bool enabled)
{
    if (enabled)
        EnableMenuItem(GetMenuHandle(menu),item);
    else
        DisableMenuItem(GetMenuHandle(menu),item);
}


void MacUtils::SetMenuMark(
    int menuId,
    int firstItem,
    int lastItem,
    int markedItem
)
{
    MenuHandle menu;
    int item;

    menu = GetMenuHandle(menuId);
    for (item=firstItem; item<=lastItem; item++)
        if (item==markedItem)
            SetItemMark(menu,item,checkMark);
        else
            SetItemMark(menu,item,noMark);
}


void MacUtils::SetWindowTitle(WindowPtr win, const string &title)
{
    Str255 pascalStr;
    ConvertStringToPascal(title, pascalStr);
    SetWTitle(win, pascalStr);
}

static bool FSPathMakeFSSpec(const UInt8 *path, FSSpec *spec, Boolean *isDirectory) /* can be NULL */
{
    /* convert the POSIX path to an FSRef */
    FSRef ref;
    if (FSPathMakeRef(path, &ref, isDirectory)!=noErr) return false;

    /* and then convert the FSRef to an FSSpec */
    if (FSGetCatalogInfo(&ref, kFSCatInfoNone, NULL, NULL, spec, NULL)!=noErr) return false;

    return true;
}

// FSSpec can point to non-existing file
// FSRef's cannot point to non-existing files, so we create a FSRef
// to the parent directory and append the filename by hand
void MacUtils::MakeFSSpec(const std::string &path, FSSpec &spec, bool notThere)
{
    if (FSPathMakeFSSpec((const unsigned char *)path.c_str(), &spec, 0)) return;

    int pos = path.rfind('/');
    string fileName = path.substr(pos+1);
    string dirPath = path.substr(0,pos);

    // Get an FSRef to the parent directory
    FSRef ref;
    Boolean isDirectory;
    OSErr err = FSPathMakeRef ((const unsigned char *)dirPath.c_str(), &ref, &isDirectory);
    ASSERT_THROW(isDirectory && err==noErr);

    // Get the FSSpec to the parent directory
    FSSpec dirFSSpec;
    err = FSGetCatalogInfo (&ref, kFSCatInfoNone, 0, 0, &dirFSSpec, 0);
    ASSERT_THROW(err==noErr);

    // Get the FSSpec to the (non-existent) file
    string dirName;
    ConvertPascalToString(dirFSSpec.name, dirName);
    dirName = string(":")+dirName+":";
    dirName += fileName;
    Str255 tmpStr;
    ConvertStringToPascal(fileName, tmpStr);
    ConvertStringToPascal(dirName, tmpStr);
    bzero(&spec, sizeof(spec));
    err = FSMakeFSSpec(dirFSSpec.vRefNum, dirFSSpec.parID, tmpStr, &spec);
    ASSERT_THROW(err==noErr || err==43);

}

OSType MacUtils::FileTypeToOsType(FileType cpcFileType)
{
    //typedef enum {FileDisk, FileSnapshot, FileRom, FileAny, FileZip, FilePrefs} FileType;
    static const OSType kRomFileType='cROM';
    static const OSType kDskFileType='cDSK';
    static const OSType kSnaFileType='cSNA';
    static const OSType kZipFileType='ZIP ';
    static const OSType kPrefFileType='PREF';
    const OSType fileTypes[]={kDskFileType,kSnaFileType,kRomFileType,kRomFileType,kZipFileType,kPrefFileType};
    return fileTypes[cpcFileType];
}

OSType MacUtils::FileCreator()
{
    static const OSType kCreator='CPC+';
    return kCreator;
}

void MacUtils::SetTypeCreator(const std::string &fName, FileType cpcFileType)
{
    OSType type = FileTypeToOsType(cpcFileType);
    OSType creator = FileCreator();
    OSErr err;
    FSSpec fSpec;
    FInfo fInfo;
    MakeFSSpec(fName,fSpec);
    err=FSpGetFInfo(&fSpec, &fInfo);
    if (fInfo.fdCreator != creator || fInfo.fdType!=type) {
        fInfo.fdType = type;
        fInfo.fdCreator = creator;
        err=FSpSetFInfo(&fSpec, &fInfo);
    }
}

bool MacUtils::IsFinderFileType(const std::string &path, FileType cpcFileType)
{
    OSType type = FileTypeToOsType(cpcFileType);
    if (path.empty()) return false;
    FSSpec fSpec;
    MakeFSSpec(path, fSpec);
    FInfo finderInfo;
    FSpGetFInfo(&fSpec, &finderInfo);
    if (finderInfo.fdType==type) return true;
    return false;
}


std::string MacUtils::GetConfigPath()
{
    long prefDirID;
    short systemVolRef;
    if (FindFolder(kOnSystemDisk, kPreferencesFolderType, kCreateFolder, &systemVolRef, &prefDirID) != noErr) return "";

    FSSpec theSpecs;
    static const unsigned char *kPrefFileName="\pCPC++";

    FSMakeFSSpec(systemVolRef, prefDirID, kPrefFileName, &theSpecs);
    std::string result;
    GetFullPath(theSpecs, result);
    return result;
}

void MacUtils::GetFullPath(FSRef &ref, std::string &result)
{
    char temp[1024];
    OSErr err=FSRefMakePath(&ref, (UInt8 *)temp, 1024);
    ASSERT_THROW(err==noErr || err==43);
    result = temp;
}

static OSErr FSMakeFSRef(FSVolumeRefNum volRefNum, SInt32 dirID, ConstStr255Param name, FSRef *ref)
{
    FSRefParam pb;
    pb.ioVRefNum = volRefNum;
    pb.ioDirID = dirID;
    pb.ioNamePtr = (StringPtr)name;
    pb.newRef = ref;
    OSErr result = PBMakeFSRefSync(&pb);
    return result;
}

// FSSpec can point to non-existing file
// FSRef's cannot point to non-existing files, so we create a FSRef
// to the parent directory and append the filename by hand
void MacUtils::GetFullPath(FSSpec &spec, std::string &result)
{
    FSRef ref;
    OSErr err=FSMakeFSRef(spec.vRefNum, spec.parID, 0, &ref);
    ASSERT_THROW(err==noErr || err==43);
    GetFullPath(ref, result);
    string append;
    ConvertPascalToString(spec.name, append);
    result += "/";
    result += append;
}

void MacUtils::PlaySound(int sndId)
{
    Handle sndRes;

    ASSERT_THROW((sndRes=GetResource('snd ',sndId)));
    HLock(sndRes);
    SndPlay(0,(SndListHandle)sndRes,true);
    HUnlock(sndRes);
    ReleaseResource(sndRes);
}

void MacUtils::DrawPictIntoNewGWorld(
    short inPICTid,
    GWorldPtr &outGWorld,
    Rect *dstRect
)
{
    PicHandle thePicture;

    ASSERT_THROW((thePicture = GetPicture(inPICTid)));
    DrawPictIntoNewGWorld(thePicture,outGWorld,dstRect);
    ReleaseResource((Handle)thePicture);
}

void MacUtils::DrawPictIntoNewGWorld(
    PicHandle thePicture,
    GWorldPtr &outGWorld,
    Rect *dstRect
)
{
    Rect picFrame;
    GWorldPtr saveWorld;
    GDHandle saveDevice;

    QDGetPictureBounds(thePicture, &picFrame);
    //picFrame = (**thePicture).picFrame;
    if (dstRect) *dstRect = picFrame;

    ASSERT_THROW(NewGWorld(&outGWorld, 32, &picFrame, 0, nil, 0)==noErr);

    GetGWorld(&saveWorld, &saveDevice);
    SetGWorld (outGWorld, nil);
    LockPixels(GetGWorldPixMap(outGWorld));

    DrawPicture(thePicture, &picFrame);

    UnlockPixels(GetGWorldPixMap(outGWorld));
    SetGWorld(saveWorld, saveDevice);
}

void MacUtils::GetFullScreen()
{
    if (menuBarHidden) return;
    menuBarHidden=true;
    HideMenuBar();
    HideCursor();
}

void MacUtils::ReleaseFullScreen()
{
    if (!menuBarHidden) return;
    ShowMenuBar();
    DrawMenuBar();  // Draw menu bar
    menuBarHidden=false;
    ShowCursor();
}

void MacUtils::SetupResourcePath()
{
    static const unsigned char *kRsrcFileName= "\pCPC++.local";
    int refNum;
    FSSpec fSpec;

    mainResFile = CurResFile();
    if (FSMakeFSSpec(0,0,kRsrcFileName,&fSpec) != noErr)
        return;
    refNum = FSpOpenResFile(&fSpec, fsRdPerm);
    if (ResError() != noErr)
        return;
    UseResFile(refNum);
}

void MacUtils::LaunchURL(const std::string &URL)
{
    InternetConfig_Init();
    ASSERT_THROW(InternetConfig_LaunchURL(URL)== noErr);
}

bool MacUtils::weHaveInternetConfig=false;
ICInstance MacUtils::InternetConfigInstance=0;

void MacUtils::InternetConfig_Init()
{
    OSStatus Err;
    ICDirSpecArray FolderSpec;

    if (weHaveInternetConfig) return;

    Err = ICStart(&InternetConfigInstance, 'CPC+');
    if (Err != noErr) return;

    Err = FindFolder(kOnSystemDisk, kPreferencesFolderType, kDontCreateFolder, &(FolderSpec[0].vRefNum), &(FolderSpec[0].dirID));
    if (Err != noErr) return;

    weHaveInternetConfig = true;
}

void MacUtils::InternetConfig_Shutdown()
{
    if (!weHaveInternetConfig) return;

    ICStop(InternetConfigInstance);

    weHaveInternetConfig = false;
}

OSErr MacUtils::InternetConfig_LaunchURL(const std::string &URL)
{
    OSStatus Err;
    long URLFrom, URLTo, URLLen;
    Str255 Hint;

    if (!weHaveInternetConfig)
    { SysBeep(30); return(icConfigNotFoundErr); }

    URLLen = URL.size();
    URLFrom = 0;
    URLTo = URLLen;
    Hint[0] = 0;
    Err = ICLaunchURL(InternetConfigInstance, Hint, URL.c_str(), URLLen, &URLFrom, &URLTo);

    return Err;
}
