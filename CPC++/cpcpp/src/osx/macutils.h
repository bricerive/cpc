#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file macutils.h - abstract layer to include Mac Gui tools
#include <Dialogs.h>
#include "cpctypes.h"
#include "InternetConfig.h"

/// Provide usefull mac functions
class MacUtils {
 public:
    MacUtils();
    virtual ~MacUtils();

    static void DrawPictIntoNewGWorld(short inPICTid,GWorldPtr &outGWorld,Rect *dstRect);

    static void SetWindowTitle(WindowPtr win, const std::string &title);

    static void MenuItemChecked(int menu, int item, bool checked);
    static void MenuItemEnabled(int menu, int item, bool enabled);

    static void OutlineButton(DialogPtr theDialog, short which);
    static void HiliteButton(DialogPtr dialog, short item, int state);
    static void SetItemText(DialogPtr dialog,int itemId,const std::string &text);
    static void GetItemText(DialogPtr dialog,int itemId,std::string &text);

    static void GetFullPath(FSSpec &spec, std::string &result);
    static void GetFullPath(FSRef &ref, std::string &result);

    static void LaunchURL(const std::string &URL);

    static void PlaySound(int sndId);

    // OS specific file operations
    static void SetTypeCreator(const std::string &fName, FileType cpcFileType);
    static bool IsFinderFileType(const std::string &path, FileType cpcFileType);
    static std::string GetConfigPath();

 protected:
    // GUI tools
    void GetItemRect(DialogPtr dialog,int itemId,Rect *rectP);
    void SetItemInt(DialogPtr dialog,int itemId,int val);
    void GetItemInt(DialogPtr dialog,int itemId,int *val);
    void SetItemVal(DialogPtr dialog,int itemId,int val);
    void GetItemVal(DialogPtr dialog,int itemId,int *val);
    void ToggleItemVal(DialogPtr dialog,int itemId);

    void SetMenuMark(int menuId,int firstItem,int lastItem,int markedItem);

    void GetFullScreen();
    void ReleaseFullScreen();

    void SetupResourcePath();

 private:
    static void DrawPictIntoNewGWorld(PicHandle thePicture,GWorldPtr &outGWorld,Rect *dstRect);
    static void MakeFSSpec(const std::string &path, FSSpec &spec, bool notThere=false);
    static OSType FileTypeToOsType(FileType cpcFileType);
    static OSType FileCreator();

    MacUtils &operator=(const MacUtils&);
    MacUtils(const MacUtils&);

    int CmdToMenu(int cmd) {return cmd>>8;}
    int CmdToItem(int cmd) {return cmd&0xFF;}
    bool menuBarHidden;
    int menuBarHeight;
    RgnHandle oldGrayRgn;
    int mainResFile;

    static void InternetConfig_Init();
    static void InternetConfig_Shutdown();
    static OSErr InternetConfig_LaunchURL(const std::string &URL);
    static bool weHaveInternetConfig;
    static ICInstance InternetConfigInstance;
};
