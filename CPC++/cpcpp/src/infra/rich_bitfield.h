#pragma once
///
/// \file  rich_bitfield.h

//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
#include <string>
#include <boost/preprocessor/repeat.hpp>

#define TERMINATE(...) PRIMITIVE_TERMINATE(__VA_ARGS__)
#define PRIMITIVE_TERMINATE(...)  __VA_ARGS__ ## _END

#define GENERATE_RE_VALUES_LIST(list) TERMINATE(GENERATE_RE_VALUES_LIST_A0 list)
#define GENERATE_RE_VALUES_LIST_A0(e) GENERATE_RE_VALUES_LIST_1(e) GENERATE_RE_VALUES_LIST_B
#define GENERATE_RE_VALUES_LIST_A(e) ,GENERATE_RE_VALUES_LIST_1(e) GENERATE_RE_VALUES_LIST_B
#define GENERATE_RE_VALUES_LIST_B(e) ,GENERATE_RE_VALUES_LIST_1(e) GENERATE_RE_VALUES_LIST_A
#define GENERATE_RE_VALUES_LIST_1(e) e	 
#define GENERATE_RE_VALUES_LIST_A_END
#define GENERATE_RE_VALUES_LIST_B_END

#define GENERATE_RE_STRINGIZER(list) TERMINATE(GENERATE_RE_STRINGIZER_A0 list)
#define GENERATE_RE_STRINGIZER_A0(e) GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_B
#define GENERATE_RE_STRINGIZER_A(e) ,GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_B
#define GENERATE_RE_STRINGIZER_B(e) ,GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_A
#define GENERATE_RE_STRINGIZER_1(e) #e
#define GENERATE_RE_STRINGIZER_A_END
#define GENERATE_RE_STRINGIZER_B_END

#define GENERATE_RE_DESTRINGIZER(list) TERMINATE(GENERATE_RE_DESTRINGIZER_A list)
#define GENERATE_RE_DESTRINGIZER_A(e) GENERATE_RE_DESTRINGIZER_1(e) GENERATE_RE_DESTRINGIZER_B
#define GENERATE_RE_DESTRINGIZER_B(e) GENERATE_RE_DESTRINGIZER_1(e) GENERATE_RE_DESTRINGIZER_A
#define GENERATE_RE_DESTRINGIZER_1(e) if (str==#e) {value=e; return;}	 
#define GENERATE_RE_DESTRINGIZER_A_END
#define GENERATE_RE_DESTRINGIZER_B_END

#define RICH_ENUM_BIT_VALUES(name, values) \
	typedef enum { RE_NOT_SET=0, GENERATE_RE_BIT_VAL(values), RE_MAX_VALUE } Value;

// could not figure out a way to generate the bitmask programmatically yet, so do it by hand for now
#define GENERATE_RE_BIT_VAL(list) TERMINATE(GENERATE_RE_BIT_VAL_0 list)
#define GENERATE_RE_BIT_VAL_0(e) e=1<<0 GENERATE_RE_BIT_VAL_1
#define GENERATE_RE_BIT_VAL_1(e) ,e=1<<1 GENERATE_RE_BIT_VAL_2
#define GENERATE_RE_BIT_VAL_2(e) ,e=1<<2 GENERATE_RE_BIT_VAL_3
#define GENERATE_RE_BIT_VAL_3(e) ,e=1<<3 GENERATE_RE_BIT_VAL_4
#define GENERATE_RE_BIT_VAL_4(e) ,e=1<<4 GENERATE_RE_BIT_VAL_5
#define GENERATE_RE_BIT_VAL_5(e) ,e=1<<5 GENERATE_RE_BIT_VAL_6
#define GENERATE_RE_BIT_VAL_6(e) ,e=1<<6 GENERATE_RE_BIT_VAL_7
#define GENERATE_RE_BIT_VAL_7(e) ,e=1<<7 GENERATE_RE_BIT_VAL_8
#define GENERATE_RE_BIT_VAL_8(e) ,e=1<<8 GENERATE_RE_BIT_VAL_9
#define GENERATE_RE_BIT_VAL_9(e) ,e=1<<9 GENERATE_RE_BIT_VAL_10
#define GENERATE_RE_BIT_VAL_10(e) ,e=1<<10 GENERATE_RE_BIT_VAL_11
#define GENERATE_RE_BIT_VAL_11(e) ,e=1<<11 GENERATE_RE_BIT_VAL_12
#define GENERATE_RE_BIT_VAL_12(e) ,e=1<<12 GENERATE_RE_BIT_VAL_13
#define GENERATE_RE_BIT_VAL_13(e) ,e=1<<13 GENERATE_RE_BIT_VAL_14
#define GENERATE_RE_BIT_VAL_14(e) ,e=1<<14 GENERATE_RE_BIT_VAL_15
#define GENERATE_RE_BIT_VAL_15(e) ,e=1<<15 GENERATE_RE_BIT_VAL_16
#define GENERATE_RE_BIT_VAL_16(e) ,e=1<<16 GENERATE_RE_BIT_VAL_17
#define GENERATE_RE_BIT_VAL_0_END
#define GENERATE_RE_BIT_VAL_1_END
#define GENERATE_RE_BIT_VAL_2_END
#define GENERATE_RE_BIT_VAL_3_END
#define GENERATE_RE_BIT_VAL_4_END
#define GENERATE_RE_BIT_VAL_5_END
#define GENERATE_RE_BIT_VAL_6_END
#define GENERATE_RE_BIT_VAL_7_END
#define GENERATE_RE_BIT_VAL_8_END
#define GENERATE_RE_BIT_VAL_9_END
#define GENERATE_RE_BIT_VAL_10_END
#define GENERATE_RE_BIT_VAL_11_END
#define GENERATE_RE_BIT_VAL_12_END
#define GENERATE_RE_BIT_VAL_13_END
#define GENERATE_RE_BIT_VAL_14_END
#define GENERATE_RE_BIT_VAL_15_END
#define GENERATE_RE_BIT_VAL_16_END
#define GENERATE_RE_BIT_VAL_17_END

#define RICH_BITFIELD(name, values) \
struct name { \
	RICH_ENUM_BIT_VALUES(name, values) \
	name(Value value) : value(value) {} \
	name() : value(RE_NOT_SET) {} \
	explicit name(int v) { value = static_cast<Value>(v); } \
	static std::vector<std::string> AllTextValues() { std::vector<std::string> v = { GENERATE_RE_STRINGIZER(values) }; return v; } \
	static std::vector<Value> AllValues() { std::vector<Value> v = { GENERATE_RE_VALUES_LIST(values) }; return v; } \
	bool operator==(const name &v)const { return v.value == value; } \
	bool operator==(Value v)const { return v == value; } \
	bool operator!=(const name &v)const { return v.value != value; } \
	bool operator!=(Value v)const { return v != value; } \
	operator Value ()const { return value; } \
	bool IsSet(name mask)const { return value & mask; } \
private:\
	Value value;\
}
