///
/// \file  i18n.cpp
/// Support for externalization and internationalization of messages
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//

#include "infra/i18n.h"
#include "infra/error.h"
#include "infra/csv.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <istream>

using namespace infra;
using namespace boost;
using namespace std;


string I18nMgr::GetText(const infra::MsgI18n &msg)
{
    return Instance().GetText_(msg);
}

I18nMgr::Strings I18nMgr::GetAvailableLocales()
{
    return Instance().GetAvailableLocales_();
}

void I18nMgr::SetLocale(string &locale)
{
    Instance().SetLocale_(locale);
}

I18nMgr::I18nMgr()
    : crtLocaleIdx(-1)
{
    std::ifstream in("i18n_msg.csv");
    if (!in) return;

    Strings headers = Csv::ReadRow(in, ',');

    // Group Tag Error#	English	French ...
    static const int firstLocaleColumn=3;
    for (int i=firstLocaleColumn; i<headers.size(); i++)
        locales.push_back(headers[i]);

    while (in.good()) {
        Strings row = Csv::ReadRow(in, ',');
        if (!row[0].empty()) {
            MsgInfo e;
            e.code = row[2];
            for (int i=firstLocaleColumn; i<row.size(); i++)
                e.values.push_back(row[i]);
            msgMap[row[0]][row[1]] = e;
        }
    }
}

I18nMgr &I18nMgr::Instance()
{
    static I18nMgr provider;
    return provider;
}

I18nMgr::Strings I18nMgr::GetAvailableLocales_()
{
    return locales;
}

void I18nMgr::SetLocale_(string &locale)
{
    for (int i=0; i<locales.size(); i++)
        if (locales[i]==locale) {
            crtLocaleIdx=i;
            return;
        }
}

string I18nMgr::GetText_(const infra::MsgI18n &msg)
{
	string str = msg.DefaultText();
	auto groupIt = msgMap.find(msg.GroupName());
	if (groupIt != msgMap.end()) {
		auto errorIt = groupIt->second.find(msg.ToString());
		if (errorIt != groupIt->second.end()) {
			MsgInfo &e(errorIt->second);
			if (crtLocaleIdx >= 0)
				str = e.values[crtLocaleIdx];
			if (!e.code.empty()) {
				if (str.empty())
					str = e.code;
				else
					str = e.code + " - " + str;
			}
		}
	}

	format f(str);
	try {
		for (auto d : msg.details)
			f % d;
	}
	catch (...) {}

	return boost::str(f);
}

std::string infra::MsgI18n::GetText()const { return I18nMgr::GetText(*this); }
