#pragma once
///
/// \file  i18n.h
/// Support for externalization and internationalization of messages
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "infra/error.h"
#include <vector>
#include <map>

#define TERMINATE(...) PRIMITIVE_TERMINATE(__VA_ARGS__)
#define PRIMITIVE_TERMINATE(...)  __VA_ARGS__ ## _END

#define GENERATE_I18N_VALUES(list) TERMINATE(GENERATE_I18N_VALUES_A0 list)
#define GENERATE_I18N_VALUES_A0(e,t) GENERATE_I18N_VALUES_1(e,t) GENERATE_I18N_VALUES_B
#define GENERATE_I18N_VALUES_A(e,t) ,GENERATE_I18N_VALUES_1(e,t) GENERATE_I18N_VALUES_B
#define GENERATE_I18N_VALUES_B(e,t) ,GENERATE_I18N_VALUES_1(e,t) GENERATE_I18N_VALUES_A
#define GENERATE_I18N_VALUES_1(e,t) e	 
#define GENERATE_I18N_VALUES_A_END
#define GENERATE_I18N_VALUES_B_END

#define GENERATE_I18N_STRINGIZER(list) TERMINATE(GENERATE_I18N_STRINGIZER_A0 list)
#define GENERATE_I18N_STRINGIZER_A0(e,t) GENERATE_I18N_STRINGIZER_1(e,t) GENERATE_I18N_STRINGIZER_B
#define GENERATE_I18N_STRINGIZER_A(e,t) ,GENERATE_I18N_STRINGIZER_1(e,t) GENERATE_I18N_STRINGIZER_B
#define GENERATE_I18N_STRINGIZER_B(e,t) ,GENERATE_I18N_STRINGIZER_1(e,t) GENERATE_I18N_STRINGIZER_A
#define GENERATE_I18N_STRINGIZER_1(e,t) #e
#define GENERATE_I18N_STRINGIZER_A_END
#define GENERATE_I18N_STRINGIZER_B_END

#define GENERATE_I18N_DESTRINGIZER(list) TERMINATE(GENERATE_I18N_DESTRINGIZER_A list)
#define GENERATE_I18N_DESTRINGIZER_A(e,t) GENERATE_I18N_DESTRINGIZER_1(e,t) GENERATE_I18N_DESTRINGIZER_B
#define GENERATE_I18N_DESTRINGIZER_B(e,t) GENERATE_I18N_DESTRINGIZER_1(e,t) GENERATE_I18N_DESTRINGIZER_A
#define GENERATE_I18N_DESTRINGIZER_1(e,t) if (str==#e) {value=e; return;}	 
#define GENERATE_I18N_DESTRINGIZER_A_END
#define GENERATE_I18N_DESTRINGIZER_B_END

#define GENERATE_I18N_STRINGS(list) TERMINATE(GENERATE_I18N_STRINGS_A0 list)
#define GENERATE_I18N_STRINGS_A0(e,t) GENERATE_I18N_STRINGS_1(e,t) GENERATE_I18N_STRINGS_B
#define GENERATE_I18N_STRINGS_A(e,t) ,GENERATE_I18N_STRINGS_1(e,t) GENERATE_I18N_STRINGS_B
#define GENERATE_I18N_STRINGS_B(e,t) ,GENERATE_I18N_STRINGS_1(e,t) GENERATE_I18N_STRINGS_A
#define GENERATE_I18N_STRINGS_1(e,t) t
#define GENERATE_I18N_STRINGS_A_END
#define GENERATE_I18N_STRINGS_B_END

namespace infra {

    // Use a base class to allow operator<< below
    struct MsgI18n {
        virtual void SetFromString(const std::string &str)=0;
        virtual const char *ToString()const=0; // enum value as a char *
        virtual const char *GroupName()const=0; // i18n group
        virtual const char *DefaultText()const=0; // the default value
        std::string GetText()const; // the translated value
        std::vector<std::string> details; // the stuff appended to it
    };
    
    class I18nMgr {
    public:
        typedef std::string string;
        typedef std::vector<std::string> Strings;

        static Strings GetAvailableLocales();
        static void SetLocale(string &locale);
        static string GetText(const MsgI18n &msg);

    private:
        static I18nMgr &Instance();

        Strings GetAvailableLocales_();
        void SetLocale_(string &locale);
        string GetText_(const MsgI18n &msg);

        I18nMgr();

        Strings locales;
        int crtLocaleIdx;
        struct MsgInfo {
            string code;
            Strings values;
        };
        typedef std::map<string, MsgInfo> MsgGroup;
        typedef std::map<string, MsgGroup> MsgMap;
        MsgMap msgMap;
    };

    std::istream &operator>>(std::istream &strm, MsgI18n &param);
    std::ostream &operator<<(std::ostream &strm, const MsgI18n &param);

}

#define I18N_GROUP(name, values) \
struct name: public infra::MsgI18n { \
	typedef enum { EC_NOT_SET, GENERATE_I18N_VALUES(values) } Value; \
	name(const std::string &str) {SetFromString(str);} \
	name(Value value): value(value) {} \
	name(): value(EC_NOT_SET) {} \
    virtual const char *GroupName()const {return #name;} \
    virtual void SetFromString(const std::string &str) { GENERATE_I18N_DESTRINGIZER((EC_NOT_SET,"")values) ERR_THROW("unknown message value")(str); } \
    virtual const char *ToString()const {static const char *strs[] ={GENERATE_I18N_STRINGIZER((EC_NOT_SET,"")values)}; return strs[value];} \
    virtual const char *DefaultText()const {static const char *strs[] ={GENERATE_I18N_STRINGS((EC_NOT_SET,"")values)}; return strs[value];} \
	bool operator<(const name &v)const {return value<v.value;} \
	bool operator==(const name &v)const {return v.value==value;} \
	bool operator==(Value v)const {return v==value;} \
	bool operator!=(const name &v)const {return v.value!=value;} \
	bool operator!=(Value v)const {return v!=value;} \
    template<class A> void serialize(A &ar, unsigned int) { ar & BOOST_SERIALIZATION_NVP(value); } \
    name &operator %(const MsgI18n &rhs) { details.push_back(rhs.GetText()); return *this; } \
    name &operator %(const std::string &rhs) { details.push_back(rhs); return *this; } \
    template<typename T> name &operator %(const T &rhs) { details.push_back(boost::lexical_cast<std::string>(rhs)); return *this; } \
    Value value; \
}
