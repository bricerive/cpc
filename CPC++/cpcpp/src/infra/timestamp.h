#pragma once
///
/// \file timestamp.h
/// Generate a timestamp string
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/serialization/nvp.hpp>

namespace infra {

    /// Generate a timestamp for logs, errors
    struct TimeStamp {

		/// return current timestamp
        static std::string Current() { return TimeStamp().Stamp(); }
        static std::string CurrentUtc() { return TimeStamp(boost::posix_time::microsec_clock::universal_time()).Stamp(); }
        static unsigned long long CurrentUtcMs() { return TimeStamp(boost::posix_time::microsec_clock::universal_time()).Ms(); }

		/// Return the current time in ms
		static unsigned long long CurrentMs() { return TimeStamp().Ms(); }


		/// Construct by recording the current time
		TimeStamp();

		/// Construct from a given timestamp string
		TimeStamp(const std::string &stamp);

		/// Construct from ptime 
		TimeStamp(const boost::posix_time::ptime &time);

		/// Construct from a ms time
		TimeStamp(unsigned long long ms);

		/// Returns max timestamp value for comparison
		static TimeStamp Max();

		/// Returns min timestamp value for comparison
		static TimeStamp Min();

		template<class A> void serialize(A &ar, unsigned int) { ar & BOOST_SERIALIZATION_NVP(ms); }

		bool operator<(const TimeStamp &rhs)const { return ms < rhs.ms; }
		bool operator>(const TimeStamp &rhs)const { return ms > rhs.ms; }
		bool operator>=(const TimeStamp &rhs)const { return !(ms < rhs.ms); }
		bool operator<=(const TimeStamp &rhs)const { return !(ms > rhs.ms); }
		bool operator==(const TimeStamp &rhs)const { return ms == rhs.ms; }
		bool operator!=(const TimeStamp &rhs)const { return !(ms == rhs.ms); }
		long long operator-(const TimeStamp &rhs)const { return Ms() - rhs.Ms(); }
        TimeStamp operator-(long long rhs)const { return TimeStamp(ms - rhs); }
        TimeStamp operator+(long long rhs)const { return TimeStamp(ms + rhs); }
        TimeStamp operator+=(long long rhs) { return *this=TimeStamp(ms+rhs); }

		unsigned long long Ms()const;
		const std::string &Stamp()const;
		static const int stampLength = 15;
		std::string Hours()const;
		std::string Minutes()const;
        std::string Seconds()const;
        std::string AsDate()const;
        std::string AsTime()const;
		std::string AsFileNameDateTime()const;

	private:
        unsigned long long ms;
        mutable std::string st;

		typedef boost::posix_time::ptime Time;

		/// Basic formatting code
		static std::string Format(const Time &now);

		unsigned short ToChunk(int s,int n)const;
		void MsFromStamp();
		void StampFromMs()const;

		unsigned short Year()const { return ToChunk(0,2); }
		unsigned short Month()const { return ToChunk(2,2); }
		unsigned short Day()const { return ToChunk(4,2); }
		unsigned short Hour()const { return ToChunk(6,2); }
		unsigned short Minute()const { return ToChunk(8,2); }
		unsigned short Second()const { return ToChunk(10,2); }
		unsigned short Millisecond()const { return ToChunk(12,3); }

	};

    std::ostream &operator<<(std::ostream &s, const infra::TimeStamp &v);
    std::istream &operator>>(std::istream &s, infra::TimeStamp &v);

}