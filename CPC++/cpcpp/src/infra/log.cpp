//
// log.cpp
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//

#include "log.h"
#include "timestamp.h"
#include <boost/lexical_cast.hpp>
#include <boost/thread/locks.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include <string>
#include <stdio.h>

#include <fcntl.h>
#include <iostream>
#include <stdarg.h>

using namespace std;
using namespace infra;
using boost::format;

Log::Log()
    : output(&multiStreamBuf)
    , level(status)
	, logMsgHandler(DefaultHandler)
    , sizeBytes(0)
	, logI18MsgHandler([](Level logLevel, const MsgI18n &i18nMsg) {return i18nMsg.GetText(); })
{
}

Log::~Log()
{
}

Log &Log::TheLog()
{
    static Log theLog;
    return theLog;
}

bool Log::Msg(Level logLevel, CodeLoc codeLoc, const MsgI18n &i18nMsg)
{
	return Msg(logLevel, codeLoc, (TheLog().logI18MsgHandler)(logLevel, i18nMsg));
}

bool Log::Msg(Level logLevel, CodeLoc codeLoc, const boost::format &msg)
{
    return Msg(logLevel, codeLoc, boost::str(msg));
}

bool Log::Msg(Level logLevel, CodeLoc codeLoc, const char *format, ...)
{
    char str[1024];
    va_list argP;
    va_start(argP, format);
    vsnprintf(str, 1024, format, argP);
    va_end(argP);
    return Msg(logLevel, codeLoc, string(str));
}

bool Log::Msg(Level logLevel, CodeLoc codeLoc, const std::string &msg)
{
    Log &log(TheLog());
    if (logLevel<log.level)
        return true;

    string hdr=BuildHeader(logLevel, codeLoc);

    // Serialize output in multithreaded
    lock_guard<recursive_mutex> lock(log.mutex);

    // skip excluded sources
    if (logLevel== Level::status && log.IsDisabledSource(codeLoc.SourceFile())) return true;

    log.logMsgHandler(logLevel, hdr, msg);
    
    return true; // always returns true so we can use it as a rvalue
}

bool Log::IsDisabledSource(const string &source)
{
    if (find(disabledSources.begin(), disabledSources.end(), source)!=disabledSources.end())
        return true;
    return false;
}

void Log::DefaultHandler(Level /*logLevel*/, const std::string &hdr, const std::string &msg)
{
    Log &log(TheLog());

    // add the end of line for stream output
	log.output << hdr << msg << std::endl << std::flush;

    // update log file size and notify listeneres
    log.sizeBytes += (hdr.size() + msg.size() + 1);
    for (auto listener : log.sizeListeners)
        listener->SizeUpdate(log.sizeBytes / 1024);
}

string Log::BuildHeader(Level type, const CodeLoc &codeLoc)
{
    return BuildHeader(type, codeLoc, TimeStamp::Current());
}

string Log::BuildHeader(Level type, const CodeLoc &codeLoc, const TimeStamp &ts)
{
    static const char *types[] = { "STA","WAR","ERR" };
    return str(format("%s|%s|%s| ") % ts % codeLoc.Formatted() % types[type]);
}

void Log::AddStreamBuf(streambuf *sb)
{
	lock_guard<recursive_mutex>lock(TheLog().mutex);
	TheLog().multiStreamBuf.AddStreamBuf(sb);
}

void Log::RemoveStreamBuf(streambuf *sb)
{
	lock_guard<recursive_mutex>lock(TheLog().mutex);
	TheLog().multiStreamBuf.RemoveStreamBuf(sb);
}

void Log::AddSizeListener(SizeListener *sizeListener)
{
    lock_guard<recursive_mutex>lock(TheLog().mutex);
	TheLog().sizeListeners.push_back(sizeListener);
}

void Log::RemoveSizeListener(SizeListener *sizeListener)
{
    lock_guard<recursive_mutex>lock(TheLog().mutex);
	Log &theLog(TheLog());
	std::vector<SizeListener *>::iterator i = std::find(theLog.sizeListeners.begin(), theLog.sizeListeners.end(), sizeListener);
	ASSERT_THROW(i != theLog.sizeListeners.end())("Attempt to remove unknown sizeListener");
	theLog.sizeListeners.erase(i);
}

void Log::SetDisabledSources(const std::vector<string> &sources)
{
    lock_guard<recursive_mutex>lock(TheLog().mutex);
    TheLog().disabledSources = sources;
}

Log::LogMsgHandler Log::SetLogMsgHandler(LogMsgHandler newHandler)
{
	Log &log(TheLog());
	LogMsgHandler previous = log.logMsgHandler;
	log.logMsgHandler = newHandler;
	return previous;
}

Log::LogI18MsgHandler Log::SetI18LogMsgHandler(LogI18MsgHandler newHandler)
{
	Log &log(TheLog());
	LogI18MsgHandler previous = log.logI18MsgHandler;
	log.logI18MsgHandler = newHandler;
	return previous;
}
namespace infra {
    istream &operator>>(istream &strm, Log::Level &val)
    {
        strm>>(int&)val;
        return strm;
    }

    ostream &operator<<(ostream &strm, Log::Level &val)
    {
        strm<<(int)val;
        return strm;
    }
}
