#pragma once
///
/// \file  struct_with_features.h
/// Handy preprocessor-generated simple struct.

//  Copyright (c) 2012-2016 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)

#include <boost/preprocessor/seq/for_each_product.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <string>
#include <iomanip>
#include <sstream>

// clang-format off

/// Use preprocessor to generate the definition of a struct with the specified features
/// This is for all the times you need a simple struct that is a container with some members.
/// The macro is capable of adding on-demand features to the struct.
/// See below for the definition of the features provided here.
///
/// example:
/// STRUCT_WITH_FEATURES(Employee, (SWF_FIELDS)(SWF_CTOR), (std::string, name) (int, age) (int, salary));
#define STRUCT_WITH_FEATURES(name, features, fields) \
struct name { \
	BOOST_PP_SEQ_FOR_EACH_PRODUCT(TRIGGER_FEATURE_GENERATOR, (features) ((name)) ((fields))) \
}

// Trigger one of the generators for the requested feature
// this gets called with product set to: (feature)((name))((fields))
#define TRIGGER_FEATURE_GENERATOR(r, product) BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_SEQ_ELEM(1, product),BOOST_PP_SEQ_ELEM(2, product))

#define TERMINATE(...) PRIMITIVE_TERMINATE(__VA_ARGS__)
#define PRIMITIVE_TERMINATE(...)  __VA_ARGS__ ## _END

// Feature: the fields definition
#define SWF_FIELDS(name, fields) TERMINATE(GENERATE_SWF_FIELDS_A fields)
#define GENERATE_SWF_FIELDS_A(t,n) GENERATE_SWF_FIELDS_1(t,n) GENERATE_SWF_FIELDS_B
#define GENERATE_SWF_FIELDS_B(t,n) GENERATE_SWF_FIELDS_1(t,n) GENERATE_SWF_FIELDS_A
#define GENERATE_SWF_FIELDS_1(t,n) t n;	 
#define GENERATE_SWF_FIELDS_A_END
#define GENERATE_SWF_FIELDS_B_END

// Feature: a default ctor
#define SWF_CTOR(name, fields) name(): GENERATE_SWF_CTOR(name, fields) {} 
#define GENERATE_SWF_CTOR(name, fields) TERMINATE(GENERATE_SWF_CTOR_A0 fields)
#define GENERATE_SWF_CTOR_A0(t,n) GENERATE_SWF_CTOR_1(t,n) GENERATE_SWF_CTOR_B
#define GENERATE_SWF_CTOR_A(t,n) ,GENERATE_SWF_CTOR_1(t,n) GENERATE_SWF_CTOR_B
#define GENERATE_SWF_CTOR_B(t,n) ,GENERATE_SWF_CTOR_1(t,n) GENERATE_SWF_CTOR_A
#define GENERATE_SWF_CTOR_1(t,n) n(t())	 
#define GENERATE_SWF_CTOR_A_END
#define GENERATE_SWF_CTOR_B_END

// Feature: a ctor with parameters
#define SWF_CTOR_PARAMS(name, fields) name(GENERATE_SWF_CTOR_PARAMS(name, fields)): GENERATE_SWF_CTOR_INIT(name, fields) {} 
#define GENERATE_SWF_CTOR_PARAMS(name, fields) TERMINATE(GENERATE_SWF_CTOR_PARAMS_A0 fields)
#define GENERATE_SWF_CTOR_PARAMS_A0(t,n) GENERATE_SWF_CTOR_PARAMS_1(t,n) GENERATE_SWF_CTOR_PARAMS_B
#define GENERATE_SWF_CTOR_PARAMS_A(t,n) ,GENERATE_SWF_CTOR_PARAMS_1(t,n) GENERATE_SWF_CTOR_PARAMS_B
#define GENERATE_SWF_CTOR_PARAMS_B(t,n) ,GENERATE_SWF_CTOR_PARAMS_1(t,n) GENERATE_SWF_CTOR_PARAMS_A
#define GENERATE_SWF_CTOR_PARAMS_1(t,n) t n	 
#define GENERATE_SWF_CTOR_PARAMS_A_END
#define GENERATE_SWF_CTOR_PARAMS_B_END
#define GENERATE_SWF_CTOR_INIT(name, fields) TERMINATE(GENERATE_SWF_CTOR_INIT_A0 fields)
#define GENERATE_SWF_CTOR_INIT_A0(t,n) GENERATE_SWF_CTOR_INIT_1(t,n) GENERATE_SWF_CTOR_INIT_B
#define GENERATE_SWF_CTOR_INIT_A(t,n) ,GENERATE_SWF_CTOR_INIT_1(t,n) GENERATE_SWF_CTOR_INIT_B
#define GENERATE_SWF_CTOR_INIT_B(t,n) ,GENERATE_SWF_CTOR_INIT_1(t,n) GENERATE_SWF_CTOR_INIT_A
#define GENERATE_SWF_CTOR_INIT_1(t,n) n(n)	 
#define GENERATE_SWF_CTOR_INIT_A_END
#define GENERATE_SWF_CTOR_INIT_B_END

// Feature: an equal operator
#define SWF_OP_EQUAL(name, fields) bool operator==(const name &rhs)const {return TERMINATE(GENERATE_SWF_OP_EQUAL_A0 fields);}
#define GENERATE_SWF_OP_EQUAL_A0(t,n) GENERATE_SWF_OP_EQUAL_1(t,n) GENERATE_SWF_OP_EQUAL_B
#define GENERATE_SWF_OP_EQUAL_A(t,n) && GENERATE_SWF_OP_EQUAL_1(t,n) GENERATE_SWF_OP_EQUAL_B
#define GENERATE_SWF_OP_EQUAL_B(t,n) && GENERATE_SWF_OP_EQUAL_1(t,n) GENERATE_SWF_OP_EQUAL_A
#define GENERATE_SWF_OP_EQUAL_1(t,n) n==rhs.n
#define GENERATE_SWF_OP_EQUAL_A_END
#define GENERATE_SWF_OP_EQUAL_B_END

// Feature: a not equal operator (requires SWF_OP_EQUAL)
#define SWF_OP_NOT_EQUAL(name, fields) bool operator!=(const name &rhs)const {return !(*this == rhs);}

// Feature: support boost::serialization
#define SWF_SERIALIZATION(name, fields) template<class A> void serialize(A &ar, unsigned int) { ar GENERATE_SWF_SERIALIZATION(fields); }
#define GENERATE_SWF_SERIALIZATION(fields) TERMINATE(GENERATE_SWF_SERIALIZATION_A fields)
#define GENERATE_SWF_SERIALIZATION_A(t,n) GENERATE_SWF_SERIALIZATION_1(t,n) GENERATE_SWF_SERIALIZATION_B
#define GENERATE_SWF_SERIALIZATION_B(t,n) GENERATE_SWF_SERIALIZATION_1(t,n) GENERATE_SWF_SERIALIZATION_A
#define GENERATE_SWF_SERIALIZATION_1(t,n) & BOOST_SERIALIZATION_NVP(n)
#define GENERATE_SWF_SERIALIZATION_A_END
#define GENERATE_SWF_SERIALIZATION_B_END

#define SWF_SERIALIZATION_VERSIONED(name, fields) \
    template<typename Archive> void DeserializeBack(Archive &ar, int version, name &o); \
    template<class A> void serialize(A &ar, unsigned int v) { if (v == BOOST_PP_CAT(name, Version)) ar GENERATE_SWF_SERIALIZATION2(fields); else DeserializeBack(ar, v, *this); }
#define GENERATE_SWF_SERIALIZATION2(fields) TERMINATE(GENERATE_SWF_SERIALIZATION2_A fields)
#define GENERATE_SWF_SERIALIZATION2_A(t,n) GENERATE_SWF_SERIALIZATION2_1(t,n) GENERATE_SWF_SERIALIZATION2_B
#define GENERATE_SWF_SERIALIZATION2_B(t,n) GENERATE_SWF_SERIALIZATION2_1(t,n) GENERATE_SWF_SERIALIZATION2_A
#define GENERATE_SWF_SERIALIZATION2_1(t,n) & BOOST_SERIALIZATION_NVP(n)
#define GENERATE_SWF_SERIALIZATION2_A_END
#define GENERATE_SWF_SERIALIZATION2_B_END

#define SWF_SERIALIZATION_VERSION_INFO(name,v) struct name; static const int  BOOST_PP_CAT(name, Version) = 1; BOOST_CLASS_VERSION(name,  BOOST_PP_CAT(name, Version));
#define NVP2(x) boost::serialization::make_nvp(BOOST_PP_STRINGIZE(x), o.x)

// Feature: apply a functor to each field
#define SWF_FOREACH(name, fields) template<typename F> void ForEachField(F &f) {GENERATE_SWF_FOREACH(fields);} template<typename F> void ForEachField(F &f)const {GENERATE_SWF_FOREACH(fields);}
#define GENERATE_SWF_FOREACH(list) TERMINATE(GENERATE_SWF_FOREACH_A list)
#define GENERATE_SWF_FOREACH_A(t,n) GENERATE_SWF_FOREACH_1(t,n) GENERATE_SWF_FOREACH_B
#define GENERATE_SWF_FOREACH_B(t,n) GENERATE_SWF_FOREACH_1(t,n) GENERATE_SWF_FOREACH_A
#define GENERATE_SWF_FOREACH_1(t,n) f(#n, n);
#define GENERATE_SWF_FOREACH_A_END
#define GENERATE_SWF_FOREACH_B_END

/// Declare insertion operator
#define SWF_INSERTION_OPERATOR_DECLARE(name) std::ostream &operator<<(std::ostream &strm, const name &val)
/// Declare extraction operator
#define SWF_EXTRACTION_OPERATOR_DECLARE(name) std::istream &operator>>(std::istream &strm, name &val)
/// Define insertion operator
#define SWF_INSERTION_OPERATOR_DEFINE(name) std::ostream &operator<<(std::ostream &strm, const name &val) { strm << val.ToString(); return strm; }
/// Define extraction operator
#define SWF_EXTRACTION_OPERATOR_DEFINE(name) std::istream &operator>>(std::istream &strm, name &val) { std::string str; strm >> str; val.FromString(str); return strm; }

/// Declare both iostream operators (Use this in a header)
#define SWF_IOSTREAM_OPERATORS_DECLARE(name)  SWF_INSERTION_OPERATOR_DECLARE(name); SWF_EXTRACTION_OPERATOR_DECLARE(name)

/// Define both iostream operators (Use this in a cpp)
 #define SWF_IOSTREAM_OPERATORS_DEFINE(name) SWF_INSERTION_OPERATOR_DEFINE(name) SWF_EXTRACTION_OPERATOR_DEFINE(name)
// clang-format on
