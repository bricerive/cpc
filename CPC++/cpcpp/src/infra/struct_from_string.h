#pragma once
///
/// \file  struct_with_features.h
/// Handy preprocessor-generated simple struct.

//  Copyright (c) 2012-2016 Brice Riv�.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)

#include "infra/struct_with_features.h"
#include "infra/from_string.h"
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/preprocessor/seq/for_each_product.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <string>
#include <iomanip>
#include <sstream>

// Feature: write the field values to a string
#define SWF_TO_STRING(name, fields) std::string ToString()const { std::ostringstream oss; oss << '{' TERMINATE(GENERATE_SWF_TO_STRING_A0 fields) << '}'; return oss.str();}
#define GENERATE_SWF_TO_STRING_A0(t,n) GENERATE_SWF_TO_STRING_1(t,n) GENERATE_SWF_TO_STRING_B
#define GENERATE_SWF_TO_STRING_A(t,n) << ',' GENERATE_SWF_TO_STRING_1(t,n) GENERATE_SWF_TO_STRING_B
#define GENERATE_SWF_TO_STRING_B(t,n) << ',' GENERATE_SWF_TO_STRING_1(t,n) GENERATE_SWF_TO_STRING_A
#define GENERATE_SWF_TO_STRING_1(t,n) << n
#define GENERATE_SWF_TO_STRING_A_END
#define GENERATE_SWF_TO_STRING_B_END

// Feature: set the fields vazlues from a string
#define SWF_FROM_STRING(name, fields) void FromString(const std::string &s) { std::istringstream iss(s.substr(1,s.size()-2)); std::string r; TERMINATE(GENERATE_SWF_FROM_STRING_A fields) }
#define GENERATE_SWF_FROM_STRING_A(t,n) GENERATE_SWF_FROM_STRING_1(t,n) GENERATE_SWF_FROM_STRING_B
#define GENERATE_SWF_FROM_STRING_B(t,n) GENERATE_SWF_FROM_STRING_1(t,n) GENERATE_SWF_FROM_STRING_A
#define GENERATE_SWF_FROM_STRING_1(t,n) std::getline(iss, r, ','); n = infra::from_string(r);	 
#define GENERATE_SWF_FROM_STRING_A_END
#define GENERATE_SWF_FROM_STRING_B_END
