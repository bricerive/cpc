#pragma once
///
/// \file  csv.h
///

//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include <vector>
#include <string>


namespace infra {
    
    class Csv {
    public:
        typedef std::string string;
        typedef std::vector<std::string> Strings;

        static Strings ReadRow(std::istream &in, char delimiter);

    };
    
}
