#pragma once
///
/// \file  from_string.h
/// Convert a string into anything.
/// Similar to boost::lexical_cast except that you don't have to specify the destination type. e.g.
/// \code 
///   double d = from_string("132.231");
///   vector<int> v = from_string("1 2 3 4");
/// \endcode
/// vs.
/// \code double d = lexical_cast<double>("132.231");\endcode
///
/// The destination type is defined by the variable that receives this function's return value.\n
/// This is achieved by using a couple proxy classes.\n
/// The first proxy uses its operator T() to extract the type information.\n
/// The second proxy lets us have partial specializations (for complex types). \n
/// The actual conversion is done by operator>> into a stringstream.\n
/// This means it works for any type that supports it.\n
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>

namespace infra{
    namespace from_string_impl {


        template <typename T, bool IsIntegral = false>
        struct Parser
        {
            static T Parse(const std::string &str) {
                std::stringstream streamIn(str);
                T ReturnValue = T();
                streamIn >> std::setprecision(20) >> ReturnValue;
                ASSERT_THROW(streamIn)("Deserialization error");
                return ReturnValue;
            }
        };

        template <typename T>
        struct Parser<T, true>
        {
            static T Parse(const std::string &str) {
                /// Specialization for int - accept 0x type base prefixing
                // Do the prefix/base selection by hand to:
                // -avoid unwanted octal conversion
                // -support non-standard binary conversion
                int base = 10;
                if (str.size() > 2 && str[0] == '0') {
                    if (str[1] == 'x' || str[1] == 'X') base = 16;
                    if (str[1] == 'b' || str[1] == 'B') base = 2;
                }
                if (base == 2) {
                    T v = 0;
                    for (int i = 2; i < str.size(); i++) {
                        ASSERT_THROW(str[i] == '1' || str[i] == '0');
                        v = (v << 1) + (str[i] - '0');
                    }
                    return v;
                }
                return std::stoi(str, 0, base);
            }
        };

        template<typename T> struct IsInteger { static const bool value = false; };
        template<> struct IsInteger<short> { static const bool value = true; };
        template<> struct IsInteger<int> { static const bool value = true; };
        template<> struct IsInteger<long> { static const bool value = true; };
        template<> struct IsInteger<unsigned short> { static const bool value = true; };
        template<> struct IsInteger<unsigned int> { static const bool value = true; };
        template<> struct IsInteger<unsigned long> { static const bool value = true; };

        /// Second proxy to enable partial specialization
        template<typename T>
        struct proxy2
        {
            static T Convert(const std::string &str)
            {
                return Parser<T, IsInteger<T>::value >::Parse(str);
            }
        };

        /// Partial specialization for vectors
        template<typename T>
        struct proxy2<std::vector<T> >
        {
            static std::vector<T> Convert(const std::string &str)
            {
                std::istringstream strm(str);
                std::vector<T> res;
                do
                {
                    std::string str;
                    strm >> str;
                    if (strm)
                        res.push_back(Parser<T, IsInteger<T>::value >::Parse(str));
                } while (strm);
                return res;
            }
        };

        /// Specialization for bool - accept "true" and "false"
        template<>
        struct proxy2<bool>
        {
            static bool Convert(const std::string &str)
            {
                if (str == "true") return true;
                if (str == "false") return false;
                return (proxy2<int>::Convert(str) ? true : false); // fallback on int conversion
            }
        };

        /// first proxy to extract the destination as a template parameter
        /// (using the conversion operator)
        struct proxy
        {
            proxy(std::string const &str) :str(str) { }

            template<typename T> operator T()
            {
                return proxy2<T>::Convert(str);
            }
        private:
            std::string str;
        };
    }

    /// The function to use.
    static inline from_string_impl::proxy from_string(const std::string &str)
    {
        return from_string_impl::proxy(str);
    }
}
