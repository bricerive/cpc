//
// codeloc.cpp
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "codeloc.h"
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace boost;
namespace fs = boost::filesystem;
using namespace infra;

CodeLoc::CodeLoc(const char *filePath, int lineNumber, const char *functionName)
    : filePath(filePath), lineNumber(lineNumber), functionName(functionName)
{}

/// returns a fixed-size version of the given string by clipping or padding with spaces
static string FixedSize(const string &str, unsigned int len)
{
    string clipped(str);
    if (clipped.size()>=len)
    	return clipped.substr(clipped.size()-len, len);
    while (clipped.size()<len)
    	clipped += ' ';
    return clipped;
}

string CodeLoc::Formatted()const
{
    // ignore the path, just take the filename
    static const int kFileNameLength=16;
    string fileName = FixedSize(fs::path(filePath).filename().string(), kFileNameLength);
    
    static const int kFunctionNameLength=20;
    string functionNameFixed = FixedSize(functionName, kFunctionNameLength);
    
    //create message header
    return str(format("%s|%04d|%s") % fileName % lineNumber % functionNameFixed);
}

string CodeLoc::SourceFile()const
{
    return fs::path(filePath).filename().string();
}

string CodeLoc::FunctionLocation()const
{
    return str(format("%s|%d|%s") %fs::path(filePath).filename().string() % functionName %lineNumber);
}
