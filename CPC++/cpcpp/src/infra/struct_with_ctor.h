#pragma once
///
/// \file  struct_with_ctor.h
/// Handy preprocessor-generated simple struct.
/// This is for all the times you need a simple struct that is a container with some members,
/// a ctor that lets you set the members and that supports serialization

//  Copyright (c) 2012-2016 Brice Riv�.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
#include "infra/struct_with_features.h"

/// Use preprocessor to generate the definition of a struct with the following features:
/// -ctor with full member initialization
/// -empty ctor
/// -serialization (for boost::serialize)
/// Example:
/// STRUCT_WITH_CTOR(Employee, (std::string, name) (int, age) (int, salary));
#define STRUCT_WITH_CTOR(name, fields) STRUCT_WITH_FEATURES(name, (SWF_FIELDS)(SWF_CTOR)(SWF_CTOR_PARAMS), fields)

/// Use preprocessor to generate the definition of a struct with the following features:
/// -ctor with full member initialization
/// -empty ctor
/// -serialization (for boost::serialize)
/// Example:
/// STRUCT_WITH_CTOR(Employee, (std::string, name) (int, age) (int, salary));
#define STRUCT_WITH_CTOR_EQUALOP(name, fields) STRUCT_WITH_FEATURES(name, (SWF_FIELDS)(SWF_CTOR)(SWF_CTOR_PARAMS)(SWF_SERIALIZATION)(SWF_OP_EQUAL)(SWF_OP_NOT_EQUAL), fields)
