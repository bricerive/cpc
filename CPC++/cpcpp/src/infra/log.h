#pragma once
///
/// \file log.h
/// Provide a simple logging mechanism
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//

#include "codeloc.h"
#include "multistreambuf.h"
#include "infra/i18n.h"
#include <mutex>
#include <boost/format.hpp>
#include <string>
#include <fstream>

// Use one these macros for ouputting log messages
// simplest usage:
//      LOG_STATUS(str);
//
// use boost::format to do some message formatting (see http://www.boost.org/doc/libs/1_50_0/libs/format/doc/format.html ) 
//      LOG_STATUS(format("Completion for step %s:  percent=%f\n") % "toto" % 40.23);
//
#define LOG_STATUS(...) infra::Log::Msg(infra::Log::status, _WHERE, __VA_ARGS__)
#define LOG_WARNING(...) infra::Log::Msg(infra::Log::warning, _WHERE, __VA_ARGS__)
#define LOG_ERROR(...) infra::Log::Msg(infra::Log::error, _WHERE, __VA_ARGS__)

namespace infra {

    /// Provide a log mechanism
    class Log {
        
    public:
        typedef std::string string;
        
        /// log levels
        typedef enum { status, warning, error } Level;
        
        /// log a message
        /// always returns true so we can use it as a rvalue
        static bool Msg(Level logLevel, CodeLoc codeLoc, const string &msg);

		/// log a message passed as a boost::format
		/// always returns true so we can use it as a rvalue
		static bool Msg(Level logLevel, CodeLoc codeLoc, const boost::format &msg);

		/// log a message passed as an i18n object
		/// always returns true so we can use it as a rvalue
		static bool Msg(Level logLevel, CodeLoc codeLoc, const MsgI18n &i18nMsg);

        /// log a message
        /// always returns true so we can use it as a rvalue
        static bool Msg(Level logLevel, CodeLoc codeLoc, const char *format, ...);

        /// Set a i18n messages handler
		/// returns the previous handler. It can be ignored or called.
		typedef std::function<string(Level, const MsgI18n &)> LogI18MsgHandler;
		static LogI18MsgHandler SetI18LogMsgHandler(LogI18MsgHandler newHandler);

        /// Set/Get the current log level (to manage basic filtering)
        static void SetLevel(Level level) {TheLog().level=level;}
        static Level GetLevel() {return TheLog().level;}
        
        /// Add/Remove streams to current log output
        static void AddStreamBuf(std::streambuf *sb);
        static void RemoveStreamBuf(std::streambuf *sb);

		struct SizeListener {
			virtual void SizeUpdate(size_t nKBytes)=0;
		};
		/// Add/Remove sizeListener to current log output
		/// A sizeListener has ae callback hook for size limiting
		static void AddSizeListener(SizeListener *sizeListener);
		static void RemoveSizeListener(SizeListener *sizeListener);

        /// Indicate list of source files for which we want to disable log
        static void SetDisabledSources(const std::vector<string> &sources);
        
        /// Set a log message handler.
        /// returns the previous handler. It can be ignored or called.
		typedef std::function<void(Level, const string &, const string &)> LogMsgHandler;
        static LogMsgHandler SetLogMsgHandler(LogMsgHandler newHandler);

        /// Call this to turn on stdout/stderr capture.
        /// This is usefull when calling some library that outputs stuff directly
        static void CaptureStd(bool on);

        static string BuildHeader(Level type, const CodeLoc &codeLoc);
        static string BuildHeader(Level type, const CodeLoc &codeLoc, const TimeStamp &ts);

    private:
        
        /// Singleton instance
        /// For thread-safety, make sure this method is called at least once
        /// before threading starts.
        static Log &TheLog();

        Log(); // private because it should only be called by the static above
        ~Log();
        
		/// The default handler simply outputs the formatted message on a list of ostreams
		static void DefaultHandler(Level logLevel, const string &hdr, const string &msg);

        Multistreambuf multiStreamBuf;
        std::ostream output;
		Level level; // only messages >= this will be logged
		LogMsgHandler logMsgHandler;

		std::vector<SizeListener *> sizeListeners;
		long long sizeBytes;

		LogI18MsgHandler logI18MsgHandler;

		std::recursive_mutex mutex;

        std::vector<string> disabledSources;
        bool IsDisabledSource(const string &source);
    };

	// Make sure we can use Level as config param type
    std::istream &operator>>(std::istream &strm, Log::Level &val);
    std::ostream &operator<<(std::ostream &strm, Log::Level &val);
}


