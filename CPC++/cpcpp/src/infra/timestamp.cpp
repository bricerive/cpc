//
//  timestamp.cpp
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "timestamp.h"
#include "infra/error.h"
#include "infra/from_string.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/format.hpp>

using namespace std;
using namespace boost;
using namespace boost::posix_time;
using namespace infra;

static ptime const epoch(gregorian::date(1970,1,1));

TimeStamp::TimeStamp()
{
	ms = (microsec_clock::local_time()-epoch).total_milliseconds();
}

TimeStamp::TimeStamp(const string &stamp)
	: st(stamp)
{
	ASSERT_THROW(st.size() == stampLength);
	MsFromStamp();
}

TimeStamp::TimeStamp(const ptime &time)
{
	ms = (time-epoch).total_milliseconds();
}

TimeStamp::TimeStamp(unsigned long long ms): ms(ms)
{}

TimeStamp TimeStamp::Max() { return TimeStamp(numeric_limits<unsigned long long>::max()); }

TimeStamp TimeStamp::Min() { return TimeStamp(numeric_limits<unsigned long long>::min()); }

string TimeStamp::Format(const Time &now)
{
	// Note that I used to have the locale stored here as static.
	// It worked fine in the normal code but it started failing in the UT:
	// the first call gave the correct result but subsequent calls reverted to the default format.
	// So I gave up on the caching and I assume that this is fast enough anyhow.
	stringstream ss;
	ss.imbue(locale(cout.getloc(), new time_facet("%y%m%d%H%M%S"))); // the facet will be deleted by the locale
	ss << now;
	int ms = now.time_of_day().total_milliseconds() % 1000;
	return str(format("%s%03ld") % ss.str() % ms);
}

unsigned short TimeStamp::ToChunk(int s,int n)const
{
	return from_string(st.substr(s,n));
}

unsigned long long TimeStamp::Ms()const
{
	return ms;
}

const string &TimeStamp::Stamp()const
{
	if (st.empty())
		StampFromMs();
	return st;
}

string TimeStamp::Hours()const
{
	return Stamp().substr(6, 2);
}

string TimeStamp::Minutes()const
{
	return Stamp().substr(8, 2);
}

string TimeStamp::Seconds()const
{
    return Stamp().substr(10);
}

string TimeStamp::AsDate()const
{
    const string &s(Stamp());
    return s.substr(0, 2)+'/'+s.substr(2, 2)+'/'+s.substr(4, 2);
}

string TimeStamp::AsTime()const
{
    const string &s(Stamp());
    return s.substr(6, 2)+':'+s.substr(8, 2)+':'+s.substr(10, 2)+'.'+s.substr(12, 3);
}

static string ConvtMonth(const int month)
{
	static const char *const names[] = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
	ASSERT_THROW(month >= 1 && month <= 13);
	const int mnth(month - 1);
	return names[mnth];
}

string TimeStamp::AsFileNameDateTime()const
{
	const string &s(Stamp());
	return s.substr(0, 2) + '-' + ConvtMonth(stoi(s.substr(2, 2), 0, 10)) + '-' + s.substr(4, 2) + '_' + Hours() + Minutes() + Stamp().substr(10, 2);
}

/// Use boost to convert our timestamp format to a total number of milliseconcs since the Epoch
void TimeStamp::MsFromStamp()
{
	using gregorian::date;
	ptime t(date(2000+Year(),Month(),Day()), hours(Hour())+ minutes(Minute())+ seconds(Second())+ milliseconds(Millisecond()));
	ms = (t - epoch).total_milliseconds();
}

void TimeStamp::StampFromMs()const
{
    Time t = epoch + milliseconds(ms);
    st = Format(t);
}

namespace infra {

	ostream &operator<<(ostream &s, const TimeStamp &v)
	{
		s << v.Stamp();
		return s;
	}

    istream &operator>>(istream &s, infra::TimeStamp &v)
    {
        string str; s>>str; v=TimeStamp(str);
        return s;
    }

}