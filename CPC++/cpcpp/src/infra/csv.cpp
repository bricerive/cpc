//
// csv.cpp
//

//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "csv.h"
#include "infra/error.h"
#include <boost/format.hpp>

using namespace std;
using namespace infra;

Csv::Strings Csv::ReadRow(std::istream &in, char delimiter)
{
    std::stringstream ss;
    bool inquotes = false;
    Strings row;//relying on RVO
    while (in.good()) {
        char c = in.get();
        if (!inquotes && c=='"') //beginquotechar
        {
            inquotes=true;
        } else if (inquotes && c=='"') //quotechar
        {
            if (in.peek() == '"')//2 consecutive quotes resolve to 1
            {
                ss << (char)in.get();
            } else //endquotechar
            {
                inquotes=false;
            }
        } else if (!inquotes && c==delimiter) //end of field
        {
            row.push_back(ss.str());
            ss.str("");
        } else if (!inquotes && (c=='\r' || c=='\n')) {
            if (in.peek()=='\n') { in.get(); }
            row.push_back(ss.str());
            return row;
        } else {
            ss << c;
        }
    }
    return row;
}
