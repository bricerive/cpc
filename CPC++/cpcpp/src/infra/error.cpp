//
// error.cpp
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "error.h"

#ifdef WIN32
#include <windows.h>
#include <dbghelp.h>
#endif

using namespace std;
using namespace infra;

Err::Err(const CodeLoc &pos, const string &msg)
    : where(pos), why(msg)
{
    SetWhatString();
}

Err::Err(const CodeLoc &pos, const boost::format &msg)
    : where(pos), why(boost::str(msg))
{
    SetWhatString();
}

void Err::SetWhatString()
{
    whatString = where.FunctionLocation() + "[" + when.Seconds() + "]:" + why;
}

std::string Err::StackDump() const
{
#ifdef WIN32
    return string("\nCall Stack:\n") + stackTrace +"\n";
#else
	return "";
#endif
}
