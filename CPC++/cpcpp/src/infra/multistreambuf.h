#pragma once
///
/// \file multistreambuf.h
/// streambuff with multiple outputs
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//

#include "error.h"
#include <vector>
#include <cstdio>
#include <algorithm>
    
namespace infra {
    
    /// A streambuff with multiple outputs (stream and FILE *)
    class Multistreambuf: public std::streambuf
    {
    private:
        typedef std::vector<FILE *> FileDescriptors;
        typedef std::vector<std::streambuf *> StreamBufs;
        typedef std::char_traits<char> traits_type;
        typedef traits_type::int_type int_type;
        
    public:
        
        void AddStreamBuf(std::streambuf *sb)
        {
            streamBufs.push_back(sb);
        }
        
        void RemoveStreamBuf(std::streambuf *sb)
        {
            StreamBufs::iterator i = std::find(streamBufs.begin(), streamBufs.end(), sb);
            ASSERT_THROW(i!=streamBufs.end())("Attempt to remove unknown stream buffer");
            streamBufs.erase(i);
        }
        
        int_type overflow(int_type c)
        {
            for (auto i: streamBufs)
                if (i->sputc(static_cast<char>(c)) == traits_type::eof())
                    return traits_type::eof();
            return c;
        }
        
        virtual int sync()
        {    // synchronize with external agent (do nothing)
            for (auto i : streamBufs)
                i->pubsync();
            return 0;
        }
        
    private:
        StreamBufs streamBufs;
    };

}