#pragma once
///
/// \file  codeloc.h
/// Store a source code location details

//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)

#include <string>

/// We use this macro to gather preprocessor generated source filename and line number
#define _WHERE infra::CodeLoc(__FILE__, __LINE__, __FUNCTION__)


namespace infra {
    
    /// simple class to carry around code location information gathered from the preprocessor.
    /// use the \ref _WHERE macro to create an instance of it.
    struct CodeLoc {
        
        CodeLoc(const char *filePath, int lineNumber, const char *functionName);
        
        /// return a formatted string with the code location (for loging)
        std::string Formatted()const;

        /// return just the source code filename (used by log filtering)
        std::string SourceFile()const;

        /// return a verbose version of the function location more suitable for errors reporting
        std::string FunctionLocation()const;

    private:
        const char *filePath;
        int lineNumber;
        const char *functionName;
    };

}