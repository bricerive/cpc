#pragma once
///
/// \file  rich_enum.h
/// construct a class that contains an enum definition and stringizer/destringizer operators

//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)

#include "infra/error.h"
#include <string>
#include <iostream>

#define TERMINATE(...) PRIMITIVE_TERMINATE(__VA_ARGS__)
#define PRIMITIVE_TERMINATE(...)  __VA_ARGS__ ## _END

#define GENERATE_RE_VALUES(list) TERMINATE(GENERATE_RE_VALUES_A0 list)
#define GENERATE_RE_VALUES_A0(e) GENERATE_RE_VALUES_1(e) GENERATE_RE_VALUES_B
#define GENERATE_RE_VALUES_A(e) ,GENERATE_RE_VALUES_1(e) GENERATE_RE_VALUES_B
#define GENERATE_RE_VALUES_B(e) ,GENERATE_RE_VALUES_1(e) GENERATE_RE_VALUES_A
#define GENERATE_RE_VALUES_1(e) e	 
#define GENERATE_RE_VALUES_A_END
#define GENERATE_RE_VALUES_B_END

#define GENERATE_RE_STRINGIZER(list) TERMINATE(GENERATE_RE_STRINGIZER_A0 list)
#define GENERATE_RE_STRINGIZER_A0(e) GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_B
#define GENERATE_RE_STRINGIZER_A(e) ,GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_B
#define GENERATE_RE_STRINGIZER_B(e) ,GENERATE_RE_STRINGIZER_1(e) GENERATE_RE_STRINGIZER_A
#define GENERATE_RE_STRINGIZER_1(e) #e
#define GENERATE_RE_STRINGIZER_A_END
#define GENERATE_RE_STRINGIZER_B_END

#define GENERATE_RE_DESTRINGIZER(list) TERMINATE(GENERATE_RE_DESTRINGIZER_A list)
#define GENERATE_RE_DESTRINGIZER_A(e) GENERATE_RE_DESTRINGIZER_1(e) GENERATE_RE_DESTRINGIZER_B
#define GENERATE_RE_DESTRINGIZER_B(e) GENERATE_RE_DESTRINGIZER_1(e) GENERATE_RE_DESTRINGIZER_A
#define GENERATE_RE_DESTRINGIZER_1(e) if (str==#e) {value=e; return;}	 
#define GENERATE_RE_DESTRINGIZER_A_END
#define GENERATE_RE_DESTRINGIZER_B_END

// Use a base class to allow operator>> below
namespace rich_enum {
	struct RichEnumBase {
		virtual void SetFromString(const std::string &str) = 0;
		virtual const char *ToString()const = 0;
	};
    std::istream &operator>>(std::istream &strm, RichEnumBase &param);
    std::ostream &operator<<(std::ostream &strm, const RichEnumBase &param);
}


#define RICH_ENUM_VALUES(enum_name, enum_values) \
	typedef enum { RE_NOT_SET=-1, GENERATE_RE_VALUES(enum_values), RE_MAX_VALUE } Value;

#define RICH_ENUM_CORE(enum_name, enum_values) \
	enum_name(Value value): value(value) {} \
	enum_name(): value(RE_NOT_SET) {} \
	explicit enum_name(const std::string &str) { SetFromString(str); } \
	explicit enum_name(int v) { ASSERT_THROW(v>=0 && v<RE_MAX_VALUE)(v); value = static_cast<Value>(v); } \
	bool IsSet()const {return value!=RE_NOT_SET;} \
    virtual void SetFromString(const std::string &str) { GENERATE_RE_DESTRINGIZER((RE_NOT_SET)enum_values) ERR_THROW("unhandled rich enum value")(str); } \
    virtual const char *ToString()const {return static_cast<const char *>(*this);} \
    static std::vector<std::string> AllTextValues() {std::vector<std::string> v = {GENERATE_RE_STRINGIZER(enum_values)}; return v;} \
    static std::vector<Value> AllValues() {std::vector<Value> v = {GENERATE_RE_VALUES(enum_values)}; return v;} \
    explicit operator const char *()const { static const char *strs[] ={GENERATE_RE_STRINGIZER((RE_NOT_SET)enum_values)}; return strs[value+1]; } \
	bool operator==(const enum_name &v)const {return v.value==value;} \
	bool operator==(Value v)const {return v==value;} \
	bool operator!=(const enum_name &v)const {return v.value!=value;} \
	bool operator!=(Value v)const {return v!=value;} \
	operator Value ()const {return value;} \
    Value value;

#define RICH_ENUM(enum_name, enum_values) \
struct enum_name: public rich_enum::RichEnumBase { \
	RICH_ENUM_VALUES(enum_name, enum_values) \
	RICH_ENUM_CORE(enum_name, enum_values) \
    template<class A> void serialize(A &ar, unsigned int) { ar & BOOST_SERIALIZATION_NVP(value); } \
}

// version without serialize that can be used as a local class
#define RICH_ENUM_LOCAL(enum_name, enum_values) \
struct enum_name: public rich_enum::RichEnumBase { \
	RICH_ENUM_VALUES(enum_name, enum_values) \
	RICH_ENUM_CORE(enum_name, enum_values) \
}
