//
// insertion_operator .cpp
// Define all the insertion operators so they are defined once

//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
#include "infra/rich_enum.h"

namespace rich_enum {
    std::istream &operator>>(std::istream &strm, RichEnumBase &param) { std::string s; strm >> s; param.SetFromString(s); return strm; }
    std::ostream &operator<<(std::ostream &strm, const RichEnumBase &param) { strm << param.ToString(); return strm; }
        }
        
#include "i18n.h"
        
        
        namespace infra {
        std::istream &operator>>(std::istream &strm, MsgI18n &param) { std::string s; strm >> s; param.SetFromString(s); return strm; }
        std::ostream &operator<<(std::ostream &strm, const MsgI18n &param) { strm << param.ToString(); return strm; }
            }
