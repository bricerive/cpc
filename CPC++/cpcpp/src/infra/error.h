#pragma once
///
/// \file  error.h
/// Error handling
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//

#include "codeloc.h"
#include "timestamp.h"
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <string>
#include <exception>

#ifdef WIN32
typedef struct _CONTEXT CONTEXT; // forward
#endif

// typical usage:
//
//      ASSERT_THROW(pt!=0)("null pointer");
//
//      ERR_THROW("Failed!!!);
//
// Use boost::format for more complex messages

/// Use this macro for asserts
#define ASSERT_THROW(cond) \
    if (!(cond)) throw infra::Err(_WHERE)(#cond)

/// Use this macro to throw an error
#define ERR_THROW(msg) \
    throw infra::Err(_WHERE, msg)

namespace infra {
        
    /// Our error object.
    /// This is what we throw. typically, use the following kind of syntax<br>
    ///    <code>if (status!=0) throw %Err(_WHERE);</code><br>
    ///    <code>if (status!=0) throw %Err(_WHERE, "failure");</code><br>
    ///    <code>if (status!=0) throw %Err(_WHERE, boost::format("Failure on OS call (%d)") % status);</code><br>
    /// The choice depends of what you want in the error message</p>
    /// <p>The macro _WHERE is used to generate the filename and position.</p>
    /// We inherit from std::exception to be compatible with generic handlers.
    class Err: public std::exception
    {
    public:
		typedef std::string string;

        virtual ~Err() throw() {}
        
        /// construct an Err from position & C++ string
        /// \param &pos should be generated using the _WHERE macro.
        /// \param msg 
		Err(const CodeLoc &pos, const string &msg = "");
        
        /// construct an Err from position & boost::format
        /// \param &pos should be generated using the _WHERE macro.
        /// \param msg a boost::format object (see <http://www.boost.org/doc/libs/1_55_0/libs/format/>)
        Err(const CodeLoc &pos, const boost::format &msg);
        
        /// Add something to the Err
        template<typename T>
        Err &operator()(const T &somethingToAdd)
        {
            if (!why.empty()) why += '|';
			why += boost::lexical_cast<string>(somethingToAdd);
            SetWhatString();
			return *this;
        }
        
        /// support standard what method - get full message
		virtual const char *what() const throw() { return whatString.c_str(); }
		std::string Why() const { return why; }
		std::string StackDump() const;

    private:
		void Init(const CodeLoc &pos, const string &msg);
        TimeStamp when;
        CodeLoc where;
        string why;
#ifdef WIN32
        string stackTrace;
#endif
        void SetWhatString();
		string whatString; // since std::exception::what returns a const char *
    };
}