// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file sdl_init.cpp - SDL2 version
#include "sdl_init.h"

#include <SDL_image.h>

#include "infra/error.h"
#include "infra/log.h"

static void SDLLogGlue(void *userdata, int category, SDL_LogPriority priority, const char *message)
{
    LOG_STATUS(message);
}

SdlInit::SdlInit()
{
    ASSERT_THROW(SDL_InitSubSystem(SDL_INIT_VIDEO) == 0)("SDL_Init Error:");
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    ASSERT_THROW(SDL_InitSubSystem(SDL_INIT_AUDIO) == 0)("SDL_Init Error:");
    SDL_LogSetOutputFunction(SDLLogGlue, this);
    SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
    SDL_ShowCursor(SDL_ENABLE);
    // Very important: without it, the window cannot regain focus after losing it
    // and we get no more key events in wx
    SDL_StopTextInput();
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
 }

SdlInit::~SdlInit()
{
    SDL_Quit();
    IMG_Quit();
}
