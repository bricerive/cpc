// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// indicators.cpp - SDL2/osx Version

#include "indicators.h"


// Indicators colors
enum
{
    BLUE = 0,
    YELLOW,
    RED,
    DARK_BLUE,
    BLACK,
    DARK_GREY,
    LIGHT_GREY
};
static uint32_t colors[] = {
    0x0000FF, // Blue
    0xFFFF00, // Yellow
    0xFF0000, // Red
    0x000080, // Dark Blue
    0x000000, // Black
    0x333333, // Dark Grey
    0x777777, // Light Grey
};

void FillBlock(const CpcDib32 &dib, int top, int left, int h, int w, int color)
{
    uint32_t mappedColor = colors[color];

    for (int row = top; row <= top + h - 1; row++)
    {
        uint32_t *p = dib.data(row) + left;
        for (int col = left; col <= left + w - 1; col++)
            *p++ = mappedColor;
    }
}

void FillText(const CpcDib32 &font, const CpcDib32 &dst, int top, int left, int backColor, int shadowColor, int textColor, const char *text)
{
    FillBlock(dst, top, left, 9, 8 * strlen(text) + 1, backColor);
    AmstradText amstradText(font);
    amstradText.DrawAmstradText(dst, left + 1, top + 1, colors[shadowColor], text);
    amstradText.DrawAmstradText(dst, left, top, colors[textColor], text);
}

Indicators::Indicators(int w, const CpcDib32 &font)
    : buff(w * kBarH), w(w), font(font)
{
    CpcDib32 dib(GetDib());
    FillBlock(dib, 0, 0, kBarH - 1, w, DARK_GREY);
    FillBlock(dib, kBarH - 1, 0, 1, w, BLACK);
    FillText(font, dib, SPEED_STR_Y, SPEED_X, DARK_GREY, BLACK, LIGHT_GREY, "Speed");
    FillText(font, dib, SPEED_STR_Y, DRIVE_XOFF, DARK_GREY, BLACK, LIGHT_GREY, "Drives");

    FillBlock(dib, DRIVE_Y - 1, DRIVE_XOFF + DRIVE_X - 1, DRIVE_H + 2, DRIVE_W + 2, BLACK);
    FillBlock(dib, DRIVE_Y - 1, DRIVE_XOFF + DRIVE_X2 - 1, DRIVE_H + 2, DRIVE_W + 2, BLACK);

    for (int drive = 0; drive < 2; drive++)
        DriveReport(drive, CpcConsole::DriveState::NOT_READY, 0);
}

void Indicators::BlitTo(const CpcDib32 &dst) const
{
    uint32_t *dstP = dst.data();
    const uint32_t *srcP = buff.data();
    for (int i = Indicators::kBarH * w; i > 0; i--)
        *dstP++ = *srcP++;
}

void Indicators::SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed)
{
    static unsigned long totalExpected = 0;
    static unsigned long totalExecuted = 0;
    static unsigned long totalElapsed = 0;

    totalExpected += expected;
    totalExecuted += executed;
    totalElapsed += elapsed;
    if (totalElapsed > SPEED_REPORT_US)
    {
        int l1 = (SPEED_W * totalExpected) / (totalElapsed * MAX_SPEED_COEFF);
        int l2 = (SPEED_W * totalExpected) / (totalExecuted * MAX_SPEED_COEFF);
        if (l1 > SPEED_W)
            l1 = SPEED_W;
        if (l2 > SPEED_W)
            l2 = SPEED_W;

        CpcDib32 dib(GetDib());

        // Draw the used speed
        FillBlock(dib, SPEED_Y, SPEED_XOFF + SPEED_X, SPEED_H, l1, YELLOW);
        // Draw the wasted speed
        if (l2 > l1)
            FillBlock(dib, SPEED_Y, SPEED_XOFF + SPEED_X + l1, SPEED_H, l2 - l1, RED);
        // Draw the rest
        FillBlock(dib, SPEED_Y, SPEED_XOFF + SPEED_X + l2, SPEED_H, SPEED_W - l2, BLUE);
        // Draw the separator bars
        for (int i = 1; i < MAX_SPEED_COEFF; i++)
            FillBlock(dib, SPEED_Y, SPEED_XOFF + SPEED_X + (SPEED_W / MAX_SPEED_COEFF) * i - 1, SPEED_H, 1, DARK_BLUE);

        char str[12];
        snprintf(str, 12, "%3d%%", (int)(100 * totalExpected / totalElapsed));
        FillText(font, dib, SPEED_STR_Y, SPEED_XOFF + SPEED_STR_X, DARK_GREY, BLACK, LIGHT_GREY, str);
        totalExpected = totalExecuted = totalElapsed = 0;
    }
}

void Indicators::DriveReport(int which, CpcConsole::DriveState state, int track)
{
    CpcDib32 dib(GetDib());
    FillBlock(dib, DRIVE_Y, DRIVE_XOFF + (which ? DRIVE_X2 : DRIVE_X), DRIVE_H, DRIVE_W, state==CpcConsole::DriveState::NOT_READY? DARK_GREY: state==CpcConsole::DriveState::INACTIVE? BLUE: RED);
    int trackPos = ((track%40)*DRIVE_W)/40;
    FillBlock(dib, DRIVE_Y+DRIVE_H/2-2, DRIVE_XOFF + (which ? DRIVE_X2 : DRIVE_X), 4, trackPos, YELLOW);
}