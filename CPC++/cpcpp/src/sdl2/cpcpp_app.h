#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2020 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file cpcpp_app.h

#include "cpcpp_frame.h"
#include <wx/app.h>
#include <wx/filename.h>

class CpcppApp : public wxApp
{
public:
    CpcppApp(const ArgsVec &args)
        : args(args)
    {}

    // Implement xwAppConsole
    virtual bool OnInit();
    
     // Implement wxApp
    virtual void MacOpenFiles(const wxArrayString &fileNames);

private:
    static wxFileName RsrcPath();
    static wxString XrcFilePath();

    ArgsVec args;
    CpcppFrame *frame;
};
