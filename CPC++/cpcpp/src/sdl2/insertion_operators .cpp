//
// insertion_operator .cpp
// Define all the insertion operators so they are defined once
#include <iostream>
#include "SDL_audio.h"
//
//  Copyright (c) 2012-2014 Brice Rivé.
// This file is distributed under a dual license (See accompanying LICENSE.txt file)
//
std::ostream &operator<<(std::ostream &strm, const SDL_AudioSpec &a)
{
    return strm<<"freq:"<<a.freq<<" format:"<<a.format<<" chan:"<<(int)a.channels<<" samples:"<<a.samples<<" silence:"<<(int)a.silence<<" size:"<<a .size;
}
