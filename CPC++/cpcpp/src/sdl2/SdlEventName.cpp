#include <SDL_events.h>
#include <SDL.h>
#include <map>



const char *SdlEventName(int e) {
    static const std::map<int, const char*> sdlEventNames = {
    {SDL_FIRSTEVENT, "SDL_FIRSTEVENT"},
    {SDL_QUIT, "SDL_QUIT"},
    {SDL_APP_TERMINATING, "SDL_APP_TERMINATING"},
    {SDL_APP_LOWMEMORY, "SDL_APP_LOWMEMORY"},
    {SDL_APP_WILLENTERBACKGROUND, "SDL_APP_WILLENTERBACKGROUND"},
    {SDL_APP_DIDENTERBACKGROUND, "SDL_APP_DIDENTERBACKGROUND"},
    {SDL_APP_WILLENTERFOREGROUND, "SDL_APP_WILLENTERFOREGROUND"},
    {SDL_APP_DIDENTERFOREGROUND, "SDL_APP_DIDENTERFOREGROUND"},
    {SDL_DISPLAYEVENT, "SDL_DISPLAYEVENT"},
    {SDL_WINDOWEVENT, "SDL_WINDOWEVENT"},
    {SDL_SYSWMEVENT, "SDL_SYSWMEVENT"},
    {SDL_KEYDOWN, "SDL_KEYDOWN"},
    {SDL_KEYUP, "SDL_KEYUP"},
    {SDL_TEXTEDITING, "SDL_TEXTEDITING"},
    {SDL_TEXTINPUT, "SDL_TEXTINPUT"},
    {SDL_KEYMAPCHANGED, "SDL_KEYMAPCHANGED"},
    {SDL_MOUSEMOTION, "SDL_MOUSEMOTION"},
    {SDL_MOUSEBUTTONDOWN, "SDL_MOUSEBUTTONDOWN"},
    {SDL_MOUSEBUTTONUP, "SDL_MOUSEBUTTONUP"},
    {SDL_MOUSEWHEEL, "SDL_MOUSEWHEEL"},
    {SDL_JOYAXISMOTION, "SDL_JOYAXISMOTION"},
    {SDL_JOYBALLMOTION, "SDL_JOYBALLMOTION"},
    {SDL_JOYHATMOTION, "SDL_JOYHATMOTION"},
    {SDL_JOYBUTTONDOWN, "SDL_JOYBUTTONDOWN"},
    {SDL_JOYBUTTONUP, "SDL_JOYBUTTONUP"},
    {SDL_JOYDEVICEADDED, "SDL_JOYDEVICEADDED"},
    {SDL_JOYDEVICEREMOVED, "SDL_JOYDEVICEREMOVED"},
    {SDL_CONTROLLERAXISMOTION, "SDL_CONTROLLERAXISMOTION"},
    {SDL_CONTROLLERBUTTONDOWN, "SDL_CONTROLLERBUTTONDOWN"},
    {SDL_CONTROLLERBUTTONUP, "SDL_CONTROLLERBUTTONUP"},
    {SDL_CONTROLLERDEVICEADDED, "SDL_CONTROLLERDEVICEADDED"},
    {SDL_CONTROLLERDEVICEREMOVED, "SDL_CONTROLLERDEVICEREMOVED"},
    {SDL_CONTROLLERDEVICEREMAPPED, "SDL_CONTROLLERDEVICEREMAPPED"},
    {SDL_FINGERDOWN, "SDL_FINGERDOWN"},
    {SDL_FINGERUP, "SDL_FINGERUP"},
    {SDL_FINGERMOTION, "SDL_FINGERMOTION"},
    {SDL_DOLLARGESTURE, "SDL_DOLLARGESTURE"},
    {SDL_DOLLARRECORD, "SDL_DOLLARRECORD"},
    {SDL_MULTIGESTURE, "SDL_MULTIGESTURE"},
    {SDL_CLIPBOARDUPDATE, "SDL_CLIPBOARDUPDATE"},
    {SDL_DROPFILE, "SDL_DROPFILE"},
    {SDL_DROPTEXT, "SDL_DROPTEXT"},
    {SDL_DROPBEGIN, "SDL_DROPBEGIN"},
    {SDL_DROPCOMPLETE, "SDL_DROPCOMPLETE"},
    {SDL_AUDIODEVICEADDED, "SDL_AUDIODEVICEADDED"},
    {SDL_AUDIODEVICEREMOVED, "SDL_AUDIODEVICEREMOVED"},
    {SDL_SENSORUPDATE, "SDL_SENSORUPDATE"},
    {SDL_RENDER_TARGETS_RESET, "SDL_RENDER_TARGETS_RESET"},
    {SDL_RENDER_DEVICE_RESET, "SDL_RENDER_DEVICE_RESET"},
    {SDL_LASTEVENT, "SDL_LASTEVENT"},
    {SDL_USEREVENT, "SDL_USEREVENT"}
    };
    auto i = sdlEventNames.find(e);
    if (i==sdlEventNames.end()) return "Unknown";
    return i->second;
}

void PrintWindowEvent(const SDL_Event &event)
{
    if (event.type == SDL_WINDOWEVENT) {
        switch (event.window.event) {
        case SDL_WINDOWEVENT_SHOWN:
            SDL_Log("Window %d shown", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            SDL_Log("Window %d hidden", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_EXPOSED:
            SDL_Log("Window %d exposed", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_MOVED:
            SDL_Log("Window %d moved to %d,%d",
                    event.window.windowID, event.window.data1,
                    event.window.data2);
            break;
        case SDL_WINDOWEVENT_RESIZED:
            SDL_Log("Window %d resized to %dx%d",
                    event.window.windowID, event.window.data1,
                    event.window.data2);
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            SDL_Log("Window %d size changed to %dx%d",
                    event.window.windowID, event.window.data1,
                    event.window.data2);
            break;
        case SDL_WINDOWEVENT_MINIMIZED:
            SDL_Log("Window %d minimized", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_MAXIMIZED:
            SDL_Log("Window %d maximized", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_RESTORED:
            SDL_Log("Window %d restored", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_ENTER:
            SDL_Log("Mouse entered window %d",
                    event.window.windowID);
            break;
        case SDL_WINDOWEVENT_LEAVE:
            SDL_Log("Mouse left window %d", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
            SDL_Log("Window %d gained keyboard focus",
                    event.window.windowID);
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            SDL_Log("Window %d lost keyboard focus",
                    event.window.windowID);
            break;
        case SDL_WINDOWEVENT_CLOSE:
            SDL_Log("Window %d closed", event.window.windowID);
            break;
#if SDL_VERSION_ATLEAST(2, 0, 5)
        case SDL_WINDOWEVENT_TAKE_FOCUS:
            SDL_Log("Window %d is offered a focus", event.window.windowID);
            break;
        case SDL_WINDOWEVENT_HIT_TEST:
            SDL_Log("Window %d has a special hit test", event.window.windowID);
            break;
#endif
        default:
            SDL_Log("Window %d got unknown event %d",
                    event.window.windowID, event.window.event);
            break;
        }
    }
}
