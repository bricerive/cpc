// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2020 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "cpcpp_frame.h"
#include "infra/log.h"
#include <wx/xrc/xmlres.h>
#include <wx/menu.h>
#include <wx/evtloop.h>
#include <map>
#include <functional>

using namespace std;

template<typename T>
uint32_t ARRAY_SZ(const T &a) {return sizeof(a)/sizeof(a[0]);}

class DropTarget : public wxFileDropTarget
{
public:
    DropTarget(CpcppFrame &frame) :frame(frame) {}
    ~DropTarget() {}
private:
    virtual bool OnDropFiles(wxCoord x, wxCoord y, const wxArrayString &filenames)
    {
        frame.OpenFiles(filenames, x>frame.GetClientSize().GetWidth()/2);
        return true;
    }
    CpcppFrame &frame;
};

// WANTS_CHARS allows our sdl embedded window getting return and other windows keys
CpcppFrame::CpcppWindow::CpcppWindow(CpcppFrame &frame, const wxPoint &pos, const wxSize &size)
    : wxWindow(&frame, wxID_ANY, pos, size, wxWANTS_CHARS|wxBORDER_NONE)
{
    Bind(wxEVT_KEY_UP, &CpcppFrame::OnKeyChanged, &frame);
    Bind(wxEVT_KEY_DOWN, &CpcppFrame::OnKeyChanged, &frame);
    Bind(wxEVT_CHAR, &CpcppFrame::OnChar, &frame);
    Bind(wxEVT_LEFT_DOWN, &CpcppFrame::OnMouse, &frame);
    Bind(wxEVT_LEFT_UP, &CpcppFrame::OnMouse, &frame);
    Bind(wxEVT_MOTION, &CpcppFrame::OnMouse, &frame);
    Bind(wxEVT_SIZE, &CpcppFrame::OnSize, &frame);
}

void CpcppFrame::OnSize(wxSizeEvent &event)
{
    console.OnSize(event.m_size.x,event.m_size.y);
}

void RegisterCapsMonitor(void (*cb)(void *, uint32_t, bool), void *param);
void CapsChanged(void *param, uint32_t scanCode, bool down) {
    LOG_STATUS("Caps state: %d", down);
    if (down) return;
    reinterpret_cast<CpcppFrame *>(param)->CapsOff();
}

void CpcppFrame::RegisterCapsOff(std::function<void ()> f)
{
    capsLockOffHandler = f;
}

void CpcppFrame::CapsOff() {
    if (capsLockOffHandler)
        capsLockOffHandler();
    else
        console.OnKey(OsConsole::KeyEvent(false, 57));
}

CpcppFrame::CpcppFrame(const ArgsVec &args)
    : wxFrame(0, wxID_ANY, wxT("CPC++"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE)
    , console(args, *this)
    , window(*this, GetClientAreaOrigin(), GetClientSize())
{
    SetDropTarget(new DropTarget(*this)); // wx will delete it in the wxWindow dtor
    SetExtraStyle(GetExtraStyle() | wxWS_EX_PROCESS_IDLE);

    Bind(wxEVT_IDLE, &CpcppFrame::OnIdle, this);
    Bind(wxEVT_CLOSE_WINDOW, &CpcppFrame::OnClose, this);

    RegisterCapsMonitor(CapsChanged, this);

    EnableFullScreenView(true);

    SetupMenu();
    Show();
    console.ConnectCpc();
    window.Show(true); // show the emulation window
    window.SetFocus();
}

void CpcppFrame::SetupMenu()
{
    // Load the menu bar definition from the XRC file
    menuBar = wxXmlResource::Get()->LoadMenuBar(this, wxT("main_menu"));

    // map the menu item IDs to the command IDs
    std::vector<std::string> cmdNames(CpcCmd::AllTextValues());
    std::vector<CpcCmd::Value> cmdIds(CpcCmd::AllValues());
    for (int i=0; i<cmdNames.size(); ++i)
        menuItemMap[XRCID(cmdNames[i].c_str())] = cmdIds[i]; // the command name must match the menu item name in the XRC
    menuItemMap[XRCID("wxID_ABOUT")] = CpcCmd::kCmdAbout; // this name/command is specialy handled by wx

    // recursive bind all menu items in every submenu to OnMenuCommand
    for (int menuIdx=0; menuIdx<menuBar->GetMenuCount (); menuIdx++)
        BindAllInMenu(*menuBar->GetMenu(menuIdx));

    SetMenuBar(menuBar);
}

void CpcppFrame::BindAllInMenu(wxMenu &menu)
{
    for (wxMenuItem *menuItem: menu.GetMenuItems()) {
        wxMenu *subMenu = menuItem->GetSubMenu();
        if (subMenu)
            BindAllInMenu(*subMenu);
        else {
            int id = menuItem->GetId();
            if (id== XRCID("wxID_EXIT")) continue; // let WX deal with it
            Bind( wxEVT_COMMAND_MENU_SELECTED, &CpcppFrame::OnMenuCommand, this, id);
        }
    }
}

void CpcppFrame::OnMenuCommand(wxCommandEvent &event)
{
    int menuItemId = event.GetId();
    auto itr = menuItemMap.find(menuItemId);
    if (itr != menuItemMap.end())
        console.DoCommand(itr->second);
}

void CpcppFrame::CmdChecked(CpcCmd cmd, bool checked)
{
    wxMenuItem *menuItem = menuBar->FindItem(XRCID(cmd.ToString()));
    ASSERT_THROW(menuItem);
    menuItem->Check(checked);
}

void CpcppFrame::CmdEnabled(CpcCmd cmd, bool enabled)
{
    wxMenuItem *menuItem = menuBar->FindItem(XRCID(cmd.ToString()));
    ASSERT_THROW(menuItem);
    menuItem->Enable(enabled);
}

static const char *runMenus[]={"kRunMenuA", "kRunMenuB"};

void CpcppFrame::EmptyRunMenus()
{
    for (int drive=0; drive<ARRAY_SZ(runMenus); drive++) {
        wxMenuItem *menuItem = menuBar->FindItem(XRCID(runMenus[drive]));
        wxMenu *runMenu = menuItem->GetSubMenu();
        int nbItems = runMenu->GetMenuItemCount();
        int crtItem = 0;
        const wxMenuItemList &menuItems(runMenu->GetMenuItems());
        while (runMenu->GetMenuItemCount() > (drive==0? ARRAY_SZ(runMenus)-1: 0))
            runMenu->Delete(runMenu->GetMenuItems()[0]);
    }
}

void CpcppFrame::AddItemToRunMenu(CpcBase::DriveId drive, const std::string &item, bool enabled)
{
    wxMenuItem *menuItem = menuBar->FindItem(XRCID(runMenus[drive]));
    wxMenu *runMenu = menuItem->GetSubMenu();
    size_t pos = runMenu->GetMenuItemCount() - (drive==0? ARRAY_SZ(runMenus)-1: 0);
    if (item.empty())
        runMenu->InsertSeparator(pos);
    else {
        wxMenuItem *menuItem = new wxMenuItem( 0, wxID_ANY, wxString(item), wxEmptyString, wxITEM_NORMAL);
        menuItem->Enable(enabled);
        runMenu->Insert(pos, menuItem);
        Bind( wxEVT_COMMAND_MENU_SELECTED, &CpcppFrame::OnRunMenuCommand, this, menuItem->GetId());
    }
}

void CpcppFrame::OnRunMenuCommand(wxCommandEvent &event)
{
    int eventItemId = event.GetId();
    
    for (int i=0; i<ARRAY_SZ(runMenus); i++) {
        wxMenuItem *menuItem = menuBar->FindItem(XRCID(runMenus[i]));
        wxMenu *runMenu = menuItem->GetSubMenu();
        int n=0;
        for (auto it: runMenu->GetMenuItems()) {
            if (it->GetId() == eventItemId) {
                console.RunProg(i? CpcBase::DriveId::B: CpcBase::DriveId::A,n);
                return;
            }
            n++;
        }
    }
}

void CpcppFrame::OnIdle(wxIdleEvent &event)
{
    // CHeck we are the active event loop.
    // Dialogs (like Startup Options) should not enter.
    wxEventLoopBase *activeEventLoop = wxEventLoopBase::GetActive();
    if (!activeEventLoop || activeEventLoop->IsMain()) {
        try {
            if (!console.CpcHeld())
                console.RunStep();
        } catch (std::exception &e) {
            LOG_ERROR(boost::format("Exception") %e.what());
        } catch (...) {
            LOG_ERROR("Unknown exception");
        }
    }
    event.RequestMore(); // say we want more idle events if possible
}

// This is called for every key press including Shift etc.
// It does not translate keys, for example lower case q is still Q
// use it for raw keyboard mode
// TODO: it does not pass up on CAPS
void CpcppFrame::OnKeyChanged(wxKeyEvent &event)
{
    event.Skip();
    int keyCode = event.GetKeyCode();
    int rawCode = event.GetRawKeyCode();
    int unicode = event.GetUnicodeKey();
    bool keyDown = event.GetEventType() == wxEVT_KEY_DOWN;
    LOG_STATUS("OnKey%s: rawCode=%02X code=%d unicode=%d", keyDown? "Down": "Up  ", rawCode, keyCode, unicode);
    if (keyCode == WXK_NONE) return; // not sure if this ever happens
    if (rawCode == 0xFF) return;
    console.OnKey(OsConsole::KeyEvent(keyDown, rawCode));
}

// this is only called for keypresses that generate characters (excludes Alt, Shift, Ctrl, ...)
// but handles unicode characters etc.
// use it for ASCII keyboard mode
void CpcppFrame::OnChar(wxKeyEvent &event)
{
    //event.Skip(); // beeps on mac
    int unicode = event.GetUnicodeKey();
    LOG_STATUS("OnChar   : rawCode=%02X code=%d unicode=%d", event.GetRawKeyCode(), event.GetKeyCode(), unicode);
    if (unicode == WXK_NONE) return; // not sure if this ever happens
    console.OnChar(unicode);
}

void CpcppFrame::OnMouse(wxMouseEvent &event)
{
    console.OnMouse(event.ButtonDown(), event.GetX(), event.GetY());
}

// called when we drop a file on the window by wx
// called by the app when drop on icon
// called when we double-click a dsk in the finder
void CpcppFrame::OpenFiles(const wxArrayString &filenames, bool right)
{
    for (unsigned int i = 0; i != filenames.GetCount(); i++)
        console.OnDropFile(filenames[i].c_str(), right);
}

// called when we close the window
void CpcppFrame::OnClose(wxCloseEvent &event)
{
    if (event.CanVeto()) {
    }
    Destroy();
    event.Skip();
}

