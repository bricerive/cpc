// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file osconsole.cpp - SDL2 version

#include "osconsole.h"
#include "dialogs.h"
#include "cpcpp_frame.h"
#include "console_shared.h"
#include "osx/cocoa_write_image.h"
#include "infra/log.h"
#include "cpckeystr.h"
#include "mackeystr.h"
#include "version.h"

#include <wx/wx.h>
#undef HAVE_STRTOULL // both SDL and wx define this
#include <SDL_image.h>

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <thread>
#include <chrono>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <map>
#include <iostream>
#include <wx/clipbrd.h>
#include <wx/sound.h>
#include <wx/xrc/xmlres.h>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>
#include <stdlib.h>
#include <libproc.h>

typedef CpcBase::PrinterMode PrinterMode;

void *GetWindowForView(void *);

static const int WIDTH = Ga::CPC_SCR_W;
static const int HEIGHT = Ga::CPC_SCR_H; //+Indicators::kBarH;

I18N_GROUP(OsConsoleMsgId,
           (ERR_CON_BADPREF, "Bad preference file...creating a new one.")
           (ERR_CON_PRNTOPEN, "Cannot open printer output file.")
           (MSG_CON_OPEN_TITLE_DSK, "Open a DSK (disk image) file")
           (MSG_CON_OPEN_PROMPT_DSK, "Select a Disk image for drive %c")
           (MSG_CON_OPEN_TITLE_SNA, "Open a SNA (snapshot) file")
           (MSG_CON_OPEN_PROMPT_SNA, "Select a snapshot file")
           (MSG_CON_OPEN_TITLE_ROM, "Open a ROM (expansion rom) file")
           (MSG_CON_OPEN_PROMPT_ROM, "Select a ROM file for rom-slot %d")
           (MSG_CON_OPEN_TITLE_ANY, "Open SNA/DSK")
           (MSG_CON_OPEN_PROMPT_ANY, "Select a snapshot file or a disk image file"));

using namespace std;
namespace fs = boost::filesystem;

template<typename T>
uint32_t ARRAY_SZ(const T &a) {return sizeof(a)/sizeof(a[0]);}

// Map Mac keyboard to CPC keys (default)
// Gives a CPC key for each Mac key (or FF)
const std::vector<BYTE> OsConsole::rawKeyMapDefault={
    0x45,0x3C,0x3D,0x35,0x2C,0x34,0x47,0x3F, // 00
    0x3E,0x37,0xFF,0x36,0x43,0x3B,0x3A,0x32, // 08
    0x2B,0x33,0x40,0x41,0x39,0x38,0x30,0x31, // 10
    0x18,0x21,0x29,0x19,0x28,0x20,0x11,0x22, // 18
    0x2A,0x1A,0x23,0x1B,0x12,0x24,0x2D,0x1C, // 20
    0x25,0x1D,0x16,0x27,0x1E,0x2E,0x26,0x1F, // 28
    0x44,0x2F,0x13,0x4F,0xFF,0x42,0xFF,0x4C, // 30
    0x15,0x46,0x09,0x17,0x15,0xFF,0xFF,0xFF, // 38
    0xFF,0x07,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 40
    0xFF,0xFF,0xFF,0xFF,0x06,0xFF,0xFF,0xFF, // 48
    0xFF,0xFF,0x0F,0x0D,0x0E,0x05,0x14,0x0C, // 50
    0x04,0x0A,0xFF,0x0B,0x03,0xFF,0xFF,0xFF, // 58
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 60
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, // 68
    0xFF,0xFF,0x10,0x48,0xFF,0x4A,0xFF,0x49, // 70
    0xFF,0x4B,0xFF,0x08,0x01,0x02,0x00,0xFF  // 78
};


// This is necessary to capture key events
struct MonitWindow : public wxFrame
{
    MonitWindow(CpcppFrame &frame, Monitor &monitor)
        : wxFrame(&frame, wxID_ANY, wxT("CPC++ Debugger"), wxDefaultPosition, wxDefaultSize, wxCAPTION) //, wxDEFAULT_FRAME_STYLE
        , window(*this, GetClientAreaOrigin(), GetClientSize())
        , monitor(monitor)
        , mainWin(SDL_CreateWindowFrom(GetWindowForView(this->GetHandle()))) // build the SDL window around the existing wxWindow
        , mainRend(SDL_CreateRenderer(mainWin, -1, SDL_RENDERER_ACCELERATED))
    {
        CpcRect rect(monitor.WinRect());
        mainTex = SDL_CreateTexture(mainRend, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, rect.Width(), rect.Height());
        SetClientSize(wxSize(rect.Width(), rect.Height())); // set based on the desired client size
        Bind(wxEVT_LEFT_DOWN, &MonitWindow::OnMouse, this);
        Bind(wxEVT_CHAR, &MonitWindow::OnChar, this);
        // need to show then hide or the SDL+wxWidgets shows the frame
        Show(true);
        Show(false);
    }

    void MonitorTrap() {
        monitor.MonitorTrap();
        Update();
    }

    void MonitorRelease() {
        monitor.MonitorRelease();
    }

    void Update()
    {
        monitor.Refresh();

        CpcDib32 monitDib = monitor.Dib();
        CpcRect srcRect = monitor.WinRect();

        // blit CPC screen onto texture
        SDL_Rect frameDst = {0,0,srcRect.Width(), srcRect.Height()};
        SDL_UpdateTexture(mainTex, &frameDst, monitDib.data(0), srcRect.Width()*4);
        // blit texture to screen
        SDL_RenderCopy(mainRend, mainTex, 0, 0);
        SDL_RenderPresent(mainRend);
    }

    void OnMouse(wxMouseEvent &event) {
        if (event.ButtonDown()) {
            monitor.MouseDown(event.GetX(), event.GetY());
            Update();
        }
    }

    // this is only called for keypresses that generate characters (excludes Alt, Shift, Ctrl, ...)
    // but handles unicode characters etc.
    // use it for ASCII keyboard mode
    void OnChar(wxKeyEvent &event)
    {
        int keyCode = event.GetKeyCode();
        int rawCode = event.GetRawKeyCode();
        wxString rawCodeStr = wxAcceleratorEntry(0 /* no modifiers */, keyCode).ToString();
        LOG_STATUS("Monitor Window OnChar: %s rawCode=%02X[%s] keyCode=%d[%s]", event.GetEventType() == wxEVT_KEY_DOWN? "Down": "Up  "
            , rawCode, macKeyStr[rawCode], keyCode, 
            const_cast<char*>((const char*)rawCodeStr.mb_str()));
        if (keyCode>=WXK_F1 && keyCode<=WXK_F20) {
            monitor.FuncKey(keyCode-WXK_F1, event.ShiftDown());
            Update();
        }
    }

    private:
      // This is necessary to capture key events
    class CpcppWindow : public wxWindow
    {
    public:
        CpcppWindow(MonitWindow &frame, const wxPoint &pos, const wxSize &size)
        : wxWindow(&frame, wxID_ANY, pos, size, wxWANTS_CHARS|wxBORDER_NONE)
        {
            Bind(wxEVT_CHAR, &MonitWindow::OnChar, &frame);
            Bind(wxEVT_LEFT_DOWN, &MonitWindow::OnMouse, &frame);
        }
    };

    Monitor &monitor;
    SDL_Window *mainWin;
    SDL_Renderer *mainRend;
    SDL_Texture *mainTex;
    CpcppWindow window;
};

OsConsole::OsConsole(const ArgsVec &argsVec, CpcppFrame &frame)
    : CpcGuiConsole(argsVec)
    , frame(frame)
    , currentDir(fs::initial_path<fs::path>())
    , rsrcDir(fs::path(args.exePath).parent_path() / "Resources")
    , mainWin(SDL_CreateWindowFrom(GetWindowForView(frame.GetHandle()))) // build the SDL window around the existing wxWindow
    , mainRend(SDL_CreateRenderer(mainWin, -1, SDL_RENDERER_ACCELERATED))
    , amstradFont(rsrcDir, mainRend)
    , pixBuff(WIDTH * HEIGHT)
    , indicators(WIDTH, amstradFont.Dib())
    , indicatorsBuff(WIDTH * Indicators::kBarH)
    , monitor(*this)
    , monitWindow(*new MonitWindow(frame, monitor))
{
    LoadConfig();
    Splash();
    SetupSignalHandlers();
    SetupDisplay(DisplayChangeReason::INIT);
    SetupSound();
}

OsConsole::~OsConsole()
{
    if (prOut)
        fclose(prOut);

    SaveConfig();

    if (audioDev)
        SDL_CloseAudioDevice(audioDev);

    delete[] psgBuff;

    SDL_DestroyWindow(mainWin);
    SDL_DestroyRenderer(mainRend);
    SDL_DestroyTexture(mainTex);
}

OsConsole::AmstradFont::AmstradFont(const path &rsrcPath, SDL_Renderer *mainRend) {
    surface = IMG_Load((rsrcPath / "font.png").string().c_str());
}

CpcDib32 OsConsole::AmstradFont::Dib() const
{
    return CpcDib32(reinterpret_cast < RAW32 * >(surface->pixels), surface->h, surface->pitch);
}

OsConsole::AmstradFont::~AmstradFont() {
    SDL_FreeSurface(surface); // we got the texture now -> free surface
}

void OsConsole::ConnectCpc()
{
    CpcGuiConsole::ConnectCpc();
    monitor.SetCpc(cpc);
}

// Load up the config data
void OsConsole::LoadConfig()
{
    // reset to defaults in case we cannot load
    cpcConfig = CpcConfig();
    guiPrefs = GuiPrefs();
    prefs = Prefs();
    try {
        ifstream in((rsrcDir / "config.xml").c_str());
        if (!in) return;
        boost::archive::xml_iarchive ar(in);
        ar >> BOOST_SERIALIZATION_NVP(cpcConfig);
        ar >> BOOST_SERIALIZATION_NVP(guiPrefs);
        ar >> BOOST_SERIALIZATION_NVP(prefs);
    } catch(...) {
        ErrReport(OsConsoleMsgId(OsConsoleMsgId::ERR_CON_BADPREF));
    }
}

// Save the preference file
void OsConsole::SaveConfig()
{
    ofstream out((rsrcDir / "config.xml").c_str());
    if (!out)
        return;
    boost::archive::xml_oarchive ar(out);
    ar << BOOST_SERIALIZATION_NVP(cpcConfig);
    ar << BOOST_SERIALIZATION_NVP(guiPrefs);
    ar << BOOST_SERIALIZATION_NVP(prefs);
}

string OsConsole::GetClipboardString()
{
    string str;
    // Read some text
    if (wxTheClipboard->Open())
    {
        if (wxTheClipboard->IsSupported(wxDF_UNICODETEXT))
        {
            wxTextDataObject data;
            wxTheClipboard->GetData(data);
            str = data.GetText();
        }
        wxTheClipboard->Close();
    }
    return str;
}

void OsConsole::SetupSignalHandlers()
{
    static const int signals[] = {
        SIGHUP, SIGINT, SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGFPE, SIGBUS,
        SIGPIPE, SIGALRM, SIGTERM, SIGURG, SIGTSTP, SIGUSR1, SIGUSR2};
    // Catch signals for clean-up
    for (int i = 0; i < sizeof(signals) / sizeof(signals[0]); i++)
        signal(signals[i], sigHndl);
}

bool OsConsole::signalCaught = false;

void OsConsole::sigHndl(int sig)
{
    LOG_STATUS("Signal caught [%d]...cleaning up", sig);
    signalCaught = true;
    signal(sig, sigHndl); // reset handler
}

void OsConsole::MonitToggle()
{
    monitVisible = monitVisible? false: true;
    monitWindow.Show(monitVisible);
    if (monitVisible)
        monitWindow.Update();
}

void OsConsole::MonitOff()
{
    if (monitVisible)
        MonitToggle();
}

void OsConsole::MonitorTrap()
{
    monitWindow.MonitorTrap();
}

void OsConsole::SaveSnapshot(const string &path, const BYTE *buff)
{
    ofstream out(path.c_str());
    if (!out) return;

    long byteCount = 256 + ((buff[0x6b] == 64) ? 64 : 128) * 1024; // temporary cut'n'paste from OSX implementation
    out.write(reinterpret_cast<const char *>(buff), byteCount);
    SetIcon(path);
}

void OsConsole::SetIcon(const std::string &path) {
    vector<uint32_t> argb = RenderToBuffer();

    // Add alpha borders to the image so it is square before being assigned to the SNA
    int width = Ga::CPC_SCR_W;
    int height = Ga::CPC_SCR_H;
    vector<uint32_t> buffer(width * width);
    uint32_t *src = argb.data();
    uint32_t *dst = buffer.data();
    int transparent = width * (width - height) / 2;
    for (int i = 0; i < transparent; i++) *dst++ = 0;
    for (int i = 0; i < argb.size(); i++) *dst++ = *src++;
    for (int i = 0; i < transparent; i++) *dst++ = 0;
    cocoa_set_image_as_icon(buffer, path);
}

void OsConsole::WindowTitle(const string &title)
{
    string newTitle = string("CPC++ [") + versionString + "] - " + title;
    SDL_SetWindowTitle(mainWin, newTitle.c_str());
}

void OsConsole::PutPrinter(BYTE val)
{
    if (!prOut)
        if (!(prOut = fopen("PrinterOut.txt", "wb+")))
            ErrReport(OsConsoleMsgId(OsConsoleMsgId::ERR_CON_PRNTOPEN));
    if (prOut)
        fwrite((char *)&val, 1, 1, prOut);
}

void OsConsole::CmdChecked(CpcCmd cmd, bool checked)
{
    frame.CmdChecked(cmd, checked);
}
void OsConsole::CmdEnabled(CpcCmd cmd, bool enabled)
{
    frame.CmdEnabled(cmd, enabled);
}

void OsConsole::SetScreenSkip(bool val)
{
    CpcGuiConsole::SetScreenSkip(val);
    //screenSkip = val;
    //SetWindow();
}

void OsConsole::SetScreenFull(bool val)
{
    CpcGuiConsole::SetScreenFull(val);
    frame.ShowFullScreen(val);
}

void OsConsole::SetIndicators(bool val)
{
    CpcGuiConsole::SetIndicators(val);
    SetupDisplay(DisplayChangeReason::INDICATORS);
}

void OsConsole::SetMonochrome(bool val)
{
    CpcGuiConsole::SetMonochrome(val);
    //SetWindow();
}

void OsConsole::FrameReady()
{
    SDL_Rect frameDst = {0, 0, WIDTH, HEIGHT};

    if (guiPrefs.indicators) {
        // render Indicators into indicatorsBuff (WIDTH x kBarH)
        indicators.BlitTo(CpcDib32(indicatorsBuff.data(), Indicators::kBarH, 4 * WIDTH));

        // Blit indicators onto texture
        SDL_Rect indicatorsDst = {0,0,WIDTH,Indicators::kBarH};
        SDL_UpdateTexture(mainTex, &indicatorsDst, indicatorsBuff.data(), WIDTH * 4);

        // shift down sceen destination
        frameDst.y += Indicators::kBarH;
    }

    // render CPC screen into pixBuff (WIDTH x HEIGHT)
    CpcBase::RenderInfo renderInfo(CpcDib32(pixBuff.data(), HEIGHT, 4 * WIDTH)
        , cpcConfig.monochrome ? M32() : P32()
        , CpcRect(0, 0, Ga::CPC_SCR_H - 1, Ga::CPC_SCR_W - 1)
        , cpcConfig.screenSkip ? CpcBase::RenderMode::SKIP : CpcBase::RenderMode::FULL);
    cpc.RenderFrame(renderInfo, false);

    // blit CPC screen onto texture
    SDL_UpdateTexture(mainTex, &frameDst, pixBuff.data(), WIDTH * 4);

    // Get available screen size - Assuming we can get screen size this way
    int screenWidth, screenHeight;
    SDL_GetRendererOutputSize(mainRend, &screenWidth, &screenHeight);

    // Variables for texture dimensions
    int textureWidth, textureHeight;
    SDL_QueryTexture(mainTex, NULL, NULL, &textureWidth, &textureHeight);

    // Calculate the scale factor to fill the screen vertically
    float scaleFactor = (float) screenHeight / (float) textureHeight;
    // Calculate the new width to maintain the aspect ratio
    int newTextureWidth = (int) (textureWidth * scaleFactor);

    // Prepare the destination rectangle - Centering horizontally is not necessary if it extends beyond the screen
    SDL_Rect destRect { 0, 0, newTextureWidth, screenHeight};

    // In case the texture is narrower than the screen - Calculate new X to center the texture
    if (newTextureWidth < screenWidth)
        destRect.x = (screenWidth - newTextureWidth) / 2;

    // Render the texture
    SDL_RenderCopy(mainRend, mainTex, NULL, &destRect);

    // Show on-screen
    SDL_RenderPresent(mainRend);
}

template<typename T>
static void Minimize(T&v,T min) {if (v<min) v=min;}

void OsConsole::SetupDisplay(DisplayChangeReason why) // 0=init, 1=indicators, 2=resize
{
    // desired texture size
    int textureWidth = WIDTH;
    int textureHeight = HEIGHT + (guiPrefs.indicators? Indicators::kBarH: 0);
    double desiredRatio = (double)textureWidth/textureHeight;

    if (why != DisplayChangeReason::RESIZE) { // don't bother on resize
        // create/recreate the texture to the desired size
        if (mainTex) SDL_DestroyTexture(mainTex);
        mainTex = SDL_CreateTexture(mainRend, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, textureWidth, textureHeight);
    }

    // Get available screen size
    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);
    auto screenWidth = displayMode.w, screenHeight = displayMode.h;

    // Get new frame size (it's already been resized and scaled)
    int clientWidth, clientHeight;
    frame.GetClientSize(&clientWidth, &clientHeight);

    int desiredWidth = clientWidth, desiredHeight = clientHeight;

    if (why==DisplayChangeReason::INDICATORS) { // adjust client size but stay in screen
        desiredHeight = clientWidth / desiredRatio +.5;
        // stay in screen. Also, in full screen, take all height
        if (clientHeight==screenHeight || desiredHeight>=screenHeight) {
            desiredHeight = screenHeight;
            desiredWidth = desiredHeight * desiredRatio + .5;
        }
    }

    if (why == DisplayChangeReason::RESIZE) { // fit into the new client size
        Minimize(desiredWidth, textureWidth);
        Minimize(desiredHeight, textureHeight);

        double availableRatio = (double)clientWidth/clientHeight;
        if (clientWidth!=screenWidth && availableRatio<=desiredRatio) {
            // reduce the height to match the ratio
            desiredHeight = desiredWidth / desiredRatio + .5;
        } else if (clientHeight!=screenHeight) {
            // reduce the width to match the ratio
            desiredWidth = desiredHeight * desiredRatio + .5;
        }
    }

    if (desiredWidth == clientWidth && desiredHeight == clientHeight) return;

    LOG_STATUS(
        boost::format("Why:%d Screen: %dx%d  Client: %dx%d  Texture: %dx%d  -> %dx%d") % why % screenWidth % screenHeight
        % clientWidth % clientHeight % textureWidth % textureHeight % desiredWidth % desiredHeight);
    frame.SetClientSize(wxSize(desiredWidth, desiredHeight)); // set based on the desired client size
}

void OsConsole::OnSize(int w, int h)
{
    LOG_STATUS(boost::format("New window size: w=%d h=%d")%w %h);
    SetupDisplay(DisplayChangeReason::RESIZE);
}

void OsConsole::CheckEvents()
{
    if (startupFlag)
    {
        startupFlag = false;
        DoStartupEvents();
    }

    while (!keyEvents.empty())
    {
        KeyEvent keyEvent = keyEvents.front();
        keyEvents.pop();
        int cpcKeyCode = prefs.keyConv[keyEvent.rawCode];
        LOG_STATUS("KeyEvent: %s rawCode=%02X[%s] cpcKey=%d[%s]", keyEvent.down? "Down": "Up  "
            , keyEvent.rawCode, macKeyStr[keyEvent.rawCode], cpcKeyCode, cpcKeyCode<80?cpcKeyStr[cpcKeyCode]:"");
        if (keyEvent.down)
            CpcKeyDown(cpcKeyCode);
        else
            CpcKeyUp(cpcKeyCode);
    }
    while (!charEvents.empty())
    {
        AsciiDownUp(charEvents.front());
        charEvents.pop();
    }

    SDL_Event event;
    void PrintWindowEvent(const SDL_Event &event);
    const char *SdlEventName(int e);

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_WINDOWEVENT:
            //PrintWindowEvent(event);
            break;
        case SDL_KEYDOWN:
            //LOG_STATUS("SDL_KEYDOWN [%s]", SDL_GetKeyName(event.key.keysym.sym));
            break;
        case SDL_KEYUP:
            //LOG_STATUS("SDL_KEYUP [%s]", SDL_GetKeyName(event.key.keysym.sym));
            break;
        default:
            //LOG_STATUS("SDL event: %s", SdlEventName(event.type));
            break;
        }
    }
}

string OsConsole::GetStr(const infra::MsgI18n &msgId)
{
    return msgId.GetText();
}

void OsConsole::PutError(const string &str)
{
    wxMessageDialog(NULL, str, "CPC++ Error", wxOK).ShowModal();
}

bool OsConsole::ConfirmDlg(const infra::MsgI18n &msgId)
{
    return MessageBoxConfirmation("CPC++ Confirmation", msgId.GetText().c_str());
}

bool OsConsole::MessageBoxConfirmation(const char *title, const char *prompt)
{
    return wxMessageDialog(NULL, prompt, title, wxYES_NO).ShowModal() == wxID_YES;
}

void OsConsole::OnDropFile(const char *file, bool right)
{
    // Look at the data
    ifstream in(file);
    BYTE data[128];
    in.read(reinterpret_cast<char *>(data), 128);
    if (!in)
        return;
    if (CpcGuiConsole::IsSnapshot(file, data)) {
        LoadSnapshot(file);
        return;
    }
    if (CpcGuiConsole::IsCpcpp(file, data)) {
        LoadCpcpp(file);
        return;
    }
    LoadDisk(file, right? CpcBase::DriveId::B: CpcBase::DriveId::A);
}

void OsConsole::OnChar(int unicode)
{
    charEvents.push(unicode);
}

void OsConsole::OnKey(const KeyEvent &keyEvent)
{
    keyEvents.push(keyEvent);
}

void OsConsole::OnMouse(bool down, int x, int y)
{
    if (down) {
        WindowClick(x, y);
        monitWindow.MonitorRelease();
    }
}

void OsConsole::DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char *str) const
{
    AmstradText amstradText(amstradFont.Dib());
    amstradText.DrawAmstradText(dst, x, y, color, str);
}

void OsConsole::SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed)
{
    if (!guiPrefs.indicators)
        return;
    indicators.SpeedReport(expected, executed, elapsed);
}

void OsConsole::DiskActivity(int which, DriveState state, int track)
{
    if (!guiPrefs.indicators)
        return;
    indicators.DriveReport(which, state, track);
}

void OsConsole::SleepUs(UINT32 us)
{
    std::this_thread::sleep_for(std::chrono::microseconds(us));
}

unsigned long OsConsole::TicksMs()
{
    return GetCrtUs() / 1000;
}

unsigned long OsConsole::GetCrtUs()
{
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    return (tv.tv_sec & 0x3FFF) * 1000000 + tv.tv_usec;
}

ostream &operator<<(ostream &strm, const SDL_AudioSpec &a);

static const int soundRenderFreq = 125000; // integer divider of PSG actual freq (125KHz/8) (15625=5^6)
static const int desiredChunkPeriodMs = 40; // render chunk size in ms
static const int soundChunkSamples = static_cast<int>(soundRenderFreq / (1000.0 / desiredChunkPeriodMs)); // number of samples in a buffer
static const int soundQueuePages = 4; // number of buffers in the queue
static const int soundQueueBlockBytes = soundChunkSamples * 4; // *2 for stereo, *2 for 16 bits

/* Use Signed 16 as unsigned 16 does not seem to be supported (https://loka.developpez.com/tutoriel/sdl/sons/):
    AUDIO_U8 : Echantillons 8 bits non signés,
    AUDIO_S8 : Echantillons 8 bits signés,
    AUDIO_U16LSB : Echantillons 16 bits non signés (non encore supportés, car peu répandus),
    AUDIO_S16LSB : Echantillons 16 bits signés (les plus répandus),
    AUDIO_U16MSB, AUDIO_S16MSB : idem que ci-dessus pour les architectures big endian (pratiquement tout ce qui n'est pas Intel, comme les PowerPC ou les Alpha).
*/
void OsConsole::SetupSound()
{
    int numDevices = SDL_GetNumAudioDevices(0);
    for (int i=0; i<numDevices; i++)
        LOG_STATUS(boost::format("Device[%d] - %s") %i % SDL_GetAudioDeviceName(i, 0));

    SDL_AudioSpec desired, obtained;
    desired.freq = soundRenderFreq;
    desired.format = AUDIO_S16LSB; // cannot use unsigned 16bits with SDL2 (see comment above))
    desired.channels = 2; // stereo
    desired.samples = soundChunkSamples;
    desired.callback = OsConsole::SoundCallbackGlue;
    desired.userdata = this;

    audioDev = SDL_OpenAudioDevice(0, 0, &desired, &obtained, 0);
    LOG_STATUS(boost::format("Desired[%s] Obtained[%s] device[%d]") % desired % obtained % (int)audioDev);

    // Setup interface for PSG
    psgBuff = new UINT8[obtained.size];
    psgBuffSize = obtained.size;
    cpc.SetSoundInfo(CpcBase::SoundInfo(obtained.channels==2, 2, soundRenderFreq, psgBuff, soundChunkSamples));

    silence.resize(obtained.size, obtained.silence);

    sndQueue = new UINT8[soundQueueBlockBytes * soundQueuePages];
}

// Got a new sound buffer to play from the CPC
void OsConsole::SoundBuffDone()
{
    memcpy(&(sndQueue[sndQueueWr * soundQueueBlockBytes]), psgBuff, psgBuffSize);
    if (waitForSound)
        while (sndQueueRd != sndQueueWr)
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
    sndQueueWr = (sndQueueWr + 1) % soundQueuePages;
}

void OsConsole::OsVol(const CpcSoundLevel &v)
{
    vol = v;
}

// Real-time sound output needs a new CPC sound buffer to play.
void OsConsole::SoundCallback(Uint8 *stream, int len)
{
    // Wr should be ahead of Rd. If not, CPC is too slow for real-time, play silence
    if (sndQueueWr == sndQueueRd) {
        memset(stream, lastSoundVal, len);
        return;
    }
    // set the buffer to silence
    memcpy(stream, silence.data(), len);
    // Mix-in our new CPC sound data
    SDL_MixAudioFormat(stream, sndQueue + sndQueueRd * soundQueueBlockBytes, AUDIO_S16LSB, len, 16*(vol+1));
    sndQueueRd = (sndQueueRd + 1) % soundQueuePages;
    lastSoundVal = stream[len-1];
}

void OsConsole::SoundCallbackGlue(void *userdata, Uint8 *stream, int len)
{
    static_cast<OsConsole *>(userdata)->SoundCallback(stream, len);
}

void OsConsole::WaitForSound(bool val) {
    waitForSound = val;
}

void OsConsole::StartSound()
{
    SDL_PauseAudioDevice(audioDev, 0);
}

void OsConsole::StopSound()
{
    SDL_PauseAudioDevice(audioDev, 1);
}

void OsConsole::PlayCpcSound(CpcSound sound)
{
    if (sound == Insert)
        wxSound((rsrcDir / "Insert.wav").c_str()).Play(wxSOUND_SYNC);
    else if (sound == Eject)
        wxSound((rsrcDir / "Eject.wav").c_str()).Play(wxSOUND_SYNC);
}

void OsConsole::AddItemToRunMenu(CpcBase::DriveId drive, const string &item, bool enabled)
{
    frame.AddItemToRunMenu(drive, item, enabled);
}

void OsConsole::EmptyRunMenus()
{
    frame.EmptyRunMenus();
}

void OsConsole::SavePICTFile()
{
    std::string name;
    if (!GetSavePath(CpcGuiMsgId::MSG_CON_SAVEPICT, CpcGuiMsgId::MSG_CON_UNTITLED_PICT, name))
        return;
    cocoa_write_image(RenderToBuffer(), name);
}

void OsConsole::CopyToClipboard()
{
    cocoa_image_to_clipboard(RenderToBuffer());
}

bool OsConsole::SelectFile(string &path, FileType &type, int &option)
{
    static const char *DSK_EXTENSION="DSK";
    static const char *SNA_EXTENSION="SNA";
    static const char *ROM_EXTENSION="ROM";

    string title, prompt, filter;
    if (type == FileDisk)
    {
        title = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_TITLE_DSK).GetText();
        prompt = (OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_PROMPT_DSK) % (option ? 'B' : 'A')).GetText();
        filter = string("DSK Image (*.") + DSK_EXTENSION + ")|*." + DSK_EXTENSION + "|All|*.*|";
    }
    else if (type == FileSnapshot)
    {
        title = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_TITLE_SNA).GetText();
        prompt = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_PROMPT_SNA).GetText();
        filter = string("SNA Snapshot (*.") + SNA_EXTENSION + ")|*." + SNA_EXTENSION + "|All|*.*|";
    }
    else if (type == FileRom)
    {
        title = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_TITLE_ROM).GetText();
        prompt = (OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_PROMPT_ROM) % option).GetText();
        filter = string("ROM Image (*.") + ROM_EXTENSION + ")|*." + ROM_EXTENSION + "|All|*.*|";
    }
    else
    {
        title = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_TITLE_ANY).GetText();
        prompt = OsConsoleMsgId(OsConsoleMsgId::MSG_CON_OPEN_PROMPT_ANY).GetText();
        filter = string("All|*.*|SNA Snapshot (*.") + SNA_EXTENSION + ")|*." + SNA_EXTENSION + "|DSK Image (*." + DSK_EXTENSION + ")|*." + DSK_EXTENSION + "|";
    }

    wxFileDialog fileDialog(0, title, "", "", filter, wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (fileDialog.ShowModal() == wxID_CANCEL)
        return false;
    path = fileDialog.GetPath();
    return true;
}

bool OsConsole::FilterFile(const std::string &path, FileType type)
{
    // Look at the data
    ifstream in(path);
    BYTE data[128];
    in.read(reinterpret_cast<char *>(data), 128);
    if (!in)
        return false;

    return IsFileType(path, data, type);
}

void OsConsole::CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size)
{
    data = 0;
    size = 0;
}

void OsConsole::SetFileType(const std::string &path, FileType type) {}

void OsConsole::LaunchUrl(const std::string &url) {}

void OsConsole::Splash() {}

bool OsConsole::GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path)
{
    string promptStr = GetStr(prompt);
    string name = path.empty() ? GetStr(defaultName) : path;

    wxFileDialog fileDialog(0, GetStr(prompt), "", name, "", wxFD_SAVE);
    if (fileDialog.ShowModal() == wxID_CANCEL)
        return false;
    path = fileDialog.GetPath();
    return true;
}

void OsConsole::DoStartupDialog() { Dialogs::DoStartupDialog(*this); }
bool OsConsole::GetHexValue(const char *prompt, int &val) { return Dialogs::GetHexValue(*this, prompt, val); }
void OsConsole::DoRomDialog() { Dialogs::DoRomDialog(*this); }
void OsConsole::DoKeyboardDialog() { Dialogs::DoKeyboardDialog(*this); }
void OsConsole::DoAboutDialog() { Dialogs::DoAboutDialog(*this); }

string OsConsole::ApplicationFolder()
{
    pid_t pid = getpid();
    char pathbuf[1024];
    if (proc_pidpath (pid, pathbuf, sizeof(pathbuf)) <=0)
        return "";
    return string(pathbuf);
}
