#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file osconsole.h - SDL2 version

#include "monitor.h"
#include "indicators.h"
#include "cpcguiconsole.h"
#include "sdl_init.h"
#include "infra/struct_with_ctor.h"
#include <boost/filesystem.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <queue>

class CpcppWindow;
class CpcppFrame;
union SDL_Event;
struct SDL_KeyboardEvent;
struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
struct SDL_Surface;
typedef uint32_t SDL_AudioDeviceID;

typedef enum {
    ERR_CON_CONFIG_LOAD,
    ERR_CON_DSKOPEN,
    MSG_MODELCHANGE,
    ERR_CON_PRNTOPEN
} MsgId;

struct MonitWindow;

/// SDL2 version of CPC++ console
class OsConsole: public CpcGuiConsole, public MonitorConsole {
    typedef std::string string;
    
public:
    OsConsole(const ArgsVec &args, CpcppFrame &frame);
    ~OsConsole();
    void OnSize(int w, int h);

    // Implement MonitorConsole interface
    bool GetHexValue(const char *prompt, int &val) override;
    void DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char *str)const override;
    const RAW32 *GetP32() override {return P32();}
    
    // Implement CpcConsole interface
    void DiskActivity(int which, DriveState state, int track)override;
    void PutPrinter(BYTE val)override;
    void MonitorTrap()override;
    void SoundBuffDone()override;
    bool CpcHeld()const {return monitor.CpcHeld();}

    // Implement OsGuiConsole interface
    void FrameReady()override;
    void ConnectCpc() override;
    void CheckEvents()override;

    // these are static but C++ needs CRP for static method polymorphism
    //  (between CpcGuiConsole & OsConsole)
    // That is:
    //      template<class OsCpcGuiConsole> class CpcGuiConsole: public CpcConsole {
    // This leads to ugly syntax everywhere in CpcGuiConsole, e.g.:
    //      template<class OSC> void CpcGuiConsole<OSC>::SaveFile
    // That's a hefty price for 3 statics
    // also, To get an instantiation of CpcGuiConsole<OsConsole> available for link, I need to do:
    //      template class CpcGuiConsole<OsConsole>;
    // Somewhere in a CPP file.
    // If I do it in CpcGuiConsole.cpp, I need to give it access to the platform specific headers used in OsConsole
    // If I do it in OsConsole.cpp, I need to put the templated code of CpcGuiConsole in its header, which make consoleLib platform aware.
    // Another option would be to try and cut OsConsole.h in two parts: One x-platform enough to avoid the library coupling and another that inherits from it.
    // In that case, I can instantiate in CpcGuiConsole.cpp
    void SleepUs(UINT32 us)override;
    unsigned long TicksMs()override;
    unsigned long GetCrtUs()override;
    std::string GetStr(const infra::MsgI18n &errId) override;

    bool CanSound() const override{ return true; };
    bool CanStereo() const override{ return true; };
    std::string GetClipboardString()override;
    void MonitOff()override;
    void MonitToggle()override;
    void PutError(const std::string &str)override;
    void CmdChecked(CpcCmd cmd, bool checked)override;
    void CmdEnabled(CpcCmd, bool enabled)override;
    bool FilterFile(const std::string &path, FileType type)override;
    void CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size)override;
    void SetFileType(const std::string &path, FileType type)override;
    void DoStartupDialog()override;
    void DoKeyboardDialog()override;
    void DoRomDialog()override;
    void DoAboutDialog()override;
    bool GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path)override;
    void AddItemToRunMenu(CpcBase::DriveId drive, const std::string &item, bool enabled)override;
    void EmptyRunMenus()override;
    void SavePICTFile()override;
    void CopyToClipboard()override;
    void PlayCpcSound(CpcSound sound)override;
    bool SelectFile(std::string &path, FileType &type, int &option)override;
    void LaunchUrl(const std::string &url)override;
    void WindowTitle(const std::string &title)override;
    void SetScreenSkip(bool val)override;
    void SetScreenFull(bool val)override;
    void SetIndicators(bool val)override;
    void SetMonochrome(bool val)override;
    
    bool ConfirmDlg(const infra::MsgI18n &msgId)override;
    void SetupSound();
    void WaitForSound(bool val)override;
    void StartSound()override;
    void StopSound()override;
    void OsVol(const CpcSoundLevel &val)override;

    void SpeedReport(UINT32 elapsed, UINT32 executed, UINT32 expected) override;
    void SaveSnapshot(const std::string &path, const BYTE *buff) override;
    void SetIcon(const std::string &path) override;

    void OnDropFile(const char *file, bool right);
    
    STRUCT_WITH_CTOR(KeyEvent, (bool,down)(int,rawCode));
    void OnKey(const KeyEvent &keyEvent);
    void OnChar(int unicode);
    void OnMouse(bool down,  int x, int y);

    static std::string ApplicationFolder();
private:
    typedef boost::filesystem::path path;

    void LoadConfig();
    void SaveConfig();
    
    void Splash();
    void SetupSignalHandlers();

    RICH_ENUM(DisplayChangeReason, (INIT) (INDICATORS) (RESIZE));
    void SetupDisplay(DisplayChangeReason why);

    bool MessageBoxConfirmation(const char *title, const char *prompt);
            
    OsConsole(const OsConsole &) = delete;
    OsConsole &operator=(const OsConsole &) = delete;
            
    // Signals handling
    static void sigHndl(int sig);
    static bool signalCaught;
        
    static void SoundCallbackGlue(void *userdata, uint8_t *stream, int len);
    void SoundCallback(uint8_t *stream, int len);

    CpcppFrame &frame;
    
    std::queue<KeyEvent> keyEvents;
    std::queue<int> charEvents;

    path currentDir;
    path rsrcDir;

    FILE *prOut=0;
    unsigned long crtTick=0;
    bool startupFlag=true;
    
    SDL_Window *mainWin;
    SDL_Renderer *mainRend;
    SDL_Texture *mainTex=0;
    
    struct AmstradFont {
        AmstradFont(const path &rsrcPath, SDL_Renderer *mainRend);
        // Create a Dib around the texture
        CpcDib32 Dib() const;
        ~AmstradFont();
    private:
        SDL_Surface *surface;
    };
    AmstradFont amstradFont;

    // GA write pointer (XImage or offscreen)
    PixBuff pixBuff;
    Indicators indicators;
    PixBuff indicatorsBuff;

    // OsPsg stuff
    int outputFreq;
    CpcSoundLevel vol  {CpcSoundLevel::LEVEL_0};
    UINT8 *psgBuff;
    std::vector<uint8_t> silence;
    uint8_t lastSoundVal=0;
    int buffEndOffset;
    int psgBuffSize;
    int canSynch;
    long buffEndTime;
    SDL_AudioDeviceID audioDev=0;
    bool waitForSound=0;
    UINT8 *sndQueue=0;
    int sndQueueRd=0;
    int sndQueueWr=0;
    
    bool fullScreen=false;

    Monitor monitor;
    MonitWindow &monitWindow;
    bool monitVisible=false;

    static const std::vector<BYTE> rawKeyMapDefault;

    /// store the part of the config specific to the OsConsole (preferences)
    CPC_CONFIG_STRUCT(PrefsBase,
        (std::vector<BYTE>, keyConv)
    );
    struct Prefs: public PrefsBase {
        Prefs(): PrefsBase(rawKeyMapDefault){}
    };    

    // Preference attributes
    Prefs prefs;

    friend class Dialogs;
};
