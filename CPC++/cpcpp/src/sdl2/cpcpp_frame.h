#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2020 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file cpcpp_frame.h

#include "osconsole.h"
#include <wx/frame.h>
#include <wx/dnd.h>
#include <wx/wx.h>


// The frame is the high-level application object.
// It handles application-level commands and menus.
class CpcppFrame : public wxFrame
{
public:
    CpcppFrame(const ArgsVec &args);

    void OpenFiles(const wxArrayString &filenames, bool right=false);
    
    void OnKeyChanged(wxKeyEvent &event);
    void OnChar(wxKeyEvent &event);
    void OnMouse(wxMouseEvent &event);
    void OnSize(wxSizeEvent &event);

    // called back from OsConsole
    void CmdChecked(CpcCmd cmd, bool checked);
    void CmdEnabled(CpcCmd, bool enabled);
    void EmptyRunMenus();
    void AddItemToRunMenu(CpcBase::DriveId drive, const std::string &item, bool enabled);
    
    void RegisterCapsOff(std::function<void ()> f);
private:
    void SetupMenu();
    void BindAllInMenu(wxMenu &menu);
    void OnMenuCommand(wxCommandEvent &event);
    void OnRunMenuCommand(wxCommandEvent &event);

    void OnIdle(wxIdleEvent &event);

    void OnClose(wxCloseEvent &event);

    // This is necessary to capture key events
    class CpcppWindow : public wxWindow
    {
    public:
        CpcppWindow(CpcppFrame &frame, const wxPoint &pos, const wxSize &size);
    };
    std::map<int, CpcCmd> menuItemMap;
    OsConsole console;
    CpcppWindow window;
    wxMenuBar *menuBar=0;

    std::function<void ()> capsLockOffHandler;
    void CapsOff();
    friend void CapsChanged(void *param, uint32_t scanCode, bool down);
};
