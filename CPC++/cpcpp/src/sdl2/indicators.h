#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file indicators.h - SDL2 version
#include "amstrad_text.h"
#include "cpcconsole.h"

typedef std::vector<uint32_t> PixBuff;

struct Indicators {
    static const int kBarH=16;
    Indicators(int w, const CpcDib32 &font);
    void BlitTo(const CpcDib32 &dst) const;
    void SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed);
    void DriveReport(int which, CpcConsole::DriveState state, int track);
    
private:
    CpcDib32 GetDib() { return CpcDib32(buff.data(), kBarH, w*4);}

    static const int SPEED_XOFF=7*8;

    static const int MAX_SPEED_COEFF=10;
    static const int SPEED_X=4;
    static const int SPEED_Y=4;
    static const int SPEED_W=60;
    static const int SPEED_H=8;
    static const int SPEED_REPORT_US=100000;
    static const int SPEED_STR_X=70;
    static const int SPEED_STR_Y=4;

    static const int DRIVE_XOFF=SPEED_XOFF+SPEED_STR_X+100;
    static const int DRIVE_X=7*8;
    static const int DRIVE_X2=DRIVE_X+32;
    static const int DRIVE_Y=6;
    static const int DRIVE_W=28;
    static const int DRIVE_H=6;
    
    PixBuff buff;
    int w;
    CpcDib32 font;
};
