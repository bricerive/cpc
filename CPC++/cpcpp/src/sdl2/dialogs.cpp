// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file dialogs.cpp - SDL2 version

#include "dialogs.h"
#include "osconsole.h"
#include "cpcpp_frame.h"
#include "cpckeystr.h"
#include "mackeystr.h"
#include <wx/wx.h>
#include <wx/statline.h>
#include <wx/tooltip.h>
#include <wx/xrc/xmlres.h>

using namespace std;
using format = boost::format;

static void AddTextInput(wxDialog *dialog, wxBoxSizer *sizer, const string &prompt, string &text)
{
    wxStaticText *label = new wxStaticText(dialog, wxID_ANY, prompt, wxDefaultPosition, wxSize(-1, -1), 0);
    wxTextCtrl *textField = new wxTextCtrl(dialog, wxID_ANY, text, wxDefaultPosition, wxSize(-1, -1), 0);
    wxBoxSizer *hSizer = new wxBoxSizer(wxHORIZONTAL);
    hSizer->Add(label, 0, wxALL, 3); // proportion, border flags, border
    hSizer->Add(textField, 1, wxALL, 3);
    sizer->Add(hSizer, 1, wxEXPAND);
    dialog->Bind(
        wxEVT_TEXT, [textField, &text](wxEvent &event) { text = textField->GetLineText(0); }, textField->GetId());
}

static void AddStdButtons(wxDialog *dialog, wxBoxSizer *sizer)
{
    wxStdDialogButtonSizer *buttonsSizer = new wxStdDialogButtonSizer();
    buttonsSizer->AddButton(new wxButton(dialog, wxID_OK));
    buttonsSizer->AddButton(new wxButton(dialog, wxID_CANCEL));
    buttonsSizer->Realize();
    sizer->Add(buttonsSizer, 1, wxEXPAND, 5);
}

template <typename F>
static void AddFileSelect(wxDialog *dialog, wxBoxSizer *sizer, const string &prompt, string &path, F selector)
{
    wxButton *select = new wxButton(dialog, wxID_ANY, prompt, wxDefaultPosition, wxSize(130, -1), 0);
    wxStaticText *info = new wxStaticText(dialog, wxID_ANY, path, wxDefaultPosition, wxSize(300, -1), 0);
    wxButton *clear = new wxButton(dialog, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0);
    wxBoxSizer *hSizer = new wxBoxSizer(wxHORIZONTAL);
    hSizer->Add(select, 0, wxALL, 3);
    hSizer->Add(info, 1, wxALL, 3);
    hSizer->Add(clear, 0, wxALL, 3);
    dialog->Bind(
        wxEVT_BUTTON, [info, &path](wxEvent &event) {path.clear();info->SetLabel(path); }, clear->GetId());
    dialog->Bind(
        wxEVT_BUTTON, [info, &path, selector](wxEvent &event) {selector(path); info->SetLabel(path); }, select->GetId());
    sizer->Add(hSizer, 1, wxEXPAND);
}

void Dialogs::DoRomDialog(OsConsole &console)
{

    class RomDialog : public wxDialog
    {
    public:
        RomDialog(OsConsole &console)
            : wxDialog(&console.frame, wxID_ANY, wxT("CPC++ ROM Selection"))
            , console(console)
        {
            auto extRomsSizerLeft = new wxBoxSizer(wxVERTICAL);
            for (int i = 0; i < 16; i++)
                SetRomPanel(boost::lexical_cast<string>(i), console.cpcConfig.upperRoms[i], *extRomsSizerLeft);
            auto extRomsSizerRight = new wxBoxSizer(wxVERTICAL);
            for (int i = 16; i < 32; i++)
                SetRomPanel(boost::lexical_cast<string>(i), console.cpcConfig.upperRoms[i], *extRomsSizerRight);
            wxBoxSizer *extRomsSizer = new wxBoxSizer(wxHORIZONTAL);
            extRomsSizer->Add(extRomsSizerLeft, 0, wxEXPAND);
            extRomsSizer->Add(new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL), 1);
            extRomsSizer->Add(extRomsSizerRight, 0, wxEXPAND);

            wxBoxSizer *lowRomSizer = new wxBoxSizer(wxHORIZONTAL);
            SetRomPanel("Lower ROM", console.cpcConfig.lowerRom, *lowRomSizer, true);

            auto mainSizer = new wxBoxSizer(wxVERTICAL);
            mainSizer->Add(extRomsSizer, 0);
            mainSizer->Add(new wxStaticLine(this), 1, wxALL);
            mainSizer->Add(lowRomSizer, 0);
            AddStdButtons(this, mainSizer);

            Layout();
            SetSizerAndFit(mainSizer);
            Centre(wxBOTH);

            if (ShowModal() == wxID_OK)
                for (auto rom : roms)
                    rom.Validate();
        }

    private:
        void SetRomPanel(const string &id, RomConfig &rom, wxBoxSizer &parent, bool lower=false)
        {
            roms.emplace_back(this, id, rom, lower);
            parent.Add(roms.back().sizer, 1, wxEXPAND);
        }
        struct RomPanel
        {
            RomPanel(RomDialog *dialog, const string &id, RomConfig &rom, bool lower)
                : console(dialog->console), rom(rom), lower(lower)
            {
                active = new wxCheckBox(dialog, wxID_ANY, wxString(id), wxDefaultPosition, wxSize(lower? 10*id.size(): 45, -1), 0);
                wxButton *select = new wxButton(dialog, wxID_ANY, wxT("Select"), wxDefaultPosition, wxSize(60, -1), 0);
                info = new wxStaticText(dialog, wxID_ANY, rom.description, wxDefaultPosition, wxSize(150, -1), 0);
                info->SetToolTip(new wxToolTip(rom.path));
                wxButton *clear = new wxButton(dialog, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxSize(60, -1), 0);
                sizer = new wxBoxSizer(wxHORIZONTAL);
                sizer->Add(active, 0, wxALL, 3);
                sizer->Add(select, 0, wxALL, 3);
                sizer->Add(info, 1, wxALL, 3);
                sizer->Add(clear, 0, wxALL, 3);
                dialog->Bind(
                    wxEVT_BUTTON, [this](wxEvent &event) { Clear(); }, clear->GetId());
                dialog->Bind(
                    wxEVT_BUTTON, [this](wxEvent &event) { Select(); }, select->GetId());
                active->SetValue(rom.active);
                path = rom.path;
            }
            void Clear()
            {
                path = "";
                info->SetLabel("");
                active->SetValue(false);
            }
            void Select()
            {
                string description;
                if (console.SelectRom(path, description, 0))
                {
                    if (lower)
                        info->SetLabel(boost::filesystem::path(path).stem().string());
                    else
                        info->SetLabel(description);
                    active->SetValue(true);
                }
            }
            void Validate()
            {
                rom.path=path;
                rom.description=info->GetLabel();
                rom.active = active->GetValue();
            }
            wxBoxSizer *sizer;

        private:
            OsConsole &console;
            wxCheckBox *active;
            wxStaticText *info;
            string path;
            RomConfig &rom;
            bool lower; // need to treat lower ROM specifically
        };

        list<RomPanel> roms; // cannot be a vector because realloc will make bound lambdas dangling
        OsConsole &console;
    };

    RomDialog dialog(console);
}

bool Dialogs::GetHexValue(OsConsole &console, const char *prompt, int &val)
{
    struct Dialog : public wxDialog
    {
        Dialog(OsConsole &console, const char *prompt, string &hexStr)
            : wxDialog(&console.frame, wxID_ANY, wxT("CPC++ Hex value"))
        {
            wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);
            AddTextInput(this, mainSizer, prompt, hexStr);
            AddStdButtons(this, mainSizer);
            Layout();
            SetSizerAndFit(mainSizer);
            Centre(wxBOTH);
        };
    };
    string hexStr;
    Dialog dialog(console, prompt, hexStr);
    if (dialog.ShowModal() == wxID_OK && sscanf(hexStr.c_str(), "%04X", &val))
        return true;
    return false;
}

void Dialogs::DoStartupDialog(OsConsole &console)
{
    struct Dialog : public wxDialog
    {
        Dialog(OsConsole &console)
            : wxDialog(&console.frame, wxID_ANY, wxT("CPC++ Startup Options"))
        {
            wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);
            string driveA = console.cpcConfig.driveADiskPath;
            AddFileSelect(this, mainSizer, "Select Drive A...", driveA, [&console](string &s) -> bool { return console.SelectFloppyImage(s, CpcBase::DriveId::A); });
            string driveB = console.cpcConfig.driveBDiskPath;
            AddFileSelect(this, mainSizer, "Select Drive B...", driveB, [&console](string &s) -> bool { return console.SelectFloppyImage(s, CpcBase::DriveId::B); });
            string snapshot = console.cpcConfig.snapshotPath;
            AddFileSelect(this, mainSizer, "Select Snapshot...", snapshot, [&console](string &s) -> bool { return console.SelectSnapshot(s); });
            string command = console.cpcConfig.startupComm;
            AddTextInput(this, mainSizer, "Command:", command);
            AddStdButtons(this, mainSizer);

            Layout();
            SetSizerAndFit(mainSizer);
            Centre(wxBOTH);

            if (ShowModal() == wxID_OK)
            {
                console.cpcConfig.driveADiskPath = driveA;
                console.cpcConfig.driveBDiskPath= driveB;
                console.cpcConfig.snapshotPath= snapshot;
                console.cpcConfig.startupComm = command;
            }
        }
    };

    Dialog dialog(console);
}

void Dialogs::DoAboutDialog(OsConsole &console) {
    struct AboutDialog: public wxDialog {
        AboutDialog() { wxXmlResource::Get()->LoadDialog(this, 0, wxT("AboutDialog")); }
    };
    AboutDialog().ShowModal();
}

void Dialogs::DoKeyboardDialog(OsConsole &console)
 {
    struct RawKeyboardFrame: public wxFrame {

        RawKeyboardFrame(OsConsole &console)
            : wxFrame(0, wxID_ANY, "Raw keyboard mapping", wxDefaultPosition, wxDefaultSize)
            , console(console)
        {
            // No raw char events without this - put it first so it does not mask keymap top left corner
            wxPanel* mainPane = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxWANTS_CHARS);

            wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

            cpckbdImage = new wxImage((console.rsrcDir / "cpckbd_x2.png").c_str());

            wxBitmap cpcKbd(*cpckbdImage);
            keyboardBitmap = new wxStaticBitmap(this, wxID_ANY, cpcKbd, wxDefaultPosition, wxDefaultSize, 0 );
            keyboardBitmap->SetToolTip(wxT(
                "Click on a CPC key to select/deselect it.\nWhen a CPC key is selected you can assign a host key to it by pressing that key."));
            sizer->Add(keyboardBitmap);

            sizer->Add(new wxStaticText( this, wxID_ANY, wxT("Press Key and simultaneously click CPC key to map it to."), wxDefaultPosition, wxDefaultSize, 0 ));

            mappingInfo = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
            mappingInfo->Wrap( -1 );
            wxFont font(wxFontInfo(10).Family(wxFONTFAMILY_TELETYPE));
            mappingInfo->SetFont(font);
            sizer->Add(mappingInfo);

            wxBoxSizer* buttonsSizer;
            buttonsSizer = new wxBoxSizer( wxHORIZONTAL );
            buttonsSizer->Add( 0, 0, 1, wxEXPAND, 5 );
            wxButton* defaultButton = new wxButton( this, wxID_ANY, wxT("Default"), wxDefaultPosition, wxDefaultSize, 0 );
            buttonsSizer->Add( defaultButton);
            wxButton* cancelButton = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
            buttonsSizer->Add( cancelButton);
            wxButton* okButton = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0 );
            buttonsSizer->Add( okButton);
            sizer->Add( buttonsSizer, 1, wxEXPAND, 5 );

            inputWin = new wxWindow(this, wxID_ANY, wxDefaultPosition, wxSize(-1, -1), wxRAISED_BORDER);
            // connect event handlers for the blue input window
            inputWin->Bind(wxEVT_KEY_DOWN, &RawKeyboardFrame::OnKeyDown, this);
            inputWin->Bind(wxEVT_KEY_UP, &RawKeyboardFrame::OnKeyUp, this);
            sizer->Add(inputWin);

            SetSizerAndFit(sizer);
            CentreOnScreen();

            keyboardBitmap->Bind(wxEVT_LEFT_DOWN, &RawKeyboardFrame::OnLeftDown, this);
            keyboardBitmap->Bind(wxEVT_LEFT_UP, &RawKeyboardFrame::OnLeftUp, this);
            defaultButton->Bind(wxEVT_BUTTON, &RawKeyboardFrame::OnDefault, this);
            cancelButton->Bind(wxEVT_BUTTON, &RawKeyboardFrame::OnCancel, this);
            okButton->Bind(wxEVT_BUTTON, &RawKeyboardFrame::OnOK, this);

            console.frame.RegisterCapsOff([this](){CapsLockOff();});

            cpckbdhitmap = new wxImage((console.rsrcDir / "cpckbdhitmap_x2.png").c_str()); 
            for (int i=0; i<128; i++) workMap[i] = console.prefs.keyConv[i];
            Show();
            inputWin->SetFocus();
        }
        ~RawKeyboardFrame() { console.frame.RegisterCapsOff(0);}

        int GetCpcKey(int x, int y) {
            int w = cpckbdhitmap->GetWidth();
            int h = cpckbdhitmap->GetHeight();
            if (x<0 || x>=w || y<0 || y>=h) return -1; // sanity check
            unsigned char grey = cpckbdhitmap->GetData()[3*(w*y+x)];
            if (grey<128 || grey >= 208) return -1; // 80 keys
            return grey-128;
        }
        int GetMacKey(int cpcKey) {
            for (int i=0; i<128; i++) // Find current mapping for this key
                if (workMap[i]==cpcKey)
                    return i;
            return -1;
        }

        void OnLeftDown(wxMouseEvent &event)
        {
            int cpcKey = GetCpcKey(event.GetX(), event.GetY());
            //if (cpcKey >= 0) 
            CpcKeyDown(cpcKey);
        }
        void OnLeftUp(wxMouseEvent &event)
        {
            int cpcKey = GetCpcKey(event.GetX(), event.GetY());
            if (cpcKey >= 0) CpcKeyUp(cpcKey);
        }

        void OnKeyDown(wxKeyEvent& event) { MacKeyDown(event.GetRawKeyCode()); event.Skip(); }
        void OnKeyUp(wxKeyEvent& event) {
            MacKeyUp(event.GetRawKeyCode());
            event.Skip();
        }
        void CapsLockOff() {MacKeyUp(57); }

        void CpcKeyDown(int cpcKey) { // click on a key
            if (crtCpcKey >= 0) { // deselect old...
                bool deselect = cpcKey == crtCpcKey;
                crtCpcKey = -1;          // deselect
                HighlightKey(crtCpcKey); // clear all
                if (deselect) return; // deselect only
            }
            if (cpcKey>=0) { // ... and select new
                crtCpcKey = cpcKey; // select
                HighlightKey(crtCpcKey);
                int macKey = GetMacKey(cpcKey);
                if (macKey >= 0)
                    mappingInfo->SetLabel(str(format("CPC key [%-8s] mapped to host key [%-8s]") % cpcKeyStr[crtCpcKey] % macKeyStr[macKey]));
                else
                    mappingInfo->SetLabel(str(format("CPC key [%-8s] not mapped") % cpcKeyStr[cpcKey]));
            } else {
                mappingInfo->SetLabel("");
            }
        }
        void CpcKeyUp(int cpcKey)
        {

        }
        void MacKeyDown(int macKey)
        {
            if (macKey == 0xFF) return;
            if (crtCpcKey>=0) { // assign
                if (macKey == GetMacKey(crtCpcKey)) { // same key -> deselect
                    crtCpcKey = -1;                   // deselect
                    HighlightKey(crtCpcKey);          // clear all
                    return;             // deselect only
                }
                // assign
                for (int i = 0; i < 128; i++)
                    if (workMap[i] == crtCpcKey) workMap[i] = -1;
                workMap[macKey] = crtCpcKey;
                mappingInfo->SetLabel(str(format("CPC key [%-8s] mapped to host key [%-8s]") % cpcKeyStr[crtCpcKey] % macKeyStr[macKey]));
            } else { // select
                int cpcKey = workMap[macKey];
                if (cpcKey != 0xFF) {
                    crtCpcKey = cpcKey; // select
                    HighlightKey(crtCpcKey);
                    mappingInfo->SetLabel(str(format("CPC key [%-8s] mapped to host key [%-8s]") % cpcKeyStr[crtCpcKey] % macKeyStr[macKey]));
                }
            }
        }
        void MacKeyUp(int macKey) {

        }
        void HighlightKey(int key) {
            int w = cpckbdhitmap->GetWidth();
            int h = cpckbdhitmap->GetHeight();
            unsigned char *map = cpckbdhitmap->GetData();
            unsigned char *pic = cpckbdImage->GetData();
            for (int y=0; y<h; y++)
                for (int x=0; x<w; x++)
                    if (map[3*(w*y+x)]==key+128)
                        pic[3*(w*y+x)] = 255;
                    else
                        pic[3*(w*y+x)] = pic[3*(w*y+x)+1];
            wxBitmap cpcKbd(*cpckbdImage);
            keyboardBitmap->SetBitmap(cpcKbd);
        }
        void OnDefault(wxEvent &event) {
            for (int i=0; i<128; i++) workMap[i] = console.rawKeyMapDefault[i];
        }
        void OnCancel(wxEvent &event) {
            Destroy();
        }
        void OnOK(wxEvent &event) {
            for (int i=0; i<128; i++) console.prefs.keyConv[i] = workMap[i];
            Destroy();
        }
        OsConsole &console;
        BYTE workMap[128];
        int crtCpcKey=-1;
        wxImage *cpckbdhitmap;
        wxImage *cpckbdImage;
        wxStaticBitmap *keyboardBitmap;
        wxStaticText* mappingInfo;
        wxWindow *inputWin=0;
    };
    RawKeyboardFrame *frame = new RawKeyboardFrame(console);
}
