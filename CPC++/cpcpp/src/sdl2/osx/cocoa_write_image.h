#pragma once
#include <vector>
#include <string>
void cocoa_set_image_as_icon(const std::vector<uint32_t> &image, const std::string &filename);
void cocoa_write_image(const std::vector<uint32_t> &image, const std::string &filename);
void cocoa_image_to_clipboard(const std::vector<uint32_t> &image);

