#include "cocoa_write_image.h"
#include "ga.h"
#include <Cocoa/Cocoa.h>

using namespace std;
// This is needed for Cocoa autoreleased objects
struct Pool {
    Pool() { pool = [ [ NSAutoreleasePool alloc ] init ];}
    ~Pool() { [pool release ]; }
    NSAutoreleasePool *pool;
};
NSBitmapImageRep *GetImageRep(const std::vector<uint32_t> &image)
{
    int nPix=image.size();
    bool small = (nPix==Ga::CPC_SCR_W*Ga::CPC_SCR_H/4);
    int width = Ga::CPC_SCR_W / (small? 2: 1);
    int height = Ga::CPC_SCR_H / (small? 2: 1);
    
    NSBitmapImageRep *imageRep = [ [ NSBitmapImageRep alloc]
                              initWithBitmapDataPlanes:NULL
                              pixelsWide:width pixelsHigh:height
                              bitsPerSample:8 samplesPerPixel:3 hasAlpha:NO isPlanar:NO
                              colorSpaceName:NSCalibratedRGBColorSpace
                              bytesPerRow:width*3 bitsPerPixel:24 ];
    
    /* Change from BRGA data to RGB data */
    unsigned char *rgb24_data = [imageRep bitmapData];
    for (int i=0; i<nPix; ++i)
    {
        const unsigned char *src = reinterpret_cast<const unsigned char *>(&image[i]);
        unsigned char *dst = rgb24_data + i * 3;
        *(dst+0) = src[2]; // red
        *(dst+1) = src[1]; // green
        *(dst+2) = src[0]; // blue
    }
    return imageRep;
}

void cocoa_write_image(const std::vector<uint32_t> &image, const std::string &filename)
{
    Pool pool;
    NSBitmapImageRep *imageRep = GetImageRep(image);
    NSData *data = [ imageRep representationUsingType:NSBitmapImageFileTypePNG properties:[NSDictionary dictionary] ]; // NSTIFFFileType NSGIFFileType NSJPEGFileType NSBMPFileType
    [ data writeToFile:[NSString stringWithUTF8String:filename.c_str()] atomically:NO ];
    [ imageRep release ];
}
void cocoa_image_to_clipboard(const std::vector<uint32_t> &image)
{    
    Pool pool;
    NSBitmapImageRep *imageRep = GetImageRep(image);
    NSPasteboard *pb = [NSPasteboard generalPasteboard];
    // Telling the pasteboard what type of data we're going to send in
    [pb declareTypes:[NSArray arrayWithObjects:NSPasteboardTypePNG,nil] owner:nil];
    // Converting the representation to PNG and sending it to the pasteboard (with type indicated)
    [pb setData:[imageRep representationUsingType:NSBitmapImageFileTypePNG properties:[NSDictionary dictionary]] forType:NSPasteboardTypePNG];

    [ imageRep release ];
}
NSBitmapImageRep *GetImageRep2(const std::vector<uint32_t> &image)
{
    int nPix=image.size();
    int width = Ga::CPC_SCR_W;
    NSBitmapImageRep *imageRep = [ [ NSBitmapImageRep alloc]
                              initWithBitmapDataPlanes:NULL
                              pixelsWide:width pixelsHigh:width
                              bitsPerSample:8 samplesPerPixel:4 hasAlpha:YES isPlanar:NO
                              colorSpaceName:NSCalibratedRGBColorSpace
                              bytesPerRow:width*4 bitsPerPixel:32 ];
    
    /* Copy image data, converting from BGRA to RGBA  */
    unsigned char *rgb32_data = [imageRep bitmapData];
    for (int i=0; i<nPix; ++i)
    {
        const unsigned char *src = reinterpret_cast<const unsigned char *>(&image[i]);
        unsigned char *dst = rgb32_data + i * 4;
        *(dst+0) = src[2]; // red
        *(dst+1) = src[1]; // green
        *(dst+2) = src[0]; // blue
        *(dst+3) = src[3]; // alpha
    }
    return imageRep;
}
void cocoa_set_image_as_icon(const std::vector<uint32_t> &image, const std::string &filename)
{
    Pool pool;
    NSBitmapImageRep *imageRep = GetImageRep2(image);
    NSImage *nsImage = [[NSImage alloc] initWithSize:[imageRep size]];
    [nsImage addRepresentation:imageRep];
    
    [[NSWorkspace sharedWorkspace] setIcon:nsImage forFile:[NSString stringWithUTF8String:filename.c_str()]  options:0];
    
    [nsImage release];
    [ imageRep release ];
}
