#import <Cocoa/Cocoa.h>

void *GetWindowForView(void *pWindowHandle)
{
    NSView *view = (NSView *)pWindowHandle;
    NSWindow *window = (NSWindow *)[view window];
    return (void *)window;
}
