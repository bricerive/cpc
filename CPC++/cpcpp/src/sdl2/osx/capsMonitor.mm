#import <Cocoa/Cocoa.h>
#import "IOKit/hid/IOHIDManager.h"

CFMutableDictionaryRef myCreateDeviceMatchingDictionary(UInt32 usagePage, UInt32 usage)
 {
    CFMutableDictionaryRef ret = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    if (!ret)
        return NULL;
    CFNumberRef pageNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &usagePage );
    if (!pageNumberRef) {
        CFRelease(ret);
        return NULL;
    }
    CFDictionarySetValue(ret, CFSTR(kIOHIDDeviceUsagePageKey), pageNumberRef);
    CFRelease(pageNumberRef);
    CFNumberRef usageNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &usage);
    if (!usageNumberRef) {
        CFRelease(ret);
        return NULL;
    }
    CFDictionarySetValue(ret, CFSTR(kIOHIDDeviceUsageKey), usageNumberRef);
    CFRelease(usageNumberRef);
    return ret;
}

static void (*gCb)(void *, uint32_t, bool);
static void *gParam;

void myHIDKeyboardCallback(void *context, IOReturn result, void *sender, IOHIDValueRef value) {
    IOHIDElementRef elem = IOHIDValueGetElement(value);
    if (IOHIDElementGetUsagePage(elem) != 0x07)
        return;
    // Note that the callback seems to be called multiple times per press or release but with usage IDs outside the normal range, which is what the "if (scancode < 4 || scancode > 231)" is for.
    uint32_t scancode = IOHIDElementGetUsage(elem);
    if (scancode < 4 || scancode > 231) return;
    // we are only interested in CAPS Lock
    if (scancode != 57) return;
    bool pressed = IOHIDValueGetIntegerValue(value);
    (*gCb)(gParam, scancode, pressed);
}

void RegisterCapsMonitor(void (*cb)(void *, uint32_t, bool), void *param)
{
    gCb = cb;
    gParam = param;
    IOHIDManagerRef hidManager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
    CFMutableDictionaryRef keyboard = myCreateDeviceMatchingDictionary(0x01, 6);
    CFMutableDictionaryRef keypad = myCreateDeviceMatchingDictionary(0x01, 7);
    CFMutableDictionaryRef matchesList[] = { keyboard, keypad };
    CFArrayRef matches = CFArrayCreate(kCFAllocatorDefault, (const void **)matchesList, 2, NULL);
    IOHIDManagerSetDeviceMatchingMultiple(hidManager, matches);
    IOHIDManagerRegisterInputValueCallback(hidManager, myHIDKeyboardCallback, NULL);
    IOHIDManagerScheduleWithRunLoop(hidManager, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
    IOHIDManagerOpen(hidManager, kIOHIDOptionsTypeNone); 
}
