// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2020 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// cpcpp_app.cpp
#include "cpcpp_app.h"

#include "log.h"
#include "sdl_init.h"
#include <wx/cmdline.h>
#include <wx/stdpaths.h>
#include <wx/sysopt.h>
#include <wx/xrc/xmlres.h>

using namespace std;

// stripped down wxWidget startup
int main(int argc, char **argv)
{
    LOG_STATUS("Entering main");
    string logFilePath = OsConsole::ApplicationFolder()+"_log.txt";
    ofstream output(logFilePath.c_str(), ofstream::trunc);
    infra::Log::AddStreamBuf(output.rdbuf());

    SdlInit sdlInitFirst;
   
    wxAppConsole::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE, "CPC++"); // check for library version mismatch

    new CpcppApp(ArgsVec(&argv[0], &argv[argc])); // will be deleted by wx
    LOG_STATUS("Entering wxEntry");
    // return wxEntry(argc, argv);
    return wxEntry(argc = 0, (char **) 0);
}

bool CpcppApp::OnInit()
{
    LOG_STATUS("Entering OnInit");
    if (!wxApp::OnInit()) return false;

    // stop all windows getting idle events
    wxIdleEvent::SetMode(wxIDLE_PROCESS_SPECIFIED);
    
    wxXmlResource::Get()->InitAllHandlers();
    wxXmlResource::Get()->Load(XrcFilePath());
    wxImage::AddHandler(new wxPNGHandler);

    char **argvC = argv;
    frame = new CpcppFrame(args); // wx will delete it (dtor called by wxAppBase::CleanUp())
    
    return true;
}

void CpcppApp::MacOpenFiles(const wxArrayString &fileNames)
{
    for (unsigned int i = 0; i != fileNames.GetCount(); i++)
        LOG_STATUS("OpenFiles: %s", (const char *)fileNames[i].c_str());
    if (frame) frame->OpenFiles(fileNames);
}

wxString CpcppApp::XrcFilePath()
{
    wxFileName rsrcPath(RsrcPath());
    rsrcPath.SetName(wxT("cpcpp.xrc"));
    return rsrcPath.GetFullPath();
}

// get the directory path of the app (with trailing separator)
// so we can easily add on another path or filename
wxFileName CpcppApp::RsrcPath()
{
    wxFileName path(wxStandardPaths::Get().GetExecutablePath());
    // on mac the files are stored within the resources directory
#if defined(__WXMAC__)
    path.Normalize(wxPATH_NORM_ENV_VARS);
    path.RemoveLastDir();
    path.AppendDir(wxT("Resources"));
#endif
    return path;
}
