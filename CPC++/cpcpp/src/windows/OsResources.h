#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// \file OsResources.h - Gui resources mapping - Windows

#include "resource.h"

/// Menu items mapping from CPC++ to Windows
static const struct {
    CpcCmd cmd;
    int id;
} cmdMap[] = {
    {CpcCmd::kCmdSaveSnapshot   ,ID_FILE_SAVESNAPSHOT},
    {CpcCmd::kCmdQuickLoad      ,ID_FILE_QUICKLOAD},
    {CpcCmd::kCmdQuickWrite     ,ID_FILE_QUICKWRITE},
    {CpcCmd::kCmdQuit           ,ID_FILE_QUIT},
    {CpcCmd::kCmdOpenAny        ,ID_FILE_OPEN_ANY},
    {CpcCmd::kCmdPause          ,ID_CONTROL_HOLD},
    {CpcCmd::kCmdReset          ,ID_CONTROL_RESET},
    {CpcCmd::kCmdRoms           ,ID_CONFIG_EXPANSIONROMS},
    {CpcCmd::kCmdLoadA          ,ID_CONFIG_DRIVEA_SELECTIMAGE},
    {CpcCmd::kCmdSaveA          ,ID_CONFIG_DRIVEA_SAVEIMAGE},
    {CpcCmd::kCmdSaveAsA        ,ID_CONFIG_DRIVEA_SAVEIMAGEAS},
    {CpcCmd::kCmdFlipA          ,ID_CONFIG_DRIVEA_FLIP},
    {CpcCmd::kCmdEjectA         ,ID_CONFIG_DRIVEA_EJECT},
    {CpcCmd::kCmdLoadB          ,ID_CONFIG_DRIVEB_SELECTIMAGE},
    {CpcCmd::kCmdSaveB          ,ID_CONFIG_DRIVEB_SAVEIMAGE},
    {CpcCmd::kCmdSaveAsB        ,ID_CONFIG_DRIVEB_SAVEIMAGEAS},
    {CpcCmd::kCmdFlipB          ,ID_CONFIG_DRIVEB_FLIP},
    {CpcCmd::kCmdEjectB         ,ID_CONFIG_DRIVEB_EJECT},
    {CpcCmd::kCmdSoundOn        ,ID_CONFIG_SOUND_ENABLED},
    {CpcCmd::kCmdStereoOn       ,ID_CONFIG_SOUND_STEREO},
    {CpcCmd::kCmdChannelA       ,ID_CONFIG_SOUND_CHANNELA},
    {CpcCmd::kCmdChannelB       ,ID_CONFIG_SOUND_CHANNELB},
    {CpcCmd::kCmdChannelC       ,ID_CONFIG_SOUND_CHANNELC},
    {CpcCmd::kCmdVolume0        ,ID_CONFIG_SOUND_VOLUME_0},
    {CpcCmd::kCmdVolume1        ,ID_CONFIG_SOUND_VOLUME_1},
    {CpcCmd::kCmdVolume2        ,ID_CONFIG_SOUND_VOLUME_2},
    {CpcCmd::kCmdVolume3        ,ID_CONFIG_SOUND_VOLUME_3},
    {CpcCmd::kCmdVolume4        ,ID_CONFIG_SOUND_VOLUME_4},
    {CpcCmd::kCmdVolume5        ,ID_CONFIG_SOUND_VOLUME_5},
    {CpcCmd::kCmdVolume6        ,ID_CONFIG_SOUND_VOLUME_6},
    {CpcCmd::kCmdVolume7        ,ID_CONFIG_SOUND_VOLUME_7},
    {CpcCmd::kCmdMonochrome     ,ID_MONITOR_MONOCHROME},
    {CpcCmd::kCmd6128_512       ,ID_CONFIG_MODEL_CPC6128_512},
    {CpcCmd::kCmd6128           ,ID_CONFIG_MODEL_CPC6128},
    {CpcCmd::kCmd664            ,ID_CONFIG_MODEL_CPC664},
    {CpcCmd::kCmd464            ,ID_CONFIG_MODEL_CPC464},
    {CpcCmd::kCmdCpmBoot        ,ID_CONFIG_MODEL_CPMBOOT},
    {CpcCmd::kCmdIsp            ,ID_CONFIG_MODEL_CONSTRUCTOR_ISP},
    {CpcCmd::kCmdTriumph        ,ID_CONFIG_MODEL_CONSTRUCTOR_TRIUMPH},
    {CpcCmd::kCmdSaisho         ,ID_CONFIG_MODEL_CONSTRUCTOR_SAISHO},
    {CpcCmd::kCmdSolavox        ,ID_CONFIG_MODEL_CONSTRUCTOR_SOLAVOX},
    {CpcCmd::kCmdAwa            ,ID_CONFIG_MODEL_CONSTRUCTOR_AWA},
    {CpcCmd::kCmdSchneider      ,ID_CONFIG_MODEL_CONSTRUCTOR_SCHNEIDER},
    {CpcCmd::kCmdOrion          ,ID_CONFIG_MODEL_CONSTRUCTOR_ORION},
    {CpcCmd::kCmdAmstrad        ,ID_CONFIG_MODEL_CONSTRUCTOR_AMSTRAD},
    {CpcCmd::kCmdAscii          ,ID_CONFIG_KEYBOARD_ASCII},
    {CpcCmd::kCmdRaw            ,ID_CONFIG_KEYBOARD_RAW},
    {CpcCmd::kCmdDefineRaw      ,ID_CONFIG_KEYBOARD_DEFINERAW},
    {CpcCmd::kCmdPrinterNone    ,ID_CONFIG_PRINTER_NONE},
    {CpcCmd::kCmdPrinterFile    ,ID_CONFIG_PRINTER_FILE},
    {CpcCmd::kCmdDigiblaster    ,ID_CONFIG_PRINTER_DIGIBLASTER},
    {CpcCmd::kCmdHalfSize       ,ID_DISPLAY_HALFSIZE},
    {CpcCmd::kCmdFullScreen     ,ID_DISPLAY_FULLSCREEN},
    {CpcCmd::kCmdSkip           ,ID_DISPLAY_SKIPODDLINES},
    {CpcCmd::kCmdIndicators     ,ID_DISPLAY_INDICATORBAR},
    {CpcCmd::kCmdSaveScreen     ,ID_DISPLAY_SAVETOFILE},
    {CpcCmd::kCmdCopyScreen     ,ID_DISPLAY_COPYTOCLIPBOARD},
    {CpcCmd::kCmdFRate1         ,ID_DISPLAY_FRAMERATE_11},
    {CpcCmd::kCmdFRate2         ,ID_DISPLAY_FRAMERATE_12},
    {CpcCmd::kCmdFRate4         ,ID_DISPLAY_FRAMERATE_14},
    {CpcCmd::kCmdFRate8         ,ID_DISPLAY_FRAMERATE_18},
    {CpcCmd::kCmdStartup        ,ID_OPTIONS_STARTUPOPTIONS},
    {CpcCmd::kCmdForget         ,ID_OPTIONS_FORGETREMEMBEREDOPTIONS},
    {CpcCmd::kCmdDebugger       ,ID_OPTIONS_DEBUGGER},
    {CpcCmd::kCmdSpeedNone      ,ID_OPTIONS_SPEEDLIMIT_NONE},
    {CpcCmd::kCmdSpeed25        ,ID_OPTIONS_SPEEDLIMIT_CPC25},
    {CpcCmd::kCmdSpeed50        ,ID_OPTIONS_SPEEDLIMIT_CPC50},
    {CpcCmd::kCmdSpeed75        ,ID_OPTIONS_SPEEDLIMIT_CPC75},
    {CpcCmd::kCmdSpeed100       ,ID_OPTIONS_SPEEDLIMIT_CPC100},
    {CpcCmd::kCmdSpeed125       ,ID_OPTIONS_SPEEDLIMIT_CPC125},
    {CpcCmd::kCmdSpeed150       ,ID_OPTIONS_SPEEDLIMIT_CPC150},
    {CpcCmd::kCmdSpeed175       ,ID_OPTIONS_SPEEDLIMIT_CPC175},
    {CpcCmd::kCmdSpeed200       ,ID_OPTIONS_SPEEDLIMIT_CPC200},
    {CpcCmd::kCmdRealTimeFdc    ,ID_OPTIONS_EMULATIONOPTIONS_REALTIMEFDC},
    {CpcCmd::kCmdAbout          ,ID_HELP_ABOUTCPC},
    {CpcCmd::kCmdWebUrl         ,ID_HELP_VISITCPCWEBSITE},
    {CpcCmd::kCmdMailUrl        ,ID_HELP_SENDEMAILTOAUTHOR},
    {CpcCmd::kCmdEndOfList      ,0}
};

