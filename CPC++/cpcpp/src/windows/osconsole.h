#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file osconsole.h - CPC++ console under Windows

#include "cpcconfig.h" // For Rom struct definition
#include "cpcguiconsole.h"
#define DIRECTINPUT_VERSION 0x0300
#include <dinput.h>
#include <ddraw.h>
#include <dsound.h>
#include "osgui.h"
#include "monitor.h"

/// CPC++ console under Windows
class OsConsole:public CpcGuiConsole, public OsGui, public MonitorConsole {

public:
    explicit OsConsole(const ArgsVec &argsVec);
    virtual ~OsConsole();
    virtual void ConnectCpc();

    // CpcConsole implementation
    virtual void FrameReady();
    virtual void DiskActivity(int which, DriveState state, int track);
    virtual void PutPrinter(BYTE val);
    void MonitorTrap()override;
    virtual void SoundBuffDone();
    bool CpcHeld()const;

    // MonitorConsole implementation
    virtual bool GetHexValue(const char* prompt, int& val);
    void DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char* str)const override;
    virtual const RAW32* GetP32() { return P32(); }

    // CpcGuiConsole implementation
    virtual void CheckEvents();
    virtual void CheckKbd();
    virtual void Update();
    virtual void SpeedReport(UINT32 expected, UINT32 executed, UINT32 elapsed);
    virtual void StartSound();
    virtual void StopSound();
    virtual void WaitForSound(bool val) {}
    virtual void OsVol(const CpcSoundLevel& val);
    unsigned long TicksMs() override;

    static void SetHInstance(HINSTANCE hInstance_) {hInstance=hInstance_;}
    static void SetCmdShow(int cmdShow_) {cmdShow=cmdShow_;}
    static HINSTANCE GetHInstance() {return hInstance;} // Only for OsPsg
    static HWND GetHWnd() {return theConsole->mainWin;} // Only for OsPsg

    void PutError(const std::string& str);
    std::string GetStr(const infra::MsgI18n& msgId) override;
    bool SelectFile(std::string& path, FileType& type, int& option);
    void SleepUs(UINT32 us) override;
    unsigned long GetCrtUs() override;

protected:
    virtual std::string GetClipboardString();
    virtual bool CanSound()const;
    virtual bool CanStereo()const;

    virtual void Put(const std::string &str);
    virtual bool PutConfirm(const std::string &str);
    virtual void CmdChecked(CpcCmd cmd, bool checked);
    virtual void CmdEnabled(CpcCmd cmd, bool enabled);
    virtual void WindowTitle(const std::string &title);
    virtual void LaunchUrl(const std::string &url);
    virtual void PlayCpcSound(CpcSound sound);
    virtual void EmptyRunMenus();
    virtual void AddItemToRunMenu(CpcBase::DriveId drive, const std::string &item, bool enabled);
    virtual void SaveSnapshot(const std::string &path, const BYTE * buff);
    virtual bool GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path);
    virtual void SetScreenSmall(bool val);
    virtual void SetScreenFull(bool val);
    virtual void SetScreenSkip(bool val);
    virtual void SetIndicators(bool val);
    virtual void CopyToClipboard(void);
    virtual void SavePICTFile();
    virtual bool ConfirmDlg(const infra::MsgI18n& msgId);
    virtual void DoAboutDialog();
    virtual void DoRomDialog();
    virtual void DoKeyboardDialog();
    virtual void DoStartupDialog();
    virtual void CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size);
    virtual bool FilterFile(const std::string &path, FileType type);
    virtual void SetFileType(const std::string &path, FileType type);
    virtual void SetIcon(const std::string& path);


    virtual void MonitToggle();
    virtual void MonitOff();

    virtual void PrintSetup();
    virtual void Print();

 private:
    HWND mainWin; // Our window
    HACCEL hAccelTable;

    typedef bool (*TypeFilter)(const std::string &, const BYTE *);
    bool FilterFileWithFilter(const std::string &path, TypeFilter filter)const;
    int CmdToId(CpcCmd cmd)const;
    static void CheckCompressedS(const std::string &name, FileType type, int index, BYTE *&data, int &size);

    static void PrinterGlue(void *param, BYTE val);

    // The offscreen buffer
    RAW32 *offScreenPt;
    uint32_t offscreenBPR;
    BITMAPINFO *bmi;

    bool RegisterWindowClass(HINSTANCE theHInstance, WNDPROC windProc, LPCSTR menuId, const char *name)const;

    LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    static LRESULT APIENTRY WndProcGlue(HWND hWndFrame, UINT wMsg, WPARAM wParam, LPARAM lParam);

    LRESULT OnCommand(WPARAM wParam, LPARAM lParam);
    long OnPaint(WPARAM wParam, LPARAM lParam);
    long OnDropFiles(WPARAM wParam, LPARAM lParam);

    bool GetPathToSave(char *&path);

    static HINSTANCE hInstance;
    static int cmdShow;
    static OsConsole *theConsole;
    friend static std::string ErrorMapper(int errId);

    char lastKeyboardData[256];

    bool startupFlag;

    bool SaveConfig()const;
    void LoadConfig();

    void RedrawFrame();

    static const int NB_DRIVES=2;
    HMENU runMenuDrive[NB_DRIVES];
    int runMenuItems[NB_DRIVES];

    // Dialogs
    HWND currentDialog;

    // About dialog
    static INT_PTR CALLBACK AboutProcGlue(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
    bool AboutProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam);
    void PrepareAboutText(HWND dialog);
    void ScrollText(HWND dialog);
    static VOID CALLBACK TimerProc(_In_  HWND hwnd,_In_  UINT uMsg,_In_  UINT_PTR idEvent,_In_  DWORD dwTime    );

    // Amstrad Font
    HBITMAP fontHBitmap;
    BITMAP fontBitmap;


    // Expansion ROMs selection
    static void RomSelectDialogSet(HWND dialog, const RomConfig *roms);
    static INT_PTR CALLBACK RomSelectProcGlue(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
    bool RomSelectProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam);

    // Raw KBD mapping definition
    void KbdDefine();
    static INT_PTR CALLBACK KbdDefineProcGlue(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
    bool KbdDefineProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam);

    static std::vector<BYTE> rawKeyMapDefault;

    /// store the part of the config specific to the OsConsole (preferences)
    CPC_CONFIG_STRUCT(Confirms, (std::string, id)(bool, val));
    CPC_CONFIG_STRUCT(PrefsBase,(std::vector<BYTE>, keyConv)(std::vector<Confirms>, confirms));
    struct Prefs : public PrefsBase {
        Prefs() : PrefsBase(rawKeyMapDefault, std::vector<Confirms>()) {}
    };

    // Preference attributes
    Prefs prefs;

    // Direct X
    void dxCheck()const;
    // Direct input
    LPDIRECTINPUT directInput;
    LPDIRECTINPUTDEVICE keyboardDevice;

    OsConsole &operator=(const OsConsole &rhs);
    OsConsole(const OsConsole &rhs); //lint !e1704

    static void UpsideDown(BITMAP &bitmap);

    Monitor monit;
    HWND monitWin;
    bool monitVisible;
    static LRESULT APIENTRY MonitWndProcGlue(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam);
    LRESULT MonitWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    void MonitUpdate();


    bool waitForSound;
    bool audioOutput;

    UINT8 *psgBuff;
    int psgBuffSize;

    // Direct sound stuff
    void DsInit();
    void DsClearBuff();
    LPDIRECTSOUND dsInstance;
    LPDIRECTSOUNDBUFFER dsBuffer;
    WAVEFORMATEX dsBufferFormat;
    long dsBuffSize;

    long writeCursor;

};
