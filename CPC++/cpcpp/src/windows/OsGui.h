#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// \file psgui.h - Gui support - Windows
#include <Windows.h>

class OsGui {
public:
    virtual ~OsGui() {}
    void SetFrame(HWND frame_) {frame=frame_;}
    void AddItemToMenu(HMENU menu, char *str, int id)const;
    void MenuItemChecked(int item, bool onOff);
    void MenuItemEnabled(int item, bool onOff);
private:
    void MenuItemFlag(int item, int flag, bool onOff);
    HWND frame;
};
