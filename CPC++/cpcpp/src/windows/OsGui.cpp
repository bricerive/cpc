// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// OsGui.cpp - Gui support - Windows
#include "osgui.h"

void OsGui::MenuItemChecked(int item, bool onOff)
{
    MenuItemFlag(item, MFS_CHECKED, onOff);
}

void OsGui::MenuItemEnabled(int item, bool onOff)
{
    MenuItemFlag(item, MFS_GRAYED, !onOff);
}

void OsGui::MenuItemFlag(int item, int flag, bool onOff)
{
    HMENU menu = GetMenu(frame);
    MENUITEMINFO menuItemInfo;
    menuItemInfo.cbSize = sizeof(MENUITEMINFO);
    menuItemInfo.fMask = MIIM_STATE;
    if (!GetMenuItemInfo(menu,item,FALSE,&menuItemInfo)) return;
    menuItemInfo.fState = (menuItemInfo.fState & ~flag) | (onOff? flag: 0);
    SetMenuItemInfo(menu,item,FALSE,&menuItemInfo);
}

void OsGui::AddItemToMenu(HMENU menu, char *str, int id)const
{
    MENUITEMINFO menuItemInfo;
    int nbItems = GetMenuItemCount(menu);

    menuItemInfo.cbSize = sizeof(menuItemInfo);
    menuItemInfo.fMask = MIIM_DATA | MIIM_ID | MIIM_TYPE;
    menuItemInfo.fType = str[0]? MFT_STRING: MFT_SEPARATOR;
    menuItemInfo.fState = MFS_DEFAULT;
    menuItemInfo.wID = id;
    menuItemInfo.hSubMenu = NULL;
    menuItemInfo.hbmpChecked = NULL;
    menuItemInfo.hbmpUnchecked = NULL;
    menuItemInfo.dwItemData = 0;
    menuItemInfo.dwTypeData = str;
    menuItemInfo.cch = static_cast<int>(strlen(str));

    InsertMenuItem(menu, nbItems, TRUE, &menuItemInfo);
}

