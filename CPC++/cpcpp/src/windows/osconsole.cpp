// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file OsConsole.cpp - CPC console under Win32
#include "OsConsole.h"
#include "error.h"
#include "log.h"
#include "console_shared.h"
#include "osresources.h"
#include "cpc.h"
#include "ga.h"
#include "cpcConfig.h"
#include "amstrad_text.h"
#include <stdio.h>
#include "unzip.h"
#include "version.h"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>

#include <algorithm>
#include <fstream>
#include <sstream>
#include <windows.h>

using namespace std;

//lint -e1924  C-style cast are all over Win32 programming
#define CPCPP_CLASS_NAME "CPC++"
#define CPCPP_MONIT_CLASS_NAME "CPC++ Monitor"

#define RM_BASEID 300

// sound
static const int nbChannels = 2;        //1;
static const int sampleRate = 22050;    //44100;    //22050;    //;;44100;
static const int sampleSize = 8;    //8;    //16;
static const int bytesPerSample = nbChannels * (sampleSize / 8);
static const int buffNbMs = 50;
static const double buffTime = static_cast<double>(buffNbMs) / 1000;
static const int buffSamples = (sampleRate * buffNbMs) / 1000;

// a few defines
#define DEFTITLE "CPC++"        // The title bar text
#define DSK_EXTENSION "DSK"
#define SNA_EXTENSION "SNA"
#define ROM_EXTENSION "ROM"
#define CREATE_WIDTH  Ga::CPC_SCR_W
#define CREATE_HEIGHT Ga::CPC_SCR_H

#include "cpckeystr.h"
#include "pckeystr.h"
#include "pc2cpckeymap.h"


I18N_GROUP(OsConsoleMsgId,
(ERR_CON_BADPREF, "Invalid preference in registry...creating a new one.")
(ERR_CON_PRNTOPEN, "Cannot open printer output file.")
(MSG_CON_OPEN_TITLE_DSK, "Open a DSK (disk image) file")
(MSG_CON_OPEN_PROMPT_DSK, "Select a Disk image for drive %c")
(MSG_CON_OPEN_TITLE_SNA, "Open a SNA (snapshot) file")
(MSG_CON_OPEN_PROMPT_SNA, "Select a snapshot file")
(MSG_CON_OPEN_TITLE_ROM, "Open a ROM (expansion rom) file")
(MSG_CON_OPEN_PROMPT_ROM, "Select a ROM file for rom-slot %d")
(MSG_CON_OPEN_TITLE_ANY, "Open SNA/DSK")
(MSG_CON_OPEN_PROMPT_ANY, "Select a snapshot file or a disk image file"));
static const char* ErrMsg[] =
{
	"Are you sure you want to quit CPC++ (unsaved changes will be lost) ?",
	"Cannot load snapshot %s.",
	"SNA file dump failed (%s).",
	"Changing the model requires a reset of the virtual CPC.",
	"Disk image to save.",
	"Untitled.sna",
	"Untitled.dsk",
	"Untitled.jpg",
	"Snapshot file to save.",
	"Picture file to save."
	"No disk in drive",
	"Cannot insert new floppy"

};

HINSTANCE OsConsole::hInstance = 0;
int OsConsole::cmdShow = 0;
OsConsole* OsConsole::theConsole;

// OsConsole constructor
// When this is called, hInstance and cmdShow are already set as static
OsConsole::OsConsole(const ArgsVec& argsVec)
	: CpcGuiConsole(argsVec)
	, mainWin(0)
	, hAccelTable(0)
	, offScreenPt(0)
	, offscreenBPR(0)
	, bmi(0)
	, startupFlag(true)
	, currentDialog(0)
	, directInput(0)
	, keyboardDevice(0)
	, monit(*this)
	, monitWin(0)
	, monitVisible(false)
	, waitForSound(false), audioOutput(true), psgBuff(0), psgBuffSize(0), dsInstance(0), dsBuffer(0), dsBuffSize(0), writeCursor(0)
{
	theConsole = this;

	dxCheck();

	// Define and register the window class
	RegisterWindowClass(hInstance, WndProcGlue, (LPCSTR)IDR_CPCPP_MENU, CPCPP_CLASS_NAME);

	// create main window
	long style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
	RECT clientRect = { 0,0,CREATE_WIDTH,CREATE_HEIGHT };
	AdjustWindowRect(&clientRect, style, TRUE);
	mainWin = CreateWindow(CPCPP_CLASS_NAME, DEFTITLE, style, CW_USEDEFAULT, 0, clientRect.right - clientRect.left, clientRect.bottom - clientRect.top, 0, 0, hInstance, 0);
	ASSERT_THROW(mainWin);
	SetWindowLongPtr(mainWin, GWLP_USERDATA, (LONG_PTR)this); // Hook up our object in the window for the WndProc glue
	SetFrame(mainWin);

	// Register for files drag'n drop
	LONG_PTR windowLong;
	windowLong = GetWindowLongPtr(mainWin, GWL_EXSTYLE);
	windowLong |= WS_EX_ACCEPTFILES;
	SetWindowLongPtr(mainWin, GWL_EXSTYLE, windowLong);
	DragAcceptFiles(mainWin, TRUE);

	// create monit window
	RegisterWindowClass(hInstance, MonitWndProcGlue, 0, CPCPP_MONIT_CLASS_NAME);
	CpcRect monitRect = monit.WinRect();
	RECT monitRect2 = { monitRect.Left(), monitRect.Top(), monitRect.Right(), monitRect.Bottom() };
	long monitWindowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
	AdjustWindowRect(&monitRect2, monitWindowStyle, FALSE);
	monitWin = CreateWindow(CPCPP_MONIT_CLASS_NAME, "CPC++ Monitor", monitWindowStyle, CW_USEDEFAULT, 0, monitRect2.right - monitRect2.left, monitRect2.bottom - monitRect2.top, 0, 0, hInstance, 0);
	ASSERT_THROW(monitWin);
	ShowWindow(monitWin, SW_HIDE);
	SetWindowLongPtr(monitWin, GWLP_USERDATA, (LONG_PTR)this);

	// Create off-screen buffer
	bmi = reinterpret_cast<BITMAPINFO*>(malloc(sizeof(BITMAPINFO) + 4 * Ga::CPC_SCR_W * Ga::CPC_SCR_H));
	offScreenPt = reinterpret_cast<RAW32*>(bmi + 1);
	offscreenBPR = 4 * Ga::CPC_SCR_W;
	memset(bmi, 0, sizeof(BITMAPINFO)); //lint -e668
	bmi->bmiHeader.biSize = sizeof(bmi->bmiHeader);
	bmi->bmiHeader.biWidth = Ga::CPC_SCR_W;
	bmi->bmiHeader.biHeight = -Ga::CPC_SCR_H;   // must be positive if we want a bottom-up DIB with origin in the lower-left
	bmi->bmiHeader.biPlanes = 1;
	bmi->bmiHeader.biBitCount = 32;
	bmi->bmiHeader.biCompression = BI_RGB;
	bmi->bmiHeader.biSizeImage = 0;
	bmi->bmiHeader.biXPelsPerMeter = 0;
	bmi->bmiHeader.biYPelsPerMeter = 0;
	bmi->bmiHeader.biClrUsed = 0;
	bmi->bmiHeader.biClrImportant = 0;

	// Show the window
	ShowWindow(mainWin, cmdShow);
	UpdateWindow(mainWin);

	// Init the direct input access for keyboard
	ASSERT_THROW(DirectInputCreate(hInstance, DIRECTINPUT_VERSION, &directInput, 0) == DI_OK);
	// create keyboard device
	ASSERT_THROW(directInput->CreateDevice(GUID_SysKeyboard, &keyboardDevice, 0) == DI_OK);
	// set data format
	ASSERT_THROW(keyboardDevice->SetDataFormat(&c_dfDIKeyboard) == DI_OK);
	// acquire it
	ASSERT_THROW(keyboardDevice->Acquire() == DI_OK);

	// load accelerators
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR));

	LoadConfig();

	CmdEnabled(CpcCmd::kCmdHalfSize, false);
	CmdEnabled(CpcCmd::kCmdFullScreen, false);
	CmdEnabled(CpcCmd::kCmdIndicators, false);

	SetCursor(LoadCursor(0, IDC_ARROW));

	// Load the amstrad font image: 768x8 black on white
	// this loads the image upside-down
	fontHBitmap = (HBITMAP)LoadImage(hInstance, MAKEINTRESOURCE(IDB_FONT), IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	GetObject(fontHBitmap, sizeof(BITMAP), &fontBitmap);
	UpsideDown(fontBitmap);

	// Get DS buffer and size
	DsInit();

	// Setup interface for Psg
	psgBuffSize = buffSamples * bytesPerSample;
	psgBuff = new UINT8[psgBuffSize];
}

void OsConsole::UpsideDown(BITMAP& bitmap)
{
	RAW32* row0 = (RAW32*)bitmap.bmBits;
	int nPixPerRow = bitmap.bmWidthBytes / 4;
	int height = bitmap.bmHeight;
	for (int i = 0; i < height / 2; i++)
	{
		RAW32* r0 = row0 + i * nPixPerRow;
		RAW32* r1 = row0 + (height - 1 - i) * nPixPerRow;
		for (int j = 0; j < nPixPerRow; j++)
			swap(*r0++, *r1++);
	}
}

void OsConsole::ConnectCpc()
{
	// Setup interface for Psg
	cpc.SetSoundInfo(CpcBase::SoundInfo(CanStereo(), 1, sampleRate, psgBuff, buffSamples));

	CpcGuiConsole::ConnectCpc();
	monit.SetCpc(cpc);
}

OsConsole::~OsConsole()
{
	dsBuffer->Stop();
	dsBuffer->Release();
	dsInstance->Release();

	// release direct input on keyboard
	if (keyboardDevice != 0) {
		// unacquire device
		// release it
		keyboardDevice->Release();
	}
	if (directInput != 0)
		directInput->Release();
	free(bmi);
	// Accelerator tables loaded from resources are freed automatically when the application terminates.
	hAccelTable = 0;
	SaveConfig();
	currentDialog = 0;
	mainWin = 0;
	DeleteObject(fontHBitmap);
}

//------------------------------------------------------------------------------------
// OsConsole interface implementation
//------------------------------------------------------------------------------------

// Called by cpc emulation when it wants to
void OsConsole::CheckEvents()
{
	if (startupFlag) {
		startupFlag = 0;
		DoStartupEvents();
	}

	//SetRunMenus();
	// Main message loop:
	MSG msg;
	while ((currentDialog != 0 || IsPaused()) ? GetMessage(&msg, 0, 0, 0) : PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
		if (msg.message != WM_MOUSEMOVE)
			//if (msg.message == WM_LBUTTONDOWN)
			Suspend();
		if (TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			continue;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		Resume();
	}
	CheckKbd();
}


// Called by emulation when it wants to refresh the keyboard
void OsConsole::CheckKbd()
{
	HRESULT hResult;
	char keyboardData[256];

	do {
		hResult = keyboardDevice->GetDeviceState(256, keyboardData);
		if ((hResult == DIERR_NOTACQUIRED) || (hResult == DIERR_INPUTLOST))
			keyboardDevice->Acquire();
	} while (hResult != DI_OK);

	for (int i = 0; i < 256; i++) {
		if (prefs.keyConv[i] == 0xFF) continue;
		if ((keyboardData[i] & 0x080) != (lastKeyboardData[i] & 0x080)) {
			if (keyboardData[i] & 0x080)
				CpcKeyDown(prefs.keyConv[i]);
			else
				CpcKeyUp(prefs.keyConv[i]);
		}
	}

	memcpy(lastKeyboardData, keyboardData, sizeof(keyboardData));
}

void OsConsole::Update() {
	FrameReady();
}

// Synchronize
void OsConsole::FrameReady()
{
	// RenderInfo to Offscreen buffer
	CpcBase::RenderInfo renderInfo(CpcDib32(offScreenPt, Ga::CPC_SCR_H, offscreenBPR), cpcConfig.monochrome ? M32() : P32()
		, CpcRect(0, 0, Ga::CPC_SCR_H - 1, Ga::CPC_SCR_W - 1), cpcConfig.screenSkip ? CpcBase::RenderMode::SKIP : CpcBase::RenderMode::FULL);
	// Render the frame
	cpc.RenderFrame(renderInfo);
	RedrawFrame();
}

std::string OsConsole::GetStr(const infra::MsgI18n& msgId)
{
	return msgId.GetText();
}

void OsConsole::PutError(const std::string& str)
{
	MessageBox(mainWin, str.c_str(), "CPC++ Error", MB_ICONERROR);
}

#include <Shlwapi.h>
static std::ofstream& MsgStrm()
{
	static std::ofstream* msgOutP(0);
	if (!msgOutP)
	{
		HMODULE hModule = GetModuleHandleW(NULL);
		char path[MAX_PATH];
		GetModuleFileName(hModule, path, MAX_PATH);
		if (PathRemoveFileSpec(path))
		{
			strcat(path, "\\msg.txt");
			msgOutP = new std::ofstream(path);
		}
		else
		{
			msgOutP = new std::ofstream("msg.txt");
		}
		*msgOutP << "Open path: " << path << std::endl;
	}
	return *msgOutP;
}

void OsConsole::Put(const std::string& str)
{
	MsgStrm() << str << std::endl;
}

void OsConsole::PutPrinter(BYTE /*val*/) {}

void OsConsole::PrinterGlue(void* param, BYTE val) {
	((OsConsole*)param)->PutPrinter(val);
}


bool OsConsole::PutConfirm(const std::string& str)
{
	if (MessageBox(mainWin, str.c_str(), "CPC++ Confirm", MB_ICONQUESTION | MB_OKCANCEL) == IDCANCEL)
		return false;
	return true;
}

//------------------------------------------------------------------------------------
// Event/Messages Processing
//------------------------------------------------------------------------------------

// Glue for event Proc
LRESULT APIENTRY OsConsole::WndProcGlue(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam)
{
	OsConsole* This = (OsConsole*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (This == 0)
		return DefWindowProc(hWnd, wMsg, wParam, lParam);
	return This->WndProc(hWnd, wMsg, wParam, lParam);
}

// Window event proc
LRESULT OsConsole::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (hWnd != mainWin)
		return 0;

	switch (message) {
	case WM_PAINT: return OnPaint(wParam, lParam);
	case WM_DESTROY: DoQuit(true); return 0;
	case WM_COMMAND: return OnCommand(wParam, lParam);
	case WM_DROPFILES: return OnDropFiles(wParam, lParam);
	case WM_SYSKEYDOWN: return 0;
	case WM_LBUTTONDOWN:
		// Give the click position to the monitor for CRT debugging
		WindowClick(LOWORD(lParam), HIWORD(lParam));
		return 0;
	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

long OsConsole::OnPaint(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	PAINTSTRUCT ps;
	BeginPaint(mainWin, &ps);
	if (!IsRectEmpty(&ps.rcPaint)) // if refresh rect isn't empty
		RedrawFrame();
	EndPaint(mainWin, &ps);
	return 0L;
}

long OsConsole::OnDropFiles(WPARAM wParam, LPARAM /*lParam*/)
{
	HDROP hDrop = (HDROP)wParam;
	int nbFiles = DragQueryFile(hDrop, 0xFFFFFFFF, 0, 0);
	for (int i = 0; i < nbFiles; i++) {
		char fName[1024];
		DragQueryFile(hDrop, i, fName, 1024);
		if (IsDiskImage(fName, 0))
			LoadDisk(fName, CpcBase::DriveId::A);
		else if (IsSnapshot(fName, 0))
			LoadSnapshot(fName);
		else {
			BYTE buff[33];
			buff[32] = 0;
			FILE* fd = fopen(fName, "rb");
			if (fd) {
				if (fread(buff, 32, 1, fd) == 1) {
					if (IsDiskImage(fName, buff))
						LoadDisk(fName, CpcBase::DriveId::A);
					else if (IsSnapshot(fName, buff))
						LoadSnapshot(fName);
				}
				fclose(fd);
			}
		}
	}
	DragFinish(hDrop);
	return 0;
}

//------------------------------------------------------------------------------------
// Commands Processing
//------------------------------------------------------------------------------------


LRESULT OsConsole::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int menuItem = (wParam & 0xFFFF); // Mask for shortcuts

	// Deal with RunMenus
	if (menuItem >= RM_BASEID && menuItem < RM_BASEID + 200) {
		int drive = (int)((menuItem - RM_BASEID) / 100);
		RunProg(drive? CpcBase::DriveId::B: CpcBase::DriveId::A, menuItem - RM_BASEID - drive * 100);
		return 0;
	}

	// Deal with Commands
	for (int i = 0; cmdMap[i].cmd != CpcCmd::kCmdEndOfList; i++) {
		if (cmdMap[i].id == menuItem) {
			DoCommand(cmdMap[i].cmd);
			return 0;
		}
	}
	return DefWindowProc(mainWin, WM_COMMAND, wParam, lParam);
}

//------------------------------------------------------------------------------------
// Dialogs
//------------------------------------------------------------------------------------

typedef struct {
	bool remember;
	const char* msg;
} ConfirmDialogInfo;

INT_PTR CALLBACK ConfirmProc(HWND dialog, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_INITDIALOG: {
		ConfirmDialogInfo* info = (ConfirmDialogInfo*)lParam; // Get the info pointer

		SetWindowText(dialog, "Please Confirm...");
		SetDlgItemText(dialog, IDC_CONFIRM_TEXT, info->msg);
		info->remember = false;
		SetWindowLongPtr(dialog, GWLP_USERDATA, (LONG_PTR)info); // Store the info pointer
		return true;
	}

	case WM_COMMAND: {
		ConfirmDialogInfo* info = (ConfirmDialogInfo*)GetWindowLongPtr(dialog, GWLP_USERDATA);
		switch (LOWORD(wParam)) {
		case IDC_CONFIRM_REMEMBER:
			info->remember = !info->remember;
			return TRUE;
		case IDOK:
			EndDialog(dialog, 1);
			return true;
		case IDCANCEL:
			EndDialog(dialog, 0);
			return true;
		default:
			break;
		}
	}break;
	default:
		break;
	}
	return false;
}

bool OsConsole::ConfirmDlg(const infra::MsgI18n& msgId)
{
	string msg = msgId.GetText();
	string msgStr = string(msgId.GroupName()) + ":" + msgId.ToString();
	ASSERT_THROW(msgStr.size() < MAX_CONFIRM_ID_SZ+1);

	// Look if we remember
	for (auto it : prefs.confirms)
		if (msgStr == it.id)
			return it.val;

	ConfirmDialogInfo info;
	info.msg = msg.c_str();
	INT_PTR ok = DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_CONFIRM), mainWin, ConfirmProc, (LPARAM)&info);
	if (info.remember && ok)
		prefs.confirms.emplace_back(msgStr, ok);
	return ok;
}

// Keyboard config dialog
//------------------------------------------------------------------------------------

static WNDPROC CpcKbdPrevProc;
static int xPos, yPos;
static int CpcKbdDone;
static HWND cpcKbdHwnd;
static HBITMAP bitMap;
static HDC CpcKbdHdc;
static int diKey;

// This Subclasses the keyboard PICT control to catch mouse click positions
// before we receive the forwarded WM_COMMAND message in our dialog proc.
LRESULT APIENTRY CpcKbdProc(HWND hWnd, UINT wMsg, WPARAM wParam, LONG lParam)
{
	if (wMsg == WM_LBUTTONDOWN) {
		xPos = LOWORD(lParam);  // horizontal position of cursor
		yPos = HIWORD(lParam);  // vertical position of cursor
	}
	return CallWindowProc(CpcKbdPrevProc, hWnd, wMsg, wParam, lParam);
}

// Create a modal dialog with our own event loop
void OsConsole::KbdDefine()
{
	CpcKbdDone = false;

	// create dialog box (calls KbdDefineProc with WM_INITDIALOG)
	HWND dialog = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_KBD_MAP), mainWin, KbdDefineProcGlue);
	if (dialog == 0) return;
	currentDialog = dialog;

	// show dialog box and put it top most
	ShowWindow(dialog, SW_SHOW);
	SetWindowPos(dialog, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	SetActiveWindow(dialog);

	// message loop: only process messages for the dialog box to make it modal
	MSG msg;
	while (GetMessage(&msg, 0, 0, 0) && !CpcKbdDone) {
		// is the message bound for us or one of our children?
		bool ourWindow = dialog == msg.hwnd || GetParent(msg.hwnd) == dialog;

		// To make it modal, if not in our window, discard all events.
		if (!ourWindow) {
			// ignore all timer, mouse and keyboard messages
			if (msg.message == WM_TIMER) continue;
			if (msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) continue;
			if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST) continue;
		}
		else if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST) {
			// on key messages, we scan the keyboard using direct input
			HRESULT hResult;
			char keyboardData[256];

			diKey = -1;
			do {
				hResult = keyboardDevice->GetDeviceState(256, keyboardData);
				if ((hResult == DIERR_NOTACQUIRED) || (hResult == DIERR_INPUTLOST))
					keyboardDevice->Acquire();
			} while (hResult != DI_OK);

			for (int i = 0; i < 256; i++) {
				if (keyboardData[i] & 0x080) {
					if (diKey >= 0) {
						diKey = -1;
						break;
					}
					diKey = i;
				}
			}
			if (msg.message == WM_KEYDOWN || msg.message == WM_SYSKEYDOWN && diKey > 0) {
				char str[1024];
				int cpcKey = prefs.keyConv[diKey];
				if (cpcKey != 0xFF)
					sprintf(str, "PC key %s (%02X) mapped to CPC key %s (%02X)", pcKeyStr[diKey], diKey, cpcKeyStr[cpcKey], cpcKey);
				else
					sprintf(str, "PC key %s (%02X) not mapped", pcKeyStr[diKey], diKey);
				SetDlgItemText(dialog, IDC_KBD_MAPPING, str);
			}
			continue; // discard
		}

		// dispatch to dialog window proc
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// destroy dialog box
	DestroyWindow(dialog);
}

INT_PTR CALLBACK OsConsole::KbdDefineProcGlue(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	return theConsole->KbdDefineProc(dialog, iMsg, wParam, lParam);
}

bool OsConsole::KbdDefineProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	int action, item;

	switch (iMsg) {
	case WM_INITDIALOG:
	{
		// Subclass CPC keyboard control
		cpcKbdHwnd = GetDlgItem(dialog, IDC_KBD_CPC);
		CpcKbdPrevProc = (WNDPROC)SetWindowLongPtr(cpcKbdHwnd, GWLP_WNDPROC, (LONG_PTR)CpcKbdProc);

		// Load the bitmap for key identification
		bitMap = (HBITMAP)LoadImage(hInstance, MAKEINTRESOURCE(IDB_CPCKBDMAP), IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
		CpcKbdHdc = CreateCompatibleDC(0);
		SelectObject(CpcKbdHdc, bitMap);
		diKey = -1;

		// set title
		SetWindowText(dialog, "Raw Keyboard Mapping");
	}
	break;

	case WM_KEYDOWN: case WM_KEYUP: case WM_SYSKEYDOWN:
	{
		int nVirtKey = (int)wParam;    // virtual-key code
		int lKeyData = (int)lParam;          // key data
		char str[1024];
		sprintf(str, "KeyPress: vKey=%d(%04X) kData=%d(%08X)", nVirtKey, nVirtKey, lKeyData, lKeyData);
		SetDlgItemText(dialog, IDC_KBD_MAPPING, str);
		return true;
	}
	case WM_CHAR:
		return true;

	case WM_COMMAND: {
		action = HIWORD(wParam);
		item = LOWORD(wParam);


		if (action != BN_CLICKED) break;

		if (item == IDC_KBD_CPC) {
			COLORREF color = GetPixel(CpcKbdHdc, xPos, yPos);
			BYTE cpcKey = static_cast<BYTE>((color & 0xFF) - 128);
			char str[1024];
			if (cpcKey < 80) {
				if (diKey >= 0) {
					prefs.keyConv[diKey] = cpcKey;
				}
				int pcKey = -1;
				for (int i = 0; i < 256; i++)
					if (prefs.keyConv[i] == cpcKey)
						pcKey = i;
				if (pcKey > 0)
					sprintf(str, "CPC key %s (%02X) mapped to PC key %s (%02X)", cpcKeyStr[cpcKey], cpcKey, pcKeyStr[pcKey], pcKey);
				else
					sprintf(str, "CPC key %s (%02X) not mapped", cpcKeyStr[cpcKey], cpcKey);
				SetDlgItemText(dialog, IDC_KBD_MAPPING, str);
			}

			return true;
		}

		if (item == IDOK) {
			CpcKbdDone = true;
			break;
		}
		if (item == IDCANCEL) {
			CpcKbdDone = true;
			break;
		}
		if (item == IDC_KBD_DEFAULT) {
			for (int i = 0; i < 256; i++)
				prefs.keyConv[i] = rawKeyMapDefault[i];
		}
	}break;
	case WM_CLOSE:
		CpcKbdDone = true;
		break;
	case WM_DESTROY:
		CpcKbdDone = true;
		break;
	default:
		break;
	}
	if (CpcKbdDone) {
		currentDialog = 0;
		SetWindowLongPtr(cpcKbdHwnd, GWLP_WNDPROC, (LONG_PTR)CpcKbdPrevProc);
		//EndDialog(dialog,0);
		DestroyWindow(dialog);
		DeleteDC(CpcKbdHdc);
		DeleteObject(bitMap);
		return true;
	}
	return false;
}

// Expansion ROMs selection
//------------------------------------------------------------------------------------
static struct { int check, select, clear, desc; } romItemIds[] = {
	{IDC_ROM_CHK1, IDC_ROM_SEL1, IDC_ROM_CLR1, IDC_ROM_DESC1}
	, {IDC_ROM_CHK2, IDC_ROM_SEL2, IDC_ROM_CLR2, IDC_ROM_DESC2}
	, {IDC_ROM_CHK3, IDC_ROM_SEL3, IDC_ROM_CLR3, IDC_ROM_DESC3}
	, {IDC_ROM_CHK4, IDC_ROM_SEL4, IDC_ROM_CLR4, IDC_ROM_DESC4}
	, {IDC_ROM_CHK5, IDC_ROM_SEL5, IDC_ROM_CLR5, IDC_ROM_DESC5}
	, {IDC_ROM_CHK6, IDC_ROM_SEL6, IDC_ROM_CLR6, IDC_ROM_DESC6}
	, {IDC_ROM_CHK7, IDC_ROM_SEL7, IDC_ROM_CLR7, IDC_ROM_DESC7}
	, {IDC_ROM_CHK8, IDC_ROM_SEL8, IDC_ROM_CLR8, IDC_ROM_DESC8}
	, {IDC_ROM_CHK9, IDC_ROM_SEL9, IDC_ROM_CLR9, IDC_ROM_DESC9}
	, {IDC_ROM_CHK10, IDC_ROM_SEL10, IDC_ROM_CLR10, IDC_ROM_DESC10}
	, {IDC_ROM_CHK11, IDC_ROM_SEL11, IDC_ROM_CLR11, IDC_ROM_DESC11}
	, {IDC_ROM_CHK12, IDC_ROM_SEL12, IDC_ROM_CLR12, IDC_ROM_DESC12}
	, {IDC_ROM_CHK13, IDC_ROM_SEL13, IDC_ROM_CLR13, IDC_ROM_DESC13}
	, {IDC_ROM_CHK14, IDC_ROM_SEL14, IDC_ROM_CLR14, IDC_ROM_DESC14}
	, {IDC_ROM_CHK15, IDC_ROM_SEL15, IDC_ROM_CLR15, IDC_ROM_DESC15}
	, {IDC_ROM_CHK16, IDC_ROM_SEL16, IDC_ROM_CLR16, IDC_ROM_DESC16}
	, {IDC_ROM_CHK17, IDC_ROM_SEL17, IDC_ROM_CLR17, IDC_ROM_DESC17}
	, {IDC_ROM_CHK18, IDC_ROM_SEL18, IDC_ROM_CLR18, IDC_ROM_DESC18}
	, {IDC_ROM_CHK19, IDC_ROM_SEL19, IDC_ROM_CLR19, IDC_ROM_DESC19}
	, {IDC_ROM_CHK20, IDC_ROM_SEL20, IDC_ROM_CLR20, IDC_ROM_DESC20}
	, {IDC_ROM_CHK21, IDC_ROM_SEL21, IDC_ROM_CLR21, IDC_ROM_DESC21}
	, {IDC_ROM_CHK22, IDC_ROM_SEL22, IDC_ROM_CLR22, IDC_ROM_DESC22}
	, {IDC_ROM_CHK23, IDC_ROM_SEL23, IDC_ROM_CLR23, IDC_ROM_DESC23}
	, {IDC_ROM_CHK24, IDC_ROM_SEL24, IDC_ROM_CLR24, IDC_ROM_DESC24}
	, {IDC_ROM_CHK25, IDC_ROM_SEL25, IDC_ROM_CLR25, IDC_ROM_DESC25}
	, {IDC_ROM_CHK26, IDC_ROM_SEL26, IDC_ROM_CLR26, IDC_ROM_DESC26}
	, {IDC_ROM_CHK27, IDC_ROM_SEL27, IDC_ROM_CLR27, IDC_ROM_DESC27}
	, {IDC_ROM_CHK28, IDC_ROM_SEL28, IDC_ROM_CLR28, IDC_ROM_DESC28}
	, {IDC_ROM_CHK29, IDC_ROM_SEL29, IDC_ROM_CLR29, IDC_ROM_DESC29}
	, {IDC_ROM_CHK30, IDC_ROM_SEL30, IDC_ROM_CLR30, IDC_ROM_DESC30}
	, {IDC_ROM_CHK31, IDC_ROM_SEL31, IDC_ROM_CLR31, IDC_ROM_DESC31}
	, {IDC_ROM_CHK32, IDC_ROM_SEL32, IDC_ROM_CLR32, IDC_ROM_DESC32}
	, {IDC_ROM_CHK33, IDC_ROM_SEL33, IDC_ROM_CLR33, IDC_ROM_DESC33}
};

void OsConsole::DoRomDialog()
{
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_ROMSELECT), mainWin, RomSelectProcGlue);
}

void OsConsole::RomSelectDialogSet(HWND dialog, const RomConfig* roms)
{
	for (int i = 0; i < NB_ROMSLOTS + 1; i++) {
		CheckDlgButton(dialog, romItemIds[i].check, roms[i].active ? BST_CHECKED : BST_UNCHECKED);
		SetDlgItemText(dialog, romItemIds[i].desc, roms[i].description.c_str());
	}
}

INT_PTR CALLBACK OsConsole::RomSelectProcGlue(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	return theConsole->RomSelectProc(dialog, iMsg, wParam, lParam);
}

RomConfig roms[NB_ROMSLOTS + 1];
bool OsConsole::RomSelectProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM /*lParam*/)
{
	int i, action, item;
	bool done = false;

	switch (iMsg) {
	case WM_INITDIALOG:
		for (i = 0; i < NB_ROMSLOTS; i++) {
			roms[i] = cpcConfig.upperRoms[i];
			if (roms[i].path[0] == 0) roms[i].active = 0;
		}
		roms[NB_ROMSLOTS] = cpcConfig.lowerRom;
		RomSelectDialogSet(dialog, roms);
		SetWindowText(dialog, "Expansion ROMs selection");
		break;

	case WM_COMMAND: {
		action = HIWORD(wParam);
		item = LOWORD(wParam);

		if (action != BN_CLICKED) break;

		if (item == IDOK) {
			for (i = 0; i < NB_ROMSLOTS; i++) {
				cpcConfig.upperRoms[i] = roms[i];
			}
			cpcConfig.lowerRom = roms[NB_ROMSLOTS];
			done = true;
			break;
		}
		if (item == IDCANCEL) {
			done = true;
			break;
		}
		for (i = 0; i < NB_ROMSLOTS + 1; i++)
		{
			if (item == romItemIds[i].check) {
				if (roms[i].path[0] != 0)
					roms[i].active ^= 1;
				RomSelectDialogSet(dialog, roms);
				return true;
			}
			if (item == romItemIds[i].select) {
				std::string path;
				std::string desc;
				if (SelectRom(path, desc, i)) {
					roms[i].description = desc;
					roms[i].path = path;
					roms[i].active = 1;
					RomSelectDialogSet(dialog, roms);
				}
				return true;
			}
			if (item == romItemIds[i].clear) {
				roms[i].description[0] = 0;
				roms[i].path[0] = 0;
				roms[i].active = 0;
				RomSelectDialogSet(dialog, roms);
				return true;
			}
		}

	}break;
	case WM_CLOSE:
		done = true;
		break;
	case WM_DESTROY:
		break;
	default:
		break;
	}
	if (done) {
		currentDialog = 0;
		//DestroyWindow(dialog);
		EndDialog(dialog, 0);
		return true;
	}
	return false;
}

//------------------------------------------------------------------------------------
// Support functions
//------------------------------------------------------------------------------------

void OsConsole::SpeedReport(UINT32 /*expected*/, UINT32 /*executed*/, UINT32 /*elapsed*/)
{
}

void OsConsole::DiskActivity(int /*which*/, DriveState /*state*/, int track) {
}

void OsConsole::LaunchUrl(const std::string& url) {
	ShellExecute(GetHWnd(), "open", url.c_str(), 0, 0, SW_SHOWNORMAL);
}

void OsConsole::CopyToClipboard(void) {
	if (!OpenClipboard(NULL)) return;
	if (!EmptyClipboard()) return;

	int buflen = sizeof(BITMAPINFO) + 4 * Ga::CPC_SCR_W * Ga::CPC_SCR_H;
	HGLOBAL hResult = GlobalAlloc(GMEM_MOVEABLE, buflen);
	if (hResult == NULL) return;

	memcpy(GlobalLock(hResult), bmi, buflen);
	GlobalUnlock(hResult);

	SetClipboardData(CF_DIB, hResult);
	CloseClipboard();
	GlobalFree(hResult);
}

#define LODEPNG_NO_COMPILE_ZLIB
#include "lodepng.h"

void OsConsole::SavePICTFile() {
	int nbPix = Ga::CPC_SCR_W * Ga::CPC_SCR_H;
	BYTE* rawPt(reinterpret_cast<BYTE *>(offScreenPt));
	for (int i = 0; i < nbPix; i++) swap(*(rawPt + 4 * i), *(rawPt + 4 * i + 2)); // BGRA -> RGBA
	vector<unsigned char> png;
	if (lodepng::encode(png, rawPt, Ga::CPC_SCR_W, Ga::CPC_SCR_H)) return;
	for (int i = 0; i < nbPix; i++) swap(*(rawPt + 4 * i), *(rawPt + 4 * i + 2)); // RGBA -> BGRA

	string stem = "cpc++_screen_";
	int index = 0;
	for (;;)
	{
		stringstream ss;
		ss << "cpc++_screen_" << index << ".png";
		ifstream in(ss.str().c_str());
		if (in) {
			index++;
		}
		else {
			lodepng::save_file(png, ss.str().c_str());
			return;
		}
	}
}

void OsConsole::SaveSnapshot(const std::string& /*path*/, const BYTE* /*buff*/) {
}

void OsConsole::SetScreenSmall(bool /*val*/) {
}

void OsConsole::SetScreenFull(bool /*val*/) {
}

void OsConsole::SetScreenSkip(bool val)
{
	CpcGuiConsole::SetScreenSkip(val);
}

void OsConsole::SetIndicators(bool /*val*/) {
}

void OsConsole::DoStartupDialog() {
}

INT_PTR CALLBACK OsConsole::AboutProcGlue(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	return theConsole->AboutProc(dialog, iMsg, wParam, lParam);
}

static void DrawLongText(CpcDib32 dib, const string& myText, AmstradText& aText, unsigned char fore, int& crtRow)
{
	char str[64], * pt;
	pt = str;
	for (unsigned int i = 0; i <= myText.size(); i++) {
		if (myText[i] == '\r' || myText[i] == '\n' || (pt - str) > 20 || myText[i] == 0) {
			*pt = 0;
			aText.DrawAmstradText(dib, static_cast<int>(8 + (19 - strlen(str)) * 4), crtRow, CpcGuiConsole::P32()[fore], str);
			pt = str;
			crtRow += 8;
		}
		else
			*pt++ = myText[i];
	}
}


static struct {
	int line;
	int lastPixRow;
	HBITMAP scrollTextHBitmap;
	BITMAP scrollTextBitmap;
	BITMAPINFO bmi;
	POINT dstCorner;
} gScrollText;

void OsConsole::PrepareAboutText(HWND dialog)
{
	AmstradText aText(CpcDib32((RAW32*)fontBitmap.bmBits, fontBitmap.bmHeight, fontBitmap.bmWidthBytes));

	// Load scroll text image
	gScrollText.scrollTextHBitmap = (HBITMAP)LoadImage(hInstance, MAKEINTRESOURCE(IDB_SCROLLTEXT), IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	BITMAP scrollTextBitmap;
	GetObject(gScrollText.scrollTextHBitmap, sizeof(BITMAP), &scrollTextBitmap);
	UpsideDown(scrollTextBitmap);
	CpcDib32 scrollTextDib((RAW32*)scrollTextBitmap.bmBits, scrollTextBitmap.bmHeight, scrollTextBitmap.bmWidthBytes);

	// Draw the Copyright years
	static const int copyrightRow = 90;
	aText.DrawAmstradText(scrollTextDib, 8, copyrightRow, P32()[BrightYellow], "1996-2014");


	static const int aboutTextRow = 120;
	gScrollText.lastPixRow = aboutTextRow;

	// Draw the version info
	string CL = string(versionString).substr(7, 3);
	string versionInfo = string("Version ") + CL + " \n";
	DrawLongText(scrollTextDib, versionInfo, aText, BrightYellow, gScrollText.lastPixRow);
	gScrollText.lastPixRow += 16;

	// Draw the scrolling text centered
	// Do line feeds for \r or more than 32 chars
	std::string myText = "Many thanks to:\n\nPierre Guerrier\nFrederique Herlem\nJacques Azoulai\nRichard Bannister\nJean Francois Amand\nGilles Blanchard\nPhilippe Castellan\nSimon Owen\nBruno Kukulcan\nPhilippe Depre";
	DrawLongText(scrollTextDib, myText, aText, BrightYellow, gScrollText.lastPixRow);

	// Make a bmi around the scrollText
	GetObject(gScrollText.scrollTextHBitmap, sizeof(BITMAP), &gScrollText.scrollTextBitmap);
	memset(&gScrollText.bmi, 0, sizeof(BITMAPINFO)); //lint -e668
	gScrollText.bmi.bmiHeader.biSize = sizeof(gScrollText.bmi.bmiHeader);
	gScrollText.bmi.bmiHeader.biWidth = gScrollText.scrollTextBitmap.bmWidth;
	gScrollText.bmi.bmiHeader.biHeight = -gScrollText.scrollTextBitmap.bmHeight;    // must be positive if we want a bottom-up DIB with origin in the lower-left
	gScrollText.bmi.bmiHeader.biPlanes = 1;
	gScrollText.bmi.bmiHeader.biBitCount = 32;
	gScrollText.bmi.bmiHeader.biCompression = BI_RGB;
	gScrollText.bmi.bmiHeader.biSizeImage = 0;
	gScrollText.bmi.bmiHeader.biXPelsPerMeter = 0;
	gScrollText.bmi.bmiHeader.biYPelsPerMeter = 0;
	gScrollText.bmi.bmiHeader.biClrUsed = 0;
	gScrollText.bmi.bmiHeader.biClrImportant = 0;

	// Compute the destination rectangle
	HWND aboutPictWnd = GetDlgItem(dialog, IDC_SCROLL_ZONE);
	RECT scrollZoneRect;
	GetWindowRect(aboutPictWnd, &scrollZoneRect);
	POINT tlCorner = { scrollZoneRect.left, scrollZoneRect.top };
	ScreenToClient(dialog, &tlCorner);
	gScrollText.dstCorner = tlCorner;
}

void OsConsole::ScrollText(HWND dialog)
{
	// Get a HDC on the about dialog
	HDC hdc = GetDC(dialog);
	// Blit
	StretchDIBits(hdc, gScrollText.dstCorner.x, gScrollText.dstCorner.y, 160, 100
		, 0, gScrollText.lastPixRow - gScrollText.line, 160, 100
		, gScrollText.scrollTextBitmap.bmBits, &gScrollText.bmi, DIB_RGB_COLORS, SRCCOPY);
	//StretchBlt(hdc,   dstRect.left, dstRect.top, dstRect.right-dstRect.left+1, dstRect.bottom-dstRect.top+1, scrollTextHdc, srcRect.left, srcRect.top, srcRect.right-srcRect.left+1, srcRect.bottom-srcRect.top+1, SRCCOPY);
	ReleaseDC(mainWin, hdc);

	if (++gScrollText.line > gScrollText.lastPixRow)
		gScrollText.line = 0;
}


VOID CALLBACK OsConsole::TimerProc(_In_  HWND hwnd, _In_  UINT uMsg, _In_  UINT_PTR idEvent, _In_  DWORD dwTime)
{
	theConsole->AboutProc(hwnd, WM_TIMER, 0, 0);
}

bool OsConsole::AboutProc(HWND dialog, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	bool done = false;
	static UINT_PTR timerId;
	switch (iMsg) {
	case WM_INITDIALOG:
		PrepareAboutText(dialog);
		SetWindowText(dialog, "About CPC++");
		timerId = SetTimer(dialog, 0, 50, TimerProc);
		break;
	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED && LOWORD(wParam) == IDOK)
			done = true;
		break;
	case WM_CLOSE:
		done = true;
		break;
	case WM_DESTROY:
		break;
	case WM_TIMER:
		ScrollText(dialog);
		break;
	default:
		break;
	}

	if (done) {
		KillTimer(dialog, timerId);
		currentDialog = 0;
		//DestroyWindow(dialog);
		EndDialog(dialog, 0);
		return true;
	}
	return false;
}


void OsConsole::DoAboutDialog() {
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_SPLASH), mainWin, AboutProcGlue);
	DeleteObject(gScrollText.scrollTextHBitmap);
}

void OsConsole::CheckCompressed(const std::string& path, FileType type, int index, BYTE*& data, int& size)
{
	CheckCompressedS(path, type, index, data, size);
}

// Uncompress the indexth item that passes the filter
// If there is such an item, data is a newly allocated buffer containing the uncompressed item
// otherwise data is zero
void OsConsole::CheckCompressedS(const std::string& path, FileType type, int index, BYTE*& data, int& size)
{
	int found = 0;
	data = 0;
	size = 0;

	if (!IsZipFile(path)) return;

	unzFile zFile = unzOpen(path.c_str());
	unz_global_info info;
	unzGetGlobalInfo(zFile, &info);
	for (unsigned int zIdx = 0; zIdx < info.number_entry; zIdx++) {
		unz_file_info fInfo;
		char zfpath[128];
		char zeField[128];
		char zComm[128];
		unzGetCurrentFileInfo(zFile, &fInfo, zfpath, 128, zeField, 128, zComm, 128);

		BYTE* buff = new BYTE[fInfo.uncompressed_size];
		if (buff == 0) {
			unzClose(zFile);
			ERR_THROW("Out of memory");
		}
		if (unzOpenCurrentFile(zFile) != UNZ_OK) {
			unzClose(zFile);
			ERR_THROW("Bad zip file");
		}
		if (unzReadCurrentFile(zFile, buff, fInfo.uncompressed_size) != static_cast<int>(fInfo.uncompressed_size)) {
			unzCloseCurrentFile(zFile);
			unzClose(zFile);
			ERR_THROW("Bad zip file");
		}
		unzCloseCurrentFile(zFile);

		if (IsFileType(zfpath, buff, type))
			found++;

		if (found == index + 1) {
			size = fInfo.uncompressed_size;
			data = buff;
			break;
		}
		delete[] buff;
		unzGoToNextFile(zFile);
	}
	unzClose(zFile);
}

void OsConsole::SetFileType(const std::string& /*path*/, FileType /*type*/)
{
#if 0 // don't play with extensions!!!
	char* ext;
	switch (type) {
	case FileDisk: ext = DSK_EXTENSION; break;
	case FileSnapshot: ext = SNA_EXTENSION; break;
	case FileRom: ext = ROM_EXTENSION; break;
	default: return;
	};

	const char* ptP = strrchr(path, '.');
	char typedPath[1024];
	if (ptP == 0)
		strcpy(typedPath, path);
	else {
		if (strcmp(ptP + 1, ext) == 0) return; // already the right extension
		strncpy(typedPath, path, ptP - path);
	}
	strcat(typedPath, ext);
	rename(path, typedPath);
#endif
}

void OsConsole::SetIcon(const std::string& path)
{
	// don't know how to that in Windows
}

void OsConsole::PrintSetup() {
}

void OsConsole::Print() {
}

void OsConsole::PlayCpcSound(CpcSound sound)
{
	switch (sound) {
	case Insert:
		PlaySound(MAKEINTRESOURCE(IDR_WAVE_INSERT), hInstance, SND_RESOURCE);
		break;
	case Eject:
		PlaySound(MAKEINTRESOURCE(IDR_WAVE_EJECT), hInstance, SND_RESOURCE);
		break;
	}
}

void OsConsole::DoKeyboardDialog()
{
	KbdDefine();
}

bool OsConsole::FilterFile(const std::string& path, FileType type)
{
	TypeFilter filter = GetFilter(type);
	return FilterFileWithFilter(path, filter);
}

bool OsConsole::FilterFileWithFilter(const std::string& path, TypeFilter filter)const
{
	// See if it looks OK based on the path
	if ((*filter)(path, 0)) return true;

	// If it's an archive look for a file in there
	if (IsZipFile(path)) {
		unzFile zFile = unzOpen(path.c_str());
		unz_global_info info;
		unzGetGlobalInfo(zFile, &info);
		for (unsigned int zIdx = 0; zIdx < info.number_entry; zIdx++) {
			unz_file_info fInfo;
			char zfpath[128];
			char zeField[128];
			char zComm[128];
			unzGetCurrentFileInfo(zFile, &fInfo, zfpath, 128, zeField, 128, zComm, 128);
			if ((*filter)(zfpath, 0)) {
				unzClose(zFile);
				return true;
			}
			BYTE buff[32];
			if (unzOpenCurrentFile(zFile) != UNZ_OK) {
				unzClose(zFile);
				return false;
			}
			if (unzReadCurrentFile(zFile, buff, 32) != 32) {
				unzCloseCurrentFile(zFile);
				unzClose(zFile);
				return false;
			}
			unzCloseCurrentFile(zFile);
			if ((*filter)("", buff)) {
				unzClose(zFile);
				return true;
			}
			unzGoToNextFile(zFile);
		}
		unzClose(zFile);
	}

	// Look at the data
	BYTE buff[32];
	FILE* fd = fopen(path.c_str(), "rb");
	if (fd) {
		fread(buff, 32, 1, fd);
		fclose(fd);
		if ((*filter)("", buff)) return true;
	}

	return false; // else reject
}

int OsConsole::CmdToId(CpcCmd cmd)const
{
	for (int i = 0; cmdMap[i].cmd != CpcCmd::kCmdEndOfList; i++) {
		if (cmdMap[i].cmd == cmd)
			return cmdMap[i].id;
	}
	return 0;
}

void OsConsole::CmdChecked(CpcCmd cmd, bool checked)
{
	MenuItemChecked(CmdToId(cmd), checked);
}

void OsConsole::CmdEnabled(CpcCmd cmd, bool enabled)
{
	MenuItemEnabled(CmdToId(cmd), enabled);
}

void OsConsole::WindowTitle(const std::string& title)
{
	string newTitle = string("CPC++ [") + versionString + "] - " + title;
	SetWindowText(mainWin, newTitle.c_str());
}

void OsConsole::EmptyRunMenus()
{
	HMENU runMenu = GetSubMenu(GetSubMenu(GetMenu(mainWin), 0), 2);
	do {} while (DeleteMenu(runMenu, 0, MF_BYPOSITION));
	for (int drive = 0; drive < NB_DRIVES; drive++) {
		char caption[16];
		sprintf(caption, "Drive %c", 'A' + drive);
		runMenuDrive[drive] = CreatePopupMenu();
		AppendMenu(runMenu, MF_POPUP, (UINT_PTR)runMenuDrive[drive], caption);
		runMenuItems[drive] = 0;
	}
}

void OsConsole::AddItemToRunMenu(CpcBase::DriveId drive, const std::string& item_, bool enabled)
{
	const char* item(item_.c_str());
	char itemStr[32];
	for (unsigned int i = 0; i <= strlen(item); i++)
		itemStr[i] = item[i] == '&' ? '*' : item[i];
	int id = RM_BASEID + drive * 100 + runMenuItems[drive]++;
	AddItemToMenu(runMenuDrive[drive], itemStr, id);
	if (!enabled)
		MenuItemEnabled(id, false);
}


// Save config and prefs to registry
bool OsConsole::SaveConfig()const
{
	HKEY registryKey;
	DWORD disposition;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, "software\\CPC++", 0, 0, REG_OPTION_NON_VOLATILE,	KEY_WRITE, 0, &registryKey, &disposition) == ERROR_SUCCESS) {

		std::ostringstream os;
		boost::archive::binary_oarchive ar(os);
		ar << BOOST_SERIALIZATION_NVP(cpcConfig);
		ar << BOOST_SERIALIZATION_NVP(guiPrefs);
		ar << BOOST_SERIALIZATION_NVP(prefs);
		std::string strData = os.str();

		long byteCount = static_cast<long>(strData.size());
		RegSetValueEx(registryKey, "Config", 0, REG_BINARY, (const BYTE*)strData.c_str(), byteCount);
		RegCloseKey(registryKey);
	}
	return true;
}

// Load up the config and prefs data from registry
void OsConsole::LoadConfig()
{
	// reset to defaults in case we cannot load
	cpcConfig = CpcConfig();
	guiPrefs = GuiPrefs();
	prefs = Prefs();

	// Load from registry
	HKEY registryKey;
	DWORD disposition;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, "software\\CPC++", 0, 0, REG_OPTION_NON_VOLATILE, KEY_READ, 0, &registryKey, &disposition) == ERROR_SUCCESS) {
		if (disposition == REG_OPENED_EXISTING_KEY) {
			try {
				DWORD type, keyByteCount;
				RegQueryValueEx(registryKey, "Config", 0, &type, 0, &keyByteCount); // get size
				string regEntry;
				regEntry.resize(keyByteCount);
				RegQueryValueEx(registryKey, "Config", 0, &type, (unsigned char*)regEntry.c_str(), &keyByteCount); // get data

				std::istringstream is(regEntry, istringstream::in);
				boost::archive::binary_iarchive ar(is);
				ar >> BOOST_SERIALIZATION_NVP(cpcConfig);
				ar >> BOOST_SERIALIZATION_NVP(guiPrefs);
				ar >> BOOST_SERIALIZATION_NVP(prefs);
			}
			catch (...) {
				ErrReport(OsConsoleMsgId(OsConsoleMsgId::ERR_CON_BADPREF));
			}
		}
	}
	RegCloseKey(registryKey);
}



//------------------------------------------------------------------------------------
// Windows stuff
//------------------------------------------------------------------------------------

bool OsConsole::SelectFile(std::string& path, FileType& type, int& /*option*/)
{
	OPENFILENAME ofn;       // common dialog box structure
	static char nameBuff[260];       // buffer for file name - must be static!!!
	memset(nameBuff, 0, sizeof(nameBuff));
	static const char* typeStrings[] = {
		"DSK Image (*." DSK_EXTENSION ")\0*." DSK_EXTENSION "\0All\0*.*\0", //lint !e786
		"SNA Snapshot (*." SNA_EXTENSION ")\0*." SNA_EXTENSION "\0All\0*.*\0", //lint !e786
		"ROM Image (*." ROM_EXTENSION ")\0*." ROM_EXTENSION "\0All\0*.*\0", //lint !e786
		"All\0*.*\0SNA Snapshot (*." SNA_EXTENSION ")\0*." SNA_EXTENSION "\0DSK Image (*." DSK_EXTENSION ")\0*." DSK_EXTENSION "\0" //lint !e786
	};

	const char* typeStr = typeStrings[type];

	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = mainWin;
	ofn.lpstrFile = nameBuff;
	ofn.nMaxFile = sizeof(nameBuff);
	ofn.lpstrFilter = typeStr;
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = 0;
	ofn.nMaxFileTitle = 0;
	//ofn.lpstrInitialDir = szBuf;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// Display the Open dialog box.
	if (GetOpenFileName(&ofn) == TRUE) {
		path = nameBuff;
		return true;
	}
	return false;
}

bool OsConsole::GetSavePath(CpcGuiMsgId promptId, CpcGuiMsgId defaultNameId, std::string& path)
{
	const std::string prompt = GetStr(promptId);
	const std::string& defaultName = GetStr(defaultNameId);

	OPENFILENAME ofn;       // common dialog box structure
	static char nameBuff[1024];       // buffer for file name - must be static!!!
	if (path.empty())
		strncpy(nameBuff, defaultName.c_str(), 1024);
	else
		strncpy(nameBuff, path.c_str(), 1024);
	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = mainWin;
	ofn.lpstrFile = nameBuff;
	ofn.nMaxFile = sizeof(nameBuff);
	ofn.lpstrFilter = "DSK Image (*." DSK_EXTENSION ")\0*." DSK_EXTENSION "\0SNA Snapshot (*." SNA_EXTENSION ")\0*." SNA_EXTENSION "\0All\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = 0;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrTitle = prompt.c_str();
	//ofn.lpstrInitialDir = szBuf;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// Display the Open dialog box.
	if (GetSaveFileName(&ofn) == TRUE) {
		path = nameBuff;
		return true;
	}
	return false;
}

// Register a window class
bool OsConsole::RegisterWindowClass(HINSTANCE theHInstance, WNDPROC windProc, LPCSTR menuId, const char* name)const
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW; //CS_OWNDC;
	wcex.lpfnWndProc = windProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = theHInstance;
	wcex.hIcon = LoadIcon(theHInstance, (LPCTSTR)IDI_CPCPP);
	wcex.hCursor = 0; //LoadCursor(0, IDC_ARROW);
	wcex.hbrBackground = 0; //(HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = menuId;
	wcex.lpszClassName = name;
	wcex.hIconSm = LoadIcon(theHInstance, (LPCTSTR)IDI_CPCPP);
	return RegisterClassEx(&wcex) ? true : false;
}

void OsConsole::RedrawFrame()
{
	// get DC if we don't have one already
	HDC hdc = GetDC(mainWin);

	//NVTRACE((NVT_GLOBAL,"paste %d,%d,%d,%d",dstX, dstY, dstWidth, dstHeight));
	StretchDIBits(hdc, 0, 0, Ga::CPC_SCR_W, Ga::CPC_SCR_H,
		0, 0, Ga::CPC_SCR_W, Ga::CPC_SCR_H,
		offScreenPt, bmi, DIB_RGB_COLORS, SRCCOPY);

	// release DC
	ReleaseDC(mainWin, hdc);
}

std::string OsConsole::GetClipboardString()
{

	return "";
}

//------------------------------------------------------------------------------------
// Direct X
//------------------------------------------------------------------------------------

void OsConsole::dxCheck()const
{
	HINSTANCE dxLib = LoadLibrary("DDRAW.DLL");
	ASSERT_THROW(dxLib)("Direct draw not found");
	FreeLibrary(dxLib);

	dxLib = LoadLibrary("DINPUT.DLL");
	ASSERT_THROW(dxLib)("Direct input not found");
	FreeLibrary(dxLib);
}

bool OsConsole::GetHexValue(const char* prompt, int& val)
{
	val = 0;
	return true;
}

void OsConsole::DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char* str)const
{
	//  std::swap(*(char *)&color, *((char*)(&color)+3));
	//  std::swap(*((char *)&color+1), *((char*)(&color)+2));
	AmstradText amstradText(CpcDib32((RAW32*)fontBitmap.bmBits, fontBitmap.bmHeight, fontBitmap.bmWidthBytes));
	amstradText.DrawAmstradText(dst, x, y, color, str);
}

void OsConsole::MonitToggle()
{
	monitVisible ^= 1;
	if (monitVisible & 1)
		ShowWindow(monitWin, SW_SHOW);
	else
		ShowWindow(monitWin, SW_HIDE);
	MonitUpdate();
}

void OsConsole::MonitOff()
{
	if (monitVisible & 1)
		MonitToggle();
}

// Glue for event Proc
LRESULT APIENTRY OsConsole::MonitWndProcGlue(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam)
{
	OsConsole* This = (OsConsole*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (This == 0)
		return DefWindowProc(hWnd, wMsg, wParam, lParam);
	return This->MonitWndProc(hWnd, wMsg, wParam, lParam);
}

// Window event proc
LRESULT OsConsole::MonitWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (hWnd != monitWin)
		return 0;

	bool run = false;
	bool refresh = false;

	switch (message) {
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		BeginPaint(mainWin, &ps);
		if (!IsRectEmpty(&ps.rcPaint)) // if refresh rect isn't empty
			MonitUpdate();
		EndPaint(mainWin, &ps);
		return 0;
	}
	case WM_CLOSE: MonitOff(); return 0;

	case WM_DESTROY: MonitOff(); return 0;
	case WM_SYSKEYDOWN: return 0;
	case WM_LBUTTONDOWN:
	{
		int xPos = ((int)(short)LOWORD(lParam));
		int yPos = ((int)(short)HIWORD(lParam));
		if (!monit.CpcHeld()) run = true;
		MonitUpdate();
	}
	break;
	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}
	if (refresh)
		MonitUpdate();
	//if (run)
	//  SetWindow();
	return 0;
}


bool OsConsole::CpcHeld()const { 
	if (!(monitVisible & 1)) false;
	return monit.CpcHeld();
}

void OsConsole::MonitorTrap()
{
	monit.MonitorTrap();
}
void OsConsole::MonitUpdate()
{
	if (!(monitVisible & 1)) return;

	monit.Refresh();

	// source pixels from the Monitor
	CpcRect monitRect = monit.WinRect();
	int width = monitRect.Width();
	int height = monitRect.Height();
	CpcDib32 monitCpcDib = monit.Dib();

	// Make a bitmap info for the source pixels
	BITMAPINFO bmi;
	memset(&bmi, 0, sizeof(BITMAPINFO)); //lint -e668
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = width;
	bmi.bmiHeader.biHeight = -height;   // must be positive if we want a bottom-up DIB with origin in the lower-left
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage = 0;
	bmi.bmiHeader.biXPelsPerMeter = 0;
	bmi.bmiHeader.biYPelsPerMeter = 0;
	bmi.bmiHeader.biClrUsed = 0;
	bmi.bmiHeader.biClrImportant = 0;

	// destination is the window
	HDC hdc = GetDC(monitWin); // get window DC (drawing context)
	// Blit the pixels
	StretchDIBits(hdc, 0, 0, width, height, 0, 0, width, height, monitCpcDib.Pt(), &bmi, DIB_RGB_COLORS, SRCCOPY);
	ReleaseDC(monitWin, hdc);
}

void OsConsole::SleepUs(UINT32 us)
{
	Sleep(us / 1000);
}

unsigned long OsConsole::TicksMs()
{
	return timeGetTime();
}

unsigned long OsConsole::GetCrtUs()
{
	return timeGetTime() * 1000;
}

// This starts double buffer sound
void OsConsole::StartSound()
{
	dsBuffer->Play(0, 0, DSBPLAY_LOOPING);
}

// This stops asynchronous sound play
void OsConsole::StopSound()
{
	dsBuffer->Stop();
}


// This is called by the PSG when a buffer is full of sound data to play ASAP
// Copies psgBuffSize bytes of sound data into the DS buffer
// direct sound:
// uses a secondary buffer (opposed to primary buffer which is the main sound buffer after mixing all secondary buffers)
// the buffer is a ring buffer with a Play and Write cursors which indicate the currently commited section
// The write cursor does not tell where to write - that's our job to maintain that.
void OsConsole::SoundBuffDone()
{
	HRESULT result;
	unsigned long dst0 = writeCursor, dst1 = writeCursor + psgBuffSize - 1;
	DWORD crtPlayCursor, crtWriteCursor;
	do {
		result = dsBuffer->GetCurrentPosition(&crtPlayCursor, &crtWriteCursor);
	} while (waitForSound && result == DS_OK && ((crtPlayCursor >= dst0 && crtPlayCursor <= dst1) || (crtWriteCursor >= dst0 && crtWriteCursor <= dst1)));

	// lock buffer
	void* pt1, * pt2;
	unsigned long sz1, sz2;
	if ((result = dsBuffer->Lock(writeCursor, psgBuffSize, &pt1, &sz1, &pt2, &sz2, 0)) != DS_OK) return;

	// copy PSG buffer to DirectSound buffer
	if (pt1) memcpy(pt1, psgBuff, sz1);
	if (pt2) memcpy(pt2, psgBuff + sz1, sz2);

	// Unlock buffer
	dsBuffer->Unlock(pt1, sz1, pt2, sz2);

	// update our write cursor
	writeCursor = (writeCursor + psgBuffSize) % dsBuffSize;
}

void OsConsole::OsVol(const CpcSoundLevel& val)
{
	dsBuffer->SetVolume(-1000 * (7 - val));
	//DSBVOLUME_MIN+(float)(DSBVOLUME_MAX-DSBVOLUME_MIN)*((float)val/7));
}

//------------------------------------------------------------------------
// Direct sound stuff
//------------------------------------------------------------------------

void OsConsole::DsInit()
{
	HINSTANCE dxLib = LoadLibrary("DSOUND.DLL");
	ASSERT_THROW(dxLib)("Direct sound not found");

	FreeLibrary(dxLib);

	// create a direct sound interface. If this fails, no sound hardware is available
	if (DirectSoundCreate(0, &dsInstance, 0) != DS_OK)
		ERR_THROW("Sound init failed");

	// initialise normal co-operation level (means that sound device can be shared
	// with other programs.
	if (dsInstance->SetCooperativeLevel(OsConsole::GetHWnd(), DSSCL_PRIORITY) != DS_OK)
		ERR_THROW("Sound init failed");

	// get access to primary buffer to set the sound parameters
	DSBUFFERDESC dsBufferDesc;
	LPDIRECTSOUNDBUFFER primaryBuffer;
	memset(&dsBufferDesc, 0, sizeof(DSBUFFERDESC));
	dsBufferDesc.dwSize = sizeof(DSBUFFERDESC);
	dsBufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
	if (dsInstance->CreateSoundBuffer(&dsBufferDesc, &primaryBuffer, 0) != DS_OK)
		ERR_THROW("Sound init failed");
	DWORD nBytes;
	primaryBuffer->GetFormat(&dsBufferFormat, sizeof(WAVEFORMATEX), &nBytes);
	dsBufferFormat.wFormatTag = WAVE_FORMAT_PCM;
	dsBufferFormat.nChannels = nbChannels;
	dsBufferFormat.nSamplesPerSec = sampleRate;
	dsBufferFormat.nBlockAlign = (sampleSize >> 3)* dsBufferFormat.nChannels;
	dsBufferFormat.wBitsPerSample = sampleSize;
	dsBufferFormat.nAvgBytesPerSec = dsBufferFormat.nBlockAlign * dsBufferFormat.nSamplesPerSec;
	dsBufferFormat.cbSize = 0;

	primaryBuffer->SetFormat(&dsBufferFormat);
	primaryBuffer->Release();

	// double buffer means twice the size
	int buffSize = buffSamples * bytesPerSample * 2;

	// channel secondary buffer
	memset(&dsBufferDesc, 0, sizeof(DSBUFFERDESC));
	dsBufferDesc.dwSize = sizeof(DSBUFFERDESC);
	dsBufferDesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_CTRLVOLUME;
	dsBufferDesc.dwBufferBytes = buffSize;
	dsBufferDesc.lpwfxFormat = &dsBufferFormat;
	dsInstance->CreateSoundBuffer(&dsBufferDesc, &dsBuffer, 0);

	// Get buffer size
	DSBCAPS buffCaps;
	buffCaps.dwSize = sizeof(DSBCAPS);
	if (dsBuffer->GetCaps(&buffCaps) != DS_OK)
		ERR_THROW("Sound init failed");
	dsBuffSize = buffCaps.dwBufferBytes;

	DsClearBuff();

	dsBuffer->Play(0, 0, DSBPLAY_LOOPING);
}

void OsConsole::DsClearBuff()
{
	// lock buffer
	void* pt1, * pt2;
	unsigned long sz1, sz2;
	if (dsBuffer->Lock(0, 0, &pt1, &sz1, &pt2, &sz2, DSBLOCK_ENTIREBUFFER) != DS_OK) return;
	// clear buffer
	if (pt1) memset(pt1, 0x080, sz1);
	if (pt2) memset(pt2, 0x080, sz2);
	// Unlock buffer
	dsBuffer->Unlock(pt1, sz1, pt2, sz2);
}

bool OsConsole::CanStereo()const
{
	return true;
}
bool OsConsole::CanSound()const
{
	return true;
}
