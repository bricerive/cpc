#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// \file pckeystr.h - Name of PC keys corresponding to Direct Input keycodes

/// Name of PC keys corresponding to Direct Input keycodes
static char *pcKeyStr[256]={
    "00","DIK_ESCAPE","DIK_1","DIK_2","DIK_3","DIK_4","DIK_5","DIK_6",
    "DIK_7","DIK_8","DIK_9","DIK_0","DIK_MINUS","DIK_EQUALS","DIK_BACK","DIK_TAB",
    "DIK_Q","DIK_W","DIK_E","DIK_R","DIK_T","DIK_Y","DIK_U","DIK_I",
    "DIK_O","DIK_P","DIK_LBRACKET","DIK_RBRACKET","DIK_RETURN","DIK_LCONTROL","DIK_A","DIK_S",
    "DIK_D","DIK_F","DIK_G","DIK_H","DIK_J","DIK_K","DIK_L","DIK_SEMICOLON",
    "DIK_APOSTROPHE","DIK_GRAVE","DIK_LSHIFT","DIK_BACKSLASH","DIK_Z","DIK_X","DIK_C","DIK_V",
    "DIK_B","DIK_N","DIK_M","DIK_COMMA","DIK_PERIOD","DIK_SLASH","DIK_RSHIFT","DIK_MULTIPLY",
    "DIK_LMENU","DIK_SPACE","DIK_CAPITAL","DIK_F1","DIK_F2","DIK_F3","DIK_F4","DIK_F5",
    "DIK_F6","DIK_F7","DIK_F8","DIK_F9","DIK_F10","DIK_NUMLOCK","DIK_SCROLL","DIK_NUMPAD7",
    "DIK_NUMPAD8","DIK_NUMPAD9","DIK_SUBTRACT","DIK_NUMPAD4","DIK_NUMPAD5","DIK_NUMPAD6","DIK_ADD","DIK_NUMPAD1",
    "DIK_NUMPAD2","DIK_NUMPAD3","DIK_NUMPAD0","DIK_DECIMAL","54","55","56","DIK_F11",
    "DIK_F12","59","5A","5B","5C","5D","5E","5F",
    "60","61","62","63","DIK_F13","DIK_F14","DIK_F15","67",
    "68","69","6A","6B","6C","6D","6E","6F",
    "DIK_KANA","71","72","73","74","75","76","77",
    "78","DIK_CONVERT","7A","DIK_NOCONVERT","7C","DIK_YEN","7E","7F",
    "80","81","82","83","84","85","86","87",
    "88","89","8A","8B","8C","DIK_NUMPADEQUALS","8E","8F",
    "DIK_CIRCUMFLEX","DIK_AT","DIK_COLON","DIK_UNDERLINE","DIK_KANJI","DIK_STOP","DIK_AX","DIK_UNLABELED",
    "98","99","9A","9B","DIK_NUMPADENTER","DIK_RCONTROL","9E","9F",
    "A0","A1","A2","A3","A4","A5","A6","A7",
    "A8","A9","AA","AB","AC","AD","AE","AF",
    "B0","B1","B2","DIK_NUMPADCOMMA","B4","DIK_DIVIDE","B6","DIK_SYSRQ",
    "DIK_RMENU","B9","BA","BB","BC","BD","BE","BF",
    "C0","C1","C2","C3","C4","DIK_PAUSE","C6","DIK_HOME",
    "DIK_UP","DIK_PRIOR","CA","DIK_LEFT","CC","DIK_RIGHT","CE","DIK_END",
    "DIK_DOWN","DIK_NEXT","DIK_INSERT","DIK_DELETE","D4","D5","D6","D7",
    "D8","D9","DA","DIK_LWIN","DIK_RWIN","DIK_APPS","DIK_POWER","DIK_SLEEP",
    "E0","E1","E2","E3","E4","E5","E6","E7",
    "E8","E9","EE","EB","EC","ED","EE","EF",
    "F0","F1","F2","F3","F4","F5","F6","F7",
    "F8","F9","FF","FB","FC","FD","FE","FF",
};
