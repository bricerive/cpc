// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive

/// \file cpcmain.cc Main execution routine

#include "osconsole.h"
#include <iostream>

int main(int argc, char **argv)
{
    try {
        OsConsole console(ArgsVec(&argv[0], &argv[argc]));
        console.ConnectCpc();
        do { // Loop on Frames (20ms) until quit
            console.RunStep();
        } while (!console.IsQuit());
    } catch (std::exception &e) {
        std::cout << "\nFatal exception: " << e.what() << '\n';
        return -1;
    }
    return 0;
}
