#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// \file osconsole.h - Linux version

#include "monitor.h"

#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include "cpcguiconsole.h"
#include "progdb.h"

class Cpc;

struct Indicators
{
    Indicators(int tick): crtCol(0), lastSpeed(0), lastWasted(0), lastDriveA(0), lastDriveB(0), lastRemaining(0), msgTick(tick) { setbuf(stdout, 0); lastMsg[0] = 0; }
    void Msg(const char *msg, int tick, bool nl = false) { Update(lastSpeed, lastWasted, lastDriveA, lastDriveB, lastRemaining, msg, tick); if (nl) crtCol = 0; }
    void Speed(float speed, float wasted) const { Update(speed, wasted, lastDriveA, lastDriveB, lastRemaining, lastMsg); }
    void Drive(int which, int state) { Update(lastSpeed, lastWasted, which ? lastDriveA : state, which ? state : lastDriveB, lastRemaining, lastMsg); }
    void Tick(int tick);
    
private:
    void Update(float speed, float wasted, bool driveA, bool driveB, int remaining, const char *msg, int tick = 0) const;
    mutable int crtCol;
    mutable float lastSpeed, lastWasted;
    mutable int lastDriveA, lastDriveB, lastRemaining;
    mutable char lastMsg[1024];
    mutable int msgTick;
};

typedef enum {
    ERR_CON_CONFIG_LOAD,
    ERR_CON_DSKOPEN,
    MSG_MODELCHANGE,
    ERR_CON_PRNTOPEN
} MsgId;

/// Linux version of CPC++ console
class OsConsole: public CpcGuiConsole, public MonitorConsole {
public:
    OsConsole(int argc, char **argv);
    ~OsConsole();
    
    // Implement MonitorConsole interface
    virtual bool GetHexValue(const char *prompt, int &val) { return false; };
    virtual void DrawAmstradText(CpcDib32 dst, int x, int y, unsigned long color, const char *str) {}
    virtual const RAW32 *GetP32() {return P32();}
    
    // Implement CpcConsole interface
    virtual void FrameReady();
    virtual void DiskActivity(int which, int state);
    virtual void PutPrinter(BYTE val);
    virtual void MonitorEventLoop();
    virtual void BuffDone();
    
    bool CanSound() const { return true; };
    bool CanStereo() const { return true; };
    std::string GetClipboardString() { return ""; };
    void MonitOff() {};
    void MonitToggle() {};
    void CheckEvents(bool monitFlags) {};
    void ConnectCpc() { CpcGuiConsole::ConnectCpc(); };
    void PutError(const std::string &str) {};
    void CmdChecked(CpcCmd cmd, bool checked) {};
    void CmdEnabled(CpcCmd, bool enabled) {};
    bool FilterFile(const std::string &path, FileType type) { return false; };
    void CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size) { data = 0; size = 0; };
    void SetFileType(const std::string &path, FileType type) {};
    void DoStartupDialog() {};
    void DoKeyboardDialog() {};
    void DoRomDialog() {};
    void DoAboutDialog() {};
    bool GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path) { return true; };
    void AddItemToRunMenu(int drive, const std::string &item, bool enabled) {};
    void EmptyRunMenus() {};
    void SavePICTFile() {};
    void CopyToClipboard() {};
    void PlayCpcSound(CpcSound sound) {};
    bool SelectFile(std::string &path, FileType &type, int &option) { return false; };
    void LaunchUrl(const std::string &url) {};
    void WindowTitle(const std::string &title);

    int ConfirmDlg(CpcGuiMsgId, ...);
    void SetupSound();
    void WaitForSound(bool val){ waitForSound = val; }
    void StartSound() {};
    void StopSound() {};
    void OsVol(int val);
    bool WaitForSound() const { return waitForSound; }

    unsigned long TicksMs() const;

    void CheckEvents(int monitFlag = 0);
    std::string GetStr(CpcGuiMsgId errId);
    void Update();
    // Timer stuff
    void SleepUs(UINT32 us)override;
    unsigned long GetCrtUs()override;
    void CheckKbd();
    void SpeedReport(UINT32 elapsed, UINT32 executed, UINT32 expected) const;
    void SaveSnapshot(const std::string &path, const BYTE *buff);

private:
    void SetRenderingInfo(CpcBase::RenderMode rMode, CpcBase::RenderInfo &renderInfo)const;

    void Msg(const char *msg);
    void ErrReport(int errIdInt, ...);
        
    void Splash();
    void SetupSignalHandlers();
    void SetupDisplay();
    
    int MessageBoxDrive();
    bool MessageBoxConfirmation(const char *, const char *,
                                const char *, const char *);
    
    
    void OnDropFile(const SDL_Event &);
    void OnReset();
        
    OsConsole(const OsConsole &);
    OsConsole &operator=(const OsConsole &);
    
    // Control
    
    FILE *prOut;
    unsigned long crtTick;
    bool startupFlag;
    
    SDL_Window *mainWin;
    SDL_Renderer *mainRend;
    SDL_Texture *mainTex;
    
    // GA write pointer (XImage or offscreen)
    char *pixBuff;
    
    Indicators indicators;
    
    int pauseOn;
    
    // Keyboard state
    int altOn;
    int ctlOn;
    int nbKSyms;
    
    // Keyboard mapping
    int ConvKey(SDL_Keycode keycode);
    
    void DialogGetStr(char *str, const char *format, ...);
    
    virtual void Put(const std::string &str);
    virtual std::string GetStr(int errId);
    static void Printer(void *param, BYTE val);
    
    void DoEvents();
    
    // Signals handling
    static void sigHndl(int sig);
    static bool signalCaught;
    
    ProgDb progDb;
    
    // Config
    void Digiblaster(int val);
    void CrtcModel(int val);
    
    void DriveADiskPath(const char *val);
    void DriveBDiskPath(const char *val);
    void SnapshotPath(const char *val);
    void StartupComm(const char *val);
    void CpcDskDir(const char *val);
    void PrinterFile(const char *val);
    void ProgDbPath(const char *val);
    void CpmBoot(int val);
    void Constructor(int val);
    void VideoFreq(int val);
    void MaxSpeed(int val);
    void WaitForSound(int val);
    void MakeSounds(int val);
    void SoundLevel(int val);
    void VSyncHReset(int val);
    void FdcRealTime(int val);
    void ProtDdTrick(int val);
    void Stereo(int val);
    void SoundChannels(int val);
    void PrinterOutput(int val);
    
    // OsPsg stuff
    int outputFreq;
    bool waitForSound;
    int audioOutput;
    int vol;
    UINT8 *psgBuff;
    int buffEndOffset;
    int buffNBytes;
    int canSynch;
    long buffEndTime;
    static const int BUFF_SIZE = 256;
    SDL_AudioDeviceID audioDev;
    
    static const int SND_QUEUE_PAGES = 4;
    static const int SND_QUEUE_BLK = BUFF_SIZE * 4;
    UINT8 *snd_queue;
    UINT8 snd_queue_rd;
    UINT8 snd_queue_wr;
    
    int AudioOutput() const { return audioOutput; }
    static void soundCallback(void *userdata, Uint8 *stream, int len);
    
};

