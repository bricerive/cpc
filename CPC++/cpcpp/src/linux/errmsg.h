// CPC++  - An Amstrad CPC emulator.// Copyright (C) 1996-2010 Brice Rive//// This program is free software: you can redistribute it and/or modify// it under the terms of the GNU General Public License as published by// the Free Software Foundation, either version 3 of the License, or// (at your option) any later version.//// This program is distributed in the hope that it will be useful,// but WITHOUT ANY WARRANTY; without even the implied warranty of// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the// GNU General Public License for more details.//// You should have received a copy of the GNU General Public License// along with this program.  If not, see <http://www.gnu.org/licenses/>.// errmsg.h unix versionstatic const char *gErrors[]= {
    "Cannot open config file",
    "Cannot open disk image file %s",
    "Cannot load snapshot %s",
    "Changing the model requires a reset of the virtual CPC."
    "Cannot open printer output file"};
