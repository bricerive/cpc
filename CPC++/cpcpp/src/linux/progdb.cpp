// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// progdb.cpp - Program database

#include "progdb.h"
#include "infra/error.h"
#include "infra/log.h"

extern "C" {
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
    extern int close(int fd);
}

using namespace boost;

#define MAX_REC 3000

void ProgDb::DbPath(const char *path)
{
    char *pt;
    struct stat st;
    FILE *fd;

    delete [] db;
    delete [] records;
    delete [] dbPath;
    dbPath = new char[strlen(path) + 1];
    strcpy(dbPath,path);

    if (stat(dbPath, &st) < 0) {
        LOG_ERROR("Could not stat prog DB %s", dbPath);
        return;
    }
    ASSERT_THROW((fd = fopen(dbPath, "r")))(format("Could not open prog DB %s") %dbPath);
    db = new char[st.st_size + 1];
    records = new PROGDB_REC[MAX_REC];
    ASSERT_THROW(fread(db, 1, static_cast<int>(st.st_size), fd) == st.st_size) (format("Could not read prog DB %s") %dbPath);
    fclose(fd);
    db[st.st_size] = 0;
    pt = db;
    index = 0;
    for (;;) {
        if (!*pt)
            return;
        if (*pt == '#')
            while (*pt && *pt++ != '\n')
                ;
        else {
            records[index].name = pt;
            for (; *pt && *pt != ','; pt++)
                ;
            ASSERT_THROW(*pt)(format("ProgDb: incomplete record [%s]") %pt);
            *pt++ = 0;
            records[index].disk = pt;
            for (; *pt && *pt != ','; pt++);
            ASSERT_THROW(*pt)(format("ProgDb: incomplete record [%s]") %pt);
            *pt++ = 0;
            records[index].comm = pt;
            for (; *pt && *pt != ','; pt++);
            ASSERT_THROW(*pt)(format("ProgDb: incomplete record [%s]") %pt);
            *pt++ = 0;
            records[index].rem = pt;
            for (; *pt && *pt != '\n'; pt++);
            *pt++ = 0;
            if (++index >= MAX_REC) {
                LOG_ERROR("Too many records ");
                return;
            }
        }
    }
}

ProgDb::ProgDb()
: dbPath(0), db(0), index(0), records(0)
{
}

ProgDb::~ProgDb()
{
    delete [] db;
    delete [] records;
    delete [] dbPath;
}

void ProgDb::GetProgRec(char *str, char *dsk, char *com)
{
    int found = -1;

    for (int i = 0; found == -1 && i < index; i++)
        if (!strncmp(records[i].name, str,
                    std::max(strlen(records[i].name), strlen(str))))
            found = i;
    ASSERT_THROW(found != -1)(format("Prog not found %s")%str);
    LOG_STATUS("Prog Notes: %s", records[found].rem);
    sprintf(dsk, "%s", records[found].disk);
    sprintf(com, "%s\r", records[found].comm);
}

void ProgDb::List(char *mask)
{
    int i, match;
    char *mp, *np;

    for (i= 0; i < index; i++) {
        np = records[i].name;
        match = 1;
        for (mp= mask; *mp && *np && match; mp++) {
            switch (*mp) {
                case '?':
                    np++;
                    break;
                case '*':
                    if (*mp + 1)
                        while (*np && *np != *(mp + 1))
                            np++;
                    else
                        while (*np)
                            np++;
                    break;
                default:
                    if (*mp != *np)
                        match = 0;
                    np++;
                    break;
            }
        }
        if (match && *np == 0 && (*mp == 0 || (*(mp + 1) == 0 && *mp == '*')))
            LOG_STATUS(records[i].name);
    }
}
