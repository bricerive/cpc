// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// osconsole.cpp - Unix Version

#include "osconsole.h"
#include "centro.h"
#include "cpc.h"
#include "psg.h"
#include "drive.h"
#include "kbd.h"
#include "ga.h"
#include "z80.h"
#include "pio.h"
#include "crtc.h"
#include "progdb.h"
#include "fdc.h"
#include "cpcfile.h"
#include "errmsg.h"
#include "cpc_state.h"
#include "sna.h"
#include "console_shared.h"

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>

extern "C" int usleep(unsigned int useconds);
extern "C" int gethostname(char *name, size_t namelen);

using namespace std;

static const int WIDTH = Ga::HVIS * 16;
static const int HEIGHT = Ga::VVIS * 2;


int OsConsole::ConvKey(SDL_Keycode keycode)
{
    // CPC Key codes
    // { = Numeric Keypad (function keys)
    // ( = Joystick 0
    // [ = Joystick 1
    //    | 0      1      2      3      4      5      6      7
    //-----------------------------------------------------------
    //  0 | UP     RIGHT  DOWN   {9     {6     {3     {ENTER {.
    //  8 | LEFT   COPY   {7     {8     {5     {1     {2     {0
    // 16 | CLR    [      ENTER  ]      {4     SHIFT  \      CTRL
    // 24 | |      -      @      P      ;      :      /      .
    // 32 | 0      9      O      I      L      K      M      ,
    // 40 | 8      7      U      Y      H      J      N      SPACE
    // 48 | 6      5      R      T      G      F      B      V
    //    |[UP    [DOWN  [LEFT  [RIGHT [FIRE2 [FIRE1 [SPARE
    // 56 | 4      3      E      W      S      D      C      X
    // 64 | 1      2      ESC    Q      TAB    A      CAPS   Z
    // 72 | (UP    (DOWN  (LEFT  (RIGHT (FIRE2 (FIRE1 (SPARE DEL
    static const int keyConvNbKeys= 80;
    static int keyConv0[keyConvNbKeys][4] = {
        //  0 | UP     RIGHT  DOWN   {9     {6     {3     {ENTER {.
        {SDLK_UP, 0}, {SDLK_RIGHT, 0}, {SDLK_DOWN, 0}, {SDLK_KP_9, 0},
        {SDLK_KP_6, 0}, {SDLK_KP_3, 0},
        {SDLK_KP_ENTER, 0}, {SDLK_KP_DECIMAL, 0},
        //  8 | LEFT   COPY   {7     {8     {5     {1     {2     {0
        {SDLK_LEFT, 0}, {SDLK_LALT, 0},
        {SDLK_KP_7, 0}, {SDLK_KP_8, 0},
        {SDLK_KP_5, 0}, {SDLK_KP_1, 0},
        {SDLK_KP_2, 0}, {SDLK_KP_0, 0},
        // 16 | CLR    [      ENTER  ]      {4     SHIFT  \      CTRL
        {SDLK_INSERT, 0}, {SDLK_RIGHTBRACKET, SDLK_KP_RIGHTBRACE, 0},
        {SDLK_RETURN, 0}, {SDLK_BACKQUOTE, SDLK_BACKQUOTE, 0},
        {SDLK_KP_4, 0}, {SDLK_LSHIFT, SDLK_RSHIFT, 0},
        {SDLK_BACKSLASH, SDLK_SLASH, 0}, {SDLK_LCTRL, SDLK_RCTRL, 0},
        // 24 | |      -      @      P      ;      :      /      .
        {SDLK_PLUS, SDLK_EQUALS, 0}, {SDLK_MINUS, SDLK_UNDERSCORE, 0},
        {SDLK_LEFTBRACKET, SDLK_LEFTBRACKET, 0}, {SDLK_p, SDLK_p, 0},
        {SDLK_QUOTEDBL, SDLK_QUOTE, 0}, {SDLK_COLON, SDLK_SEMICOLON, 0},
        {SDLK_SLASH, SDLK_QUESTION, 0}, {SDLK_PERIOD, SDLK_GREATER, 0},
        // 32 | 0      9      O      I      L      K      M      ,
        {SDLK_0, SDLK_RIGHTPAREN, 0}, {SDLK_9, SDLK_LEFTPAREN, 0},
        {SDLK_o, SDLK_o, 0}, {SDLK_i, SDLK_i, 0},
        {SDLK_l, SDLK_l, 0}, {SDLK_k, SDLK_k, 0},
        {SDLK_m, SDLK_m, 0}, {SDLK_COMMA, SDLK_LESS, 0},
        // 40 | 8      7      U      Y      H      J      N      SPACE
        {SDLK_8, SDLK_ASTERISK, 0}, {SDLK_7, SDLK_AMPERSAND, 0},
        {SDLK_u, SDLK_u, 0}, {SDLK_y, SDLK_y, 0},
        {SDLK_h, SDLK_h, 0}, {SDLK_j, SDLK_j, 0},
        {SDLK_n, SDLK_n, 0}, {SDLK_SPACE, 0},
        // 48 | 6      5      R      T      G      F      B      V
        //    |[UP    [DOWN  [LEFT  [RIGHT [FIRE2 [FIRE1 [SPARE
        {SDLK_6, SDLK_BACKQUOTE, 0}, {SDLK_5, SDLK_PERCENT, 0},
        {SDLK_r, SDLK_r, 0}, {SDLK_t, SDLK_t, 0},
        {SDLK_g, SDLK_g, 0}, {SDLK_f, SDLK_f, 0},
        {SDLK_b, SDLK_b, 0}, {SDLK_v, SDLK_v, 0},
        // 56 | 4      3      E      W      S      D      C      X
        {SDLK_4, SDLK_DOLLAR, 0}, {SDLK_3, SDLK_BACKSLASH, 0},
        {SDLK_e, SDLK_e, 0}, {SDLK_w, SDLK_w, 0},
        {SDLK_s, SDLK_s, 0}, {SDLK_d, SDLK_d, 0},
        {SDLK_c, SDLK_c, 0}, {SDLK_x, SDLK_x, 0},
        // 64 | 1      2      ESC    Q      TAB    A      CAPS   Z
        {SDLK_1, SDLK_EXCLAIM, 0}, {SDLK_2, SDLK_AT, 0},
        {SDLK_ESCAPE, 0}, {SDLK_q, SDLK_q, 0},
        {SDLK_TAB, 0}, {SDLK_a, SDLK_a, 0},
        {SDLK_CAPSLOCK, 0}, {SDLK_z, SDLK_z, 0},
        // 72 | (UP    (DOWN  (LEFT  (RIGHT (FIRE2 (FIRE1 (SPARE DEL
        {SDLK_HOME, 0}, {SDLK_END, 0},
        {SDLK_DELETE, 0}, {SDLK_INSERT, 0},
        {SDLK_LGUI, SDLK_RGUI, SDLK_RALT, 0}, {SDLK_AGAIN, 0},
        {1, 0}, {SDLK_BACKSPACE, 0}
    };
    for (int i = 0; i < keyConvNbKeys; i++)
        for (int j = 0; keyConv0[i][j]; j++)
            if ((unsigned int) keyConv0[i][j] == keycode)
                return i;

    return -1;
}

bool OsConsole::signalCaught = false;

void OsConsole::sigHndl(int sig)
{
    LOG_STATUS("Signal caught [%d]...cleaning up", sig);
    signalCaught = true;
    signal(sig, sigHndl); // reset handler
}


OsConsole::OsConsole(int argc, char **argv)
    : CpcGuiConsole(Args(&argv[0], &argv[argc]))
    , prOut(0)
    , crtTick(0)
    , startupFlag(true)
    , pixBuff(0)
    , indicators(TicksMs())
    , pauseOn(0)
    , altOn(0)
    , ctlOn(0)
    , nbKSyms(0)
    , audioDev(0)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        ERR_THROW("SDL_Init Error:");
    }

    // SDL_Surface * image = SDL_LoadBMP("bar.png");
    // SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, image);
    // int flags = IMG_INIT_PNG;
    // ASSERT_THROW(IMG_Init(flags) & flags == flags);

    Splash();

    SetupSignalHandlers();

    SetupDisplay();

    SetupSound();
}

void OsConsole::Splash()
{
    puts("\n"
            "+----------------------------------------------------+\n"
            "| CPC++ V3.0b0 -- Original version by Brice Rive     |\n"
            "| CPC++ comes with ABSOLUTELY NO WARRANTY            |\n"
            "| For details, see attached README file              |\n"
            "+----------------------------------------------------+"
        );
    puts("\n"
            "              CPC emulator function keys:\n"
            "                Normal    Control   Alt \n"
            " F1 - Reset   : reset     forced    Pause\n"
            " F2 - Config  : reload\n"
            " F3 - \n"
            " F4 - Sound   : down      up        toggle \n"
            " F5 - Disk A  : insert    save      flip \n"
            " F6 - Disk B  : insert    save      flip \n"
            " F7 - SnapShot: load      save \n"
            " F8 - \n"
            " F9 - ProgDb  : run       list \n"
            " F10- Quit    : quit      forced \n"
        );
}

void OsConsole::SetupDisplay()
{
    SDL_CreateWindowAndRenderer(
            WIDTH, HEIGHT, SDL_WINDOW_RESIZABLE,
            &mainWin, &mainRend);

    mainTex = SDL_CreateTexture(mainRend,
            SDL_PIXELFORMAT_ARGB8888,
            SDL_TEXTUREACCESS_STREAMING,
            WIDTH, HEIGHT);

    pixBuff = new char[WIDTH * 4 * HEIGHT];
}

void OsConsole::SetupSignalHandlers()
{
    static const int signals[] = {
        SIGHUP, SIGINT, SIGQUIT, SIGILL , SIGTRAP, SIGABRT, SIGFPE, SIGBUS,
        SIGPIPE, SIGALRM , SIGTERM, SIGURG, SIGTSTP, SIGUSR1, SIGUSR2
    };
    // Catch signals for clean-up
    for (int i = 0; i < sizeof(signals) / sizeof(signals[0]); i++)
        signal(signals[i], sigHndl);
}

OsConsole::~OsConsole()
{
    if (prOut)
        fclose(prOut);

    printf("\n");

    if (audioDev)
        SDL_CloseAudioDevice(audioDev);

    delete [] psgBuff;

    delete [] pixBuff;
    SDL_DestroyWindow(mainWin);
    SDL_DestroyRenderer(mainRend);
    SDL_DestroyTexture(mainTex);
    SDL_Quit();
}


void OsConsole::MonitorEventLoop()
{
}

void OsConsole::SaveSnapshot(const std::string &path, const BYTE *buff)
{

    ofstream out(path.c_str());
    if (out) {
        long byteCount = 256 + ((buff[0x6b]==64)?64:128)*1024; // temporary cut'n'paste from OSX implementation
        out.write(reinterpret_cast<const char *>(buff), byteCount);
    }
}

void OsConsole::FdcRealTime(int val) {
    gConfig.fdcRealTime = val;
    cpc.RealTime(gConfig.fdcRealTime);
}
void OsConsole::PrinterOutput(int val) {
    gConfig.printerOutput = (CpcBase::PrinterMode) val;
    cpc.PrinterOutput(gConfig.printerOutput);
}

void OsConsole::CpmBoot(int val) {
    gConfig.CpmBoot = val;
    cpc.CpmBoot(gConfig.CpmBoot);
}

void OsConsole::Constructor(int val) {
    gConfig.Constructor = val & 7;
    cpc.Constructor(gConfig.Constructor);
}
void OsConsole::Digiblaster(int val) {
    gConfig.HasDigiblaster = val ? 1 : 0;
    if (gConfig.HasDigiblaster)
        cpc.PrinterOutput(CpcBase::PrinterMode::PrinterDigiblaster);
}
void OsConsole::Stereo(int val) {
    gConfig.Stereo = val ? 1 : 0;
    cpc.Stereo(gConfig.Stereo);
}
void OsConsole::WaitForSound(int val) {};

void OsConsole::WindowTitle(const std::string &title)
{
    SDL_SetWindowTitle(mainWin, title.c_str());
}

void OsConsole::MakeSounds(int val) {
    gConfig.MakeSounds = val;
    cpc.SoundOnOff(gConfig.MakeSounds);
}
void OsConsole::SoundChannels(int val) {
    gConfig.soundChannels = val & 7;
    cpc.ChannelsOnOff(
            (gConfig.soundChannels & 1) ? 1 : 0,
            (gConfig.soundChannels & 2) ? 1 : 0,
            (gConfig.soundChannels & 4) ? 1 : 0);
}
void OsConsole::SoundLevel(int val) {
    gConfig.soundLevel= val & 7;
    OsVol(gConfig.soundLevel);
}

void OsConsole::DialogGetStr(char *str, const char *format, ...)
{
    va_list argP;
    fflush(stdin);
    va_start(argP, format);
    char prompt[1024];
    vsprintf(prompt, format, argP);
    indicators.Msg(prompt, TicksMs(), true);
    fgets(str, 128, stdin);
    str[strlen(str) - 1] = 0;
    indicators.Msg("", TicksMs());
}

void OsConsole::Put(const std::string &str)
{
    indicators.Msg(str.c_str(), TicksMs());
}

void OsConsole::Printer(void *param, BYTE val)
{
    ((OsConsole *)param)->PutPrinter(val);
}

void OsConsole::PutPrinter(BYTE val)
{
    if (!prOut) {
        if (!(prOut = fopen(gConfig.PrinterFile, "wb+")))
            ErrReport(ERR_CON_PRNTOPEN);
    }
    if (prOut) {
        fwrite((char *) &val, 1, 1, prOut);
    }
}

std::string OsConsole::GetStr(int errId)
{
    return std::string(gErrors[errId]);
}

void OsConsole::SetRenderingInfo(CpcBase::RenderMode rMode, CpcBase::RenderInfo &renderInfo)const
{
    PixMap **mainWinPixmap = GetPortPixMap(GetWindowPort(mainWin));
    Rect &bounds = (*mainWinPixmap)->bounds;
    int mainWinBPR = (*mainWinPixmap)->rowBytes & 0x3fff;
    Ptr mainWinPt = GetPixBaseAddr(mainWinPixmap) - bounds.left * 32/8 - bounds.top * mainWinBPR;
    renderInfo.base = (unsigned char *)mainWinPt + cpcScrR * mainWinBPR + cpcScrC * 32/8;
    renderInfo.bytesPerRow = mainWinBPR;
    renderInfo.clip.Set(blitY0,blitX0,blitY1-1,blitX1-1);
    renderInfo.renderMode = rMode;
    renderInfo.lut = gConfig.Monochrome? M32(): P32();
}

void OsConsole::Update()
{
    CpcBase::RenderInfo renderInfo;

    renderInfo.base = (UINT8 *) pixBuff;
    renderInfo.clip.Set(0, 0, Ga::CPC_SCR_H - 1, Ga::CPC_SCR_W - 1);
    renderInfo.bytesPerRow = WIDTH * 4;
    renderInfo.lut = gConfig.Monochrome ? M32() : P32();

    cpc.RenderFrame(renderInfo, true);
}

void OsConsole::FrameReady()
{
    Update();
    SDL_UpdateTexture(mainTex, NULL, pixBuff, WIDTH * 4);
    SDL_SetRenderDrawColor(mainRend, 0, 0, 0, 255);
    SDL_RenderClear(mainRend);
    SDL_RenderCopy(mainRend, mainTex, NULL, NULL);
    SDL_RenderPresent(mainRend);
}

void OsConsole::CheckEvents(int monitFlag)
{
    do {
        indicators.Tick(TicksMs());
        DoEvents();
    } while (pauseOn);
}

void OsConsole::CheckKbd()
{
    CheckEvents();
}

int OsConsole::MessageBoxDrive()
{
    const SDL_MessageBoxButtonData buttons[] = {
        {SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, -1, "Cancel"},
        {0, 0, "Drive A"},
        {0, 1, "Drive B"}
    };

    const SDL_MessageBoxData messageboxdata = {
        SDL_MESSAGEBOX_INFORMATION,
        NULL,
        "Disk Image attach destination",
        "Please select which drive attach to the disk image:",
        sizeof(buttons) / sizeof(buttons[0]),
        buttons,
        NULL
    };

    int buttonid;

    SDL_ShowMessageBox(&messageboxdata, &buttonid);

    return buttonid;
}

std::string OsConsole::GetStr(CpcGuiMsgId msgId)
{
    switch (msgId) {
        case MSG_QUITCONFIRM:
            return "Are you sure to quit the emulator?";
    }

    return "";
}

int OsConsole::ConfirmDlg(CpcGuiMsgId msgId, ...)
{
    return MessageBoxConfirmation(
        "CPC", GetStr(msgId).c_str(),
        "No", "Yes");
}

bool OsConsole::MessageBoxConfirmation(
        const char *title, const char *message,
        const char *cancel, const char *confirm)
{
    const SDL_MessageBoxButtonData buttons[] = {
        {SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, -1, cancel},
        {0, 1, confirm},
    };

    const SDL_MessageBoxData messageboxdata = {
        SDL_MESSAGEBOX_INFORMATION,
        NULL,
        title, message,
        sizeof(buttons) / sizeof(buttons[0]), buttons,
        NULL
    };

    int buttonid;

    SDL_ShowMessageBox(&messageboxdata, &buttonid);

    return buttonid == 1;
}

void OsConsole::OnDropFile(const SDL_Event &event)
{
    // Look at the data
    ifstream in(event.drop.file);
    BYTE data[128];
    in.read(reinterpret_cast<char *>(data), 128);
    if (!in) return;
    if (CpcGuiConsole::IsSnapshot(event.drop.file, data)) {
        LoadSnapshot(event.drop.file);
        return;
    }
    int destination = MessageBoxDrive();
    if (destination != -1)
        LoadDisk(event.drop.file, destination);
}

void OsConsole::OnReset()
{
    if (MessageBoxConfirmation(
                "Reset confirmation",
                "Are you sure to reset CPC++?",
                "No", "Yes")) {
        cpc.Reset();
    }
}

void OsConsole::DoEvents()
{
    char str[128];
    int keyCode;

    if (signalCaught) {
        DoQuit();
        return;
    }

    if (startupFlag) {
        DoStartupEvents();
        startupFlag = false;
    }

    SDL_Keysym *ks;
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                DoQuit();
                break;
            case SDL_DROPFILE:
                OnDropFile(event);
                break;
            case SDL_KEYDOWN:
                ks = &(((SDL_KeyboardEvent *) (&event))->keysym);

                /* First check for the 'special keys' */
                if (ks->sym == SDLK_LALT) altOn = 1;
                if (ks->sym == SDLK_LCTRL) ctlOn = 1;

                // Emulator control keys
                switch (ks->sym) {
                    case SDLK_F1: // Reset
                        if (ctlOn) {
                            cpc.Reset();
                        } else if (altOn) {
                            pauseOn ^= 1;
                            Msg(pauseOn ? "Pause On" : "Pause Off");
                        } else
                            OnReset();
                        break;
                    case SDLK_F4: // Sound
                        if (altOn) {
                            MakeSounds(gConfig.MakeSounds ^ 1);
                        }/* else if (ctlOn)
                            cpc->VolUp();
                            else
                            cpc->VolDown();*/
                        break;
                    case SDLK_F5:    // Drive A
                        if (ctlOn) {
                            // cpc->SaveDisk(0, NULL);
                        } else if (altOn) {
                            //drives[0]->FlipDisk();
                        } else {
                            str[0] = 0;
                            DialogGetStr(str, "Disk A: ");
                            LoadDisk(str, 0);
                        }
                        break;
                    case SDLK_F6:    // Drive B
                        if (ctlOn) {
                            // cpc->SaveDisk(1, NULL);
                        } else if (altOn) {
                            //drives[1]->FlipDisk();
                        } else {
                            str[0] = 0;
                            DialogGetStr(str, "Disk B: ");
                            LoadDisk(str, 1);
                        }
                        break;
                    case SDLK_F7: // SnapShots
                        if (ctlOn) { // Save snapshot
                            str[0] = 0;
                            DialogGetStr(str, "SnapShot to save: ");
                          //  SaveSnapshot(std::string(str));
                        } else { // Load snapshot
                            str[0] = 0;
                            DialogGetStr(str, "SnapShot to load: ");
                            LoadSnapshot(str);
                        }
                        break;
                    case SDLK_F8:
                        break;
                    case SDLK_F9: // Prog Db
                        if (ctlOn) {
                            str[0] = 0;
                            DialogGetStr(str, "Mask: ");
                            progDb.List(str);
                        } else {
                            char dsk[128], com[128];
                            str[0] = 0;
                            DialogGetStr(str, "Program name: ");
                            try {
                                progDb.GetProgRec(str, dsk, com);
                                LoadDisk(dsk, 0);
                                // pendingStr += com;
                            } catch (...) {}
                        }
                        break;
                    case SDLK_F10: // Exit
                        DoQuit();
                        break;
                    default:
                        if ((keyCode = ConvKey(ks->sym)) >= 0) {
                            cpc.KeyDown(keyCode);
                        }
                        break;
                }
                break;
            case SDL_KEYUP:
                ks = &(((SDL_KeyboardEvent *) (&event))->keysym);

                if (ks->sym == SDLK_LALT) altOn = 0;
                if (ks->sym == SDLK_LCTRL) ctlOn = 0;

                if ((keyCode = ConvKey(ks->sym)) >= 0) {
                    cpc.KeyUp(keyCode);
                }
                break;
            default:
                break;
        }
    }
}

void OsConsole::SpeedReport(
        UINT32 expected,
        UINT32 executed,
        UINT32 elapsed) const
{
    static long totalExpected = 0;
    static long totalExecuted = 0;
    static long totalElapsed = 0;

    totalExpected += expected;
    totalExecuted += executed;
    totalElapsed += elapsed;
    if (totalElapsed > 5000000) {
        indicators.Speed(100.0 * totalExpected / totalElapsed,
                         100.0 - 100.0 * totalExecuted / totalElapsed);
        totalExpected = totalExecuted = totalElapsed = 0;
    }
}

void OsConsole::DiskActivity(int which, int state)
{
    indicators.Drive(which, state);
}

void OsConsole::SleepUs(UINT32 us)
{
    usleep(us);
}

unsigned long OsConsole::TicksMs() const
{
    return GetCrtUs() / 1000;
}

unsigned long OsConsole::GetCrtUs()
{
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    return (tv.tv_sec & 0x3FFF) * 1000000 + tv.tv_usec;
}

void OsConsole::BuffDone()
{
    memcpy(&(snd_queue[snd_queue_wr * SND_QUEUE_BLK]), psgBuff, buffNBytes);
    snd_queue_wr = (++snd_queue_wr) % SND_QUEUE_PAGES;
}

void OsConsole::OsVol(int val)
{
}


void OsConsole::soundCallback(void *userdata, Uint8 *stream, int len)
{
    OsConsole *osc = static_cast<OsConsole *>(userdata);

    if (osc->snd_queue_wr == osc->snd_queue_rd) {
        return;
    }

    memcpy(stream, &(osc->snd_queue[osc->snd_queue_rd * SND_QUEUE_BLK]), len);
    osc->snd_queue_rd = (++osc->snd_queue_rd) % SND_QUEUE_PAGES;
}

void OsConsole::SetupSound()
{
    SDL_AudioSpec desired, obtained;
    int frequency = 15625; // To match call at scanline resolution

    memset(&desired, 0, sizeof(desired));
    desired.freq = frequency;
    desired.format = AUDIO_U8;
    desired.channels = 2;
    desired.samples = BUFF_SIZE;
    desired.callback = OsConsole::soundCallback;
    desired.userdata = this;

    audioDev = SDL_OpenAudioDevice(NULL, 0, &desired, &obtained, 0);

    printf("%d\n", obtained.freq);
    printf("%d\n", obtained.format);
    printf("%d\n", obtained.channels);
    printf("%d\n", obtained.samples);
    printf("DEV %d\n", audioDev);

    // Setup interface for PSG
    CpcBase::SoundInfo sndInfo;
    sndInfo.encodeBased = 0;
    sndInfo.canStereo = gConfig.Stereo;
    sndInfo.precision = 1;
    sndInfo.frequencyHz = frequency;
    cpc.SetSoundInfo(sndInfo);

    // Set buffer
    buffNBytes = BUFF_SIZE * (sndInfo.canStereo ? 2 : 1);
    psgBuff = new UINT8[buffNBytes];
    cpc.SndBuff(psgBuff, BUFF_SIZE);

    snd_queue = new UINT8[SND_QUEUE_BLK * SND_QUEUE_PAGES];
    snd_queue_rd = 0;
    snd_queue_wr = 0;

    if (audioDev)
        SDL_PauseAudioDevice(audioDev, 0);
}

void Indicators::Update(float speed, float wasted, bool driveA, bool driveB, int remaining, const char *msg, int tick) const
{
    lastSpeed = speed;
    lastWasted = wasted;
    lastDriveA = driveA;
    lastDriveB = driveB;
    lastRemaining = remaining;
    if (strcmp(msg, lastMsg)) {
        strcpy(lastMsg, msg);
        msgTick = tick & 0xFFFF;
    }
    char move[1024];
    char str[1024];
    int i;
    int lastCrtCol = crtCol;

    for (i = 0; i < crtCol; i++)
        move[i] = '\b';
    move[i] = 0;
    sprintf(str, "Speed: %7.2f%%  Drives: %c%c",
            speed, driveA ? 'A' : '_', driveB ? 'B' : '_');
    if (remaining > 0)
        sprintf(str, "%s  TimeLeft: %2dmn", str, remaining);
    sprintf(str, "%s  Msg: %s", str, msg);
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1]= 0;
    crtCol = strlen(str);
    fputs(move, stdout);
    fputs(str, stdout);
    if (crtCol < lastCrtCol) {
        for (i = crtCol; i < lastCrtCol; i++) {
            str[i - crtCol] = ' ';
            move[i - crtCol] = '\b';
        }
        str[i - crtCol]= move[i - crtCol]= 0;
        fputs(str, stdout);
        fputs(move, stdout);
    }
}

void Indicators::Tick(int tick)
{
    const int MSG_TIMEOUT= 2000;
    if (strlen(lastMsg) && (((tick & 0xFFFF + 0x10000) - msgTick) & 0xFFFF) > MSG_TIMEOUT)
        Msg("", tick);
}

void OsConsole::Msg(const char *msg)
{
    indicators.Msg(msg, TicksMs(), true);
}

void OsConsole::ErrReport(int errIdInt, ...)
{
    va_list argP;
    va_start(argP, errIdInt);
    CpcGuiMsgId errId = (CpcGuiMsgId)errIdInt;
    std::string format = GetStr(errId);
    char str[128];
    vsprintf(str, format.c_str(), argP);
    va_end(argP);
    Msg(str);
}
