#pragma once
// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// progdb.h - Program database

typedef struct {
    char *name;
    char *disk;
    char *comm;
    char *rem;
} PROGDB_REC;

class ProgDb {
    public:
        ProgDb();
        virtual ~ProgDb();
        void GetProgRec(char *str, char *dsk, char *com);
        void List(char *mask);
        void DbPath(const char *val);
        const char *DbPath() const { return dbPath; }
    private:
        ProgDb(const ProgDb&rhs);
        ProgDb &operator=(const ProgDb &);
        char *dbPath;
        char *db;
        int index;
        PROGDB_REC *records;
};
