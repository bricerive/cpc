// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Monitor.cpp - CPC monitor
#include "amstrad_text.h"
#include "monitor.h"
#include "disass.h"
#include "cpc_state.h"
#include "console_shared.h"
#include <string.h>
#include <ctype.h>

static const int MONITOR_ROWS=50;
static const int MONITOR_COLS=80;
static const int MONITOR_FONT_SIZE=8;

static const int MONITOR_CPU_ROW=0;
static const int MONITOR_CPU_COL=0;

// Monitor window event handling
void Monitor::MouseDown(int x, int y) {
    cpcHeld=true;
    if (buttons.Click(x,y))
        cpcHeld = false;
}

// key==0 -> F1
void Monitor::FuncKey(int key, bool shift)
{
    bool result=false;
    switch (key) {
        case 4: // F5
            result = true; //RUN
            break;
        case 9: // F10
            cpc->AddConditionCpuBreak(lastCpu.pc+DisAss::Disassemble(cpc->GetMmu(), lastCpu.pc));
            result = true;
            break;
        case 10: // F11
            if (shift)
                cpc->AddConditionCpuStack(lastCpu.sp+2);
            else
                cpc->AddConditionCpuStep();
            result = true;
            break;
    }
    if (result)
        cpcHeld = false;
}

bool Monitor::Buttons::Click(unsigned int x, unsigned int y)
{
    for (unsigned int i=0; i<nButtons; i++) {
        Button &button = buttons[i];
        if (button.Clicked(x,y)) {
            return button.action();
        }
    }
    return false;
}

bool Monitor::Buttons::Button::Clicked(unsigned int x, unsigned int y)const
{
    return x>=c*8 && x<=(c+strlen(caption))*8+7 && y>=r*8 && y<=r*8+7;
}

// Draw a 1 bit register
void Monitor::DrawRegister1(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%d", val & 1);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%d", lastVal & 1);
    MonitorText(row+1,col,str,White);
}

// Draw a 2 bit register
void Monitor::DrawRegister2(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str,32,"%d",val&3);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%d", lastVal & 3);
    MonitorText(row+1,col,str,White);
}

// Draw a 3 bit register
void Monitor::DrawRegister3(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%d", val & 7);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%d", lastVal & 7);
    MonitorText(row+1,col,str,White);
}

// Draw a 8 bit register
void Monitor::DrawRegister8(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%d", val & 0XFF);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%d", lastVal & 0XFF);
    MonitorText(row+1,col,str,White);
}

// Draw a 8 bit register
void Monitor::DrawRegister08X(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%02X", val & 0XFF);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%02X", lastVal & 0XFF);
    MonitorText(row+1,col,str,White);
}

// Draw a 8 bit register horizontaly
void Monitor::DrawRegister08XH(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%02X", val & 0xFF);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%02X", lastVal & 0xFF);
    MonitorText(row,col+3,str,White);
}

// Draw a color value
void Monitor::DrawColor(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%02d", val);
    MonitorText(row, col, str, val);
    snprintf(str, 32, "%02d", lastVal);
    MonitorText(row+1, col, str, lastVal);
}

// Draw a 16 bit register
void Monitor::DrawRegister16(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%d", val & 0XFFFF);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%d", lastVal & 0xFFFF);
    MonitorText(row+1,col,str,White);
}

// Draw a 16 bit register hexa
void Monitor::DrawRegister16X(int row, int col, int val, int lastVal)
{
    char str[32];
    snprintf(str, 32, "%04X", val & 0XFFFF);
    MonitorText(row,col,str,(val!=lastVal)?BrightRed:monitForeColor);
    snprintf(str, 32, "%04X", lastVal & 0xFFFF);
    MonitorText(row+1,col,str,White);
}

void Monitor::Buttons::Add(int row, int col, const char *caption, int c0, int c1, Action action)
{
    Button &button = buttons[nButtons++];
    if (nButtons>=MAX_NB_BUTTONS) return;
    button.r = row;
    button.c = col;
    button.c0 = c0;
    button.c1 = c1;
    strcpy(button.caption, caption);
    button.action = action;
    osm.MonitorText(row,col,caption,c0,c1);
}

void Monitor::Refresh()
{
    int row,col;
    char str[128];

    MonitorClear();
    snprintf(str, 128, "lastClick:  x=%03d y=%03d", cpc->LastClickX(), cpc->LastClickY());
    MonitorMsg(str);

    CpcState state;
    cpc->GetState(state);

    // CPU
    //-----
    CpuState &cpu(state.cpu);

    row=MONITOR_CPU_ROW; col=MONITOR_CPU_COL;
    MonitorText(row,col,"  CPU     ",Black,BrightWhite);

    // Main registers
    row=MONITOR_CPU_ROW+1; col=MONITOR_CPU_COL;
    MonitorText(row,col,"AF   BC   DE   HL   ",monitBackColor,monitForeColor);
    DrawRegister16X(row+1,col,cpu.af,lastCpu.af);
    DrawRegister16X(row+1,col+5,cpu.bc,lastCpu.bc);
    DrawRegister16X(row+1,col+10,cpu.de,lastCpu.de);
    DrawRegister16X(row+1,col+15,cpu.hl,lastCpu.hl);
    row=MONITOR_CPU_ROW+4; col=MONITOR_CPU_COL;
    MonitorText(row,col,"AF'  BC'  DE'  HL'  ",monitBackColor,monitForeColor);
    DrawRegister16X(row+1,col,cpu.af2,lastCpu.af2);
    DrawRegister16X(row+1,col+5,cpu.bc2,lastCpu.bc2);
    DrawRegister16X(row+1,col+10,cpu.de2,lastCpu.de2);
    DrawRegister16X(row+1,col+15,cpu.hl2,lastCpu.hl2);
    row=MONITOR_CPU_ROW+7; col=MONITOR_CPU_COL;
    MonitorText(row,col,"IX   IY   SP   PC   ",monitBackColor,monitForeColor);
    DrawRegister16X(row+1,col,cpu.ix,lastCpu.ix);
    DrawRegister16X(row+1,col+5,cpu.iy,lastCpu.iy);
    DrawRegister16X(row+1,col+10,cpu.sp,lastCpu.sp);
    DrawRegister16X(row+1,col+15,cpu.pc,lastCpu.pc);

    // Other register info (flags ...)
    row=MONITOR_CPU_ROW+1; col=MONITOR_CPU_COL+22;
    UINT8 f = cpu.af&0xFF;
    MonitorText(row,col,"Flags   ",monitBackColor,monitForeColor);
    snprintf(str, 128, "%c%c%c%c%c%c%c%c", (f & Z80::S_M) ? 'S' : '.', (f & Z80::Z_M) ? 'Z' : '.',
        (f & 0x20) ? 'X' : '.', (f & Z80::H_M) ? 'H' : '.', (f & 0x08) ? 'X' : '.',
        (f & Z80::PV_M) ? 'P' : '.', (f & Z80::N_M) ? 'N' : '.', (f & Z80::C_M) ? 'C' : '.');
    MonitorText(row+1,col,str);
    f = lastCpu.af&0xFF;
    snprintf(str, 128, "%c%c%c%c%c%c%c%c", (f & Z80::S_M) ? 'S' : '.', (f & Z80::Z_M) ? 'Z' : '.',
        (f & 0x20) ? 'X' : '.', (f & Z80::H_M) ? 'H' : '.', (f & 0x08) ? 'X' : '.',
        (f & Z80::PV_M) ? 'P' : '.', (f & Z80::N_M) ? 'N' : '.', (f & Z80::C_M) ? 'C' : '.');
    MonitorText(row+2,col,str,White);

    row=MONITOR_CPU_ROW+4; col=MONITOR_CPU_COL+22;
    MonitorText(row,col,"I  R  IF",monitBackColor,monitForeColor);
    DrawRegister08X(row+1,col,cpu.i,lastCpu.i);
    DrawRegister08X(row+1,col+3,cpu.r,lastCpu.r);
    DrawRegister1(row+1,col+6,cpu.iff1,lastCpu.iff1);
    DrawRegister1(row+1,col+7,cpu.iff2,lastCpu.iff2);

    row=MONITOR_CPU_ROW+7; col=MONITOR_CPU_COL+22;
    MonitorText(row,col,"IM IT NI",monitBackColor,monitForeColor);
    DrawRegister2(row+1,col,cpu.im,lastCpu.im);
    DrawRegister1(row+1,col+3,cpu.it,lastCpu.it);
    DrawRegister1(row+1,col+6,cpu.nmi,lastCpu.nmi);

    // Stack
    row=MONITOR_CPU_ROW+1; col=MONITOR_CPU_COL+31;
    MonitorText(row,col,"Stack    ",monitBackColor,monitForeColor);
    for (int i=0; i<8; i++) {
        snprintf(str, 128, "%04X %04X", cpu.sp + 14 - 2 * i, cpc->MemRd16(cpu.sp + 14 - 2 * i));
        MonitorText(row+1+i,col,str,(i==7)?-1:Green);
    }

    // Execution (PC, code...)
    row=MONITOR_CPU_ROW+1; col=MONITOR_CPU_COL+41;
    MonitorText(row,col,"Execution    ",monitBackColor,monitForeColor);
    snprintf(str, 128, "%04X ", lastCpu.pc);
    MonitorText(row+1,col,str,White);
    DisAss::Disassemble(cpc->GetMmu(), lastCpu.pc, str, 128, true, lastCpu.bc);
    SizeStr(str,26);
    MonitorText(row+1,col+5,str,White);
    int pc=cpu.pc-DisAss::CodeRewind(cpc->GetMmu(),cpu.pc,2);
    for (int i=0; i<7; i++) {
        char str2[64];
        if (i==2) pc=cpu.pc; // Code rewind sometimes fails :(
        int nextPc=pc+DisAss::Disassemble(cpc->GetMmu(), pc, str2, 64, pc==cpu.pc, cpu.bc);
        SizeStr(str2,26);
        snprintf(str, 128, "%04X %s", pc, str2);
        MonitorText(row+2+i,col,str,(pc==cpu.pc)?monitForeColor:Green);
        pc=nextPc;
    }

    // Buttons
    row=MONITOR_CPU_ROW+5;col=MONITOR_CPU_COL+70;
    buttons.Add(row,col," StepIn  ",Black,White,[this](){ cpc->AddConditionCpuStep(); return true;});
    buttons.Add(row+1,col,"  Step   ",Black,White,[this](){ cpc->AddConditionCpuBreak(lastCpu.pc+DisAss::Disassemble(cpc->GetMmu(), lastCpu.pc)); return true;});
    buttons.Add(row+2,col," StepOut ",Black,White,[this](){ cpc->AddConditionCpuStack(lastCpu.sp+2); return true;});
    buttons.Add(row+3,col,"   Run   ",Black,White,[this](){ return true;});
    buttons.Add(row+4,col," Run to  ",Black,White,[this](){ int trap;
            if (console.GetHexValue("Enter Hexadecimal Adress",trap)) {
                cpc->AddConditionCpuBreak(trap, true);
                return true;
            }
            return false;});
    buttons.Add(row+4,col-10," Clear   ",Black,White,[this](){ cpc->RemoveCpuCondition(); return false;});
    snprintf(str, 128, "Trace %s", cpuTrace ? "OFF" : "ON ");
    buttons.Add(row+3,col-10, str, Black,White,[this](){ cpuTrace ^= 1;
            cpc->CpuTrace(cpuTrace);
            MonitorText(8,66,cpuTrace?"OFF":"ON ",Black,White);
            return false;});
    buttons.Add(row+2,col-10," IntAck  ",Black,White,[this](){ cpc->AddConditionCpuIntAck(); return true;});
    lastCpu = cpu;

    // CRTC
    //------
    CrtcState &crtc(state.crtc);
    int crtcRow=11, crtcCol=0;

    row=crtcRow; col=crtcCol;
    MonitorText(row,col,"  CRTC    ",Black,BrightWhite);
    for (int i=0; i<4; i++) {
        str[0]=0;
        for (int j=0; j<4; j++) {
            MonitorText(row+i+1,col+12*j,Crtc::RegName(i*4+j),monitBackColor,monitForeColor);
            DrawRegister08XH(row+i+1,col+12*j+6,crtc.r[i*4+j],lastCrtc.r[4*i+j]);
        }
    }
    row=crtcRow+1;col=crtcCol+48;
    MonitorText(row,col,"ra",monitBackColor,monitForeColor);
    DrawRegister08X(row+1,col,crtc.addressRegister,lastCrtc.addressRegister);
    MonitorText(row+1,col+3,Crtc::RegName(crtc.addressRegister));
    MonitorText(row+2,col+3,Crtc::RegName(lastCrtc.addressRegister),White);
    lastCrtc = crtc;
    // Buttons
    row=crtcRow+1;col=crtcCol+70;
    buttons.Add(row,col," rChange ",Black,White,[this](){cpc->TrapCrtcChanges();
            return true;});
    snprintf(str, 128, "Trace %s", crtcTrace ? "OFF" : "ON ");
    buttons.Add(row,col-10,str,Black,White,[this](){crtcTrace ^= 1;
            cpc->CrtcTrace(crtcTrace);
            MonitorText(12,66,crtcTrace?"OFF":"ON ",Black,White);
            return false;});

    // GA
    //----
    GaState &ga(state.ga);
    int gaRow=17, gaCol=0;
    row=gaRow; col=gaCol;
    MonitorText(row,col,"  GA      ",Black,BrightWhite);
    // Index Border & Colors
    row=gaRow+1; col=gaCol;
    MonitorText(row,col,"Idx Bdr ",monitBackColor,monitForeColor);
    DrawRegister08X(row+1,col+1,ga.colIdx,lastGa.colIdx);
    DrawColor(row+1,col+4,ga.border,lastGa.border);
    for (int i=0; i<16; i++) {
        snprintf(str, 128, "%02d ", i);
        MonitorText(row, col+8+3*i, str,monitBackColor,monitForeColor);
        DrawColor(row+1, col+8+3*i,ga.col[i], lastGa.col[i]);
    }
    // Mode Ram & Rom
    row=gaRow+4;col=gaCol;
    MonitorText(row,col,"m ram  rom uRom hCnt",monitBackColor,monitForeColor);
    DrawRegister2(row+1,col,ga.mf&3,lastGa.mf&3);

    DrawRegister3(row+1,col+2,Ga::RamPattern(ga.ramSelection, 3),Ga::RamPattern(lastGa.ramSelection,3));
    DrawRegister3(row+1,col+3,Ga::RamPattern(ga.ramSelection,2),Ga::RamPattern(lastGa.ramSelection,2));
    DrawRegister3(row+1,col+4,Ga::RamPattern(ga.ramSelection,1),Ga::RamPattern(lastGa.ramSelection,1));
    DrawRegister3(row+1,col+5,Ga::RamPattern(ga.ramSelection,0),Ga::RamPattern(lastGa.ramSelection,0));
    snprintf(str, 128, "%c%c", ((ga.mf >> 2) & 1) ? '.' : 'L', ((ga.mf >> 2) & 2) ? '.' : 'U');
    MonitorText(row+1,col+7,str);
    snprintf(str, 128, "%c%c", ((lastGa.mf >> 2) & 1) ? '.' : 'L', ((lastGa.mf >> 2) & 2) ? '.' : 'U');
    MonitorText(row+2,col+7,str,White);
    DrawRegister3(row+1,col+11,ga.upperRom,lastGa.upperRom);
    DrawRegister8(row+1,col+16,ga.hCnt,lastGa.hCnt);
    lastGa=ga;

    // VDU
    //----
    VduState &vdu(state.vdu);
    int vduRow=25, vduCol=0;
    row=vduRow;col=vduCol;
    MonitorText(row,col,"  VDU     ",Black,BrightWhite);
    // Horizontal control
    row=vduRow+1;col=vduCol;
    MonitorText(row,col,"hCnt hSyn hSnt",monitBackColor,monitForeColor);
    DrawRegister08X(row+1,col+1,vdu.hCount,lastVdu.hCount);
    DrawRegister1(row+1,col+7,vdu.crtcHSync,lastVdu.crtcHSync);
    DrawRegister08X(row+1,col+12,vdu.hSyncCnt,lastVdu.hSyncCnt);
    // Vertical control
    row=vduRow+1;col=vduCol+15;
    MonitorText(row,col,"sCnt Frez vAdj vCnt vSyn vSnt",monitBackColor,monitForeColor);
    DrawRegister08X(row+1,col+1,vdu.sCount,lastVdu.sCount);
    DrawRegister1(row+1,col+6,vdu.vAdj,lastVdu.vAdj);
    DrawRegister08X(row+1,col+11,vdu.vAdj,lastVdu.vAdj);
    DrawRegister08X(row+1,col+16,vdu.vCount,lastVdu.vCount);
    DrawRegister1(row+1,col+21,vdu.crtcVSync,lastVdu.crtcVSync);
    DrawRegister08X(row+1,col+26,vdu.vSyncCnt,lastVdu.vSyncCnt);
    // Memory
    row=vduRow+4;col=vduCol;
    MonitorText(row,col,"memAdd vRam",monitBackColor,monitForeColor);
    DrawRegister16X(row+1,col+1,vdu.memAdd,lastVdu.memAdd);
    WORD vRam =(((vdu.memAdd&0x3000)<<2) |
                ((vdu.sCount&0x7)<<11)|((vdu.memAdd&0x3FF)<<1));
    WORD lastVRam =(((lastVdu.memAdd&0x3000)<<2) |
                    ((lastVdu.sCount&0x7)<<11)|((lastVdu.memAdd&0x3FF)<<1));
    DrawRegister16X(row+1,col+7,vRam,lastVRam);
    // Display
    row=vduRow+4; col=vduCol+20;
    MonitorText(row,col,"disp vduRow     vduCol",monitBackColor,monitForeColor);
    DrawRegister2(row+1,col+1,vdu.dispEn,lastVdu.dispEn);
    DrawRegister16X(row+1,col+5,vdu.vduRow,lastVdu.vduRow);
    DrawRegister16(row+1,col+10,vdu.vduRow,lastVdu.vduRow);
    DrawRegister16X(row+1,col+16,vdu.vduCol,lastVdu.vduCol);
    DrawRegister16(row+1,col+21,vdu.vduCol,lastVdu.vduCol);
    // Buttons
    row=vduRow+1;col=vduCol+60;
    buttons.Add(row,col,"  vduPos ",Black,White, [this](){cpc->SetTrap(MonitoredCpc::TrapId::VDUPOS); MonitorMsg("Click in main Window for VduTrap location"); return false;});
    buttons.Add(row,col+10,"  hReset ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::HRESET); return true;});
    buttons.Add(row+1,col,"  sReset ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::SRESET); return true;});
    buttons.Add(row+1,col+10,"  vReset ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::VRESET); return true;});
    buttons.Add(row+2,col,"  hSync  ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::HSYNC); return true;});
    buttons.Add(row+2,col+10,"  vSync  ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::VSYNC); return true;});
    buttons.Add(row+3,col,"   Tick  ",Black,White,[this](){cpc->SetTrap(MonitoredCpc::TrapId::TICK); return true;});

    lastVdu=vdu;

    // Mem
    //----
    BYTE mem[256];
    int memRow=32,memCol=0;
    row=memRow-1;col=memCol;
    for (int i=0; i<256; i++) {
        mem[i]=cpc->MemRd8(memAddr+i);
        if ((i%16)==0) {
            row++;
            snprintf(str, 128, "%04X", memAddr + i), MonitorText(row, col, str);
        }
        snprintf(str, 128, "%02X", mem[i]);
        MonitorText(row,col+5+(i%16)*3,str,mem[i]!=lastMem[i]? BrightRed:monitForeColor);
        snprintf(str, 128, "%c", isprint(mem[i]) ? mem[i] : '.');
        MonitorText(row,col+53+(i%16)*1,str,mem[i]!=lastMem[i]? BrightRed:monitForeColor);
        lastMem[i]=mem[i];
    }
    // Buttons
    row=memRow+1;col=memCol+70;
    buttons.Add(row,col," setAddr ",Black,White, [this](){int trap; if (console.GetHexValue("Enter Hexadecimal Adress",trap)) memAddr = trap;return false;});
    buttons.Add(row+1,col,"  Watch  ",Black,White,[this](){int trap; if (console.GetHexValue("Enter Hexadecimal Adress",trap)) {
                cpc->SetWatch(trap);
                return true;
            }
            return false;});
    buttons.Add(row+2,col,"  Dump   ",Black,White,[this](){
            int start,size;
            if (console.GetHexValue("Enter Hexadecimal Adress",start)) {
                if (console.GetHexValue("Enter Hexadecimal Size",size)) {
                    FILE *fd = fopen("ramDump","wb");
                    for (int i=0; i<size; i++) {
                        BYTE val = cpc->MemRd8(start+i);
                        fwrite(&val,1,1,fd);
                    }
                    fclose(fd);
                }
            }
            return false;});
}

void Monitor::SizeStr(char *str, int n)
{
    size_t i;
    for (i=strlen(str); i<n; i++)
        str[i]=' ';
    str[i]=0;
}

void Monitor::DrawUINT4(int row, int col, int val)
{
    char str[32];
    snprintf(str,32,"%01X",(UINT8)val&0xF);
    MonitorText(row,col,str);
}

void Monitor::DrawUINT8(int row, int col, int val)
{
    char str[32];
    snprintf(str,32,"%02X",(UINT8)val&0xFF);
    MonitorText(row,col,str);
}

void Monitor::DrawUINT16(int row, int col, int val)
{
    char str[32];
    snprintf(str,32,"%04X",(UINT16)val&0xFFFF);
    MonitorText(row,col,str);
}

void Monitor::MonitorMsg(const char *msg)
{
    char str[256];
    snprintf(str, 256, "%s", msg);
    SizeStr(str,MONITOR_COLS);
    MonitorText(MONITOR_ROWS-1,0,str,BrightWhite);
}

static bool IsBlack(int c)
{
    return ((c)==1||(c)==8||(c)==9||(c)==16||(c)==17||(c)==20);
}

// Draw a string at a given position with given colors
void Monitor::MonitorText(int row, int col, const char *str, int fore, int back)
{
    if (back<0)
        back = monitBackColor;
    if (fore<0)
        fore = monitForeColor;
    if (back==fore || (IsBlack(back)&&IsBlack(fore)))
        back=monitAltBackColor;

    int left = MONITOR_FONT_SIZE * col;
    int top = MONITOR_FONT_SIZE * row;
    AmstradText::RectFill(Dib(), CpcRect(top, left, top+MONITOR_FONT_SIZE, static_cast<int>(left+MONITOR_FONT_SIZE*strlen(str))), console.GetP32()[back]);
    console.DrawAmstradText(Dib(), col*MONITOR_FONT_SIZE, row*MONITOR_FONT_SIZE, console.GetP32()[fore], str);
}

void Monitor::MonitorClear()
{
    AmstradText::RectFill(Dib(), CpcRect(0,0,monitWinH-1,monitWinW-1), console.GetP32()[monitBackColor]);
    buttons.Reset();
}

void Monitor::SetCpc(MonitoredCpc &cpc_)
{
    cpc=&cpc_;
}

CpcRect Monitor::WinRect()const
{
    return CpcRect(monitWinR, monitWinC, monitWinR+monitWinH-1, monitWinC+monitWinW-1);
}

CpcDib32 Monitor::Dib()
{
    return CpcDib32(canvas.data(), monitWinH, monitWinW * sizeof(uint32_t));
}

Monitor::Monitor(MonitorConsole &console)
: buttons(*this)
, cpc(0), monitWinC(50), monitWinR(50)
, monitWinW(MONITOR_COLS*MONITOR_FONT_SIZE), monitWinH(MONITOR_ROWS*MONITOR_FONT_SIZE)
, canvas(monitWinW*monitWinH)
, monitForeColor(SeaGreen), monitBackColor(Black), monitAltBackColor(White)
, monitState(0)
, lastVdu(), lastCpu(), lastCrtc(), lastGa()
, memAddr(0)
, console(console)
{
    // Clear the previous states
    memset(&lastCpu,0,sizeof(lastCpu));
    memset(&lastCrtc,0,sizeof(lastCrtc));
    memset(&lastGa,0,sizeof(lastGa));
    memset(&lastVdu,0,sizeof(lastVdu));
}

