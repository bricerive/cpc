#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// mackeystr.h

// Names for all the Mac virtual keys
static const char *macKeyStr[128]={
    "A","S","D","F","H","G","Z","X", // 00
    "C","V","NoName0A","B","Q","W","E","R", // 08
    "Y","T","1","2","3","4","6","5", // 10
    "=","9","7","-","8","0","]","O", // 18
    "U","[","I","P","Enter","L","J","'", // 20
    "K",";","\\",",","/","N","M",".", // 28
    "Tab","Space","`","Del","VTab","Esc","Power","Command", // 30
    "Shift","Caps","Option","Control","Shift2","Option2","Control2","NoName3F", // 38
    "NoName40","Kpd.","NoName42","Kpd*","NoName44","Kpd+","NoName46","KpdClear", // 40
    "NoName48","NoName49","NoName4A","Kpd/","KpdEnter","NoName4D","Kpd-","NoName4F", // 48
    "NoName50","Kpd=","Kpd0","Kpd1","Kpd2","Kpd3","Kpd4","Kpd5", // 50
    "Kpd6","Kpd7","NoName5A","Kpd8","Kpd9","NoName5D","NoName5E","NoName5F", // 58
    "F5","F6","F7","F3","F8","F9","NoName66","F11", // 60
    "NoName68","F13","NoName6A","F14","NoName6C","F10","NoName6E","F12", // 68
    "NoName70","F15","Help","Home","PgUp","Del","F4","End", // 70
    "F2","PgDn","F1","Left","Right","Down","Up","NoName7F" // 78
};