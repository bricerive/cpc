//
//  sna.cpp
//  CPC++
//
//  Created by Brice Rivé on 10/28/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#include "sna.h"
#include "cpc_state.h"
#include "infra/error.h"
#include "infra/struct_with_features.h"
#include "infra/log.h"
#include <cstring>

using namespace std;
using boost::format;
typedef CpcBase::CpcModelType CpcModelType;

static inline BYTE L(UINT16 v) { return v&0xFF; }
static inline BYTE H(UINT16 v) { return v>>8; }
static inline UINT16 W(BYTE L, BYTE H) { return L + (H<<8); }

void Sna::StateToSna(const CpcState &state, vector<BYTE> &sna)
{
    sna.resize(256 + 1024 * 128);

    // Fill the buffer with snapshot info
    strcpy(reinterpret_cast<char *>(&sna[0]), "MV - SNA");
    sna[0x10] = 3;

    {
        const CpuState &st(state.cpu);
        sna[0x11] = L(st.af);
        sna[0x12] = H(st.af);
        sna[0x13] = L(st.bc);
        sna[0x14] = H(st.bc);
        sna[0x15] = L(st.de);
        sna[0x16] = H(st.de);
        sna[0x17] = L(st.hl);
        sna[0x18] = H(st.hl);
        sna[0x19] = st.r;
        sna[0x1a] = st.i;
        sna[0x1b] = static_cast<BYTE>(st.iff1);
        sna[0x1c] = static_cast<BYTE>(st.iff2);
        sna[0x1d] = L(st.ix);
        sna[0x1e] = H(st.ix);
        sna[0x1f] = L(st.iy);
        sna[0x20] = H(st.iy);
        sna[0x21] = L(st.sp);
        sna[0x22] = H(st.sp);
        sna[0x23] = L(st.pc);
        sna[0x24] = H(st.pc);
        switch (st.im) {
            case 0: sna[0x25]=0;break;
            case 1: sna[0x25]=3;break;
            case 2: sna[0x25]=1;break;
            case 3: sna[0x25]=2;break;
            default:
                ERR_THROW(format("Invalid interrupt mode %d")%st.im);
        }
        sna[0x26] = L(st.af2);
        sna[0x27] = H(st.af2);
        sna[0x28] = L(st.bc2);
        sna[0x29] = H(st.bc2);
        sna[0x2a] = L(st.de2);
        sna[0x2b] = H(st.de2);
        sna[0x2c] = L(st.hl2);
        sna[0x2d] = H(st.hl2);
    }

    {
        const GaState &st(state.ga);
        sna[0x2e] = st.colIdx;
        for (int i=0; i<16; i++) sna[0x2f+i] = st.col[i];
        sna[0x3f] = st.border;
        sna[0x40] = 0x80 | st.mf;
        sna[0x41] = static_cast<BYTE>(st.ramSelection);
        sna[0x55] = static_cast<BYTE>(st.upperRom);
        for (size_t i=0; i<min<size_t>(st.ram.size(), 8); i++)
            memcpy(reinterpret_cast<char *>(&sna[0])+256+16384*i, st.ram[i].Pt(), 16384);
    }
    {
        const CrtcState &st(state.crtc);
        sna[0x42] = st.addressRegister;
        for (int i=0; i<16; i++)
            sna[0x43+i] = st.r[i];
    }

    {
        const PioState &st(state.pio);
        sna[0x56] = st.portA;
        sna[0x57] = st.portB;
        sna[0x58] = st.portC;
        sna[0x59] = st.regCtrl;
    }

    {
        const PsgState st(state.psg);
        sna[0x5a] = st.crtAdd;
        for (int i=0; i<16; i++)
            sna[0x5b+i] = st.reg[i];
    }

    {
        const ConfigState &st(state.config);
        // 0 = CPC464,CPC664,CPC6128,unknown,6128 Plus,464 Plus,GX4000
        STRUCT_WITH_CTOR(SnaType, (UINT16, ramSize)(UINT8, model));
        static const map<CpcModelType, SnaType> snaTypes = {
            {CpcModelType::MODEL_464,{64,0}}
            ,{CpcModelType::MODEL_664,{64,1}}
            ,{CpcModelType::MODEL_6128,{128,2}}
            ,{CpcModelType::MODEL_6128_512K,{512,2}}
            ,{CpcModelType::MODEL_6128_4M,{4096,2}}
       };

        const SnaType &snaType(snaTypes.at(st.model));
        sna[0x6b] = snaType.ramSize & 0xFF;
        sna[0x6c] = snaType.ramSize >> 8;
        sna[0x6d] = snaType.model;
        for (int i=0x6e; i<0x100; i++)
            sna[i] = 0;
    }
}

/// Create a state from a snapshot.
/// The snapshot format does not contain all we need so we try
/// to fill up the blanks here.
/// See current spec:
/// https://www.cpcwiki.eu/index.php/Format:SNA_snapshot_file_format
/// http://www.cpcwiki.eu/index.php/Snapshot
void Sna::SnaToState(const vector<BYTE> &sna, CpcState &state)
{
    ASSERT_THROW(strcmp("MV - SNA", reinterpret_cast<const char *>(&sna[0]))==0)("Not a snapshot file.");
    int snaVersion = sna[0x10];
    ASSERT_THROW(snaVersion== 1 || snaVersion==2|| snaVersion==3)(format("Unknown SNA version [%d].")%snaVersion);

    int ramKb = sna[0x6b] + 256 * sna[0x6c];
    const BYTE *pt = sna.data() + ramKb*1024 + 256;
    const BYTE *snaEnd = sna.data() + sna.size();
    ASSERT_THROW(pt<=snaEnd)(format("SNA too small for expected RAM dump [%d].")% sna.size());
    while (pt<snaEnd && snaVersion>=3) {
        // Immediately following the memory dump there is optional data which is separated into chunks.
        // Each chunk of data has a header and this is followed by the data in the chunk. The header has the following format:
        // Offset (Hex)	Count	Description
        // 0	4	Chunk name (note 1)
        // 4	4	Chunk data length (note 2)
        // Notes:
        // 1. The chunks are defined with 4-byte character codes. (e.g. "CPC+"). In this example, the 4-byte character code would be stored in the file as 'C' then 'P' then 'C' then '+'.
        // 2. The "Chunk data length" defines the length of data following the header and does not include the size of the header. This number is stored in little endian format.
        // 3. If a emulator finds a chunk which it does not support then it should skip the chunk and continue with the next chunk in the file. Therefore an emulator author may add emulator specific chunks to the file and it will not prevent the snapshot from being used with other emulators that do not recognise the added chunks.
        // 4. There is not a terminator chunk. The snapshot reader should determine if there are more chunks based on the size of data remaining to be read from the file.
        string chunkName(pt, pt+4);
        int chunkSize = pt[4]+256*(pt[5]+256*(pt[6]+256*pt[7]));
        LOG_STATUS(boost::format("Found V3 chunk in SNA: %s %d")%chunkName %chunkSize);
        pt += chunkSize + 8;
    }

    {
        ConfigState &st(state.config);
        st.model = ramKb==128? CpcModelType::MODEL_6128: CpcModelType::MODEL_664;
        if (snaVersion>=2 || sna[0x6d]!=0) {
            int cpcType = sna[0x6d];
            // 0 = CPC464,CPC664,CPC6128,unknown,6128 Plus,464 Plus,GX4000
            // this is incorrect - 3 should be "UNKNOWN" - only here to load my wrong snapshots
            static const vector<CpcModelType> models = {CpcModelType::MODEL_464, CpcModelType::MODEL_664, CpcModelType::MODEL_6128, CpcModelType::MODEL_6128, CpcModelType::MODEL_6128};
            ASSERT_THROW(cpcType<models.size());
            st.model = models[cpcType];
        }
    }
    int ramBlocks = (state.config.model==CpcModelType::CpcModelType::MODEL_6128? 8: 4);
    int ramBlocksAvailable = ramKb/16;

    {
        CpuState &st(state.cpu);
        // Load the Z80 registers from the snapshot values
        st.af = W(sna[0x11], sna[0x12]);
        st.bc = W(sna[0x13], sna[0x14]);
        st.de = W(sna[0x15], sna[0x16]);
        st.hl = W(sna[0x17], sna[0x18]);
        st.r = sna[0x19];
        st.i = sna[0x1a];
        st.iff1 = sna[0x1b];
        st.iff2 = sna[0x1c];
        st.ix = W(sna[0x1d], sna[0x1e]);
        st.iy = W(sna[0x1f], sna[0x20]);
        st.sp = W(sna[0x21], sna[0x22]);
        st.pc = W(sna[0x23], sna[0x24]);
        switch (sna[0x25]) {
            case 0: st.im=0;break;
            case 1: st.im=2;break;
            case 2: st.im=3;break;
            default:
                ERR_THROW(format("Invalid interruption mode %d") %sna[0X25]);
        }
        st.af2 = W(sna[0x26], sna[0x27]);
        st.bc2 = W(sna[0x28], sna[0x29]);
        st.de2 = W(sna[0x2a], sna[0x2b]);
        st.hl2 = W(sna[0x2c], sna[0x2d]);

        st.it=0;
        st.nmi=0;
    }
    {
        GaState &st(state.ga);
        st.colIdx = sna[0x2e];
        for (int i=0; i<16; i++) st.col[i]=sna[0x2f+i];
        st.border = sna[0x3f];
        st.mf = sna[0x40];
        st.ramSelection = sna[0x41]; // RAM config
        st.upperRom = sna[0x55];
        st.ram.resize(ramBlocks);
        for (int i=0; i<ramBlocks; i++) {
            if (i<ramBlocksAvailable)
                memcpy(st.ram[i].Pt(), reinterpret_cast<const char *>(&sna[0])+256+16384*i, 16384);
        }
        st.vSync = st.vSyncArmed = false;
        st.vSyncCnt=0;
    }
    {
        CrtcState &st(state.crtc);
        st.addressRegister = sna[0x42];
        for (int i=0; i<16; i++)
            st.r[i] = sna[0x43+i];
        st.model = snaVersion>3? sna[0xA4]: 0;
    }
    {
        PioState &st(state.pio);
        st.regCtrl = sna[0x59]; // CTL
        st.portA = sna[0x56]; // Reg A
        st.portB = sna[0x57]; // Reg B
        st.portC = sna[0x58]; // Reg C
        // Printer Data/Strobe Register
        // This byte in the snapshot represents the last byte written to the printer I/O port
        // (this byte does not include the automatic inversion of the strobe caused by the Amstrad hardware).
        st.printerData = snaVersion>3? sna[0xA1]: 0xFF;
    }
    {
        PsgState &st(state.psg);
        for (int i=0; i<16; i++) st.reg[i] = sna[0x5b+i];
        st.crtAdd = sna[0x5a];
    }
    {
        FdcState &st(state.fdc);
        // FDD motor drive state (0=off, 1=on)
        st.motorOn = snaVersion>3? sna[0x9C]: false;
        // FDD current physical track - This is the current cylinder for each of 4 drives.
        st.currentTrack[0] = snaVersion>3? sna[0x9D]: 0;
        st.currentTrack[1] = snaVersion>3? sna[0x9E]: 0;
        st.currentTrack[2] = snaVersion>3? sna[0x9F]: 0;
        st.currentTrack[3] = snaVersion>3? sna[0xA0]: 0;
    }
    {
        VduState &st(state.vdu);
        memset((void *)&st, 0, sizeof(VduState));

        if (snaVersion>=3) {
            // CRTC horizontal character counter register -
            // This register counts the number of characters. This counter counts up. This value is in the range 0-255. (This counter is compared against CRTC register 0).
            st.hCount = sna[0xA9];
            // Current frame scan line since monitor retrace
            // In WinAPE this counter is reset if a VSYNC occurs which causes the monitor to retrace vertically. This occurs if the VSYNC signal is active and this counter is greater than a threshold set by the V-Hold (normally 295 when V-Hold is 0), or if the counter reaches the maximum value (normally 351 when V-Hold is 0).
            st.vduRow = (sna[0xA2]+256*sna[0xA3]);
            // CRTC character-line counter register
            // This register counts the number of character-lines. The counter counts up. This value is in the range 0-127. (This counter is compared against CRTC register 4).
            st.vCount = sna[0xAB];
            // CRTC raster-line counter register (note 3)
            // This register counts the number of raster-lines. The counter counts up. This value is in the range 0-31. (This counter is compared against CRTC register 9).
            st.sCount = sna[0xAC];
            // CRTC vertical total adjust counter register (note 4)
            // This register counts the number of raster-lines during vertical adjust. The counter counts up. This value is in the range 0-31. This should be ignored if the CRTC is not "executing" vertical. adjust.(This counter is compared against CRTC register 5).
            st.vAdjCnt = sna[0xAD];
            // CRTC horizontal sync width counter
            // This register counts the number of characters during horizontal sync. This counter counts up. This value is in the range 0-16. This should be ignored if the CRTC is not "executing" horizontal sync. (This counter is compared against CRTC register 3).
            st.hSyncCnt = sna[0xAE];
            // CRTC vertical sync width counter
            // This register counts the number of scan-lines during vertical sync. This counter counts up. This value is in the range 0-16. This should be ignored if the CRTC is not "executing" vertical sync. (This counter is compared against CRTC register 3).
            st.vSyncCnt = sna[0xAF];
            if (sna[0xB0] & 0x01) st.crtcVSync=1;
            if (sna[0xB0] & 0x02) st.crtcHSync=1;
            if (sna[0xB0] & 0x80) st.vAdj=1;

            // GA vsync delay counter
            // This is a counter internal to the GA and counts the number of HSYNCs since the start of the VSYNC and it is used to reset the interrupt counter to synchronise interrupts with the VSYNC. This counter counts up. This value is between 0 and 2. If this value is 0, the counter is inactive. If this counter is 1 or 2 the counter is active.
            st.gaHSync = sna[0xB2];

            // GA interrupt scanline counter (note 12)
            // This counter is internal to the GA and counts the number of HSYNCs. This counter is used to generate CPC raster interrupts. This counter counts up. This value is in the range 0-51.
            // st. = sna[0xB3];

            // interrupt request flag (0=no interrupt requested, 1=interrupt requested) (note 13)
            // This flag is "1" if a interrupt request has been sent to the Z80 and it has not yet been acknowledged by the Z80. (A interrupt request is sent by the GA for standard CPC raster interrupts or by the ASIC for raster or dma interrupts).
            // st. = sna[0xB4];
        }
    }
}






