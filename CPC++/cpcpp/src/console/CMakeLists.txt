# console
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

# Get the source list
file(GLOB Source "*.txt" "*.h" "*.cpp")

add_library( ${Target} STATIC ${Source} )
target_link_libraries( ${Target} core)
