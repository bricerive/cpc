#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// amstrad_text.h
#include "cpctypes.h"

/// allow drawing text with the original CPC font
class AmstradText {
public:
    /// fontDib contains the full CPC font int 8 rows by 768 columns (96 characters)
    AmstradText(CpcDib32 fontDib);
    /// fill a rectangle. not strictly font but usefull
    static void RectFill(CpcDib32 dst, const CpcRect &rect, uint32_t color);
    /// Draw a string in CPC font into a 32 bit pixmap
    void DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char *str) const;
private:
    CpcDib32 fontDib;
};
