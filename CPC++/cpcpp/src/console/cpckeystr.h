#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// cpckeystr.h

/// Names for all the CPC keys
// CPC Key codes
// { = Numeric Keypad (function keys)
// ( = Joystick 0
// [ = Joystick 1
//    | 0      1      2      3      4      5      6      7
//-----------------------------------------------------------
//  0 | UP     RIGHT  DOWN   {9     {6     {3     {ENTER {.
//  8 | LEFT   COPY   {7     {8     {5     {1     {2     {0
// 16 | CLR    [      ENTER  ]      {4     SHIFT  \      CTRL
// 24 | |      -      @      P      ;      :      /      .
// 32 | 0      9      O      I      L      K      M      ,
// 40 | 8      7      U      Y      H      J      N      SPACE
// 48 | 6      5      R      T      G      F      B      V
//    |[UP    [DOWN  [LEFT  [RIGHT [FIRE2 [FIRE1 [SPARE
// 56 | 4      3      E      W      S      D      C      X
// 64 | 1      2      ESC    Q      TAB    A      CAPS   Z
// 72 | (UP    (DOWN  (LEFT  (RIGHT (FIRE2 (FIRE1 (SPARE DEL
static const char *cpcKeyStr[80]={
"Up","Right","Down","Kpd 9","Kpd 6","Kpd 3","Enter","Kpd .",
"Left","Copy","Kpd 7","Kpd 8","Kpd 5","Kpd 1","Kpd 2","Kpd 0",
"Clr","[","Return","]","Kpd 4","Shift","\\","Crtl",
"^","-","@","P",";",":","/",".",
"0","9","O","I","L","K","M",",",
"8","7","U","Y","H","J","N","Space",
"6","5","R","T","G","F","B","V",
"4","3","E","W","S","D","C","X",
"1","2","Esc","Q","Tab","A","Caps","Z",
"Joy Up","Joy Down","Joy Left","Joy Right","Joy Fire2","Joy Fire1","Joy Spare","Del"
};
