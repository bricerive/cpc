// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// CpcGuiConsole.cpp - control part of a GUI for CPC++
#include "cpcguiconsole.h"

#include "console_shared.h"
#include "cpc.h"
#include "cpc_state.h"
#include "cpcconfig.h"
#include "cpcfile.h"
#include "ga.h"
#include "sna.h"

#include "infra/error.h"
#include "infra/log.h"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>

#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <map>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <vector>

using namespace std;
typedef CpcBase::CpcModelType CpcModelType;
typedef CpcBase::PrinterMode PrinterMode;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

static const char *kRomExt=".ROM";

static const char *kDskExt=".DSK";
static const char *kDskMagic="MV - CPC";
static const char *kDskMagic2="EXTENDED";

static const char *kSnaExt=".SNA";
static const char *kSnaMagic="MV - SNA";

static const char *kZipExt=".ZIP";

static const char *kCpcppExt=".CPCPP";
static const char *kCpcppMagic="---CPC++";

static bool IsFileExtension(const std::string &path, const char *ext)
{
    boost::filesystem::path p(path);
    string extension = p.extension().string();
    std::string tmp = extension;
    std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
    return tmp==ext;
}

static int AddressFromString(const std::string &str)
{
    size_t processed_chars;
    int value;

    // Check if the string is hexadecimal
    if (str.find("0x") == 0 || str.find("0X") == 0) {
        // Parse as hexadecimal
        value = std::stoi(str, &processed_chars, 16);
    } else {
        // Parse as decimal
        value = std::stoi(str, &processed_chars, 10);
    }

    // Check if the entire string was processed
    if (processed_chars != str.size()) {
        throw std::invalid_argument("Invalid input string");
    }

    return value;
}

// Glue code
static void SetOption(const po::variables_map &vm, const char *name, OptionalStr &arg)
{
    if (vm.count(name)) arg = vm[name].as<string>();
}

CpcGuiConsole::CpcGuiConsole(const ArgsVec &argsVec)
: cpc(*new Cpc(*this))
, timer([this](){return GetCrtUs();})
{
    // parse our CLI options
    po::options_description desc("Allowed options");
    desc.add_options()("help,h", "produce help message")("load,l",
        po::value<vector<string>>()->multitoken()->composing(),
        "Load a binary file at a specified address. Use format: -l <address> <binFilePath>")(
        "jump,j", po::value<string>(), "Jump to the specified address.")("command,c", po::value<string>(),
        "GhostType the given command.")("disk_a,a", po::value<string>(), "Insert the disk in drive A.")(
        "disk_b,b", po::value<string>(), "Insert the disk in drive B.")("break,k",
        po::value<vector<string>>()->multitoken()->composing(),
        "Add breakpoints.")("snapshot,s", po::value<string>(), "Load a snapshot.");
    po::variables_map vm;
    // Parse command line options, allowing for unrecognized options to skip OSX's -psn_0_xxxxxxx
    po::parsed_options parsed = po::command_line_parser(argsVec).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return;
    }

    if (vm.count("load")) {
        vector<string> loads = vm["load"].as<vector<string>>();
        int address = -1;
        // Each element in 'loads' is a pair of address and file path
        for (auto &load : loads) {
            if (address < 0) {
                address = AddressFromString(load);
            } else {
                args.binariesToLoad.emplace_back(load, address);
                address = -1;
            }
        }
    }
    if (vm.count("jump")) args.jumpAddr = AddressFromString(vm["jump"].as<string>());
    SetOption(vm, "disk_a", args.diskA);
    SetOption(vm, "disk_b", args.diskB);
    SetOption(vm, "snapshot", args.snapshot);
    SetOption(vm, "command", args.startupCommand);
    if (vm.count("jump")) args.jumpAddr = AddressFromString(vm["jump"].as<string>());
    args.exePath = fs::system_complete(argsVec[0]).parent_path().string();
    args.exeName = fs::path(argsVec[0]).stem().string();
    if (vm.count("break")) {
        vector<string> breakpoints = vm["break"].as<vector<string>>();
        for (auto &s : breakpoints) args.breakPoints.push_back(AddressFromString(s));
    }
}

 CpcGuiConsole::~CpcGuiConsole()
{
    delete &cpc;
}

void CpcGuiConsole::RunStep()
{
    if (IsPaused()) return;

    bool render = frameCnt==0;
    CpcBase::RunState runState = cpc.RunSome(render);

    if (runState.frameDone) {
        if (render) FrameReady(); // Refresh full screen
        int cnt = 1<<frameRate.value;
        if (++frameCnt>=cnt) frameCnt=0; // Frame skipping
    }

    if (runState.cpcPhase >= evtPhase) {
        // Events in CPC time
        evtPhase += CpcDuration::FromMs(evtCheckMs);
        CheckEvents();
    }

    if (runState.cpcPhase>=speedPhase)
        speedPhase += SpeedControl();

    if (runState.cpcPhase>=ghostPhase) {
        if (!delayedStartupEventsDone) DelayedStartupEvents();
        ghostPhase = runState.cpcPhase + GhostTyping();
    }
}

void CpcGuiConsole::DelayedStartupEvents()
{
    delayedStartupEventsDone=true;
    for (auto &breakPoint : args.breakPoints) 
        cpc.AddConditionCpuBreak(breakPoint, true);
    if (args.binariesToLoad.empty() || !args.jumpAddr) return;
    CpcState state;
    cpc.GetState(state);
    for (auto &load : args.binariesToLoad)
        LoadBinary(load, state);
    if (args.jumpAddr) {
        state.cpu.pc = *args.jumpAddr;
        state.ga.mf |= 0x0C;
    }
    cpc.SetState(state);
}

// Handle Speed control
// MaximumSpeed is in Mhz, normal CPC is 4
CpcDuration CpcGuiConsole::SpeedControl()
{
    unsigned long expectedUs = speedCheckMs*4000/maxSpeed; // maxspeed of 4 is 100%
    unsigned long executedUs = timer.LapseUs();
    unsigned long elapsedUs = executedUs;
    if (limitSpeed && elapsedUs < expectedUs) {
        SleepUs(expectedUs-elapsedUs);
        elapsedUs = expectedUs;
        timer.LapseUs();
    }
    SpeedReport(speedCheckMs*1000,executedUs,elapsedUs);
    return CpcDuration::FromMs(speedCheckMs);
}

void CpcGuiConsole::ConnectCpc()
{
    // Set attributes
    SetScreenSmall(cpcConfig.screenSmall);
    SetScreenFull(cpcConfig.screenFull);
    SetScreenSkip(cpcConfig.screenSkip);
    SetIndicators(guiPrefs.indicators);
    SetModel(cpcConfig.cpcModel,0);
    SetRoms();
    SetConstructor(CpcBase::CpcConstructor(cpcConfig.constructor));
    SetLastSna("");
    SetPause(0);
    SetSoundMenu();
    SetSound(cpcConfig.makeSounds);
    SetVolume(cpcConfig.soundLevel);
    SetStereo(cpcConfig.stereo);
    SetChannels(cpcConfig.soundChannels);
    
    SetMonochrome(cpcConfig.monochrome);
    SetFrameRate(cpcConfig.frameRate);
    SetSpeedLimit(cpcConfig.maximumSpeed);
    SetLimitSpeed(cpcConfig.limitSpeed);
    SetRealTimeFdc(cpcConfig.fdcRealTime);
    SetCpm(cpcConfig.cpmBoot);
    SetPrinter(cpcConfig.printerOutput);
    SetKbdRawMode(cpcConfig.kbdRawMode);
}

// Glue code
template<typename F>
static void ApplyOptionalConfig(const string &config, OptionalStr &param, F f)
{
    string s = config;
    if (param) s = *param;
    if (!s.empty()) f(s);
}

void CpcGuiConsole::DoStartupEvents()
{
    SetStereo(cpcConfig.stereo);
    SetVolume(cpcConfig.soundLevel);
    SetChannels(cpcConfig.soundChannels);
    SetSound(cpcConfig.makeSounds);
    SetDrives();
    
    // Insert disk A
    ApplyOptionalConfig(cpcConfig.driveADiskPath, args.diskA, [&](const string &s) {LoadDisk(s,CpcBase::DriveId::A);});
    // Insert disk B
    ApplyOptionalConfig(cpcConfig.driveBDiskPath, args.diskB, [&](const string &s) {LoadDisk(s,CpcBase::DriveId::B);});
    // Insert startup command
    ApplyOptionalConfig(cpcConfig.startupComm, args.startupCommand, [&](const string &s) {ghostStr+=s; ghostStr+="\r";});
    // Load startup snapshot
    ApplyOptionalConfig(cpcConfig.snapshotPath, args.snapshot, [&](const string &s) {LoadSnapshot(s);});
}

// Handle ghost typing
CpcDuration CpcGuiConsole::GhostTyping()
{
    InsertKeys();
    static const int ghostCheckMs = 10;
    return CpcDuration::FromMs(ghostCheckMs);
}

void CpcGuiConsole::InsertKeys()
{
    if (pendingDelay) {
        if (pendingDelay < 0) {
            ++pendingDelay;
        } else if (--pendingDelay == 0) {
            AsciiUp(pendingStr[0]);
            pendingStr.erase(pendingStr.begin());
            pendingDelay = -1;
        }
    } else if (!pendingStr.empty()) {
        AsciiDown(pendingStr[0]);
        pendingDelay = 1;
    } else if (!ghostStr.empty()) {
        pendingStr += ghostStr[0];
        ghostStr.erase(ghostStr.begin());
    }
}

void CpcGuiConsole::AsciiDownUp(int code)
{
    if (cpcConfig.kbdRawMode || !ghostStr.empty()) return;
    pendingStr += code;
}

void CpcGuiConsole::AsciiDown(int code)
{
    int key;
    for (int i=0; (key=asciiConv[code][i])>=0; i++)
        cpc.KeyChange(key, CpcBase::KeyDirection::DOWN);
}

void CpcGuiConsole::AsciiUp(int code)
{
    int key;
    for (int i=0; (key=asciiConv[code][i])>=0; i++)
        cpc.KeyChange(key, CpcBase::KeyDirection::UP);
}

void CpcGuiConsole::CpcKeyDown(int code)
{
    if (!cpcConfig.kbdRawMode || !ghostStr.empty()) return;
    cpc.KeyChange(code, CpcBase::KeyDirection::DOWN);
}

void CpcGuiConsole::CpcKeyUp(int code)
{
    if (!cpcConfig.kbdRawMode) return;
    cpc.KeyChange(code, CpcBase::KeyDirection::UP);
}

void CpcGuiConsole::WindowClick(int x, int y)const
{
    cpc.LastClick(x/16,y/2);
}

void CpcGuiConsole::SoundOnOff(bool on)
{
    if (CanSound()) {
        if (on && !makeSounds) StartSound();
        if (!on && makeSounds) StopSound();
    }
    cpc.SoundOnOff(on);
    makeSounds=on;
}

void CpcGuiConsole::Suspend()
{
    SoundOnOff(false);
}

void CpcGuiConsole::Resume()
{
    SoundOnOff(cpcConfig.makeSounds);
}

void CpcGuiConsole::SetCheckMark(const CpcCmd *cmds, int idx)
{
    for (int i=0; cmds[i]; i++)
        CmdChecked(cmds[i], idx==i);
}

void CpcGuiConsole::DoCommand(CpcCmd cmd)
{
    std::map<CpcCmd, function<void()>> cmdMap = {
        {CpcCmd::kCmdAbout, [this](){DoAboutDialog();} }
        , {CpcCmd::kCmdWebUrl, [this](){LaunchUrl("http://bricerive.free.fr/cpc/cpcpp.html");} }
        , {CpcCmd::kCmdMailUrl, [this](){LaunchUrl("mailto:bricerive@free.fr");} }
        , {CpcCmd::kCmdOpenAny, [this](){SelectAny();} }
        , {CpcCmd::kCmdSave, [this](){SaveCpcpp();} }
        , {CpcCmd::kCmdSaveAs, [this](){SaveAsCpcpp();} }
        , {CpcCmd::kCmdSaveSnapshot, [this](){PutSnapshot(true);} }
        , {CpcCmd::kCmdQuickLoad, [this](){GetSnapshot();} }
        , {CpcCmd::kCmdQuickWrite, [this](){PutSnapshot(false);} }
        , {CpcCmd::kCmdQuit, [this]() {DoQuit();} }

        , {CpcCmd::kCmdPause, [this](){SetPause(pause^1);} }
        , {CpcCmd::kCmdReset, [this](){if (pause) SetPause(0);cpc.Reset();} }
        , {CpcCmd::kCmdGhostType, [this](){ghostStr+=(std::string("\r")+GetClipboardString()+"\r").c_str();} }

        , {CpcCmd::kCmdRoms, [&](){DoRomDialog(); SetRoms();} }
        
        , {CpcCmd::kCmdLoadA, [this](){string str; if (SelectFloppyImage(str,CpcBase::DriveId::A)) LoadDisk(str,CpcBase::DriveId::A);} }
        , {CpcCmd::kCmdSaveA, [this](){SaveDisk(CpcBase::DriveId::A,drivePath[0]);} }
        , {CpcCmd::kCmdSaveAsA, [this](){SaveFloppy(CpcBase::DriveId::A);} }
        , {CpcCmd::kCmdFlipA, [this](){cpc.FlipDisk(CpcBase::DriveId::A); PlayCpcSound(Eject); PlayCpcSound(Insert); SetDrives();} }
        , {CpcCmd::kCmdEjectA, [this](){cpc.EjectDisk(CpcBase::DriveId::A); PlayCpcSound(Eject); SetDrives();} }

        , {CpcCmd::kCmdLoadB, [this](){string str; if (SelectFloppyImage(str,CpcBase::DriveId::B)) LoadDisk(str,CpcBase::DriveId::B);} }
        , {CpcCmd::kCmdSaveB, [this](){SaveDisk(CpcBase::DriveId::B,drivePath[1]);} }
        , {CpcCmd::kCmdSaveAsB, [this](){SaveFloppy(CpcBase::DriveId::B);} }
        , {CpcCmd::kCmdFlipB, [this](){cpc.FlipDisk(CpcBase::DriveId::B); PlayCpcSound(Eject); PlayCpcSound(Insert); SetDrives();} }
        , {CpcCmd::kCmdEjectB, [this](){cpc.EjectDisk(CpcBase::DriveId::B); PlayCpcSound(Eject); SetDrives();} }

        , {CpcCmd::kCmdSoundOn, [this](){SetSound(cpcConfig.makeSounds^1);} }
        , {CpcCmd::kCmdStereoOn, [this](){SetStereo(cpcConfig.stereo^1);} }
        , {CpcCmd::kCmdChannelA, [this](){SetChannels(cpcConfig.soundChannels^1);} }
        , {CpcCmd::kCmdChannelB, [this](){SetChannels(cpcConfig.soundChannels^2);} }
        , {CpcCmd::kCmdChannelC, [this](){SetChannels(cpcConfig.soundChannels^4);} }
        
        , {CpcCmd::kCmdVolume0, [this](){SetVolume(CpcSoundLevel::LEVEL_0);} }
        , {CpcCmd::kCmdVolume1, [this](){SetVolume(CpcSoundLevel::LEVEL_1);} }
        , {CpcCmd::kCmdVolume2, [this](){SetVolume(CpcSoundLevel::LEVEL_2);} }
        , {CpcCmd::kCmdVolume3, [this](){SetVolume(CpcSoundLevel::LEVEL_3);} }
        , {CpcCmd::kCmdVolume4, [this](){SetVolume(CpcSoundLevel::LEVEL_4);} }
        , {CpcCmd::kCmdVolume5, [this](){SetVolume(CpcSoundLevel::LEVEL_5);} }
        , {CpcCmd::kCmdVolume6, [this](){SetVolume(CpcSoundLevel::LEVEL_6);} }
        , {CpcCmd::kCmdVolume7, [this](){SetVolume(CpcSoundLevel::LEVEL_7);} }

        , {CpcCmd::kCmdMonochrome, [this](){SetMonochrome(cpcConfig.monochrome^1);} }

        , {CpcCmd::kCmd6128_4M, [this](){SetModel(CpcModelType::MODEL_6128_4M,1);} }
        , {CpcCmd::kCmd6128_512, [this](){SetModel(CpcModelType::MODEL_6128_512K,1);} }
        , {CpcCmd::kCmd6128, [this](){SetModel(CpcModelType::MODEL_6128,1);} }
        , {CpcCmd::kCmd664, [this](){SetModel(CpcModelType::MODEL_664,1);} }
        , {CpcCmd::kCmd464, [this](){SetModel(CpcModelType::MODEL_464,1);} }
        
        , {CpcCmd::kCmdCpmBoot, [this](){SetCpm(cpcConfig.cpmBoot^1);} }
        
        , {CpcCmd::kCmdIsp, [this](){SetConstructor(CpcBase::CpcConstructor::Isp);} }
        , {CpcCmd::kCmdTriumph, [this](){SetConstructor(CpcBase::CpcConstructor::Triumph);} }
        , {CpcCmd::kCmdSaisho, [this](){SetConstructor(CpcBase::CpcConstructor::Saisho);} }
        , {CpcCmd::kCmdSolavox, [this](){SetConstructor(CpcBase::CpcConstructor::Solavox);} }
        , {CpcCmd::kCmdAwa, [this](){SetConstructor(CpcBase::CpcConstructor::Awa);} }
        , {CpcCmd::kCmdSchneider, [this](){SetConstructor(CpcBase::CpcConstructor::Schneider);} }
        , {CpcCmd::kCmdOrion, [this](){SetConstructor(CpcBase::CpcConstructor::Orion);} }
        , {CpcCmd::kCmdAmstrad, [this](){SetConstructor(CpcBase::CpcConstructor::Amstrad);} }
        
        , {CpcCmd::kCmdAscii, [this](){SetKbdRawMode(false);} }
        , {CpcCmd::kCmdRaw, [this](){SetKbdRawMode(true);} }
        , {CpcCmd::kCmdDefineRaw, [this](){DoKeyboardDialog();} }
        
        , {CpcCmd::kCmdPrinterNone, [this](){SetPrinter(PrinterMode::PrinterNone);} }
        , {CpcCmd::kCmdPrinterFile, [this](){SetPrinter(PrinterMode::PrinterFile);} }
        , {CpcCmd::kCmdDigiblaster, [this](){SetPrinter(PrinterMode::PrinterDigiblaster);} }
        
        , {CpcCmd::kCmdHalfSize, [this](){SetScreenSmall(!cpcConfig.screenSmall);} }
        , {CpcCmd::kCmdFullScreen, [this](){SetScreenFull(!cpcConfig.screenFull);} }
        , {CpcCmd::kCmdSkip, [this](){SetScreenSkip(!cpcConfig.screenSkip);} }
        , {CpcCmd::kCmdIndicators, [this](){SetIndicators(!guiPrefs.indicators);} }
        , {CpcCmd::kCmdSaveScreen, [this](){SavePICTFile();} }
        , {CpcCmd::kCmdCopyScreen, [this](){CopyToClipboard();} }
        
        , {CpcCmd::kCmdFRate1, [this](){SetFrameRate(CpcFrameRate::RATE_1);} }
        , {CpcCmd::kCmdFRate2, [this](){SetFrameRate(CpcFrameRate::RATE_2);} }
        , {CpcCmd::kCmdFRate4, [this](){SetFrameRate(CpcFrameRate::RATE_4);} }
        , {CpcCmd::kCmdFRate8, [this](){SetFrameRate(CpcFrameRate::RATE_8);} }

        , {CpcCmd::kCmdStartup, [this](){DoStartupDialog();} }
        , {CpcCmd::kCmdForget, [this](){guiPrefs.confirmPrefs.clear();} }
        , {CpcCmd::kCmdDebugger, [this](){MonitToggle();} }
        , {CpcCmd::kCmdSpeedNone, [this](){SetLimitSpeed(cpcConfig.limitSpeed^1);} }

        , {CpcCmd::kCmdSpeed25, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_25);} }
        , {CpcCmd::kCmdSpeed50, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_50);} }
        , {CpcCmd::kCmdSpeed75, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_75);} }
        , {CpcCmd::kCmdSpeed100, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_100);} }
        , {CpcCmd::kCmdSpeed125, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_125);} }
        , {CpcCmd::kCmdSpeed150, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_150);} }
        , {CpcCmd::kCmdSpeed175, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_175);} }
        , {CpcCmd::kCmdSpeed200, [this](){SetSpeedLimit(CpcSpeedLimit::LIMIT_200);} }
        
        , {CpcCmd::kCmdRealTimeFdc, [this](){SetRealTimeFdc(cpcConfig.fdcRealTime^1);} }
    };
    std::string str;
    auto i=cmdMap.find(cmd);
    if (i==cmdMap.end()) return;
    i->second();
}

// Select a floppy image from a file
bool CpcGuiConsole::SelectFloppyImage(std::string &fName, CpcBase::DriveId drive)
{
    FileType type = FileDisk;
    int option = drive;
    return SelectFile(fName, type, option);
}

// Select a snapshot from a file
bool CpcGuiConsole::SelectSnapshot(std::string &sName)
{
    FileType type = FileSnapshot;
    int option = 0;
    return SelectFile(sName, type, option);
}

// Select a ROM from a file
bool CpcGuiConsole::SelectRom(std::string &rName, std::string &rDesc, int slot)
{
    FileType type = FileRom;
    int option = slot;
    if (!SelectFile(rName, type, option)) return false;
    
    rDesc.clear();
    // Make a ROM description string
    // If this is a background ROM, we use the first entry in the name table
    // That is a good convention used by a lot of ROMs
    BYTE block[16384];
    try {
        LoadRom(rName, block);
    } catch (infra::Err &) {
        return false;
    }
    // check the ROM type as only Foreground and background ROMs have name tables
    // Note that standard lower ROM unfortunately starts with 1 too (LD BC,7F89)
    BYTE romType = block[0]&0x7F;
    if (romType>1) return true;

    const BYTE *nameTable = block + ((block[5]&0x3F)<<8) + block[4]; // mask MSB so we stay in ROM
    do {
        rDesc += *nameTable & 0x7F;
    } while ( !(*nameTable++ & 0x80) && rDesc.size()<32);
    char version[7];
    snprintf(version,7," %d.%d.%d", block[1], block[2], block[3]);
    rDesc += version;
    return true;
}

// Select any file loadable by CPC++
bool CpcGuiConsole::SelectAny()
{
    std::string name;
    FileType type = FileAny;
    int drive = 0;
    if (!SelectFile(name, type, drive))
        return false;
    if (FilterFile(name, FileDisk))
        LoadDisk(name,drive? CpcBase::DriveId::B: CpcBase::DriveId::A);
    if (FilterFile(name, FileSnapshot))
        LoadSnapshot(name);
    if (FilterFile(name, FileCpcpp))
        LoadCpcpp(name);
    return true;
}

void CpcGuiConsole::SaveCpcpp()
{
    if (crtCpc.empty())
        SaveAsCpcpp();
    else
        SaveCurrentCpc(crtCpc);
}

void CpcGuiConsole::SaveAsCpcpp()
{
    std::string name;
    if (GetSavePath(CpcGuiMsgId::MSG_CON_SAVECPC, CpcGuiMsgId::MSG_CON_CPC_DEFAULT, name)) {
        SaveCurrentCpc(name);
        crtCpc = name;
    }
}

template<typename Archive>
void CpcGuiConsole::LoadCpcppArchive(Archive &ar)
{
    // boost::archive::binary_iarchive ar(in);
    FullState fullState;
    ar >> BOOST_SERIALIZATION_NVP(fullState);
    ASSERT_THROW(fullState.magic == kCpcppMagic);
    cpcConfig = fullState.config;
    guiPrefs = fullState.guiPrefs;
    cpc.SetState(fullState.state);
    ConnectCpc();
}

void CpcGuiConsole::LoadCpcpp(const std::string &name)
{
    ifstream in(name.c_str());
    if (!in) return;
    try {
        boost::archive::xml_iarchive ar(in);
        LoadCpcppArchive(ar);
        crtCpc = name;
    } catch (const std::exception &err) {
        in.clear();
        in.seekg(0);
        boost::archive::binary_iarchive ar(in);
        LoadCpcppArchive(ar);
        crtCpc = name;
    } catch (...) {
        ErrReport(CpcGuiMsgId(CpcGuiMsgId::ERR_CON_LOADCPC) % name.c_str());
    }
}

void CpcGuiConsole::SaveCurrentCpc(const std::string &name)
{
    ofstream out(name.c_str());
    if (!out)
        return;
    //boost::archive::xml_oarchive ar(out);
    boost::archive::binary_oarchive ar(out);
    CpcState state;
    cpc.GetState(state);
    FullState fullState(kCpcppMagic, cpcConfig, guiPrefs, state);
    ar << BOOST_SERIALIZATION_NVP(fullState);
    SetIcon(name);
}

void CpcGuiConsole::SetScreenSmall(bool val)
{
    cpcConfig.screenSmall=val;
    CmdChecked(CpcCmd::kCmdHalfSize, val);
}

void CpcGuiConsole::SetScreenFull(bool val)
{
    cpcConfig.screenFull=val;
    CmdChecked(CpcCmd::kCmdFullScreen, val);
}

void CpcGuiConsole::SetScreenSkip(bool val)
{
    cpcConfig.screenSkip=val;
    CmdChecked(CpcCmd::kCmdSkip, val);
}

void CpcGuiConsole::SetIndicators(bool val)
{
    guiPrefs.indicators=val;
    CmdChecked(CpcCmd::kCmdIndicators, val);
}

void CpcGuiConsole::SetModel(CpcModelType model,int running)
{
    if (running && cpcConfig.cpcModel != model) {
        if (!ConfirmDlg(CpcGuiMsgId(CpcGuiMsgId::MSG_MODECHANGE)))
            return;
        cpc.Reset();
    }
    cpcConfig.cpcModel = model;
    cpc.CpcModel(model);
    static const CpcCmd cmds[] = {CpcCmd::kCmd464, CpcCmd::kCmd664, CpcCmd::kCmd6128, CpcCmd::kCmd6128_512, CpcCmd::kCmd6128_4M, CpcCmd::kCmdEndOfList};
    SetCheckMark(cmds, model);
    static const char *cpcNames[]={"CPC 464","CPC 664","CPC 6128","CPC 6128 + 512K","CPC 6128 + 4M"};
    WindowTitle(cpcNames[cpcConfig.cpcModel]);
}

void CpcGuiConsole::SetPause(bool val)
{
    pause = val;
    CmdChecked(CpcCmd::kCmdPause, pause);
}

void CpcGuiConsole::StoreRom(const RomConfig &rom, const BYTE *&store)
{
    if (rom.active && !rom.path.empty()) {
        BYTE *data = new BYTE[16384];
        // Make a ROM description string
        try {
            LoadRom(rom.path, data);
            store=data;
        } catch (infra::Err &) {
            delete[]data;
        }
    }
}

void CpcGuiConsole::SetRoms()
{
    vector<const BYTE *> upperRoms(256);
    for (int i=0; i<NB_ROMSLOTS; i++)
        StoreRom(cpcConfig.upperRoms[i], upperRoms[i]);
    
    const BYTE *lowerRom = 0;
    StoreRom(cpcConfig.lowerRom, lowerRom);
    cpc.SetRoms(lowerRom, &upperRoms[0]); // takes ownership of the buffers
}

void CpcGuiConsole::SetConstructor(CpcBase::CpcConstructor constructor)
{
    CpcCmd consCmd[]={CpcCmd::kCmdIsp,CpcCmd::kCmdTriumph,CpcCmd::kCmdSaisho,CpcCmd::kCmdSolavox,CpcCmd::kCmdAwa,CpcCmd::kCmdSchneider,CpcCmd::kCmdOrion,CpcCmd::kCmdAmstrad,CpcCmd::kCmdEndOfList};
    cpcConfig.constructor=constructor;
    SetCheckMark(consCmd, constructor);
    cpc.Constructor(constructor);
}

void CpcGuiConsole::SetSoundMenu()
{
    if (CanSound()) {
        CmdEnabled(CpcCmd::kCmdSoundOn,true);
        //CmdEnabled(GetMenuHandle(kSoundMenu),kEnabled+1,true);
        CmdEnabled(CpcCmd::kCmdChannelA,true);
        CmdEnabled(CpcCmd::kCmdChannelB,true);
        CmdEnabled(CpcCmd::kCmdChannelC,true);
        if (CanStereo())
            CmdEnabled(CpcCmd::kCmdStereoOn,true);
        else
            CmdEnabled(CpcCmd::kCmdStereoOn,false);
        
    } else {
        CmdChecked(CpcCmd::kCmdSoundOn,false);
        CmdChecked(CpcCmd::kCmdChannelA,false);
        CmdChecked(CpcCmd::kCmdChannelB,false);
        CmdChecked(CpcCmd::kCmdChannelC,false);
        CmdChecked(CpcCmd::kCmdStereoOn,false);
    }
}

void CpcGuiConsole::SetSound(bool val)
{
    if (CanSound()) {
        cpcConfig.makeSounds=val;
        SoundOnOff(cpcConfig.makeSounds);
        CmdChecked(CpcCmd::kCmdSoundOn, cpcConfig.makeSounds);
    }
}

void CpcGuiConsole::SetVolume(const CpcSoundLevel &val)
{
    CpcCmd volCmds[]={CpcCmd::kCmdVolume0,CpcCmd::kCmdVolume1,CpcCmd::kCmdVolume2,CpcCmd::kCmdVolume3,CpcCmd::kCmdVolume4,CpcCmd::kCmdVolume5,CpcCmd::kCmdVolume6,CpcCmd::kCmdVolume7,CpcCmd::kCmdEndOfList};
    if (CanSound()) {
        cpcConfig.soundLevel = val;
        OsVol(cpcConfig.soundLevel);
        SetCheckMark(volCmds, val);
    }
}

void CpcGuiConsole::SetStereo(bool val)
{
    if (CanSound() && CanStereo()) {
        cpcConfig.stereo = val;
        cpc.Stereo(cpcConfig.stereo);
        CmdChecked(CpcCmd::kCmdStereoOn, cpcConfig.stereo);
    }
}

void CpcGuiConsole::SetChannels(int val)
{
    if (CanSound()) {
        val &= 7;
        cpcConfig.soundChannels=val;
        cpc.ChannelsOnOff(val&1,val&2,val&4);
        CmdChecked(CpcCmd::kCmdChannelA, (val&1)!=0);
        CmdChecked(CpcCmd::kCmdChannelB, (val&2)!=0);
        CmdChecked(CpcCmd::kCmdChannelC, (val&4)!=0);
    }
}

void CpcGuiConsole::SetMonochrome(bool val)
{
    cpcConfig.monochrome = val;
    CmdChecked(CpcCmd::kCmdMonochrome, cpcConfig.monochrome);
}

void CpcGuiConsole::SetFrameRate(const CpcFrameRate &val)
{
    CpcCmd cmds[] = {CpcCmd::kCmdFRate1,CpcCmd::kCmdFRate2,CpcCmd::kCmdFRate4,CpcCmd::kCmdFRate8,CpcCmd::kCmdEndOfList};
    cpcConfig.frameRate = val;
    frameRate=val;
    SetCheckMark(cmds, val);
}

void CpcGuiConsole::SetDrives()
{
    map<CpcBase::DriveId, array<CpcCmd, 5>> drvCmd = {
        {CpcBase::DriveId::A, {CpcCmd::kCmdLoadA,CpcCmd::kCmdSaveA,CpcCmd::kCmdSaveAsA,CpcCmd::kCmdFlipA,CpcCmd::kCmdEjectA}},
        {CpcBase::DriveId::B, {CpcCmd::kCmdLoadB,CpcCmd::kCmdSaveB,CpcCmd::kCmdSaveAsB,CpcCmd::kCmdFlipB,CpcCmd::kCmdEjectB}}
    };
    for (auto i: CpcBase::DriveId::AllValues()) {
         CmdEnabled(drvCmd[i][0],false);
         CmdEnabled(drvCmd[i][1],false);
         CmdEnabled(drvCmd[i][2],false);
         CmdEnabled(drvCmd[i][3],false);
         CmdEnabled(drvCmd[i][4],false);
        if (cpc.DriveEnabled(i)) {
             CmdEnabled(drvCmd[i][0],true);
            if (cpc.IsDriveReady(i)) {
                 CmdEnabled(drvCmd[i][4],true);
                 CmdEnabled(drvCmd[i][2],true);
                if (!cpc.IsWriteProtected(i))
                     CmdEnabled(drvCmd[i][1],true);
                if (cpc.DoubleSidedFloppy(i))
                     CmdEnabled(drvCmd[i][3],true);
             }
         }
    }
    SetRunMenus();
}

void CpcGuiConsole::SetLimitSpeed(bool val)
{
    limitSpeed=val;
    cpcConfig.limitSpeed=val;
}

void CpcGuiConsole::SetSpeedLimit(const CpcSpeedLimit &val)
{
    cpcConfig.maximumSpeed=val;
    maxSpeed=val+1;

    CpcCmd cmds[]={CpcCmd::kCmdSpeed25,CpcCmd::kCmdSpeed50,CpcCmd::kCmdSpeed75,CpcCmd::kCmdSpeed100
        ,CpcCmd::kCmdSpeed125,CpcCmd::kCmdSpeed150,CpcCmd::kCmdSpeed175,CpcCmd::kCmdSpeed200,CpcCmd::kCmdEndOfList};
    SetCheckMark(cmds, val);
    WaitForSound(val==4);
}

void CpcGuiConsole::SetRealTimeFdc(bool val)
{
    cpcConfig.fdcRealTime=val;
    CmdChecked(CpcCmd::kCmdRealTimeFdc, cpcConfig.fdcRealTime);
    cpc.RealTime(val);
}

void CpcGuiConsole::SetCpm(bool val)
{
    cpcConfig.cpmBoot=val;
    CmdChecked(CpcCmd::kCmdCpmBoot, cpcConfig.cpmBoot);
    cpc.CpmBoot(val);
}

void CpcGuiConsole::SetPrinter(CpcBase::PrinterMode val)
{
    CpcCmd cmds[]={CpcCmd::kCmdPrinterNone,CpcCmd::kCmdPrinterFile,CpcCmd::kCmdDigiblaster,CpcCmd::kCmdEndOfList};
    cpcConfig.printerOutput=val;
    SetCheckMark(cmds,val);
    cpc.PrinterOutput(val);
}

void CpcGuiConsole::SetKbdRawMode(bool val)
{
    cpcConfig.kbdRawMode=val;
    CmdChecked(CpcCmd::kCmdAscii, val==0);
    CmdChecked(CpcCmd::kCmdRaw, val==1);
}

void CpcGuiConsole::SetLastSna(const std::string &name)
{
    lastSna=name;
    if (!lastSna.empty()) {
        CmdEnabled(CpcCmd::kCmdQuickWrite, true);
        CmdEnabled(CpcCmd::kCmdQuickLoad, true);
    } else {
        CmdEnabled(CpcCmd::kCmdQuickWrite, false);
        CmdEnabled(CpcCmd::kCmdQuickLoad, false);
    }
}

void CpcGuiConsole::PutSnapshot(bool getFile)
{
    if (!lastSna.empty() && !getFile) {
        SaveCurrentSnapshot(lastSna);
    } else {
        std::string name;
        if (!lastSna.empty()) {
            std::string::size_type separator = lastSna.find_last_of(":/\\");
            if (separator)
                name = lastSna.substr(separator+1);
            else
                name = lastSna;
        }
        if (GetSavePath(CpcGuiMsgId::MSG_CON_SAVESNA, CpcGuiMsgId::MSG_CON_SNA_DEFAULT, name)) {
            SaveCurrentSnapshot(name);
        }
    }
}

void CpcGuiConsole::SaveCurrentSnapshot(const std::string &name)
{
    try {
        CpcState state;
        cpc.GetState(state);
        vector<BYTE> sna;
        Sna::StateToSna(state, sna);
        SaveSnapshot(name, &sna[0]);
        SetLastSna(name); // Remember snapshot path for QuickSave/QuickLoad
    } catch (const infra::Err &err) {
        LOG_ERROR(err.what());
        ErrReport(CpcGuiMsgId(CpcGuiMsgId::ERR_CON_SNAP)%name.c_str());
    }
}

void CpcGuiConsole::GetSnapshot()
{
    if (lastSna.empty()) return;
    LoadSnapshot(lastSna);
}

void CpcGuiConsole::LoadSnapshot(const std::string &name)
{
    BYTE *buff=0;
    try {
        int size;
        
        // Uncompress if needed
        CheckCompressed(name, FileSnapshot, 0, buff, size);
        if (buff) {
            SetLastSna("");
        } else {
            LoadFile(name, buff, size);
            SetLastSna(name);
            //SetFileType(name,FileSnapshot);
        }
        vector<BYTE> sna(buff, buff+size);
        CpcState state;
        cpc.GetState(state); // set stuff that sna can't
        Sna::SnaToState(sna, state);
        cpc.SetState(state);
    } catch (const infra::Err &err) {
        LOG_ERROR(err.what());
        ErrReport(CpcGuiMsgId(CpcGuiMsgId::ERR_CON_SNAG) % name.c_str());
    }
    delete[] buff;
}

void CpcGuiConsole::DoQuit(bool force)
{
    if (force || ConfirmDlg(CpcGuiMsgId(CpcGuiMsgId::MSG_QUITCONFIRM))) {
        quit = true;
        if (pause) SetPause(0);
        MonitOff();
    }
}

// Detect changes in the drives directories and reflect in Run menus
void CpcGuiConsole::SetRunMenus()
{
    bool changed=false;
    bool canCpm;
    for (auto drive: CpcBase::DriveId::AllValues()) {
        RunMenu newMenu;
        newMenu.active=true;
        try {
            cpc.GetDir(drive, newMenu.dir, canCpm);
        } catch (...) {
            newMenu.active=false;
        }
        if (newMenu.active!=runMenus[drive].active ||
            newMenu.dir!=runMenus[drive].dir)
            changed=true;
    }
    if (!changed) return;
    
    EmptyRunMenus();
    
    for (auto drive: CpcBase::DriveId::AllValues()) {
        try {
            cpc.GetDir(drive,runMenus[drive].dir, canCpm);
            AddItemToRunMenu(drive, "DIR", true);
            if (canCpm) AddItemToRunMenu(drive, "CPM", true);
            if (!runMenus[drive].dir.empty()) AddItemToRunMenu(drive, "", false); // separator
            for (auto item: runMenus[drive].dir)
                AddItemToRunMenu(drive, item, true);
        } catch (...) {
            runMenus[drive].active=false;
            std::string str = GetStr(CpcGuiMsgId(CpcGuiMsgId::MSG_CON_NODISK));
            AddItemToRunMenu(drive, str, false);
        }
    }
}

void CpcGuiConsole::RunProg(CpcBase::DriveId drive, int entry)
{
    char com[256];
    if (entry==0)
        snprintf(com, 256, "\r|dir%s\r", drive ? ",\"b:\"" : "");
    else {
        std::vector<string> dir;
        bool canCpm;
        try {
            cpc.GetDir(drive, dir, canCpm);
        } catch (...) {
            return;
        }
        if (!canCpm)
            entry++;
        if (entry==1)
            snprintf(com,256,"%s\r|cpm\r",drive?"\r|b":"");
        else
        {
            string name = dir[entry-3];
            // Shrink name if possible
            //if (!strcmp(name+strlen(name)-1,".")) name[strlen(name)-1]=0;
            snprintf(com, 256, "%s\rRUN\"%s\r", drive ? "\r|B" : "", name.c_str());
        }
    }
    cpc.Reset();
    pendingDelay=100;// tempo for reset
    ghostStr+=com;
}

void CpcGuiConsole::LoadDisk(const std::string &path, CpcBase::DriveId drive)
{
    try {
        // Try loading a DSK from ZIP
        bool loaded;
        LoadNthDiskFromCompressed(path, 0, drive, loaded);
        if (loaded) {
            // If it worked try a second one, toggling the drive
            LoadNthDiskFromCompressed(path, 1, drive==CpcBase::DriveId::A?CpcBase::DriveId::B:CpcBase::DriveId::A, loaded);
        } else {
            // If nothing was loaded try not as ZIP
            cpc.LoadDisk(drive, LoadFile(path), false);
            if (cpc.IsDriveReady(drive)) {
                drivePath[drive] = path;
                PlayCpcSound(Insert);
                //SetFileType(path,FileDisk);
            }
        }
    } catch (infra::Err &err) {
        LOG_ERROR(boost::format("%s %s") %err.what() %path.c_str());
        ErrReport(CpcGuiMsgId(CpcGuiMsgId::ERR_DRV_INSERT));
    }
    SetDrives();
}

void CpcGuiConsole::LoadNthDiskFromCompressed(const std::string &path, int index, CpcBase::DriveId drive, bool &loaded)
{
    loaded = false;
    BYTE *data;
    int size;
    
    // Uncompress if needed
    CheckCompressed(path, FileDisk, index, data, size);
    if (data) {
        CpcFile file(size, data);
        cpc.LoadDisk(drive,file,true);
        if (cpc.IsDriveReady(drive)) {
            PlayCpcSound(Insert);
            loaded = true;
        }
    }
}

void CpcGuiConsole::SaveFloppy(CpcBase::DriveId drive)
{
    std::string name;
    if (GetSavePath(CpcGuiMsgId::MSG_CON_SAVEIMAGE, CpcGuiMsgId::MSG_CON_DSK_DEFAULT, name)) {
        SaveDisk(drive,name);
        //SetFileType(name, FileDisk);
    }
}

void CpcGuiConsole::SaveDisk(CpcBase::DriveId drive, const std::string &path)
{
    CpcFile file(1024*1024);
    cpc.SaveDisk(drive,file);
    SaveFile(path, file.Data(), file.Cursor());
    //SetFileType(path, FileDisk);
}

bool CpcGuiConsole::IsPaused()const
{
    return pause;
}

// Palette definition
const RAW32 * CpcGuiConsole::P32()
{
    // colors by Hardware Colour Index
    // White, White, Sea Green, Pastel Yellow, Blue, Purple, Cyan, Pink
    // Purple, Pastel Yellow, Bright Yellow, Bright White, Bright Red, Bright Magenta, Orange, Pastel Magenta
    // Blue, Sea Green, Bright Green, Bright Cyan, Black, Bright Blue, Green, Sky Blue
    // Magenta, Pastel Green, Lime, Pastel Cyan, Red, Mauve, Yellow, Pastel Blue
    static const RAW32 p32[] = {
        0xFF7F7F7F, 0xFF7F7F7F, 0xFF00FF7F, 0xFFFFFF7F, 0xFF00007F, 0xFFFF007F, 0xFF007F7F, 0xFFFF7F7F,
        0xFFFF007F, 0xFFFFFF7F, 0xFFFFFF00, 0xFFFFFFFF, 0xFFFF0000, 0xFFFF00FF, 0xFFFF7F00, 0xFFFF7FFF,
        0xFF00007F, 0xFF00FF7F, 0xFF00FF00, 0xFF00FFFF, 0xFF000000, 0xFF0000FF, 0xFF007F00, 0xFF007FFF,
        0xFF7F007F, 0xFF7FFF7F, 0xFF7FFF00, 0xFF7FFFFF, 0xFF7F0000, 0xFF7F00FF, 0xFF7F7F00, 0xFF7F7FFF,
        0xFF000000
    };
    return p32;
}

const RAW32 * CpcGuiConsole::M32()
{
    static RAW32 m32[33]={0};
    
    // Monochrom LUTs are computed using green brightness levels, which, incidentaly
    // are firmware color indices. So we use the hard-to-firmware index map below
    static unsigned char greenVal[32] =
    {13,13,19,25,1,7,10,16,7,25,24,26,6,8,15,17,1,19,18,20,0,2,9,11,4,22,21,23,3,5,12,14};
    
    if (m32[0]==0)
    {
        double greenStep=255.0/26.0;
        for (int i=0; i<32; i++)
            m32[i] = 0xFF000000 + ((int)((float)greenVal[i] * greenStep + .5) << 8);
        m32[32] = 0xFF000000;
    }
    return m32;
}

void CpcGuiConsole::LoadFile(const std::string &path, BYTE *&data, int &size)
{
    std::ifstream file(path.c_str(), std::ios::binary | std::ios::in);
    ASSERT_THROW(file)("File open failed.");
    file.seekg(0, std::ios::end);
    std::streamoff fileSize = file.tellg();
    ASSERT_THROW(fileSize<1000000);
    size = static_cast<int>(fileSize);
    data = new BYTE[size];
    file.seekg(0);
    file.read((char *)data, size);
}

CpcFile CpcGuiConsole::LoadFile(const std::string &path)
{
    std::ifstream file(path.c_str(), std::ios::binary | std::ios::in);
    ASSERT_THROW(file)("File open failed.");
    file.seekg(0, std::ios::end);
    std::streamoff fileSize = file.tellg();
    ASSERT_THROW(fileSize<1000000);
    CpcFile cpcFile(static_cast<int>(fileSize));
    file.seekg(0);
    file.read((char *) cpcFile.Data(), cpcFile.Size());
    return cpcFile;
}

void CpcGuiConsole::SaveFile(const std::string &path, const BYTE *data, int size)
{
    FILE *fd =fopen(path.c_str(),"wb");
    ASSERT_THROW(fd);
    fwrite(data,size,1,fd);
    fclose(fd);
}

// does it have an Amsdos header?
static bool SkipAmsdosHeader(FILE *fd, size_t &nBytes)
{
    if (nBytes <= 128) return false;
    unsigned char header[128];    
    fread(header,1,128,fd);
    int i, checksum = 0;
    unsigned char* p = (unsigned char*)&header;
    for (i = 0; i < 67; i++)
        checksum += header[i];
    if (checksum == *(unsigned short *)(header+67)) {
        nBytes -= 128;
        return true;
    }
    fseek(fd, 0, SEEK_SET);
    return false;
}

void CpcGuiConsole::LoadRom(const std::string &path, BYTE *buff)
{
    FILE *fd =fopen(path.c_str(),"rb");
    ASSERT_THROW(fd);
    size_t nBytes = boost::filesystem::file_size(path);
    if (SkipAmsdosHeader(fd, nBytes))
        LOG_STATUS("Skipping Amsdos header for ROM %s", path.c_str());
    if (nBytes > 16384)
        LOG_STATUS("Clipping ROM (nBytes=%d) %s", nBytes, path.c_str());
    if (nBytes < 16384)
        LOG_STATUS("Padding ROM (nBytes=%d) %s", nBytes, path.c_str());
    memset(buff, 0, 16384);
    fread(buff,1,16384,fd);
    fclose(fd);
}

bool CpcGuiConsole::IsRomImage(const std::string &path, const BYTE *data)
{
    if (IsFileExtension(path, kRomExt)) return true;
    if (data) { }
    return false;
}

bool CpcGuiConsole::IsDiskImage(const std::string &path, const BYTE *data)
{
    if (IsFileExtension(path, kDskExt)) return true;
    if (data) {
        if (!strncmp((const char *)data, kDskMagic, 8)) return true;
        if (!strncmp((const char *)data, kDskMagic2, 8)) return true;
        if (*data==2) return true;
    }
    return false;
}

bool CpcGuiConsole::IsSnapshot(const std::string &path, const BYTE *data)
{
    if (IsFileExtension(path, kSnaExt)) return true;
    if (data) {
        if (!strncmp((const char *)data, kSnaMagic, 8)) return true;
    }
    return false;
}

bool CpcGuiConsole::IsZipFile(const std::string &path)
{
    if (IsFileExtension(path, kZipExt)) return true;
    return false;
}

bool CpcGuiConsole::IsFileType(const std::string &path, const BYTE *data, FileType type)
{
    TypeFilter filter = GetFilter(type);
    return (*filter)(path, data);
}

bool CpcGuiConsole::IsCpcpp(const std::string &path, const BYTE *data)
{
    if (IsFileExtension(path, kCpcppExt)) return true;
    if (data && !strncmp((const char *)data, kCpcppMagic, 8)) return true;
    return false;
}

typename CpcGuiConsole::TypeFilter CpcGuiConsole::GetFilter(FileType type)
{
    switch (type) {
        case FileDisk:
            return IsDiskImage;
        case FileSnapshot:
            return IsSnapshot;
        case FileRom:
            return IsRomImage;
        case FileCpcpp:
            return IsCpcpp;
        default:
            return IsAnyFile;
    }
    return 0;
}

bool CpcGuiConsole::IsAnyFile(const std::string &path, const BYTE *data)
{
    if (IsDiskImage(path, data))
        return true;
    if (IsSnapshot(path, data))
        return true;
    return false;
}

// Render current frame into the buffer
vector<uint32_t> CpcGuiConsole::RenderToBuffer(bool small)
{
    int width = Ga::CPC_SCR_W / (small? 2: 1);
    int height = Ga::CPC_SCR_H / (small? 2: 1);
    
    vector<uint32_t> buffer(width * height);
    CpcBase::RenderInfo renderInfo(CpcDib32(&buffer[0], height, 4 * width), cpcConfig.monochrome? M32(): P32(), CpcRect(0, 0, Ga::CPC_SCR_H-1, Ga::CPC_SCR_W-1)
    , small? CpcBase::RenderMode::SMALL: CpcBase::RenderMode::FULL);
    cpc.RenderFrame(renderInfo, true);
    return buffer;
}

void CpcGuiConsole::ErrReport(const infra::MsgI18n &msg)
{
    PutError(msg.GetText());
}

void CpcGuiConsole::LoadBinary(const BinaryToLoad &load, CpcState &state)
{
    BYTE *buff = 0;
    int size;
    LoadFile(load.path, buff, size);
    BYTE *src = buff;
    for (int pt = load.address; pt < load.address + size; pt++) {
        int block = pt / 16384;
        int offset = pt % 16384;
        state.ga.ram[block].Pt()[offset] = *src++;
    }
    delete buff;
}

void Timer::Start()
{
    start = getCrtUs();
}

unsigned long Timer::LapseUs()
{
    unsigned long crt,lapse;
    GetBoth(crt,lapse);
    start=crt;
    return lapse;
}

unsigned long Timer::ElapsedUs()
{
    unsigned long crt,lapse;
    GetBoth(crt,lapse);
    return lapse;
}

void Timer::GetBoth(unsigned long &crt, unsigned long &lapse)
{
    crt = getCrtUs();
    if (crt>start)
        lapse = crt-start;
    else
        lapse = 0xFFFFFFFF-(start-crt)+1;
}
