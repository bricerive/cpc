#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file cpcconfig.h - Runtime parameters management
#include "cpc_base.h"

#include "infra/struct_with_features.h"

#include <boost/serialization/version.hpp>

static const int NB_ROMSLOTS=32;
static const int PATH_BYTES=1024;

RICH_ENUM(CpcSoundLevel, (LEVEL_0)(LEVEL_1)(LEVEL_2)(LEVEL_3)(LEVEL_4)(LEVEL_5)(LEVEL_6)(LEVEL_7));
RICH_ENUM(CpcFrameRate, (RATE_1)(RATE_2)(RATE_4)(RATE_8));
RICH_ENUM(CpcSpeedLimit, (LIMIT_25)(LIMIT_50)(LIMIT_75)(LIMIT_100)(LIMIT_125)(LIMIT_150)(LIMIT_175)(LIMIT_200));

#define CPC_CONFIG_STRUCT(name, fields) \
    STRUCT_WITH_FEATURES(name, (SWF_FIELDS) (SWF_CTOR) (SWF_CTOR_PARAMS) (SWF_SERIALIZATION), fields)
#define CPC_CONFIG_STRUCT_VERSIONED(name, fields) \
    STRUCT_WITH_FEATURES(name, (SWF_FIELDS) (SWF_CTOR) (SWF_CTOR_PARAMS) (SWF_SERIALIZATION_VERSIONED), fields)

/// configuration info for a ROM file
CPC_CONFIG_STRUCT(RomConfigBase,
    (bool, active)
    (std::string, path)
    (std::string, description)
);
struct RomConfig:public RomConfigBase {
    RomConfig(): RomConfigBase(false,"","") {}
};

typedef std::array<RomConfig, NB_ROMSLOTS> UpperRoms;

struct CpcConfigBase;
static const int CpcConfigBaseVersion = 1;
BOOST_CLASS_VERSION(CpcConfigBase, CpcConfigBaseVersion);
struct GaState;

CPC_CONFIG_STRUCT_VERSIONED(CpcConfigBase,
    // Speed control
    (CpcFrameRate, frameRate)(bool, limitSpeed)(CpcSpeedLimit, maximumSpeed) // MaximumSpeed is in Mhz, normal CPC is 4 - none is +1000
    (bool, speedLimitOff)
    (CpcBase::CpcModelType, cpcModel)(bool, monochrome)(bool, hasDigiblaster)(bool, driveA)(bool, driveB)
    // File locations
    (std::string, driveADiskPath)(std::string, driveBDiskPath)(std::string, snapshotPath)(std::string, startupComm)
    // CPC Jumpers
    (bool, cpmBoot)(CpcBase::CpcConstructor, constructor)
    // Sounds
    (bool, makeSounds)(CpcSoundLevel, soundLevel)(bool, stereo)(uint8_t, soundChannels) // A=1 B=2 C=4
    // Screen
    (bool, screenSmall)(bool, screenFull)(bool, screenSkip)
    (bool, fdcRealTime)(CpcBase::PrinterMode, printerOutput)(bool, kbdRawMode)
    // ROMs
    (UpperRoms, upperRoms)(RomConfig, lowerRom));

#define NVP(x) BOOST_SERIALIZATION_NVP(x)
#define NVP2(x) boost::serialization::make_nvp(BOOST_PP_STRINGIZE(x), o.x)

template<typename Archive>
void CpcConfigBase::DeserializeBack(Archive &ar, int version, CpcConfigBase &o)
{
    std::string printerFile;
    switch (version) {
    case 0: // version 0 had printerFile
        ar &NVP2(frameRate) & NVP2(limitSpeed) & NVP2(maximumSpeed) & NVP2(speedLimitOff) & NVP2(cpcModel)
            & NVP2(monochrome) & NVP2(hasDigiblaster) & NVP2(driveA) & NVP2(driveB) & NVP2(driveADiskPath)
            & NVP2(driveBDiskPath) & NVP2(snapshotPath) & NVP2(startupComm) & NVP(printerFile)
            & NVP2(cpmBoot) & NVP2(constructor) & NVP2(makeSounds) & NVP2(soundLevel) & NVP2(stereo)
            & NVP2(soundChannels) & NVP2(screenSmall) & NVP2(screenFull) & NVP2(screenSkip)
            & NVP2(fdcRealTime) & NVP2(printerOutput) & NVP2(kbdRawMode) & NVP2(upperRoms) & NVP2(lowerRom);
        break;
    }
}
struct CpcConfig: public CpcConfigBase {
    CpcConfig()
        : CpcConfigBase(CpcFrameRate::RATE_1, true, CpcSpeedLimit::LIMIT_100, false, CpcBase::CpcModelType::MODEL_6128, false,
              false, true, true, "", "", "", "", false, CpcBase::CpcConstructor::Amstrad, true, CpcSoundLevel::LEVEL_7,
              true, 7, false, false, true, true, CpcBase::PrinterMode::PrinterNone, true, {}, {})
    {}
};
