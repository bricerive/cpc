// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// amstrad_text.cpp allow drawing text with the original CPC font

#include "infra/error.h"
#include "amstrad_text.h"

AmstradText::AmstradText(CpcDib32 fontDib)
    :fontDib(fontDib)
{
}

void AmstradText::RectFill(CpcDib32 dst, const CpcRect &rect, uint32_t color)
{
    for (int row=rect.Top(); row<rect.Bottom(); row++) {
        RAW32 *pixP = dst.data(row);
        for (int col=rect.Left(); col<rect.Right(); col++) {
            *(pixP+col)=color;
        }
    }
}

void AmstradText::DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char *str)const
{
    for (int idx=0; str[idx]; idx++) {
        char c = str[idx];
        if (c<' ') c='.';
        for (int i=0; i<8; i++) {
            RAW32 *pixP = dst.data(y + i) + x + 8 * idx;
            RAW32 *fontP = fontDib.data(i) + 8 * (c - ' ');
            for (int j=0; j<8; j++) {
                if (*(fontP+j)!=0xFFFFFFFF)
                    *(pixP+j)=color;
            }
        }
    }
}
