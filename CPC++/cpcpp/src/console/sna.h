#pragma once
//
//  sna.h
//  CPC++
//
//  Created by Brice Rivé on 10/28/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//
#include "cpctypes.h"
#include <vector>

struct CpcState;

class Sna {
public:
    static void StateToSna(const CpcState &state, std::vector<BYTE> &sna);
    static void SnaToState(const std::vector<BYTE> &sna, CpcState &state);
};
