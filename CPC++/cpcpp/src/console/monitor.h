#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file monitor.h - CPC monitor/debugger

#include "z80.h"
#include "crtc.h"
#include "ga.h"
#include "cpc_base.h"
#include "cpc_state.h"

class MonitorButton;

// Interface to support Monitor
class MonitorConsole {
public:
    virtual bool GetHexValue(const char *prompt, int &val)=0;
    virtual void DrawAmstradText(CpcDib32 dst, int x, int y, uint32_t color, const char *str)const=0;
    virtual const RAW32 *GetP32()=0;
};

/// Monitor/Debugger for the CPC
class Monitor {
public:
    Monitor(MonitorConsole &console); // console is needed for GetHexValue and DrawAmstradText

    void SetCpc(MonitoredCpc &cpc);

    // return true to give the hand to the CPC, false to stay in the monitor
    void MouseDown(int x, int y);

    void FuncKey(int key, bool shift);

    void MonitorTrap() {cpcHeld=true;}
    void MonitorRelease() {cpcHeld=false;}
    bool CpcHeld()const {return cpcHeld;}
    
    void Refresh();

    CpcRect WinRect()const;
    CpcDib32 Dib();

private:
    Monitor(const Monitor&);
    Monitor &operator=(const Monitor &);

    void MonitorClear();
    void MonitorText(int row, int col, const char *str, int fore=-1, int back=-1);
    void MonitorMsg(const char *msg);
    void DrawUINT4(int row, int col, int val);
    void DrawUINT8(int row, int col, int val);
    void DrawUINT16(int row, int col, int val);
    void DrawButton()const;
    void DrawRegister1(int row, int col, int val, int lastVal);
    void DrawRegister2(int row, int col, int val, int lastVal);
    void DrawRegister3(int row, int col, int val, int lastVal);
    void DrawRegister8(int row, int col, int val, int lastVal);
    void DrawRegister08X(int row, int col, int val, int lastVal);
    void DrawRegister08XH(int row, int col, int val, int lastVal);
    void DrawRegister16(int row, int col, int val, int lastVal);
    void DrawRegister16X(int row, int col, int val, int lastVal);
    void DrawColor(int row, int col, int val, int lastVal);

    void SizeStr(char *str, int n);

    typedef std::function<bool ()> Action; // returns true if CPC should be released

    class Buttons {
        static const unsigned int MAX_NB_BUTTONS=1024;
        struct Button {
            unsigned int r,c;
            int c0,c1;
            char caption[32];
            Action action;
            bool Clicked(unsigned int x, unsigned int y)const;
        };
        Button buttons[MAX_NB_BUTTONS];
        unsigned int nButtons;
        Monitor &osm;
    public:
        Buttons(Monitor &osm_): nButtons(0), osm(osm_) {}
        void Reset() {nButtons=0;}
        void Add(int r, int c, const char *caption, int c0, int c1, Action action);
        bool Click(unsigned int x, unsigned int y);
    };
    Buttons buttons;

    MonitoredCpc *cpc;

    // Monitor Window
    int monitWinC, monitWinR;
    int monitWinW, monitWinH;

    // Monitor Offscreen
    std::vector<RAW32> canvas;

    // Colors
    int monitForeColor;
    int monitBackColor;
    int monitAltBackColor;
    int monitState;
    VduState lastVdu;
    CpuState lastCpu;
    CrtcState lastCrtc;
    GaState lastGa;
    WORD memAddr;
    BYTE lastMem[256];

    bool cpuTrace=false;
    bool crtcTrace=false;
    bool cpcHeld=false;

    MonitorConsole &console;
};
