#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file cpcguiconsole.h - control part of a GUI for CPC++

// Controls a CPC from a UI point of view. Deals with:
// -Connecting/disconnecting CPC to UI
// -Enabling/Disabling commands based on CPC state
// -Handling commands
#include "cpc_base.h"
#include "cpc_state.h"
#include "cpcconfig.h"
#include "cpcconsole.h"
#include "cpcphase.h"

#include "infra/i18n.h"
#include "infra/rich_enum.h"
#include "infra/struct_with_ctor.h"

#include <string>
#include <vector>
#include <optional>

class CpcBase;

/// Define the GUI command we support for interacting with CPC++
RICH_ENUM(CpcCmd, (kCmdEndOfList)
    (kCmdAbout)
    (kCmdWebUrl)
    (kCmdMailUrl)
    (kCmdSaveSnapshot)
    (kCmdQuickLoad)
    (kCmdQuickWrite)
    (kCmdOpenAny)
    (kCmdQuit)
    (kCmdSave)
    (kCmdSaveAs)
    (kCmdPause)
    (kCmdReset)
    (kCmdGhostType)
    (kCmdRoms)
    (kCmdLoadA)(kCmdSaveA)(kCmdSaveAsA)(kCmdFlipA)(kCmdEjectA)
    (kCmdLoadB)(kCmdSaveB)(kCmdSaveAsB)(kCmdFlipB)(kCmdEjectB)
    (kCmdSoundOn)
    (kCmdStereoOn)
    (kCmdChannelA)(kCmdChannelB)(kCmdChannelC)
    (kCmdVolume0)(kCmdVolume1)(kCmdVolume2)(kCmdVolume3)(kCmdVolume4)(kCmdVolume5)(kCmdVolume6)(kCmdVolume7)
    (kCmdMonochrome)
    (kCmd6128_4M)(kCmd6128_512)(kCmd6128)(kCmd664)(kCmd464)
    (kCmdCpmBoot)
    (kCmdIsp)(kCmdTriumph)(kCmdSaisho)(kCmdSolavox)(kCmdAwa)(kCmdSchneider)(kCmdOrion)(kCmdAmstrad)
    (kCmdAscii)(kCmdRaw)
    (kCmdDefineRaw)
    (kCmdPrinterNone)(kCmdPrinterFile)(kCmdDigiblaster)
    (kCmdHalfSize)
    (kCmdFullScreen)
    (kCmdSkip)
    (kCmdIndicators)
    (kCmdSaveScreen)
    (kCmdCopyScreen)
    (kCmdFRate1)(kCmdFRate2)(kCmdFRate4)(kCmdFRate8)
    (kCmdStartup)
    (kCmdForget)
    (kCmdDebugger)
    (kCmdSpeedNone)
    (kCmdSpeed25)(kCmdSpeed50)(kCmdSpeed75)(kCmdSpeed100)(kCmdSpeed125)(kCmdSpeed150)(kCmdSpeed175)(kCmdSpeed200)
    (kCmdRealTimeFdc));


/// internationalizable messages
I18N_GROUP(CpcGuiMsgId,
    (MSG_QUITCONFIRM,"Are you sure you want to quit CPC++ (unsaved changes will be lost) ?")
    (ERR_CON_SNAG,"Cannot load snapshot %s.")
    (ERR_CON_SNAP,"SNA file dump failed (%s).")
    (MSG_MODECHANGE,"Changing the model requires a reset of the virtual CPC.")
    (MSG_CON_SAVEIMAGE,"Disk image to save.")
    (MSG_CON_SNA_DEFAULT,"Untitled.sna")
    (MSG_CON_CPC_DEFAULT,"Untitled.cpcpp")
    (MSG_CON_DSK_DEFAULT,"Untitled.dsk")
    (MSG_CON_UNTITLED_PICT,"Untitled.png")
    (MSG_CON_SAVECPC,"CPC++ file to save.")
    (ERR_CON_LOADCPC,"Cannot load CPC++ saved state %s.")
    (MSG_CON_SAVESNA,"Snapshot file to save.")
    (MSG_CON_SAVEPICT,"Picture file to save.")
    (MSG_CON_NODISK,"No disk in drive")
    (ERR_DRV_INSERT,"Disk insert fail (check log for details)"));

/// A simple timer class
class Timer {
public:
    Timer(std::function<unsigned long()> getCrtUs): start(0), getCrtUs(getCrtUs) {}
    virtual ~Timer() {}
    void Start();
    unsigned long ElapsedUs();
    unsigned long LapseUs();
private:
    void GetBoth(unsigned long &crt, unsigned long &lapse);
    unsigned long start;
    std::function<unsigned long()> getCrtUs;
};

// Define the interface that OS-specific subclasses must provide
// These are the functionss each concrete implementation needs to provide
class OsGuiConsole {
public:
    typedef enum {FileDisk, FileSnapshot, FileRom, FileAny, FileZip, FileCpcpp} FileType;
    typedef enum {Insert, Eject} CpcSound;

    // Called by this class - to be provided by subclass
    virtual void PutError(const std::string &str)=0;
    virtual bool SelectFile(std::string &path, FileType &type, int &option)=0;
    virtual void CheckEvents()=0;
    virtual void SleepUs(UINT32 us)=0;
    virtual unsigned long TicksMs()=0;
    virtual unsigned long GetCrtUs()=0;
    virtual void FrameReady()=0;
    virtual void ConnectCpc() = 0;
    virtual void OsVol(const CpcSoundLevel &val)=0;
    virtual void WaitForSound(bool val)=0;
    virtual void SpeedReport(UINT32 elapsed, UINT32 executed, UINT32 expected)=0;
    virtual void CmdChecked(CpcCmd cmd, bool checked)=0;
    virtual void CmdEnabled(CpcCmd cmd, bool enabled)=0;
    virtual void WindowTitle(const std::string &title)=0;
    virtual void LaunchUrl(const std::string &url)=0;
    virtual void PlayCpcSound(CpcSound sound)=0;
    virtual void CopyToClipboard()=0;
    virtual void SavePICTFile()=0;
    virtual void EmptyRunMenus()=0;
    virtual void AddItemToRunMenu(CpcBase::DriveId drive, const std::string &item, bool enabled)=0;
    virtual void SaveSnapshot(const std::string &path, const BYTE *buff) = 0;
    virtual void SetIcon(const std::string &path) = 0;
    virtual bool GetSavePath(CpcGuiMsgId prompt, CpcGuiMsgId defaultName, std::string &path) = 0;
    virtual void SetScreenSmall(bool val)=0;
    virtual void SetScreenFull(bool val)=0;
    virtual void SetScreenSkip(bool val)=0;
    virtual void SetIndicators(bool val)=0;
    virtual void SetMonochrome(bool val)=0;
    virtual bool ConfirmDlg(const infra::MsgI18n &msgId)=0;
    virtual void DoAboutDialog()=0;
    virtual void DoRomDialog() = 0;
    virtual void DoKeyboardDialog()=0;
    virtual void DoStartupDialog()=0;
    virtual void SetFileType(const std::string &path, FileType type)=0;
    virtual void CheckCompressed(const std::string &name, FileType type, int index, BYTE *&data, int &size)=0;
    virtual bool FilterFile(const std::string &path, FileType type)=0;

    virtual void MonitToggle()=0;
    virtual void MonitOff()=0;
    virtual std::string GetClipboardString()=0;
    virtual bool CanSound()const=0;
    virtual bool CanStereo()const=0;
    virtual void StartSound()=0;
    virtual void StopSound()=0;

    virtual std::string GetStr(const infra::MsgI18n &errId)=0;
};

STRUCT_WITH_CTOR(BinaryToLoad, (std::string, path) (int, address));
typedef std::vector<BinaryToLoad> BinariesToLoad;
typedef std::vector<int> BreakPoints;
typedef std::optional<int> OptionalInt;
typedef std::optional<std::string> OptionalStr;
STRUCT_WITH_CTOR(Args,
    (std::string, exePath)(std::string, exeName)(BinariesToLoad, binariesToLoad)(BreakPoints, breakPoints)(OptionalInt, jumpAddr)(
        OptionalStr, diskA)(OptionalStr, diskB)(OptionalStr, snapshot)(OptionalStr, startupCommand));
typedef std::vector<std::string> ArgsVec;

/// Control part of a GUI for CPC++
/// Implements the logic of CPC++'s GUI so it can be shared by all the platforms
/// The specifics must be provided by the concrete subclass through the OsGuiConsole interface
class CpcGuiConsole: public CpcConsole, public OsGuiConsole {
public:
    explicit CpcGuiConsole(const ArgsVec &args);
    ~CpcGuiConsole();

    void ConnectCpc();
    void RunStep();
    
    // Hard LUT palette  as ARGB
    static const RAW32* P32();
    static const RAW32* M32();
    
    void DoCommand(CpcCmd cmd);// Called by OS-specific class
    bool IsQuit()const { return quit; }

    void RunProg(CpcBase::DriveId drive, int entry);

protected:

    // provide base implementation for these:
    void SetScreenSmall(bool val);
    void SetScreenFull(bool val);
    void SetScreenSkip(bool val);
    void SetIndicators(bool val);
    void SetMonochrome(bool val);
    
    typedef bool (*TypeFilter)(const std::string &, const BYTE *);
    static TypeFilter GetFilter(FileType type);
    static bool IsFileType(const std::string &name, const BYTE *data, FileType type);
    static bool IsDiskImage(const std::string &name, const BYTE *data);
    static bool IsSnapshot(const std::string &name, const BYTE *data);
    static bool IsZipFile(const std::string &name);
    static bool IsRomImage(const std::string &name, const BYTE *data);
    static bool IsCpcpp(const std::string &name, const BYTE *data);
    static bool IsAnyFile(const std::string &name, const BYTE *data);
    
    /// Error reporting that goes to the user - bounces back on GetStr/PutError
    void ErrReport(const infra::MsgI18n &msg);
    
    // Called by OS-specific class
    void DoStartupEvents();
    void AsciiDown(int code);
    void AsciiUp(int code);
    void AsciiDownUp(int code);
    void CpcKeyDown(int code);
    void CpcKeyUp(int code);
    void WindowClick(int x, int y)const;
    void Suspend();
    void Resume();
    void SetRunMenus();
    std::vector<uint32_t> RenderToBuffer(bool smallScreen=false);
    
    static void LoadFile(const std::string &path, BYTE *&data, int &size);
    static CpcFile LoadFile(const std::string &path);
    static void SaveFile(const std::string &path, const BYTE *data, int size);
    static void LoadRom(const std::string &path, BYTE *buff);
    
    bool IsPaused()const;
    void DelayedStartupEvents();

    void LoadDisk(const std::string &name, CpcBase::DriveId drive);
    void LoadNthDiskFromCompressed(const std::string &path, int index, CpcBase::DriveId drive, bool &loaded);
    void LoadSnapshot(const std::string &name);
    void LoadCpcpp(const std::string &name);
    void SaveCpcpp();
    void SaveAsCpcpp();
    void DoQuit(bool force=false);
    void SaveFloppy(CpcBase::DriveId drive);
    bool SelectAny();
    bool SelectFloppyImage(std::string &fName, CpcBase::DriveId drive);
    bool SelectSnapshot(std::string &sName);
    bool SelectRom(std::string &sName, std::string &rDesc, int slot);
    void SaveDisk(CpcBase::DriveId drive, const std::string &path);
    
    static const int MAX_CONFIRMS = 8;
    static const int MAX_CONFIRM_ID_SZ = 64;

    /// store the part of the config specific to the CpcGuiConsole (preferences)
    CPC_CONFIG_STRUCT(ConfirmPref,(bool, val)(std::string, id));

    CPC_CONFIG_STRUCT(GuiPrefsBase,
        (bool, indicators)
        (std::vector<ConfirmPref>, confirmPrefs)
     );
    struct GuiPrefs: public GuiPrefsBase{
        GuiPrefs(): GuiPrefsBase(true,std::vector<ConfirmPref>())
        {}
    };

    // Preference attributes
    GuiPrefs guiPrefs;
    CpcConfig cpcConfig;

    /// the reference to the CPC object.
    /// \todo I am trying to loosen the coupling between the console and the CPC:
    /// -By using abstract interfaces between them (CpcBase/CpcConsole)
    /// -To be able to have multiple CPC's running in the app (soon)
    /// -I am thinking about using stronger encapsulation that does not rely on polymorphism.
    /// In the current state, the CPC is still strongly coupled as it is there at ctor time
    /// and inited by ctors of OsConsole.
    /// I need to tighten the CPC setup so it happens in only one place.
    CpcBase &cpc;

    Args args;

private:
    CpcGuiConsole &operator=(const CpcGuiConsole&);
    CpcGuiConsole(const CpcGuiConsole&);

    template<typename Archive>
    void LoadCpcppArchive(Archive &ar);


    // Scheduler
    static const int evtCheckMs = 20;
    Phase evtPhase = Phase::FromMs(evtCheckMs);
    static const int speedCheckMs = 100;
    Phase speedPhase = Phase::FromMs(speedCheckMs);

    static const int ghostCheckFirstMs = 2500;
    Phase ghostPhase = Phase::FromMs(ghostCheckFirstMs);
    Timer timer;

    bool delayedStartupEventsDone = false;
    bool pause = false;
    bool quit = false;
    int frameCnt = 0;
    CpcFrameRate frameRate=CpcFrameRate::RATE_1;
    CpcDuration SpeedControl();
    CpcDuration GhostTyping();
    void InsertKeys();
    std::string pendingStr;
    std::string ghostStr;
    int pendingDelay = 0;

    bool limitSpeed=true;
    int maxSpeed=4;
    
    void SetModel(CpcBase::CpcModelType model,int running);
    void SetPause(bool val);
    void SetRoms();
    void StoreRom(const RomConfig &rom, const BYTE *&store);
    void SetConstructor(CpcBase::CpcConstructor constructor);
    void SetSoundMenu();
    void SetSound(bool val);
    void SetVolume(const CpcSoundLevel &val);
    void SetStereo(bool val);
    void SetChannels(int val);
    void SetFrameRate(const CpcFrameRate &val);
    void SetDrives();
    void SetLimitSpeed(bool val);
    void SetSpeedLimit(const CpcSpeedLimit &val);
    void SetRealTimeFdc(bool val);
    void SetCpm(bool val);
    void SetPrinter(CpcBase::PrinterMode val);
    void SetKbdRawMode(bool val);
    void SetLastSna(const std::string &name);
    
    void SoundOnOff(bool on);
    bool makeSounds = false;
    
    static const int NB_DRIVES=2;
    struct RunMenu {
        bool active=false;
        std::vector<std::string> dir;
    };
    RunMenu runMenus[NB_DRIVES];
    
    void PutSnapshot(bool getFile);
    void GetSnapshot();
    void SaveCurrentSnapshot(const std::string &name);
    void SaveCurrentCpc(const std::string &name);
    void SetCheckMark(const CpcCmd *cmds, int idx);

    void LoadBinary(const BinaryToLoad &load, CpcState &state);

    CPC_STATE_STRUCT(FullState, (std::string, magic)(CpcConfig, config)(GuiPrefs, guiPrefs)(CpcState, state));

    std::string lastSna;
    std::string crtCpc;

    std::string drivePath[2];
};
