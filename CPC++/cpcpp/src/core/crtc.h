#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file crtc.h - Cathod Ray Tube Controller emulation
#include "periph.h"

struct CrtcState;

/// Cathod Ray Tube Controller emulation
class Crtc:public Peripheral {
  public:
    struct Client {
        virtual void CrtcRegisterChanged(Phase crtPhase, int ar, int val)const=0;
        virtual void CrtcChanged(int ar)=0;
    };
    Crtc(Client &client, TrapFlag &trapFlag);
    virtual ~Crtc() {}

    // Monitoring
    void TrapRegisterChanges(bool on) {trapRegisterChanges=on;}
    static const char *RegName(int i);

    // Chip signals
    virtual void Reset();
    virtual void Put(Phase phase, WORD addr, BYTE val);
    virtual BYTE Get(Phase phase, WORD addr);

    void SetVSync(int val){vSync=val;}

    // Access registers contents
    BYTE Reg(int i)const {return r[i];}
    BYTE HTot()const {return r[0];}
    BYTE HDisp()const {return r[1];}
    BYTE HSync()const {return r[2];}
    BYTE HWidth()const {return r[3]&0x0F;}
    BYTE VWidth()const {return (model==1||model==2||((r[3]>>4)==0))?16:(r[3]>>4);}
    BYTE VTot()const {return r[4];}
    BYTE VAdj()const {return r[5];}
    BYTE VDisp()const {return r[6];}
    BYTE VSync()const {return r[7];}
    BYTE Skew()const {return (r[8]>>4)&3;}
    BYTE Interlace()const {return r[8]&3;}
    BYTE VScan()const {return r[9];}
    UINT16 MemAdd()const {return (r[12]<<8)+r[13];}

    // Serialization

    void GetState(CrtcState &state)const;
    void SetState(const CrtcState &sate);

  private:
    Crtc &operator=(const Crtc&);
    Crtc(const Crtc&);

    // Wiring
    Client &client;

    // Monitor support
    TrapFlag &trapFlag;
    bool trapRegisterChanges=false;

    // internal state
    BYTE addressRegister=0;
    BYTE r[32];

    void WriteRegister(int index, BYTE val);

    // Model selection
    int model=0;
    BYTE rSave[32];

    // This is here because type 1 can report vSync state through status register
    int vSync=0;
};
