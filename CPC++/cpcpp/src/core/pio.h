#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Pio.h - Parallel input output emulation
#include "periph.h"
#include "cpc_base.h"

class Psg;
class Centro;
class Kbd;
class Tape;
class Ga;
struct PioState;

/// parallel IO 8255
class Pio:public Peripheral {
 public:
    Pio(Psg &psg, Centro &centro, Kbd &kbd, Tape &tape, Ga &ga, TrapFlag &trapFlag);
    virtual void Reset();
    virtual void Put(Phase phase, WORD addr, BYTE val);
    virtual BYTE Get(Phase phase, WORD addr);
    void GetState(PioState &state)const;
    void SetState(const PioState &state);
    BYTE GetPortA()const;
    BYTE GetPortB()const;
    BYTE GetPortC()const;
    // options
    void Constructor(CpcBase::CpcConstructor val);
    // not emulated void VideoFreq(int val) {videoFreq=val;}
 private:
    Pio &operator=(const Pio&);
    Pio(const Pio&);

    BYTE ReadPortA()const;
    BYTE ReadPortB()const;
    BYTE ReadPortC()const;

    // options
    CpcBase::CpcConstructor constructor = CpcBase::CpcConstructor::Amstrad;
    int videoFreq=50;

    BYTE regA = 0, regB = 0, regC = 0, regCtrl=0;
    BYTE portA = 0, portB = 0, portC = 0;
    Psg &psg;
    Centro &centro;
    Kbd &kbd;
    Tape &tape;
    Ga &ga;

    // Monitor support
    TrapFlag &trapFlag;
};
