// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file cpcfile.h - Buffer file
#pragma once
#include "cpctypes.h"

/// A file like interface to a memory buffer.
/// this is used to let file based routines work on a memory buffer.
/// Needed mostly when feeding in un-ZIP'ed data.
/// Note that the buffer's size is fixed at construction time.
class CpcFile {
public:
    /// create by allocating a buffer or taking ownership of the given buffer
    explicit CpcFile(int nBytes, BYTE *buff=0);
    /// deletes the buffer
    ~CpcFile();
    void Reset()const;
    void Read(unsigned char *buff, unsigned int nBytes)const;
    void Write(const unsigned char *buff, unsigned int nBytes);
    void Seek(int offset)const;
    const unsigned char *Data()const {return data;}
    int Size()const {return size;}
    int Cursor()const {return cursor;}
private:
    unsigned char * const data;
    const unsigned int size;
    mutable unsigned int cursor;
};
