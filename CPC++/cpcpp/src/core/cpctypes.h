#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file cpctypes.h - Definitions visible from every file
#include "infra/rich_enum.h"
#include <algorithm>

#if defined(_MSC_VER) && (_MSC_VER <= 1600)
typedef unsigned __int8 uint8_t;
typedef signed __int8 int8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
#else
#include <stdint.h> /* POSIX and C99 */
#endif

typedef uint8_t BYTE;
typedef uint8_t UINT8;
typedef int8_t INT8;
typedef uint16_t WORD;
typedef uint32_t QUAD;
typedef uint16_t UINT16;
typedef uint32_t UINT32;
typedef uint8_t RAW8;
typedef uint16_t RAW16;
typedef uint32_t RAW32;
typedef double RAW64;

static inline RAW64 *As64(void *data) { return reinterpret_cast<RAW64 *>(data);  }
static inline const RAW64 *As64(const void *data) { return reinterpret_cast<const RAW64 *>(data); }
static inline RAW32 *As32(void *data) { return reinterpret_cast<RAW32 *>(data);  }
static inline const RAW32 *As32(const void *data) { return reinterpret_cast<const RAW32 *>(data); }

RICH_ENUM(TrapFlag, (OFF)(CPU_RES)(HIRES));

/// A rectangle
class CpcRect {
public:
    CpcRect(): top(0), left(0), bottom(0), right(0) {}
    CpcRect(int t, int l, int b, int r): top(t), left(l), bottom(b), right(r) {}
    void Set(int t, int l, int b, int r) {top=t;left=l;bottom=b;right=r;}
    int Width()const {return right-left+1;}
    int Height()const {return bottom-top+1;}
    int Top()const {return top;}
    int Left()const {return left;}
    int Bottom()const {return bottom;}
    int Right()const {return right;}
private:
    int top,left,bottom,right;
};

/// A device independent 32 bit bitmap.
struct CpcDib32 {
    CpcDib32(RAW32 *pt, unsigned int h, unsigned long bpr): pt(pt), h(h), bpr(bpr) {}
    uint32_t *data(unsigned int row=0)const { ASSERT_THROW(row<h); return pt+row*bpr/4;}
    uint8_t *Pt(unsigned int row = 0) const { return reinterpret_cast<uint8_t *>(data(row)); }

private:
    RAW32 *pt;
    unsigned int h;
    unsigned long bpr;
};
