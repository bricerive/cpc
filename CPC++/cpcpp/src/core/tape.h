#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file tape.h - Tape drive emulation

#include "cpctypes.h"

/// Emulation of the Tape interface - not implemented (I have a 6128)
class Tape {
public:
    Tape() {}
    void Motor(BYTE val);
    void LineIn(BYTE val);
    BYTE LineOut();
};
