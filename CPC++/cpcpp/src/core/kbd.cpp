// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Kbd.cc - Keyboard emulation
#include "kbd.h"
#include "pio.h"
#include "infra/error.h"
#include "infra/log.h"

#include <string.h>

Kbd::Kbd(Pio &pio)
: keyMap(NB_COLS), pio(pio)
{
    Kbd::Reset();
}

void Kbd::Reset()
{
    keyMap = std::vector<BYTE>(NB_COLS, 0xFF);
}

void Kbd::KeyChange(int keyCode, KeyDirection direction)
{
    if (keyCode<NB_KEYS) {
        if (direction == KeyDirection::DOWN)
            keyMap[keyCode/NB_ROWS] &= ~(1<<(keyCode%NB_ROWS));
        else
            keyMap[keyCode/NB_ROWS] |= 1<<(keyCode%NB_ROWS);
    }
}

BYTE Kbd::ReadRow()const
{
    int col = pio.GetPortC()&0x0F;

    // PIO port C0-3 is connected to a 74LS145 BCD decoder.
    // All outputs remain off for all invalid binary input conditions.
    if (col>=NB_COLS) {
        LOG_STATUS("KBD: crtCol = %d",col);
        return 0xFF;
    }
    return keyMap[col];
}

BYTE Kbd::ReadCol()const
{
    return 0x0F;
}
