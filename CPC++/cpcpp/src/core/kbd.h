#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file Kbd.h - Keyboard emulation
#include <vector>
#include "cpctypes.h"
#include "cpc_base.h"

class Pio;
class Psg;

/// Keyboard emulation
class Kbd: public CpcKeyboard {
 public:
    Kbd(Pio &pio);
    void Reset();
    BYTE ReadRow()const;
    BYTE ReadCol()const;

    void KeyChange(int keyCode, KeyDirection direction);
 private:

    static const int NB_KEYS=80;
    static const int NB_COLS=10;
    static const int NB_ROWS=NB_KEYS/NB_COLS;
    std::vector<BYTE> keyMap;
    Pio &pio;
};
