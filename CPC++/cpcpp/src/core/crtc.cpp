// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Crtc.cc - Cathod Ray Tube Controller emulation

// CRTC registers:
//
// R0: Horizontal total         0x3F 63
// R1: Horizontal displayed     0x28 40
// R2: Horizontal sync position 0x2E 46
// R3: Sync widths              0x8E
// R4: Vertical total           0x26 38
// R5: Vertical total adjust    0x00
// R6: Vertical displayed       0x19 25
// R7: Vertical sync position   0x1E 30
// R8: Interlace and skew       0x00
// R9: Maximum raster adress    0x07
// R10:Cursor start raster      0x00
// R11:Cursor end raster        0x00
// R12:Start adress high        0x30
// R13:Start adress low         0x00
// R14:Cursor high              0x00
// R15:Cursor low               0x00
//
#include "cpc_state.h"
#include "infra/error.h"
#include "infra/log.h"
#include "crtc.h"
#include <stdio.h>

// CRTC Types:
// 0 -> HD6845S
// 1 -> UM6845R
// 2 -> MC6845

const unsigned char ReadMasks[3][32]= {
    {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,
     0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,
     0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,
     0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}
};
const unsigned char WriteMasks[3][32]= {
    {0xFF,0xFF,0xFF,0xFF,0x7F,0x1F,0x7F,0x7F,0xF3,0x1F,0x7F,0x1F,0x3F,0xFF,0x3F,0xFF,
     0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    {0xFF,0xFF,0xFF,0x0F,0x7F,0x1F,0x7F,0x7F,0x03,0x1F,0x7F,0x1F,0x3F,0xFF,0x3F,0xFF,
     0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    {0xFF,0xFF,0xFF,0x0F,0x7F,0x1F,0x7F,0x7F,0x03,0x1F,0x7F,0x1F,0x3F,0xFF,0x3F,0xFF,
     0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}
};



Crtc::Crtc(Client &client, TrapFlag &trapFlag)
: client(client), trapFlag(trapFlag)
{
    for (int i=0; i<16; i++) r[i]=0;
}

void Crtc::Reset()
{
    addressRegister = 0;
    for (int i=0; i<16; i++)
        WriteRegister(i,0);
}

const char *Crtc::RegName(int i)
{
    static const char *regName[]={
        "HTotl","HDisp","HSync","Width",
        "VTotl","VAdju","VDisp","VSync",
        "IntSk","Rastr","CuSta","CuEnd",
        "AddHi","AddLo","CurHi","CurLo",
        "LPeHi","LPeLo","Illegal"};
    return regName[std::min(i,18)];
}

void Crtc::Put(Phase phase, WORD addr, BYTE val)
{
    switch (addr&0x0300) {
    case 0x0000: // Write address register
        //if (val&0xF0) LOG_STATUS("CRTC: Adress register = %d !!!", val);
        addressRegister = val&0x1F;
        break;
    case 0x0100:
        // notify cpc in case we are tracing
        client.CrtcRegisterChanged(phase, addressRegister, val);
        WriteRegister(addressRegister, val);
        break;
    default:
        LOG_STATUS("CRTC: Write with A9=1 !!!");
        break;
    }
}

void Crtc::WriteRegister(int index, BYTE val)
{
    rSave[index] = val;
    r[index] = val & WriteMasks[model][index];
    client.CrtcChanged(index); // Notify the client
    if (trapRegisterChanges) { // Notify the monitor
         trapFlag=TrapFlag::CPU_RES;
         trapRegisterChanges=false;
    }
}

BYTE Crtc::Get(Phase /*phase*/, WORD addr)
{
    BYTE val = 0xFF;

    switch (addr & 0x0300) {
    case 0x0200: // Read status register
        if (model==1) { // only type 1 has a status register
            val = 0;
            if (vSync)
                val |= 0x20;
        }
        //LOG_STATUS("CRTC: Status read");
        break;
    case 0x0300: // Read register
        val = r[addressRegister] & ~ReadMasks[model][addressRegister];
        break;
    default:
        LOG_STATUS("CRTC: Read with A9=0 !!!");
        break;
    }
    return val;
}

void Crtc::GetState(CrtcState &state)const
{
    state.addressRegister = addressRegister;
    for (int i=0; i<16; i++)
        state.r[i]=r[i];
}

void Crtc::SetState(const CrtcState &state)
{
    addressRegister = state.addressRegister;
    for (int i=0; i<16; i++)
        r[i] = state.r[i];
    client.CrtcChanged(-1);
}
