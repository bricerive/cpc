// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Pio.cc - Parallel input output emulation
#include "pio.h"
#include "infra/error.h"
#include "infra/log.h"
#include "psg.h"
#include "kbd.h"
#include "tape.h"
#include "centro.h"
#include "ga.h"
#include "cpc_state.h"

// From Intel's spec:
// The 8255A contains three 8-bit ports (A,B, and C).
// All can be configures in a wide variety of functional
// characteristics by the system software but each has
// its own special features or "personality" to further
// enhance the power and flexibility of the 8255A.
//
// Port A. One 8-bit data output latch/buffer and one
// 8-bit data input latch.
//
// Port B. One 8-bit data input/output latch/buffer and
// one 8-bit data input buffer.
//
// Port C. One 8-bit data output latch/buffer and one
// 8-bit data input buffer (no latch for input). This port
// can be divided into two 4-bit ports under the mode
// control. Each 4-bit port contains a 4-bit latch and it
// can be used for the control signal outputs and status
// signal inputs in conjunction with ports A and B.

// Mode 0 Basic Functional Definitions:
//-Two 8-bit ports and two 4-bit ports.
//-Any port can be input or output.
//-Outputs are latched.
//-Inputs are not latched.
//-16 different Input/Output configurations are possible in this mode.

// Note:
// latch means that the data value is updated at clock time.
// buffer means that the data value is updated constantly.

// This is a kludge to let prehistorik work.
// There seems to be a timing error where the interrupt comes
// in at a time where the PIO port A is in Input mode, but
// is expected to be in output mode!!
static const bool PREHISTORIK_KLUDGE = false;

Pio::Pio(Psg &psg, Centro &centro, Kbd &kbd, Tape &tape, Ga &ga, TrapFlag &trapFlag)
: psg(psg), centro(centro), kbd(kbd), tape(tape), ga(ga), trapFlag(trapFlag)
{}

void Pio::Constructor(CpcBase::CpcConstructor val) {constructor=CpcBase::CpcConstructor(val&7);}

// Port A: Input/output
//  PSG data
BYTE Pio::ReadPortA()const
{
    return psg.Get();
}

// Port B: Input
//  Bit0: Crtc VSync
//  Bit1-3: Constructor
//  Bit4: LK4 jumper 0=60Hz 1=50Hz
//  Bit5: EXP from extension connector
//  Bit6: Centronics busy
//  Bit7: Tape line out
BYTE Pio::ReadPortB()const
{
    return static_cast<BYTE>((ga.VSync())|(constructor<<1)|(videoFreq==50?0x10:0x00)|(centro.GetBusy()<<6)|(tape.LineOut()<<7));
}

// Port C: Output
//  Bit1-3: Kbd row
//  Bit4: Tape motor
//  Bit5: Tape line in
//  Bit6-7: PSG BC1/BDIR
BYTE Pio::ReadPortC()const
{
    return (kbd.ReadCol()|0xF0);
}

void Pio::Reset()
{
    regA = regB = regC = regCtrl = 0;
}

void Pio::Put(Phase /*phase*/, WORD addr, BYTE val)
{
    int bit;

    switch (addr & 0x0300) {

        case 0x0000:    // Registre A
            regA = val;
            if ((regCtrl & 0x10) == 0) { // Output mode
                portA = regA;
                psg.Put(portA);
            } else { // Input mode
                LOG_STATUS("[Pio] Port A write in input mode");
                if constexpr(PREHISTORIK_KLUDGE) {
                    // This is a kludge to let prehistorik work.
                    // There seems to be a timing error where the interrupt comes
                    // in at a time where the PIO port A is in Input mode, but
                    // is expected to be in output mode!!
                    portA = regA;
                    psg.Put(portA);
                }
            }
            break;

        case 0x0100:    // Registre B
            regB = val;
            if ((regCtrl & 0x02) == 0) // Output mode
                portB = regB;
            else // Input mode
                LOG_STATUS("[Pio] Port B write in input mode");
            break;

        case 0x0200:    // Registre C
            regC = val;
            if ((regCtrl & 0x01) == 0) // lower half output
                portC = (portC&0xF0) | (regC&0x0F);
            else
                LOG_STATUS("[Pio] Port C write in lower input mode");
            if ((regCtrl & 0x08) == 0) { // upper half ouput
                portC = (portC&0x0F) | (regC&0xF0);
                psg.Ctrl(portC);
            }else
                LOG_STATUS("[Pio] Port C write in upper input mode");
            break;

        case 0x0300:    // Registre de controle
            switch (val & 0x80) {
                case 0x80: // Set operating mode
                    regCtrl = val&0x7F;
                    regA = regB = regC = 0;
                    if (regCtrl&0x04)
                        LOG_STATUS("[Pio} Port B in Mode 1");
                    if (regCtrl&0x60)
                        LOG_STATUS("[Pio} Port A in Mode %d",(regCtrl>>5));
                    break;
                case 0x00: // C register bit control
                    bit = (val>>1) & 7;
                    if (val&1) // Set bit
                        regC |= 1<<bit;
                    else // Reset bit
                        regC &= ~(1<<bit);
                    if ((regCtrl & 0x01) == 0) // lower half output
                        portC = (portC&0xF0) | (regC&0x0F);
                    else
                        LOG_STATUS("[Pio] Port C write in lower input mode");
                    if ((regCtrl & 0x08) == 0) { // upper half ouput
                        portC = (portC&0x0F) | (regC&0xF0);
                        psg.Ctrl(portC);
                    }else
                        LOG_STATUS("[Pio] Port C write in upper input mode");
                    break;
                default:
                    ERR_THROW("Internal error");
            }
            break;
        default:
            ERR_THROW("Internal error");
    }
}

BYTE Pio::Get(Phase /*phase*/, WORD addr)
{
    BYTE val=0xFF;

    switch (addr & 0x0300) {
        case 0x0000:    // Registre A
            if (regCtrl & 0x10) // Input mode
                regA = portA = ReadPortA();
            else { // Output mode
                LOG_STATUS("[Pio] Port A read in output mode");
            }
            val = regA;
            break;
        case 0x0100:    // Registre B
            if (regCtrl & 0x02) // Input mode
                regB = portB = static_cast<BYTE>(ReadPortB());
            else {
                //LOG_STATUS("[Pio] Port B read in output mode");
            }
            val = regB;
            break;
        case 0x0200:    // Registre C
            if (regCtrl & 0x01) { // Input mode lower
                LOG_STATUS("[Pio] reading from port C low bits");
                portC = (portC&0xF0) | (ReadPortC()&0x0F);
                regC = (regC&0xF0) | (portC&0x0F);
            }
            if (regCtrl & 0x08) { // Input mode upper
                LOG_STATUS("[Pio] reading from port C high bits");
                portC = (portC&0x0F) | (ReadPortC()&0xF0);
                regC = (regC&0x0F) | (portC&0xF0);
            }
            val = regC;
            break;
        case 0x0300:    // Registre de controle
            LOG_STATUS("[Pio] Reading control register");
            val = regCtrl;
            break;
        default:
            ERR_THROW("Internal error");
    }
    return val;
}

void Pio::GetState(PioState &state)const
{
    state.portA = portA;
    state.portB = portB;
    state.portC = portC;
    state.regCtrl = regCtrl;
}

void Pio::SetState(const PioState &state)
{
    Put(0,0x0300,state.regCtrl|0x80);
    Put(0,0x0000,state.portA);
    Put(0,0x0100,state.portB);
    Put(0,0x0200,state.portC);
}

BYTE Pio::GetPortA()const
{
    return portA;
}

BYTE Pio::GetPortB()const
{
    return portB;
}

BYTE Pio::GetPortC()const
{
    return portC;
}
