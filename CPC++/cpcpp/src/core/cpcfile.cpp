// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// CpcFile.cpp - Buffer file
#include "infra/error.h"
#include "infra/log.h"
#include "cpcfile.h"
#include <string.h>

CpcFile::CpcFile(int nBytes, BYTE *buff)
: data(buff? buff: new unsigned char[nBytes]), size(nBytes), cursor(0)
{
}

CpcFile::~CpcFile()
{
    delete[] data;
}

void CpcFile::Reset()const
{
    cursor=0;
}

void CpcFile::Write(const BYTE *buff, unsigned int nBytes)
{
    ASSERT_THROW(cursor+nBytes<=size)("Write out of bounds");
    memcpy(data+cursor,buff,nBytes);
    cursor+=nBytes;
}

void CpcFile::Read(BYTE *buff, unsigned int nBytes)const
{
    ASSERT_THROW(cursor+nBytes<=size)("FRead failed...Disk Image Could be dammaged.");
    memcpy(buff,data+cursor,nBytes);
    cursor+=nBytes;
}

void CpcFile::Seek(int offset)const
{
    if (cursor+offset>size)
        LOG_STATUS("Seek beyond file end: %d+%d>%d",cursor,offset,size);
    cursor+=offset;
}
