// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Cpc.cc - Main emulator class
#include "cpc.h"

#include <stdio.h>
#include <string.h>

#include "cpc_state.h"
#include "cpcconsole.h"
#include "cpcframe.h"
#include "drive.h"
#include "infra/error.h"

// Notes:
//
// The Demo-part 1
//      13FC    Call 1978
//  ->  1987    Out  a,(c)  -> Sets Max Raster to 7
// this happens before hEnd so sReset (sCount==crtc.VScan()) will not be true at hEnd
// That proves that:
//  -vReset is triggered and latched at vTot+sReset
//  -vReset will force sCount to 0
//
//-The living Daylights
// hTot=3F, hDisp=20, hSync=2A, Width=8E

/// controls whether tracing (activated by Monitor) also traces CRTC actions
static const bool CPC_TRACE = true;

// when is the maReg reloaded from CRTC registers
// 0: at hCount reset
// 1: at hCount==vTot  -> cleans up dirty line in Hi-Res demo
static const bool MAREG_LOAD_MODE = true;

Cpc::Cpc(CpcConsole &console)
    : IoSpace(crtc, pio, centro, fdc, ga)
    , crtc(*this, trapFlag)
    , pio(psg, centro, kbd, tape, ga, trapFlag)
    , centro(console)
    , drives({std::make_unique<Drive>(), std::make_unique<Drive>(), 0, 0})
    , fdc(drives, console, trapFlag)
    , ga(*this, cpu, crtc, trapFlag)
    , kbd(pio)
    , psg(kbd, centro, console, trapFlag)
    , cpu(ga, *this, trapFlag)
    , console(console)
{
}

Cpc::~Cpc() {
    if (crtcOut) fclose(crtcOut);
}

void Cpc::TrapCondition(int n) {
    if (trapActive == n) {
        trapFlag = TrapFlag::HIRES;
        trapActive = MonitoredCpc::TrapId::NONE;
    }
}

BYTE Cpc::MemRd8(WORD addr) const { return ga.MemRd8(addr); }
BYTE Cpc::MemRdWr8(WORD addr) const { return ga.MemRdWr8(addr); }
UINT16 Cpc::MemRd16(WORD addr) const { return ga.MemRd16(addr); }

void Cpc::ResetInner() {
    hDelay = 0;
    hCount = crtcHSync = hSyncCnt = 0;
    hSyncShort = 0;
    vAdj = vAdjCnt = vAdjReset = 0;
    vSyncBlank = crtcVSync = vSyncCnt = vSyncReset = 0;
    vCount = 0;
    vReset = sReset = hReset = 0;
    sCount = dispEn = r8lock = 0;
    memAdd = maReg = 0;
    vduCol = 0;
    vduRow = 0;
    vduHSync = vduVSync = false;
    vduVSyncCnt = 0;
    gaHSync = 0;
    resetFlag = false;
}

void Cpc::CrtcTrace(Phase crtPhase, const char *eventStr) const {
    if constexpr(CPC_TRACE) {
        if (!tracing) return;
        unsigned long delta = crtPhase.ElapsedUs(lastCrtcPhase);
        int lines = delta / (crtc.HTot() + 1);
        int cLines = lines / (crtc.VScan() + 1);
        int chars = delta - lines * (crtc.HTot() + 1);
        char str[1024];
        snprintf(str, 1024,"%08ld nops=%04ld(%04d/%04d/%04d)  LC=%03d RC=%03d HC=%03d %s\n", crtPhase.PhaseUs(), delta, lines, cLines, chars, vCount, sCount, hCount, eventStr);
        if (!crtcOut) crtcOut = fopen("CrtcTrace.txt", "w");
        fwrite(str, strlen(str), 1, crtcOut);
        lastCrtcPhase = crtPhase;
    }
}

void Cpc::CrtcRegisterChanged(Phase crtPhase, int ar, int val) const {
    if constexpr(CPC_TRACE) {
        if (!tracing) return;
        char str[128];
        snprintf(str, 128,"RChange:%02d=%03d", ar, val);
        CrtcTrace(crtPhase, str);
    }
}

void Cpc::CrtcChanged(int ar) { crtcChanges |= 1 << ar; }

void Cpc::SetWatch(WORD addr) {
    trapActive = MonitoredCpc::TrapId::WATCH;
    watchAddr = addr;
    watchVal = MemRdWr8(addr);
}

Cpc::RunState Cpc::RunSome(bool render)
 {
    bool trapped=false;
    do {
        if (loopDepth==0) { 
            if (resetFlag) ResetInner();
            if (render) ga.FrameStart(cpcPhase);
            loopDepth++;
        }
        if (loopDepth==1) {
            vduVSyncFront = false;
            hScroll = false;
            if (render) {
                lineChanged = false;
                ga.LineStart(vduRow + Ga::vduVSyncVal, cpcPhase, renderPt32);
            }
            loopDepth++;
        }

        // Inner loop on chars 1us
        if (cpcPhase >= cpuPhase) CpuStep(cpcPhase);
        if (cpcPhase >= psgPhase) psgPhase += psg.Step(cpcPhase);  // Step the PSG
        VduStep(cpcPhase, render);
        TrapCondition(MonitoredCpc::TrapId::TICK);
        if (trapFlag && (trapFlag == TrapFlag::HIRES || cpuPhase.ElapsedUs(cpcPhase) == 1)) {
            console.MonitorTrap();  // Notify that a trap triggered
            trapped = true;
            trapFlag = TrapFlag::OFF;    // reset the flag
        }
        cpcPhase += CpcDuration::FromUs(1);
        if (vduHSyncFront)
            loopDepth--;

        if (loopDepth==1) {
            vduCol = -Ga::vduHSyncVal * 2;  // VDU HSync
            // if (crtc.HSync()==0x31) vduCol-=2;
            if (render)
                ga.LineDone(vduRow + Ga::vduVSyncVal, cpcPhase - CpcDuration::FromUs(1), renderPt32 - 1, lineChanged, hScroll);
            vduRow++;  // Move VDU beam down

            if (vduRow >= Ga::vduVTot + Ga::vduVOverload) {  // CRT V capacitor overload
                // if (vduRow>=vduVTot) { <- breaks GPA Vision demo
                vduVSync = true;
                vduVSyncFront = true;
            }
            if (vduVSyncFront)
                loopDepth--;
        }
        if (loopDepth==0) { 
            if (render) ga.FrameDone(vduRow + Ga::vduVSyncVal - 1);
            vduRow = -Ga::vduVSyncVal;  // VDU Vsync
        }
    } while (loopDepth && !trapped);
    return RunState(vduVSyncFront, cpcPhase);
}

void Cpc::VduStep(const Phase &crtPhase, bool render) {

    vduHSyncFront = false;
    hCount = hReset ? 0 : ((hCount + 1) & 0xFF);

    if (hCount == hStart) {  // Start of visible
        dispEn = dispEn | 1;
    }
    if (hCount == hEnd) {  // End of visible
        dispEn = dispEn & ~1;
        if (sReset && !hReset) {
            maReg = memAdd;  // Push screen address at last raster
        }
    }
    if (hReset) {  // Horizontal reset
        hReset = 0;
        TrapCondition(MonitoredCpc::TrapId::HRESET);

        if constexpr(!MAREG_LOAD_MODE)
            memAdd = maReg;  // multiplexed-pop memory address
        sCount = (sCount + 1) & 0x1F;  // increment raster count

        if (vSyncBlank) vSyncBlank = vSyncBlank - 1;

        if (crtcVSync) {
            vSyncCnt++;
            vSyncReset = (vSyncCnt == crtc.VWidth());
            if (vSyncReset) {
                crtcVSync = vSyncCnt = 0;
                CrtcTrace(crtPhase, "vSync OFF");
                ga.VSyncOff();
                crtc.SetVSync(0);  // Tell the CRTC
            }
        }

        if (sReset) {  // raster count reset
            sCount = 0;
            vCount = (vCount + 1) & 0x7F;
            TrapCondition(MonitoredCpc::TrapId::SRESET);
            // to avoid multiple vSyncs on the same vCount, vSyncReset stays up for the whole char
            vSyncReset = 0;
        }

        if (vReset) {  // Vertical count reset
            vReset = 0;
            vAdjCnt = 0;
            vAdj = 1;
        }
        if (vAdj) {  // Vertical adjust
            if (vAdjCnt == crtc.VAdj()) {
                vAdj = 0;
                vAdjCnt = 0;
                if constexpr(MAREG_LOAD_MODE)
                    maReg = prevMemAdd;
                else
                    maReg = memAdd = crtc.MemAdd();
                sCount = 0;
                vCount = 0;
                dispEn = dispEn | 2;
                CrtcTrace(crtPhase, "vReset");
                TrapCondition(MonitoredCpc::TrapId::VRESET);
            } else
                vAdjCnt = (vAdjCnt + 1) & 0x1F;
        }

        // \/ Breaks TheDemo-Lobotomia
        // if (vCount==crtc.VDisp() || vCount==crtc.VSync())
        if (vCount == crtc.VDisp()) {  // End of visible
            dispEn = dispEn & ~2;
        }

        // on types other than 2, VSync can start on any scan
        // V93 demo - Oh No!
        if (vCount == crtc.VSync() && vSyncReset == 0 && crtcVSync == 0 && crtc.VWidth()) {  // Start of crtc VSync
            vSyncBlank = 25;
            crtcVSync = 1;
            CrtcTrace(crtPhase, "vSync ON");
            TrapCondition(MonitoredCpc::TrapId::VSYNC);
            ga.VSyncOn();      // Tell the gate array
            crtc.SetVSync(1);  // Tell the CRTC
        }

        sReset = sCount == crtc.VScan();  // raster count reset flag

        // vReset = vCount==crtc.VTot();
        // if (vCount==crtc.VTot()) vReset=1;
        // if (sReset && vCount==crtc.VTot()) vReset=1;
        if (sReset) {
            vReset = vCount == crtc.VTot();  // vertical count reset flag
        }

        if (vReset) CrtcTrace(crtPhase, "vTot");

        if constexpr(MAREG_LOAD_MODE)
            memAdd = maReg;  // multiplexed-pop memory address
    }

    // HSync emulation
    // _______________
    // There are three hSync signals in chain:
    // crtcHSync: generated by the CRTC from internal registers
    //  starts when hCount=crtc.HSync
    //  ends when hSyncCnt==crtc.HWidth
    // gaHSync: generated by the GA from the crtcHSync
    //  generates black if it occurs in visible area
    //
    // vduHSync: actual VDU hsync from the gaHSync
    //  brings back the beam to the left of the next line
    //  does not trigger if the gaHSync occurs too early in the scan
    //  does not trigger if the gaHSync is too short
    //  is auto-generated (capacitor overload) if the beam goes too far to the right
    if (hCount == crtc.HSync() && hSyncCnt != crtc.HWidth()) {  // Horizontal sync
        crtcHSync = 1;                                          // start HSync width counter
        hSyncShort = 1;                                         // short 'till proven
        gaHSync = 1;                                            // start black
        TrapCondition(MonitoredCpc::TrapId::HSYNC);
    }
    if (crtcHSync) {
        if (hSyncCnt == crtc.HWidth()) {
            bool intReq;
            int hCnt;
            ga.HSyncOff(crtPhase, intReq, hCnt);
            // char str[128];
            // sprintf(str,"hCnt=%02d%s",hCnt,intReq?"->intReq":"");
            // CrtcTrace(crtPhase,str);
            if (!vduVSync && crtcVSync != 0) {
                // The VDU VSync will not trigger if it happens too early in the scan
                // (cf. Energy-103A: >123)
                // if (vduRow>124)
                if (vduRow > 272 - Ga::vduVSyncVal) {
                    vduVSyncFront = true;
                    vduVSync = true;
                }
                vduVSyncCnt = 0;
            }
            if (vduVSync) {
                if (++vduVSyncCnt == 25) {
                    vduVSyncCnt = 0;
                    vduVSync = false;
                }
            }
            hSyncCnt = 0;
            crtcHSync = 0;
            gaHSync = 0;
            // HScroll: move screen one half char to the right (only true if the vdu actualy triggered)
            if (vduHSync && hSyncShort) hScroll = true;
            vduHSync = false;
        } else {
            hSyncCnt = (hSyncCnt + 1) & 0x0F;
            if (hSyncCnt == 1) {
                gaHSync = 1;
            }
            if (hSyncCnt == 4 && vduCol > 100) {  // Minimum length for VDU HSync + not too early (cf. S&KoH)
                if (!vduHSync) {
                    vduHSync = true;  // HSync VDU beam
                    vduHSyncFront = true;
                }
            }
            if (hSyncCnt == 6) {  // Maximum length of GA Hsync
                hSyncShort = 0;   //  (Skateball: HSyncW=5)
                vduHSync = false;
                gaHSync = 0;
            }
        }
    }
    // if (vduCol/2>=vduHTot+150) {
    if (vduCol >= (Ga::vduHTot + Ga::vduHOverload) * 2) {
        //if (!vduHSync) {
            vduHSync = true;  // CRT H capacitor overload
            vduHSyncFront = true;
        //}
    }

    if (hCount == crtc.HTot()) {  // End of scan
        hReset = 1;
        if constexpr(MAREG_LOAD_MODE)
            prevMemAdd = crtc.MemAdd();
    }

    if (render) {
        RAW32 renderData;
        // Output render data
        if (vSyncBlank) {
            renderData = CpcFrame::FlagVSync;
        } else if (gaHSync) {
            renderData = CpcFrame::FlagHSync;
        } else if (dispEn == 3 && r8lock == 0) {  // Display
            const WORD *vRam = reinterpret_cast<const WORD *>(ga.VideoRam((memAdd & 0x3000) >> 12) + (((sCount & 0x7) << 11) | ((memAdd & 0x3FF) << 1)));
            WORD val2 = *vRam;
            renderData = val2;
        } else {  // border
            renderData = CpcFrame::FlagBorder;
        }
        if (*renderPt32 != renderData) {
            lineChanged = true;
            *renderPt32 = renderData;
        }
        renderPt32++;
    }
    memAdd++;

    vduCol += 2;  // Move VDU beam right
    if (trapActive == MonitoredCpc::TrapId::VDUPOS && vduRow == lastClickY && vduCol / 2 == lastClickX) TrapCondition(MonitoredCpc::TrapId::VDUPOS);
}

void Cpc::CpuStep(Phase &crtPhase) {
    cpuPhase = cpu.Step(crtPhase);  // Step the Z80

    // Look if any CRTC register changed
    // either it changed during previous Z80 step, which is correct,
    // or it changed outside of here and it does not matter that we deal
    // with it a little too late
    if (crtcChanges) {
        if (crtcChanges & 0x001) {  // HTot
        }
        if (crtcChanges & 0x002) {  // HDisp
            hStart = hDelay;
            hEnd = crtc.HDisp() + hDelay;
            if (crtc.HDisp() == 0) {
                hStart = 0;
                hEnd = 0;
            }
        }
        if (crtcChanges & 0x004) {  // HSync
        }
        if (crtcChanges & 0x008) {  // Sync Width
        }
        if (crtcChanges & 0x010) {  // VTot
        }
        if (crtcChanges & 0x020) {  // VAdj
        }
        if (crtcChanges & 0x040) {  // VDisp
            if (vCount == crtc.VDisp()) dispEn = dispEn & ~2;
        }
        if (crtcChanges & 0x080) {  // VSync
        }
        if (crtcChanges & 0x100) {  // Interlace and skew
            hDelay = crtc.Skew();
            r8lock = 0;
            if (hDelay == 3) {
                r8lock = 1;
                hDelay = 0;
            }
            hStart = hDelay;
            hEnd = crtc.HDisp() + hDelay;
            if (crtc.HDisp() == 0) {
                hStart = 0;
                hEnd = 0;
            }
        }
        if (crtcChanges & 0x200) {  // Max Raster
            // if (sCount==crtc.VScan() && hCount<=(crtc.HTot()<<1)) sReset=1;
            sReset = sCount == crtc.VScan() && hCount <= static_cast<unsigned int>(crtc.HTot() << 1);
        }
        crtcChanges = 0;
    }
    if (trapActive == MonitoredCpc::TrapId::WATCH && watchVal != ga.MemRdWr8(watchAddr)) {
        trapFlag = TrapFlag::CPU_RES;
        watchVal = ga.MemRdWr8(watchAddr);
    }
}

void Cpc::GetVduState(VduState &state) const {
    state.hCount = hCount;
    state.hReset = hReset;
    state.crtcHSync = crtcHSync;
    state.hSyncCnt = hSyncCnt;
    state.sCount = sCount;
    state.sReset = sReset;
    state.vCount = vCount;
    state.vReset = vReset;
    state.vAdj = vAdj;
    state.vAdj = vAdjCnt;
    state.crtcVSync = crtcVSync;
    state.vSyncCnt = vSyncCnt;
    state.memAdd = memAdd - 1;
    state.maReg = maReg;
    state.dispEn = dispEn;
    state.vduRow = vduRow;
    state.vduCol = vduCol / 2;
}

void Cpc::SetVduState(const VduState &state) const {}

void Cpc::GetConfigState(ConfigState &state) const { state.model = ga.CpcModel(); }

void Cpc::SetConfigState(const ConfigState &state) { CpcModel(state.model); }

void Cpc::GetState(CpcState &state) const {
    cpu.GetState(state.cpu);
    ga.GetState(state.ga);
    crtc.GetState(state.crtc);
    pio.GetState(state.pio);
    psg.GetState(state.psg);
    fdc.GetState(state.fdc);
    GetVduState(state.vdu);
    GetConfigState(state.config);
}

void Cpc::SetState(const CpcState &state) {
    SetConfigState(state.config);
    cpu.Reset();
    ga.Reset();
    crtc.Reset();
    pio.Reset();
    psg.Reset();
    centro.Reset();
    kbd.Reset();
    fdc.Reset();
    cpu.SetState(state.cpu);
    ga.SetState(state.ga);
    crtc.SetState(state.crtc);
    pio.SetState(state.pio);
    psg.SetState(state.psg);
    fdc.SetState(state.fdc);
    SetVduState(state.vdu);
}

// Reset peripherals
// Note that extension peripherals are reset by Out #F8FF
void Cpc::Reset() {
    cpu.Reset();
    ga.Reset();
    crtc.Reset();
    pio.Reset();
    psg.Reset();
    centro.Reset();
    kbd.Reset();
    fdc.Reset();
    resetFlag = true;
}

// drives options
void Cpc::LoadDisk(DriveId drive, const CpcFile &file, bool protect) const { drives[drive]->LoadDisk(file, protect); }

void Cpc::GetDir(DriveId drive, std::vector<std::string> &dir, bool &canCpm) const { drives[drive]->GetDir(dir, canCpm); }

void Cpc::SaveDisk(DriveId drive, CpcFile &file) const { drives[drive]->SaveDisk(file); }

void Cpc::FlipDisk(DriveId drive) const { drives[drive]->FlipDisk(); }

void Cpc::EjectDisk(DriveId drive) const { drives[drive]->EjectDisk(); }

bool Cpc::DriveEnabled(DriveId drive) const { return drives[drive] ? 1 : 0; }

bool Cpc::DoubleSidedFloppy(DriveId drive) const { return drives[drive]->DoubleSidedFloppy(); }

bool Cpc::IsWriteProtected(DriveId drive) const { return drives[drive]->IsProtected(); }

bool Cpc::IsDriveReady(DriveId drive) const { return drives[drive]->IsFull(); }

// Ga options
void Cpc::SetRoms(const BYTE *const low, const BYTE *const roms[256]) { ga.SetRoms(low, roms); }

void Cpc::CpcModel(CpcBase::CpcModelType val) { ga.CpcModel(val); }

void Cpc::CpmBoot(bool val) { ga.CpmBoot(val); }

void Cpc::RenderFrame(const CpcBase::RenderInfo &info, bool force) { ga.RenderFrame(info, force); }

// Fdc options
void Cpc::RealTime(bool val) { fdc.RealTime(val); }

// Centro options
void Cpc::PrinterOutput(CpcBase::PrinterMode val) {
    centro.PrinterOutput(val);
    psg.Digiblaster(val == PrinterMode::PrinterDigiblaster);
}

// Pio options
void Cpc::Constructor(CpcConstructor val) { pio.Constructor(val); }

void Cpc::AddConditionCpuStep() { cpu.AddConditionStep(); }
void Cpc::AddConditionCpuBreak(UINT16 address, bool persist) { cpu.AddConditionBreak(address, persist); }
void Cpc::AddConditionCpuStack(UINT16 address) { cpu.AddConditionStack(address); }
void Cpc::AddConditionCpuIntAck() { cpu.AddConditionIntAck(); }
void Cpc::RemoveCpuCondition() { cpu.RemoveCondition(); }
void Cpc::TrapCrtcChanges() { crtc.TrapRegisterChanges(1); }

void Cpc::CpuTrace(bool on) { cpu.TraceFlag(on); }
void Cpc::CrtcTrace(bool on) { tracing = on; }

const Mmu &Cpc::GetMmu() { return ga; }
void Cpc::DropOn(bool val) { psg.DropOn(val); }
void Cpc::SetSoundInfo(const CpcBase::SoundInfo &sndInfo) { psg.SetSoundInfo(sndInfo); }
void Cpc::SoundOnOff(bool on) { psg.SoundOnOff(on); }
void Cpc::Stereo(bool val) { psg.Stereo(val); }
void Cpc::ChannelsOnOff(int a, int b, int c) { psg.ChannelsOnOff(a, b, c); }

void Cpc::KeyChange(int keyCode, KeyDirection direction) { kbd.KeyChange(keyCode, direction); }
void Cpc::LastClick(int x, int y) {
    lastClickX = x;
    lastClickY = y;
}
void Cpc::SetTrap(TrapId trapId) { trapActive = trapId; }
int Cpc::LastClickX() const { return lastClickX; }
int Cpc::LastClickY() const { return lastClickY; }
