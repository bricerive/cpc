#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file z80.h - Z80 emulation

#include "packed_quad.h"
#include "cpcphase.h"
#include "cpctypes.h"
#include "cpc_base.h"
#include "rich_bitfield.h"
#include "disass.h"
#include <stdio.h> // For disassemble file
#include <vector>
class Mmu;
class IoSpace;
struct CpuState;

static const bool Z80_TRACE=true;

// Monitoring
class CpuCondition;

/// Emulation of the Z80 CPU.
class Z80 {
friend class CpuCondition;
 public:
    Z80(Mmu &mmu, IoSpace &ioSpace, TrapFlag &trapFlag);
    virtual ~Z80();
    void Reset();

    Phase Step(Phase crtPhase);

    void GetState(CpuState &state) const;
    void SetState(const CpuState &state);

    // Signals
    void Int(int val);
    void Nmi(int val);

    // Trace control
    void TraceFlag(bool val) {traceFlag=val;}

    void AddConditionStep();
    void AddConditionBreak(UINT16 address, bool persist);
    void AddConditionStack(UINT16 address);
    void AddConditionIntAck();
    void RemoveCondition();

    // F register definition
    enum {
        S_M=0x80,
        Z_M=0x40,
        H_M =0x10,
        PV_M=0x04,
        N_M =0x02,
        C_M =0x01,
        X_M =0x28
    };

private:
    Z80 &operator=(const Z80&);
    Z80(const Z80&);

    void DoTrace(Phase phase)const;
    void Iff1(int val);
    void Iff2(int val);
    void Im(int val);
    void Halt(int val);

    int Int()const;
    int Nmi()const;
    int Iff1()const;
    int Iff2()const;
    int Im()const;
    int Halt()const;

    BYTE MemGet(WORD a)const;
    char GetOff(WORD a)const;
    WORD MemGetW(WORD a)const;
    void MemPut(WORD a, BYTE v);
    BYTE *MemPutP(WORD a);
    void MemPutW(WORD a, WORD W);

    BYTE GetIo(const Phase &phase, WORD addr);
    void PutIo(const Phase &phase, WORD addr, BYTE val);

    static void MakeFlagTables();
    static BYTE incFlagsTable[256];
    static BYTE decFlagsTable[256];
    static BYTE orFlagsTable[256];
    static BYTE andFlagsTable[256];
    static BYTE parTable[256];
    static BYTE carryTable[8];
    static BYTE hCarryTable[8];
    static BYTE overflowTable[8];
    static BYTE carryTableSub[8];
    static BYTE hCarryTableSub[8];
    static BYTE overflowTableSub[8];
    static BYTE signTable[8];

    BYTE &A=pcaf.r.r3;
    BYTE &F=pcaf.r.r4;
    BYTE &B=spbc.r.r3;
    BYTE &C=spbc.r.r4;
    BYTE &H=hlde.r.r1;
    BYTE &L=hlde.r.r2;
    BYTE &D=hlde.r.r3;
    BYTE &E=hlde.r.r4;
    BYTE &HX=ixiy.r.r1;
    BYTE &LX=ixiy.r.r2;
    BYTE &HY=ixiy.r.r3;
    BYTE &LY=ixiy.r.r4;

    WORD &AF=pcaf.rr.rr2;
    WORD &BC=spbc.rr.rr2;
    WORD &HL=hlde.rr.rr1;
    WORD &DE=hlde.rr.rr2;
    WORD &AF2=afbc2.rr.rr1;
    WORD &BC2=afbc2.rr.rr2;
    WORD &HL2=hlde2.rr.rr1;
    WORD &DE2=hlde2.rr.rr2;
    WORD &IX=ixiy.rr.rr1;
    WORD &IY=ixiy.rr.rr2;
    WORD &SP=spbc.rr.rr1;
    WORD &PC=pcaf.rr.rr1;

    BYTE &I=irfc.r.r1;
    BYTE &R=irfc.r.r2;
    BYTE &FLG=irfc.r.r3; // iff1 | iff2 | im | halt
    BYTE &CHK=irfc.r.r4; // io | nmi | int | ei

    void SplitR() {regR=R; rMsb=R&0x80;}
    void MergeR() {R=static_cast<BYTE>(regR); R&=0x7F; R|=rMsb;}

    enum {
        FLG_IFF1    =0x08,
        FLG_IFF2    =0x04,
        FLG_IM      =0x03,
        FLG_HALT    =0x10,
    };
    enum {
        CHK_EI      =0x01,
        CHK_INT     =0x02,
        CHK_NMI     =0x04,
        CHK_OI_OUT  =0x08,
        CHK_OI_IN   =0x10
    };

    // F register handling
    BYTE F_S ()const { return F&S_M; }
    BYTE F_Z ()const { return F&Z_M; }
    BYTE F_X ()const { return F&X_M; }
    BYTE F_H ()const { return F&H_M; }
    BYTE F_PV()const { return F&PV_M; }
    BYTE F_N ()const { return F&N_M; }
    BYTE F_C ()const { return F&C_M; }

    void UHF(int i) { F=(F&~H_M)|hCarryTable[i&7]; }
    void UHFS(int i) { F=(F&~H_M)|hCarryTableSub[i&7]; }
    void SHF() { F=(F|H_M); }
    void RHF() { F=(F&~H_M); }

    void UPF(BYTE b) { F=(F&~PV_M)|parTable[b]; }
    void UVF(int i) { F=(F&~PV_M)|overflowTable[i>>4]; }
    void UVFS(int i){ F=(F&~PV_M)|overflowTableSub[i>>4]; }

    void USF(BYTE b) { F=(F&~S_M)|((b)&S_M); UXF(b); }
    void USF2(int i) { F=(F&~S_M)|signTable[i>>4]; }

    void UXF(BYTE b) { F=(F&~X_M)|((b)&X_M); }
    
    void UCF2(int i) { F=(F&~C_M)|carryTable[i>>4]; }
    void UCF2S(int i) { F=(F&~C_M)|carryTableSub[i>>4]; }
    void UZF(WORD b) { F=(F&~Z_M)|((b)?0:Z_M); }
    void SNF() { F=(F|N_M); }
    void RNF() { F=(F&~N_M); }

    int MakeIndex(BYTE s, BYTE a, BYTE r) { return ((s&0x88)>>1)|((a&0x88)>>2)|((r&0x88)>>3); }
    void addFlags(BYTE s, BYTE a, BYTE r) { int gIndex=MakeIndex(s,a,r);RNF();UZF(r);UHF(gIndex);UCF2(gIndex);UVF(gIndex);USF2(gIndex);UXF(r);}
    void subFlags(BYTE s, BYTE a, BYTE r) { int gIndex=MakeIndex(s,a,r);SNF();UZF(r);UHFS(gIndex);UCF2S(gIndex);UVFS(gIndex);USF2(gIndex);UXF(r);}
    void cpFlags(BYTE s, BYTE a, BYTE r) { int gIndex=MakeIndex(s,a,r);SNF();UZF(r);UHFS(gIndex);UCF2S(gIndex);UVFS(gIndex);USF2(gIndex);UXF(a);}

    int MakeIndex16(WORD s, WORD a, WORD r) { return ((s&0x8800)>>9)|((a&0x8800)>>10)|((r&0x8800)>>11); }
    void add16Flags(WORD s, WORD a, WORD r) { int gIndex=MakeIndex16(s,a,r);UHF(gIndex);UCF2(gIndex);RNF();UXF(r>>8); }
    void adc16Flags(WORD s, WORD a, WORD r) { int gIndex=MakeIndex16(s,a,r);UZF(r);RNF();UHF(gIndex);UCF2(gIndex);UVF(gIndex);USF2(gIndex);;UXF(r>>8); }
    void sbc16Flags(WORD s, WORD a, WORD r) { int gIndex=MakeIndex16(s,a,r);UZF(r);UHFS(gIndex);UCF2S(gIndex);UVFS(gIndex);USF2(gIndex);SNF();UXF(r>>8); }

    void bitFlags(BYTE b, BYTE m) {UZF(b&m);RNF();SHF();F=(F&~PV_M)|(F_Z()?PV_M:0);F=(F&~S_M)| ((m==0x80)?b&S_M:0);UXF(b); }

    void bitFlags2(BYTE b) {F = ( b ? 0 : (Z_M | PV_M)) | (b & S_M) | (memRegH & X_M) | H_M | (F & C_M);}
    void bitFlags3(BYTE b) { F = (b ? 0 : (Z_M | PV_M)) | (b & S_M) | (memRegH & X_M) | H_M | (F & C_M); }

    void UCF(BYTE c) {( F=(F&~C_M)|(c?C_M:0) );}
    void uupurr(BYTE b, BYTE c) {UCF(c);UZF(b);UPF(b);USF(b);RNF();RHF();}
    void iuxxsx(BYTE w) {UZF(w);SNF();}

    void incFlags(BYTE b) {( F=(F&C_M)|incFlagsTable[b] );}
    void decFlags(BYTE b) {( F=(F&C_M)|decFlagsTable[b] );}
    void orFlags(BYTE b) {( F=orFlagsTable[b] );}
    void andFlags(BYTE b) {( F=andFlagsTable[b] );}
    void ldirFlags(WORD w, BYTE b) { (F = (F & ~(PV_M | N_M | H_M | X_M)) | ((w) ? PV_M : 0) | (b & 0x08) | ((b & 2) << 4)); }
    void scfFlags() {( F=((F&~(N_M|H_M))| C_M) );UXF(A);}
    void cplFlags(BYTE b) {( F=F|N_M|H_M );UXF(b);}
    void rotaFlags(BYTE c) {( F=(F&~(C_M|N_M|H_M|X_M))|(c?C_M:0)|(A&X_M) );}
    void ldairFlags(BYTE b) {( F=(F&~(Z_M|PV_M|S_M|N_M|H_M|X_M))|(b?0:Z_M)|(Iff1()?PV_M:0)|(b&S_M)|(b&X_M) );}
    void inFlags(BYTE b) {( F=(F&~(Z_M|PV_M|S_M|N_M|H_M|X_M))|(b?0:Z_M)|parTable[b]|(b&S_M)|(b&X_M) );}
    
    Phase MainOp(Phase phase);
    Phase CBop(Phase phase);
    Phase EDop(Phase phase);
    Phase DDop(Phase phase);
    Phase FDop(Phase phase);

    Mmu &mmu;
    IoSpace &ioSpace;

    PackedQuad pcaf;
    PackedQuad spbc;
    PackedQuad hlde;
    PackedQuad ixiy;
    PackedQuad afbc2;
    PackedQuad hlde2;
    PackedQuad irfc;

    BYTE nullReg; // for delayed IO
    
    PackedPair memPtrReg; // undocumented internal register
    WORD &memReg=memPtrReg.rr;
    BYTE &memRegH=memPtrReg.r.r1;
    BYTE &memRegL=memPtrReg.r.r2;

    // Z80 tracing
    DisAss da;
    mutable FILE *daOut;
    bool traceFlag=false;

    TrapFlag &trapFlag;

    // Split R register
    int regR=0;
    int rMsb=0;

    // Delayed IO writes
    BYTE delayedIoVal=0;
    BYTE *delayedIoValDest=0;
    WORD delayedIoPort=0;
    int delayedIoPhase=0;

    Phase lastIntAck=0;

    // Monitor
    typedef std::vector<std::unique_ptr<CpuCondition>> Conditions;
    Conditions conditions;
};

/// Monitoring condition base class
class CpuCondition {
 public:
    CpuCondition(bool persist=false): persist(persist) {}
    virtual ~CpuCondition() {}
    virtual bool Check(Z80 &cpu,Phase phase=0)=0;
    bool Persistant()const {return persist;}
 protected:
    UINT16 Pc(Z80 &cpu);
    UINT16 Sp(Z80 &cpu);
    Phase LastIntAck(Z80 &cpu);
    bool persist;
private:
    CpuCondition &operator=(const CpuCondition&);
    CpuCondition(const CpuCondition&);
};

/// Monitor condition for Step
class CpuCondStep:public CpuCondition {
 public:
    CpuCondStep() {}
    bool Check(Z80 &,Phase) {return true;}
};

/// Monitor condition for BreakPt
class CpuCondBreakPt:public CpuCondition {
 public:
    CpuCondBreakPt(UINT16 a, bool persist): CpuCondition(persist), addr(a) {}
    bool Check(Z80 &cpu,Phase) {if (Pc(cpu)==addr) return true; return false;}
 private:
    UINT16 addr;
};

/// Monitor condition for Stack
class CpuCondStack:public CpuCondition {
 public:
    CpuCondStack(UINT16 a): addr(a) {}
    bool Check(Z80 &cpu,Phase) {if (Sp(cpu)==addr) return true; return false;}
 private:
    UINT16 addr;
};

/// Monitor condition for IntAck
class CpuCondIntAck:public CpuCondition {
 public:
    bool Check(Z80 &cpu,Phase phase) {if (LastIntAck(cpu)==phase) return true; return false;}
};
