#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file Mmu.h - Memory management unit emulation (part of GA)
#include "periph.h"

static const bool FAST_WORDS = false;    // FAST_WORDS don't check for 16 bits rollover !!!

/// Memory management unit emulation (part of GA)
class Mmu: public Peripheral {
protected:
    const BYTE *memGet[4]; // 16 bit addressing in 4 bank of 16Ko
    BYTE *memPut[4];

public:
    Mmu() {
        for (int i=0; i<4; i++)
            memGet[i] = memPut[i] = 0;
    }
    virtual ~Mmu() {}
    virtual void IntAck(Phase phase)=0; // Should be in a different class but I'm lazy

    bool IsRam(WORD add)
    {
        return memGet[add>>14] == memPut[add>>14];
    }

    // Bytes access
    inline BYTE MemRd8(WORD add)const
    {
        return *(memGet[add>>14]+(add&0x3FFF));
    }
    inline BYTE MemRdWr8(WORD add)const
    {
        return *(memPut[add>>14]+(add&0x3FFF));
    }
    inline void MemWr8(WORD add, BYTE val)const
    {
        *(memPut[add>>14]+(add&0x3FFF))=val;
    }
    inline BYTE * MemWr8P(WORD add)const
    {
        return memPut[add>>14]+(add&0x3FFF);
    }
    inline signed char MemRdS(WORD add)const
    {
        return reinterpret_cast<const INT8*>(memGet[add>>14])[add&0x3FFF];
    }

    // Word access
    inline WORD MemRd16(WORD add) const
    {
        if constexpr(FAST_WORDS) {
            const BYTE *pt = memGet[add>>14]+(add&0x3FFF);
            return static_cast<WORD>(*pt) + (static_cast<WORD>(*(pt+1))<<8);
        } else {
            return MemRd8(add)+(MemRd8(add+1)<<8);
        }
    }
    inline void MemWr16(WORD add, WORD val) const {
        if constexpr(FAST_WORDS) {
            BYTE *pt = memPut[add>>14]+(add&0x3FFF);
            *pt = static_cast<BYTE>(val&0xFF);
            *(pt+1) = static_cast<BYTE>(val>>8);
        } else {
            MemWr8(add,val&0xFF);MemWr8(add+1,val>>8);
        }
    }

private:
    Mmu(const Mmu &);
    Mmu &operator=(const Mmu&);
};
