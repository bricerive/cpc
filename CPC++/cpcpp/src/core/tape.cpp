// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Tape.cc - Tape emulation class
#include "tape.h"

void Tape::Motor(BYTE /*val*/)
{
}

void Tape::LineIn(BYTE /*val*/)
{
}

BYTE Tape::LineOut()
{
    return 0;
}
