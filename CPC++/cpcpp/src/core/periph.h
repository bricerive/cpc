#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file periph.h - Abstract class for Z80 peripherals
#include "cpcphase.h"
#include "cpctypes.h"

/// Abstract class for Z80 peripherals
class Peripheral {
 public:
    virtual ~Peripheral() {}
    virtual void Reset();
    virtual void Put(Phase phase, WORD addr, BYTE val);
    virtual BYTE Get(Phase phase, WORD addr);
 protected:
    CpcDuration GetElapsedUs(Phase crtPhase);
 private:
    Phase lastPhase=0;
};
