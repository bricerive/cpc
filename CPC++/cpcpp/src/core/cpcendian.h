#pragma once
//
//  cpcendian.h
//  CPC++
//
//  Created by Brice Rivé on 8/10/11.
//  Copyright 2011 Brice Rivé. All rights reserved.
//
#include "cpctypes.h"

// compile time endian detection - does not work for cross-compile
constexpr bool LittleEndian() {
    const short check=0x00FF;
    return (const uint8_t &)check;
}

template<bool littleEndian>
struct UnPackEndian {
    static uint8_t WORD_BYTE_1(uint16_t w) { return w&0xFF;}
    static uint8_t WORD_BYTE_2(uint16_t w) { return (w>>8)&0xFF;}
};

template<>
struct UnPackEndian<true> {
    static uint8_t WORD_BYTE_1(uint16_t w) { return (w>>8)&0xFF;}
    static uint8_t WORD_BYTE_2(uint16_t w) { return w&0xFF;}
};

typedef UnPackEndian<LittleEndian()> Unpack;
