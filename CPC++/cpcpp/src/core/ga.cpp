// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Ga.cc - Gate Array emulation

#include "infra/error.h"
#include "ga.h"
#include "z80.h"
#include "crtc.h"
#include "cpcconsole.h"
#include "cpcframe.h"
#include "roms.h"
#include "cpc_state.h"
#include "infra/log.h"
#include <string.h>
#include <stdio.h>

typedef CpcBase::CpcModelType CpcModelType;

// Notes:
//
//-The living Daylights
// It uses Out to reset the hCnt. This proves that the hCnt is not &0x1F but =0 on that Out.

static long MODLUT_COLORS=0x0FFFF;
static long MODLUT_BORDER=0x10000;
static long MODLUT_ALL=0x1FFFF;

// VDU size constants
const int Ga::vduHSyncVal=10;
const int Ga::vduVSyncVal=30;
const int Ga::vduHTot=64;
const int Ga::vduVTot=312;
const int Ga::vduHOverload=100; // clean up first dirty line in HiRes demo
const int Ga::vduVOverload=40;
const int Ga::maxLinesPerFrame = vduVTot+vduVSyncVal+vduVOverload; // 312+30+40=382
const int Ga::maxNopPerLine = vduHTot+vduHSyncVal+vduHOverload; // 64+10+100=174
const int Ga::maxNopsPerFrame = maxLinesPerFrame*maxNopPerLine; // 66468

Ga::Ga(Client &client, Z80 &cpu, Crtc &crtc, TrapFlag &trapFlag)
: cpu(cpu), crtc(crtc), client(client), trapFlag(trapFlag)
{
    for (int i=0; i<NB_UROM_SLOTS; i++)
        hiRomMap[i]=0;
    for (int i=0; i<NB_UROM_SLOTS; i++)
        romData[i]=0;
    lowerRomData=0;
}

Ga::~Ga()
{
    // release memory buffers
    for (int i=0; i<NB_UROM_SLOTS; i++)
        delete[] romData[i];
    delete[] lowerRomData;
}

void Ga::SetRoms(const BYTE * const rom, const BYTE * const roms[NB_UROM_SLOTS])
{
    for (int i=0; i<NB_UROM_SLOTS; i++) {
        if (romData[i])
            delete[] const_cast<BYTE *>(romData[i]);
        romData[i]=roms[i];
    }
    SetHiRomMap();
    SetMemoryPointers();


    delete lowerRomData;
    lowerRomData=rom;
    if (lowerRomData) {
        romPattern[0][0] = lowerRomData;
        romPattern[1][0] = lowerRomData;
        romPattern[2][0] = lowerRomData;
    } else {
        romPattern[0][0] = ROMLO464;
        romPattern[1][0] = ROMLO664;
        romPattern[2][0] = ROMLO6128;
    }
}

void Ga::CpcModel(CpcModelType val)
{
    if (cpcModel==val) return;
    static const std::map<CpcModelType, int> physicalRamPagesPerModel = {
        {CpcModelType::MODEL_464,1},{CpcModelType::MODEL_664,1},{CpcModelType::MODEL_6128,2},{CpcModelType::MODEL_6128_512K,9},{CpcModelType::MODEL_6128_4M,65}
    };
    nbRamBanks = physicalRamPagesPerModel.at(val);
    cpcModel = val;

    // Kludge: fasllback on 6128 for the rest of the memory mechanisms
    if (cpcModel==CpcModelType::MODEL_6128_512K || cpcModel==CpcModelType::MODEL_6128_4M)
        cpcModel = CpcModelType::MODEL_6128;

    SetHiRomMap();
    SetRamMap();
    SetMemoryPointers();
}

void Ga::CpmBoot(bool val)
{
    cpmBoot=val;
    SetHiRomMap();
    SetMemoryPointers();
}

void Ga::SetRamMap()
{
    ramData.resize(4 * nbRamBanks);
    for (auto &i: ramData) i.resize(16384);
    ramSelection=0;
    ramBank=0;
}

void Ga::SetHiRomMap()
{
    // Basic rom map:
    // Map in all connected expansion ROMs
    // non-connected slots point to onboard ROM
    for (int slot=0; slot<NB_UROM_SLOTS; slot++) {
        hiRomMap[slot] = romData[slot];
        if (hiRomMap[slot]==0)
            hiRomMap[slot]=romPattern[cpcModel][1];
    }
    // The 6128 & 664 have the AMSDOS ROM on-board
    // it can be masked by expansion ROMS
    // if CP/M boot jumper is on, the Amdos ROM becomes ROM Nb 0
    if (cpcModel == CpcModelType::MODEL_664 || cpcModel == CpcModelType::MODEL_6128) {
        if (cpmBoot)
            hiRomMap[0]=ROMXT07;
        else if (romData[7]==0)
            hiRomMap[7]=ROMXT07;
    }
}

// tells which roms to use
const BYTE * Ga::romPattern[3][2] = {{ROMLO464,ROMHI464},{ROMLO664,ROMHI664},{ROMLO6128,ROMHI6128}};

const BYTE *Ga::VideoRam(int block)const {
    return ramData[block].data();
}

int Ga::RamPattern(int ramSelection, int ramBlock)
{
   static const int ramPatterns[8][4] = {
        {0,1,2,3},  {0,1,2,7},  {4,5,6,7},  {0,3,2,7},  {0,4,2,3},  {0,5,2,3},  {0,6,2,3},  {0,7,2,3}
    };
   return ramPatterns[ramSelection & 7][ramBlock];
}

BYTE *Ga::GetRamBlock(int ramBlockId)
{
    int ramBlock = (cpcModel == CpcModelType::MODEL_6128) ? RamPattern(ramSelection, ramBlockId): ramBlockId;
    // if the block is above 3 then it's an extended ram block from the current extension RAM page
    if (ramBlock>=4) {
        int requestedExpansionBlock = ramBank * 4;
        int availableExpansionBlocks = static_cast<int>(ramData.size())-4;
        if (availableExpansionBlocks) {
            int effectiveExpansionBlock = requestedExpansionBlock % availableExpansionBlocks;
            ramBlock += effectiveExpansionBlock;
        }
    }
    return ramData[ramBlock].data();
}

void Ga::SetMemoryPointers()
{
    for (int block = 0; block < 4; block++)
        memGet[block] = memPut[block] = GetRamBlock(block);
    if (!(mf&0x04)) // Lower ROM enabled
        memGet[0] = romPattern[cpcModel][0];
    if (!(mf&0x08)) // Upper ROM enabled
        memGet[3] = hiRomMap[upperRom];
}

void Ga::RenderFrame(const CpcBase::RenderInfo &info, bool force)
{
    frameRenderer.RenderFrame(info,renderFrame,force);
}

void Ga::Reset()
{
    frameRenderer.ResetLuts();

    gcBorder=0;
    for (int i=0; i<16; i++)
        color[i]=0;
    
    vSync=0;
    vSyncCnt=0;
    vSyncArmed=0;
    hCnt = 0;
    intReq = 0;
    LOG_STATUS("GA: Reset");

    ramSelection = 0; // default Ram pattern
    ramBank = 0; // default ram bank
    mf = 0x09; // lower rom on - upper rom off - mode 1
    upperRom = 0; // default upper rom
    SetMemoryPointers();
}

void Ga::Put(Phase phase, WORD addr, BYTE val)
{
    if ((addr&0x2000) == 0x0000) {
        RomSel(val);
    }
    if ((addr&0xC000) == 0x4000) {
        switch (val & 0xC0) {
            case 0x00:
                nc = val&0x1F;
                break;
            case 0x40:
                if (nc & 0x10) {
                    if (gcBorder != (val & 0x1F)) {
                        gcBorder = val & 0x1F;
                        AddModlutChange(phase, MODLUT_BORDER);
                    }
                } else {
                    if (color[nc] != (val & 0x1F)) {
                        color[nc] = val & 0x1F;
                        if (nc<2 || (nc<4 && gcMode<2) || gcMode==0) {
                            AddModlutChange(phase, 1<<nc);
                        }
                    }
                }
                break;
            case 0x80:
                if (val&0x10) {
                    client.CrtcTrace(phase, "hCnt Clear");
                    hCnt=0;
                    SetIntReq(0);
                }
                mf = val&0x3F;
                SetMemoryPointers();
                break;
            case 0xC0:
                // Yarek extension
                // https://www.cpcwiki.eu/index.php/Standard_Memory_Expansions
                //  Expansions bigger than 512K extend that above standard.
                //  The LSBs of the 64K bank number is kept in D5-D3 data bits (as above),
                //  the MSBs of the 64K bank number are in A10-A8 address bits.
                //  Typically in inverted form (so the first 512K block is accessed via Port 7Fxxh, the next via 7Exxh, etc.
                //
                //  OUT [01111aaaxxxxxxxx],11bbbccc
                //  aaa  = upper bits of 64K bank selection (512K-step) (A8,A9,A10 bits)
                //  bbb  = lower bits of 64K bank selection (64K-step)  (D3,D4,D5 bits)
                //  ccc = configuration (as in CPC6128)
                //  xxxxxxxx = should be set so that it doesn't conflict with other ports (*)
                ramBank = (~addr&0x0700)>>5 | (val&0x38)>>3;
                //LOG_STATUS("Ram select: %04X <- %02X  ramPage=%d", addr, val, ramBank);
                ramSelection = val;
                SetMemoryPointers();
                break;
            default:
                ERR_THROW("Internal error");
        }
    }
}

BYTE Ga::Get(Phase /*phase*/,WORD /*addr*/)
{
    LOG_STATUS("GA:Read attempt!!!");
    return 0xFF;
}

void Ga::RomSel(int val)
{
    upperRom = val;
    SetMemoryPointers();
}

// Update mode change (after end of scan only)
void Ga::ApplyModeChange(Phase phase)
{
    if (gcMode != (mf&3)) {
        gcMode = mf&3;
        AddModlutChange(phase, MODLUT_COLORS);
    }
}

void Ga::VSyncOn()
{
    vSync=1;
    vSyncCnt=0;
    vSyncArmed = 3;
}

void Ga::VSyncOff()
{
    vSync=0;
    vSyncArmed &= ~1;
}

void Ga::HSyncOff(Phase phase, bool &intRaised, int &hCnt_)
{
    intRaised=false;
    hCnt++;
    if (!vSyncAtLastHSync && vSync) {
        vSyncCnt=0;
        vSyncArmed = 3;
    }
    if (!vSyncArmed) {
        if (hCnt==52) {
            SetIntReq(1);
            intRaised=true;
            hCnt=0;
        }
    } else {
        vSyncCnt++;
        if (vSyncCnt==vSyncHReset) {
            vSyncArmed=0;
            if (hCnt>=32) {
                SetIntReq(1);
                intRaised=true;
            }
            hCnt=0;
        }
    }
    vSyncAtLastHSync=vSync;
    hCnt_=hCnt;
    ApplyModeChange(phase);
}

void Ga::IntAck(Phase phase)
{
    client.CrtcTrace(phase,"IntAck");
    SetIntReq(0);
    hCnt &= 0x1F; // Sim City
}

void Ga::SetIntReq(int val)
{
    intReq = val;
    cpu.Int(val);
}

void Ga::GetState(GaState &state) const
{
    state.colIdx = nc;
    for (int i=0; i<16; i++)
        state.col[i] = color[i];
    state.border = gcBorder;
    state.ramSelection=ramSelection;
    state.upperRom = upperRom;
    state.mf = mf;
    state.vSync = vSync;
    state.vSyncArmed = vSyncArmed;
    state.vSyncCnt = vSyncCnt;
    state.hCnt = hCnt;
    state.intReq = intReq;

    state.ram.resize(ramData.size());
    for (int i=0; i<ramData.size(); i++) {
        memcpy(state.ram[i].Pt(), ramData[i].data(), 16384);
    }
}

void Ga::SetState(const GaState &state)
{
    nc = state.colIdx;
    for (int i=0; i<16; i++)
        color[i]= state.col[i];
    gcBorder = state.border;

    Put(0, 0x7FFF, 0x80 | state.mf); // Mode select - ROM config
    Put(0, 0x7FFF, 0xC0 | state.ramSelection); // RAM config
    AddModlutChange(0, MODLUT_ALL);
    RomSel(state.upperRom);
    for (int i=0; i<state.ram.size() && i<ramData.size(); i++)
        memcpy(ramData[i].data(), state.ram[i].Pt(), 16384);
}

void Ga::FrameStart(Phase phase)
{
    renderFrame.FrameStart(phase);
}

void Ga::LineStart(int lineNo, Phase phase, RAW32 *&pt)
{
    renderFrame.LineStart(lineNo, phase, pt);
}

void Ga::LineDone(int lineNo, Phase phase, RAW32 *pt, bool changed, bool hScroll)
{
    renderFrame.LineDone(lineNo, phase, pt, changed, hScroll);
}

void Ga::FrameDone(int lastLine)
{
    renderFrame.FrameDone(lastLine);
}

void Ga::AddModlutChange(Phase phase, long flags)
{
    if (flags & MODLUT_COLORS)
        renderFrame.NewColors(phase, gcMode, color);
    if (flags & MODLUT_BORDER)
        renderFrame.NewBorder(phase, gcBorder);
}
