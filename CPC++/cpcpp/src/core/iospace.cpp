// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// IoSpace.cc - I/O space management unit emulation
#include "iospace.h"
#include "infra/log.h"

IoSpace::IoSpace(Peripheral &crtc, Peripheral &pio, Peripheral &centro, Peripheral &fdc, Peripheral &ga)
    : crtc(crtc), pio(pio), centro(centro), fdc(fdc), ga(ga)
{
}

void IoSpace::ResetExt(Phase /*phase*/)
{
    // fdc.Reset();
    // sio.Reset();
    // brg.Reset();
    // multi.Reset();
}

BYTE IoSpace::Get(const Phase &phase, WORD addr) {
    int caught = 0;
    BYTE val=0xFF;
    if ((addr&0x4000) == 0x0000) { caught++; val=crtc.Get(phase,addr); }
    if ((addr&0x1000) == 0x0000) { caught++; val=centro.Get(phase,addr); }
    if ((addr&0x0800) == 0x0000) { caught++; val=pio.Get(phase,addr); }
    if ((addr&0x0480) == 0x0000) { caught++; val=fdc.Get(phase,addr); }
    //    if ((addr&0x0720) == 0x0200) caught++,val=sio.Get(phase,addr);
    //    if ((addr&0x0720) == 0x0300) caught++,val=brg.Get(phase,addr);
    //    if ( addr         == 0xFEE8) caught++,val=multi.Get(phase,addr);

    // From Kev Thacker:
    // This test shows that an I/O read can be used to write
    // data into the Gate-Array and that it doesn't check
    // the R/W signal from the Z80 when accessing it's registers.
    // This uses the 8255 as a temporary store and selects both
    // the 8255 AND GA when the read is done. The 8255 puts the
    // data onto the bus which the GA then writes into it's registers.
    // This test displays a coloured bar ('raster') Colours are yellow, green and grey.
    if ((addr&0xC000) == 0x4000) { caught++; ga.Put(phase,addr,val); }
    if (caught!=1) {
        if (caught==0) val=nullDev.Get(phase,addr);
        else LOG_STATUS("Input clash at addr %04X",addr);
    }
    return val;
}

 void IoSpace::Put(const Phase &phase, WORD addr, BYTE val) {
    int caught=0;
    if ((addr&0xC000) == 0x4000 || (addr&0x2000) == 0x0000) { caught++,ga.Put(phase,addr,val); }
    if ((addr&0x4000) == 0x0000) { caught++,crtc.Put(phase,addr,val); }
    if ((addr&0x1000) == 0x0000) { caught++,centro.Put(phase,addr,val); }
    if ((addr&0x0800) == 0x0000) { caught++,pio.Put(phase,addr,val); }
    if ((addr&0x0480) == 0x0000) { caught++,fdc.Put(phase,addr,val); }
    //    if ((addr&0x0720) == 0x0200) caught++,sio.Put(phase,addr,val);
    //    if ((addr&0x0720) == 0x0300) caught++,brg.Put(phase,addr,val);
    if ( addr         == 0xF8FF) { caught++,ResetExt(phase); }
    //    if ( addr         == 0xFEE8) caught++,multi.Put(phase,addr,val);
    if (caught!=1) {
        if (caught==0) nullDev.Put(phase,addr,val);
        //else LOG_STATUS("Multiple Out at port %04X",addr);
    }
}