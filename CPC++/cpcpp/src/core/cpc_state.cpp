/// \file cpc_state.cpp

#include "cpc_state.h"

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>

#include <sstream>

std::string to_base64(const std::vector<uint8_t> &data)
{
    using namespace boost::archive::iterators;
    using It = base64_from_binary<transform_width<std::vector<uint8_t>::const_iterator, 6, 8>>;
    auto tmp = std::string(It(std::begin(data)), It(std::end(data)));
    return tmp.append((3 - data.size() % 3) % 3, '='); // Padding
}

std::vector<uint8_t> from_base64(const std::string &base64_str)
{
    using namespace boost::archive::iterators;
    using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

    // Remove padding characters
    size_t num_pad_chars = (4 - base64_str.size() % 4) % 4;
    std::string base64_str_padded = base64_str;
    base64_str_padded.append(num_pad_chars, '=');

    size_t pad_chars = count(base64_str_padded.begin(), base64_str_padded.end(), '=');
    std::replace(base64_str_padded.begin(), base64_str_padded.end(), '=', 'A');
    std::vector<uint8_t> data(It(base64_str_padded.begin()), It(base64_str_padded.end()));
    data.erase(data.end() - pad_chars, data.end()); // Remove padding characters from decoded data
    return data;
}
