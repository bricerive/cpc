// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// DisAss.cpp - Z80 disassembler
#include <stdio.h>
#include <string.h>

#include "disass.h"
#include "mmu.h"

static const char *r0[] = {"B", "C", "D", "E", "H", "L", "(HL)", "A"};
static const char *rr0[] = {"(BC)", "A", "(DE)", "A", "(nn)", "HL", "(nn)", "A"};
static const char *rr1[] = {"BC","DE","HL","SP"};
static const char *rr2[] = {"BC","DE","HL","AF"};
static const char *i0[] = {"RLCA","RRCA","RLA","RRA","DAA","CPL","SCF","CCF"};
static const char *i1[] = {"ADD","ADC","SUB","SBC","AND","XOR","OR ","CP "};
static const char *i2[] = {"LDI","CPI","INI","OUTI","LDD","CPD","IND","OUTD"};
static const char *i3[] = {"LDIR","CPIR","INIR","OTIR","LDDR","CPDR","INDR","OTDR"};
static const char *i4[] = {"RLC  ","RL   ","RRC  ","RR   ","SLA  ","SRA  ","SLL  ","SRL  "};
static const char *c0[] = {"NZ","Z","NC","C","PO","PE","P","M"};


/* Main JumpBlock entries (#BB00 -> #BD37) */
static const char *mainJumpBlock[] = {
    "KMINITIALIZE","KMRESET","KMWAITCHAR","KMREADCHAR",
    "KMCHARRETURN","KMSETEXPAND","KMGETEXPAND","KMEXPBUFFER",
    "KMWAITKEY","KMREADKEY","KMTESTKEY","KMGETSTATE",
    "KMGETJOYSTICK","KMSETTRANSLATE","KMGETTRANSLATE","KMSETSHIFT",
    "KMGETSHIFT","KMSETCONTROL","KMGETCONTROL","KMSETREPEAT",
    "KMGETREPEAT","KMSETDELAY","KMGETDELAY","KMARMBREAKS",
    "KMDISARMBREAKS","KMBREAKEVENT","TXTINITIALIZE","TXTRESET",
    "TXTVDUENABLE","TXTVDUDISABLE","TXTOUTPUT","TXTWRCHAR",
    "TXTRDCHAR","TXTSETGRAPHIC","TXTWINENABLE","TXTGETWINDOW",
    "TXTCLEARWINDOW","TXTSETCOLUMN","TSTSETROW","TSTSETCURSOR",
    "TXTGETCURSOR","TXTCURENABLE","TXTCURDISABLE","TXTCURON",
    "TXTCUROFF","TXTVALIDATE","TXTPLACECURSOR","TXTREMOVECURSOR",
    "TXTSETPEN","TXTGETPEN","TXTSETPAPER","TXTGETPAPER",
    "TXTINVERSE","TXTSETBACK","TXTGETBACK","TXTGETMATRIX",
    "TXTSETMATRIX","TXTSETMTABLE","TXTGETMTABLE","TXTGETCONTROLS",
    "TXTSTRSELECT","TXTSWAPSTREAMS","GRAINITIALIZE","GRARESET",
    "GRAMOVEABSOLUTE","GRAMOVERELATIVE","GRAASKCURSOR","GRASETORIGIN",
    "GRAGETORIGIN","GRAWINWIDTH","GRAWINHEIGHT","GRAGETWWIDTH",
    "GRAGETWHEIGHT","GRACLEARWINDOW","GRASETPEN","GRAGETPEN",
    "GRASETPAPER","GRAGETPAPER","GRAPLOTABSOLUTE","GRAPLOTRELATIVE",
    "GRATESTABSOLUTE","GRATESTRELATIVE","GRALINEABSOLUTE","GRALINERELATIVE",
    "GRAWRCHAR","SCRINITIALIZE","SCRRESET","SCRSETOFFSET",
    "SCRSETBASE","SCRGETLOCATION","SCRSETMODE","SCRGETMODE",
    "SCRCLEAR","SCRCHARLIMITS","SCRCHARPOSITION","SCRDOTPOSITION",
    "SCRNEXTBYTE","SCRPREVBYTE","SCRNEXTLINE","SCRPREVLINE",
    "SCRINKENCODE","SCRINKDECODE","SCRSETINK","SCRGETINK",
    "SCRSETBORDER","SCRGETBORDER","SCRSETFLASHING","SCRGETFLASHING",
    "SCRFILLBOX","SCRFLOODBOX","SCRCHARINVERT","SCRHWROLL",
    "SCRSWROLL","SCRUNPACK","SCRREPACK","SCRACCESS",
    "SCRPIXELS","SCRHORIZONTAL","SCRVERTICAL","CASINITIALIZE",
    "CASSETSPEED","CASNOISY","CASSTARTMOTOR","CASSTOPMOTOR",
    "CASRESTOREMOTOR","CASINOPEN","CASINCLOSE","CASINABANDON",
    "CASINCHAR","CASINDIRECT","CASRETURN","CASTESTEOF",
    "CASOUTOPEN","CASOUTCLOSE","CASOUTABANDON","CASOUTCHAR",
    "CASOUTDIRECT","CASCATALOG","CASWRITE","CASREAD",
    "CASCHECK","SOUNDRESET","SOUNDQUEUE","SOUNDCHECK",
    "SOUNDARMEVENT","SOUNDRELEASE","SOUNDHOLD","SOUNDCONTINUE",
    "SOUNDAMPLENV","SOUNDTONEENV","SOUNDAMPADD","SOUNTTONEADD",
    "KLCHOKEOFF","KLROMWALK","KLINITBACK","KLLOGEXT",
    "KLFINDCOMMAND","KLNEWFRAMEFLY","KLADDFRAMEFLY","KLDELFRAMEFLY",
    "KLNEWFASTTICKER","KLADDFASTTICKER","KLDELFASTTICKER","KLADDTICKER",
    "KLDELTICKER","KLINITEVENT","KLEVENT","KLSYNCRESET",
    "KLDELSYNC","KLNEXTSYNC","KLDOSYNC","KLDONESYNC","KLEVENTDISABLE",
    "KLEVENTENABLE","KLDISARMEVENT","KLTIMEPLEASE","KLTIMESET",
    "MCBOOTPROG","MCSTARTPROG","MCWAITFLYBACK","MCSETMODE",
    "MCSCREENOFFSET","MCCLEARINKS","MCSETINKS","MCRESETPRINTER",
    "MCPRINTCHAR","MCBUSYPRINTER","MCSENDPRINTER","MCSOUNDREGISTER",
    "JUMPRESTORE"
};
/* Firmware indirections (#BDCD -> #BDF1) */
static const char *firmInd[] = {
    "TXTDRAWCURSOR","TXTUNDRAWCURSOR","TXTWRITECHAR","TXTUNWRITE",
    "TXTOUTACTION","GRAPLOT","GRATEST","GRALINE",
    "SCRREAD","SCRWRITE","SCRMODECLEAR","KMTESTBREAK",
    "MCWAITPRINTER"
};
/* Hi jumpBlock (#B900 -> #B921) */
static const char *hiJumpBlock[] = {
    "KLUROMENABLE","KLUROMDISABLE","KLLROMENABLE","KLLROMDISABLE",
    "KLROMRESTORE","KLROMSELECT","KLCURRSELECTION","KLPROBEROM",
    "KLROMDESELECT","KLLDIR","KLLDDR","KLPOLLSYNC"
};

const char *DisAss::PortName(WORD addr)
{
    if ((addr&0x8000)==0) return "Ga";
    if ((addr&0x4000)==0) {
        if ((addr&0x0100)==0) return "Crtc addr";
        return "Crtc reg";
    }
    if ((addr&0x2000)==0) return "RomSel";
    if ((addr&0x1000)==0) return "Centro";
    if ((addr&0x0800)==0) {
        switch (addr&0x0300) {
            case 0x0000: return "PIO A";
            case 0x0100: return "PIO B";
            case 0x0200: return "PIO C";
            case 0x0300: return "PIO Ctrl";
        }
    }
    if ((addr&0x0400)==0) {
        if ((addr&0x0080)==0) {
            if ((addr&0x0100)==0) return "FDC motor";
            if ((addr&0x0001)==0) return "FDC Status";
            return "FDC Data";
        }
        if ((addr&0x0040)==0) return "Reserved";
        if ((addr&0x0020)==0) {
            if ((addr&0x0200)==1) {
                if ((addr&0x0100)==0) return "SIO";
                return "BRG";
            }
        }
        if ((addr&0x00E0)==0x00E0) return "Usr Ext";
        if (addr==0xF8FF) return "Ext Rst";
    }
    if (addr == 0xFEE8) return "Multiface";
    return "Unknown";
}

void DisAss::mcpy(char *dst, const char *src, size_t n)
{
    if (dst+n<=src || dst>=src+n) // no overlap
        memcpy(dst,src,n);
    else if (src>dst) // overlap up
        while(n--) *dst++=*src++;
    else // overlap down
        while(n--) *(dst+n)=*(src+n);
}

WORD DisAss::CompleteStr(const Mmu &mmu, char *s, int n, WORD add, int offset, bool withComment, WORD bcForIoComments)
{
    char *pt=s;
    int n2 = n;

    do {
        switch (*pt) {
            case 'n':
                if (*(pt+1) == 'n') {
                    WORD addr = mmu.MemRd8(add+1)*256+mmu.MemRd8(add);
                    add+=2;
                    mcpy(pt+4, pt+2, strlen(pt+2)+1);
                    snprintf(pt, n2, "%04X%s", addr, pt+4);
                    if (withComment) {
                        if (addr>=0xBB00 && addr<=0xBD37 && ((addr-0xBB00)%3)==0)
                            snprintf(s, n, "%s[%s]", s, mainJumpBlock[(addr - 0xBB00) / 3]);
                        if (addr>=0xBDCD && addr<=0xBDF1 && ((addr-0xBDCD)%3)==0)
                            snprintf(s, n, "%s[%s]", s, firmInd[(addr - 0xBDCD) / 3]);
                        if (addr>=0xB900 && addr<=0xB921 && ((addr-0xB900)%3)==0)
                            snprintf(s, n, "%s[%s]", s, hiJumpBlock[(addr - 0xB900) / 3]);
                    }
                } else {
                    mcpy(pt+2, pt+1, strlen(pt+1)+1);
                    snprintf(pt, n2, "%02X%s", mmu.MemRd8(add), pt+2);
                    add++;
                }
                break;
            case 'o':
                mcpy(pt+2, pt+1, strlen(pt+1)+1);
                snprintf(pt, n2, "%02X%s", offset, pt+2);
                break;
            case 'e':
                mcpy(pt+4, pt+1, strlen(pt+1)+1);
                snprintf(pt, n2, "%04X%s", add+mmu.MemRdS(add)+1, pt+4);
                add++;
                break;
            default:
                break;
        }
        --n2;
    } while (*(pt++));
    if (withComment) {
        if (!strncmp(s, "OUT  ", 5) && !strncmp(s+6, ",(C)", 4))
            snprintf(s, n, "%s [%s]", s, PortName(bcForIoComments));
        if (!strncmp(s, "IN   ", 5) && !strncmp(s+6, ",(C)", 4))
            snprintf(s, n, "%s [%s]", s, PortName(bcForIoComments));
        if (!strncmp(s, "OUTI", 4)) snprintf(s, n, "%s [%s]", s, PortName(bcForIoComments - 0x0100));
        if (!strncmp(s, "INI", 3)) snprintf(s, n, "%s [%s]", s, PortName(bcForIoComments - 0x0100));
    }
    return add;
}

void DisAss::ConvertIXIY(bool x, char *s, int n, int offset)
{
    const char upper = x? 'X': 'Y';
    const char lower = x? 'x': 'y';

    char *pt = strstr(s, " ");
    if (!pt) return;
    char *pt2 = strstr(pt,"(HL)");
    if (pt2 && !(pt[0]=='J' && pt[1]=='P')) {
        mcpy(pt2+5, pt2+3, strlen(pt2+3)+1);
        if (offset>=0)
            snprintf(pt2+1, n - (pt2+1-pt), "I%c+o%s", upper, pt2+5);
        else
            snprintf(pt2 + 1, n - (pt2 + 1 - pt), "I%c+n%s", upper, pt2 + 5);
        return;
    }
    pt2=strstr(pt,"HL");
    if (pt2) {
        snprintf(pt2, n - (pt2 - pt), "I%c%s", upper, pt2 + 2);
        return;
    }
    pt2=strstr(pt,"H");
    if (pt2) {
        mcpy(pt2+2, pt2+1, strlen(pt2+1)+1);
        snprintf(pt2, n - (pt2 - pt), "I%c%s", lower, pt2 + 2);
        return;
    }
    pt2=strstr(pt,"L");
    if (pt2) {
        mcpy(pt2+2, pt2+1, strlen(pt2+1)+1);
        snprintf(pt2, n - (pt2 - pt) , "i%c%s", upper, pt2 + 2);
        return;
    }
    strcpy(s, "CPCERR");
}

void DisAss::ConvertIX(char *s, int n, int offset)
{
    ConvertIXIY(true, s, n, offset);
}

void DisAss::ConvertIY(char *s, int n, int offset)
{
    ConvertIXIY(false, s, n, offset);
}

WORD  DisAss::Disassemble(const Mmu &mmu, WORD add, char *s, int n, bool withComment, WORD bcForIoComments)
{
    int offset=0;
    return Disassemble(mmu, add, s, n, withComment, bcForIoComments, false, offset);
}

WORD DisAss::Disassemble(const Mmu &mmu, WORD add, char *s, int n, bool withComment, WORD bcForIoComments,
    bool indexed, int &offset)
{
    BYTE in1;
    WORD add1=add;
    char buf[128];

    if (!s)
        s = buf;

    strcpy(s, "CPCERR");
    in1 = mmu.MemRd8(add++);


    switch (in1 & 0xC0) {
        case 0x00:
            switch (in1 & 0x07) {
                case 0x00:
                    switch (in1 & 0x38) {
                        case 0x00:
                            snprintf(s, n, "NOP");
                            break;
                        case 0x08:
                            snprintf(s, n, "EX   AF, AF'");
                            break;
                        case 0x10:
                            snprintf(s, n, "DJNZ e");
                            break;
                        case 0x18:
                            snprintf(s, n, "JR   e");
                            break;
                        case 0x20:
                            snprintf(s, n, "JR   NZ,e");
                            break;
                        case 0x28:
                            snprintf(s, n, "JR   Z,e");
                            break;
                        case 0x30:
                            snprintf(s, n, "JR   NC,e");
                            break;
                        case 0x38:
                            snprintf(s, n, "JR   C,e");
                            break;
                    }
                    break;
                case 0x01:
                    switch (in1 & 0x08) {
                        case 0x00:
                            snprintf(s, n, "LD   %s,nn", rr1[(in1 >> 4) & 3]);
                            break;
                        case 0x08:
                            snprintf(s, n, "ADD  HL,%s", rr1[(in1 >> 4) & 3]);
                            break;
                    }
                    break;
                case 0x02:
                    snprintf(s, n, "LD   %s,%s", rr0[(in1 >> 3) & 7], rr0[((in1 >> 3) & 7) ^ 1]);
                    break;
                case 0x03:
                    switch (in1 & 0x08) {
                        case 0x00:
                            snprintf(s, n, "INC  %s", rr1[(in1 >> 4) & 3]);
                            break;
                        case 0x08:
                            snprintf(s, n, "DEC  %s", rr1[(in1 >> 4) & 3]);
                            break;
                    }
                    break;
                case 0x04:
                    if (indexed && ((in1>>3)&7)==6) {
                        offset = mmu.MemRd8(add++);
                    }
                    snprintf(s, n, "INC  %s", r0[(in1 >> 3) & 7]);
                    break;
                case 0x05:
                    if (indexed && ((in1>>3)&7)==6) {
                        offset = mmu.MemRd8(add++);
                    }
                    snprintf(s, n, "DEC  %s", r0[(in1 >> 3) & 7]);
                    break;
                case 0x06:
                    if (indexed && ((in1>>3)&7)==6) {
                        offset = mmu.MemRd8(add++);
                    }
                    snprintf(s, n, "LD   %s,n", r0[(in1 >> 3) & 7]);
                    break;
                case 0x07:
                    snprintf(s, n, "%s", i0[(in1 >> 3) & 7]);
                    break;
            }
            break;
        case 0x40:
            if (indexed && ((in1 >> 3) & 7) == 6) {
                offset = mmu.MemRd8(add++);
            }
            snprintf(s, n, "LD   %s,%s", r0[(in1 >> 3) & 7], r0[in1 & 7]);
            if (in1 == 0x76) snprintf(s, n, "HALT");
            break;
        case 0x80:
            if (indexed && ((in1>>3)&7)==6) {
                offset = mmu.MemRd8(add++);
            }
            snprintf(s, n, "%s  %s", i1[(in1 >> 3) & 7], r0[in1 & 7]);
            break;
        case 0xC0:
            switch (in1 & 7) {
                case 0x00:
                    snprintf(s, n, "RET  %s", c0[(in1 >> 3) & 7]);
                    break;
                case 0x01:
                    if (in1 & 0x08) {
                        switch (in1 & 0x30) {
                            case 0x00:
                                snprintf(s, n, "RET");
                                break;
                            case 0x10:
                                snprintf(s, n, "EXX");
                                break;
                            case 0x20:
                                snprintf(s, n, "JP   (HL)");
                                break;
                            case 0x30:
                                snprintf(s, n, "LD   SP,HL");
                                break;
                        }
                    } else
                        snprintf(s, n, "POP  %s", rr2[(in1 >> 4) & 3]);
                    break;
                case 0x02:
                    snprintf(s, n, "JP   %s,nn", c0[(in1 >> 3) & 7]);
                    break;
                case 0x03:
                    switch (in1 & 0x38) {
                        case 0x00:
                            snprintf(s, n, "JP   nn");
                            break;
                        case 0x08:  /* CB */
                            in1 = mmu.MemRd8(add++);
                            if (indexed) {
                                offset = in1;
                                in1 = mmu.MemRd8(add++);
                            }
                            switch (in1 & 0xC0) {
                                case 0x00:
                                    snprintf(s, n, "%s%s", i4[(in1 >> 3) & 7], r0[in1 & 7]);
                                    if (indexed && ((in1&0x07)!=0x06)) {
                                        strcat(s, ",");
                                        strcat(s, r0[in1&7]);
                                    }
                                    break;
                                case 0x40:
                                    snprintf(s, n, "BIT  %d,%s", (in1 >> 3) & 7, r0[in1 & 7]);
                                    break;
                                case 0x80:
                                    snprintf(s, n, "RES  %d,%s", (in1 >> 3) & 7, r0[in1 & 7]);
                                    if (indexed && ((in1&0x07)!=0x06)) {
                                        strcat(s, ",");
                                        strcat(s, r0[in1&7]);
                                    }
                                    break;
                                case 0xC0:
                                    snprintf(s, n, "SET  %d,%s", (in1 >> 3) & 7, r0[in1 & 7]);
                                    if (indexed && ((in1&0x07)!=0x06)) {
                                        strcat(s, ",");
                                        strcat(s, r0[in1&7]);
                                    }
                                    break;
                            }
                            break;
                        case 0x10:
                            snprintf(s, n, "OUT  (n),A");
                            break;
                        case 0x18:
                            snprintf(s, n, "IN   A,(n)");
                            break;
                        case 0x20:
                            snprintf(s, n, "EX   (SP),HL");
                            break;
                        case 0x28:
                            snprintf(s, n, "EX   DE,HL");
                            break;
                        case 0x30:
                            snprintf(s, n, "DI");
                            break;
                        case 0x38:
                            snprintf(s, n, "EI");
                            break;
                    }
                    break;
                case 0x04:
                    snprintf(s, n, "CALL %s,nn", c0[(in1 >> 3) & 7]);
                    break;
                case 0x05:
                    if ((in1 & 0x08) == 0) {
                        snprintf(s, n, "PUSH %s", rr2[(in1 >> 4) & 3]);
                    } else {
                        switch (in1 & 0x30) {
                            case 0x00:
                                snprintf(s, n, "CALL nn");
                                break;
                            case 0x10: /* DD */
                                add = add+Disassemble(mmu, add, s, n, withComment, bcForIoComments, true, offset);
                                ConvertIX(s, n, offset);
                                break;
                            case 0x20: /* ED */
                                in1 = mmu.MemRd8(add++);
                                switch (in1 & 0xC0) {
                                    case 0x40:
                                        switch (in1 & 7) {
                                            case 0x00:
                                                snprintf(s, n, "IN   %s,(C)", r0[(in1 >> 3) & 7]);
                                                break;
                                            case 0x01:
                                                snprintf(s, n, "OUT  %s,(C)", r0[(in1 >> 3) & 7]);
                                                break;
                                            case 0x02:
                                                if (in1&0x08)
                                                    snprintf(s, n, "ADC  HL,%s", rr1[(in1 >> 4) & 3]);
                                                else
                                                    snprintf(s, n, "SBC  HL,%s", rr1[(in1 >> 4) & 3]);
                                                break;
                                            case 0x03:
                                                if (in1&0x08)
                                                    snprintf(s, n, "LD   %s,(nn)", rr1[(in1 >> 4) & 3]);
                                                else
                                                    snprintf(s, n, "LD   (nn),%s", rr1[(in1 >> 4) & 3]);
                                                break;
                                            case 0x04:
                                                switch (in1 & 0x38) {
                                                    case 0x00:
                                                        snprintf(s, n, "NEG");
                                                        break;
                                                }
                                                break;
                                            case 0x05:
                                                switch (in1 & 0x38) {
                                                    case 0x00:
                                                        snprintf(s, n, "RETN");
                                                        break;
                                                    case 0x08:
                                                        snprintf(s, n, "RETI");
                                                        break;
                                                }
                                                break;
                                            case 0x06:
                                                switch (in1 & 0x38) {
                                                    case 0x00:
                                                        snprintf(s, n, "IM   0");
                                                        break;
                                                    case 0x10:
                                                        snprintf(s, n, "IM   1");
                                                        break;
                                                    case 0x18:
                                                        snprintf(s, n, "IM   2");
                                                        break;
                                                }
                                                break;
                                            case 0x07:
                                                switch (in1 & 0x38) {
                                                    case 0x00:
                                                        snprintf(s, n, "LD   I,A");
                                                        break;
                                                    case 0x08:
                                                        snprintf(s, n, "LD   R,A");
                                                        break;
                                                    case 0x10:
                                                        snprintf(s, n, "LD   A,I");
                                                        break;
                                                    case 0x18:
                                                        snprintf(s, n, "LD   A,R");
                                                        break;
                                                    case 0x20:
                                                        snprintf(s, n, "RRD");
                                                        break;
                                                    case 0x28:
                                                        snprintf(s, n, "RLD");
                                                        break;
                                                }
                                                break;
                                        }
                                        break;
                                    case 0x80:
                                        switch (in1 & 0x34) {
                                            case 0x20:
                                                snprintf(s, n, "%s", i2[(in1 & 3) + ((in1 >> 1) & 0x04)]);
                                                break;
                                            case 0x30:
                                                snprintf(s, n, "%s", i3[(in1 & 3) + ((in1 >> 1) & 0x04)]);
                                                break;
                                        }
                                        break;
                                }
                                    break;
                            case 0x30: /* FD */
                                add = add+Disassemble(mmu, add, s, n, withComment, bcForIoComments, true, offset);
                                ConvertIY(s, n, offset);
                                break;
                        }
                    }
                    break;
                case 0x06:
                    snprintf(s, n, "%s  n", i1[(in1 >> 3) & 7]);
                    break;
                case 0x07:
                    switch (in1 & 0x38) {
                        case 0x00:
                            snprintf(s,n,"RST  0[RESET]");
                            break;
                        case 0x08:
                            snprintf(s, n, "RST  1[LOWJMP #%02X%02X]", mmu.MemRd8(add + 1), mmu.MemRd8(add));
                            add+=2;
                            break;
                        case 0x10:
                            snprintf(s, n, "RST  2[SIDECALL #%02X%02X]", mmu.MemRd8(add + 1), mmu.MemRd8(add));
                            add+=2;
                            break;
                        case 0x18:
                        {
                            WORD inlineAdd = mmu.MemRd16(add);
                            snprintf(s, n, "RST  3[FARCALL #%04X->[%04X-%02X]]", inlineAdd,
                                mmu.MemRd16(inlineAdd), mmu.MemRd8(inlineAdd + 2));
                            add+=2;
                        }
                            break;
                        case 0x20:
                            snprintf(s, n, "RST  4[RAMLAM]");
                            break;
                        case 0x28:
                            snprintf(s, n, "RST  5[FIRMJMP #%02X%02X]", mmu.MemRd8(add + 1), mmu.MemRd8(add));
                            add+=2;
                            break;
                        case 0x30:
                            snprintf(s, n, "RST  6[USERRST]");
                            break;
                        case 0x38:
                            snprintf(s, n, "RST  7[INTRST]");
                            break;
                    }
                    break;
            }
            break;
    }
    add = CompleteStr(mmu, s, n, add, offset, withComment, bcForIoComments);
    return add-add1;
}

int DisAss::CodeRewind(const Mmu &mmu, WORD add, int cnt)
{
    int i,j;
    WORD prevAdd[16],d;

    prevAdd[0]=add;
    for (i=0; i<cnt; i++) {
        for (j=5; j>=1; j--) { // rewind 5 bytes max
            d = Disassemble(mmu,prevAdd[i]-j);
            if (d==j)
                break;
        }
        // If we failed, j is 0, and we simply put the same addr back
        prevAdd[i+1]=prevAdd[i]-j;
    }
    return prevAdd[0]-prevAdd[i];
}
