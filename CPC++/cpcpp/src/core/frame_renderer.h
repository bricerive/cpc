#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file frame_renderer.h - CPC frame rendering
#include "cpcframe.h"
#include "cpc_base.h"
#include <array>

class FrameRenderer
{
public:
    void ResetLuts();
    void RenderFrame(const CpcBase::RenderInfo &info, CpcFrame &frame, bool force=false);

private:
    template<typename RenderDetails>
    void RenderFrame(const CpcBase::RenderInfo &info, bool force, CpcFrame &frame);

    // PixP for colors
    void SetPixColors(const GaColors &gc, const CpcBase::RenderInfo &info);
    // PixP for border
    void SetPixBorder(const GaBorder &gc, const CpcBase::RenderInfo &info);
    void UpdateBorderValue(GaBorder *&gaBorder, Phase phase, const CpcBase::RenderInfo &info);
    void UpdateColorValues(GaColors *&gaColors, Phase phase, const CpcBase::RenderInfo &info);
    // PixP for Sync
    void SetPixSync(const CpcBase::RenderInfo &info);

    int lastRenderMode=-1;     // Render mode
    const RAW32 *lastLut=0;
    static const int NB_PAL=16;
    GaColors modlutBuff[NB_PAL]; // stores colors+mode for each cached palette
    std::array<int, NB_PAL> sortedPal={};
    int crtPal=0;
    static const int PIXCODE_SZ = 4 * 256;     // 32 bits RGBA * 256 possible vram values
    RAW64 pixCodeBuff[NB_PAL][PIXCODE_SZ];
    RAW64 *crtPixCodeBuff=0;
    UINT8 *lastRenderBase=0;

    RAW64 border[4]={}; // 32 bit RGBA
    RAW64 black[4]={}; // 32 bit RGBA
};
