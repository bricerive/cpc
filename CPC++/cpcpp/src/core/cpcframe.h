#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file cpcframe.h - Class to store a rendered frame
#include "cpctypes.h"
#include "cpcphase.h"

/// store the state of color registers for part of a frame
class GaColors {
public:
    GaColors();
    void SetVal(BYTE mode, const BYTE *col);

    void Activated(bool val) {activated=val;}
    void ValidUntil(const Phase &val) {validUntil=val;}

    bool Activated()const {return activated;}
    Phase ValidUntil()const {return validUntil;}
    BYTE Mode()const {return mode;}
    BYTE Color(int i)const {return colors[i];}

    bool operator!=(const GaColors &rhs) const;
    bool operator==(const GaColors &rhs) const;

private:
    RAW8 colors[16];
    RAW8 mode;
    Phase validUntil;
    bool activated;
};

/// store the state of border register for part of a frame
class GaBorder {
public:
    GaBorder();
    void SetVal(BYTE color);

    void Activated(bool val) {activated=val;}
    void ValidUntil(const Phase &val) {validUntil=val;}

    bool Activated()const {return activated;}
    Phase ValidUntil()const {return validUntil;}
    BYTE Color()const {return color;}

    bool operator!=(const GaBorder &rhs)const;
    bool operator==(const GaBorder &rhs)const;

private:
    Phase validUntil;
    RAW8 color;
    bool activated;
};

/// store a line in the frame
class CpcLine {
public:
    CpcLine();

    void Start(Phase phase, RAW32 *&pt, const GaColors &colors, const GaBorder &border);
    void Done(Phase phase, RAW32 *pt, const GaColors &colors, const GaBorder &border, bool changed, bool hScroll_);

    Phase StartPhase()const {return startPhase;}
    Phase EndPhase()const {return endPhase;}
    const RAW32 *Data() const { return data.data(); }
    const RAW32 *Eol()const {return eol;}
    bool HScroll()const {return hScroll;}

    bool UnChanged()const {return unchanged;}

private:
    Phase startPhase=0,endPhase=0;
    GaColors lastColors;
    const GaColors *startColors=0;
    GaBorder lastBorder;
    const GaBorder *startBorder=0;
    bool unchanged=false;
    bool hScroll=false;
    RAW32 *eol=0;
    std::vector<RAW32> data; // line data. starts at the tick after the vduHSync triggered
    static const int guard;

    CpcLine &operator=(const CpcLine &rhs);
    CpcLine(const CpcLine &rhs);
};

/// store a full Frame
class CpcFrame {
 public:

    CpcFrame();
    ~CpcFrame();

    void FrameStart(Phase phase);
    void LineStart(int lineNo, const Phase &phase, RAW32 *&pt) {
        lines.at(lineNo).Start(phase,pt,CrtRenderColors(),CrtRenderBorder());
    }
    void LineDone(int lineNo, const Phase &phase, RAW32 *pt, bool changed, bool hScroll) {
        lines.at(lineNo).Done(phase,pt,CrtRenderColors(),CrtRenderBorder(),changed,hScroll);
    }
    void FrameDone(int lastLine);

    void NewColors(Phase phase, BYTE mode, const BYTE *colors);
    void NewBorder(Phase phase, BYTE border);

    bool Rendered()const {return rendered;}
    int LastLine()const {return lastLine;}

    Phase LineStartPhase(int row) const {return lines.at(row).StartPhase(); }
    Phase LineEndPhase(int row) const {return lines.at(row).EndPhase(); }
    bool LineUnchanged(int row) const {return lines.at(row).UnChanged(); }
    const RAW32 *LineData(int lineNo) const {return lines.at(lineNo).Data(); }
    const RAW32 *LineEol(int row) const {return lines.at(row).Eol(); }
    bool LineHScroll(int row)const {return lines.at(row).HScroll();}

    GaColors &CrtRenderColors() {return renderColors[renderColorsIdxLast];}
    GaBorder &CrtRenderBorder() {return renderBorder[renderBorderIdxLast];}
    GaColors *RenderColors() {return renderColors.data();}
    GaBorder *RenderBorder() { return renderBorder.data(); }

    // data to fill the buffer with
    static const unsigned long FlagBorder;
    static const unsigned long FlagHSync;
    static const unsigned long FlagVSync;

private:
    static const int guard;
    std::vector<GaColors> renderColors;
    int renderColorsIdxLast;
    std::vector<GaBorder> renderBorder;
    int renderBorderIdxLast;
    int lastLine;
    std::vector<CpcLine> lines;
    bool rendered;

    CpcFrame &operator=(const CpcFrame &rhs);
    CpcFrame(const CpcFrame &rhs);
};
