#pragma once
/// \file cpc_state.h

#include "cpc_base.h"
#include "cpcphase.h"
#include "cpctypes.h"
#include "fdc.h"

#include "infra/struct_with_features.h"

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/version.hpp>

#include <array>

#define NVP(x) BOOST_SERIALIZATION_NVP(x)

// Function to convert byte vector to Base64 string
std::string to_base64(const std::vector<uint8_t> &data);

// Function to convert Base64 string to byte vector
std::vector<uint8_t> from_base64(const std::string &base64_str);

#define CPC_STATE_STRUCT(name, fields) \
    STRUCT_WITH_FEATURES(name, (SWF_FIELDS) (SWF_CTOR) (SWF_CTOR_PARAMS) (SWF_SERIALIZATION), fields)
#define CPC_STATE_STRUCT_VERSIONED(name, fields) \
    STRUCT_WITH_FEATURES(name, (SWF_FIELDS) (SWF_CTOR) (SWF_CTOR_PARAMS) (SWF_SERIALIZATION_VERSIONED), fields)

/// store a complete Z80 CPU state (for monitor, snapshot etc.)
SWF_SERIALIZATION_VERSION_INFO(CpuState, 1);
CPC_STATE_STRUCT_VERSIONED(CpuState,
    (UINT16, af)(UINT16, bc)(UINT16, de)(UINT16, hl)(UINT16, af2)(UINT16, bc2)(UINT16, de2)(UINT16, hl2)(
        UINT16, ix)(UINT16, iy)(UINT16, pc)(UINT16, sp)(UINT16, memReg)(UINT8, i)(UINT8, r)(UINT8, im)(
        UINT8, iff1)(UINT8, iff2)(UINT8, it)(UINT8, nmi));

template<typename Archive>
void CpuState::DeserializeBack(Archive &ar, int version, CpuState &o)
{
    switch (version) {
    case 0: // version 0 didn't have memReg
        ar &NVP2(af) & NVP2(bc) & NVP2(de) & NVP2(hl) & NVP2(af2) & NVP2(bc2) & NVP2(de2) & NVP2(hl2)
            & NVP2(ix) & NVP2(iy) & NVP2(pc) & NVP2(sp) & NVP2(i) & NVP2(r) & NVP2(im) & NVP2(iff1)
            & NVP2(iff2) & NVP2(it) & NVP2(nmi);
        memReg = 0;
        break;
    }
}

typedef std::array<BYTE, 16> Bytes16;

class MemBlock {
public:
    MemBlock()
        : data(16384) {}
    const BYTE *Pt() const { return data.data(); }
    BYTE *Pt() { return data.data(); }

private:
    std::vector<BYTE> data;
    friend class boost::serialization::access;

    // Custom serialization
    template<class Archive>
    void save(Archive &ar, const unsigned int version) const
    {
        std::string base64_str = to_base64(data);
        ar &BOOST_SERIALIZATION_NVP(base64_str);
    }

    template<class Archive>
    void load(Archive &ar, const unsigned int version)
    {
        std::string base64_str;
        ar &BOOST_SERIALIZATION_NVP(base64_str);
        data = from_base64(base64_str);
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

/// Store the Gate Array state (for monitor, snapshot etc.)
CPC_STATE_STRUCT(GaState,
    (BYTE, colIdx)(Bytes16, col)(BYTE, border)(BYTE, ramSelection)(BYTE, upperRom)(BYTE, mf)(int, vSync)(int, vSyncArmed)(
        int, vSyncCnt)(int, intReq)(int, vduRow)(int, hCnt)(std::vector<MemBlock>, ram)(std::vector<MemBlock>, rom));

/// Store the CRTC state (for monitor, snapshot etc.)
CPC_STATE_STRUCT(CrtcState, (BYTE, addressRegister)(Bytes16, r)(BYTE, model));

/// Store the PIO state (for monitor, snapshot etc.)
SWF_SERIALIZATION_VERSION_INFO(PioState, 1);
CPC_STATE_STRUCT_VERSIONED(PioState, (BYTE, portA)(BYTE, portB)(BYTE, portC)(BYTE, regCtrl)(BYTE, printerData)(BYTE, regA)(BYTE, regB)(BYTE, regC));
template<typename Archive>
void PioState::DeserializeBack(Archive &ar, int version, PioState &o)
{
    switch (version) {
    case 0: // version 0 didn't have regA, regB, regC
        ar &NVP2(portA) & NVP2(portB) & NVP2(portC) & NVP2(regCtrl) & NVP2(printerData);
        regA= regB= regC = 0;
        break;
    }
}

/// Store the PSG state (for monitor, snapshot etc.)
CPC_STATE_STRUCT(PsgState, (BYTE, crtAdd)(Bytes16, reg));

typedef std::array<BYTE, 4> Bytes4;

/// Store the FDC state (for monitor, snapshot etc.)
SWF_SERIALIZATION_VERSION_INFO(FdcState, 1);
CPC_STATE_STRUCT_VERSIONED(FdcState, (bool, motorOn)(Bytes4, currentTrack)(Fdc::Drives, drives));
template<typename Archive>
void FdcState::DeserializeBack(Archive &ar, int version, FdcState &o)
{
    switch (version) {
    case 0: // version 0 had an empty state
        bool motorOn;
        Bytes4 currentTrack;
        ar &NVP(motorOn) & NVP(currentTrack);
        break;
    }
}

/// Store the VDU state (for monitor, snapshot etc.)
CPC_STATE_STRUCT(VduState,
    // Hi-res registers
    //-----------------
    //  Horizontal control
    (uint16_t, hCount)(uint16_t, hReset)(uint16_t, hStart)(uint16_t, hEnd)(uint16_t, hDelay)(
        uint16_t, crtcHSync)(uint16_t, hSyncCnt)(uint16_t, hSyncShort)(uint16_t, r8lock)
    //  Vertical control
    (uint16_t, vAdj)(uint16_t, vAdjCnt)(uint16_t, vAdjReset)(uint16_t, crtcVSync)(uint16_t, vSyncCnt)(
        uint16_t, vSyncReset)(uint16_t, vCount)(uint16_t, vReset)(uint16_t, sCount)(uint16_t, sReset)
    //  Display control
    (WORD, memAdd)(WORD, maReg)(int, dispEn)(int, vSyncBlank)
    // Ga
    (int, gaHSync)
    // VDU control
    (int, vduRow)(int, vduCol)(bool, vduHSync)(bool, vduVSync)(int, vduVSyncCnt));

CPC_STATE_STRUCT(ConfigState, (CpcBase::CpcModelType, model));

/// Store the full CPC state (for monitor, snapshot etc.)
CPC_STATE_STRUCT(CpcState,
    (CpuState, cpu)(GaState, ga)(CrtcState, crtc)(PioState, pio)(PsgState, psg)(FdcState, fdc)(VduState, vdu)(
        ConfigState, config)(WORD, watchAddr)(Phase, cpcPhase)(Phase, cpuPhase)(Phase, psgPhase));
