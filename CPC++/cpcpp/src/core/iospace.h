#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file iospace.h - I/O space management unit emulation
#include "infra/error.h"
#include "periph.h"


/// I/O space management unit emulation.
/// IO space of the CPC:
/// \code
/// 15   11   7    3
/// |    |    |    |
/// 0... .... .... ....      7FFF-> Ga
/// .0.. ...X .... ....          -> Crtc
/// .0.. ..00 .... ....      BCFF  -> Adress Register Write
/// .0.. ..01 .... ....      BDFF  -> Data Register Write
/// .0.. ..10 .... ....      BEFF  -> Status Read
/// .0.. ..11 .... ....      BFFF  -> Data Register Read
/// ..0. .... .... ....      DFFF-> RomSel (Ga)
/// ...0 .... .... ....      EFFF-> Centro
/// .... 0.XX .... ....          -> Pio
/// .... 0.00 .... ....      F4FF  -> reg A
/// .... 0.01 .... ....      F5FF  -> reg B
/// .... 0.10 .... ....      F6FF  -> reg C
/// .... 0.11 .... ....      F7FF  -> Ctrl
/// .... .0XX XXXX XXXX          -> Ext.
/// .... .0.X 0... ....            -> Disk
/// .... .0.0 0... ....      FA7F    -> Motor
/// .... .0.1 0... ....      FB7F    -> Fdc
/// .... .0.. .0.. ....      FBBF  -> Reserved
/// .... .0XX ..0. ....            -> Communication
/// .... .010 ..0. ....      FADF    -> Sio
/// .... .011 ..0. ....      FBDF    -> Brg
/// .... .0.. 111. ....      FBFF  -> User extensions
/// 1111 1000 1111 1111      F8FF-> Reset extensions
/// 1111 1110 1110 1000      FEE8-> Multiface
/// \endcode
class IoSpace {
public:
    IoSpace(Peripheral &crtc, Peripheral &pio, Peripheral &centro, Peripheral &fdc, Peripheral &ga);
    virtual ~IoSpace() {}

    BYTE Get(const Phase &phase, WORD addr);
    void Put(const Phase &phase, WORD addr, BYTE val);
    void ResetExt(Phase phase);
protected:
    Peripheral nullDev;
    Peripheral &crtc;
    Peripheral &pio;
    Peripheral &centro;
    Peripheral &fdc;
    Peripheral &ga;
    //  Peripheral &sio;
    //  Peripheral &brg;
    //  Peripheral &multi;
private:
    IoSpace(const IoSpace &)= delete;
    IoSpace &operator=(const IoSpace&)= delete;
};
