#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// roms.h public access to the internalized ROMs

extern const unsigned char ROMLO464[16384];
extern const unsigned char ROMHI464[16384];
extern const unsigned char ROMLO664[16384];
extern const unsigned char ROMHI664[16384];
extern const unsigned char ROMLO6128[16384];
extern const unsigned char ROMHI6128[16384];
extern const unsigned char ROMXT07[16384];
