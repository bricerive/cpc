// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Centro.cc - Emulation of the Centronics port
#include "centro.h"
#include "infra/error.h"
#include "infra/log.h"

// "Centronics port" as I named it here, or more accurately "Printer Port"
// output is a dedicated 74LS273 (8-Bit Register with Clear).
// - Bit 7 is inverted by one of the NAND gates of IC110 to become !STROBE signal
// - !MR (Master Reset) is connected to !RESET and !MR sets all output to 0
// input (BUSY) goes to bit B7 of 8255

Centro::Centro(CpcConsole &console)
: console(console)
{}

void Centro::PrinterOutput(PrinterMode val)
{
    printerOutput=val;
}

void Centro::Reset()
{
    crtVal = 0;
    busy = false;
}

bool Centro::GetBusy()const {return busy;}

void Centro::Put(Phase /*phase*/, WORD /*addr*/, BYTE val)
{
    switch (printerOutput) {
        case PrinterMode::PrinterNone:
        break;
    case PrinterMode::PrinterFile:
        if (!(crtVal&0x80) && (val&0x80))
            console.PutPrinter(val & 0x7F);
        break;
    case PrinterMode::PrinterDigiblaster:
        // note that digiblaster values are pulled by the Psg, not pushed.
        break;
    default:
        break;
    }
    crtVal = val;
}

BYTE Centro::Get(Phase /*phase*/, WORD /*addr*/)
{
    LOG_STATUS("[Centro] reading from centro");
    return 0xFF;
}
