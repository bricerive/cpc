#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Centro.h - Emulation of the Centronics port
#include "periph.h"
#include "cpcconsole.h"
#include "cpc_base.h"

/// Emulation of the Centronics port
class Centro: public Peripheral {
 public:
    typedef CpcBase::PrinterMode PrinterMode;

    Centro(CpcConsole &console);

    // Peripheral methods
    void Reset();
    void Put(Phase phase, WORD addr, BYTE val);
    BYTE Get(Phase phase, WORD addr);

    // Centronics interface
    bool GetBusy()const;
    BYTE GetOutput()const {return crtVal^0x80;}

    // Options
    void PrinterOutput(PrinterMode val);

 private:
    Centro(const Centro&);
    Centro &operator=(const Centro &);
    BYTE crtVal=0;
    bool busy=false;
    PrinterMode printerOutput=PrinterMode::PrinterNone;
    CpcConsole &console;
};
