// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Drive.cc - Floppy drive emulation

// * Proper identification of Extended disk image header (check for
// "EXTENDED" only)
// * Removed check for disk images and track header. Not needed. (Now
// loads disk images created with Make_Dsk. Yes, make disk didnt insert
// the ascii values for /r/n it actually put the text for /r/n... ooops.
#include "drive.h"
#include "cpcfile.h"
#include "infra/error.h"
#include "infra/log.h"
#include <stdio.h>
#include <string.h>

// subset of Fdc state flags needed here (duplicate from fdc.cpp)
static const BYTE St1EndOfCylinder=0x80;
static const BYTE St1DataError=0x20;
static const BYTE St2None=0x00;
static const BYTE St2MissingAddressMarkInDataField=0x01;
static const BYTE St2DataErrorInDataField=0x20;

using std::string;

void Drive::Motor(bool on)
{
    motor = on;
}

bool Drive::IsReady(Phase phase)const
{
    return floppy && motor;
}

void Drive::LoadDisk(const CpcFile &file, bool protect)
{
    floppy.reset();
    try {
        floppy = std::make_unique<Floppy>();
        floppy->Load(file, protect);
    } catch (const infra::Err &err) {
        floppy.reset();
        throw err;
    }
}

void Drive::SaveDisk(CpcFile &file)
{
    ASSERT_THROW(floppy);
    floppy->Save(file);
}

void Drive::EjectDisk()
{
    floppy.reset();
}

void Drive::FlipDisk()
{
    ASSERT_THROW(floppy);
    floppy->Flip();
}

void Drive::GetDir(std::vector<std::string> &dir, bool &canCpm)const
{
    ASSERT_THROW(floppy);
    floppy->GetDir(dir, canCpm);
}

class Track *Drive::CrtTrack()
{
    ASSERT_THROW(floppy);
    return floppy->GetTrack(crtHead,crtTrack);
}

Sector *Drive::CrtSect()
{
    Track *track=CrtTrack();
    if (!track) return 0;
    return track->CrtSect();
}

Sector *Track::CrtSect()
{
    if (NbSect()<=crtSect) return 0;
    return &Sect(crtSect);
}

void Track::AdvanceCrtSect()
{
    if (++crtSect >= NbSect())
        crtSect=0;
}

bool Drive::IsProtected()const
{
    return floppy && floppy->IsProtected();
}

bool Drive::DoubleSidedFloppy()const
{
    return floppy && floppy->IsDoubleSided();
}

void Drive::Step(Phase phase, int nb, int &trk0, int &dt)
{
    int prevTrack = crtTrack;
    if (crtTrack+nb <= 0) {
        crtTrack=0;
        trk0=1;
    } else {
        crtTrack += nb;
        trk0=0;
    }
    dt = abs(crtTrack-prevTrack);
}

// Find a sector on the current track
// Returns an error if the track is not formatted
// Returns NULL sect if the sector is not found
// modifies delayBytes before throwing
bool Drive::FindSect(int head, const SectId &id, bool erased, bool skip, Sector *&sect, int &delay)
{
    crtHead = head;
    Track *track = CrtTrack();
    delay=10;
    ASSERT_THROW(track && track->Formatted())(boost::format("Unformatted track [%02X-%02X-%02X-%02X]")%id.cylinder%id.head%id.id%id.size);
    return track->FindSect(id, erased, skip, sect, delay);
}

// Find a sector on the current track
// Returns an error if the track is not formatted
// Returns NULL sect if the sector is not found
// modifies delayBytes before throwing
bool Track::FindSect(const SectId &id, bool erased, bool skip, Sector *&sect, int &delay)
{
    int idxCount=0;
    delay=0;
    
    do {
#if 1
        bool eot;
        int nextSectDelay;
        NextSect(sect, eot, nextSectDelay);
        if (eot)
            idxCount++;
        delay += nextSectDelay;
#else
        
        if (++crtSect >= NbSect())
        {
            idxCount++;
        }
        sect = CrtSect();
#endif
        if (sect->IsSameId(id))
        {
            //          if (!(!erased && skip && (sect->st2&0x40)) &&
            //              !(erased && skip && !(sect->st2&0x40)))
            
            //Mathematiques 3éme
            // un missing DAM sur le secteur &63 de la piste 40
            if (sect->St2()&St2MissingAddressMarkInDataField)
                return false;
            return true;
        }
    } while (idxCount != 2); // if we see the index twice, the search fails
    //crtSect = CrtTrack()->NbSect()-1;
    sect=0;
    return false;
}

void Track::FirstSect(Sector *&sect, int &delay)
{
    crtSect = 0;
    sect = CrtSect();
}

void Track::NextSect(Sector *&sect, bool &eot, int &delay)
{
    // La Chose de Grotemburg wants this here...
    // Le Necromancien wants this here...
    AdvanceCrtSect();
    sect = CrtSect();
    // Here, I use storedSize(). It is wrong if there is some gap data, but it is
    // even worst if we use logicalSize() as this comes from sect ID which can be anything
    ///\todo use Simon's sector offset info
    if (crtSect==0)
    {
        eot = true;
        // first sector of the track (after index)
        
        //---Track prelude
        //  80  4E  Gap 4A
        //  12  00  Sync
        //  03  1A  Index AM
        //  01  FC
        //  50  4E  Gap 1
        //---Sector intro
        //  12  00  Sync
        //  03  1A  ID AM
        //  01  FE
        //  04  XX  ID Field
        //  02  XX  IDF CRC
        
        // the delay is critical for "La Chose..." which uses it with ReadID
        delay = 80+ 12+ 4+ 50+ 12+ 4+ 4+ 2;
        if (NbSect()) delay += Sect(NbSect()-1).storedSize();
        delay += 354;
        //delay = 210 + Gap3() + Sect(NbSect()-1).storedSize();
    } else {
        eot=false;
        //---End of Previous Sector
        //  22  4D  Gap 2
        //  12  00  Sync
        //  03  1A  Data AM
        //  01  FB
        //
        //  xx  xx ... data Field
        //
        // Intersect data
        //  02  XX  CRC
        //  nn  4E  Gap 3 (82 by default)
        //---Sector intro
        //  12  00  Sync
        //  03  1A  ID AM
        //  01  FE
        //  04  XX  ID Field
        //  02  XX  CRC
        delay = 22+ 12+ 4+ Sect(crtSect-1).storedSize() + 2+ Gap3()+ 12+ 4+ 4+ 2 ;
        //delay = 62 + Gap3() + Sect(crtSect-1).storedSize();
    }
}

void Drive::FirstSect(int head, Sector *&sect, int &delay)
{
    delay=10;
    crtHead = head;
    Track *track = CrtTrack();
    
    ASSERT_THROW(track && track->Formatted())(boost::format("FirstSect: no data on track %d")%crtTrack);
    track->FirstSect(sect, delay);
}

void Drive::NextSect(int head, Sector *&sect, bool &eot, int &delay)
{
    delay=0;
    crtHead = head;
    Track *track = CrtTrack();
    eot=0;
    ASSERT_THROW(track && track->Formatted());
    track->NextSect(sect, eot, delay);
}

bool Drive::NextSectId(int head, bool erased, bool skip, Sector *&sect, int &delay)
{
    delay=10;
    crtHead = head;
    Track *track = CrtTrack();
    ASSERT_THROW(track && track->Formatted());
    return track->NextSectId(erased, skip, sect, delay);
}

bool Track::NextSectId(bool erased, bool skip, Sector *&sect, int &delay)
{
    int idxCount=0;
    delay=10;
    sect = CrtSect();
    int searchId = sect->Id()+1;
    do {
        AdvanceCrtSect();
        if (crtSect==0)
            idxCount++;
        sect = CrtSect();
        if (sect->Id() == searchId)
        {
            if ( !(!erased && skip && sect->CtrlMark()) &&
                !(erased && skip && !sect->CtrlMark()))
                return false;
        }
    } while (idxCount!=2);
    LOG_STATUS("NextSectId: no sector %02X on track",searchId);
    return true;
}

void Drive::FormatTrack(int head, BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    crtHead = head;
    ASSERT_THROW(floppy);
    return floppy->FormatTrack(crtHead,crtTrack,fmtSize,fmtNb,fmtGap,fmtData);
}

void Drive::FormatSect(int head, int sect, const SectId &id, BYTE st1, BYTE st2)
{
    crtHead = head;
    ASSERT_THROW(floppy && CrtTrack());
    CrtTrack()->FormatSect(sect,id,st1,st2);
}

//-----------------------------------
// Floppy disk object
//

void Floppy::Flip()
{
    ASSERT_THROW(nbSides==2);
    flipped ^= 1;
}

Track * Floppy::GetTrack(int side, int track)
{
    if (side>=nbSides) side=nbSides-1;
    try {
        return tracks.at(track*nbSides+(side^flipped)).get();
    } catch (...) {
        return nullptr;
    }
}

void Floppy::FormatTrack(int /*side*/, int track, BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    tracks.resize(track+1);
    tracks[track] = std::make_unique<Track>(0,0,0,0);
    return tracks[track]->Format(fmtSize,fmtNb,fmtGap,fmtData);
}

void Floppy::Load(const CpcFile &file, bool protect)
{
    BYTE temp[8192];
    BYTE * const buf=temp;
    
    dskFormat = UNKNOWN;
    writeProtect = protect;
    flipped=0;
    
    // Get header
    file.Read(buf,256);
    // Identify format
    SetFormat(buf);
    file.Seek(-256);
    
    switch (dskFormat) {
        case DIF0: case DIF1:
            LoadDIF(file);
            break;
        case DIF2:
            LoadDIF2(file);
            break;
        case DSK:
            LoadDSK(file);
            break;
        case EXTENDED:
            LoadEXTDSK(file);
            break;
        case UNKNOWN:
        default:
            ERR_THROW("Unknown format");
    }
}

void Floppy::SetFormat(const BYTE *buf)
{
    if (!strncmp(reinterpret_cast<const char *>(buf), "MV - CPC", 8)) {
        LOG_STATUS("DSK format");
        dskFormat = DSK;
    } else if (!strncmp(reinterpret_cast<const char *>(buf), "EXTENDED", 8)) {
        LOG_STATUS("EXTENDED DSK format");
        dskFormat = EXTENDED;
    } else if (*buf == 0 && *(buf+1)<=80) {
        LOG_STATUS("DIF0 format");
        dskFormat = DIF0;
    } else if (*buf == 1 && *(buf+1)<=80) {
        LOG_STATUS("DIF1 format");
        dskFormat = DIF1;
    } else if (*buf == 2 && *(buf+1)<=80) {
        LOG_STATUS("DIF2 format");
        dskFormat = DIF2;
    } else
        ERR_THROW("Unknown format");
}

void Floppy::LoadDIF(const CpcFile &file)
{
    BYTE buf[2048];
    file.Read(buf, 2048);
    const BYTE *pt = buf+1;
    int nbTracks = *pt++;
    ASSERT_THROW(nbTracks)("track number = 0!!!");
    nbSides = 1;
    tracks.clear();
    tracks.resize(nbTracks);
    for (int i=0; i<nbTracks; i++) {
        tracks[i] = std::make_unique<Track>(*pt++,0,0,0);
        Track &track = *tracks[i];

        for (int j = 0; j < track.NbSect(); j++) {
            SectId id;
            id.cylinder = *pt++;
            id.head = *pt++;
            id.id = *pt++;
            id.size = *pt++;
            track.LoadSect(j, id, file, St1EndOfCylinder, St2None);
        }
    }
}

void Floppy::LoadDIF2(const CpcFile &file)
{
    BYTE buf[8192];
    file.Read(buf, 8192);
    const BYTE *pt = buf+1;
    int nbTracks = *pt++;
    ASSERT_THROW(nbTracks)("track number = 0!!!");
    nbSides = 1;
    tracks.clear();
    tracks.resize(nbTracks);
    for (int i=0; i<nbTracks; i++) {
        tracks[i] = std::make_unique<Track>(*pt++,0,0,0);
        Track &track = *tracks[i];
        for (int j = 0; j < track.NbSect(); j++) {
            SectId id;
            id.cylinder = *pt++;
            id.head = *pt++;
            id.id = *pt++;
            id.size = *pt++;
            pt++;
            BYTE st1 = *pt++;
            BYTE st2 = *pt++;
            BYTE cptVal = *pt++;
            if (st2 & 0x80) {
                st2 = st2&0x7F;
                track.LoadSect(j, id, cptVal, st1, st2);
            } else
                track.LoadSect(j, id, file, st1, st2);
        }
    }
}

struct DskHdr
{
    char idDsk[34];
    char idCreator[14];
    unsigned char trackCount;
    unsigned char sideCount;
    unsigned char trackLength[2]; // unused for EDSK
    const BYTE *TrackSizeTable()const {return trackLength+2;}
};

struct TrackHdr
{
    char idTrack[12];
    char idFree[4];
    unsigned char trackNumber;
    unsigned char sideNumber;
    unsigned char unused[2];
    unsigned char sectorSize;
    unsigned char sectorCount;
    unsigned char gap3length;
    unsigned char fillerByte;
};
struct SectHdr
{
    unsigned char C;
    unsigned char H;
    unsigned char R;
    unsigned char N;
    unsigned char st1;
    unsigned char st2;
    unsigned char dataLength[2];
};

void Floppy::LoadDSK(const CpcFile &file)
{
    BYTE buf[256];
    file.Read(buf,256);
    
    DskHdr &dskHdr(*reinterpret_cast<DskHdr *>(buf));
    
    int nbTracks = dskHdr.trackCount;
    ASSERT_THROW(nbTracks)("track number = 0!!!");
    if (nbTracks>80) LOG_STATUS("DSK: Tracks: %d",nbTracks);

    nbSides = dskHdr.sideCount;
    ASSERT_THROW(nbSides==1 || nbSides==2)(boost::format("Invalid number of sides (%d)...Disk Image Could be dammaged.")%nbSides);
    if (nbSides!=1) LOG_STATUS("DSK: Sides: %d",nbSides);

    tracks.clear();
    tracks.resize(nbTracks*nbSides);
    
    int trSize = dskHdr.trackLength[0] + 256*dskHdr.trackLength[1];
    ASSERT_THROW(trSize>=1 && trSize<=8192)(boost::format("Invalid track size (%d)...Disk Image Could be dammaged.")%trSize);
    
    for (int i=0; i<nbTracks; i++) {
        for (int j=0; j<nbSides; j++) {
            // Get track header
            
            // Normally we need to fseek, but BTL etc. have it wrong
            // Try without fseek first, then try with fseek
            // This will fail on DSKs without the proper track header
            // AND with incorrect handling of trSize
            file.Read(buf, 256);
            if (strncmp(reinterpret_cast<char *>(buf),"Track-Info\r\n",12)) {
                file.Reset();
                file.Seek(trSize*(i*nbSides+j)+0x100);
                file.Read(buf,256);
            }
            TrackHdr &trackHdr(*reinterpret_cast<TrackHdr *>(buf));
            
            // Sanity checks
            if (strncmp(trackHdr.idTrack,"Track-Info\r\n",12))
                LOG_STATUS("DSK[%02X] bad track header",i);
            if (trackHdr.trackNumber!=i)
                LOG_STATUS("DSK[%02X] track number mismatch: %d ~ %d",i,i,trackHdr.trackNumber);
            if (trackHdr.sideNumber!=j)
                LOG_STATUS("DSK[%02X] side number mismatch: %d ~ %d",i,j,trackHdr.sideNumber);
            if (!trackHdr.sectorCount)
                LOG_STATUS("DSK[%02X] track not formatted",i);
            
            // Create track
            tracks[i*nbSides+j] =
            std::make_unique<Track>(trackHdr.sectorCount,trackHdr.sectorSize,trackHdr.gap3length,trackHdr.fillerByte);
            Track &track = *tracks[i * nbSides + j];

            for (int k = 0; k < track.NbSect(); k++) {
                SectHdr &sectHdr(*reinterpret_cast<SectHdr *>(buf+sizeof(TrackHdr)+k*sizeof(SectHdr)));
                
                // Sanity checks
                if (sectHdr.N==0 || sectHdr.N>5)
                    LOG_STATUS("EDSK[%02X/%02X] size= %02X",i,k,sectHdr.N);
                if (sectHdr.st1&0x37)
                    LOG_STATUS("EDSK[%02X/%02X] st1= %02X",i,k,sectHdr.st1);
                if (sectHdr.st2&0x7F)
                    LOG_STATUS("EDSK[%02X/%02X] st2= %02X",i,k,sectHdr.st2);
                
                // Create sector
                SectId id;
                id.cylinder = sectHdr.C;
                id.head = sectHdr.H;
                id.id = sectHdr.R;
                id.size = sectHdr.N;
                track.LoadSect(k, id, file, sectHdr.st1, sectHdr.st2);
                const Sector *sector = &track.Sect(k);

                // Adjust position
                int offset = track.SectNBytes() - sector->storedSize();
                if (offset)
                    file.Seek(offset);
            }
        }
    }
}

void Floppy::LoadEXTDSK(const CpcFile &file)
{
    BYTE diskInformationBlock[256];
    file.Read(diskInformationBlock,256);
    DskHdr &dskHdr(*reinterpret_cast<DskHdr *>(diskInformationBlock));
    
    int nbTracks = dskHdr.trackCount;
    ASSERT_THROW(nbTracks)("track number = 0!!!");
    nbSides = dskHdr.sideCount;
    if (nbSides!=1)
        LOG_STATUS("EDSK: sides=%d",nbSides);
    tracks.clear();
    tracks.resize(nbTracks*nbSides);
    const BYTE *trackSizeP = dskHdr.TrackSizeTable();
    
    BYTE trackInformationBlock[512];
    for (int i=0; i<nbTracks; i++) {
        for (int j=0; j<nbSides; j++) {
            if (*trackSizeP) {
                // Get track header
                file.Read(trackInformationBlock, 256);
                TrackHdr &trackHdr(*reinterpret_cast<TrackHdr *>(trackInformationBlock));
                
                // Create track
                tracks[i*nbSides+j] =
                std::make_unique<Track>(trackHdr.sectorCount,trackHdr.sectorSize,trackHdr.gap3length,trackHdr.fillerByte);
                Track &track = *tracks[i * nbSides + j];
                int trSize = (*trackSizeP++) * 256;
                int offset = 256;
                
                // Sanity checks
                if (strncmp(trackHdr.idTrack,"Track-Info\r\n",13))
                    LOG_STATUS("EDSK[%02X] bad track header",i);
                if (trackHdr.trackNumber!=i)
                    LOG_STATUS("EDSK[%02X] track number mismatch: %d ~ %d",i,trackHdr.trackNumber);
                if (trackHdr.sideNumber!=j)
                    LOG_STATUS("EDSK[%02X] side number mismatch: %d ~ %d",i,j,trackHdr.sideNumber);
                if (!trackHdr.sectorCount)
                    LOG_STATUS("EDSK[%02X] empty track",i);
                if (trackHdr.sectorCount>29) {
                    // in case there is more than 29 sectors in the track, the header is 512 bytes instead of 256
                    // (Puffy's Saga - Terres et Conquérants)
                    LOG_STATUS("EDSK[%02X] extended Sector information list (%d sectors)",i,trackHdr.sectorCount);
                    file.Read(trackInformationBlock+256, 256);
                    offset=512;
                }
                
                for (int k=0; k<track.NbSect(); k++) {
                    // get sector header
                    SectHdr &sectHdr(*reinterpret_cast<SectHdr *>(trackInformationBlock+sizeof(TrackHdr)+k*sizeof(SectHdr)));
                    
                    // Sanity checks
                    if (sectHdr.N==0 || sectHdr.N>5)
                        LOG_STATUS("EDSK[%02X/%02X] size= %02X",i,k,sectHdr.N);
                    if (sectHdr.st1&0x37)
                        LOG_STATUS("EDSK[%02X/%02X] st1= %02X",i,k,sectHdr.st1);
                    if (sectHdr.st2&0x7F)
                        LOG_STATUS("EDSK[%02X/%02X] st2= %02X",i,k,sectHdr.st2);
                    
                    SectId id;
                    id.cylinder = sectHdr.C;
                    id.head = sectHdr.H;
                    id.id = sectHdr.R;
                    id.size = sectHdr.N;
                    
                    int dataSize;
                    if (sectHdr.dataLength[1]==0xFF)
                        dataSize = sectHdr.dataLength[0];
                    else
                        dataSize = sectHdr.dataLength[0]|(sectHdr.dataLength[1]<<8);
                    if (dataSize==0 || dataSize>6000)
                        LOG_STATUS("EDSK[%02X/%02X] dataSize=%04X",i,k,dataSize);
                    
                    
                    // EDSK extensions - Simon
                    // Storing Multiple Versions of Weak/Random Sectors.
                    // Some copy protections have what is described as 'weak/random' data. Each time the sector is
                    // read one or more bytes will change, the value may be random between consecutive reads of the same sector.
                    // To support these formats the following extension has been proposed.
                    // Where a sector has weak/random data, there are multiple copies stored.
                    // The actual sector size field in the SECTOR INFORMATION LIST describes the size of all the copies.
                    // To determine if a sector has multiple copies then compare the actual sector size field to the size defined by the N parameter.
                    // For multiple copies the actual sector size field will have a value which is a multiple of the size defined by the N parameter.
                    // The emulator should then choose which copy of the sector it should return on each read.
                    //----------
                    // I've made a change to add an extra byte (7B) to the data if
                    // it clashes with the multiple copies extension.  This extra byte should
                    // be removed on EDSK loading if it meets these conditions:
                    // 1) sector has data CRC error.
                    // 2) ((dataStored MOD sectorSize) == 1).
                    // 3) last byte of data is 7B.
                    int sectSize = Size2Bytes(id.size);
                    int loadSize=dataSize, skipSize=0;
                    int nbCopies=1;
                    if (dataSize > sectSize)
                    {
                        if (sectHdr.st2 & St2DataErrorInDataField)
                        {
                            if (dataSize%sectSize==0)
                            {
                                // Weak sector
                                nbCopies = dataSize/sectSize;
                            }
                            else if (dataSize%sectSize==1)
                            {
                                // inter-sector data that has the same size as the sector itself
                                // (need the 7B check here)
                                loadSize = dataSize-1;
                                skipSize = 1;
                            }
                        }
                    }

                    track.LoadSect(k, id, loadSize, file, nbCopies, sectHdr.st1, sectHdr.st2);
                    if (skipSize)
                        file.Seek(skipSize);
                    offset += dataSize;
                }
                file.Seek(trSize - offset);
            } else {
                LOG_STATUS("EDSK[%02X] not formated",i);
                tracks[i*nbSides+j] = std::make_unique<Track>(0,0,0,0);
                trackSizeP++;
            }
        }
    }
}

void Floppy::Save(CpcFile &file)
{
    BYTE buff[0x100];
    const Sector * sect;
    
    // Make up header
    snprintf(reinterpret_cast<char *>(buff)+0x00,256,"EXTENDED CPC DSK File\r\nDisk-Info\r\n");
    snprintf(reinterpret_cast<char *>(buff)+0x22,222,"CPC++         ");
    buff[0x30]=static_cast<BYTE>(tracks.size());
    buff[0x31]=nbSides;
    buff[0x32]=0;
    buff[0x33]=0;
    BYTE *offsets = &buff[0x34];
    for (Tracks::iterator trackI=tracks.begin(); trackI!=tracks.end(); ++trackI) { // Track offset table
        Track &track = **trackI;
        if (!track.NbSect())
            *offsets=0; // Not formatted
        else {
            BYTE maxSz=0;
            int totSz=0;
            for (int j = 0; j < track.NbSect(); j++) {
                if (maxSz < track.Sect(j).Size()) maxSz = track.Sect(j).Size();
                totSz += 0x80 << track.Sect(j).Size();
            }
            *offsets = static_cast<BYTE>(1 + ((totSz+0xFF)>>8));
            track.SectSize(maxSz); // Might not be set yet
        }
        offsets++;
    }
    // Write header
    file.Write(buff,256);
    
    // Make up track blocks
    BYTE trackIdx=0;
    for (Tracks::iterator trackI=tracks.begin(); trackI!=tracks.end(); ++trackI) { // Track offset table
        Track &track = **trackI;
        if (track.NbSect()) {
            // Make up track header
            snprintf(reinterpret_cast<char *>(buff),256,"Track-Info\r\n");
            buff[0x0D]=buff[0x0E]=buff[0x0F]=0;
            buff[0x10]=trackIdx;
            buff[0x11]=0; // Side
            buff[0x12]=buff[0x13]=0;
            buff[0x14] = track.SectSize();
            buff[0x15] = static_cast<BYTE>(track.NbSect());
            buff[0x16] = track.Gap3();
            buff[0x17] = track.Filler();
            for (int j = 0; j < track.NbSect(); j++) { // Sector info list
                sect = &track.Sect(j);
                buff[0x18+8*j]=sect->Cylinder();
                buff[0x19+8*j]=sect->Head();
                buff[0x1A+8*j]=sect->Id();
                buff[0x1B+8*j]=sect->Size();
                buff[0x1C+8*j]=sect->St1();
                buff[0x1D+8*j]=sect->St2();
                buff[0x1E +8*j]=static_cast<BYTE>(sect->storedSize()&0xFF);
                buff[0x1F+8*j]=static_cast<BYTE>(sect->storedSize()>>8);
            }
            // Write track header
            file.Write(buff,256);
            // Write sectors data
            for (int j = 0; j < track.NbSect(); j++) {
                sect = &track.Sect(j);
                file.Write(sect->Data(),sect->storedSize());
            }
        }
        trackIdx++;
    }
}

const Sector *Track::FindSect(BYTE id, BYTE size)const
{
    for (int sectorIndex=0; sectorIndex<NbSect(); sectorIndex++) {
        const Sector &crtSect(Sect(sectorIndex));
        if (crtSect.Id() == id && crtSect.Size()==size)
            return &crtSect;
    }
    return 0;
}

bool Floppy::CanCpm()const
{
    return tracks[0]->FindSect(0x41,2);
}

const Track &Floppy::DirTrack()const
{
    ASSERT_THROW(!tracks.empty());
    BYTE diskType = tracks[0]->Sect(0).Id() & 0xC0;
    switch(diskType) {
        case 0x00: // IBM
            ASSERT_THROW(tracks.size()>=1);
            return *tracks[1];
        case 0x40: // CPM
            ASSERT_THROW(tracks.size()>=2);
            return *tracks[2];
        case 0xC0: // AMSDOS
            return *tracks[0];
        default:
            ERR_THROW("Unexpected secor Id");;
    }
}

// https://www.cpcwiki.eu/index.php/Disk_structure#Directory_entries
struct DirectoryEntry {
    BYTE user;
    BYTE name[8];
    BYTE extension[3];
    BYTE extentL;
    BYTE ByteCount;
    BYTE extentH;
    BYTE RecordCount;
    BYTE blockId[16];
    bool IsSystem()const {return extension[1]&0x80;}
    bool IsValidName()const { 
        if (!IsValidStr(name,8)) return false;
        if (!IsValidStr(extension,3)) return false;
        return true;
    }
    static bool IsValidStr(const BYTE *pt, int n) {
        static const string forbidden = "<>.,;:=?*[]";
        for (int i=0; i<n; i++) {
            BYTE c = *pt++ & 0x7F;
            if (forbidden.find(c)!=string::npos) return false;
            if (c<0x20 || c>0x7E) return false;
        }
        return true;
    }
    string Name()const
    {
        string result;
        int i;
        for (i=0; i<sizeof(name) && name[i]!=' '; i++)
            result += name[i]&0x7F;
        result += '.';
        for (i=0; i<sizeof(extension) && extension[i]!=' '; i++)
            result += extension[i]&0x7F;
        return result;
    }
    uint16_t Extent()const {return (uint16_t)extentL + 256 * extentH; }
};

void Floppy::GetDir(std::vector<std::string> &dir, bool &canCpm)const
{
    dir.clear();
    canCpm=CanCpm();

    const Track &dirTrack(DirTrack());
    BYTE baseId = dirTrack.BaseIndex();

    // we start at 0xC1 or 0x41 or 0x01
    static const int nbSectorsPerDirectory=4;
    for (int sectorId=baseId+1; sectorId<=baseId+nbSectorsPerDirectory; sectorId++) {
        const Sector *sector = dirTrack.FindSect(sectorId, 2);
        if (!sector) continue;
        const DirectoryEntry *directoryEntryP = reinterpret_cast<const DirectoryEntry *>(sector->Data());
        static const int nbEntriesPerSector=16;
        for (int entryIndex=0; entryIndex<nbEntriesPerSector; entryIndex++) {
            const DirectoryEntry &entry(*directoryEntryP++);
            if (entry.user==0 && entry.Extent()==0 && !entry.IsSystem() && entry.IsValidName())
                dir.push_back(entry.Name());
        }
    }
}

Track::Track(int nbSect_, BYTE sectSize_, BYTE myGap3, BYTE filler_)
: sectSize(sectSize_), gap3(myGap3), filler(filler_), sectors(nbSect_), crtSect(0)
{}

Track::~Track()
{
    for (Sectors::iterator i=sectors.begin(); i!=sectors.end(); ++i)
        delete *i;
}

void Track::Format(BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    for (Sectors::iterator i=sectors.begin(); i!=sectors.end(); ++i)
        delete *i;
    sectors.clear();
    sectSize = fmtSize;
    gap3 = fmtGap;
    filler = fmtData;
    sectors.resize(fmtNb);
}

void Track::FormatSect(int no, const SectId &id, BYTE st1, BYTE st2)
{
    sectors[no] = new Sector(id, filler, st1, st2);
}

void Track::LoadSect(int no, const SectId &id, const CpcFile &file, BYTE st1, BYTE st2)
{
    sectors[no] = new Sector(id, file, st1, st2);
}

void Track::LoadSect(int no, const SectId &id, int fSize, const CpcFile &file, int nbCopies, BYTE st1, BYTE st2)
{
    sectors[no] = new Sector(id, fSize, file, nbCopies, st1, st2);
}

void Track::LoadSect(int no, const SectId &id, int cptVal, BYTE st1, BYTE st2)
{
    sectors[no] = new Sector(id,cptVal, st1, st2);
}

BYTE Track::BaseIndex()const {
    ASSERT_THROW(Formatted());
    return Sect(0).Id() & 0xC0;
}

Sector::Sector(const SectId &myId, int filler, BYTE st1, BYTE st2)
: id(myId), st1(st1), st2(st2), crtCopy(0), data(1), randomDataError(false)
{
    int dataSize = logicalSize();
    data[0].resize(dataSize);
    memset(&data[0][0],filler,dataSize);
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

Sector::Sector(const SectId &myId, const CpcFile &file, BYTE st1, BYTE st2)
: id(myId), st1(st1), st2(st2), crtCopy(0), data(1), randomDataError(false)
{
    int dataSize = logicalSize();
    data[0].resize(dataSize);
    file.Read(&data[0][0], dataSize);
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

Sector::Sector(const SectId &myId, int fSize, const CpcFile &file, int nbCopies, BYTE st1, BYTE st2)
: id(myId), st1(st1), st2(st2), crtCopy(0), data(nbCopies), randomDataError(nbCopies>1)
{
    int dataSize = fSize/NbCopies();
    for (int i=0; i<NbCopies(); i++)
    {
        data[i].resize(dataSize);
        if (dataSize) file.Read(&data[i][0], dataSize);
    }
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

bool Sector::IsSameId(const SectId &id_)const
{
    return id == id_;
}

BYTE Sector::Data(int idx)const
{
    // DD error protection trick:
    // This protection has a damaged sector which, when repeatedly
    // read, returns random data.
    if (idx==0 && randomDataError)
    {
        //crtCopy = rand()%nbCopies;
        // Using rand() is not very good as it does not guarantee non-repeat
        // for example it fails on Robocop
        crtCopy = (crtCopy+1)%NbCopies();
    }
    if (idx >= storedSize())
        return static_cast<BYTE>(rand()&0xFF);
    return data[crtCopy][idx];
}

void Sector::Data(int idx, BYTE val)
{
    if (idx >=storedSize())
        LOG_STATUS("Attempt to write after physical end of sector");
    else
        data[0][idx] = val;
}

