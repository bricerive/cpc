// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Peripheral.cc - Abstract class for Z80 peripherals
#include "periph.h"
#include "infra/error.h"
#include "infra/log.h"

void Peripheral::Reset()
{
    lastPhase=0;
}

void Peripheral::Put(Phase /*phase*/, WORD addr, BYTE val)
{
    LOG_STATUS("Out %02X on empty port %04X",val,addr);
}

BYTE Peripheral::Get(Phase /*phase*/, WORD addr)
{
    LOG_STATUS("In on empty port %04X",addr);
    return 0xFF;
}

CpcDuration Peripheral::GetElapsedUs(Phase crtPhase)
{
    long elapsedUs=crtPhase.ElapsedUs(lastPhase);
    lastPhase=crtPhase;
    return CpcDuration::FromUs(elapsedUs);
}
