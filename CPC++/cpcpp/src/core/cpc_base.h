#pragma once
//
//  cpc_base.h
//  CPC++
//
//  Created by Brice Rivé on 11/27/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
#include "cpctypes.h"
#include "cpcphase.h"
#include "infra/struct_with_ctor.h"
#include "infra/rich_enum.h"

class Phase;
class CpcFile;
struct CpcState;
struct VduState;
class Mmu;

struct CpcKeyboard {
    RICH_ENUM(KeyDirection, (DOWN)(UP));
    /// Tell the CPC that a key is pressed/released
    virtual void KeyChange(int keyCode, KeyDirection direction)=0;
};

struct CpcSound {
    /// Define characteristics of sound generation
    typedef BYTE *BYTE_P;
    STRUCT_WITH_CTOR(SoundInfo, (bool, canStereo)(int, precision)(int, frequencyHz)(BYTE_P, buff)(int, nbSamples));

    /// Set sound generation info
    virtual void SetSoundInfo(const SoundInfo &sndInfo)=0;

    /// Toggle sound generation
    virtual void SoundOnOff(bool on)=0;

    /// Toggle dropping of sound frames
    virtual void DropOn(bool val)=0;

    /// Toggle stereo/mono sound generation
    virtual void Stereo(bool val)=0;

    /// Control activation of each CPC sound channel
    virtual void ChannelsOnOff(int chanA, int chanB, int chanC)=0;
};

// Monitor support
struct MonitoredCpc {

    /// Serialization: get complete CPC state
    virtual void GetState(CpcState &state)const=0;

    /// Serialization: set CPC to a state
    virtual void SetState(const CpcState &state)=0;

    RICH_ENUM(TrapId,(NONE)(VDUPOS)(HRESET)(SRESET)(VRESET)(HSYNC)(VSYNC)(TICK)(WATCH));
    virtual void SetTrap(TrapId trapId)=0;
    virtual void SetWatch(WORD addr)=0;
    /// store the last click position in the cpc window for vduPos trap
    virtual void LastClick(int x, int y)=0;
    virtual int LastClickX()const=0;
    virtual int LastClickY()const=0;
    virtual BYTE MemRd8(WORD addr)const=0;
    virtual BYTE MemRdWr8(WORD addr)const=0;
    virtual UINT16 MemRd16(WORD addr)const=0;
    virtual void AddConditionCpuStep()=0;
    virtual void AddConditionCpuBreak(UINT16 address, bool persist=false)=0;
    virtual void AddConditionCpuStack(UINT16 address)=0;
    virtual void AddConditionCpuIntAck()=0;
    virtual void RemoveCpuCondition()=0;
    virtual void CpuTrace(bool on)=0;
    virtual void CrtcTrace(bool on)=0;
    virtual void TrapCrtcChanges()=0;
    virtual const Mmu &GetMmu()=0;
};

struct CpcFdc {

    RICH_ENUM(DriveId, (A)(B));

    /// Tells whether this drive is present
    virtual bool DriveEnabled(DriveId drive)const=0;

    /// Tells whether this drive contains a floppy
    virtual bool IsDriveReady(DriveId drive)const=0;

    /// Tells whether this drive's floppy is double-sided
    virtual bool DoubleSidedFloppy(DriveId drive)const=0;

    /// Tells whether this drive's floppy is write-protected
    virtual bool IsWriteProtected(DriveId drive)const=0;

    /// Get the directory content info
    virtual void GetDir(DriveId drive, std::vector<std::string> &dir, bool &canCpm)const=0;

    /// load a disk into a drive
    virtual void LoadDisk(DriveId drive, const CpcFile &file, bool protect)const=0;

    /// Save a drive's current disk
    virtual void SaveDisk(DriveId drive, CpcFile &file)const=0;

    /// Flip a drive's disk (must be double-sided)
    virtual void FlipDisk(DriveId drive)const=0;

    /// Eject a drive's disk
    virtual void EjectDisk(DriveId drive)const=0;

    /// Control real-(CPC)time emulation of FDC
    virtual void RealTime(bool val)=0;
};

/// pure abstract class that defines what a CPC looks like
/// from a CpcConsole point of view
/// A base class for a CPC so we can have a clean interface
class CpcBase: public CpcKeyboard, public CpcSound, public MonitoredCpc, public CpcFdc
{
public:
    virtual ~CpcBase() {}

    /// Reset the CPC
    virtual void Reset()=0;

    /// Run the CPC emulation for one CPC-time frame (usually 20ms)
    STRUCT_WITH_FEATURES(RunState, (SWF_FIELDS)(SWF_CTOR_PARAMS)
        , (bool, frameDone)(Phase, cpcPhase));
    virtual RunState RunSome(bool render)=0;

    /// Ask for rendering of current screen frame
    RICH_ENUM(RenderMode, (FULL)(SKIP)(SMALL));
    STRUCT_WITH_FEATURES(RenderInfo, (SWF_FIELDS)(SWF_CTOR_PARAMS),
        (CpcDib32, dib)(const RAW32 *, lut)(CpcRect, clip)(RenderMode, renderMode));
    virtual void RenderFrame(const RenderInfo &info, bool force=false)=0;

    /// Set the CPC model
    RICH_ENUM(CpcModelType, (MODEL_464)(MODEL_664)(MODEL_6128)(MODEL_6128_512K)(MODEL_6128_4M));
    virtual void CpcModel(CpcModelType val)=0;

    /// Set the ROMs
    virtual void SetRoms(const BYTE * const low, const BYTE * const roms[256])=0;

    /// Set the constructor name as per LK4
    RICH_ENUM(CpcConstructor, (Isp)(Triumph)(Saisho)(Solavox)(Awa)(Schneider)(Orion)(Amstrad));
    virtual void Constructor(CpcConstructor val)=0;

    /// Control CP/M boot jumper
    virtual void CpmBoot(bool val)=0;

    /// Set printer output mode
    RICH_ENUM(PrinterMode, (PrinterNone)(PrinterFile)(PrinterDigiblaster));
    virtual void PrinterOutput(PrinterMode val)=0;

};

