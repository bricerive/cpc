#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file drive.h - Floppy drive emulation

#include "cpctypes.h"
#include "cpcphase.h"
#include <vector>

class Floppy;
class Track;
class Sector;
class CpcFile;

/// A sector ID used by the Fdc
struct SectId {
    bool operator==(const SectId &rhs)const {return cylinder==rhs.cylinder && head==rhs.head && id==rhs.id && size==rhs.size;}
    BYTE cylinder=0;
    BYTE head=0;
    BYTE id=0;
    BYTE size=0;
    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(cylinder) & BOOST_SERIALIZATION_NVP(head) & BOOST_SERIALIZATION_NVP(id)
            & BOOST_SERIALIZATION_NVP(size);
    }
};

/// Emulate one of the floppy drives
class Drive {

public:
    Drive(){}
    // Floppy handling

    /// Load a disk into the drive.
    void LoadDisk(const CpcFile &file, bool protect);

    /// Save the current disk
    void SaveDisk(CpcFile &file);

    /// Eject the current disk (deletes whatever changes were made)
    void EjectDisk();

    /// Flip a dual sided floppy. Some DSK files contain dual sided floppy.
    /// Not a very clean design but we support it somewhat since we do the
    /// file decoding.
    void FlipDisk();

    // Drive state
    bool DoubleSidedFloppy()const;
    bool IsProtected()const;
    bool IsFull()const {return floppy!=0;}
    bool IsReady(Phase phase)const;
    bool IsTrack0()const {return (crtTrack==0)?1:0;}
    bool IsTwoSides()const {return (nbHeads==2)?1:0;}

    void Motor(bool on);

    // Head movement
    void Step(Phase phase, int nb, int &trk0, int &dt);

    // Sector access
    void FirstSect(int head, Sector *&sect, int &delay);
    bool FindSect(int head, const SectId &id, bool erased, bool skip, Sector *&sect, int &delay);
    bool NextSectId(int head, bool erased, bool skip, Sector *&sect, int &delay);
    void NextSect(int head, Sector *&sect, bool &eot, int &delay);
    Sector *CrtSect();

    // Formatting
    void FormatTrack(int head, BYTE size, BYTE nb, BYTE gap, BYTE data);
    void FormatSect(int head, int sect, const SectId &id, BYTE st1, BYTE st2);

    /// Get the DSK directory content for GUI.
    /// \param dir returns the directory data, in the AMSDOS format (32 bytes per entry)
    /// \param canCpm tells if the disk might be able to be booted by |CPM
    void GetDir(std::vector<std::string> &dir, bool &canCpm)const;

    Track *CrtTrack();
    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(nbHeads) & BOOST_SERIALIZATION_NVP(motor) & BOOST_SERIALIZATION_NVP(crtHead)
            & BOOST_SERIALIZATION_NVP(crtTrack) & BOOST_SERIALIZATION_NVP(floppy);
    }

private:
    Drive &operator=(const Drive&);
    Drive(const Drive&);

    int nbHeads=1;
    bool motor=false;
    int crtHead=0;
    int crtTrack=0;
    std::unique_ptr<Floppy> floppy;
};

/// Floppy emulation
class Floppy{

public:
    void Load(const CpcFile &file, bool protect);
    void Save(CpcFile &file);
    void Flip();

    bool IsProtected()const {return writeProtect;}
    bool IsDoubleSided()const {return nbSides==2;}
    int NbTracks()const {return static_cast<int>(tracks.size());}

    Track *GetTrack(int side, int track);
    void GetDir(std::vector<std::string> &dir, bool &canCpm)const;

    void FormatTrack(int side,int track,BYTE fmtSize,BYTE fmtNb,BYTE fmtGap,BYTE fmtData);
    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(writeProtect) & BOOST_SERIALIZATION_NVP(dskFormat) & BOOST_SERIALIZATION_NVP(nbSides)
            & BOOST_SERIALIZATION_NVP(flipped) & BOOST_SERIALIZATION_NVP(tracks);
    }

private:
    bool CanCpm()const;
    const Track &DirTrack()const;

    void SetFormat(const BYTE *buf);
    void LoadDIF(const CpcFile &file);
    void LoadDIF2(const CpcFile &file);
    void LoadDSK(const CpcFile &file);
    void LoadEXTDSK(const CpcFile &file);

    bool writeProtect=false;
    enum {UNKNOWN,DIF0,DIF1,DIF2,DSK,EXTENDED} dskFormat=UNKNOWN;
    BYTE nbSides=0;
    int flipped=0;
    typedef std::vector<std::unique_ptr<Track>> Tracks;
    Tracks tracks;
};

static int Size2Bytes(BYTE size) { return 128<<std::min<int>(6,size);}

/// Floppy track emulation
class Track {
public:
    Track(){}
    Track(int nbSect, BYTE sectSize, BYTE gap3, BYTE filler);
    ~Track();

    void Format(BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData);
    void FormatSect(int sect, const SectId &id, BYTE st1, BYTE st2);
    void LoadSect(int no, const SectId &id, const CpcFile &file, BYTE st1, BYTE st2);
    void LoadSect(int no, const SectId &id, int fSize, const CpcFile &file, int nbCopies, BYTE st1, BYTE st2);
    void LoadSect(int no, const SectId &id, int cptVal, BYTE st1, BYTE st2);
    Sector &Sect(int i) {return *sectors.at(i);}
    const Sector &Sect(int i)const {return *sectors.at(i);}
    int NbSect()const {return static_cast<int>(sectors.size());}
    int Formatted()const {return !sectors.empty();}
    BYTE BaseIndex()const;

    BYTE SectSize()const {return sectSize;}
    int SectNBytes()const {return Size2Bytes(sectSize);}
    void SectSize(BYTE val) {sectSize = val;}
    BYTE Gap3()const {return gap3;}
    BYTE Filler()const {return filler;}

    void AdvanceCrtSect();
    Sector *CrtSect();
    const Sector *FindSect(BYTE id, BYTE size)const;
    bool FindSect(const SectId &id, bool erased, bool skip, Sector *&sect, int &delay);
    void FirstSect(Sector *&sect, int &delay);
    void NextSect(Sector *&sect, bool &eot, int &delay);
    bool NextSectId(bool erased, bool skip, Sector *&sect, int &delay);

    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(sectSize) & BOOST_SERIALIZATION_NVP(gap3) & BOOST_SERIALIZATION_NVP(filler)
            & BOOST_SERIALIZATION_NVP(sectors) & BOOST_SERIALIZATION_NVP(crtSect);
    }

private:
    BYTE sectSize;
    BYTE gap3;
    BYTE filler;
    typedef std::vector<Sector *> Sectors;
    Sectors sectors;
    int crtSect;
};


/// Floppy sector emulation
class Sector {
public:
    Sector() {}
    Sector(const SectId &id, int filler, BYTE st1, BYTE st2);
    Sector(const SectId &id, const CpcFile &file, BYTE st1, BYTE st2);
    Sector(const SectId &id, int fSize, const CpcFile &file, int nbCopies, BYTE st1, BYTE st2);

    bool IsSameId(const SectId &id)const;
    BYTE Cylinder()const {return id.cylinder;}
    BYTE Head()const {return id.head;}
    BYTE Id()const {return id.id;}
    BYTE Size()const {return id.size;}

    int logicalSize()const {return Size2Bytes(id.size);}
    int storedSize()const {return static_cast<int>(data[0].size());}
    const BYTE *Data()const {return &data[0][0];}
    BYTE Data(int idx)const;
    void Data(int idx, BYTE val);

    void St1(BYTE val) {st1=val;}
    void St2(BYTE val) {st2=val;}
    BYTE St1()const {return st1;}
    BYTE St2()const {return st2;}
    int CtrlMark()const {return (st2&0x40) != 0;}

    int NbCopies()const {return static_cast<int>(data.size());}

    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(id) & BOOST_SERIALIZATION_NVP(st1) & BOOST_SERIALIZATION_NVP(st2)
            & BOOST_SERIALIZATION_NVP(crtCopy) & BOOST_SERIALIZATION_NVP(data)
            & BOOST_SERIALIZATION_NVP(randomDataError);
    }

private:
    SectId id;
    BYTE st1;
    BYTE st2;
    mutable int crtCopy;
    std::vector< std::vector<BYTE> > data;

    bool randomDataError;
};
