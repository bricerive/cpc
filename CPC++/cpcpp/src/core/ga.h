#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file ga.h - Gate Array emulation
#include "mmu.h"
#include "cpcconsole.h"
#include "cpcframe.h"
#include "infra/error.h"
#include "cpc_base.h"
#include "frame_renderer.h"

class Z80;
class Crtc;
class Psg;
class Pio;

class GaColors;
class GaBorder;
class CpcFrame;
struct GaState;


/// Gate Array emulation.
/// Takes care of:
/// -memory mapping (roms and rams)
/// -VDU sync
/// -VDU rendering
/// -Interrupt generation
class Ga: public Mmu {
 public:
    struct Client {
        virtual void CrtcTrace(Phase crtPhase, const char *eventStr)const=0;
    };

    Ga(Client &client, Z80 &cpu, Crtc &crtc, TrapFlag &trapFlag);
    ~Ga();

    virtual void Reset();

    virtual void Put(Phase phase, WORD addr, BYTE val);

    virtual BYTE Get(Phase phase, WORD addr);

    void SetIntReq(int val);

    virtual void IntAck(Phase phase);

    inline int HcntGet()const { return hCnt; }

    inline int VSync()const {return vSync;}

    inline int IntGet()const { return intReq; }

    static const int NB_UROM_SLOTS=256;
    void SetRoms(const BYTE * const rom, const BYTE * const roms[NB_UROM_SLOTS]);

    static const int HVIS=48;
    static const int VVIS=280;

    // CPC screen size
    static const int CPC_SCR_W=HVIS*8*2;
    static const int CPC_SCR_H=VVIS*2;

    // Buffered rendering mode
    void FrameStart(Phase phase);
    void LineStart(int lineNo, Phase phase, RAW32 *&pt);
    void LineDone(int lineNo, Phase phase, RAW32 *pt, bool changed, bool hScroll);
    void FrameDone(int lastLine);

    // VDU size constants
    static const int vduHSyncVal;
    static const int vduVSyncVal;
    static const int vduHTot;
    static const int vduVTot;
    static const int vduHOverload;
    static const int vduVOverload;
    static const int maxLinesPerFrame; // 312+32+40
    static const int maxNopPerLine; // 64+10+40
    static const int maxNopsPerFrame;

    /// This is called by the console to render the last generated frame into a pixmap
    /// lut: LUT in the specified depth (32 CPC colors + sync color)
    void RenderFrame(const CpcBase::RenderInfo &info, bool force=false);


    void GetState(GaState &state)const;
    void SetState(const GaState &state);

    // options
    void CpcModel(CpcBase::CpcModelType val);
    CpcBase::CpcModelType CpcModel()const { return cpcModel;}
    void CpmBoot(bool val);

    // Signals emulation
    void HSyncOff(Phase phase, bool &intReq, int &hCnt);
    void ApplyModeChange(Phase phase);
    void VSyncOn();
    void VSyncOff();

    const BYTE *VideoRam(int block)const;

    static int RamPattern(int ramSelection, int ramBlock);
  private:

    void AddModlutChange(Phase phase, long flags);

    // options
    CpcBase::CpcModelType cpcModel;
    bool cpmBoot=false;
    const int vSyncHReset=2;

    // Collaborators
    Z80 &cpu;
    Crtc &crtc;
    Client &client;

    // Internal GA Registers
    BYTE mf = 0; // mode and ROM
    BYTE nc = 0; // color pointer
    BYTE gcBorder;
    BYTE gcMode;
    BYTE color[16];

    // Memory layout
    // In mmu.h
    //  BYTE *memGet[4];
    //  BYTE *memPut[4];

    int ramSelection = 0; // ram pattern selected
    int nbRamBanks=0; // numnber of 64k RAM pages available (1 -> 65)
    int ramBank = 0;  // current ram bank
    std::vector<std::vector<BYTE>> ramData;// 4 to 4+64 16K blocks
    BYTE *GetRamBlock(int ramBlockId);

    int upperRom = 0; // upper ROM selected
    static const BYTE *romPattern[3][2];
    void RomSel(int val);
    const BYTE *hiRomMap[NB_UROM_SLOTS]; // Upper Rom mapping
    const BYTE *romData[NB_UROM_SLOTS]; // ROM data buffers
    const BYTE *lowerRomData;

    void SetRamMap();
    void SetHiRomMap();
    void SetMemoryPointers();

    // Interrupts control
    int hCnt = 0;
    int intReq = 0;

    // frame rendering
    FrameRenderer frameRenderer;
    CpcFrame renderFrame;

    // Screen control
    int vSync = 0;
    int vSyncCnt=0;
    int vSyncArmed=0;
    int vSyncAtLastHSync = 0;

    // Monitor support
    TrapFlag &trapFlag;

    Ga &operator=(const Ga&);
    Ga(const Ga &rhs);
};
