// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Fdc.cc - Floppy disk controller emulation
#include <string.h>
//#include <unistd.h>
#include <stdio.h>
#include "infra/error.h"
#include "fdc.h"
#include "infra/log.h"
#include "drive.h"
#include "cpcconsole.h"
#include "cpc_state.h"

using namespace std;

// FDC Status register 0 flags
static const BYTE St0ErrorCodeNone=0x00;
static const BYTE St0ErrorCodeIncomplete=0x40;
static const BYTE St0ErrorCodeInvalid=0x80;
static const BYTE St0ErrorCodeStopped=0xC0;
static const BYTE St0SeekEnd=0x20;
static const BYTE St0EquipmentCheck=0x10;
static const BYTE St0NotReady=0x08;
//static const BYTE St0HeadAddress=0x04;
//static const BYTE St0UnitSelect1=0x02;
//static const BYTE St0UnitSelect0=0x01;
// FDC Status register 1 flags
static const BYTE St1None=0x00;
static const BYTE St1EndOfCylinder=0x80;
static const BYTE St1DataError=0x20;
static const BYTE St1Overrun=0x10;
static const BYTE St1NoData=0x04;
static const BYTE St1NotWriteable=0x02;
static const BYTE St1MissingAddressMark=0x01;
// FDC Status register 2 flags
static const BYTE St2None=0x00;
static const BYTE St2ControlMark=0x40;
static const BYTE St2DataErrorInDataField=0x20;
static const BYTE St2WrongCylinderTrackValue=0x10;
static const BYTE St2ScanEqualHit=0x08;
static const BYTE St2ScanNotSatisfied=0x04;
//static const BYTE St2BadCylinder=0x02;
static const BYTE St2MissingAddressMarkInDataField=0x01;
// FDC Status register 3 flags
static const BYTE St3None=0x00;
//static const BYTE St3Fault=0x80;
static const BYTE St3DiskWriteProtected=0x40;
static const BYTE St3DriveReady=0x20;
static const BYTE St3Track0=0x10;
static const BYTE St3TwoSide=0x08;

/*
 130316-How to emulate a peripheral like the FDC:
 In a sense, until we talk to it (with the Z80), we don't care about what it does, so it is tempting
 To emulate on R/W only.
 On the other hand, doing so means that we need to catch up on state changes. For example, the GAP protection
 in Chicago '90 only polls the main status register waiting for end of execution phase on a read track.
 A solution I like is to use events/time that get pushed when we know we can expect them.
 When there is action from the CPU, we can process all the pending events in order to get the correct state.
 */

// From FDC765 DataSheet:
// During disk data transfers between the FDC and the processor, via the data bus,
// the FDC must be serviced by the processor every 27us in the FM mode and every 13us
// in the MFM mode, or the FDC sets the OR (Overrun) flag in the status register 1
// to a 1 (high), and terminates the Read Data command.
static const int FDC_US_PER_BYTE=32;

inline void Fdc::SetDelayUs(int delay)
{
    crtElapsedUs=0;
    crtDelayUs=(realTime?delay:0);
    crtTimeout=2*delay;
}

inline void Fdc::SetReady(Phase phase)
{
    if (!(mainState&FDC_RQM))
        lastReadPhase=phase;
    mainState|=FDC_RQM;
}

int Fdc::CrtTrack()const
{
    return CrtDrive()? driveStates[crtDrive].crtTrack: 0xFF;
}

bool Fdc::DriveNotReady(Phase phase)const
{
    return !CrtDrive() || !CrtDrive()->IsReady(phase);
}

bool Fdc::DriveProtected()const
{
    return CrtDrive()->IsProtected();
}

void Fdc::UpdateSeekState(Phase phase)
{
    for (int i=0; i<4; i++)
    {
        DriveState &ds(driveStates[i]);
        if (ds.seekState==DriveState::YES)
            if (driveStates[i].SeekDone(phase))
            {
                ds.seekState = DriveState::DONE;
            }
    }
}

Fdc::DriveState::DriveState(): st0(St0ErrorCodeNone), crtTrack(0), seekState(NO) {}

bool Fdc::DriveState::SeekDone(Phase phase)const
{
    if (phase<stepDonePhase)
        return false;
    stepDonePhase=0;
    return true;
}

void Fdc::Instr0(BYTE val, int warnMask)
{
    skip=multi=0;
    mfm=1;
    if (val&0x80) {
        if (warnMask&0x80) LOG_STATUS("Fdc: Multitrack Bit set");
        multi = 1;
    }
    if (!(val&0x40)) {
        if (warnMask&0x40) LOG_STATUS("Fdc: MFM Bit not set");
        mfm = 0;
    }
    if (val&0x20) {
        // Le livre du lecteur de disquette dit que ce bit est toujours à 0 sous AMSDOS
        // Mais la trace dit le contraire
        // if (warnMask&0x20) LOG_STATUS("Fdc: Skip bit set");
        skip=1;
    }
    StateInstr();
}

void Fdc::Instr1(BYTE val)
{
    if (val & 0x04)
        LOG_STATUS("Fdc: Head select Bit set");
    if (val & 0x02)
        LOG_STATUS("Fdc: Unit select Bit 1 set");
    crtHead = (val & 0x04)>>2;
    crtDrive = (val & 0x03) & 0x01; // US1 is not connected on CPC
    console.DiskActivity(crtDrive, DriveNotReady(0)? CpcConsole::DriveState::NOT_READY: CpcConsole::DriveState::ACTIVE, driveStates[crtDrive].crtTrack);
}

/*
 Read Deleted Data
 This command is the same as the Read Data Command except that when the FDC detects a Data Address Mark at the beginning of a Data Field (and SK=0 (low)), It will read all the data in the sector and set the CM flag in Status Register 2 to a 1 (high), and then terminate the command. If SK=1, then the FDC skips the sector with the Data Address Mark and reads the next sector.
 */
BYTE Fdc::ReadDataE(BYTE val, Phase phase)
{
    return ReadDataCommon(val, phase, true);
}

BYTE Fdc::ReadData(BYTE val, Phase phase) {
    return ReadDataCommon(val, phase, false);
}

BYTE Fdc::ReadDataCommon(BYTE val, Phase phase, bool erased)
{
    char cmdName[128];
    snprintf(cmdName, 128, "Read%s%s%s", track?"Track":"Data", erased? "Erased": "", skip? "Skip": "");

    int delay=0;
    int delayBytes=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val);
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            id.cylinder = val;
            break;
        case 3:
            id.head = val;
            break;
        case 4:
            id.id = val;
            break;
        case 5:
            id.size = val;
            break;
        case 6:
            lastSect = val;
            if (lastSect != id.id)
                LOG_STATUS("\t%s: lastSect=%02X id.sect=%02X",cmdName,lastSect,id.id);
            break;
        case 7:
            gap = val;
            break;
        case 8:
            length = val;
            crtSect = 0;
            LOG_STATUS("FDC[%s] ID[%02X%02X%02X%02X] La:%02X Ga:%02X Ln:%02X",cmdName,id.cylinder,id.head,id.id,id.size,lastSect,gap,length);
            if (DriveNotReady(phase)) {
                LOG_STATUS("\t%s: Drive access error (%d)",cmdName,crtDrive);
                st0 = St0ErrorCodeIncomplete|St0NotReady |  (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateResult();
                break;
            }
            try {
                if (!CrtDrive()->FindSect(crtHead,id,erased,skip,crtSect,delayBytes))
                {
                    delay = delayBytes * FDC_US_PER_BYTE;
                    LOG_STATUS("\t%s: Not found [%02X-%02X-%02X-%02X]",cmdName,id.cylinder,id.head,id.id,id.size);
                    st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                    st1 = St1NoData; // No data

                    //Mathematiques 3éme
                    // un missing DAM sur le secteur &63 de la piste 40
                    // Simon:
                    // The reason for getting in touch is the "ReadData: Not found" in the
                    // log.  The sector header exists, but there's no data field -- SAMdisk
                    // shows it as 'nd' for no-data.  Though from what I can tell from your
                    // result bytes, it's being reported as a regular sector-not-found?  I
                    // think the SR1 value is probably correct (I've not checked on PC yet),
                    // but I think ST2 should have the MD (bit 0) set to show it couldn't find
                    // the data field.  What do you think?
                    if (crtSect) st1 |= St1MissingAddressMark;

                    st2 = St2None;
                    StateResult();
                    break;
                }
            } catch (infra::Err &) {
                delay = delayBytes * FDC_US_PER_BYTE;
                LOG_STATUS("\t%s: Track not formated",cmdName);
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                st1 = St1MissingAddressMark; // Missing adress mark
                st2 = St2None;
                StateResult();
                break;
            }
            SetupReadData();
            st0 = (crtHead<<2) | crtDrive;
            st1 = crtSect->St1();
            st2 = crtSect->St2();
            delay = delayBytes * FDC_US_PER_BYTE;
            StateExecOut();
            dataIdx=0;
            break;
        case 9:
            val = st0;
            break;
        case 10:
            val = st1;
            break;
        case 11:
            val = st2;
            break;
        case 12:
            if (crtSect) val = crtSect->Cylinder();
            break;
        case 13:
            if (crtSect) val = crtSect->Head();
            break;
        case 14:
            if (crtSect) val = crtSect->Id();
            break;
        case 15:
            if (crtSect) val = crtSect->Size();
            LOG_STATUS("\tFDC[%s] Hd:%d Dr:%d La:%02X Ga:%02X Ln:%02X [%02X%02X%02X%02X][%02X%02X%02X]"
                        ,cmdName,crtHead,crtDrive,lastSect,gap,length,id.cylinder,id.head,id.id,id.size
                        ,st0,st1,st2);
            StateReady();
            InstructionComplete();
            break;
        default:
            delay = FDC_US_PER_BYTE;

            int elapsed=phase-lastReadPhase;
            while (elapsed > 200 && crtCnt)
            {
                dataIdx++;
                crtCnt++;
                elapsed -= FDC_US_PER_BYTE*5;
            }

            //val = crtSect->Data(dataIdx++);
            val = readBuffer[(dataIdx++)%fullTrackBufferSize]; // wrap around in case of real big sector size

            lastReadPhase = phase;

            if (crtCnt == 0) { // End of sector
                bool done=false;
                if (st1 & St1DataError) { // Data error
                    if (erased && !(st2&St2ControlMark))
                        st1 |= St1NoData; // No Data
                    st0 |= St0ErrorCodeIncomplete;
                    done=true;
                }
                if (st1 & St1MissingAddressMark) { // Missing adress mark
                    done=true;
                }
                if (erased && !skip)
                    st2 ^= St2ControlMark;
                if (!done && crtSect->Id()==lastSect) {
                    // Programmed end of track
                    // This is the normal end of read command for CPC (see comments below)

                    /*
                     So because of the FDC connections on cpc it's normal to see this error and you can ignore it. It is possible you do not see the error if you are reading multiple sectors with one read data command (R in read data command is not the same as EOT), and an error occurs in the middle, or the last sector is skipped (when SK is set in command and sector is marked with deleted data code).
                     -----------
                     At the end of a successful read/write command, the program should send a Terminal Count (TC) signal to the FDC. However, in the CPC the TC pin isn't connected to the I/O bus, making it impossible for the program to confirm a correct operation. For that reason, the FDC will assume that the command has failed, and it'll return both Bit 6 in Status Register 0  and Bit 7 in Status Register 1 set. The program should ignore this error message.
                     -----------
                     ST1=&80 is "end of cylinder" error.

                     This is related to the terminal count (TC) input to the FDC and when the FDC has just read the sector ID indicated by the EOT parameter in the read data command.

                     This input is connected to reset on CPC, so is only activated when a reset is done.

                     On PC for example, TC is triggered when all data has been read by the DMA controller.

                     So because of the FDC connections on cpc it's normal to see this error and you can ignore it. It is possible you do not see the error if you are reading multiple sectors with one read data command (R in read data command is not the same as EOT), and an error occurs in the middle, or the last sector is skipped (when SK is set in command and sector is marked with deleted data code).

                     So, for CPC, it's quite normal to see it.
                     -----------
                     */
                    // Le Necromancien does not want the flag below
                    // EXIT wants the flag below
                    //01040102
                    /*
                     After a lot of talk with Simon Owen, he came up with the theory below, which I believe is right:

                     I do have another more hopeful theory...  During a multi-sector read, if
                     the controller encounters a DAM that doesn't match the current command
                     type, it terminates the command early after reading the sector, and sets
                     the CM bit in SR2.  For the second read in Le Necro it's using ReadData
                     to read a deleted sector, and despite only requesting a single sector,
                     that should give the same termination, perhaps without the usual TC
                     failure.  So your current CM hack may be closer than you think!  Of
                     course, it would also need to behave the same way after reading the
                     first non-deleted sector using ReadDeletedData.
                     When SAMdisk reads some old 'regular' disk formats, it attempts to read
                     the full track with one multi-sector read.  I remember needing to check
                     the result bytes to see if it had finished the full block, and continue
                     if it stopped.  A change in the CM type of sectors was the usual reason
                     for an early stop, without any error being indicated.  I can probably
                     re-test this on the PC, and if it's as I remember it seems reasonable to
                     think it gives successful early command termination on the CPC too.

                     I've just run a few tests using this following test track:
                     250Kbps MFM,  9 sectors,  512 bytes/sector:
                     33.0  1 2 3 4d 5d 6d 7 8 9
                     It has 3 normal sectors, then 3 deleted, then 3 normal.
                     I used ReadData to request all 9 sectors starting from sector 1.
                     Checking the returned data I can see it read the first 4 sectors, so
                     it stopped after reading the first deleted sector.
                     The result bytes were:  [01 00 40][21 00 04 02].
                     So the top 2 bits of SR0 are clear, as we'd hoped, and SR2 shows the control mark.
                     Also note that execution was terminated before R was advanced to 5.
                     To check the Le Necro case I then used ReadData to request 3 sectors
                     starting from sector 4 (deleted).  The data confirmed it only read
                     the first sector before the command was terminated.
                     The result bytes were: [01 00 40][21 00 04 02].

                     I reckon that's good enough to make the FDC changes, which should fix
                     Le Necro with the existing dump.
                     For now we're assuming the CM termination happens on the CPC before
                     the TC failure, but everything points to that being the case.



                     les a100% d'or:
                     0.0  193 198d 194d 199d 195d 200d 196d 201 197d
                     FDC[ReadDataErased] ID[0000C802] La:C8 Ga:2A Ln:FF
                        -> [0000C802][408000] = Fail
                        -> [0000C802][008000] = Pass

                     */
                    /*
                      If the FDC reads a Deleted Data Address Mark off the diskette, and SK bit (bit D5 in the first Command Word) is not set (SK=0), then the FDC sets the CM (Control Mark) flag in Status Register 2 to a 1 (high), and terminates the Read Data Command, after reading all the data in the Sector. If SK=1, the FDC skips the sector with the Deleted Data Address Mark and reads the next sector. The CRC bits in the deleted data field are not checked when SK=1.
                     Read Deleted Data
                     This command is the same as the Read Data Command except that when the FDC detects a Data Address Mark at the beginning of a Data Field (and SK=0 (low)), It will read all the data in the sector and set the CM flag in Status Register 2 to a 1 (high), and then terminate the command. If SK=1, then the FDC skips the sector with the Data Address Mark and reads the next sector.

                     */
                    if (erased)
                    {
                        // Techno Cop:
                        //  FDC[FindTrack] dr=A tr=06
                        //  FDC[ReadDataErased] ID[0600C402] La:C4 Ga:14 Ln:FF
                        //  FDC[ReadDataErased] Hd:0 Dr:0 La:C4 Ga:14 Ln:FF [0600C402][008000]
                        // Fix the code so that a readErased of a deleted returns:
                        //  FDC[ReadDataErased] Hd:0 Dr:0 La:C4 Ga:14 Ln:FF [0600C402][408000]
                        // GPL (Gap Length) is at 14 instaed of 2A
                        //  During Read/Write operations this value determines the number of
                        //  bytes that VCO sync will stay low after two CRC bytes.

                        // this test needs to be done on the sector's st2 as st2 can be reset by statements above
                        if ( (crtSect->St2()&St2ControlMark) != 0 )
                            st0 |= St0ErrorCodeIncomplete;
                    } else {
                        if ( (crtSect->St2()&St2ControlMark) == 0 )
                            st0 |= St0ErrorCodeIncomplete;
                    }

                    st1 |= St1EndOfCylinder;
                    done = true;
                }
                if (!done) {
                    // multi-sector read
                    try {
                        bool eot = CrtDrive()->NextSectId(crtHead,erased,skip,crtSect,delayBytes);
                        done = eot;
                    } catch (infra::Err &) {
                        delay = delayBytes * FDC_US_PER_BYTE;
                        st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                        st1 = St1NoData;
                        st2 = St2None;
                        done = true;
                    }
                    SetupReadData();
                    st0 = (crtHead<<2) | crtDrive;
                    st1 = crtSect->St1();
                    st2 = crtSect->St2();
                    delay = delayBytes * FDC_US_PER_BYTE;
                    StateExecOut();
                    dataIdx=0;
                }
                if (done) {
                    //st0 |= St0ErrorCodeIncomplete;
                    crtCnt = 9;
                    StateResult();
                } else {
                    crtCnt = - crtSect->logicalSize();
                    dataIdx=0;
                    //id.id = crtSect->Id();
                }
            }
            break;
    }
    SetDelayUs(delay);
    return val;
}

int Fdc::IntersectDataSize(Track &crtTrack)
{
    int gap3 = crtTrack.Gap3();
    int crtIntersectSize=2+gap3+12+3+1+4+2+22+12+3+1;
    return crtIntersectSize;
}

void Fdc::IntersectData(Sector &crtReadDataSect, int gap3, std::vector<BYTE> &data)
{
    int crtIntersectSize=2+gap3+12+3+1+4+2+22+12+3+1;
    data.resize(crtIntersectSize);
    BYTE *pt = &data[0];

    // CRC (on data+ID, i.e. 516 bytes)
    *pt++ = 0x7C;
    *pt++ = 0x09;
    // GAP #3
    for (int i=0; i<gap3; i++)
        *pt++ = 0x4E;
    // Sync
    for (int i=0; i<12; i++)
        *pt++ = 0x00;
    // ID AM
    for (int i=0; i<3; i++)
        *pt++ = 0x1A;
    *pt++ = 0xFE;
    // ID
    *pt++ = crtReadDataSect.Cylinder();
    *pt++ = crtReadDataSect.Head();
    *pt++ = crtReadDataSect.Id();
    *pt++ = crtReadDataSect.Size();
    // CRC on ID
    *pt++ = 0x45;
    *pt++ = 0xAC;
    // GAP #2
    for (int i=0; i<22; i++)
        *pt++ = 0x4E;
    // Sync
    for (int i=0; i<12; i++)
        *pt++ = 0x00;
    // DATA AM
    for (int i=0; i<3; i++)
        *pt++ = 0x1A;
    *pt++ = 0xFB;
}

int highestOneBit(int i)
{
    i |= (i >> 1);
    i |= (i >> 2);
    i |= (i >> 4);
    i |= (i >> 8);
    i |= (i >> 16);
    return i - (i >> 1);
}

// Generate the track as seen by ReadData:
// Starts at data from requested sector
// Includes data in-between sectors
// Has gap at the end of the track
void Fdc::SetupReadData()
{
    st0 = (crtHead<<2) | crtDrive | St0ErrorCodeIncomplete;
    st1 = (crtSect->St2()&St2MissingAddressMarkInDataField)? 0x05: 0x34;
    st2 = (crtSect->St2()&St2MissingAddressMarkInDataField)? 0x01: 0x20;

    // a ReadData returns as many bytes as the sector's logical size
    int nBytesToGenerate = min<int>(crtSect->logicalSize(), fullTrackBufferSize);
    GenerateSectorReadData(*CrtDrive()->CrtTrack(), *crtSect, readBuffer, nBytesToGenerate);
    crtCnt = - nBytesToGenerate;
}

// Generate data for a read operation
// if nBytesToGenerate exceeds the current sector's size, we generate intersect data and then go into the next sectors etc.
void Fdc::GenerateSectorReadData(Track &crtTrack, Sector &crtReadDataSect, char *dst, int nBytesToGenerate)
{
    int state=0;
    int crtSectPhysicalSize,crtSectAvail,crtSectIdx;
    int crtIntersectSize, crtIntersectIdx;
    int crtEndOfTrackSize, crtEndOfTrackIdx;
    int syntheticIntersectDataSize;
    bool eot;
    int delay;
    std::vector<BYTE> crtIntersectData;
    int gap3 = crtTrack.Gap3();

    Sector *crtSect = &crtReadDataSect;

    for (int crtIdx=0; crtIdx<nBytesToGenerate; crtIdx++)
    {
        switch (state)
        {
            case 0: // init
                crtSectAvail = crtSect->storedSize();
                crtSectPhysicalSize = crtTrack.SectNBytes();
                crtSectPhysicalSize = highestOneBit(crtSectAvail);
                crtSectIdx=0;
                state=1;
            case 1: // in sector
                if (crtSectIdx<crtSectAvail && crtSectIdx<crtSectPhysicalSize)
                {
                    dst[crtIdx] = crtSect->Data(crtSectIdx++);
                    break;
                }
                // trust the data that is there if there is more that the synthetic.
                // Disco: 8K sectors with 6238 bytes avail (>4096+144)
                syntheticIntersectDataSize=IntersectDataSize(crtTrack);
                crtIntersectSize = max(crtSectAvail-crtSectIdx, syntheticIntersectDataSize);
                crtIntersectIdx=0;
                state=2;
            case 2: // inter-sector from dsk
                if (crtSectIdx<crtSectAvail && crtIntersectIdx<crtIntersectSize)
                {
                    dst[crtIdx] = crtSect->Data(crtSectIdx++);
                    crtIntersectIdx++;
                    break;
                }
                crtTrack.NextSect(crtSect,eot,delay);
                IntersectData(*crtSect, gap3, crtIntersectData);
                state=3;
            case 3: // inter-sector synthesized
                if (crtIntersectIdx<crtIntersectSize)
                {
                    dst[crtIdx] = crtIntersectData[crtIntersectIdx++];
                    break;
                }

                if (!eot)
                {
                    state=0;
                    crtIdx--;
                    break;
                }
                crtEndOfTrackSize = 6300-crtTrack.NbSect()*(crtTrack.SectNBytes()+crtIntersectSize);
                crtEndOfTrackIdx=0;
                state=4;
            case 4: // EOT Gap
                if (crtEndOfTrackIdx<crtEndOfTrackSize)
                {
                    dst[crtIdx] = 0x4E;
                    crtEndOfTrackIdx++;
                    break;
                }
                state=0;
                crtIdx--;
                break;
        }
    }

}

BYTE Fdc::WriteDataE(BYTE val, Phase phase)
{
    return WriteDataCommon(val, phase, true);
}

BYTE Fdc::WriteData(BYTE val, Phase phase)
{
    return WriteDataCommon(val, phase, false);
}

BYTE Fdc::WriteDataCommon(BYTE val, Phase phase, bool erased)
{
    char cmdName[128];
    snprintf(cmdName, 128, "WriteData%s", erased? "Erased": "");

    int delay=0, delayBytes=0;

    switch (crtCnt++) {
        case 0: // beginning of command phase
            Instr0(val, 0xC0);
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            id.cylinder = val;
            break;
        case 3:
            id.head = val;
            break;
        case 4:
            id.id = val;
            break;
        case 5:
            id.size = val;
            break;
        case 6:
            lastSect = val;
            if (lastSect != id.id)
                LOG_STATUS("%s: lastSect=%02X id.sect=%02X",cmdName,lastSect,id.id);
            break;
        case 7:
            gap = val;
            break;
        case 8: // end of command phase
            LOG_STATUS("FDC[%s] ID[%02X%02X%02X%02X] La:%02X Ga:%02X Ln:%02X"
                        ,cmdName,id.cylinder,id.head,id.id,id.size,lastSect,gap,length);
            length = val;
            crtSect = 0;
            if (DriveNotReady(phase) || DriveProtected()) {
                LOG_STATUS("%s: Drive access error (%d)",cmdName,crtDrive);
                StateResult();
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                if (DriveNotReady(phase))
                    st0 |= St0NotReady;
                st1 = St1None;
                if (!DriveNotReady(phase))
                    st1 |= St1NotWriteable;
                st2 = St2None;
                break;
            }
            try {
                CrtDrive()->FindSect(crtHead,id,erased,skip,crtSect, delayBytes); // modifies delayBytes before throwing
                delay = delayBytes * FDC_US_PER_BYTE;
                if (crtSect) {
                    StateExecIn();
                    st0 = (crtHead<<2) | crtDrive;
                    st1 = crtSect->St1();
                    st2 = crtSect->St1();
                    dataIdx=0;
                    crtCnt = - crtSect->logicalSize();
                } else {
                    LOG_STATUS("%s: Not found [%02X-%02X-%02X-%02X]",cmdName,id.cylinder,id.head,id.id,id.size);
                    StateResult();
                    st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                    st1 = St1NoData; // No data
                    st2 = St2None;
                }
            } catch (infra::Err &) {
                delay = delayBytes * FDC_US_PER_BYTE;
                LOG_STATUS("%s: Track not formated",cmdName);
                StateResult();
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                st1 = St1MissingAddressMark; // Missing adress mark
                st2 = St2None;
            }
            break;
        case 9: // begin of result phase
            val = st0;
            break;
        case 10:
            val = st1;
            break;
        case 11:
            val = st2;
            break;
        case 12:
            if (crtSect) val = crtSect->Cylinder();
            break;
        case 13:
            if (crtSect) val = crtSect->Head();
            break;
        case 14:
            if (crtSect) val = crtSect->Id();
            break;
        case 15: // end of result phase
            if (crtSect) val = crtSect->Size();
            LOG_STATUS("\tFDC[%s] Hd:%d Dr:%d La:%02X Ga:%02X Ln:%02X [%02X%02X%02X%02X][%02X%02X%02X]"
                 ,cmdName,crtHead,crtDrive,lastSect,gap,length,id.cylinder,id.head,id.id,id.size,st0,st1,st2);
            if (crtSect) {
                crtSect->St1(St1EndOfCylinder);
                crtSect->St2((erased?St2ControlMark:0) | (CrtTrack()==id.cylinder?0:St2WrongCylinderTrackValue));
            }
            StateReady();
            InstructionComplete();
            break;
        default: {// Execution phase
            if (crtSect->logicalSize()+crtCnt <= crtSect->storedSize())
                crtSect->Data(dataIdx++,val);
            if (crtCnt == 0) { // done
                if (crtSect->Id()==lastSect || CrtDrive()->NextSectId(crtHead,erased,skip,crtSect,delayBytes)) {
                    delay = FDC_US_PER_BYTE;
                    crtCnt = 9;
                    StateResult();
                } else {
                    delay = FDC_US_PER_BYTE;
                    crtCnt = - crtSect->logicalSize();
                    dataIdx=0;
                }
            }
            delay = FDC_US_PER_BYTE;
        } break;
    }
    SetDelayUs(delay);
    return val;
}

// Generate the full track as seen by ReadTrack:
// Starts at data from first sector
// Includes data in-between sectors
// Has gap at the end of the track

// EXIT: count gap3 length on track 0
//      42 00 00 00 FF FF FF FF FF
// Chicago 90: reads gap  data after sector 41 of size 2
//      42 00 27 00 41 03 41 2A FF
// Platoon: does not want gap data (23.0  177 178 179 180 181 182 183 184)
//      62 00 17 00 08 02 08 2A FF

// Simon sez:
// ReadTrack always starts from the data field of the first sector after the index.
// The CHRN values in the command do affect the result bytes, but aren't required to match for ReadTrack to return data.
// The N value in the command isn't used for matching, and only determines how much to read from the data field(s).
// The EOT value is a count of sectors to read, and if greater than one it may read multiple blocks of N.
// After reading each block, the FDC goes to back to search for ID headers, and will return data from the data field of the next sector found.
// This continues until 'EOT' sectors are returned, or the index marker has been seen again.
// If a block overlaps the index point, the complete block will still be returned before the command is terminated.
//
//      FDC[ReadTrack] ID[2800C103] La:C1 Ga:2A Ln:FF
//      [BroderbundClassics2A1 [MAXIT][SamDISK36B15][Original].dsk] 42 Cyls, Head 0: 250Kbps MFM,  9 sectors,  512 bytes/sector:
//      ... 40.0  193 198 194 199 195 200 196 201 197
//
// I think the command above will work, and start by returning 1024 bytes from the data field of sector 193.
// The next data field returned depends on the track position when it finished the block of 1024 bytes, which itself depends on gap sizes.
// I'm guessing it will continue to return data from sectors 194, 195, 196, and possibly 197, before stopping due to the index being seen.
// So there will probably be 4-5K of data returned in total, unless the reading code manually terminates earlier.
// The only strange thing about the request is the EOT value of C1.
// With ReadData the EOT value is the ID of the last sector to read, but with ReadTrack it's a count — that makes this a very big count!

void Fdc::SetupReadTrack()
{
    st0 = (crtHead<<2) | crtDrive;

    // Hercule wants the readTrack of its track 37 to return: 40 05 01 (because there is no DAM on the first sect)
    // Hercule wants the readTrack of its track 36 to return: 40 34 20
    // Platoon wants the readTrack of its track 23 to return: 00xxxxxx (because it completed by reading the 8 sectors)
    st1 = (crtSect->St2()&St2MissingAddressMarkInDataField)? (St1NoData|St1MissingAddressMark): (St1DataError|St1Overrun|St1NoData);
    st2 = (crtSect->St2()&St2MissingAddressMarkInDataField)? (St2MissingAddressMarkInDataField): (St2DataErrorInDataField);

     Track &crtTrack(*CrtDrive()->CrtTrack());
    // a ReadTrack returns <lastSect> times <logicalSize> bytes (with N from the specified ID)
    // or less if it hits the index before that (or finds an error)
    int blockSize = Size2Bytes(id.size);
    int nbSect = lastSect;
    bool eot = false;
    int crtIdx=0;
    int nBytesToRead=0;
    bool done=false;
    int sectIdx=0;
    for (; sectIdx<nbSect && !done && !eot && crtIdx<fullTrackBufferSize; sectIdx++)
    {
        if (crtSect->St2() & St2MissingAddressMarkInDataField)
        {
            done = true;
            continue;
        }
        int nBytesToGenerate = min<int>(blockSize, fullTrackBufferSize-crtIdx);
        GenerateSectorReadData(crtTrack, *crtSect, &readBuffer[crtIdx], nBytesToGenerate);
        crtIdx += nBytesToGenerate;
        nBytesToRead += blockSize;
        int delay;
        crtTrack.NextSect(crtSect,eot,delay);
    }
    if (sectIdx!=nbSect)
        st0 |= St0ErrorCodeIncomplete;
    crtCnt = - nBytesToRead;
}

// From FDC765 DataSheet:
// Read a Track
/* This command is similar to the Read Data command except that this is a continuous read operation where
 the entire data field from each of the sectors is read. Immediately after sensing the index hole, the FDC
 starts reading all data fields on the track as continuous blocks of data. If the FDC finds an error in
 the ID or data CRC check bytes, it continues to read data from the track. The FDC compares the ID
 information read from each sector with the value stored in the IDR and sets the ND flag of status
 register 1 to a 1 (high) if there is no comparison. Multi-track or skip operations are not allowed
 with this command.
 -This command terminates when the number of sectors read is equal to EOT. If the FDC does not find
 an ID address mark on the diskette after it senses the index hole for the second time, it sets the
 MA (missing address mark) flag in status register 1 to 1 (high) and terminates the command.
 (Status register 0 has bits 7 and 6 set to 0 and 1, respectively).
 */
BYTE Fdc::ReadTrack(BYTE val, Phase phase)
{
    int delay=0;
    int delayBytes=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x60);
            if (skip) {
                LOG_STATUS("ReadTrack: Skip bit ignored");
                skip=0;
            }
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            id.cylinder = val;
            break;
        case 3:
            id.head = val;
            break;
        case 4:
            id.id = val;
            break;
        case 5:
            id.size = val;
            break;
        case 6:
            lastSect = val;
            if (lastSect != id.id)
                LOG_STATUS("ReadTrack: lastSect=%02X id.sect=%02X",lastSect,id.id);
            break;
        case 7:
            gap = val;
            break;
        case 8:
            LOG_STATUS("FDC[ReadTrack] ID[%02X%02X%02X%02X] La:%02X Ga:%02X Ln:%02X",id.cylinder,id.head,id.id,id.size,lastSect,gap,length);
            length = val;
            if (DriveNotReady(phase)) {
                LOG_STATUS("ReadTrack: Drive not ready (%d)",crtDrive);
                st0 = St0ErrorCodeIncomplete|St0NotReady | (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateResult();
                break;
            }
            try {
                CrtDrive()->FirstSect(crtHead,crtSect,delayBytes); // throws if there is no sector, modifies delayBytes before throwing
                SetupReadTrack();
                delay = delayBytes * FDC_US_PER_BYTE;
                if (crtCnt == 0) { // End of sector or intersector space
                    crtCnt = 9;
                    StateResult();
                }
                else {
                    StateExecOut();
                    dataIdx=0;
                }
            } catch (infra::Err &) {
                delay = delayBytes * FDC_US_PER_BYTE;
                LOG_STATUS("ReadTrack: no data on track");
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                st1 = St1NoData;
                st2 = St2None;
                StateResult();
            }
            break;
        case 9:
            val = st0;
            break;
        case 10:
            val = st1;
            break;
        case 11:
            val = st2;
            break;
        case 12:
            val = id.cylinder;
            break;
        case 13:
            val = id.head;
            break;
        case 14:
            val = id.id;
            break;
        case 15:
            val = id.size;
            LOG_STATUS("\tFDC[ReadTrack] Hd:%d Dr:%d La:%02X Ga:%02X Ln:%02X [%02X%02X%02X%02X][%02X%02X%02X]"
                 ,crtHead,crtDrive,lastSect,gap,length,id.cylinder,id.head,id.id,id.size,st0,st1,st2);
            StateReady();
            InstructionComplete();
            break;
        default:
            val = readBuffer[dataIdx++];
            if (crtCnt == 0) { // End of sector or intersector space
                crtCnt = 9;
                StateResult();
            }
            break;
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::Format(BYTE val, Phase phase)
{
    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x40);
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            fmtSize=val;
            break;
        case 3:
            fmtNb=val;
            break;
        case 4:
            fmtGap=val;
            break;
        case 5:
            fmtData=val;
            LOG_STATUS("FDC[Format] Sz:%02X Nb:%02X Ga:%02X Fi:%02X",fmtSize,fmtNb,fmtGap,fmtData);

            if (DriveNotReady(phase)) {
                LOG_STATUS("Format: Drive not ready (%d)",crtDrive);
                StateResult();
                st0 = St0ErrorCodeIncomplete|St0NotReady | (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
            } else {
                CrtDrive()->FormatTrack(crtHead,fmtSize,fmtNb,fmtGap,fmtData);
                StateExecIn();
                crtCnt = -fmtNb * 4;
                fmtSect=0;
                st0 = (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
            }
            break;
        case 6:
            val=st0;
            break;
        case 7:
            val=st1;
            break;
        case 8:
            val=st2;
            break;
        case 9:
            val=0;
            break;
        case 10:
            val=0;
            break;
        case 11:
            val=0;
            break;
        case 12:
            val=0;
            StateReady();
            InstructionComplete();
            break;
        default:
            switch ((-crtCnt)&3) {
                case 3:
                    fmtId.cylinder=val;
                    break;
                case 2:
                    fmtId.head=val;
                    break;
                case 1:
                    fmtId.id=val;
                    break;
                case 0:
                    fmtId.size=val;
                    LOG_STATUS("\t\tID[%02X%02X%02X%02X]",fmtId.cylinder,fmtId.head,fmtId.id,fmtId.size);
                    CrtDrive()->FormatSect(crtHead,fmtSect,fmtId, St1EndOfCylinder, St2None);
                    if (++fmtSect==fmtNb) {
                        StateResult();
                        crtCnt=6;
                    }
                    break;
                default:
                    ERR_THROW("Internal error");
            }
            break;
    }
    SetDelayUs(100);
    return val;
}

// Read the next ID on the track
BYTE Fdc::ReadId(BYTE val, Phase phase)
{
    bool eot;
    int delay=0;
    int delayBytes=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x40);
            break;
        case 1:
            Instr1(val);
            id.cylinder = CrtTrack();
            id.head = 0xFF;
            id.id = 0xFF;
            id.size = 0xFF;
            if (DriveNotReady(phase)) {
                // Drive is not ready
                st0 = St0ErrorCodeStopped|St0NotReady | (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateResult();
                break;
            }
            crtSect = CrtDrive()->CrtSect();
            if (!crtSect) {
                // Empty track
                st0 = St0ErrorCodeStopped|St0NotReady | (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateResult();
                break;
            }

            st0 = (crtHead<<2) | crtDrive;
#if 0 // Puffy's Saga etc. expect these to be 0
            st1 = crtSect->St1();
            st2 = crtSect->St2();
#else
            st1 = St1None;
            st2 = St2None;
#endif
            id.cylinder = crtSect->Cylinder();
            id.head = crtSect->Head();
            id.id = crtSect->Id();
            id.size = crtSect->Size();
            StateResult();

            // Le Necromancien want's this here.
            try {
                CrtDrive()->NextSect(crtHead,crtSect,eot,delayBytes);
            } catch (infra::Err &) {
                // Track is not formatted
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                st1 = St1NoData;
                st2 = St2None;
                break;
            }
            delay = delayBytes*FDC_US_PER_BYTE;
            break;
        case 2:
            val = st0;
            break;
        case 3:
            val = st1;
            break;
        case 4:
            val = st2;
            break;
        case 5:
            val = id.cylinder;
            break;
        case 6:
            val = id.head;
            break;
        case 7:
            val = id.id;
            break;
        case 8:
            val = id.size;
            LOG_STATUS("FDC[readID] Hd=%d Dr=%d -> [%02X%02X%02X%02X][%02X%02X%02X]",crtHead,crtDrive,id.cylinder,id.head,id.id,id.size,st0,st1,st2);
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::ScanEqual(BYTE val, Phase phase)
{
    return ScanCommon(val, phase, ScanMode::EQUAL);
}

BYTE Fdc::ScanCommon(BYTE val, Phase phase, ScanMode scanMode)
{
    char cmdName[128];
    snprintf(cmdName, 128, "ScanEqual%s", skip? "Skip": "");

    int delay=0;
    int delayBytes=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val);
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            id.cylinder = val;
            break;
        case 3:
            id.head = val;
            break;
        case 4:
            id.id = val;
            break;
        case 5:
            id.size = val;
            break;
        case 6:
            lastSect = val;
            if (lastSect != id.id)
                LOG_STATUS("\t%s: lastSect=%02X id.sect=%02X",cmdName,lastSect,id.id);
            break;
        case 7:
            gap = val;
            break;
        case 8: // end of command phase
            step = val;
            if (step<1 || step>2) {
                LOG_WARNING("Unexpected step value in scan... forcing to 1");
                step = 1;
            }

            LOG_STATUS("FDC[%s] ID[%02X%02X%02X%02X] La:%02X Ga:%02X St:%02X",cmdName,id.cylinder,id.head,id.id,id.size,lastSect,gap,step);
            crtSect = 0;
            if (DriveNotReady(phase)) {
                LOG_STATUS("\t%s: Drive access error (%d)",cmdName,crtDrive);
                st0 = St0ErrorCodeIncomplete|St0NotReady |  (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateResult();
                break;
            }
            try {
                if (!CrtDrive()->FindSect(crtHead,id,false,skip,crtSect,delayBytes)) {
                    delay = delayBytes * FDC_US_PER_BYTE;
                    LOG_STATUS("\t%s: Not found [%02X-%02X-%02X-%02X]",cmdName,id.cylinder,id.head,id.id,id.size);
                    st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                    st1 = St1NoData; // No data
                    if (crtSect) st1 |= St1MissingAddressMark;
                    st2 = St2None;
                    StateResult();
                    break;
                }
            } catch (infra::Err &) {
                delay = delayBytes * FDC_US_PER_BYTE;
                LOG_STATUS("\t%s: Track not formated",cmdName);
                st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                st1 = St1MissingAddressMark; // Missing adress mark
                st2 = St2None;
                StateResult();
                break;
            }
            SetupReadData();
            st0 = (crtHead<<2) | crtDrive;
            st1 = crtSect->St1();
            st2 = crtSect->St2();
            delay = delayBytes * FDC_US_PER_BYTE;
            StateExecIn();
            dataIdx=0;
            scanHit = true;
            scanNotSatisfied = false;
            break;

        default: {// Execution phase
            // OverRun
            int elapsed=phase-lastReadPhase;
            while (elapsed > 200 && crtCnt) {
                dataIdx++;
                crtCnt++;
                elapsed -= FDC_US_PER_BYTE*5;
            }
            lastReadPhase = phase;

            BYTE fdcVal = readBuffer[(dataIdx++)%fullTrackBufferSize]; // wrap around in case of real big sector size
            if (fdcVal != val) scanHit=false;
            if (scanMode == ScanMode::EQUAL && fdcVal != val) scanNotSatisfied = true;
            if (scanMode == ScanMode::LOW_OR_EQUAL && fdcVal > val) scanNotSatisfied = true;
            if (scanMode == ScanMode::HIGH_OR_EQUAL && fdcVal < val) scanNotSatisfied = true;

            if (crtCnt == 0) { // End of sector
                // done?
                crtCnt = 9;
                StateResult();
                if (!scanNotSatisfied) { // condition met
                    st2 = (scanHit? St2ScanEqualHit: 0);
                    break;
                }
                if (crtSect->Id()==lastSect) { // last sector
                    if ( (crtSect->St2()&St2ControlMark) == 0 )
                        st0 |= St0ErrorCodeIncomplete;
                    st1 |= St1EndOfCylinder;
                    st2 = St2ScanNotSatisfied;
                    break;
                }
                // multi-sector read
                for (int i=0; i<step; i+=step)
                    if (CrtDrive()->NextSectId(crtHead,false,skip,crtSect,delayBytes)) {
                        delay = delayBytes * FDC_US_PER_BYTE;
                        st0 = St0ErrorCodeIncomplete | (crtHead<<2) | crtDrive;
                        st1 = St1NoData;
                        st2 = St2ScanNotSatisfied;
                        break;
                    }
                SetupReadData();
                st0 = (crtHead<<2) | crtDrive;
                st1 = crtSect->St1();
                st2 = crtSect->St2();
                delay = delayBytes * FDC_US_PER_BYTE;
                StateExecIn();
                crtCnt = - crtSect->logicalSize();
                scanHit = true;
                scanNotSatisfied = false;
                dataIdx=0;
            }
        } break;

        case 9:
            val = st0;
            break;
        case 10:
            val = st1;
            break;
        case 11:
            val = st2;
            break;
        case 12:
            val = id.cylinder;
            break;
        case 13:
            val = id.head;
            break;
        case 14:
            val = id.id;
            break;
        case 15:
            val = id.size;
            LOG_STATUS("\tFDC[%s] Hd:%d Dr:%d La:%02X Ga:%02X St:%02X [%02X%02X%02X%02X][%02X%02X%02X]"
                 ,cmdName,crtHead,crtDrive,lastSect,gap,step,id.cylinder,id.head,id.id,id.size,st0,st1,st2);
            StateReady();
            InstructionComplete();
            break;
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::ScanLow(BYTE val, Phase phase)
{
    LOG_STATUS("FDC: Scan Low or Equal [%02X]",val);
    return ScanCommon(val, phase, ScanMode::LOW_OR_EQUAL);
}

BYTE Fdc::ScanHigh(BYTE val, Phase phase)
{
    LOG_STATUS("FDC: Scan High or Equal [%02X]",val);
    return ScanCommon(val, phase, ScanMode::HIGH_OR_EQUAL);
}

BYTE Fdc::Recalibrate(BYTE val, Phase phase)
{
    int trk0;
    int delay=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            break;
        case 1:
            Instr1(val);
            LOG_STATUS("FDC[Recalibrate] dr=%c",(crtDrive?'B':'A'));
            driveStates[crtDrive].seekState = DriveState::YES;
            if (DriveNotReady(phase)) {
                st0 = St0ErrorCodeStopped|St0NotReady | (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateReady();
                InstructionComplete();
                break;
            }
            int dt;
            CrtDrive()->Step(phase, -77,trk0, dt);
            driveStates[crtDrive].stepDonePhase = phase + CpcDuration::FromMs(stepRateTimeMs*dt);
            delay = 1000*stepRateTimeMs*abs(77);
            mainState |= (1<<crtDrive);
            st0 = (crtHead<<2) | crtDrive;
            st1 = St1None;
            st2 = St2None;
            if (trk0) {
                driveStates[crtDrive].crtTrack=0;
            } else {
                driveStates[crtDrive].crtTrack-=77;
                st0 |= St0EquipmentCheck;
            }
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::FindTrack(BYTE val, Phase phase)
{
    int delay=0, trk0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            break;
        case 1:
            Instr1(val);
            break;
        case 2:
            LOG_STATUS("FDC[FindTrack] dr=%c tr=%02X",(crtDrive?'B':'A'),val);
            driveStates[crtDrive].seekState = DriveState::YES;
            if (DriveNotReady(phase)) {
                st0 = St0ErrorCodeStopped| (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                StateReady();
                InstructionComplete();
                break;
            }
            int dt;
            CrtDrive()->Step(phase, val-CrtTrack(), trk0, dt);
            driveStates[crtDrive].stepDonePhase = phase + CpcDuration::FromMs(stepRateTimeMs*dt);
            delay = 1000*stepRateTimeMs*abs(val-CrtTrack());
            mainState |= (1<<crtDrive);
            driveStates[crtDrive].crtTrack=val;
            st0 = crtDrive | (crtHead<<2);
            st1 = St1None;
            st2 = St2None;
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::SenseInt(BYTE val, Phase phase)
{
    int delay=0;

    switch (crtCnt++) {
        case 0:
            StateResult();
            break;
        case 1:
        {
            UpdateSeekState(phase);

            // Any drive has seek interrupt?
            int driveIndex=0;
            for (; driveIndex<4 && driveStates[driveIndex].seekState!=DriveState::DONE; driveIndex++);
            if (driveIndex==4)
            {
                st0 = St0ErrorCodeInvalid;
                val = st0;
                //LOG_STATUS("FDC[Sense Int] r0=%02X",st0);
                StateReady();
                InstructionComplete();
                break;
            }

            driveStates[driveIndex].seekState=DriveState::NO;
            mainState &= ~(1<<driveIndex); // reset drive seek mode flag

            st0 = St0SeekEnd | driveIndex | (crtHead<<2);
            st1 = St1None;
            st2 = St2None;
            val = st0;
            break;
        }
        case 2:
            val = CrtTrack();
            //LOG_STATUS("FDC[Sense Int] r0=%02X tr=%02X",st0,val);
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::SenseDrive(BYTE val, Phase phase)
{
    int delay=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            break;
        case 1:
            Instr1(val);
            StateResult();
            break;
        case 2: {
            if (!CrtDrive())
            {
                st0 = St0ErrorCodeStopped| (crtHead<<2) | crtDrive;
                st1 = St1None;
                st2 = St2None;
                val = st3 = crtDrive | (crtHead<<2);
                StateReady();
                InstructionComplete();
                break;
            }

            int driveState =
            (CrtDrive()->IsProtected()? St3DiskWriteProtected: 0) |
            (CrtDrive()->IsReady(phase)? St3DriveReady: 0) |
            (CrtDrive()->IsTrack0()? St3Track0: 0) |
            (CrtDrive()->IsTwoSides()? 0: St3TwoSide);
            val = st3 = driveState | crtDrive | (crtHead<<2);
            //LOG_STATUS("FDC[Sense Drive] dr=%d -> [%02X]", crtDrive, val);
            StateReady();
            InstructionComplete();
        } break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::SetDrive(BYTE val, Phase phase)
{
    int delay=0;

    // The time intervals mentioned below are a direct function of the clock (CLK
    // on pin 19). Times indicated below are for an 8 Mhz clock, if the clock was
    // reduced to a 4Mhz (mini-floppy application) then all time intervals are
    // increased by a factor of 2.

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            break;
        case 1:
            // The HUT (Head Unload Time) defines the time from the end of the
            // Execution Phase of one of the Read/Write Commands to the head unload
            // state. The Timer is programmable from 16 to 240ms in increments of
            // 16 ms (01=16 ms, 02=32 ms.....0F=240ms).
            headUnloadTimeMs = 2* 16 * (val & 0x0F); // ms

            // The SRT (Step Rate Time) defines the time interval between adjacent
            // step pulses. This timer is programmable from 1 to 16ms in increments
            // of 1 ms (F = 1ms, E = 2ms, D=3ms, etc).
            stepRateTimeMs =  2 * (16 - (val >>4)); // ms
            break;
        case 2:
            // The HLT (Head Load Time) defines the time between when the Head Load
            // signal goes high and the Read/Write operation starts. This timer is
            // programmable from 2 to 254 ms in increments of 2ms (01=2ms, 02=4ms,
            // 03=6ms ..... FE=254 ms)
            headLoadTimeMs = 2 * 2 * (val>>1);//ms
            // The choice of DMA or NON-DMA operation is made by the ND (NON-DMA)
            // bit. When this bit is high (ND=1) the NON-DMA mode is selected, and
            // when ND=0 the DMA mode is selected.
            dma = val&1;
            LOG_STATUS("FDC[Set Drive Data] st=%02X lf=%02X ld=%02X dma=%s",
                stepRateTimeMs, headUnloadTimeMs, headLoadTimeMs, dma?"NO":"YES");
            st0 = St0ErrorCodeNone;
            st1 = St1None;
            st2 = St2None;
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::Version(BYTE val, Phase phase)
{
    int delay=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            StateResult();
            break;
        case 1:
            val=0x80;
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

BYTE Fdc::Invalid(BYTE val, Phase phase)
{
    int delay=0;

    switch (crtCnt++) {
        case 0:
            Instr0(val, 0x00);
            StateResult();
            break;
        case 1:
            val=St0ErrorCodeNone;
            StateReady();
            InstructionComplete();
            break;
        default:
            ERR_THROW("Internal error");
    }
    SetDelayUs(delay);
    return val;
}

#define CMD(s) { Cmd_##s, &Fdc::s }

void Fdc::SelectInstruction(BYTE val)
{

    // These are the instruction codes for the 15 Fdc instructions
    static const struct { int code; Instruction instruction; } cmds[] =
    {
        CMD(ReadData), CMD(WriteData), CMD(ReadDataE), CMD(WriteDataE), CMD(ReadTrack),
        CMD(Format), CMD(ReadId), CMD(ScanEqual), CMD(ScanLow), CMD(ScanHigh),
        CMD(Recalibrate), CMD(FindTrack), CMD(SenseInt), CMD(SenseDrive), CMD(SetDrive),
        CMD(Version)
    };
    static const int nbCmds = sizeof(cmds)/sizeof(cmds[0]);

    int code = val&0x1F; // instruction mask
    for (int i=0; i<nbCmds; i++)
        if (code==cmds[i].code)
            currentInstruction = cmds[i].instruction;

    if (currentInstruction==0)
    {
        // Techno Cop uses that command (00)
        currentInstruction = &Fdc::Invalid;
        LOG_STATUS("Invalid FDC instr: %02X",val);
    }
    crtCnt = 0;
}

void Fdc::InstructionComplete()
{
    currentInstruction=0;
    console.DiskActivity(crtDrive, DriveNotReady(0)? CpcConsole::DriveState::NOT_READY: CpcConsole::DriveState::INACTIVE, driveStates[crtDrive].crtTrack);
}

void Fdc::Put(Phase phase, WORD addr, BYTE val)
{
    switch (addr & 0x0100) {
        case 0x0000:        // FA7F motor
            LOG_STATUS("FDC[Motors] %s",(val&1? "On": "Off"));
            motors = val;
            for (auto &drive: drives)
                if (drive)
                    drive->Motor(val&1);
            break;
        case 0x0100:        // FB7F Fdc
            switch (addr & 0x01) {
                case 0x00:      // FB7E Main status register
                    LOG_STATUS("[FDC]Writing %02X to [%04X]",val,addr);
                    break;
                case 0x01:      // FB7F Data register
                    crtElapsedUs += GetElapsedUs(phase);
                    if (crtElapsedUs >= crtDelayUs)
                        SetReady(phase);
                    if (IsExecuting()  && crtElapsedUs >= crtTimeout) {
                        // I don't think an overrun starts result phase
                        // StateResult();
                        // crtCnt = 9;
                        st1 |= St1Overrun;
                    }
                    if (IsInputing()) {
                        if (currentInstruction==0) { // If we are expecting an instruction code
                            SelectInstruction(val);
                        }
                        (this->*currentInstruction)(val, phase);
                    } else
                        LOG_STATUS("FDC: Writing in input stage");
                    break;
                default:
                    ERR_THROW("Internal error");
            }
            break;
        default:
            ERR_THROW("Internal error");
    }
}

BYTE Fdc::Get(Phase phase, WORD addr)
{
    BYTE val=0xFF;

    switch (addr & 0x0100) {
        case 0x0000:        // FA7F motor
            LOG_STATUS("[FDC]Reading from motor [%04X]",addr);
            break;
        case 0x0100:        // FB7F Fdc
            crtElapsedUs += GetElapsedUs(phase);
            if (crtElapsedUs >= crtDelayUs)
                SetReady(phase);
            if (IsExecuting()  && crtElapsedUs >= crtTimeout) {
                //st1 |= St1Overrun;
                // I don't think an overrun starts result phase
                // StateResult();
                // crtCnt = 9;
            }
            switch (addr&0x01) {
                case 0x00:      // FB7E Main status register
                    UpdateMainState(phase);
                    val = mainState;
                    break;
                case 0x01:      // FB7F Data register
                    if (IsOutputing()) {
                        if (currentInstruction)
                            val = (this->*currentInstruction)(0, phase);
                        else
                            LOG_STATUS("FDC: Reading from data register");
                    } else
                        LOG_STATUS("FDC: Reading in output stage");

                    break;
                default:
                    ERR_THROW("Internal error");
            }
            break;
        default:
            ERR_THROW("Internal error");
    }
    return val;
}


// The GAP protection:
// (http://www.cpc-power.com/cpcarchives/index.php?page=articles&num=207)
// Chicago 90 (F) (1989) [Original] (GAPS).dsk (break at 804C)
// -Does a read track on track 39.
// -Reads 519 bytes from execution but stores them all at 8089
// -Waits for result phase
// -Reads result phase
// -checks that byte at 8089 is F7
// -It does not disable INTs so it can over run.
//    8075 LD   A,D
//    8076 OR   E
//    8077 JR   Z,807F
//    8079 INC  C
//    807A IN   A,(C) [FDC Data Reg]
//    807C LD   (HL),A
//    807D DEC  C
//    807E DEC  DE
//    807F IN   A,(C) [FDC Status Reg]
//    8081 JP   P,807F  ;; FDC is ready for data transfer.
//    8084 AND  20      ;; FDC is performing execution phase of command
//    8086 JR   NZ,8075
//    8088 RET

// 12 Jeux Fantastiques semble faire la même chose avec un Read sur un secteur de 8K
void Fdc::UpdateMainState(Phase phase)
{
    if (currentInstruction==&Fdc::ReadTrack||currentInstruction==&Fdc::ReadData)
    {
        if (IsExecuting())
        {
            // this timeout should only trigger in cases where the code actually
            // ignores the execution phase and waits for the end of it by polling main status reg.
            // If the timeout is too short, RPM measurement will fail
            static const int executionTime = 5000; // guestimate
            if (crtElapsedUs>crtDelayUs+CpcDuration::FromUs(executionTime))
            {
                StateResult();
                crtCnt = 9;
            }
        }
    }
}

Fdc::Fdc(Drives &drives, CpcConsole &console, TrapFlag &trapFlag)
    : drives(drives), trapFlag(trapFlag), console(console)
{
    Fdc::Reset();
}

void Fdc::Reset()
{
    LOG_STATUS("FDC: Reset");
    st0 = St0ErrorCodeNone;
    for (int i=0; i<4; i++)
        driveStates[i]=DriveState();
    st1 = St1None;
    st2 = St2None;
    st3 = St3None;
    StateReset();
    currentInstruction=0;
    crtCnt=0;
    crtDelayUs=crtElapsedUs=crtTimeout=0;
    motors=0;
    crtDrive=0;
}

void Fdc::GetState(FdcState &state)const
{
    state.drives = drives;
}

void Fdc::SetState(const FdcState &state)
{
    for (int i = 0; i < NBMAX_DRIVES; i++)
        if (state.drives[i]) drives[i] = state.drives[i];
}
