// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// cpcphase.cpp - Class for phase counting
#include "cpcphase.h"

const int CpcClock::CLOCK_MHZ = 4;

CpcDuration::CpcDuration(const Us &us)
    : val(us.us * CLOCK_MHZ)
{}
CpcDuration::CpcDuration(const Ms &ms)
    : val(ms.ms * 1000 * CLOCK_MHZ)
{}
long CpcDuration::Val() const
{
    return val;
}

Phase::Phase(int init)
    : val(init)
{}
long Phase::PhaseUs() const
{
    return val / CLOCK_MHZ;
}

Phase &Phase::operator=(const Phase &rhs)
{
    val = rhs.val;
    return *this;
}

Phase Phase::operator+(const CpcDuration &rhs) const
{
    return Phase((val + rhs.Val()) & (MAX_PHASE - 1));
}

Phase Phase::operator-(const CpcDuration &rhs) const
{
    if (rhs.Val() > val) return Phase(MAX_PHASE - rhs.Val() + val);
    return Phase(val - rhs.Val());
}

Phase Phase::operator+=(const CpcDuration &rhs)
{
    return val = (val + rhs.Val()) & 0x3FFFFFFF;
}

bool Phase::operator==(const Phase &rhs) const
{
    return val == rhs.val;
}

bool Phase::operator>(const Phase &rhs) const
{
    if (val == rhs.val) return false;
    if (val & MAX_PHASE) return true;
    if (rhs.val & MAX_PHASE) return false;
    unsigned long bits = static_cast<unsigned long>(val - rhs.val);
    if ((bits & 0x20000000) == 0) return true;
    return false;
}

bool Phase::operator>=(const Phase &rhs) const
{
    if (val & MAX_PHASE) return true;
    if (rhs.val & MAX_PHASE) return false;
    unsigned long bits = static_cast<unsigned long>(val - rhs.val);
    if ((bits & 0x20000000) == 0) return true;
    return false;
}
bool Phase::operator<(const Phase &rhs) const
{
    return !operator>=(rhs);
}

// operator long()const {return val;}

unsigned long Phase::ElapsedUs(const Phase &prev) const
{
    return ElapsedSince(prev) / CLOCK_MHZ;
}

Phase Phase::FromMs(long ms)
{
    return FromUs(ms * 1000);
}
Phase Phase::FromUs(long us)
{
    return Phase(us * CLOCK_MHZ);
}
Phase Phase::Forever()
{
    return Phase(MAX_PHASE);
}

const int Phase::MAX_PHASE = 0x40000000;

unsigned long Phase::ElapsedSince(const Phase &before) const
{
    return (val + (val < before.val ? MAX_PHASE : 0)) - before.val;
}
