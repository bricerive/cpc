#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file fdc.h - Floppy disk controller emulation
#include "periph.h"
#include "drive.h"
#include "infra/rich_enum.h"

// Commands
// These are the instruction codes for the 15 Fdc instructions
/*
 æPD765 Version    D7  D6  D5  D4  D3  D2  D1  D0

 command byte 0:     ?  ?   ?   1   0   0   0   0
 result byte 0:    status register 0
 90h = æPD765B;  80h = æPD765A or æPD765A-2

 Invalid Command

 result byte 0:    status register 0 (value of 80h)
 */
static const int Cmd_ReadData=0x06;
static const int Cmd_WriteData=0x05;
static const int Cmd_ReadDataE=0x0C;
static const int Cmd_WriteDataE=0x09;
static const int Cmd_ReadTrack=0x02;
static const int Cmd_Format=0x0D;
static const int Cmd_ReadId=0x0A;
static const int Cmd_ScanEqual=0x11;
static const int Cmd_ScanLow=0x19;
static const int Cmd_ScanHigh=0x1D;
static const int Cmd_Recalibrate=0x07;
static const int Cmd_FindTrack=0x0F;
static const int Cmd_SenseInt=0x08;
static const int Cmd_SenseDrive=0x04;
static const int Cmd_SetDrive=0x03;
static const int Cmd_Version=0x10;
static const int Cmd_Invalid=0x00;

static const int fullTrackBufferSize=8192;

class CpcConsole;
struct FdcState;

/// Floppy disk controller emulation.
/// \todo replace this event driven implementation by a clock-driven one.
/// this would provide general accuracy in timing emulation. Currently timings
/// are apporximated by kludges.
/// \todo create a test suite to make sure we do not break compatibility.
/// the Fdc implementaion is critical in providing correct emulation of all the
/// tough game protections out there. It took a lot of painstaking debugging
/// and reverse-engineering to get there. That stuff should be part of the code test
class Fdc: public Peripheral {
public:
    /// maximum number of drives. The fdc supports 4 but the amstrad supoprts only 2
    static const int NBMAX_DRIVES=4;

    /// ctor
    /// \param drives The drives are owned by the CPC
    /// \param console The Fdc needs direct console access to blink the lights
    /// \param trapFlag polled by monitor to check if a  condition occurs
    typedef std::shared_ptr<Drive> DriveP;
    typedef std::array<DriveP, Fdc::NBMAX_DRIVES> Drives;

    Fdc(Drives &drives, CpcConsole &console, TrapFlag &trapFlag);

    /// Simulate a hardware reset on the FDC
    virtual void Reset();

    /// write a byte to the Fdc
    virtual void Put(Phase phase, WORD addr, BYTE val);

    /// read a byte from the Fdc
    virtual BYTE Get(Phase phase, WORD addr);

    /// control (pseudo) realtime emulation.
    /// in real time mode, the Fdc waits before returning data (simulating the disk speed)
    /// otherwise, it return righ away (as if the disk was instantaneous)
    void RealTime(bool val) {realTime=val;}

    /// Serialize
    void GetState(FdcState &state)const;
    /// Deserialize
    void SetState(const FdcState &state);

private:
    Fdc &operator=(const Fdc &) = delete;
    Fdc(const Fdc &) = delete;

    // The drives
    Drives &drives;

    // Monitor support
    TrapFlag &trapFlag;

    // options
    bool realTime=false;

    // Instruction execution
    Phase crtElapsedUs=0;
    Phase crtDelayUs = 0;
    Phase crtTimeout = 0;
    void SelectInstruction(BYTE val);
    void InstructionComplete();
    void Instr0(BYTE val, int warnMask=0xE0);
    void Instr1(BYTE val);
    void SetDelayUs(int delay);
    void UpdateMainState(Phase phase);

    struct DriveState
    {
        DriveState();
        // Suppose there is one st0 for each drive
        BYTE st0;
        // the FDC has its own current track idea as there is no way to ask directly to the drive
        int crtTrack;
        typedef enum {NO,YES,DONE} SeekState;
        SeekState seekState;
        mutable Phase stepDonePhase;
        bool SeekDone(Phase phase)const;
    };
    DriveState driveStates[NBMAX_DRIVES];

    // Fdc state
    BYTE mainState=0;
    BYTE st0, st1, st2, st3;
    //    BYTE r[4];
    int motors = 0;

    // read buffer for ReadTrack, ReadData, ReadDataErased
    char readBuffer[fullTrackBufferSize];
    static int IntersectDataSize(Track &crtTrack);
    static void IntersectData(Sector &crtReadDataSect, int gap3, std::vector<BYTE> &data);

    // Readtrack emulation
    void SetupReadTrack();

    // ReadData emulation
    void SetupReadData();
    static void GenerateSectorReadData(Track &crtTrack, Sector &crtReadDataSect, char *dst, int nBytesToGenerate);

    // Execution data
    typedef BYTE (Fdc::*Instruction)(BYTE, Phase);
    Instruction currentInstruction = 0;

    // Data set by instruction phase
    bool multi = false; // from first  instruction byte
    bool mfm=false; // from first  instruction byte
    bool skip = false; // from first  instruction byte
    bool track=false;  // from first  instruction byte
    int crtDrive = 0; // from second instruction byte
    BYTE crtHead = 0; // from second instruction byte

    SectId id; // set by ReadData, ReadDataErased, WriteData, WriteDataErased, ReadTrack
    BYTE lastSect = 0; // set by ReadData, ReadDataErased, WriteData, WriteDataErased, ReadTrack
    BYTE gap=0; // set by ReadData, ReadDataErased, WriteData, WriteDataErased, ReadTrack
    BYTE length = 0; // set by ReadData, ReadDataErased, WriteData, WriteDataErased, ReadTrack
    BYTE step; // set by ScanEqual, ScanLowOrEqual, ScanHighOrEqual

    BYTE fmtSize = 0; // set by Format
    BYTE fmtNb=0; // set by Format
    BYTE fmtGap = 0;  // set by Format
    BYTE fmtData = 0; // set by Format

    // Data used by execution phase
    int crtCnt=0;
    Phase lastReadPhase;
    BYTE fmtSect=0;
    BYTE stepRateTimeMs = 0, headUnloadTimeMs, headLoadTimeMs = 0, dma = 0; // value defined by SetDrive
    SectId fmtId;
    int dataIdx = 0;
    bool scanHit, scanNotSatisfied;

    DriveP CrtDrive()const {return drives[crtDrive];}
    int CrtTrack()const;
    Sector *crtSect = 0;

    // Methods
    SectId *FindID(BYTE *dsk, int track, int *sector);
    BYTE ReadData(BYTE val, Phase phase);
    BYTE ReadDataE(BYTE val, Phase phase);
    BYTE WriteData(BYTE val, Phase phase);
    BYTE WriteDataE(BYTE val, Phase phase);
    BYTE ReadTrack(BYTE val, Phase phase);
    BYTE Format(BYTE val, Phase phase);
    BYTE ReadId(BYTE val, Phase phase);
    BYTE ScanEqual(BYTE val, Phase phase);
    BYTE ScanLow(BYTE val, Phase phase);
    BYTE ScanHigh(BYTE val, Phase phase);
    BYTE Recalibrate(BYTE val, Phase phase);
    BYTE FindTrack(BYTE val, Phase phase);
    BYTE SenseInt(BYTE val, Phase phase);
    BYTE SenseDrive(BYTE val, Phase phase);
    BYTE SetDrive(BYTE val, Phase phase);
    BYTE Version(BYTE val, Phase phase);
    BYTE Invalid(BYTE val, Phase phase);

    BYTE ReadDataCommon(BYTE val, Phase phase, bool erased);
    BYTE WriteDataCommon(BYTE val, Phase phase, bool erased);
    RICH_ENUM(ScanMode, (EQUAL)(LOW_OR_EQUAL)(HIGH_OR_EQUAL));
    BYTE ScanCommon(BYTE val, Phase phase, ScanMode scanMode);
    
    // Drive state
    bool DriveNotReady(Phase phase)const;
    bool DriveProtected()const;
    void UpdateSeekState(Phase phase);

    // Main status register flags
    static const BYTE FDC_RQM=0x80;
    static const BYTE FDC_DIO=0x40;
    static const BYTE FDC_EXM=0x20;
    static const BYTE FDC_CBY=0x10;
    static const BYTE FDC_DBY3=0x08;
    static const BYTE FDC_DBY2=0x04;
    static const BYTE FDC_DBY1=0x02;
    static const BYTE FDC_DBY0=0x01;

    // MainState register
    inline void StateReset() {mainState=FDC_RQM;} // RQM=1 DIO=0 -> ready for write from CPU
    inline void StateInstr() {mainState=(mainState&0x0F)|FDC_CBY;}
    inline void StateExecIn() {mainState=(mainState&0x0F)|FDC_CBY|FDC_EXM;}
    inline void StateExecOut() {mainState=(mainState&0x0F)|FDC_CBY|FDC_EXM|FDC_DIO;}
    inline void StateResult() {mainState=(mainState&0x0F)|FDC_CBY|FDC_DIO;}
    inline void StateReady() {mainState=(mainState&0x0F)|FDC_RQM;}

    inline int IsExecuting()const {return mainState & FDC_EXM;}
    inline int IsInputing()const {return !(mainState & FDC_DIO);}
    inline int IsOutputing()const {return mainState & FDC_DIO;}
    inline void SetReady(Phase phase);


    CpcConsole &console;
};

