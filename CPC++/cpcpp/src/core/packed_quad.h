#pragma once
//
//  packed_quad.h
//  CPC++
//
//  Created by Brice Rivé on 8/10/11.
//  Copyright 2011 Brice Rivé. All rights reserved.
//
#include "cpctypes.h"
#include "cpcendian.h"

template<bool littleEndian>
struct PackedQuadEndian {
    typedef union {
        struct { WORD rr1,rr2; } rr;
        struct { BYTE r1,r2,r3,r4; } r;
    } Val;
};
template<>
struct PackedQuadEndian<true> {
    typedef union {
        struct { WORD rr2,rr1; } rr;
        struct { BYTE r4,r3,r2,r1; } r;
    } Val;
};

typedef PackedQuadEndian<LittleEndian()>::Val PackedQuad;


template<bool littleEndian>
struct PackedPairEndian {
    typedef union {
        WORD rr;
        struct { BYTE r1,r2; } r;
    } Val;
};
template<>
struct PackedPairEndian<true> {
    typedef union {
        WORD rr;
        struct { BYTE r2,r1; } r;
    } Val;
};

typedef PackedPairEndian<LittleEndian()>::Val PackedPair;
