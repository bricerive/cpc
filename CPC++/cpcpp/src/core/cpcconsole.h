#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// CpcConsole.h - abstract CPC console
#include "cpctypes.h"
#include "infra/rich_enum.h"

/// abstract class that defines what a console looks like
/// from a CpcBase point of view
class CpcConsole {
 public:
    virtual ~CpcConsole() {}

    /// called by Fdc to notify of drive activity
    RICH_ENUM(DriveState, (NOT_READY)(INACTIVE)(ACTIVE));
    virtual void DiskActivity(int which, DriveState state, int track)=0;

    /// called by Centro when a byte is output to the printer port
    virtual void PutPrinter(BYTE val)=0;

    /// pass the hand to the monitor event loop when the CPC hits a trap condition
    virtual void MonitorTrap()=0;

    /// Called by PSG to notify of sound buffer ready.
    /// This is called by the PSG when a buffer is full of sound data to play ASAP
    virtual void SoundBuffDone()=0;
};
