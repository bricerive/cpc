// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Psg.cc - Programable sound generator emulation
#include "psg.h"
#include "infra/error.h"
#include "infra/log.h"
#include "kbd.h"
#include "centro.h"
#include "cpc_state.h"

static const bool LOG_VOL = false;

static const int Log(BYTE val)
{
    if constexpr(LOG_VOL) {
        static const int logVal[] = {0,1,1,2,2,3,3,4,5,6,8,10,13,16,20,25};
        return logVal[val];
    } else {
        static const int logVal[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        return logVal[val];
    }
}

Psg::Psg(Kbd &kbd, Centro &centro, CpcConsole &console, TrapFlag &trapFlag)
: kbd(kbd), centro(centro), trapFlag(trapFlag), console(console)
{
    Psg::Reset();
}

void Psg::Reset()
{
    deltaUs=0;

    for (int i=0; i<16; i++)
        reg[i]=0;
    reg[7]=63;
    reg[14]=255;
    perA = perB = perC = perN = perE = 1<<16;
    cntA = cntB = cntC = cntN = cntE = 0;
    sigA = sigB = sigC = sigN = sigE = 1;
    mode = 0;
    crtDatIn=14;
    crtDatOut=255;
    crtAdd=14;
    portA=255;
    portB=0;
    envHold = 1;
    envAlt = 0;
    attack=continu=alternate=hold=0;
    psgRand = 1; // anything but 0
}

void Psg::SoundOnOff(bool on)
{
    makeSounds = on;
}

BYTE Psg::GetPortA()const {return portA;}
BYTE Psg::GetPortB()const {return portB;}

void Psg::ChannelsOnOff(int chanA, int chanB, int chanC)
{
    channelA = chanA?1:0;
    channelB = chanB?1:0;
    channelC = chanC?1:0;
}

template<typename T, typename T2>
T &MAXIMIZE(T &a, const T2 b)
{
    return a=std::max<T>(a,b);
}

// Write data to PSG
void Psg::Write(BYTE val)
{
    switch (crtAdd) {
    case 0:
        reg[0] = val;
        perA = reg[0] + (reg[1]<<8);
        MAXIMIZE(perA,1); // Period = 0 is actually processed as 1
        perA<<=16;
        break;
    case 1:
        reg[1] = val & 0x0F;
        perA = reg[0] + (reg[1]<<8);
        MAXIMIZE(perA,1);
        perA<<=16;
        break;
    case 2:
        reg[2] = val;
        perB = reg[2] + (reg[3]<<8);
        MAXIMIZE(perB,1);
        perB<<=16;
        break;
    case 3:
        reg[3] = val & 0x0F;
        perB = reg[2] + (reg[3]<<8);
        MAXIMIZE(perB,1);
        perB<<=16;
        break;
    case 4:
        reg[4] = val;
        perC = reg[4] + (reg[5]<<8);
        MAXIMIZE(perC,1);
        perC<<=16;
        break;
    case 5:
        reg[5] = val & 0x0F;
        perC = reg[4] + (reg[5]<<8);
        MAXIMIZE(perC,1);
        perC<<=16;
        break;
    case 6:
        reg[6] = val & 0x1F;
        perN = reg[6]<<1;
        MAXIMIZE(perN,1);
        perN<<=16;
        break;
    case 7:
        reg[7] = val;
        break;
    case 8:
        reg[8] = val & 0x1F;
        break;
    case 9:
        reg[9] = val & 0x1F;
        break;
    case 10:
        reg[10] = val & 0x1F;
        break;
    case 11:
        reg[11] = val;
        perE = reg[11] + (reg[12]<<8);
        MAXIMIZE(perE,1);
        perE<<=16;
        break;
    case 12:
        reg[12] = val;
        perE = reg[11] + (reg[12]<<8);
        MAXIMIZE(perE,1);
        perE<<=16;
        break;
    case 13:
        reg[13] = val & 0x0F;
        // Reset envelop phase
        continu = (val>>3)&1;
        attack = (val>>2)&1;
        alternate = (val>>1)&1;
        hold = val&1;
        envHold = envAlt = 0;
        sigE = attack? 0xFF: 0x10;
        break;
    case 14:
        if (reg[7]&0x40) // Output
            portA = reg[14] = val;
        else
            reg[14] = val;
        break;
    case 15:
        LOG_STATUS("[Psg] Writing register 15 (%02X)",val);
        reg[15] = val;
        break;
    default:
        LOG_STATUS("[Psg] Writing invalid register %d (%02X)",crtAdd,val);
    }
}

// Read data from Psg
BYTE Psg::Read()
{
    BYTE val=0xFF;

    switch (crtAdd) {
    case 14:
        if (reg[7]&0x40) // Output
            val = reg[14] = portA;
        else             // Input
            val = reg[14] = portA = kbd.ReadRow();
        break;
    case 15:
        if (reg[7]&0x80) // Output
            val = reg[15];
        else             // Input
            val = reg[15] = portB = 0xFF;
        LOG_STATUS("[Psg] Reading register 15 (%02X)",val);
        break;
    default:
        if (crtAdd<16)
            val = reg[crtAdd];
        break;
    }
    return val;
}

void Psg::Put(BYTE val)
{
    crtDatIn=val;
    if (mode==0x80) // Write to PSG
        Write(crtDatIn);
    if (mode==0xC0) // Latch address
        crtAdd = crtDatIn;
}

BYTE Psg::Get()
{
    BYTE val=0xFF;
    if (mode==0x40) // Read from PSG
        val = crtDatOut = Read();
    else
        LOG_STATUS("[Psg] read in mode %02X",mode);
    return val;
}

void Psg::Ctrl(BYTE val)
{
    mode = val & 0xC0;

    switch (mode) {
    case 0x00:
        break;
    case 0x40: // Read from PSG
        crtDatOut = Read();
        break;
    case 0x80: // Write to PSG
        Write(crtDatIn);
        break;
    case 0xC0: // Latch current address
        crtAdd = crtDatIn;
        break;
    default:
        ERR_THROW("Internal error");
    }
}

void Psg::GetState(PsgState &state)const
{
    state.crtAdd = crtAdd;
    for (int i=0; i<16; i++)
        state.reg[i] = reg[i];
}

void Psg::SetState(const PsgState &state)
{
    for (int i=0; i<16; i++) {
        crtAdd = static_cast<BYTE>(i);
        Write(state.reg[i]);
    }
    crtAdd = state.crtAdd;
}

// This is called every audioTimeUnitUs of CPC time
CpcDuration Psg::Step(Phase phase)
{
    // The calling frequency is determined by the desired output frequency
    // (itself determined by the available hardware)
    // on the Mac, outputFreqHz is 44100Hz (CD quality)
    double audioTimeUnitUs = (1000000.0/sndInfo.frequencyHz);

    // The PSG rate is 125KHz (=1000000/8) (max tone frequency of 62.5KHz)
    static const int psgTimeUnitUs = 8; // = 1000000.0/125000.0

    // Number of PSG steps elapsed between two calls
    double audioPsgCnt = audioTimeUnitUs/psgTimeUnitUs;

    // Sampling is done in fixed arithmetic
    long audioPsgCntFix = static_cast<int>(audioPsgCnt*65536.0);

    long usUntilNextCall = static_cast<long>(audioTimeUnitUs+deltaUs);

    deltaUs = audioTimeUnitUs + deltaUs - usUntilNextCall;
    long elapsedUs = phase.ElapsedUs(lastPhase);
    lastPhase = phase;

    // On first call we do not have any time elapsed yet
    if (elapsedUs == 0 || !makeSounds) return CpcDuration::FromUs(usUntilNextCall);

    cntN -= audioPsgCntFix;
    if (cntN <= 0) {
        cntN = perN - ((-cntN)%perN);

        /*
            I looked for a precise description  of how the pseudo-random generator works
            I could find several descriptions (see refs below), but they do not all agree on some details

            It seems agreed that AY-3-8910 uses a 17 bits LFSR as Pseudo-Random Noise Generator
            see https://en.wikipedia.org/wiki/Linear-feedback_shift_register

            Implemnting it as Gallois or Fibonacci, left or right, does not make any difference in the output.
            What matters is the register size and the position of the taps.
            It seems logical that the taps positions should correspond to a maximumn-length LFSR with minimum number of taps

            at 17 bits, the polynoms with two taps are:
            (https://web.archive.org/web/20180829202406/http://www.newwaveinstruments.com/resources/articles/m_sequence_linear_feedback_shift_register_lfsr/17stages.txt)
                [17, 14]    -> 0x12000
                [17, 12]    -> 0x10800
                [17, 11]    -> 0x10400

            Combining this and the references I found, I think it is a 17 bits LFSR with taps 17 & 14

            From MAME:
            The Random Number Generator of the 8910 is a 17-bit shift register.
            The input to the shift register is bit0 XOR bit3 (bit0 is the output).
            This was verified on AY-3-8910 and YM2149 chips.
				// TODO : get actually algorithm for AY8930
				m_rng ^= (((m_rng & 1) ^ ((m_rng >> 3) & 1)) << 17);
				m_rng >>= 1;

            From https://github.com/schnitzeltony/z80/blob/master/src/ay8910.c
            The Pseudo Random Number Generator of the 8910 is a 17-bit shift register.
            The input to the shift register is bit0 XOR bit3 (bit0 is the output).
            This was verified on AY-3-8910 and YM2149 chips.
            The following is a fast way to compute bit17 = bit0^bit3.
            Instead of doing all the logic operations, we only check bit0,
            relying on the fact that after three shifts of the register, what now
            is bit3 will become bit0, and will  invert, if necessary, bit14, which previously was bit17.
            This version is called the "Galois configuration".
	            // Is noise output going to change?
				if ((chip.prng + 1) & 2) // (bit0^bit1)
					chip.output_n = ~chip.output_n;
                if (0 != (chip.prng & 1))
                    chip.prng ^= 0x24000;
                chip.prng >>= 1;
                chip.count_n += chip.period_n;

            From Arnold:
            The Random Number Generator of the 8910 is a 17-bit shift register.
            The input to the shift register is bit0 XOR bit2 (bit0 is the output).
            The following is a fast way to compute bit 17 = bit0^bit2.
            Instead of doing all the logic operations, we only check bit 0,
            relying on the fact that after two shifts of the register, what now
            is bit 2 will become bit 0, and will invert, if necessary, bit 16, which previously was bit 18.
                // Is noise output going to change?
                if ((RNG + 1) & 2)	// (bit0^bit1)?
                    NoiseOutput = NoiseOutput ^ 0x0ffff;
                if (RNG & 1) RNG ^= 0x28000;
                RNG >>= 1;

            From Ayumi (https://github.com/true-grue/ayumi)
                int bit0x3 = ((ay->noise ^ (ay->noise >> 3)) & 1);
                ay->noise = (ay->noise >> 1) | (bit0x3 << 16);
                return ay->noise & 1;

            From BlueMSX
                ay8910->noiseVolume ^= ((ay8910->noiseRand + 1) >> 1) & 1;
                ay8910->noiseRand    = (ay8910->noiseRand ^ (0x28000 * (ay8910->noiseRand & 1))) >> 1;
        */

        // Galois style implementation of right shift 17 bits LFSR with taps 17 and 14
        sigN = psgRand & 1;
        psgRand >>= 1;
        psgRand ^= (-sigN) & 0xB400u;
    }

    cntE -= audioPsgCntFix;
    while (cntE <= 0) {
        cntE += perE;
        sigE = sigE+static_cast<BYTE>((1-envHold) * (-1 + 2 * (attack^envAlt)));
        if (sigE==0x10) {
            sigE=0x00;
            envHold = (hold | ~continu) &1;
            sigE |= 0x0F * (continu & (hold ^ alternate));
            envAlt ^= (continu & ~hold & alternate) &1;
        }
        if (sigE==0xFF) {
            sigE = 0x0F;
            envHold = (hold | ~continu) & 1;
            sigE &= ( (continu & (hold ^ alternate)) | ~continu)? 0: 0x0F;
            envAlt ^= (continu & ~hold & alternate) &1;
        }
    }

    cntA -= audioPsgCntFix;
    if (cntA <= 0) {
        int cnt = (-cntA)/perA+1; // number of changes in the signal since last time
        sigA ^= cnt & 1; // change signal if there has been an odd number of changes since last time
        cntA = perA - ((-cntA)%perA);
    }

    cntB -= audioPsgCntFix;
    if (cntB <= 0) {
        int cnt = (-cntB)/perB+1;
        sigB ^= cnt & 1;
        cntB = perB - ((-cntB)%perB);
    }

    cntC -= audioPsgCntFix;
    if (cntC <= 0) {
        int cnt = (-cntC)/perC+1;
        sigC ^= cnt & 1;
        cntC = perC - ((-cntC)%perC);
    }

    // to avoid sampling artifact noises we erase the signals over the Nyquist freqency.
    // For example, in Robocop, the voice synthesis is damaged.
    // That is because the period of each channel is set to 0 and the code uses amplitude
    // control to generate sound.
    // A period of 0 is (by the AY doc) the same as 1.
    // So we get a freq of 1000000/16 = 62.5KHz. This is
    // a) too high for hearing
    // b) too high for our rendering freq (typically 22k or 44k)
    // In this case, the result is an artifact freq around 20K.
    // To avoid it, we block the output to 1 (0 would make the speech synth inaudible)
//    if (audioPsgCntFix>perA) sigA=1;
//    if (audioPsgCntFix>perB) sigB=1;
//    if (audioPsgCntFix>perC) sigC=1;
//    if (audioPsgCntFix>perN) sigN=1;
    if (perA==0x10000) sigA=1;
    if (perB==0x10000) sigB=1;
    if (perC==0x10000) sigC=1;
    if (perN==0x10000) sigN=1;

    if (dropOn) return CpcDuration::FromUs(usUntilNextCall);

    // reg 7 mixing
    int bitA = ((sigA | (reg[7]   )) & (sigN | (reg[7]>>3))) & 1;
    int bitB = ((sigB | (reg[7]>>1)) & (sigN | (reg[7]>>4))) & 1;
    int bitC = ((sigC | (reg[7]>>2)) & (sigN | (reg[7]>>5))) & 1;

    int volA = channelA*(Log((reg[8]&0x10)? sigE: reg[8])); // 0 < volA < 15
    int volB = channelB*(Log((reg[9]&0x10)? sigE: reg[9]));
    int volC = channelC*(Log((reg[10]&0x10)? sigE: reg[10]));

    // Convert each channel to [-8191,+8192]
    // PSG output is 0-15
    // Digiblaster is 8 bit data centered around 0x80
    double outA = 512.0 * volA * ((bitA<<1)-1);
    double outB = 512.0 * volB * ((bitB<<1)-1);
    double outC = 512.0 * volC * ((bitC<<1)-1);

    //outA = 512.0 * volA * bitA;
    //outB = 512.0 * volB * bitB;
    //outC = 512.0 * volC * bitC;

    // Mix the channels
    double left = outA + outB;
    double right = outC + outB;

    if (!stereo) {
        right = left = (left+right)/2.0;
    }

    if (digiblaster) { // Digiblaster
        // 74LS273 output has bit 7 inverted by HW
        // At reset, it goes 0x00 (0x80) (from 74LS273 Master Reset) then 0x7F (0xFF) from ROM init
        // (05A1-LD BC,EF7F 05A$ OUT (C),C)
        // That means that if nothing special is output to it after that, we get 0xFF here.
        // Problem is that, if there is delays in the replay, the silence injection (at 0) will
        // make click noises -> silence injection needs to extend last level
        double outD = 128.0 * (centro.GetOutput()-0x80);
        left += outD;
        right += outD;
    }

    // Convert to output format
    if (sndInfo.canStereo) {
        if (sndInfo.precision==2) { // 16 bits 0-centered
            reinterpret_cast<int16_t*>(sndInfo.buff)[2*idx]=static_cast<int16_t>(left);
            reinterpret_cast<int16_t*>(sndInfo.buff)[2*idx+1]=static_cast<int16_t>(right);
        } else { // 8 bit 0x80 centered
            *(sndInfo.buff+2*idx)=static_cast<UINT8>(left/512.0)+0x80;
            *(sndInfo.buff+2*idx+1)=static_cast<UINT8>(right/512.0)+0x80;
        }
    } else {
        if (sndInfo.precision==2)
            reinterpret_cast<int16_t*>(sndInfo.buff)[2*idx]=static_cast<int16_t>(left);
        else
            *(sndInfo.buff+idx)=static_cast<UINT8>(left/256.0)+0x80;
    }

    idx++;
    if (idx==sndInfo.nbSamples) {
        console.SoundBuffDone();
        idx=0;
    }
    return CpcDuration::FromUs(usUntilNextCall);
}

void Psg::SetSoundInfo(const CpcBase::SoundInfo &soundInfo)
{
    sndInfo = soundInfo;
}

