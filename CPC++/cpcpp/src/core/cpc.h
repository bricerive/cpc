#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// cpc.h - Main emulator class

#include "centro.h"
#include "cpc_base.h"
#include "crtc.h"
#include "fdc.h"
#include "ga.h"
#include "iospace.h"
#include "kbd.h"
#include "pio.h"
#include "psg.h"
#include "tape.h"
#include "z80.h"
class Drive;
struct CpcState;
struct ConfigState;

/// CPC emulator class
/// complies with CpcBase interface.
class Cpc : public CpcBase, public IoSpace, public Crtc::Client, public Ga::Client {
   public:
    Cpc(CpcConsole &console);
    virtual ~Cpc();

    // runs the CPC until either of:
    // -end of frame (vduVsSync)
    // -monitor trap
    RunState RunSome(bool render) override;

    void RenderFrame(const CpcBase::RenderInfo &info, bool force = false) override;
    void Reset() override;

    void GetState(CpcState &state) const override;
    void SetState(const CpcState &state) override;

    void LastClick(int x, int y) override;
    void SetTrap(TrapId trapId) override;
    void SetWatch(WORD addr) override;
    int LastClickX() const override;
    int LastClickY() const override;
    BYTE MemRd8(WORD addr) const override;
    BYTE MemRdWr8(WORD addr) const override;
    UINT16 MemRd16(WORD addr) const override;

    void AddConditionCpuStep() override;
    void AddConditionCpuBreak(UINT16 address, bool persist) override;
    void AddConditionCpuStack(UINT16 address) override;
    void AddConditionCpuIntAck() override;
    void RemoveCpuCondition() override;
    void CpuTrace(bool on) override;
    void CrtcTrace(bool on) override;
    void TrapCrtcChanges() override;
    const Mmu &GetMmu() override;

    // Options
    void DropOn(bool val) override;
    void SetSoundInfo(const CpcBase::SoundInfo &sndInfo) override;
    void SoundOnOff(bool on) override;
    void Stereo(bool val) override;
    void ChannelsOnOff(int a, int b, int c) override;

    void KeyChange(int keyCode, KeyDirection direction) override;

    void LoadDisk(DriveId drive, const CpcFile &file, bool protect) const override;
    void GetDir(DriveId drive, std::vector<std::string> &dir, bool &canCpm) const override;
    void SaveDisk(DriveId drive, CpcFile &file) const override;
    void FlipDisk(DriveId drive) const override;
    void EjectDisk(DriveId drive) const override;
    bool DriveEnabled(DriveId drive) const override;
    bool DoubleSidedFloppy(DriveId drive) const override;
    bool IsWriteProtected(DriveId drive) const override;
    bool IsDriveReady(DriveId drive) const override;

    // The buffers passed here then belong to the CPC
    void SetRoms(const BYTE *const rom, const BYTE *const roms[256]) override;

    void CpcModel(CpcBase::CpcModelType val) override;
    void CpmBoot(bool val) override;

    void RealTime(bool val) override;

    void PrinterOutput(CpcBase::PrinterMode val) override;

    void Constructor(CpcConstructor val) override;

    // Crtc::Client
    void CrtcChanged(int ar) override;
    void CrtcRegisterChanged(Phase crtPhase, int ar, int val) const override;

    // Ga::Client
    void CrtcTrace(Phase crtPhase, const char *eventStr) const override;

   private:
    void ResetInner();
    void GetVduState(VduState &state) const;
    void SetVduState(const VduState &state) const;
    void GetConfigState(ConfigState &state) const;
    void SetConfigState(const ConfigState &state);
    void CpuStep(Phase &crtPhase);
    void VduStep(const Phase &crtPhase, bool render);

    // Components
    Crtc crtc;
    Pio pio;
    Centro centro;
    Fdc::Drives drives; // Cpc only supports 2 drives
    Fdc fdc;
    Ga ga;
    Tape tape;
    Kbd kbd;
    Psg psg;
    Z80 cpu;

    // Monitor support
    WORD watchAddr=0;
    BYTE watchVal=0;
    TrapId trapActive=TrapId::NONE;
    void TrapCondition(int n);

    TrapFlag trapFlag = TrapFlag::OFF;
    bool resetFlag=false;
    int lastClickX=0, lastClickY=0;

    // Tracing
    mutable bool tracing=false;
    mutable FILE *crtcOut=0;
    mutable Phase lastCrtcPhase=0;  // for CrtcTrace

    // Scheduler in CPC time
    Phase cpcPhase;  // current main phase
    Phase cpuPhase;
    Phase psgPhase;

    // Hi-res registers
    //-----------------
    //  Horizontal control
    unsigned int hCount=0, hReset=0, hStart=0, hEnd=0, hDelay=0;
    unsigned int crtcHSync=0, hSyncCnt=0, hSyncShort=0;
    unsigned int r8lock=0;
    //  Vertical control
    unsigned int vAdj = 0, vAdjCnt = 0, vAdjReset = 0;
    unsigned int crtcVSync = 0, vSyncCnt = 0, vSyncReset = 0;
    unsigned int vCount = 0, vReset = 0;
    unsigned int sCount = 0, sReset = 0;
    //  Display control
    WORD memAdd = 0, maReg=0;
    int dispEn=0, vSyncBlank=0;
    // Ga
    int gaHSync = 0;
    // VDU control
    int vduRow=0;
    int vduCol=0;
    bool vduHSync = false, vduVSync = false;
    int vduVSyncCnt = 0;
    // CRTC changes
    unsigned long crtcChanges = 0;

    // Hi-res loop state
    bool hScroll;
    bool vduVSyncFront;
    bool vduHSyncFront;
    WORD prevMemAdd;
    bool lineChanged;
    RAW32 *renderPt32;
    int loopDepth = 0;

    CpcConsole &console;
};
