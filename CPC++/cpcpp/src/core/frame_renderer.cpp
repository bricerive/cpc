// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// frame_renderer.cc - CPC frame rendering

#include "frame_renderer.h"
#include "ga.h"
#include "infra/error.h"
#include "cpcendian.h"

// Notes:
//
//-The living Daylights
// It uses Out to reset the hCnt. This proves that the hCnt is not &0x1F but =0 on that Out.

struct RenderFull {
    inline static void RenderBothData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        RAW64 v0,v1,v2,v3;
        v0 = valP[0];
        v1 = valP[1];
        v2 = valP[2];
        v3 = valP[3];
        pixP[0] = pixP[4] = v0;
        pixP[1] = pixP[5] = v1;
        pixP[2] = pixP[6] = v2;
        pixP[3] = pixP[7] = v3;
        pixP2[0] = pixP2[4] = v0;
        pixP2[1] = pixP2[5] = v1;
        pixP2[2] = pixP2[6] = v2;
        pixP2[3] = pixP2[7] = v3;
    }
    inline static void RenderLeftData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        pixP2[0] = pixP[0] = valP[0];
        pixP2[1] = pixP[1] = valP[1];
        pixP2[2] = pixP[2] = valP[2];
        pixP2[3] = pixP[3] = valP[3];
    }
    inline static void RenderRightData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        pixP2[4] = pixP[4] = valP[0];
        pixP2[5] = pixP[5] = valP[1];
        pixP2[6] = pixP[6] = valP[2];
        pixP2[7] = pixP[7] = valP[3];
    }
    inline static void RenderNextPix(RAW64 *&pixP, RAW64 *&pixP2) {
        pixP+=8; pixP2+=8;
    }
    inline static void RenderGetLeft(const RAW64 *&dataLeft, RAW64 *crtPixCodeBuff, int val) {
        dataLeft = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderGetRight(const RAW64 *&dataRight, RAW64 *crtPixCodeBuff, int val) {
        dataRight = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderFirstPix(const CpcBase::RenderInfo &info, int row, int col, RAW64 *&pixP, RAW64 *&pixP2) {
        pixP = As64(info.dib.Pt(row*2)+col*8*32/8);
        pixP2 = As64(info.dib.Pt(row*2+1)+col*8*32/8);
    }
};

struct RenderSkip {
    inline static void RenderBothData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        pixP[4] = pixP[0] = valP[0];
        pixP[5] = pixP[1] = valP[1];
        pixP[6] = pixP[2] = valP[2];
        pixP[7] = pixP[3] = valP[3];
    }
    inline static void RenderLeftData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        pixP[0] = valP[0];
        pixP[1] = valP[1];
        pixP[2] = valP[2];
        pixP[3] = valP[3];
    }
    inline static void RenderRightData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        pixP[4] = valP[0];
        pixP[5] = valP[1];
        pixP[6] = valP[2];
        pixP[7] = valP[3];
    }
    inline static void RenderNextPix(RAW64 *&pixP, RAW64 *&pixP2) {
        pixP+=8;
    }
    inline static void RenderGetLeft(const RAW64 *&dataLeft, RAW64 *crtPixCodeBuff, int val) {
        dataLeft = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderGetRight(const RAW64 *&dataRight, RAW64 *crtPixCodeBuff, int val) {
        dataRight = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderFirstPix(const CpcBase::RenderInfo &info, int row, int col, RAW64 *&pixP, RAW64 *&pixP2) {
        pixP = As64(info.dib.Pt(row*2)+col*8*32/8);
    }
};

struct RenderSmall {
    inline static void RenderBothData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        const RAW32 *leftP = As32(valP);
        *(As32(pixP)+4) = *(As32(pixP)+0) = leftP[0];
        *(As32(pixP)+5) = *(As32(pixP)+1) = leftP[2];
        *(As32(pixP)+6) = *(As32(pixP)+2) = leftP[4];
        *(As32(pixP)+7) = *(As32(pixP)+3) = leftP[6];
    }
    inline static void RenderLeftData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        const RAW32 *leftP = As32(valP);
        *(As32(pixP)+0) = leftP[0];
        *(As32(pixP)+1) = leftP[2];
        *(As32(pixP)+2) = leftP[4];
        *(As32(pixP)+3) = leftP[6];
    }
    inline static void RenderRightData(RAW64 *pixP, RAW64 *pixP2, const RAW64 *valP) {
        const RAW32 *rightP = As32(valP);
        *(As32(pixP)+4) = rightP[0];
        *(As32(pixP)+5) = rightP[2];
        *(As32(pixP)+6) = rightP[4];
        *(As32(pixP)+7) = rightP[6];
    }
    inline static void RenderNextPix(RAW64 *&pixP, RAW64 *&pixP2) {
        pixP+=4;
    }
    inline static void RenderGetLeft(const RAW64 *&dataLeft, RAW64 *crtPixCodeBuff, int val) {
        dataLeft = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderGetRight(const RAW64 *&dataRight, RAW64 *crtPixCodeBuff, int val) {
        dataRight = crtPixCodeBuff + (val<<2);
    }
    inline static void RenderFirstPix(const CpcBase::RenderInfo &info, int row, int col, RAW64 *&pixP, RAW64 *&pixP2) {
        pixP = As64(info.dib.Pt(row) + col/2*8*32/8);
    }
};

void FrameRenderer::RenderFrame(const CpcBase::RenderInfo &info, CpcFrame &frame, bool force)
{
    switch (info.renderMode) {
        case 0: return RenderFrame<RenderFull>(info,force,frame);
        case 1: return RenderFrame<RenderSkip>(info,force,frame);
        case 2: return RenderFrame<RenderSmall>(info,force,frame);
        default: ERR_THROW("Internal error");
    }
}

void FrameRenderer::ResetLuts()
{
    for (int i=0; i<NB_PAL; i++) {
        modlutBuff[i] = GaColors();
        sortedPal[i] = i;
    }
    crtPal = sortedPal[0];
    crtPixCodeBuff = pixCodeBuff[crtPal];
}


template<typename RenderDetails>
void FrameRenderer::RenderFrame(const CpcBase::RenderInfo &info, bool force, CpcFrame &frame)
{
    if (!frame.Rendered())
        return;
    RAW32 data;
    GaColors *gaColors = frame.RenderColors();
    GaBorder *gaBorder = frame.RenderBorder();

    int newRenderMode = info.renderMode;

    if (!gaColors->Activated()) { // make sure the color is activated once
        SetPixColors(*gaColors, info);
        gaColors->Activated(true);
    }
    if (!gaBorder->Activated()) { // make sure the border is activated once
        SetPixBorder(*gaBorder, info);
        gaBorder->Activated(true);
    }

    // pre-compute clipping in render data steps
    int leftClip = info.clip.Left()/8;
    int rightClip = info.clip.Right()/8;
    int topClip = info.clip.Top()/2+Ga::vduVSyncVal;
    int bottomClip = info.clip.Bottom()/2+Ga::vduVSyncVal;

    // if the rendering parameters changed, update all the pixel data
    if (newRenderMode!=lastRenderMode || info.lut!=lastLut) {
        ResetLuts();
        SetPixColors(*gaColors, info);
        SetPixBorder(*gaBorder, info);
        SetPixSync(info);
        force=true;
        lastLut=info.lut;
    }
    UINT8 *newRenderBase = info.dib.Pt(0);

    // If we're in skip mode, fill odd lines with black
    if (newRenderMode==CpcBase::RenderMode::SKIP && (newRenderMode!=lastRenderMode || newRenderBase!=lastRenderBase)) {
        force=true;
        for (int row=info.clip.Top(); row*2<=info.clip.Bottom(); row++) {
            RAW64 *pixP = As64(info.dib.Pt(row*2+1)+leftClip*8*32/8), dummy[8], *pixP2=dummy;
            for (int col=leftClip; col<=rightClip; col+=2) {
                RenderDetails::RenderBothData(pixP, pixP2, black);
                RenderDetails::RenderNextPix(pixP, pixP2);
            }
        }
    }

    lastRenderMode = newRenderMode;
    lastRenderBase = newRenderBase;

    // skip to first visible row, then display visible rows
    int vduRow = topClip;
    int lastRow = std::min(frame.LastLine(), bottomClip);
    for (; vduRow<=lastRow; vduRow++) {

        // See if we really have to render that line
        if (!force && frame.LineUnchanged(vduRow)) {
            Phase eolPhase = frame.LineEndPhase(vduRow);
            if (gaBorder->ValidUntil()>=eolPhase && gaColors->ValidUntil()>=eolPhase)
                continue;
        }

        int vduCol = leftClip;
        const RAW32 *dataP = frame.LineData(vduRow) + (Ga::vduHSyncVal+vduCol);
        Phase phase = frame.LineStartPhase(vduRow) + CpcDuration::FromUs(Ga::vduHSyncVal+vduCol);
        RAW64 *pixP, *pixP2;
        RenderDetails::RenderFirstPix(info, vduRow-Ga::vduVSyncVal, vduCol, pixP, pixP2);

        int lastCol = std::min(static_cast<int>(vduCol+2*(frame.LineEol(vduRow)-dataP)), rightClip);
        for (; vduCol<=lastCol; vduCol+=2) {
            if (frame.LineHScroll(vduRow)) {
                // Do left
                data = *(dataP-1);
                if (data & 0xFFFF0000) {
                    if (data & CpcFrame::FlagBorder) { // border
                        if (phase-CpcDuration::FromUs(1) > gaBorder->ValidUntil())
                            UpdateBorderValue(gaBorder, phase-CpcDuration::FromUs(1), info);
                        RenderDetails::RenderLeftData(pixP, pixP2, border);
                    } else if (data & (CpcFrame::FlagHSync|CpcFrame::FlagVSync)) { // sync black
                        RenderDetails::RenderLeftData(pixP, pixP2, black);
                    }
                } else { // display data
                    if (phase-CpcDuration::FromUs(1) > gaColors->ValidUntil()) // Update ink values if needed
                        UpdateColorValues(gaColors, phase-CpcDuration::FromUs(1), info);
                    int val = Unpack::WORD_BYTE_1(data);
                    const RAW64 *dataLeft;
                    RenderDetails::RenderGetLeft(dataLeft, crtPixCodeBuff, val);
                    RenderDetails::RenderLeftData(pixP, pixP2, dataLeft);
                }
                // Do right
                data = *(dataP);
                if (data & 0xFFFF0000) {
                    if (data & CpcFrame::FlagBorder) { // border
                        if (phase > gaBorder->ValidUntil())
                            UpdateBorderValue(gaBorder, phase, info);
                        RenderDetails::RenderRightData(pixP, pixP2, border);
                    } else if (data & (CpcFrame::FlagHSync|CpcFrame::FlagVSync)) { // sync black
                        RenderDetails::RenderRightData(pixP, pixP2, black);
                    }
                } else { // display data
                    if (phase > gaColors->ValidUntil()) // Update ink values if needed
                        UpdateColorValues(gaColors, phase, info);
                    int val = Unpack::WORD_BYTE_2(data);
                    const RAW64 *dataRight;
                    RenderDetails::RenderGetRight(dataRight, crtPixCodeBuff, val);
                    RenderDetails::RenderRightData(pixP, pixP2, dataRight);
                }
                dataP++;
                phase+=CpcDuration::FromUs(1);
                RenderDetails::RenderNextPix(pixP, pixP2);
            } else {
                data = *dataP++;
                if (data & 0xFFFF0000) {
                    if (data & CpcFrame::FlagBorder) { // border
                        if (phase>gaBorder->ValidUntil())
                            UpdateBorderValue(gaBorder, phase, info);
                        RenderDetails::RenderBothData(pixP, pixP2, border);
                    } else if (data & (CpcFrame::FlagHSync|CpcFrame::FlagVSync)) { // sync black
                        RenderDetails::RenderBothData(pixP, pixP2, black);
                    }
                } else { // display data
                    if (phase>gaColors->ValidUntil()) // Update ink values if needed
                        UpdateColorValues(gaColors, phase, info);
                    int val = Unpack::WORD_BYTE_2(data);
                    const RAW64 *dataLeft;
                    RenderDetails::RenderGetLeft(dataLeft, crtPixCodeBuff, val);
                    RenderDetails::RenderLeftData(pixP, pixP2, dataLeft);
                    val = Unpack::WORD_BYTE_1(data);
                    const RAW64 *dataRight;
                    RenderDetails::RenderGetRight(dataRight, crtPixCodeBuff, val);
                    RenderDetails::RenderRightData(pixP, pixP2, dataRight);
                }
                phase+=CpcDuration::FromUs(1);
                RenderDetails::RenderNextPix(pixP, pixP2);
            }
        }
        for (; vduCol<=rightClip; vduCol+=2) {
            RenderDetails::RenderBothData(pixP, pixP2, black);
            RenderDetails::RenderNextPix(pixP, pixP2);
        }
    }

    // If the frame's last line is before the end of the window, draw black
    while (vduRow<=bottomClip) { // early VSync (Logon The Demo: menu)
        int vduCol=leftClip;
        RAW64 *pixP, *pixP2;
        RenderDetails::RenderFirstPix(info, vduRow-Ga::vduVSyncVal, vduCol, pixP, pixP2);
        for (; vduCol<=rightClip; vduCol+=2) {
            RenderDetails::RenderBothData(pixP, pixP2, black);
            RenderDetails::RenderNextPix(pixP, pixP2);
        }
        vduRow++;
    }
}

// Macros for mixing screen bit patterns into ink numbers
static BYTE MIX01(int i) {return ((i&0x02)<<2)|((i&0x20)>>3)|((i&0x08)>>2)|((i&0x80)>>7); }
static BYTE MIX02(int i) {return ((i&0x01)<<3)|((i&0x10)>>2)|((i&0x04)>>1)|((i&0x40)>>6); }
static BYTE MIX11(int i) {return ((i&0x08)>>2)|((i&0x80)>>7); }
static BYTE MIX12(int i) {return ((i&0x04)>>1)|((i&0x40)>>6); }
static BYTE MIX13(int i) {return ((i&0x02)<<0)|((i&0x20)>>5); }
static BYTE MIX14(int i) {return ((i&0x01)<<1)|((i&0x10)>>4); }

void FrameRenderer::SetPixColors(const GaColors &gc, const CpcBase::RenderInfo &info)
{
    // Check if we are coming back to a previously used palette
    for (int i=0; i<NB_PAL; i++) {
        if (gc == modlutBuff[sortedPal[i]]) {
            // We found that palette.
            // move that palette to the front of the list (for faster future search)
            int k = sortedPal[i];
            for (int j=i; j>0; j--)
                sortedPal[j] = sortedPal[j-1];
            sortedPal[0]=k;
            crtPal = sortedPal[0];
            crtPixCodeBuff = pixCodeBuff[crtPal];
            return;
        }
    }
    //LOG_STATUS("[Ga] Recomputing modelut table");
    int k = sortedPal[NB_PAL-1];
    for (int i=NB_PAL-1; i>0; i--)
        sortedPal[i] = sortedPal[i-1];
    sortedPal[0] = k;
    crtPal = sortedPal[0];
    crtPixCodeBuff = pixCodeBuff[crtPal];
    modlutBuff[sortedPal[0]] = gc;

    RAW32 *pixCodeP = As32(crtPixCodeBuff);
    const RAW32 *lut = info.lut;
    BYTE v0,v1,v;
    switch (gc.Mode()) {
        case 0: // 2 big pixels (width=4)
            for (int i=0; i<256; i++) {
                v0=gc.Color(MIX01(i));
                pixCodeP[0]=pixCodeP[1]=pixCodeP[2]=pixCodeP[3] = lut[v0];
                v0=gc.Color(MIX02(i));
                pixCodeP[4]=pixCodeP[5]=pixCodeP[6]=pixCodeP[7] = lut[v0];
                pixCodeP+=8;
            }
            break;
        case 1: // 4 medium pixels (width=2)
            for (int i=0; i<256; i++) {
                v0 = gc.Color(MIX11(i));
                v1 = gc.Color(MIX12(i));
                pixCodeP[0]=pixCodeP[1] = lut[v0];
                pixCodeP[2]=pixCodeP[3] = lut[v1];
                v0 = gc.Color(MIX13(i));
                v1 = gc.Color(MIX14(i));
                pixCodeP[4]=pixCodeP[5] = lut[v0];
                pixCodeP[6]=pixCodeP[7] = lut[v1];
                pixCodeP+=8;
            }
            break;
        case 2: // eight small pixels (width=1)
            v0 = gc.Color(0);
            v1 = gc.Color(1);
            for (int i=0; i<256; i++) {
                for (int k=0, j=i; k<8; k++, j<<=1) {
                    v = (j&0x80)? v1: v0;
                    pixCodeP[k] = lut[v];
                }
                pixCodeP+=8;
            }
            break;
        default:
            // Mode 3: 160x200 pixels with 4 colors (2bpp) (this is not an official mode, but rather a side-effect of the hardware)
            for (int i=0; i<256; i++) {
                v0=gc.Color(MIX01(i));
                pixCodeP[0]=pixCodeP[1]=pixCodeP[2]=pixCodeP[3] = lut[v0];
                v0=gc.Color(MIX02(i));
                pixCodeP[4]=pixCodeP[5]=pixCodeP[6]=pixCodeP[7] = lut[v0];
                pixCodeP+=8;
            }
            break;
    }
}

void FrameRenderer::SetPixBorder(const GaBorder &gb, const CpcBase::RenderInfo &info)
{
    RAW32 *borderP = As32(border);
    const RAW32 *lut = info.lut;
    borderP[0]=borderP[1]=borderP[2]=borderP[3]=borderP[4]=borderP[5]=borderP[6]=borderP[7] = lut[gb.Color()]; // Set border
}

void FrameRenderer::UpdateBorderValue(GaBorder *&gaBorder, Phase phase,const CpcBase::RenderInfo &info)
{
    while (phase>gaBorder->ValidUntil())
        gaBorder++;
    SetPixBorder(*gaBorder, info);
}

void FrameRenderer::UpdateColorValues(GaColors *&gaColors, Phase phase, const CpcBase::RenderInfo &info)
{
    while (phase>gaColors->ValidUntil())
        gaColors++;
    SetPixColors(*gaColors, info);
}

void FrameRenderer::SetPixSync(const CpcBase::RenderInfo &info)
{
    RAW32 *blackP = As32(black);
    const RAW32 *lut = info.lut;
    blackP[0]=blackP[1]=blackP[2]=blackP[3]=blackP[4]=blackP[5]=blackP[6]=blackP[7] = lut[32]; // Set sync
}

