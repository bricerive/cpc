// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// frame.cpp - Class to store a rendered frame
#include "infra/error.h"
#include "cpcframe.h"
#include "ga.h"

const unsigned long CpcFrame::FlagBorder=0x10000;
const unsigned long CpcFrame::FlagHSync=0x20000;
const unsigned long CpcFrame::FlagVSync=0x40000;
const int CpcLine::guard=32;


static const int minNopsPerOut=1; // The Demo intro goes up with OUT H,(C); OUT L,(C); OUT D,(C); OUT E,(C); OUT C,(C)
static const int maxNbColors=Ga::maxNopsPerFrame/minNopsPerOut;

CpcLine::CpcLine()
    : data(Ga::maxNopPerLine + guard)
{}

void CpcLine::Start(Phase phase, RAW32 *&pt, const GaColors &colors, const GaBorder &border)
{
    startPhase = phase;
    pt=data.data();
    unchanged = true;
    hScroll = false;
    startBorder = &border;
    startColors = &colors;
}

void CpcLine::Done(Phase phase, RAW32 *pt, const GaColors &colors, const GaBorder &border, bool changed, bool hScroll_)
{
    if (pt>=data.data()+Ga::maxNopPerLine+guard)
        return;
    ASSERT_THROW(pt<data.data()+Ga::maxNopPerLine+guard)("CpcLine overflow");

    endPhase=phase;
    eol=pt;
    hScroll=hScroll_;

    // We have changed if any of the following
    // Data changed since last time
    // Colors changed during last time
    // Colors changed during this time
    // Colors changed between last time and this time
    // Border changed during last time
    // Border changed during this time
    // Border changed between last time and this time
    if (changed
        || !lastColors.Activated() || &colors != startColors || colors!=lastColors
        || !lastBorder.Activated() || &border != startBorder || border!=lastBorder)
        unchanged=false;

    // Colors did not change this time - remember
    if (&colors == startColors) {
        lastColors = colors;
        lastColors.Activated(true);
    } else
        lastColors.Activated(false);

    // Border did not change this time - remember
    if (&border == startBorder) {
        lastBorder = border;
        lastBorder.Activated(true);
    } else
        lastBorder.Activated(false);
}

const int CpcFrame::guard=32;

CpcFrame::CpcFrame()
    : renderColors(maxNbColors) // max change rate (OUT)
    , renderColorsIdxLast(0)    // start with one dummy in 0
    , renderBorder(maxNbColors) // max change rate (OUT)
    , renderBorderIdxLast(0) // start with one dummy in 0
    , lastLine(0)
    , lines(Ga::maxLinesPerFrame + guard)
    , rendered(false)
{}

CpcFrame::~CpcFrame()
{}

void CpcFrame::FrameStart(Phase phase)
{
    rendered=false;

    // Start with the last color info from previous frame
    renderColors[0] = CrtRenderColors();
    renderColorsIdxLast = 0;
    // Start with the last border info from previous frame
    renderBorder[0] = CrtRenderBorder();
    renderBorderIdxLast = 0;
}

void CpcFrame::FrameDone(int lineNo)
{
    lastLine = lineNo;
    rendered=true;
}

void CpcFrame::NewColors(Phase phase, BYTE mode, const BYTE *colors)
{
    CrtRenderColors().ValidUntil(phase-CpcDuration::FromUs(1));
    renderColorsIdxLast++;
    ASSERT_THROW(renderColorsIdxLast<maxNbColors)("NewColors overflow");
    CrtRenderColors().SetVal(mode, colors);
}

void CpcFrame::NewBorder(Phase phase, BYTE color)
{
    CrtRenderBorder().ValidUntil(phase-CpcDuration::FromUs(1));
    renderBorderIdxLast++;
    ASSERT_THROW(renderBorderIdxLast<maxNbColors)("NewBorder overflow");
    CrtRenderBorder().SetVal(color);
}

void GaColors::SetVal(BYTE mode_, const BYTE *colors_)
{
    activated=false;
    validUntil=Phase::Forever();
    mode=mode_;
    RAW64 *dst = As64(colors);
    const RAW64 *src = As64(colors_);
    *dst++ = *src++;
    *dst = *src;
}

GaColors::GaColors(): mode(1), validUntil(0), activated(false)
{
    validUntil = Phase::Forever(); // that lasts until another comes
    for (int i=0; i<16; i++) colors[i]=4; // dark blue
}

bool GaColors::operator==(const GaColors &rhs)const
{
    const RAW64 *dst = As64(colors);
    const RAW64 *src = As64(rhs.colors);
    return rhs.mode==mode && dst[0]==src[0] && dst[1]==src[1];
}

bool GaColors::operator!=(const GaColors &rhs)const
{
    return ! (*this == rhs);
}

GaBorder::GaBorder()
: validUntil(0), color(0), activated(0)
{
    validUntil = Phase::Forever(); // that lasts until another comes
    activated = false;
    color=4; // dark blue
}

void GaBorder::SetVal(BYTE color_)
{
    activated=false;
    validUntil=Phase::Forever();
    color=color_;
}

bool GaBorder::operator==(const GaBorder &rhs)const
{
    return rhs.color==color;
}

bool GaBorder::operator!=(const GaBorder &rhs)const
{
    return rhs.color!=color;
}
