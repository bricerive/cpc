#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file psg.h - Programable sound generator emulation
#include "cpcphase.h"
#include "cpctypes.h"
#include "cpcconsole.h"
#include "cpc_base.h"

class Kbd;
class Centro;
struct PsgState;

/// Emulation of the AY38910 PSG
/// Note that it does not inherit from Peripheral because it is accessed through the PIO
class Psg {
 public:

    /// ctor
    /// \param kbd the keyboard, as it is read through the Psg port A
    /// \param centro for digiblaster emulation
    /// \param console we need the console to send sound buffer when ready
    /// \param trapFlag polled by monitor to check if a  condition occurs
    Psg(Kbd &kbd, Centro &centro, CpcConsole &console, TrapFlag &trapFlag);

    // Chip control
    void Reset();
    void Put(BYTE val);
    BYTE Get();
    void Ctrl(BYTE val);

    // IO ports
    BYTE GetPortA()const;
    BYTE GetPortB()const;

    // PSG execution clock at scanline resolution
    CpcDuration Step(Phase phase);

    // Serialization
    void GetState(PsgState &state)const;
    void SetState(const PsgState &state);

    // Sound control
    void Stereo(bool val) {stereo = val;}

    void Digiblaster(bool val) {digiblaster=val;}
    void SoundOnOff(bool on);
    void ChannelsOnOff(int chanA, int chanB, int chanC);
    void DropOn(bool val) {dropOn=val;}
    void SetSoundInfo(const CpcBase::SoundInfo &sndInfo);

 private:
    Psg &operator=(const Psg&);
    Psg(const Psg&);

    bool dropOn=false;

    // Run-time options set by OS-specific code
    CpcBase::SoundInfo sndInfo;

    // Sound states
    bool stereo = false;
    bool digiblaster=false;
    bool makeSounds = false;
    BYTE vol=0;
    bool channelA = 0, channelB=0, channelC=0;
    UINT32 idx=0;

    // keep track of rounding error
    double deltaUs=0;
    Phase lastPhase;

    // PSG emulation
    int mode = 0;
    BYTE crtDatIn=0,crtDatOut=0;
    BYTE crtAdd=0;
    BYTE reg[16];
    BYTE portA = 0, portB=0;
    long perA=0, perB=0, perC=0, perN=0, perE=0;
    long cntA = 0, cntB = 0, cntC = 0, cntN = 0, cntE = 0;
    int sigA=0, sigB=0, sigC=0, sigN=0;
    unsigned int psgRand = 0;
    BYTE sigE = 0;
    int envHold = 0, envAlt = 0;
    int continu = 0, attack = 0, alternate = 0, hold = 0;

    // IO support
    Kbd &kbd;
    Centro &centro;
    void Write(BYTE val);
    BYTE Read();

    // Monitor support
    TrapFlag &trapFlag;

    CpcConsole &console;
};
