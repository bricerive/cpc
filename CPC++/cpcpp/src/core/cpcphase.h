#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// cpcphase.h - Class for phase counting
#include <boost/serialization/nvp.hpp>

/// CPC Clock cycles (4 per Mhz, or 250 ns)
class CpcClock {
public:
    static const int CLOCK_MHZ;
};

/// CPC time duration in clock cycles (4Mhz, or 250 ns)
class CpcDuration: public CpcClock {
public:
    struct Ms {
        Ms(int ms)
            : ms(ms)
        {}
        int ms;
    };
    struct Us {
        Us(int us)
            : us(us)
        {}
        int us;
    };

    CpcDuration(const Us &us);
    CpcDuration(const Ms &ms);
    long Val() const;

    static CpcDuration FromMs(int ms) { return CpcDuration(Ms(ms)); }
    static CpcDuration FromUs(int us) { return CpcDuration(Us(us)); }

private:
    long val;
};

/// CPC time phase in clock cycles (4Mhz, or 250 ns)
class Phase: public CpcClock {
public:
    Phase(int init = 0);
    long PhaseUs() const;

    Phase &operator=(const Phase &rhs);

    Phase operator+(const CpcDuration &rhs) const;
    Phase operator-(const CpcDuration &rhs) const;
    int operator-(const Phase &rhs) const { return ElapsedSince(rhs); }

    Phase operator+=(const CpcDuration &rhs);

    bool operator==(const Phase &rhs) const;
    bool operator>=(const Phase &rhs) const;
    bool operator>(const Phase &rhs) const;
    bool operator<(const Phase &rhs) const;

    // operator long()const {return val;}

    unsigned long ElapsedUs(const Phase &prev) const;
    static Phase FromMs(long ms);
    static Phase FromUs(long us);
    static Phase Forever();

    template<class A>
    void serialize(A &ar, unsigned int)
    {
        ar &BOOST_SERIALIZATION_NVP(val);
    }

private:
    unsigned long ElapsedSince(const Phase &before) const;

    static const int MAX_PHASE;
    long val;
};
