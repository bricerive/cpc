// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
// Z80PPC.cc - Switch between C++ and Asm versions of the Z80 emulation



#include "infra/error.h"
#include "z80.h"
#include "mmu.h"
#include "iospace.h"
#include "cpc_state.h"
#include "infra/log.h"
#include <memory>

using boost::format;

// Compilation flags

//  from ACCC1.1
// INSTRUCTIONS    DUREE   PRISE EN COMPTE I/O
//
// OUT (C),r8      4 μsec  3ème μsec
// OUT (n),A       3 μsec  3ème μsec
// OUT (C),0       4 μsec  3ème μsec
// OUTI/OUTD       5 μsec  5ème μsec
//
// IN r8,(C)       4 μsec  4ème μsec
// IN A,(n)        3 μsec  3ème μsec
// INI/IND         5 μsec  4ème μsec
static const int DELAY_OUT_C=2; // 4 nops / delay=2    Good for Hi-Tech demo
static const int DELAY_OUT_N=2; // 3 nops / delay=2
static const int DELAY_OUTI=4;  // 5 nops / delay=4
static const int DELAY_OUTIR=4; // 5/6 nops / delay=4?
static const int DELAY_IN_C=3;
static const int DELAY_IN_N=2;
static const int DELAY_INI=3;  // 5 nops / delay=3?
static const int DELAY_INIR=3; // 5/6 nops / delay=3

// Common code

// Power on defaults
Z80::Z80(Mmu &mmu, IoSpace &ioSpace, TrapFlag &trapFlag)
    :  pcaf(), spbc(), hlde(), ixiy(), afbc2(), hlde2(), irfc()
    , mmu(mmu), ioSpace(ioSpace), trapFlag(trapFlag)
{
    if constexpr(Z80_TRACE)
      daOut=0;
    MakeFlagTables();
    Z80::Reset();
    CHK=0;
}

Z80::~Z80()
{
    if constexpr(Z80_TRACE) {
  if (daOut) fclose(daOut);
    }
}

void Z80::Reset()
{
    // registers will have random values after a cold-boot depending on
    // time elapsed since power was removed. It probably tends to FF's everywhere
    // after power has drained out.
    /*
     2.4    Power on defaults
     Matt4 has done some excellent research on this. He found that AF and SP are always set to FFFFh after a reset,
     and all other registers are undefined (different depending on how long the CPU has been powered off, different for
     different Z80 chips). Of course the PC should be set to 0 after a reset, and so should the IFF1 and IFF2 flags
     (otherwise strange things could happen). Also since the Z80 is 8080 compatible, interrupt mode is probably 0.
     Probably the best way to simulate this in an emulator is set PC, IFF1, IFF2, IM to 0 and set all other registers to FFFFh.
     After a hardware reset, or after power on, the R register is reset to 0.
     */
    AF = BC = DE = HL = AF2 = BC2 = DE2 = HL2 = IX = IY = SP = PC = 0xFFFF;
    PC = 0;
    I = R = FLG = CHK = 0;
    memReg = 0xFFFF;
}

void Z80::GetState(CpuState &state)const {
    state.af = AF;
    state.bc = BC;
    state.de = DE;
    state.hl = HL;
    state.af2 = AF2;
    state.bc2 = BC2;
    state.de2 = DE2;
    state.hl2 = HL2;
    state.ix = IX;
    state.iy = IY;
    state.pc = PC;
    state.sp = SP;
    state.memReg = memReg;
    state.i = I;
    state.r = R;
    state.im = static_cast<UINT8>(Im());
    state.iff1 = static_cast<UINT8>(Iff1());
    state.iff2 = static_cast<UINT8>(Iff2());
    state.it = static_cast<UINT8>(Int());
    state.nmi = static_cast<UINT8>(Nmi());
}

void Z80::SetState(const CpuState &state) {
    AF=state.af;
    BC=state.bc;
    DE=state.de;
    HL=state.hl;
    AF2=state.af2;
    BC2=state.bc2;
    DE2=state.de2;
    HL2=state.hl2;
    IX=state.ix;
    IY=state.iy;
    PC=state.pc;
    SP=state.sp;
    memReg = state.memReg;
    I = state.i;
    R=state.r;
    Im(state.im);
    Iff1(state.iff1);
    Iff2(state.iff2);
    Halt(0);
    CHK=0;
}

void Z80::RemoveCondition()
{
    if (conditions.empty()) return;
    size_t n = conditions.size();
    conditions.resize(n-1);
}

UINT16 CpuCondition::Pc(Z80 &cpu) {return cpu.PC;}
UINT16 CpuCondition::Sp(Z80 &cpu) {return cpu.SP;}
Phase CpuCondition::LastIntAck(Z80 &cpu) {return cpu.lastIntAck;}


// F register handling
BYTE Z80::incFlagsTable[256];
BYTE Z80::decFlagsTable[256];
BYTE Z80::orFlagsTable[256];
BYTE Z80::andFlagsTable[256];
BYTE Z80::parTable[256];
BYTE Z80:: carryTable[8] = {0,0,C_M,0,C_M,0,C_M,C_M};
BYTE Z80:: hCarryTable[8] = {0,0,H_M,0,H_M,0,H_M,H_M};
BYTE Z80:: overflowTable[8] = {0,PV_M,0,0,0,0,PV_M,0};
BYTE Z80:: carryTableSub[8] = {0,C_M,C_M,C_M,0,0,0,C_M};
BYTE Z80:: hCarryTableSub[8] = {0,H_M,H_M,H_M,0,0,0,H_M};
BYTE Z80:: overflowTableSub[8] = {0,0,0,PV_M,PV_M,0,0,0};
BYTE Z80:: signTable[8] = {0,S_M,0,S_M,0,S_M,0,S_M};

void Z80::MakeFlagTables()
{
    int i;
    int val;

    for (i=0; i<256; i++) {
        val = ((i&0x80)?1:0) + ((i&0x40)?1:0) + ((i&0x20)?1:0) + ((i&0x10)?1:0) + ((i&0x08)?1:0) + ((i&0x04)?1:0) + ((i&0x02)?1:0) + ((i&0x01)?1:0) ;
        parTable[i] = (val&1)? 0: PV_M;
    }
    for (i=0; i<256; i++) {
        val = (i?0:Z_M)|((i==0x80)?PV_M:0)|(i&(S_M|X_M))|(((i&0x0F)-1)&H_M);
        incFlagsTable[i]=static_cast<BYTE>(val);
    }
    for (i=0; i<256; i++) {
        val = (i?0:Z_M)|((i==0x7F)?PV_M:0)|(i&(S_M|X_M))|N_M|(((i&0x0F)+1)&H_M);
        decFlagsTable[i]=static_cast<BYTE>(val);
    }
    for (i=0; i<256; i++) {
        val = (i?0:Z_M)|parTable[i]|(i&(S_M|X_M));
        orFlagsTable[i]=static_cast<BYTE>(val);
    }
    for (i=0; i<256; i++) {
        val = (i?0:Z_M)|parTable[i]|(i&(S_M|X_M))|H_M;
        andFlagsTable[i]=static_cast<BYTE>(val);
    }
}

//  anotherlin / z80emu
static const unsigned char SZYX_FLAGS_TABLE[256] = {

    0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28,
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28,
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28, 0x28,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0,
    0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8,
    0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0,
    0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0,
    0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8,
    0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0, 0xa0,
    0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8, 0xa8,

};

//  anotherlin / z80emu
static const unsigned char SZYXP_FLAGS_TABLE[256] = {

    0x44, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
    0x08, 0x0c, 0x0c, 0x08, 0x0c, 0x08, 0x08, 0x0c,
    0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
    0x0c, 0x08, 0x08, 0x0c, 0x08, 0x0c, 0x0c, 0x08,
    0x20, 0x24, 0x24, 0x20, 0x24, 0x20, 0x20, 0x24,
    0x2c, 0x28, 0x28, 0x2c, 0x28, 0x2c, 0x2c, 0x28,
    0x24, 0x20, 0x20, 0x24, 0x20, 0x24, 0x24, 0x20,
    0x28, 0x2c, 0x2c, 0x28, 0x2c, 0x28, 0x28, 0x2c,
    0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
    0x0c, 0x08, 0x08, 0x0c, 0x08, 0x0c, 0x0c, 0x08,
    0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
    0x08, 0x0c, 0x0c, 0x08, 0x0c, 0x08, 0x08, 0x0c,
    0x24, 0x20, 0x20, 0x24, 0x20, 0x24, 0x24, 0x20,
    0x28, 0x2c, 0x2c, 0x28, 0x2c, 0x28, 0x28, 0x2c,
    0x20, 0x24, 0x24, 0x20, 0x24, 0x20, 0x20, 0x24,
    0x2c, 0x28, 0x28, 0x2c, 0x28, 0x2c, 0x2c, 0x28,
    0x80, 0x84, 0x84, 0x80, 0x84, 0x80, 0x80, 0x84,
    0x8c, 0x88, 0x88, 0x8c, 0x88, 0x8c, 0x8c, 0x88,
    0x84, 0x80, 0x80, 0x84, 0x80, 0x84, 0x84, 0x80,
    0x88, 0x8c, 0x8c, 0x88, 0x8c, 0x88, 0x88, 0x8c,
    0xa4, 0xa0, 0xa0, 0xa4, 0xa0, 0xa4, 0xa4, 0xa0,
    0xa8, 0xac, 0xac, 0xa8, 0xac, 0xa8, 0xa8, 0xac,
    0xa0, 0xa4, 0xa4, 0xa0, 0xa4, 0xa0, 0xa0, 0xa4,
    0xac, 0xa8, 0xa8, 0xac, 0xa8, 0xac, 0xac, 0xa8,
    0x84, 0x80, 0x80, 0x84, 0x80, 0x84, 0x84, 0x80,
    0x88, 0x8c, 0x8c, 0x88, 0x8c, 0x88, 0x88, 0x8c,
    0x80, 0x84, 0x84, 0x80, 0x84, 0x80, 0x80, 0x84,
    0x8c, 0x88, 0x88, 0x8c, 0x88, 0x8c, 0x8c, 0x88,
    0xa0, 0xa4, 0xa4, 0xa0, 0xa4, 0xa0, 0xa0, 0xa4,
    0xac, 0xa8, 0xa8, 0xac, 0xa8, 0xac, 0xac, 0xa8,
    0xa4, 0xa0, 0xa0, 0xa4, 0xa0, 0xa4, 0xa4, 0xa0,
    0xa8, 0xac, 0xac, 0xa8, 0xac, 0xa8, 0xa8, 0xac,

};

// Memory access
BYTE Z80::MemGet(WORD a)const { return mmu.MemRd8(a); }
char Z80::GetOff(WORD nn)const { return mmu.MemRdS(nn); }
WORD Z80::MemGetW(WORD a)const { return mmu.MemRd16(a); }
void Z80::MemPut(WORD a, BYTE v) {mmu.MemWr8(a,v);}
BYTE *Z80::MemPutP(WORD a) {return mmu.MemWr8P(a);}
void Z80::MemPutW(WORD a, WORD w) {mmu.MemWr16(a,w);}

BYTE Z80::GetIo(const Phase &phase, WORD addr) {return ioSpace.Get(phase,addr);}
void Z80::PutIo(const Phase &phase, WORD addr, BYTE val) {ioSpace.Put(phase,addr,val);}

void Z80::Int(int val) {CHK = static_cast<unsigned char>((CHK&~CHK_INT)|(val?CHK_INT:0));}
void Z80::Nmi(int val) {CHK = static_cast<unsigned char>((CHK&~CHK_NMI)|(val?CHK_NMI:0));}

void Z80::Iff1(int val) {FLG = static_cast<unsigned char>((FLG & ~FLG_IFF1) | (val? FLG_IFF1: 0));}
void Z80::Iff2(int val) {FLG = static_cast<unsigned char>((FLG & ~FLG_IFF2) | (val? FLG_IFF2: 0));}
void Z80::Im(int val) {FLG = static_cast<unsigned char>((FLG & ~FLG_IM) | (val&FLG_IM));}
void Z80::Halt(int val) {FLG = static_cast<unsigned char>((FLG & ~FLG_HALT) | (val? FLG_HALT: 0));}

int Z80::Int()const {return (CHK&CHK_INT)?1:0;}
int Z80::Nmi()const {return (CHK&CHK_NMI)?1:0;}
int Z80::Iff1()const {return (FLG &FLG_IFF1)?1:0;}
int Z80::Iff2()const {return (FLG &FLG_IFF2)?1:0;}
int Z80::Im()const {return FLG & FLG_IM;}
int Z80::Halt()const {return (FLG &FLG_HALT)?1:0;}

// About NOP cycles:
// Although I don't quite understand why, using the
// Wait line to arbitrate RAM access between CPU and CRTC has for
// result that each Z80 op starts at the next NOP cycle.
// Actualy, each M cycle starts at the beginning of a NOP cycle.
// I have tried timing series of RET Z, which is a good candidate
// for phase dependant duration as it has 5 T cycles for 1 M cycle.
// But the result is that, in every test I have tried, RET Z takes
// 2 full NOP cycles.
// So, in the UPDTCYCLE macro below, the n parameter is used directly.

// Updates NOP cycles count and R register
// t = T cycles
// m = M cycles
// n = NOP cycles
// r = R register increment
#define UPDTCYCLE2(t, m, n, r) \
    { \
        phase += CpcDuration::FromUs(n); \
        regR += r; \
        goto nextOpCheck; \
    }

Phase Z80::Step(Phase crtPhase)
{
    Phase phase=crtPhase;

    SplitR();

    if (CHK) {
        // DELAYED_IO
        if (CHK & CHK_OI_OUT) { // Delayed IO writes
            CHK &= ~CHK_OI_OUT;
            ioSpace.Put(phase,delayedIoPort,delayedIoVal);
            UPDTCYCLE2(0,0,delayedIoPhase,0)
        }
        if (CHK & CHK_OI_IN) { // Delayed IO reads
            CHK &= ~CHK_OI_IN;
            *delayedIoValDest = ioSpace.Get(phase,delayedIoPort);
            inFlags(*delayedIoValDest);
            UPDTCYCLE2(0,0,delayedIoPhase,0)
        }
        if ((CHK & CHK_INT) && !(CHK & CHK_EI) && Iff1()) {
            if (Halt()) {PC++;Halt(0);} // Step out of HALT
            Iff1(0); Iff2(0); // mask interrupts
            mmu.IntAck(phase); // Acknowledge INT
            lastIntAck=crtPhase; // remember for monitor
            int nops=0;
            int rInc=0;
            switch (Im()) {
                case 0: // IM 0 (read instruction from data bus)
                    SP -= 2;
                    MemPutW(SP, PC);
                    PC = memReg = 0x0038; // RST 38
                                 //    INT and interrupt mode 0 set
                                 //    In this mode, timing depends on the instruction put on the bus. The interrupt processing last 2 clock cycles more than this instruction usually needs.
                                 //    Two typical examples follow:
                                 //
                                 //    a RST n on the data bus, it takes 13 cycles to get to 'n':
                                 //    M1 cycle: 7 ticks
                                 //    acknowledge interrupt and decrement SP
                                 //    M2 cycle: 3 ticks
                                 //    write high byte and decrement SP
                                 //    M3 cycle: 3 ticks
                                 //    write low byte and jump to 'n'
                                 //
                                 //    With a CALL nnnn on the data bus, it takes 19 cycles:
                                 //    M1 cycle: 7 ticks
                                 //    acknowledge interrupt
                                 //    M2 cycle: 3 ticks
                                 //    read low byte of 'nnnn' from data bus
                                 //    M3 cycle: 3 ticks
                                 //    read high byte of 'nnnn' and decrement SP
                                 //    M4 cycle: 3 ticks
                                 //    write high byte of PC to the stack and decrement SP
                                 //    M5 cycle: 3 ticks
                                 //    write low byte of PC and jump to 'nnnn'.
                    nops=5; // Read data bus (returns FF=RST 38) + Fast push +RST 38
                    rInc=2; // ???
                    break;
                case 1: // Undocumented
                    LOG_STATUS("INT Mode 1 undocumented");
                    break;
                case 2: // IM 1 (Perform RST 38)
                    SP -= 2;
                    MemPutW(SP, PC);
                    PC = memReg = 0x0038;
                    //    INT and interrupt mode 1 set
                    //    It takes 13 clock cycles to reach #0038:
                    //
                    //    M1 cycle: 7 ticks
                    //    acknowledge interrupt and decrement SP
                    //    M2 cycle: 3 ticks
                    //    write high byte of PC onto the stack and decrement SP
                    //    M3 cycle: 3 ticks
                    //    write low byte onto the stack and to set PC to #0038.
                    nops=5; // Read data bus ??? + Fast push
                    rInc=1; // This one is confirmed by "Conspiration de l'an III"
                    break;
                case 3: // IM 2 (Read INT vector from data bus)
                    SP -= 2;
                    MemPutW(SP, PC);
                    PC = memReg = MemGetW(((I)<<8) | 0xFF);
                    //    INT and interrupt mode 2 set
                    //    It takes 19 clock cycles to get to the interrupt routine:
                    //
                    //    M1 cycle: 7 ticks
                    //    acknowledge interrupt and decrement SP
                    //    M2 cycle: 3 ticks
                    //    write high byte of PC onto stack and decrement SP
                    //    M3 cycle: 3 ticks
                    //    write low byte onto the stack
                    //    M4 cycle: 3 ticks
                    //    read low byte from the interrupt vector
                    //    M5 cycle: 3 ticks
                    //    read high byte from bus and jump to interrupt routine
                    nops=7; // Read data bus (returns FF) + Fast push
                    rInc=1; // ???
                    break;
                default:
                    ERR_THROW(format("Erroneous interruption mode %d")%Im());
            }
            UPDTCYCLE2(0,0,nops,rInc)
        }
        if (CHK & CHK_EI)
            CHK &= ~CHK_EI;
        if (CHK & CHK_NMI) {
            if (Halt()) {PC++;Halt(0);}
            SP -= 2;
            MemPutW(SP, PC);
            PC = memReg = 0x0066;
            Iff2(Iff1());
            Iff1(0);
            //    NMI
            //    It takes 11 clock cycles to get to #0066:
            //
            //    M1 cycle: 5 T states to do an opcode read and decrement SP
            //    M2 cycle: 3 T states write high byte of PC to the stack and decrement SP
            //    M3 cycle: 3 T states write the low byte of PC and jump to #0066.
            UPDTCYCLE2(0,0,3,2) // Fast push
        }
    }

    if constexpr(Z80_TRACE)
        DoTrace(phase);

    phase = MainOp(phase);

nextOpCheck:
    for (Conditions::iterator i=conditions.begin(); i!=conditions.end(); )
    {
        if ((*i)->Check(*this,crtPhase)) {
            if (!(*i)->Persistant()) {
                i = conditions.erase(i);
            } else ++i;
            trapFlag=TrapFlag::CPU_RES;
        } else ++i;
    }

    // Restore registers
    MergeR();
    return phase;
}

void Z80::DoTrace(Phase phase)const
{
    if (!traceFlag) return;
    if (!daOut) daOut = fopen("CpuTrace.txt","w");
    if (!mmu.IsRam(PC)) return; // only dump RAM code
    char str[128];
    snprintf(str,128,"%10ld %04X ",phase.PhaseUs(),PC);
    da.Disassemble(mmu,PC,str+16,112,true,BC);
    strcat(str,"\n");
    fwrite(str,strlen(str),1,daOut);
}

#define UPDTCYCLE(t, m, n, r) \
    { \
        phase += CpcDuration::FromUs(n); \
        regR += r; \
        return phase; \
    }

Phase Z80::MainOp(Phase phase)
{
    BYTE fTmp, bTmp;
    WORD wTmp;
    signed char dTmp;

    BYTE opCode = MemGet(PC);
    PC++;
    switch(opCode) {
        case 0x00:  // NOP
            UPDTCYCLE(4,1,1,1)
        case 0x01:  // LD BC,NN
            BC=MemGetW(PC); PC+=2; UPDTCYCLE(10,3,3,1)
        case 0x02:  // LD (BC),A
            MemPut(BC, A); memReg=BC+1; memRegH=A;
            UPDTCYCLE(7,2,2,1)
        case 0x03:  // INC BC
            BC++; UPDTCYCLE(6,2,2,1)
        case 0x04:  // INC B
            B++; incFlags(B); UPDTCYCLE(4,1,1,1)
        case 0x05:  // DEC B
            B--; decFlags(B); UPDTCYCLE(4,1,1,1)
        case 0x06:  // LD B,N
            B=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x07:  // RLCA
            fTmp=A>>7; A=(A<<1)|fTmp; rotaFlags(fTmp); UPDTCYCLE(4,1,1,1)
        case 0x08:  // EX AF,AF
            wTmp=AF; AF=AF2; AF2=wTmp;
            UPDTCYCLE(4,1,1,1)
        case 0x09:  // ADD HL,BC
            memReg=HL;
            HL=memReg+BC; add16Flags(memReg,BC,HL); ++memReg; UPDTCYCLE(11,3,3,1)
        case 0x0A:  // LD A,(BC)
            A=MemGet(BC); memReg=BC+1; UPDTCYCLE(7,2,2,1)
        case 0x0B:  // DEC BC
            BC--; UPDTCYCLE(6,2,2,1)
        case 0x0C:  // INC C
            C++; incFlags(C); UPDTCYCLE(4,1,1,1)
        case 0x0D:  // DEC C
            C--; decFlags(C); UPDTCYCLE(4,1,1,1)
        case 0x0E:  // LD C,N
            C=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x0F:  // RRCA
            fTmp=A<<7; A=(A>>1)|fTmp; rotaFlags(fTmp); UPDTCYCLE(4,1,1,1)
        case 0x10:  // DJNZ DIS
            dTmp=GetOff(PC++); B=B-1;
            if (B==0) UPDTCYCLE(8,2,3,1)
                PC=memReg=PC+dTmp; UPDTCYCLE(13,4,4,1)
        case 0x11:  // LD DE,NN
            DE=MemGetW(PC); PC+=2; UPDTCYCLE(10,3,3,1)
        case 0x12:  // LD (DE),A
            MemPut(DE, A); memReg=DE+1; memRegH=A;
            UPDTCYCLE(7,2,2,1)
        case 0x13:  // INC DE
            DE++; UPDTCYCLE(6,2,2,1)
        case 0x14:  // INC D
            D++; incFlags(D); UPDTCYCLE(4,1,1,1)
        case 0x15:  // DEC D
            D--; decFlags(D); UPDTCYCLE(4,1,1,1)
        case 0x16:  // LD D,N
            D=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x17:  // RLA
            fTmp=A>>7; A=(A<<1)|F_C(); rotaFlags(fTmp); UPDTCYCLE(4,1,1,1)
        case 0x18:  // JR DIS
            dTmp=GetOff(PC++); PC=memReg=PC+dTmp; UPDTCYCLE(12,3,3,1)
        case 0x19:  // ADD HL,DE
            memReg = HL; HL=memReg+DE; add16Flags(memReg,DE,HL); ++memReg; UPDTCYCLE(11,3,3,1)
        case 0x1A:  // LD A,(DE)
            A=MemGet(DE); memReg=DE+1; UPDTCYCLE(7,2,2,1)
        case 0x1B:  // DEC DE
            DE--; UPDTCYCLE(6,2,2,1)
        case 0x1C:  // INC E
            E++; incFlags(E); UPDTCYCLE(4,1,1,1)
        case 0x1D:  // DEC E
            E--; decFlags(E); UPDTCYCLE(4,1,1,1)
        case 0x1E:  // LD E,N
            E=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x1F:  // RRA
            fTmp=A<<7; A=(A>>1)|(F_C()<<7); rotaFlags(fTmp); UPDTCYCLE(4,1,1,1)
        case 0x20:  // JR NZ,DIS
            if (!F_Z()) {dTmp=GetOff(PC++); PC=memReg=PC+dTmp; UPDTCYCLE(12,3,3,1)}
            PC++; UPDTCYCLE(7,2,2,1)
        case 0x21:  // LD HL,NN
            HL=MemGetW(PC); PC+=2; UPDTCYCLE(10,3,3,1)
        case 0x22:  // LD (NN),HL
            memReg = MemGetW(PC); MemPutW(memReg++, HL); PC+=2; UPDTCYCLE(16,4,5,1)
        case 0x23:  // INC HL
            HL++; UPDTCYCLE(6,2,2,1)
        case 0x24:  // INC H
            H++; incFlags(H); UPDTCYCLE(4,1,1,1)
        case 0x25:  // DEC H
            H--; decFlags(H); UPDTCYCLE(4,1,1,1)
        case 0x26:  // LD H,N
            H=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x27:  // DAA
        { //  anotherlin / z80emu
          // The following algorithm is from comp.sys.sinclair's FAQ.
            int a = A, c, d;
            if (a > 0x99 || F_C()) {
                c = C_M;
                d = 0x60;

            } else
                c = d = 0;

            if ((a & 0x0f) > 0x09 || F_H())
                d += 0x06;

            A += F_N() ? -d : +d;
            F = SZYXP_FLAGS_TABLE[A] | ((A ^ a) & H_M) | (F & N_M) | c;

        }
            UPDTCYCLE(4,1,1,1)
        case 0x28:  // JR Z,DIS
            if (F_Z()) {dTmp=GetOff(PC++); PC=memReg=PC+dTmp; UPDTCYCLE(12,3,3,1)}
            PC++; UPDTCYCLE(7,2,2,1)
        case 0x29:  // ADD HL,HL
            memReg=HL; HL=memReg+HL; add16Flags(memReg,memReg,HL); ++memReg; UPDTCYCLE(11,3,3,1)
        case 0x2A:  // LD HL,(NN)
            memReg=MemGetW(PC);
            HL=MemGetW(memReg++);
            PC+=2; UPDTCYCLE(16,4,5,1)
        case 0x2B:  // DEC HL
            HL--; UPDTCYCLE(6,2,2,1)
        case 0x2C:  // INC L
            L++; incFlags(L); UPDTCYCLE(4,1,1,1)
        case 0x2D:  // DEC L
            L--; decFlags(L); UPDTCYCLE(4,1,1,1)
        case 0x2E:  // LD L,N
            L=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x2F:  // CPL
            bTmp=A;A=~A; cplFlags(A); UPDTCYCLE(4,1,1,1)
        case 0x30:  // JR NC,DIS
            if (!F_C()) {dTmp=GetOff(PC++); PC=memReg=PC+dTmp; UPDTCYCLE(12,3,3,1)}
            PC++; UPDTCYCLE(7,2,2,1)
        case 0x31:  // LD SP,NN
            SP=MemGetW(PC); PC+=2; UPDTCYCLE(10,3,3,1)
        case 0x32:  // LD (NN),A
            memReg=MemGetW(PC); MemPut(memReg,A); PC+=2; ++memReg; memRegH=A;
            UPDTCYCLE(13,4,4,1)
        case 0x33:  // INC SP
            SP++; UPDTCYCLE(6,2,2,1)
        case 0x34:  // INC (HL)
            bTmp=MemGet(HL)+1; MemPut(HL, bTmp); incFlags(bTmp);
            UPDTCYCLE(11,3,3,1)
        case 0x35:  // DEC (HL)
            bTmp=MemGet(HL)-1; MemPut(HL, bTmp); decFlags(bTmp);
            UPDTCYCLE(11,3,3,1)
        case 0x36:  // LD (HL),N
            MemPut(HL, MemGet(PC++)); UPDTCYCLE(10,3,3,1)
        case 0x37:  // SCF
            scfFlags(); UPDTCYCLE(4,1,1,1)
        case 0x38:  // JR C,DIS
            if (F_C()) { dTmp=GetOff(PC++); PC=memReg=PC+dTmp; UPDTCYCLE(12,3,3,1) }
            PC++; UPDTCYCLE(7,2,2,1)
        case 0x39:  // ADD HL,SP
            memReg=HL; HL=HL+SP; add16Flags(memReg,SP,HL); ++memReg; UPDTCYCLE(11,3,3,1)
        case 0x3A:  // LD A,(NN)
            memReg=MemGetW(PC); A=MemGet(memReg); PC+=2; ++memReg;
            UPDTCYCLE(13,4,4,1)
        case 0x3B:  // DEC SP
            SP--; UPDTCYCLE(6,2,2,1)
        case 0x3C:  // INC A
            A++; incFlags(A); UPDTCYCLE(4,1,1,1)
        case 0x3D:  // DEC A
            A--; decFlags(A); UPDTCYCLE(4,1,1,1)
        case 0x3E:  // LD A,N
            A=MemGet(PC++); UPDTCYCLE(7,2,2,1)
        case 0x3F:  // CCF
            F = (F&~0x3B) | (A&X_M) | (F_C()?H_M:0) | (~F&C_M);
            UPDTCYCLE(4,1,1,1)
        case 0x40:  // LD B,B
            /*B=B;*/ UPDTCYCLE(4,1,1,1)
        case 0x41:  // LD B,C
            B=C; UPDTCYCLE(4,1,1,1)
        case 0x42:  // LD B,D
            B=D; UPDTCYCLE(4,1,1,1)
        case 0x43:  // LD B,E
            B=E; UPDTCYCLE(4,1,1,1)
        case 0x44:  // LD B,H
            B=H; UPDTCYCLE(4,1,1,1)
        case 0x45:  // LD B,L
            B=L; UPDTCYCLE(4,1,1,1)
        case 0x46:  // LD B,(HL)
            B=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x47:  // LD B,A
            B=A; UPDTCYCLE(4,1,1,1)
        case 0x48:  // LD C,B
            C=B; UPDTCYCLE(4,1,1,1)
        case 0x49:  // LD C,C
            /*C=C;*/ UPDTCYCLE(4,1,1,1)
        case 0x4A:  // LD C,D
            C=D; UPDTCYCLE(4,1,1,1)
        case 0x4B:  // LD C,E
            C=E; UPDTCYCLE(4,1,1,1)
        case 0x4C:  // LD C,H
            C=H; UPDTCYCLE(4,1,1,1)
        case 0x4D:  // LD C,L
            C=L; UPDTCYCLE(4,1,1,1)
        case 0x4E:  // LD C,(HL)
            C=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x4F:  // LD C,A
            C=A; UPDTCYCLE(4,1,1,1)
        case 0x50:  // LD D,B
            D=B; UPDTCYCLE(4,1,1,1)
        case 0x51:  // LD D,C
            D=C; UPDTCYCLE(4,1,1,1)
        case 0x52:  // LD D,D
            /*D=D;*/ UPDTCYCLE(4,1,1,1)
        case 0x53:  // LD D,E
            D=E; UPDTCYCLE(4,1,1,1)
        case 0x54:  // LD D,H
            D=H; UPDTCYCLE(4,1,1,1)
        case 0x55:  // LD D,L
            D=L; UPDTCYCLE(4,1,1,1)
        case 0x56:  // LD D,(HL)
            D=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x57:  // LD D,A
            D=A; UPDTCYCLE(4,1,1,1)
        case 0x58:  // LD E,B
            E=B; UPDTCYCLE(4,1,1,1)
        case 0x59:  // LD E,C
            E=C; UPDTCYCLE(4,1,1,1)
        case 0x5A:  // LD E,D
            E=D; UPDTCYCLE(4,1,1,1)
        case 0x5B:  // LD E,E
            /*E=E;*/ UPDTCYCLE(4,1,1,1)
        case 0x5C:  // LD E,H
            E=H; UPDTCYCLE(4,1,1,1)
        case 0x5D:  // LD E,L
            E=L; UPDTCYCLE(4,1,1,1)
        case 0x5E:  // LD E,(HL)
            E=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x5F:  // LD E,A
            E=A; UPDTCYCLE(4,1,1,1)
        case 0x60:  // LD H,B
            H=B; UPDTCYCLE(4,1,1,1)
        case 0x61:  // LD H,C
            H=C; UPDTCYCLE(4,1,1,1)
        case 0x62:  // LD H,D
            H=D; UPDTCYCLE(4,1,1,1)
        case 0x63:  // LD H,E
            H=E; UPDTCYCLE(4,1,1,1)
        case 0x64:  // LD H,H
            /*H=H;*/ UPDTCYCLE(4,1,1,1)
        case 0x65:  // LD H,L
            H=L; UPDTCYCLE(4,1,1,1)
        case 0x66:  // LD H,(HL)
            H=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x67:  // LD H,A
            H=A; UPDTCYCLE(4,1,1,1)
        case 0x68:  // LD L,B
            L=B; UPDTCYCLE(4,1,1,1)
        case 0x69:  // LD L,C
            L=C; UPDTCYCLE(4,1,1,1)
        case 0x6A:  // LD L,D
            L=D; UPDTCYCLE(4,1,1,1)
        case 0x6B:  // LD L,E
            L=E; UPDTCYCLE(4,1,1,1)
        case 0x6C:  // LD L,H
            L=H; UPDTCYCLE(4,1,1,1)
        case 0x6D:  // LD L,L
            /*L=L;*/ UPDTCYCLE(4,1,1,1)
        case 0x6E:  // LD L,(HL)
            L=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x6F:  // LD L,A
            L=A; UPDTCYCLE(4,1,1,1)
        case 0x70:  // LD (HL),B
            MemPut(HL,B); UPDTCYCLE(7,2,2,1)
        case 0x71:  // LD (HL),C
            MemPut(HL,C); UPDTCYCLE(7,2,2,1)
        case 0x72:  // LD (HL),D
            MemPut(HL,D); UPDTCYCLE(7,2,2,1)
        case 0x73:  // LD (HL),E
            MemPut(HL,E); UPDTCYCLE(7,2,2,1)
        case 0x74:  // LD (HL),H
            MemPut(HL,H); UPDTCYCLE(7,2,2,1)
        case 0x75:  // LD (HL),L
            MemPut(HL,L); UPDTCYCLE(7,2,2,1)
        case 0x76:  // HALT
            Halt(1); PC--; UPDTCYCLE(4,1,1,1)
        case 0x77:  // LD (HL),A
            MemPut(HL,A); UPDTCYCLE(7,2,2,1)
        case 0x78:  // LD A,B
            A=B; UPDTCYCLE(4,1,1,1)
        case 0x79:  // LD A,C
            A=C; UPDTCYCLE(4,1,1,1)
        case 0x7A:  // LD A,D
            A=D; UPDTCYCLE(4,1,1,1)
        case 0x7B:  // LD A,E
            A=E; UPDTCYCLE(4,1,1,1)
        case 0x7C:  // LD A,H
            A=H; UPDTCYCLE(4,1,1,1)
        case 0x7D:  // LD A,L
            A=L; UPDTCYCLE(4,1,1,1)
        case 0x7E:  // LD A,(HL)
            A=MemGet(HL); UPDTCYCLE(7,2,2,1)
        case 0x7F:  // LD A,A
            /*A=A;*/ UPDTCYCLE(4,1,1,1)
        case 0x80:  // ADD A,B
            bTmp=A; A=A+B; addFlags(bTmp,B,A);
            UPDTCYCLE(4,1,1,1)
        case 0x81:  // ADD A,C
            bTmp=A; A=A+C; addFlags(bTmp,C,A);
            UPDTCYCLE(4,1,1,1)
        case 0x82:  // ADD A,D
            bTmp=A; A=A+D; addFlags(bTmp,D,A);
            UPDTCYCLE(4,1,1,1)
        case 0x83:  // ADD A,E
            bTmp=A; A=A+E; addFlags(bTmp,E,A);
            UPDTCYCLE(4,1,1,1)
        case 0x84:  // ADD A,H
            bTmp=A; A=A+H; addFlags(bTmp,H,A);
            UPDTCYCLE(4,1,1,1)
        case 0x85:  // ADD A,L
            bTmp=A; A=A+L; addFlags(bTmp,L,A);
            UPDTCYCLE(4,1,1,1)
        case 0x86:  // ADD A,(HL)
            fTmp=MemGet(HL); bTmp=A; A=A+fTmp; addFlags(bTmp,fTmp,A);
            UPDTCYCLE(7,2,2,1)
        case 0x87:  // ADD A,A
            bTmp=A; A=A+A; addFlags(bTmp,bTmp,A);
            UPDTCYCLE(4,1,1,1)
        case 0x88:  // ADC A,B
            bTmp=A; A=A+B+F_C();
            addFlags(bTmp,B,A); UPDTCYCLE(4,1,1,1)
        case 0x89:  // ADC A,C
            bTmp=A; A=A+C+F_C();
            addFlags(bTmp,C,A); UPDTCYCLE(4,1,1,1)
        case 0x8A:  // ADC A,D
            bTmp=A; A=A+D+F_C();
            addFlags(bTmp,D,A); UPDTCYCLE(4,1,1,1)
        case 0x8B:  // ADC A,E
            bTmp=A; A=A+E+F_C();
            addFlags(bTmp,E,A); UPDTCYCLE(4,1,1,1)
        case 0x8C:  // ADC A,H
            bTmp=A; A=A+H+F_C();
            addFlags(bTmp,H,A); UPDTCYCLE(4,1,1,1)
        case 0x8D:  // ADC A,L
            bTmp=A; A=A+L+F_C();
            addFlags(bTmp,L,A); UPDTCYCLE(4,1,1,1)
        case 0x8E:  // ADC A,(HL)
            fTmp=MemGet(HL); bTmp=A; A=A+fTmp+F_C();
            addFlags(bTmp,fTmp,A); UPDTCYCLE(7,2,2,1)
        case 0x8F:  // ADC A,A
            bTmp=A; A=A+A+F_C();
            addFlags(bTmp,bTmp,A); UPDTCYCLE(4,1,1,1)
        case 0x90:  // SUB A,B
            bTmp=A; A=A-B; subFlags(bTmp,B,A);
            UPDTCYCLE(4,1,1,1)
        case 0x91:  // SUB A,C
            bTmp=A; A=A-C; subFlags(bTmp,C,A);
            UPDTCYCLE(4,1,1,1)
        case 0x92:  // SUB A,D
            bTmp=A; A=A-D; subFlags(bTmp,D,A);
            UPDTCYCLE(4,1,1,1)
        case 0x93:  // SUB A,E
            bTmp=A; A=A-E; subFlags(bTmp,E,A);
            UPDTCYCLE(4,1,1,1)
        case 0x94:  // SUB A,H
            bTmp=A; A=A-H; subFlags(bTmp,H,A);
            UPDTCYCLE(4,1,1,1)
        case 0x95:  // SUB A,L
            bTmp=A; A=A-L; subFlags(bTmp,L,A);
            UPDTCYCLE(4,1,1,1)
        case 0x96:  // SUB A,(HL)
            fTmp=MemGet(HL); bTmp=A; A=A-fTmp; subFlags(bTmp,fTmp,A);
            UPDTCYCLE(7,2,2,1)
        case 0x97:  // SUB A,A
            bTmp=A; A=A-A; subFlags(bTmp,bTmp,A);
            UPDTCYCLE(4,1,1,1)
        case 0x98:  // SBC A,B
            bTmp=A; A=A-B-F_C();
            subFlags(bTmp,B,A); UPDTCYCLE(4,1,1,1)
        case 0x99:  // SBC A,C
            bTmp=A; A=A-C-F_C();
            subFlags(bTmp,C,A); UPDTCYCLE(4,1,1,1)
        case 0x9A:  // SBC A,D
            bTmp=A; A=A-D-F_C();
            subFlags(bTmp,D,A); UPDTCYCLE(4,1,1,1)
        case 0x9B:  // SBC A,E
            bTmp=A; A=A-E-F_C();
            subFlags(bTmp,E,A); UPDTCYCLE(4,1,1,1)
        case 0x9C:  // SBC A,H
            bTmp=A; A=A-H-F_C();
            subFlags(bTmp,H,A); UPDTCYCLE(4,1,1,1)
        case 0x9D:  // SBC A,L
            bTmp=A; A=A-L-F_C();
            subFlags(bTmp,L,A); UPDTCYCLE(4,1,1,1)
        case 0x9E:  // SBC A,(HL)
            fTmp=MemGet(HL); bTmp=A; A=A-fTmp-F_C();
            subFlags(bTmp,fTmp,A); UPDTCYCLE(7,2,2,1)
        case 0x9F:  // SBC A,A
            bTmp=A; A=A-A-F_C();
            subFlags(bTmp,bTmp,A); UPDTCYCLE(4,1,1,1)
        case 0xA0:  // AND B
            A&=B; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA1:  // AND C
            A&=C; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA2:  // AND D
            A&=D; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA3:  // AND E
            A&=E; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA4:  // AND H
            A&=H; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA5:  // AND L
            A&=L; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA6:  // AND (HL)
            A&=MemGet(HL); andFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xA7:  // AND A
            A&=A; andFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA8:  // XOR B
            A^=B; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xA9:  // XOR C
            A^=C; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xAA:  // XOR D
            A^=D; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xAB:  // XOR E
            A^=E; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xAC:  // XOR H
            A^=H; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xAD:  // XOR L
            A^=L; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xAE:  // XOR (HL)
            A^=MemGet(HL); orFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xAF:  // XOR A
            A^=A; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB0:  // OR B
            A|=B; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB1:  // OR C
            A|=C; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB2:  // OR D
            A|=D; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB3:  // OR E
            A|=E; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB4:  // OR H
            A|=H; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB5:  // OR L
            A|=L; orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB6:  // OR (HL)
            A|=MemGet(HL); orFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xB7:  // OR A
            orFlags(A); UPDTCYCLE(4,1,1,1)
        case 0xB8:  // CP B
            cpFlags(A,B,A-B); UPDTCYCLE(4,1,1,1)
        case 0xB9:  // CP C
            cpFlags(A,C,A-C); UPDTCYCLE(4,1,1,1)
        case 0xBA:  // CP D
            cpFlags(A,D,A-D); UPDTCYCLE(4,1,1,1)
        case 0xBB:  // CP E
            cpFlags(A,E,A-E); UPDTCYCLE(4,1,1,1)
        case 0xBC:  // CP H
            cpFlags(A,H,A-H); UPDTCYCLE(4,1,1,1)
        case 0xBD:  // CP L
            cpFlags(A,L,A-L); UPDTCYCLE(4,1,1,1)
        case 0xBE:  // CP (HL)
            fTmp = MemGet(HL);
            cpFlags(A,fTmp,A-fTmp); UPDTCYCLE(7,2,2,1)
        case 0xBF:  // CP A
            cpFlags(A,A,A-A); UPDTCYCLE(4,1,1,1)
        case 0xC0:  // RET NZ
            if (F_Z()) { UPDTCYCLE(5,2,2,1) }
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xC1:  // POP BC
            BC=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,3,1)
        case 0xC2:  // JP NZ,NN
            memReg=MemGetW(PC);
            if (! F_Z()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xC3:  // JP NN
            PC=memReg=MemGetW(PC); UPDTCYCLE(10,3,3,1)
        case 0xC4:  // CALL NZ,NN
            memReg=MemGetW(PC);
            if (F_Z()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xC5:  // PUSH BC
            SP-=2; MemPutW(SP, BC); UPDTCYCLE(11,3,4,1)
        case 0xC6:  // ADD A,N
            fTmp=MemGet(PC++); bTmp=A; A=A+fTmp; addFlags(bTmp,fTmp,A);
            UPDTCYCLE(7,2,2,1)
        case 0xC7:  // RST 0
            SP-=2; MemPutW(SP, PC); PC=memReg=0x00; UPDTCYCLE(11,3,4,1)
        case 0xC8:  // RET Z
            if (!F_Z()) { UPDTCYCLE(5,2,2,1) }
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xC9:  // RET
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,3,1)
        case 0xCA:  // JP Z,NN
            memReg=MemGetW(PC);
            if (F_Z()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xCB:  // Extended CB
            return CBop(phase);
        case 0xCC:  // CALL Z,NN
            memReg=MemGetW(PC);
            if (!F_Z()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2;MemPutW(SP, PC+2);PC=MemGetW(PC);UPDTCYCLE(17,5,5,1)
        case 0xCD:  // CALL NN
            memReg=MemGetW(PC);
            SP-=2; MemPutW(SP, PC+2); PC=memReg;
            UPDTCYCLE(17,5,5,1)
        case 0xCE:  // ADC A,N
            fTmp=MemGet(PC++); bTmp=A; A=A+fTmp+F_C();
            addFlags(bTmp,fTmp,A); UPDTCYCLE(7,2,2,1)
        case 0xCF:  // RST 8
            SP-=2; MemPutW(SP, PC); PC=memReg=0x08; UPDTCYCLE(11,3,4,1)
        case 0xD0:  // RET NC
            if (F_C()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xD1:  // POP DE
            DE=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,3,1)
        case 0xD2:  // JP NC,NN
            memReg=MemGetW(PC);
            if (! F_C()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xD3:  // OUT (N),A
            memReg=(A<<8)|MemGet(PC++); delayedIoVal=A;delayedIoPort=memReg++;delayedIoPhase=3-DELAY_OUT_N; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(10,3,DELAY_OUT_N,1)
        case 0xD4:  // CALL NC,NN
            memReg=MemGetW(PC);
            if (F_C()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xD5:  // PUSH DE
            SP-=2; MemPutW(SP, DE); UPDTCYCLE(11,3,4,1)
        case 0xD6:  // SUB A,N
            fTmp=MemGet(PC++); bTmp=A; A=A-fTmp; subFlags(bTmp,fTmp,A);
            UPDTCYCLE(7,2,2,1)
        case 0xD7:  // RST 10
            SP-=2; MemPutW(SP, PC); PC=memReg=0x10; UPDTCYCLE(11,3,4,1)
        case 0xD8:  // RET C
            if (!F_C()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xD9:  // EXX
            wTmp=BC;BC=BC2;BC2=wTmp; wTmp=DE;DE=DE2;DE2=wTmp;
            wTmp=HL;HL=HL2;HL2=wTmp; UPDTCYCLE(4,1,1,1)
        case 0xDA:  // JP C,NN
            memReg=MemGetW(PC);
            if (F_C()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xDB:  // IN A,(N)
            memRegH = A;
            memReg=(A<<8)|MemGet(PC++); delayedIoValDest=&A;delayedIoPort=memReg++;delayedIoPhase=3-DELAY_IN_N; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(10,3,DELAY_IN_N,1)
        case 0xDC:  // CALL C,NN
            memReg=MemGetW(PC);
            if (!F_C()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xDD:  // Extended DD
            return DDop(phase);
        case 0xDE:  // SBC A,N
            fTmp=MemGet(PC++); bTmp=A; A=A-fTmp-F_C();
            subFlags(bTmp,fTmp,A); UPDTCYCLE(7,2,2,1)
        case 0xDF:  // RST 18
            SP-=2; MemPutW(SP, PC); PC=memReg=0x18; UPDTCYCLE(11,3,4,1)
        case 0xE0:  // RET PO
            if (F_PV()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xE1:  // POP HL
            HL=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,3,1)
        case 0xE2:  // JP PO,NN
            memReg=MemGetW(PC);
            if (! F_PV()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xE3:  // EX (SP),HL
            memReg=MemGetW(SP); MemPutW(SP, HL); HL=memReg;
            UPDTCYCLE(19,5,6,1)
        case 0xE4:  // CALL PO,NN
            memReg=MemGetW(PC);
            if (F_PV()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xE5:  // PUSH HL
            SP-=2; MemPutW(SP, HL); UPDTCYCLE(11,3,4,1)
        case 0xE6:  // AND N
            A&=MemGet(PC++); andFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xE7:  // RST 20
            SP-=2; MemPutW(SP, PC); PC=memReg=0x20; UPDTCYCLE(11,3,4,1)
        case 0xE8:  // RET PE
            if (!F_PV()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xE9:  // JP (HL)
            PC=HL; UPDTCYCLE(4,1,1,1)
        case 0xEA:  // JP PE,NN
            memReg=MemGetW(PC);
            if (F_PV()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xEB:  // EX DE,HL
            wTmp=DE; DE=HL; HL=wTmp; UPDTCYCLE(4,1,1,1)
        case 0xEC:  // CALL PE,NN
            memReg=MemGetW(PC);
            if (!F_PV()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xED:  // Extended ED
            return EDop(phase);
        case 0xEE:  // XOR N
            A^=MemGet(PC++); orFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xEF:  // RST 28
            SP-=2; MemPutW(SP, PC); PC=memReg=0x28; UPDTCYCLE(11,3,4,1)
        case 0xF0:  // RET P
            if (F_S()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xF1:  // POP AF
            AF=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,3,1)
        case 0xF2:  // JP P,NN
            memReg=MemGetW(PC);
            if (! F_S()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xF3:  // DI
            Iff1(0);Iff2(0); UPDTCYCLE(4,1,1,1)
        case 0xF4:  // CALL P,NN
            memReg=MemGetW(PC);
            if (F_S()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xF5:  // PUSH AF
            SP-=2; MemPutW(SP, AF); UPDTCYCLE(11,3,4,1)
        case 0xF6:  // OR N
            A|=MemGet(PC++); orFlags(A); UPDTCYCLE(7,2,2,1)
        case 0xF7:  // RST 30
            SP-=2; MemPutW(SP, PC); PC=memReg=0x30; UPDTCYCLE(11,3,4,1)
        case 0xF8:  // RET M
            if (!F_S()) UPDTCYCLE(5,2,2,1)
                PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(10,3,4,1)
        case 0xF9:  // LD SP,HL
            SP=HL; UPDTCYCLE(6,2,2,1)
        case 0xFA:  // JP M,NN
            memReg=MemGetW(PC);
            if (F_S()) PC=memReg; else PC+=2; UPDTCYCLE(10,3,3,1)
        case 0xFB:  // EI
            Iff1(1);Iff2(1); CHK|=CHK_EI; UPDTCYCLE(4,1,1,1)
        case 0xFC:  // CALL M,NN
            memReg=MemGetW(PC);
            if (!F_S()) { PC+=2; UPDTCYCLE(10,3,3,1) }
            SP-=2; MemPutW(SP, PC+2); PC=memReg; UPDTCYCLE(17,5,5,1)
        case 0xFD:  // Extended FD
            return FDop(phase);
        case 0xFE:  // CP N
            fTmp = MemGet(PC++); cpFlags(A,fTmp,A-fTmp);
            UPDTCYCLE(7,2,2,1)
        case 0xFF:  // RST 38
            SP-=2; MemPutW(SP, PC); PC=memReg=0x38; UPDTCYCLE(11,3,4,1)
        default:
            ERR_THROW(format("Missing op-code %d")% opCode);
    }
}

Phase Z80::CBop(Phase phase)
{
    BYTE fTmp,bTmp;

    BYTE opCode=MemGet(PC++);

    switch (opCode) {
        case 0x00: // RLC B
            fTmp=B>>7; B=(B<<1)|fTmp; uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x01: // RLC C
            fTmp=C>>7; C=(C<<1)|fTmp; uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x02: // RLC D
            fTmp=D>>7; D=(D<<1)|fTmp; uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x03: // RLC E
            fTmp=E>>7; E=(E<<1)|fTmp; uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x04: // RLC H
            fTmp=H>>7; H=(H<<1)|fTmp; uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x05: // RLC L
            fTmp=L>>7; L=(L<<1)|fTmp; uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x06: // RLC (HL)
            bTmp=MemGet(HL); fTmp=bTmp>>7; bTmp=(bTmp<<1)|fTmp;
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x07: // RLC A
            fTmp=A>>7; A=(A<<1)|fTmp; uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x08: // RRC B
            fTmp=B<<7; B=(B>>1)|fTmp; uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x09: // RRC C
            fTmp=C<<7; C=(C>>1)|fTmp; uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x0A: // RRC D
            fTmp=D<<7; D=(D>>1)|fTmp; uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x0B: // RRC E
            fTmp=E<<7; E=(E>>1)|fTmp; uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x0C: // RRC H
            fTmp=H<<7; H=(H>>1)|fTmp; uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x0D: // RRC L
            fTmp=L<<7; L=(L>>1)|fTmp; uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x0E: // RRC (HL)
            bTmp=MemGet(HL); fTmp=bTmp<<7; bTmp=(bTmp>>1)|fTmp;
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x0F: // RRC A
            fTmp=A<<7; A=(A>>1)|fTmp; uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x10: // RL B
            fTmp=B>>7; B=(B<<1)|F_C(); uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x11: // RL C
            fTmp=C>>7; C=(C<<1)|F_C(); uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x12: // RL D
            fTmp=D>>7; D=(D<<1)|F_C(); uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x13: // RL E
            fTmp=E>>7; E=(E<<1)|F_C(); uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x14: // RL H
            fTmp=H>>7; H=(H<<1)|F_C(); uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x15: // RL L
            fTmp=L>>7; L=(L<<1)|F_C(); uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x16: // RL (HL)
            bTmp=MemGet(HL); fTmp=bTmp>>7; bTmp=(bTmp<<1)|F_C();
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x17: // RL A
            fTmp=A>>7; A=(A<<1)|F_C(); uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x18: // RR B
            fTmp=B<<7; B=(B>>1)|(F_C()<<7); uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x19: // RR C
            fTmp=C<<7; C=(C>>1)|(F_C()<<7); uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x1A: // RR D
            fTmp=D<<7; D=(D>>1)|(F_C()<<7); uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x1B: // RR E
            fTmp=E<<7; E=(E>>1)|(F_C()<<7); uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x1C: // RR H
            fTmp=H<<7; H=(H>>1)|(F_C()<<7); uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x1D: // RR L
            fTmp=L<<7; L=(L>>1)|(F_C()<<7); uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x1E: // RR (HL)
            bTmp=MemGet(HL); fTmp=bTmp<<7; bTmp=(bTmp>>1)|(F_C()<<7);
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x1F: // RR A
            fTmp=A<<7; A=(A>>1)|(F_C()<<7); uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x20: // SLA B
            fTmp=B>>7; B=(B<<1); uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x21: // SLA C
            fTmp=C>>7; C=(C<<1); uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x22: // SLA D
            fTmp=D>>7; D=(D<<1); uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x23: // SLA E
            fTmp=E>>7; E=(E<<1); uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x24: // SLA H
            fTmp=H>>7; H=(H<<1); uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x25: // SLA L
            fTmp=L>>7; L=(L<<1); uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x26: // SLA (HL)
            bTmp=MemGet(HL); fTmp=bTmp>>7; bTmp=(bTmp<<1);
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x27: // SLA A
            fTmp=A>>7; A=(A<<1); uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x28: // SRA B
            fTmp=B&0x01; B=(B>>1)|(B&0x80); uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x29: // SRA C
            fTmp=C&0x01; C=(C>>1)|(C&0x80); uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x2A: // SRA D
            fTmp=D&0x01; D=(D>>1)|(D&0x80); uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x2B: // SRA E
            fTmp=E&0x01; E=(E>>1)|(E&0x80); uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x2C: // SRA H
            fTmp=H&0x01; H=(H>>1)|(H&0x80); uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x2D: // SRA L
            fTmp=L&0x01; L=(L>>1)|(L&0x80); uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x2E: // SRA (HL)
            bTmp=MemGet(HL); fTmp=bTmp&0x01; bTmp=(bTmp>>1)|(bTmp&0x80);
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x2F: // SRA A
            fTmp=A&0x01; A=(A>>1)|(A&0x80); uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x30: // SLL B
            fTmp=B>>7; B=(B<<1)|0x01; uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x31: // SLL C
            fTmp=C>>7; C=(C<<1)|0x01; uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x32: // SLL D
            fTmp=D>>7; D=(D<<1)|0x01; uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x33: // SLL E
            fTmp=E>>7; E=(E<<1)|0x01; uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x34: // SLL H
            fTmp=H>>7; H=(H<<1)|0x01; uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x35: // SLL L
            fTmp=L>>7; L=(L<<1)|0x01; uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x36: // SLL (HL)
            bTmp=MemGet(HL); fTmp=bTmp>>7; bTmp=(bTmp<<1)|0x01;
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x37: // SLL A
            fTmp=A>>7; A=(A<<1)|0x01; uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x38: // SRL B
            fTmp=B&0x01; B=B>>1; uupurr(B,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x39: // SRL C
            fTmp=C&0x01; C=C>>1; uupurr(C,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x3A: // SRL D
            fTmp=D&0x01; D=D>>1; uupurr(D,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x3B: // SRL E
            fTmp=E&0x01; E=E>>1; uupurr(E,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x3C: // SRL H
            fTmp=H&0x01; H=H>>1; uupurr(H,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x3D: // SRL L
            fTmp=L&0x01; L=L>>1; uupurr(L,fTmp); UPDTCYCLE(8,2,2,2)
        case 0x3E: // SRL (HL)
            bTmp=MemGet(HL); fTmp=bTmp&0x01; bTmp=bTmp>>1;
            MemPut(HL,bTmp); uupurr(bTmp,fTmp); UPDTCYCLE(15,4,4,2)
        case 0x3F: // SRL A
            fTmp=A&0x01; A=A>>1; uupurr(A,fTmp);
            UPDTCYCLE(8,2,2,2)
        case 0x40: // BIT 0,B
            bitFlags(B,0x01); UPDTCYCLE(8,2,2,2)
        case 0x41: // BIT 0,C
            bitFlags(C,0x01); UPDTCYCLE(8,2,2,2)
        case 0x42: // BIT 0,D
            bitFlags(D,0x01); UPDTCYCLE(8,2,2,2)
        case 0x43: // BIT 0,E
            bitFlags(E,0x01); UPDTCYCLE(8,2,2,2)
        case 0x44: // BIT 0,H
            bitFlags(H,0x01); UPDTCYCLE(8,2,2,2)
        case 0x45: // BIT 0,L
            bitFlags(L,0x01); UPDTCYCLE(8,2,2,2)
        case 0x46: // BIT 0,(HL)
            bitFlags2(MemGet(HL)&0x01); UPDTCYCLE(12,3,3,2)
        case 0x47: // BIT 0,A
            bitFlags(A,0x01); UPDTCYCLE(8,2,2,2)
        case 0x48: // BIT 1,B
            bitFlags(B,0x02); UPDTCYCLE(8,2,2,2)
        case 0x49: // BIT 1,C
            bitFlags(C,0x02); UPDTCYCLE(8,2,2,2)
        case 0x4A: // BIT 1,D
            bitFlags(D,0x02); UPDTCYCLE(8,2,2,2)
        case 0x4B: // BIT 1,E
            bitFlags(E,0x02); UPDTCYCLE(8,2,2,2)
        case 0x4C: // BIT 1,H
            bitFlags(H,0x02); UPDTCYCLE(8,2,2,2)
        case 0x4D: // BIT 1,L
            bitFlags(L,0x02); UPDTCYCLE(8,2,2,2)
        case 0x4E: // BIT 1,(HL)
            bitFlags2(MemGet(HL)&0x02); UPDTCYCLE(12,3,3,2)
        case 0x4F: // BIT 1,A
            bitFlags(A,0x02); UPDTCYCLE(8,2,2,2)
        case 0x50: // BIT 2,B
            bitFlags(B,0x04); UPDTCYCLE(8,2,2,2)
        case 0x51: // BIT 2,C
            bitFlags(C,0x04); UPDTCYCLE(8,2,2,2)
        case 0x52: // BIT 2,D
            bitFlags(D,0x04); UPDTCYCLE(8,2,2,2)
        case 0x53: // BIT 2,E
            bitFlags(E,0x04); UPDTCYCLE(8,2,2,2)
        case 0x54: // BIT 2,H
            bitFlags(H,0x04); UPDTCYCLE(8,2,2,2)
        case 0x55: // BIT 2,L
            bitFlags(L,0x04); UPDTCYCLE(8,2,2,2)
        case 0x56: // BIT 2,(HL)
            bitFlags2(MemGet(HL)&0x04); UPDTCYCLE(12,3,3,2)
        case 0x57: // BIT 2,A
            bitFlags(A,0x04); UPDTCYCLE(8,2,2,2)
        case 0x58: // BIT 3,B
            bitFlags(B,0x08); UPDTCYCLE(8,2,2,2)
        case 0x59: // BIT 3,C
            bitFlags(C,0x08); UPDTCYCLE(8,2,2,2)
        case 0x5A: // BIT 3,D
            bitFlags(D,0x08); UPDTCYCLE(8,2,2,2)
        case 0x5B: // BIT 3,E
            bitFlags(E,0x08); UPDTCYCLE(8,2,2,2)
        case 0x5C: // BIT 3,H
            bitFlags(H,0x08); UPDTCYCLE(8,2,2,2)
        case 0x5D: // BIT 3,L
            bitFlags(L,0x08); UPDTCYCLE(8,2,2,2)
        case 0x5E: // BIT 3,(HL)
            bitFlags2(MemGet(HL)&0x08); UPDTCYCLE(12,3,3,2)
        case 0x5F: // BIT 3,A
            bitFlags(A,0x08); UPDTCYCLE(8,2,2,2)
        case 0x60: // BIT 4,B
            bitFlags(B,0x10); UPDTCYCLE(8,2,2,2)
        case 0x61: // BIT 4,C
            bitFlags(C,0x10); UPDTCYCLE(8,2,2,2)
        case 0x62: // BIT 4,D
            bitFlags(D,0x10); UPDTCYCLE(8,2,2,2)
        case 0x63: // BIT 4,E
            bitFlags(E,0x10); UPDTCYCLE(8,2,2,2)
        case 0x64: // BIT 4,H
            bitFlags(H,0x10); UPDTCYCLE(8,2,2,2)
        case 0x65: // BIT 4,L
            bitFlags(L,0x10); UPDTCYCLE(8,2,2,2)
        case 0x66: // BIT 4,(HL)
            bitFlags2(MemGet(HL)&0x10); UPDTCYCLE(12,3,3,2)
        case 0x67: // BIT 4,A
            bitFlags(A,0x10); UPDTCYCLE(8,2,2,2)
        case 0x68: // BIT 5,B
            bitFlags(B,0x20); UPDTCYCLE(8,2,2,2)
        case 0x69: // BIT 5,C
            bitFlags(C,0x20); UPDTCYCLE(8,2,2,2)
        case 0x6A: // BIT 5,D
            bitFlags(D,0x20); UPDTCYCLE(8,2,2,2)
        case 0x6B: // BIT 5,E
            bitFlags(E,0x20); UPDTCYCLE(8,2,2,2)
        case 0x6C: // BIT 5,H
            bitFlags(H,0x20); UPDTCYCLE(8,2,2,2)
        case 0x6D: // BIT 5,L
            bitFlags(L,0x20); UPDTCYCLE(8,2,2,2)
        case 0x6E: // BIT 5,(HL)
            bitFlags2(MemGet(HL)&0x20); UPDTCYCLE(12,3,3,2)
        case 0x6F: // BIT 5,A
            bitFlags(A,0x20); UPDTCYCLE(8,2,2,2)
        case 0x70: // BIT 6,B
            bitFlags(B,0x40); UPDTCYCLE(8,2,2,2)
        case 0x71: // BIT 6,C
            bitFlags(C,0x40); UPDTCYCLE(8,2,2,2)
        case 0x72: // BIT 6,D
            bitFlags(D,0x40); UPDTCYCLE(8,2,2,2)
        case 0x73: // BIT 6,E
            bitFlags(E,0x40); UPDTCYCLE(8,2,2,2)
        case 0x74: // BIT 6,H
            bitFlags(H,0x40); UPDTCYCLE(8,2,2,2)
        case 0x75: // BIT 6,L
            bitFlags(L,0x40); UPDTCYCLE(8,2,2,2)
        case 0x76: // BIT 6,(HL)
            bitFlags2(MemGet(HL)&0x40); UPDTCYCLE(12,3,3,2)
        case 0x77: // BIT 6,A
            bitFlags(A,0x40); UPDTCYCLE(8,2,2,2)
        case 0x78: // BIT 7,B
            bitFlags(B,0x80); UPDTCYCLE(8,2,2,2)
        case 0x79: // BIT 7,C
            bitFlags(C,0x80); UPDTCYCLE(8,2,2,2)
        case 0x7A: // BIT 7,D
            bitFlags(D,0x80); UPDTCYCLE(8,2,2,2)
        case 0x7B: // BIT 7,E
            bitFlags(E,0x80); UPDTCYCLE(8,2,2,2)
        case 0x7C: // BIT 7,H
            bitFlags(H,0x80); UPDTCYCLE(8,2,2,2)
        case 0x7D: // BIT 7,L
            bitFlags(L,0x80); UPDTCYCLE(8,2,2,2)
        case 0x7E: // BIT 7,(HL)
            bitFlags2(MemGet(HL)&0x80); UPDTCYCLE(12,3,3,2)
        case 0x7F: // BIT 7,A
            bitFlags(A,0x80); UPDTCYCLE(8,2,2,2)
        case 0x80: // RES 0,B
            B&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x81: // RES 0,C
            C&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x82: // RES 0,D
            D&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x83: // RES 0,E
            E&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x84: // RES 0,H
            H&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x85: // RES 0,L
            L&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x86: // RES 0,(HL)
            MemPut(HL, MemGet(HL)&~0x01); UPDTCYCLE(15,4,4,2)
        case 0x87: // RES 0,A
            A&=~0x01; UPDTCYCLE(8,2,2,2)
        case 0x88: // RES 1,B
            B&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x89: // RES 1,C
            C&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x8A: // RES 1,D
            D&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x8B: // RES 1,E
            E&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x8C: // RES 1,H
            H&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x8D: // RES 1,L
            L&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x8E: // RES 1,(HL)
            MemPut(HL, MemGet(HL)&~0x02); UPDTCYCLE(15,4,4,2)
        case 0x8F: // RES 1,A
            A&=~0x02; UPDTCYCLE(8,2,2,2)
        case 0x90: // RES 2,B
            B&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x91: // RES 2,C
            C&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x92: // RES 2,D
            D&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x93: // RES 2,E
            E&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x94: // RES 2,H
            H&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x95: // RES 2,L
            L&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x96: // RES 2,(HL)
            MemPut(HL, MemGet(HL)&~0x04); UPDTCYCLE(15,4,4,2)
        case 0x97: // RES 2,A
            A&=~0x04; UPDTCYCLE(8,2,2,2)
        case 0x98: // RES 3,B
            B&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x99: // RES 3,C
            C&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x9A: // RES 3,D
            D&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x9B: // RES 3,E
            E&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x9C: // RES 3,H
            H&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x9D: // RES 3,L
            L&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0x9E: // RES 3,(HL)
            MemPut(HL, MemGet(HL)&~0x08); UPDTCYCLE(15,4,4,2)
        case 0x9F: // RES 3,A
            A&=~0x08; UPDTCYCLE(8,2,2,2)
        case 0xA0: // RES 4,B
            B&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA1: // RES 4,C
            C&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA2: // RES 4,D
            D&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA3: // RES 4,E
            E&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA4: // RES 4,H
            H&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA5: // RES 4,L
            L&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA6: // RES 4,(HL)
            MemPut(HL, MemGet(HL)&~0x10); UPDTCYCLE(15,4,4,2)
        case 0xA7: // RES 4,A
            A&=~0x10; UPDTCYCLE(8,2,2,2)
        case 0xA8: // RES 5,B
            B&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xA9: // RES 5,C
            C&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xAA: // RES 5,D
            D&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xAB: // RES 5,E
            E&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xAC: // RES 5,H
            H&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xAD: // RES 5,L
            L&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xAE: // RES 5,(HL)
            MemPut(HL, MemGet(HL)&~0x20); UPDTCYCLE(15,4,4,2)
        case 0xAF: // RES 5,A
            A&=~0x20; UPDTCYCLE(8,2,2,2)
        case 0xB0: // RES 6,B
            B&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB1: // RES 6,C
            C&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB2: // RES 6,D
            D&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB3: // RES 6,E
            E&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB4: // RES 6,H
            H&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB5: // RES 6,L
            L&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB6: // RES 6,(HL)
            MemPut(HL, MemGet(HL)&~0x40); UPDTCYCLE(15,4,4,2)
        case 0xB7: // RES 6,A
            A&=~0x40; UPDTCYCLE(8,2,2,2)
        case 0xB8: // RES 7,B
            B&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xB9: // RES 7,C
            C&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xBA: // RES 7,D
            D&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xBB: // RES 7,E
            E&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xBC: // RES 7,H
            H&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xBD: // RES 7,L
            L&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xBE: // RES 7,(HL)
            MemPut(HL, MemGet(HL)&~0x80); UPDTCYCLE(15,4,4,2)
        case 0xBF: // RES 7,A
            A&=~0x80; UPDTCYCLE(8,2,2,2)
        case 0xC0: // SET 0,B
            B|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC1: // SET 0,C
            C|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC2: // SET 0,D
            D|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC3: // SET 0,E
            E|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC4: // SET 0,H
            H|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC5: // SET 0,L
            L|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC6: // SET 0,(HL)
            MemPut(HL, MemGet(HL)|0x01); UPDTCYCLE(15,4,4,2)
        case 0xC7: // SET 0,A
            A|=0x01; UPDTCYCLE(8,2,2,2)
        case 0xC8: // SET 1,B
            B|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xC9: // SET 1,C
            C|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xCA: // SET 1,D
            D|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xCB: // SET 1,E
            E|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xCC: // SET 1,H
            H|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xCD: // SET 1,L
            L|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xCE: // SET 1,(HL)
            MemPut(HL, MemGet(HL)|0x02); UPDTCYCLE(15,4,4,2)
        case 0xCF: // SET 1,A
            A|=0x02; UPDTCYCLE(8,2,2,2)
        case 0xD0: // SET 2,B
            B|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD1: // SET 2,C
            C|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD2: // SET 2,D
            D|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD3: // SET 2,E
            E|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD4: // SET 2,H
            H|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD5: // SET 2,L
            L|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD6: // SET 2,(HL)
            MemPut(HL, MemGet(HL)|0x04); UPDTCYCLE(15,4,4,2)
        case 0xD7: // SET 2,A
            A|=0x04; UPDTCYCLE(8,2,2,2)
        case 0xD8: // SET 3,B
            B|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xD9: // SET 3,C
            C|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xDA: // SET 3,D
            D|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xDB: // SET 3,E
            E|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xDC: // SET 3,H
            H|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xDD: // SET 3,L
            L|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xDE: // SET 3,(HL)
            MemPut(HL, MemGet(HL)|0x08); UPDTCYCLE(15,4,4,2)
        case 0xDF: // SET 3,A
            A|=0x08; UPDTCYCLE(8,2,2,2)
        case 0xE0: // SET 4,B
            B|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE1: // SET 4,C
            C|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE2: // SET 4,D
            D|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE3: // SET 4,E
            E|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE4: // SET 4,H
            H|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE5: // SET 4,L
            L|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE6: // SET 4,(HL)
            MemPut(HL, MemGet(HL)|0x10); UPDTCYCLE(15,4,4,2)
        case 0xE7: // SET 4,A
            A|=0x10; UPDTCYCLE(8,2,2,2)
        case 0xE8: // SET 5,B
            B|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xE9: // SET 5,C
            C|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xEA: // SET 5,D
            D|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xEB: // SET 5,E
            E|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xEC: // SET 5,H
            H|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xED: // SET 5,L
            L|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xEE: // SET 5,(HL)
            MemPut(HL, MemGet(HL)|0x20); UPDTCYCLE(15,4,4,2)
        case 0xEF: // SET 5,A
            A|=0x20; UPDTCYCLE(8,2,2,2)
        case 0xF0: // SET 6,B
            B|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF1: // SET 6,C
            C|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF2: // SET 6,D
            D|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF3: // SET 6,E
            E|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF4: // SET 6,H
            H|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF5: // SET 6,L
            L|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF6: // SET 6,(HL)
            MemPut(HL, MemGet(HL)|0x40); UPDTCYCLE(15,4,4,2)
        case 0xF7: // SET 6,A
            A|=0x40; UPDTCYCLE(8,2,2,2)
        case 0xF8: // SET 7,B
            B|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xF9: // SET 7,C
            C|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xFA: // SET 7,D
            D|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xFB: // SET 7,E
            E|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xFC: // SET 7,H
            H|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xFD: // SET 7,L
            L|=0x80; UPDTCYCLE(8,2,2,2)
        case 0xFE: // SET 7,(HL)
            MemPut(HL, MemGet(HL)|0x80); UPDTCYCLE(15,4,4,2)
        case 0xFF: // SET 7,A
            A|=0x80; UPDTCYCLE(8,2,2,2)
        default:
            LOG_STATUS("Unknown CPU instr [PC=%04X->%02X]",PC,MemGet(PC));
            UPDTCYCLE(8,2,2,2)
    }
}

Phase Z80::EDop(Phase phase)
{
    BYTE bTmp;

    switch (MemGet(PC++)) {
        case 0x40:  // IN B,(C)
            delayedIoValDest=&B;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x41:  // OUT (C),B
            delayedIoVal=B;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x42:  //  SBC HL,BC
            memReg=HL; HL-=BC+F_C();
            sbc16Flags(memReg,BC,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x43:  //  LD (NN),BC
            memReg = MemGetW(PC); MemPutW(memReg++, BC); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x44:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x45:  //  RETN
            PC=MemGetW(SP); Iff1(Iff2()); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x46:  //  IM 0
            Im(0); UPDTCYCLE(8,2,2,2)
        case 0x47:  //  LD I,A
            I=A; UPDTCYCLE(9,3,3,2)
        case 0x48:  // IN C,(C)
            delayedIoValDest=&C;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x49:  // OUT (C),C
            delayedIoVal=C;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x4A:  //  ADC HL,BC
            memReg=HL; HL=HL+BC+F_C();
            adc16Flags(memReg,BC,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x4B:  //  LD BC,(NN)
            memReg=MemGetW(PC);
            BC=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x4C:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x4D:  //  RETI
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x4E:  //  IM 1
            Im(1); UPDTCYCLE(8,2,2,2)
        case 0x4F:  //  LD R,A
            R=A; SplitR(); UPDTCYCLE(9,3,3,0)
        case 0x50:  // IN D,(C)
            delayedIoValDest=&D;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x51:  // OUT (C),D
            delayedIoVal=D;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x52:  //  SBC HL,DE
            memReg=HL; HL-=DE+F_C();
            sbc16Flags(memReg,DE,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x53:  //  LD (NN),DE
            memReg = MemGetW(PC); MemPutW(memReg++, DE); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x54:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x55:  //  RETN
            PC=MemGetW(SP); Iff1(Iff2()); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x56:  //  IM 2
            Im(2); UPDTCYCLE(8,2,2,2)
        case 0x57:  //  LD A,I
            A=I; ldairFlags(A); UPDTCYCLE(9,3,3,2)
        case 0x58:  // IN E,(C)
            delayedIoValDest=&E;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x59:  // OUT (C),E
            delayedIoVal=E;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x5A:  //  ADC HL,DE
            memReg=HL; HL=HL+DE+F_C();
            adc16Flags(memReg,DE,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x5B:  //  LD DE,(NN)
            memReg=MemGetW(PC);
            DE=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x5C:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x5D:  //  RETI
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x5E:  //  IM 3
            Im(3); UPDTCYCLE(8,2,2,2)
        case 0x5F:  //  LD A,R
            regR+=2;
            MergeR(); A=R; ldairFlags(A); UPDTCYCLE(9,3,3,0)
        case 0x60:  // IN H,(C)
            delayedIoValDest=&H;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x61:  // OUT (C),H
            delayedIoVal=H;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x62:  //  SBC HL,HL
            memReg=HL; HL-=HL+F_C();
            sbc16Flags(memReg,memReg,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x63:  //  LD (NN),HL
            memReg = MemGetW(PC); MemPutW(memReg++, HL); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x64:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x65:  //  RETN
            PC=MemGetW(SP); Iff1(Iff2()); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x66:  //  IM 0
            Im(0); UPDTCYCLE(8,2,2,2)
        case 0x67:  //  RRD
            memReg = HL;
            bTmp=MemGet(memReg); MemPut(memReg,(bTmp>>4)|((A&0x0F)<<4)); ++memReg;
            A=(A&0xF0)|(bTmp&0x0F); inFlags(A); UPDTCYCLE(18,5,5,2)
        case 0x68:  // IN L,(C)
            delayedIoValDest=&L;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x69:  // OUT (C),L
            delayedIoVal=L;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x6A:  //  ADC HL,HL
            memReg=HL; HL=HL+HL+F_C();
            adc16Flags(memReg,memReg,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x6B:  //  LD HL,(NN)
            memReg=MemGetW(PC);
            HL=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x6C:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x6D:  //  RETI
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x6E:  //  IM 1
            Im(1); UPDTCYCLE(8,2,2,2)
        case 0x6F:  //  RLD
            memReg = HL;
            bTmp=MemGet(memReg); MemPut(memReg,((bTmp<<4)&0xF0)|(A&0x0F)); ++memReg;
            A=(A&0xF0)|(bTmp>>4); inFlags(A); UPDTCYCLE(18,5,5,2)
        case 0x70: // IN null,(C)
            delayedIoValDest=&nullReg;delayedIoPort=BC;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x71: // OUT (C),null
            delayedIoVal=0;delayedIoPort=BC;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x72:  // SBC HL,SP
            memReg=HL; HL-=SP+F_C();
            sbc16Flags(memReg,SP,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x73:  // LD (NN),SP
            memReg = MemGetW(PC); MemPutW(memReg++, SP); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x74:  // NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x75:  // RETN
            PC=MemGetW(SP); Iff1(Iff2()); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x76:  // IM 2
            Im(2); UPDTCYCLE(8,2,2,2)
        case 0x78:  // IN A,(C)
            memReg=BC;delayedIoValDest=&A;delayedIoPort=memReg++;delayedIoPhase=4-DELAY_IN_C; CHK|=CHK_OI_IN; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_IN_C,2)
        case 0x79:  // OUT (C),A
            memReg=BC;delayedIoVal=A;delayedIoPort=memReg++;delayedIoPhase=4-DELAY_OUT_C; CHK|=CHK_OI_OUT; // DELAYED_IO
            UPDTCYCLE(11,3,DELAY_OUT_C,2)
        case 0x7A:  //  ADC HL,SP
            memReg=HL; HL=HL+SP+F_C();
            adc16Flags(memReg,SP,HL); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x7B:  //  LD SP,(NN)
            memReg=MemGetW(PC);
            SP=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x7C:  //  NEG
            bTmp=A; A=-A; subFlags(0,bTmp,A);
            UPDTCYCLE(8,2,2,2)
        case 0x7D:  //  RETI
            PC=memReg=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0x7E:  //  IM 3
            Im(3); UPDTCYCLE(8,2,2,2)
        case 0xA0:  // LDI
            bTmp=MemGet(HL++); MemPut(DE++, bTmp); BC--; ldirFlags(BC, bTmp+A);
            UPDTCYCLE(16,4,5,2)
        case 0xA1:  // CPI
        {   //  anotherlin / z80emu
            int a = A;
            int n = MemGet(HL++);
            int z = a - n;
            int f = (a ^ n ^ z) & H_M;

            n = z - (f >> 4);
            f |= (n << (5 - 1)) & 0x20;
            f |= n & 0x08;
            f |= SZYX_FLAGS_TABLE[z & 0xff] & (S_M|Z_M);
            f |= --BC ? PV_M : 0;
            F = f | N_M | (F & C_M);
            ++memReg;
        }
            UPDTCYCLE(16,4,4,2)
        case 0xA2:  // INI
            delayedIoValDest=MemPutP(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_INI; CHK|=CHK_OI_IN; // DELAYED_IO
            memReg = BC+1;
            B--; HL++; iuxxsx(B); UPDTCYCLE(15,4,DELAY_INI,2)
        case 0xA3:  // OUTI
            B--;
            memReg = BC+1;
            delayedIoVal=MemGet(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_OUTI; CHK|=CHK_OI_OUT; // DELAYED_IO
            HL++; iuxxsx(B); UPDTCYCLE(15,4,DELAY_OUTI,2)
        case 0xA8:  // LDD
            bTmp=MemGet(HL--);MemPut(DE--, bTmp); BC--; ldirFlags(BC, bTmp+A);
            UPDTCYCLE(16,4,5,2)
        case 0xA9:  // CPD
        {    //  anotherlin / z80emu
            int a = A;
            int n = MemGet(HL--);
            int z = a - n;
            int f = (a ^ n ^ z) & H_M;

            n = z - (f >> 4);
            f |= (n << (5 - 1)) & 0x20;
            f |= n & 0x08;
            f |= SZYX_FLAGS_TABLE[z & 0xff] & (S_M|Z_M);
            f |= --BC ? PV_M : 0;
            F = f | N_M | (F & C_M);
            --memReg;
        }
            UPDTCYCLE(16,4,4,2)
        case 0xAA:  // IND
            delayedIoValDest=MemPutP(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_INI; CHK|=CHK_OI_IN; // DELAYED_IO
            memReg = BC-1;
            B--; HL--; iuxxsx(B); UPDTCYCLE(15,4,DELAY_INI,2)
        case 0xAB:  // OUTD
            B--;
            memReg = BC-1;
            delayedIoVal=MemGet(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_OUTI; CHK|=CHK_OI_OUT; // DELAYED_IO
            HL--; iuxxsx(B); UPDTCYCLE(15,4,DELAY_OUTI,2)
        case 0xB0:  // LDIR
            bTmp=MemGet(HL++); MemPut(DE++, bTmp); BC--; ldirFlags(BC, bTmp+A);
            if (BC) {PC-=2; memReg=PC+1; UPDTCYCLE(21,6,6,2)}
            UPDTCYCLE(16,4,5,2)
        case 0xB1:  // CPIR
        {   //  anotherlin / z80emu
            int a = A;
            int n = MemGet(HL++);
            int z = a - n;
            int f = (a ^ n ^ z) & H_M;

            n = z - (f >> 4);
            f |= (n << (5 - 1)) & 0x20;
            f |= n & 0x08;
            f |= SZYX_FLAGS_TABLE[z & 0xff] & (S_M|Z_M);
            f |= --BC ? PV_M : 0;
            F = f | N_M | (F & C_M);
            ++memReg;
        }
            if (F_PV() && !F_Z()) {PC-=2;memReg=PC+1; UPDTCYCLE(21,6,6,2)}
            UPDTCYCLE(16,4,4,2)
        case 0xB2:  // INIR
            delayedIoValDest=MemPutP(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_INI; CHK|=CHK_OI_IN; // DELAYED_IO
            memReg = BC+1;
            B--; HL++; iuxxsx(B);
            if (B) {PC-=2;delayedIoPhase++;UPDTCYCLE(20,5,DELAY_INI,2);}
            UPDTCYCLE(15,4,DELAY_INI,2)
        case 0xB3:  // OTIR
            B--;
            memReg = BC+1;
            delayedIoVal=MemGet(HL);delayedIoPort=BC;CHK|=CHK_OI_OUT; // DELAYED_IO
            HL++; iuxxsx(B);
            if (B) {PC-=2;delayedIoPhase=6-DELAY_OUTIR; UPDTCYCLE(20,5,DELAY_OUTIR,2)}
            delayedIoPhase=5-DELAY_OUTIR; UPDTCYCLE(15,4,DELAY_OUTIR,2)
        case 0xB8:  // LDDR
            bTmp=MemGet(HL--);MemPut(DE--, bTmp); BC--; ldirFlags(BC, bTmp+A);
            if (BC) {PC-=2; memReg=PC+1; UPDTCYCLE(21,6,6,2)}
            UPDTCYCLE(16,4,5,2)
        case 0xB9:  // CPDR
        {   //  anotherlin / z80emu
            int a = A;
            int n = MemGet(HL--);
            int z = a - n;
            int f = (a ^ n ^ z) & H_M;

            n = z - (f >> 4);
            f |= (n << (5 - 1)) & 0x20;
            f |= n & 0x08;
            f |= SZYX_FLAGS_TABLE[z & 0xff] & (S_M|Z_M);
            f |= --BC ? PV_M : 0;
            F = f | N_M | (F & C_M);
            --memReg;
        }
            if (F_PV() && !F_Z()) {PC-=2; memReg=PC+1; UPDTCYCLE(21,6,6,2)}
            UPDTCYCLE(16,4,4,2)
        case 0xBA:  // INDR
            memReg = BC-1;
            delayedIoValDest=MemPutP(HL);delayedIoPort=BC;delayedIoPhase=5-DELAY_INI; CHK|=CHK_OI_IN; // DELAYED_IO
            B--; HL--; iuxxsx(B);
            if (B) {PC-=2; delayedIoPhase++;UPDTCYCLE(20,5,DELAY_INI,2)}
            UPDTCYCLE(15,4,DELAY_INI,2)
        case 0xBB:  // OTDR
            B--;
            memReg = BC-1;
            delayedIoVal=MemGet(HL);delayedIoPort=BC;CHK|=CHK_OI_OUT; // DELAYED_IO
            HL--; iuxxsx(B);
            if (B) {PC-=2;delayedIoPhase=6-DELAY_OUTIR;UPDTCYCLE(20,5,DELAY_OUTIR,2)}
            delayedIoPhase=5-DELAY_OUTIR; UPDTCYCLE(15,4,DELAY_OUTIR,2)
        default: // NOP
                 // LOG_STATUS("ED NOP instr [PC=%04X->%02X]", PC-1, MemGet(PC-1));
            UPDTCYCLE(8,2,2,2)
    }
}

Phase Z80::DDop(Phase phase)
{
    BYTE bTmp,fTmp;

    switch (MemGet(PC++)) {
        case 0x09: // ADD IX,BC
            memReg=IX; IX=IX+BC; add16Flags(memReg,BC,IX); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x19: // ADD IX,DE
            memReg=IX; IX=IX+DE; add16Flags(memReg,DE,IX); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x21: // LD IX,NN
            IX=MemGetW(PC); PC+=2; UPDTCYCLE(14,4,4,2)
        case 0x22: // LD (NN),IX
            memReg = MemGetW(PC); MemPutW(memReg++, IX); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x23: // INC IX
            IX++; UPDTCYCLE(10,3,3,2)
        case 0x24: // INC HX
            HX++; incFlags(HX); UPDTCYCLE(8,2,2,2)
        case 0x25: // DEC HX
            HX--; decFlags(HX); UPDTCYCLE(8,2,2,2)
        case 0x26: // LD HX,N
            HX=MemGet(PC++); UPDTCYCLE(11,3,3,2)
        case 0x29: // ADD IX,IX
            memReg=IX; IX=IX+IX; add16Flags(memReg,memReg,IX); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x2A: // LD IX,(NN)
            memReg=MemGetW(PC);
            IX=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x2B: // DEC IX
            IX--; UPDTCYCLE(10,3,3,2)
        case 0x2C: // INC LX
            LX++; incFlags(LX); UPDTCYCLE(8,2,2,2)
        case 0x2D: // DEC LX
            LX--; decFlags(LX); UPDTCYCLE(8,2,2,2)
        case 0x2E: // LD LX,N
            LX=MemGet(PC++); UPDTCYCLE(11,3,3,2)
        case 0x34: // INC (IX)
            memReg = IX+GetOff(PC++); bTmp=MemGet(memReg)+1;
            MemPut(memReg,bTmp); incFlags(bTmp); UPDTCYCLE(23,6,6,2)
        case 0x35: // DEC (IX)
            memReg = IX+GetOff(PC++); bTmp=MemGet(memReg)-1;
            MemPut(memReg,bTmp); decFlags(bTmp); UPDTCYCLE(23,6,6,2)
        case 0x36: // LD (IX),N
            memReg = IX+GetOff(PC++);
            MemPut(memReg, MemGet(PC++)); UPDTCYCLE(19,5,6,2)
        case 0x39: // ADD IX,SP
            memReg=IX; IX=IX+SP; add16Flags(memReg,SP,IX); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x44: // LD B,HX
            B=HX; UPDTCYCLE(8,2,2,2)
        case 0x45: // LD B,LX
            B=LX; UPDTCYCLE(8,2,2,2)
        case 0x46: // LD B,(IX)
            memReg = IX+GetOff(PC++);
            B=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x4C: // LD C,HX
            C=HX; UPDTCYCLE(8,2,2,2)
        case 0x4D: // LD C,LX
            C=LX; UPDTCYCLE(8,2,2,2)
        case 0x4E: // LD C,(IX)
            memReg = IX+GetOff(PC++);
            C=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x54: // LD D,HX
            D=HX; UPDTCYCLE(8,2,2,2)
        case 0x55: // LD D,LX
            D=LX; UPDTCYCLE(8,2,2,2)
        case 0x56: // LD D,(IX)
            memReg = IX+GetOff(PC++);
            D=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x5C: // LD E,HX
            E=HX; UPDTCYCLE(8,2,2,2)
        case 0x5D: // LD E,LX
            E=LX; UPDTCYCLE(8,2,2,2)
        case 0x5E: // LD E,(IX)
            memReg = IX+GetOff(PC++);
            E=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x60: // LD HX,B
            HX=B; UPDTCYCLE(8,2,2,2)
        case 0x61: // LD HX,C
            HX=C; UPDTCYCLE(8,2,2,2)
        case 0x62: // LD HX,D
            HX=D; UPDTCYCLE(8,2,2,2)
        case 0x63: // LD HX,E
            HX=E; UPDTCYCLE(8,2,2,2)
        case 0x64: // LD HX,HX
            /*HX=HX;*/ UPDTCYCLE(8,2,2,2)
        case 0x65: // LD HX,LX
            HX=LX; UPDTCYCLE(8,2,2,2)
        case 0x66: // LD H,(IX)
            memReg = IX+GetOff(PC++);
            H=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x67: // LD HX,A
            HX=A; UPDTCYCLE(8,2,2,2)
        case 0x68: // LD LX,B
            LX=B; UPDTCYCLE(8,2,2,2)
        case 0x69: // LD LX,C
            LX=C; UPDTCYCLE(8,2,2,2)
        case 0x6A: // LD LX,D
            LX=D; UPDTCYCLE(8,2,2,2)
        case 0x6B: // LD LX,E
            LX=E; UPDTCYCLE(8,2,2,2)
        case 0x6C: // LD LX,HX
            LX=HX; UPDTCYCLE(8,2,2,2)
        case 0x6D: // LD LX,LX
            /*LX=LX;*/ UPDTCYCLE(8,2,2,2)
        case 0x6E: // LD L,(IX)
            memReg = IX+GetOff(PC++);
            L=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x6F: // LD LX,A
            LX=A; UPDTCYCLE(8,2,2,2)
        case 0x70: // LD (IX),B
            memReg = IX+GetOff(PC++);
            MemPut(memReg, B); UPDTCYCLE(19,5,5,2)
        case 0x71: // LD (IX),C
            memReg = IX+GetOff(PC++);
            MemPut(memReg, C); UPDTCYCLE(19,5,5,2)
        case 0x72: // LD (IX),D
            memReg = IX+GetOff(PC++);
            MemPut(memReg, D); UPDTCYCLE(19,5,5,2)
        case 0x73: // LD (IX),E
            memReg = IX+GetOff(PC++);
            MemPut(memReg, E); UPDTCYCLE(19,5,5,2)
        case 0x74: // LD (IX),H
            memReg = IX+GetOff(PC++);
            MemPut(memReg, H); UPDTCYCLE(19,5,5,2)
        case 0x75: // LD (IX),L
            memReg = IX+GetOff(PC++);
            MemPut(memReg, L); UPDTCYCLE(19,5,5,2)
        case 0x77: // LD (IX),A
            memReg = IX+GetOff(PC++);
            MemPut(memReg, A); UPDTCYCLE(19,5,5,2)
        case 0x7C: // LD A,HX
            A=HX; UPDTCYCLE(8,2,2,2)
        case 0x7D: // LD A,LX
            A=LX; UPDTCYCLE(8,2,2,2)
        case 0x7E: // LD A,(IX)
            memReg = IX+GetOff(PC++);
            A=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x84:  // ADD A,HX
            bTmp=A; A=A+HX; addFlags(bTmp,HX,A);
            UPDTCYCLE(8,2,2,2)
        case 0x85:  // ADD A,LX
            bTmp=A; A=A+LX; addFlags(bTmp,LX,A);
            UPDTCYCLE(8,2,2,2)
        case 0x86:  // ADD A,(IX)
            memReg = IX+GetOff(PC++);
            fTmp=MemGet(memReg); bTmp=A;
            A=A+fTmp; addFlags(bTmp,fTmp,A);
            UPDTCYCLE(19,5,5,2)
        case 0x8C: // ADC A,HX
            bTmp=A; A=A+HX+F_C();
            addFlags(bTmp,HX,A); UPDTCYCLE(8,2,2,2)
        case 0x8D: // ADC A,LX
            bTmp=A; A=A+LX+F_C();
            addFlags(bTmp,LX,A); UPDTCYCLE(8,2,2,2)
        case 0x8E: // ADC A,(IX)
            memReg = IX+GetOff(PC++);
            fTmp=MemGet(memReg); bTmp=A; A=A+fTmp+F_C();
            addFlags(bTmp,fTmp,A); UPDTCYCLE(19,5,5,2)
        case 0x94: // SUB A,HX
            bTmp=A; A=A-HX; subFlags(bTmp,HX,A);
            UPDTCYCLE(8,2,2,2)
        case 0x95: // SUB A,LX
            bTmp=A; A=A-LX; subFlags(bTmp,LX,A);
            UPDTCYCLE(8,2,2,2)
        case 0x96: // SUB A,(IX)
            memReg = IX+GetOff(PC++);
            fTmp=MemGet(memReg);
            bTmp=A; A=A-fTmp; subFlags(bTmp,fTmp,A);
            UPDTCYCLE(19,5,5,2)
        case 0x9C: // SBC A,HX
            bTmp=A; A=A-HX-F_C();
            subFlags(bTmp,HX,A); UPDTCYCLE(8,2,2,2)
        case 0x9D: // SBC A,LX
            bTmp=A; A=A-LX-F_C();
            subFlags(bTmp,LX,A); UPDTCYCLE(8,2,2,2)
        case 0x9E: // SBC A,(IX)
            memReg = IX+GetOff(PC++);
            fTmp=MemGet(memReg);bTmp=A; A=A-fTmp-F_C();
            subFlags(bTmp,fTmp,A); UPDTCYCLE(19,5,5,2)
        case 0xA4: // AND HX
            A&=HX; andFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xA5: // AND LX
            A&=LX; andFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xA6: // AND (IX)
            memReg = IX+GetOff(PC++);
            A&=MemGet(memReg); andFlags(A); UPDTCYCLE(19,5,5,2)
        case 0xAC: // XOR HX
            A^=HX; orFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xAD: // XOR LX
            A^=LX; orFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xAE: // XOR (IX)
            memReg = IX+GetOff(PC++);
            A^=MemGet(memReg); orFlags(A); UPDTCYCLE(19,5,5,2)
        case 0xB4: // OR HX
            A|=HX; orFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xB5: // OR LX
            A|=LX; orFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xB6: // OR (IX)
            memReg = IX+GetOff(PC++);
            A|=MemGet(memReg); orFlags(A); UPDTCYCLE(19,5,5,2)
        case 0xBC: // CP HX
            cpFlags(A,HX,A-HX); UPDTCYCLE(8,2,2,2)
        case 0xBD: // CP LX
            cpFlags(A,LX,A-LX); UPDTCYCLE(8,2,2,2)
        case 0xBE: // CP (IX)
            memReg = IX+GetOff(PC++);
            fTmp = MemGet(memReg);
            cpFlags(A,fTmp,A-fTmp); UPDTCYCLE(19,5,5,2)
        case 0xCB: { // Extended DDCB
            static BYTE * const dstSelect[] = {&B, &C, &D, &E, &H, &L, &nullReg, &A};
            static const BYTE masks[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
            memReg = IX+GetOff(PC++);
            bTmp = MemGet(memReg);
            BYTE opCode = MemGet(PC++);

            switch (opCode&0xC0) {
                case 0x00: {
                    BYTE *dst = dstSelect[opCode&0x07];
                    switch(opCode&0x38) {
                        case 0x00: // RLC (IX)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|fTmp;
                            break;
                        case 0x08: // RRC (IX);
                            fTmp=bTmp<<7; *dst=bTmp=(bTmp>>1)|fTmp;
                            break;
                        case 0x10: // RL (IX)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|F_C();
                            break;
                        case 0x18: // RR (IX)
                            fTmp=bTmp<<7; *dst=bTmp=(bTmp>>1)|(F_C()<<7);
                            break;
                        case 0x20: // SLA (IX)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1);
                            break;
                        case 0x28: // SRA (IX)
                            fTmp=bTmp&0x01; *dst=bTmp=(bTmp>>1)|(bTmp&0x80);
                            break;
                        case 0x30: // SLL (IX)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|0x01;
                            break;
                        case 0x38: // SRL (IX)
                            fTmp=bTmp&0x01; *dst=bTmp=bTmp>>1;
                            break;
                    }
                    MemPut(memReg,bTmp); uupurr(bTmp,fTmp);
                    UPDTCYCLE(23,6,7,2)
                } break;
                case 0x40: { // BIT n,(IX)
                    BYTE mask = masks[(opCode&0x38)>>3];
                    bitFlags2(bTmp&mask);
                    UPDTCYCLE(8,2,6,2)
                } break;
                case 0x80: { // RES n,(IX)
                    BYTE *dst = dstSelect[opCode&0x07];
                    BYTE mask = masks[(opCode&0x38)>>3];
                    *dst=bTmp=bTmp&~mask;
                    MemPut(memReg,bTmp);
                    UPDTCYCLE(23,6,7,2)
                } break; 
                case 0xC0: { // SET n,(IX)
                    BYTE *dst = dstSelect[opCode&0x07];
                    BYTE mask = masks[(opCode&0x38)>>3];
                    *dst=bTmp=bTmp|mask;
                    MemPut(memReg,bTmp);
                    UPDTCYCLE(23,6,7,2)
                } break; 
            }
        } //break;
        case 0xE1: // POP IX
            IX=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0xE3: // EX (SP),IX
            memReg=MemGetW(SP); MemPutW(SP, IX); IX=memReg;
            UPDTCYCLE(23,6,7,2)
        case 0xE5: // PUSH IX
            SP-=2; MemPutW(SP, IX); UPDTCYCLE(15,4,5,2)
        case 0xE9: // JP (IX)
            PC=IX; UPDTCYCLE(8,2,4,2)
        case 0xF9: // LD SP,IX
            SP=IX; UPDTCYCLE(10,3,3,2)
        default:
            PC--;
            // LOG_STATUS("DD Fallback [PC=%04X->%02X]", C,MemGet(PC));
            UPDTCYCLE(4,1,1,1)
    }
}

Phase Z80::FDop(Phase phase)
{
    BYTE bTmp,fTmp;

    BYTE opCode = MemGet(PC++);
    switch (opCode) {
        case 0x09: // ADD IY,BC
            memReg=IY; IY=IY+BC; add16Flags(memReg,BC,IY); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x19: // ADD IY,DE
            memReg=IY; IY=IY+DE; add16Flags(memReg,DE,IY); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x21: // LD IY,NN
            IY=MemGetW(PC); PC+=2; UPDTCYCLE(14,4,4,2)
        case 0x22: // LD (NN),IY
            memReg = MemGetW(PC); MemPutW(memReg++, IY); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x23: // INC IY
            IY++; UPDTCYCLE(10,3,3,2)
        case 0x24: // INC HY
            HY++; incFlags(HY); UPDTCYCLE(8,2,2,2)
        case 0x25: // DEC HY
            HY--; decFlags(HY); UPDTCYCLE(8,2,2,2)
        case 0x26: // LD HY,N
            HY=MemGet(PC++); UPDTCYCLE(11,3,3,2)
        case 0x29: // ADD IY,IY
            memReg=IY; IY=IY+IY; add16Flags(memReg,memReg,IY); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x2A: // LD IY,(NN)
            memReg=MemGetW(PC);
            IY=MemGetW(memReg++); PC+=2; UPDTCYCLE(20,5,6,2)
        case 0x2B: // DEC IY
            IY--; UPDTCYCLE(10,3,3,2)
        case 0x2C: // INC LY
            LY++; incFlags(LY); UPDTCYCLE(8,2,2,2)
        case 0x2D: // DEC LY
            LY--; decFlags(LY); UPDTCYCLE(8,2,2,2)
        case 0x2E: // LD LY,N
            LY=MemGet(PC++); UPDTCYCLE(11,3,3,2)
        case 0x34: // INC (IY)
            memReg=IY+GetOff(PC++); bTmp=MemGet(memReg)+1;
            MemPut(memReg,bTmp); incFlags(bTmp); UPDTCYCLE(23,6,6,2)
        case 0x35: // DEC (IY)
            memReg=IY+GetOff(PC++); bTmp=MemGet(memReg)-1;
            MemPut(memReg,bTmp); decFlags(bTmp); UPDTCYCLE(23,6,6,2)
        case 0x36: // LD (IY),N
            memReg = IY+GetOff(PC++);
            MemPut(memReg, MemGet(PC++)); UPDTCYCLE(19,5,5,2)
        case 0x39: // ADD IY,SP
            memReg=IY; IY=IY+SP; add16Flags(memReg,SP,IY); ++memReg; UPDTCYCLE(15,4,4,2)
        case 0x44: // LD B,HY
            B=HY; UPDTCYCLE(8,2,2,2)
        case 0x45: // LD B,LY
            B=LY; UPDTCYCLE(8,2,2,2)
        case 0x46: // LD B,(IY)
            memReg = IY+GetOff(PC++);
            B=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x4C: // LD C,HY
            C=HY; UPDTCYCLE(8,2,2,2)
        case 0x4D: // LD C,LY
            C=LY; UPDTCYCLE(8,2,2,2)
        case 0x4E: // LD C,(IY)
            memReg = IY+GetOff(PC++);
            C=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x54: // LD D,HY
            D=HY; UPDTCYCLE(8,2,2,2)
        case 0x55: // LD D,LY
            D=LY; UPDTCYCLE(8,2,2,2)
        case 0x56: // LD D,(IY)
            memReg = IY+GetOff(PC++);
            D=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x5C: // LD E,HY
            E=HY; UPDTCYCLE(8,2,2,2)
        case 0x5D: // LD E,LY
            E=LY; UPDTCYCLE(8,2,2,2)
        case 0x5E: // LD E,(IY)
            memReg = IY+GetOff(PC++);
            E=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x60: // LD HY,B
            HY=B; UPDTCYCLE(8,2,2,2)
        case 0x61: // LD HY,C
            HY=C; UPDTCYCLE(8,2,2,2)
        case 0x62: // LD HY,D
            HY=D; UPDTCYCLE(8,2,2,2)
        case 0x63: // LD HY,E
            HY=E; UPDTCYCLE(8,2,2,2)
        case 0x64: // LD HY,HY
            /*HY=HY;*/ UPDTCYCLE(8,2,2,2)
        case 0x65: // LD HY,LY
            HY=LY; UPDTCYCLE(8,2,2,2)
        case 0x66: // LD H,(IY)
            memReg = IY+GetOff(PC++);
            H=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x67: // LD HY,A
            HY=A; UPDTCYCLE(8,2,2,2)
        case 0x68: // LD LY,B
            LY=B; UPDTCYCLE(8,2,2,2)
        case 0x69: // LD LY,C
            LY=C; UPDTCYCLE(8,2,2,2)
        case 0x6A: // LD LY,D
            LY=D; UPDTCYCLE(8,2,2,2)
        case 0x6B: // LD LY,E
            LY=E; UPDTCYCLE(8,2,2,2)
        case 0x6C: // LD LY,HY
            LY=HY; UPDTCYCLE(8,2,2,2)
        case 0x6D: // LD LY,LY
            /*LY=LY;*/ UPDTCYCLE(8,2,2,2)
        case 0x6E: // LD L,(IY)
            memReg = IY+GetOff(PC++);
            L=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x6F: // LD LY,A
            LY=A; UPDTCYCLE(8,2,2,2)
        case 0x70: // LD (IY),B
            memReg = IY+GetOff(PC++);
            MemPut(memReg, B); UPDTCYCLE(19,5,5,2)
        case 0x71: // LD (IY),C
            memReg = IY+GetOff(PC++);
            MemPut(memReg, C); UPDTCYCLE(19,5,5,2)
        case 0x72: // LD (IY),D
            memReg = IY+GetOff(PC++);
            MemPut(memReg, D); UPDTCYCLE(19,5,5,2)
        case 0x73: // LD (IY),E
            memReg = IY+GetOff(PC++);
            MemPut(memReg, E); UPDTCYCLE(19,5,5,2)
        case 0x74: // LD (IY),H
            memReg = IY+GetOff(PC++);
            MemPut(memReg, H); UPDTCYCLE(19,5,5,2)
        case 0x75: // LD (IY),L
            memReg = IY+GetOff(PC++);
            MemPut(memReg, L); UPDTCYCLE(19,5,5,2)
        case 0x77: // LD (IY),A
            memReg = IY+GetOff(PC++);
            MemPut(memReg, A); UPDTCYCLE(19,5,5,2)
        case 0x7C: // LD A,HY
            A=HY; UPDTCYCLE(8,2,2,2)
        case 0x7D: // LD A,LY
            A=LY; UPDTCYCLE(8,2,2,2)
        case 0x7E: // LD A,(IY)
            memReg = IY+GetOff(PC++);
            A=MemGet(memReg); UPDTCYCLE(19,5,5,2)
        case 0x84:  // ADD A,HY
            bTmp=A; A=A+HY; addFlags(bTmp,HY,A);
            UPDTCYCLE(8,2,2,2)
        case 0x85:  // ADD A,LY
            bTmp=A; A=A+LY; addFlags(bTmp,LY,A);
            UPDTCYCLE(8,2,2,2)
        case 0x86:  // ADD A,(IY)
            memReg = IY+GetOff(PC++);
            fTmp=MemGet(memReg); bTmp=A;
            A=A+fTmp; addFlags(bTmp,fTmp,A);
            UPDTCYCLE(19,5,5,2)
        case 0x8C: // ADC A,HY
            bTmp=A; A=A+HY+F_C();
            addFlags(bTmp,HY,A); UPDTCYCLE(8,2,2,2)
        case 0x8D: // ADC A,LY
            bTmp=A; A=A+LY+F_C();
            addFlags(bTmp,LY,A); UPDTCYCLE(8,2,2,2)
        case 0x8E: // ADC A,(IY)
            memReg = IY+GetOff(PC++);
            fTmp=MemGet(memReg); bTmp=A; A=A+fTmp+F_C();
            addFlags(bTmp,fTmp,A); UPDTCYCLE(19,5,5,2)
        case 0x94: // SUB A,HY
            bTmp=A; A=A-HY; subFlags(bTmp,HY,A);
            UPDTCYCLE(8,2,2,2)
        case 0x95: // SUB A,LY
            bTmp=A; A=A-LY; subFlags(bTmp,LY,A);
            UPDTCYCLE(8,2,2,2)
        case 0x96: // SUB A,(IY)
            memReg = IY+GetOff(PC++);
            fTmp=MemGet(memReg);
            bTmp=A; A=A-fTmp; subFlags(bTmp,fTmp,A);
            UPDTCYCLE(19,5,5,2)
        case 0x9C: // SBC A,HY
            bTmp=A; A=A-HY-F_C();
            subFlags(bTmp,HY,A); UPDTCYCLE(8,2,2,2)
        case 0x9D: // SBC A,LY
            bTmp=A; A=A-LY-F_C();
            subFlags(bTmp,LY,A); UPDTCYCLE(8,2,2,2)
        case 0x9E: // SBC A,(IY)
            memReg = IY+GetOff(PC++);
            fTmp=MemGet(memReg);bTmp=A; A=A-fTmp-F_C();
            subFlags(bTmp,fTmp,A); UPDTCYCLE(19,5,5,2)
        case 0xA4: // AND HY
            A&=HY; andFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xA5: // AND LY
            A&=LY; andFlags(A); UPDTCYCLE(8,2,2,2)
        case 0xA6: // AND (IY)
            memReg = IY+GetOff(PC++);
            A&=MemGet(memReg); andFlags(A);
            UPDTCYCLE(19,5,5,2)
        case 0xAC: // XOR HY
            A^=HY; orFlags(A);
            UPDTCYCLE(8,2,2,2)
        case 0xAD: // XOR LY
            A^=LY; orFlags(A);
            UPDTCYCLE(8,2,2,2)
        case 0xAE: // XOR (IY)
            memReg = IY+GetOff(PC++);
            A^=MemGet(memReg); orFlags(A);
            UPDTCYCLE(19,5,5,2)
        case 0xB4: // OR HY
            A|=HY; orFlags(A);
            UPDTCYCLE(8,2,2,2)
        case 0xB5: // OR LY
            A|=LY; orFlags(A);
            UPDTCYCLE(8,2,2,2)
        case 0xB6: // OR (IY)
            memReg = IY+GetOff(PC++);
            A|=MemGet(memReg); orFlags(A);
            UPDTCYCLE(19,5,5,2)
        case 0xBC: // CP HY
            cpFlags(A,HY,A-HY);
            UPDTCYCLE(8,2,2,2)
        case 0xBD: // CP LY
            cpFlags(A,LY,A-LY);
            UPDTCYCLE(8,2,2,2)
        case 0xBE: // CP (IY)
            memReg = IY+GetOff(PC++);
            fTmp = MemGet(memReg);
            cpFlags(A,fTmp,A-fTmp);
            UPDTCYCLE(19,5,5,2)
        case 0xCB: { // Extended FDCB
            static BYTE * const dstSelect[] = {&B, &C, &D, &E, &H, &L, &nullReg, &A};
            static const BYTE masks[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
            memReg = IY+GetOff(PC++);
            bTmp = MemGet(memReg);
            BYTE opCode = MemGet(PC++);

            switch (opCode&0xC0) {
                case 0x00: {
                    BYTE *dst = dstSelect[opCode&0x07];
                    switch(opCode&0x38) {
                        case 0x00: // RLC (IY)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|fTmp;
                            break;
                        case 0x08: // RRC (IY);
                            fTmp=bTmp<<7; *dst=bTmp=(bTmp>>1)|fTmp;
                            break;
                        case 0x10: // RL (IY)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|F_C();
                            break;
                        case 0x18: // RR (IY)
                            fTmp=bTmp<<7; *dst=bTmp=(bTmp>>1)|(F_C()<<7);
                            break;
                        case 0x20: // SLA (IY)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1);
                            break;
                        case 0x28: // SRA (IY)
                            fTmp=bTmp&0x01; *dst=bTmp=(bTmp>>1)|(bTmp&0x80);
                            break;
                        case 0x30: // SLL (IY)
                            fTmp=bTmp>>7; *dst=bTmp=(bTmp<<1)|0x01;
                            break;
                        case 0x38: // SRL (IY)
                            fTmp=bTmp&0x01; *dst=bTmp=bTmp>>1;
                            break;
                    }
                    MemPut(memReg,bTmp); uupurr(bTmp,fTmp);
                    UPDTCYCLE(23,6,7,2)
                } break;
                case 0x40: { // BIT n,(IY)
                    BYTE mask = masks[(opCode&0x38)>>3];
                    bitFlags2(bTmp&mask);
                    UPDTCYCLE(8,2,6,2)
                } break;
                case 0x80: { // RES n,(IY)
                    BYTE *dst = dstSelect[opCode&0x07];
                    BYTE mask = masks[(opCode&0x38)>>3];
                    *dst=bTmp=bTmp&~mask;
                    MemPut(memReg,bTmp);
                    UPDTCYCLE(23,6,7,2)
                } break; 
                case 0xC0: { // SET n,(IY)
                    BYTE *dst = dstSelect[opCode&0x07];
                    BYTE mask = masks[(opCode&0x38)>>3];
                    *dst=bTmp=bTmp|mask;
                    MemPut(memReg,bTmp);
                    UPDTCYCLE(23,6,7,2)
                } break; 
            }
        } //break;
        case 0xE1: // POP IY
            IY=MemGetW(SP); SP+=2; UPDTCYCLE(14,4,4,2)
        case 0xE3: // EX (SP),IY
            memReg=MemGetW(SP); MemPutW(SP, IY); IY=memReg;
            UPDTCYCLE(23,6,7,2)
        case 0xE5: // PUSH IY
            SP-=2; MemPutW(SP, IY); UPDTCYCLE(15,4,5,2)
        case 0xE9: // JP (IY)
            PC=IY; UPDTCYCLE(8,2,4,2)
        case 0xF9: // LD SP,IY
            SP=IY; UPDTCYCLE(10,3,3,2)
        default:
            PC--;
            // LOG_STATUS("FD Fallback [PC=%04X->%02X]",PC,MemGet(PC));
            UPDTCYCLE(4,1,1,1)
    }
}

void Z80::AddConditionStep()
{
    conditions.push_back(std::make_unique<CpuCondStep>());
}
void Z80::AddConditionBreak(UINT16 address, bool persist)
{
    conditions.push_back(std::make_unique<CpuCondBreakPt>(address, persist));
}
void Z80::AddConditionStack(UINT16 address)
{
    conditions.push_back(std::make_unique<CpuCondStack>(address));
}
void Z80::AddConditionIntAck()
{
    conditions.push_back(std::make_unique<CpuCondIntAck>());
}

