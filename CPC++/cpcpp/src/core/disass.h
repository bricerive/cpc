#pragma once
// CPC++ - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
//
/// \file disass.h - Z80 disassembler

#include "cpctypes.h"

class Mmu;

/// Z80 disassembler
class DisAss {
  public:

    static WORD Disassemble(const Mmu &mmu, WORD add, char *s=0, int n=0, bool withComment=false, WORD bcForIoComments=0);
    static int CodeRewind(const Mmu &mmu, WORD add, int cnt);
  private:
    // this one is recursive
    static WORD Disassemble(const Mmu &mmu, WORD add, char *s, int n, bool withComment, WORD bcForIoComments, bool indexed, int &offset);
    /// give name to known I/O ports
    static const char *PortName(WORD addr);
    static WORD CompleteStr(const Mmu &mmu, char *s, int n, WORD add, int offset, bool withComment, WORD bcForIoComments);
    static void ConvertIX(char *s, int n, int offset);
    static void ConvertIY(char *s, int n, int offset);
    static void ConvertIXIY(bool x, char *s, int n, int offset);
    // Standard memcpy does not handle overlapping areas properly
    // on all platforms (Namely CodeWarrior)
    static void mcpy(char *dst, const char *src, size_t n);
};
