=================================
CPC++ -- The Amstrad CPC Emulator
=================================

Overview
--------

CPC++ is an extremely accurate emulator for the Amstrad CPC family of personal computers. It lets you do pretty much anything you could do with an actual CPC.

CPC++ is a project developed by Brice Riv�.


Project Layout
--------------
```
--CPC++					Source and Build
    +--cpcpp			Main CMake for build
	|   +--src			Source code
	|      +--console	Console - cross-platform part
	|	   +--core		Emulation core
	|	   +--doxygen	Documentation generation
	|	   +--infra		Infrastructure library
	|	   +--linux		Platform specific - Linux
	|	   +--osx		Platform specific - MacOS using Carbon
	|	   +--sdl2		Platform specific - SDL2+wxWindow should build on all platforms
	|	   +--windows	Platform specific - Windows using win32
	+--build*			Build go here (e.g. build-VS for VisualCode)
--HomePage				Web page
--Project				All other project related info
    +--timeline			Timestamped steps (only recent for now)
```