----------------------------------------------------------------
Create a Machine as osx
Tried to boot from ISO made from my USB key - fail
Tried to boot from my USB key - fail
Grabbed a new ISO from Pirate Bay - Success
----------------------------------------------------------------
On SL install make sure to format the disk as OSX extended journaled
----------------------------------------------------------------
InstallXCode, Rosetta, X11 from the install CD
----------------------------------------------------------------
Do all the Sotware Updates
----------------------------------------------------------------
Notes about OSX guest in virtualBox 14:

Mac OS X guests:

    Mac OS X guests can only run on a certain host hardware. For details about license and host hardware limitations, please see Section 3.1.1, “Mac OS X guests” and check the Apple software license conditions.

    VirtualBox does not provide Guest Additions for Mac OS X at this time.

    The graphics resolution currently defaults to 1024x768 as Mac OS X falls back to the built-in EFI display support. See Section 3.12.1, “Video modes in EFI” for more information on how to change EFI video modes.

    Mac OS X guests only work with one CPU assigned to the VM. Support for SMP will be provided in a future release.

    Depending on your system and version of Mac OS X, you might experience guest hangs after some time. This can be fixed by turning off energy saving (set timeout to "Never") in the system preferences.

    By default, the VirtualBox EFI enables debug output of the Mac OS X kernel to help you diagnose boot problems. Note that there is a lot of output and not all errors are fatal (they would also show on your physical Mac). You can turn off these messages by issuing this command:

    VBoxManage setextradata "VM name" "VBoxInternal2/EfiBootArgs" "  "

    To revert to the previous behavior, use:

    VBoxManage setextradata "VM name" "VBoxInternal2/EfiBootArgs" ""

    It is currently not possible to start a Mac OS X guest in safe mode by specifying "-x" option in "VBoxInternal2/EfiBootArgs" extradata.

----------------------------------------------------------------
Turned off energy savings
----------------------------------------------------------------
Turned off boot messages:
VBoxManage setextradata "Snow Leopard" "VBoxInternal2/EfiBootArgs" "  "