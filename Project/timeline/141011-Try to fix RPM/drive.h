// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
// 
// Drive.h - Floppy drive emulation
#ifndef Drive_h
#define Drive_h

#include <vector>
class Floppy;
class cpcTrack;
class Sector;
class CpcFile;
#include "cpcphase.h"

struct SECT_ID {
    SECT_ID(): track(0), head(0), id(0), size(0) {}
    bool operator==(const SECT_ID &rhs)const {
        return track==rhs.track && head==rhs.head && id==rhs.id && size==rhs.size;
        return id==rhs.id;
    }
    //bool operator==(const SECT_ID &rhs)const {return id==rhs.id;}
    SECT_ID &operator=(const SECT_ID &rhs) {if (this==&rhs) return *this; track=rhs.track; head=rhs.head; id=rhs.id; size=rhs.size; return *this;}
    BYTE track;
    BYTE head;
    BYTE id;
    BYTE size;
};

class Drive {
    
public:
    Drive();
    ~Drive();
    
    // Floppy handling
    void LoadDisk(const CpcFile *file, bool protect);
    void SaveDisk(CpcFile &file);
    void EjectDisk();
    void FlipDisk();
    
    // Drive state
    BYTE State();
    int DoubleSidedFloppy()const;
    bool IsProtected()const;
    int IsFull()const {return floppy?1:0;}
    int IsReady(Phase phase)const;
    int IsTrack0()const {return (crtTrack==0)?1:0;}
    int CrtTrack()const {return crtTrack;}
    int IsTwoSides()const {return (nbHeads==2)?1:0;}
    int SeekDone(Phase phase)const;
    
    // Head movement
    void Step(Phase phase, int nb, int &trk0);
    
    // Sector access
    void FirstSect(int head, Sector *&sect, int &delay);
    bool FindSect(int head, const SECT_ID &id, int erased, int skip, Sector *&sect, int &delay);
    bool NextSectId(int head, int erased, int skip, Sector *&sect, int &eot, int &delay);
    void NextSect(int head, Sector *&sect, int &eot, int &delay);
    Sector *CrtSect();

    // Formatting
    void FormatTrack(int head, BYTE size, BYTE nb, BYTE gap, BYTE data);
    void FormatSect(int head, int sect, const SECT_ID &id);
    
    // Get directory for GUI
    void GetDir(int &nbEntries, std::vector<char> &dir);
    bool CanCpm()const;
    
    // Options
    void RandomDataError(bool val);
    
    inline cpcTrack *CrtTrack();
    
private:
    Drive &operator=(const Drive&);
    Drive(const Drive&);
    
    int nbHeads;
    int crtHead;
    int crtTrack;
    Floppy *floppy;
    bool randomDataError;
    mutable Phase stepDonePhase;
};

class Floppy{
    
public:
    Floppy(bool randomDataError);
    ~Floppy();
    
    void Load(const CpcFile &file, bool protect);
    void Save(CpcFile &file);
    void Flip();
    
    bool IsProtected()const {return writeProtect;}
    int IsDoubleSided()const {return nbSides==2;}
    int NbTracks()const {return tracks.size();}
    
    cpcTrack *GetTrack(int side, int track);
    void GetDir(int &nbEntries, std::vector<char> &dir);
    bool CanCpm()const;

    void FormatTrack(int side,int track,BYTE fmtSize,BYTE fmtNb,BYTE fmtGap,BYTE fmtData);
    
    // Options
    void RandomDataError(bool val);
    
private:
    void SetFormat(const BYTE *buf);
    void LoadDIF(const CpcFile &file);
    void LoadDIF2(const CpcFile &file);
    void LoadDSK(const CpcFile &file);
    void LoadEXTDSK(const CpcFile &file);
    
    bool writeProtect;
    enum {UNKNOWN,DIF0,DIF1,DIF2,DSK,EXTENDED} format;
    BYTE nbSides;
    int flipped;
    typedef std::vector<cpcTrack *> Tracks;
    Tracks tracks;
    bool randomDataError;
    int randomDataErrorFlag;
};

static int Size2Bytes(BYTE size) { return 128<<std::min<int>(6,size);}

class cpcTrack {
public:
    cpcTrack(int nbSect, BYTE sectSize, BYTE gap3, BYTE filler, bool randomDataError);
    ~cpcTrack();
    
    void Format(BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData);
    void FormatSect(int sect, const SECT_ID &id);
	void LoadSect(int no, const SECT_ID &id, const CpcFile &file);
	void LoadSect(int no, const SECT_ID &id, int fSize, const CpcFile &file, int nbCopies);
	void LoadSect(int no, const SECT_ID &id, int cptVal);
	void SetSectState(int no, BYTE st1, BYTE st2);
    Sector &Sect(int i) {return *sectors.at(i);}
    int NbSect()const {return sectors.size();}
    int Formatted()const {return !sectors.empty();}
    
    BYTE SectSize()const {return sectSize;}
    int SectNBytes()const {return Size2Bytes(sectSize);}
    void SectSize(BYTE val) {sectSize = val;}
    BYTE Gap3()const {return gap3;}
    BYTE Filler()const {return filler;}
    
    void RandomDataError(bool val);

    bool AdvanceCrtSect(); // true if eot
    Sector *CrtSect();
    bool FindSect(const SECT_ID &id, int /*erased*/, int /*skip*/, Sector *&sect, int &delay);
    void FirstSect(Sector *&sect, int &delay);
    void NextSect(Sector *&sect, int &eot, int &delay);
    bool NextSectId(int erased, int skip, Sector *&sect, int &eot, int &delay);

    void GenerateSectorReadData(Sector &crtReadDataSect, char *dst, int nBytesToGenerate);
    int IntersectDataSize();
    static const int totalTrackSize=6300;
private:
    BYTE sectSize;
    BYTE gap3;
    BYTE filler;
    typedef std::vector<Sector *> Sectors;
    Sectors sectors;
    bool randomDataError;
    int crtSect;
};

// FDC Status register 0 flags
static const BYTE St0ErrorCodeNone=0x00;
static const BYTE St0ErrorCodeIncomplete=0x40;
static const BYTE St0ErrorCodeInvalid=0x80;
static const BYTE St0ErrorCodeStopped=0xC0;
static const BYTE St0SeekEnd=0x20;
static const BYTE St0EquipmentCheck=0x10;
static const BYTE St0NotReady=0x08;
static const BYTE St0HeadAddress=0x04;
static const BYTE St0UnitSelect1=0x02;
static const BYTE St0UnitSelect0=0x01;
// FDC Status register 1 flags
static const BYTE St1None=0x00;
static const BYTE St1EndOfCylinder=0x80;
static const BYTE St1DataError=0x20;
static const BYTE St1Overrun=0x10;
static const BYTE St1NoData=0x04;
static const BYTE St1NotWriteable=0x02;
static const BYTE St1MissingAddressMark=0x01;
// FDC Status register 2 flags
static const BYTE St2None=0x00;
static const BYTE St2ControlMark=0x40;
static const BYTE St2DataErrorInDataField=0x20;
static const BYTE St2WrongCylinderTrackValue=0x10;
static const BYTE St2ScanEqualHit=0x08;
static const BYTE St2ScanNotSatisfied=0x04;
static const BYTE St2BadCylinder=0x02;
static const BYTE St2MissingAddressMarkInDataField=0x01;
// FDC Status register 3 flags
static const BYTE St3None=0x00;
static const BYTE St3Fault=0x80;
static const BYTE St3DiskWriteProtected=0x40;
static const BYTE St3DriveReady=0x20;
static const BYTE St3Track0=0x10;
static const BYTE St3TwoSide=0x08;

class Sector {
public:
    
    Sector(const SECT_ID &id, int filler, bool rde);
    Sector(const SECT_ID &id, const CpcFile &file, bool rde);
    Sector(const SECT_ID &id, int fSize, const CpcFile &file, bool rde, int nbCopies);
    
    bool IsSameId(const SECT_ID &id)const;
    BYTE Track()const {return id.track;}
    BYTE Head()const {return id.head;}
    BYTE Id()const {return id.id;}
    BYTE Size()const {return id.size;}
    
    int logicalSize()const {return Size2Bytes(id.size);}
    int storedSize()const {return data[0].size();}
    const BYTE *Data()const {return &data[0][0];}
    BYTE Data(int idx)const;
    void Data(int idx, BYTE val);
    
    void St1(BYTE val) {st1=val;}
    void St2(BYTE val) {st2=val;}
    BYTE St1()const {return st1;}
    BYTE St2()const {return st2;}
    int CtrlMark()const {return (st2&0x40) != 0;}
    
    void RandomDataError(bool val);
    
    void IntersectData(int gap3, std::vector<BYTE> &data);

private:
    SECT_ID id;
    BYTE st1;
    BYTE st2;
    int nbCopies;
    mutable int crtCopy;
    std::vector< std::vector<BYTE> > data;
    
    // Two modes of Random Data Error emulation
    typedef enum {noRde, rdeRandom, rdeFromDsk} Rde;    
    Rde randomDataError;
};

#endif
