// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
// 
// Drive.cc - Floppy drive emulation

// * Proper identification of Extended disk image header (check for 
// "EXTENDED" only)
// * Removed check for disk images and track header. Not needed. (Now 
// loads disk images created with Make_Dsk. Yes, make disk didnt insert
// the ascii values for /r/n it actually put the text for /r/n... ooops.
#include "config.h"
#include <stdio.h>
#include <string.h>
//#include <unistd.h>

#include "cpcerror.h"
#include "drive.h"
#include "cpcconsoleaccess.h"
#include "cpcfile.h"

Drive::Drive()
:nbHeads(1), crtHead(0), crtTrack(0), floppy(0), randomDataError(0)
{
}

Drive::~Drive()
{
    delete floppy;
}

int Drive::IsReady(Phase phase)const 
{
    return floppy?1:0;
}

int Drive::SeekDone(Phase phase)const
{
    if (stepDonePhase!=0 && phase<stepDonePhase)
        return 0;
    stepDonePhase=0;
    return 1;    
}

void Drive::RandomDataError(bool val)
{
    randomDataError=val;
    if (floppy) floppy->RandomDataError(val);
}

void Drive::LoadDisk(const CpcFile *file, bool protect)
{
    delete floppy;
    floppy=0;
    if (file) {
        try {
            floppy = new Floppy(randomDataError);
            floppy->Load(*file, protect);
        } catch (const Err &err) {
            gError(err.what());
            gErrReport(ERR_DRV_INSERT);
            delete floppy;
            floppy=0;
        }
    }
}

void Drive::SaveDisk(CpcFile &file)
{
    if (floppy==0)
        throw Err(_WHERE);
    floppy->Save(file);
}

void Drive::EjectDisk()
{
    delete floppy;
    floppy=0;
}

void Drive::FlipDisk()
{
    if (floppy==0)
        throw Err(_WHERE);
    floppy->Flip();
}

void Drive::GetDir(int &nbEntries, std::vector<char> &dir)
{
    if (floppy==0)
        throw Err(_WHERE);
    floppy->GetDir(nbEntries, dir);
}

bool Drive::CanCpm()const
{
    if (floppy==0)
        throw Err(_WHERE);
    return floppy->CanCpm();    
}

class cpcTrack *Drive::CrtTrack()
{
    if (floppy==0)
        throw Err(_WHERE);
    return floppy->GetTrack(crtHead,crtTrack);
}

Sector *Drive::CrtSect()
{
    cpcTrack *track=CrtTrack();
    if (!track) return 0;
    return track->CrtSect();
}

Sector *cpcTrack::CrtSect()
{
    if (NbSect()<=crtSect) return 0;
    return &Sect(crtSect);
}

bool cpcTrack::AdvanceCrtSect()
{
    if (++crtSect >= NbSect())
    {
        crtSect=0;
        return true;
    }
    return false;
}

bool Drive::IsProtected()const
{
    return floppy && floppy->IsProtected();
}

int Drive::DoubleSidedFloppy()const
{
    return floppy && floppy->IsDoubleSided();
}

void Drive::Step(Phase phase, int nb, int &trk0)
{
    int prevTrack = crtTrack;
    if (crtTrack+nb <= 0) {
        crtTrack=0;
        trk0=1;
    } else {
        crtTrack += nb;
        trk0=0;
    }
    stepDonePhase = phase + 10000*abs(crtTrack-prevTrack);
}

// Find a sector on the current track
// Returns an error if the track is not formatted
// Returns NULL sect if the sector is not found
// modifies delayBytes before throwing
bool Drive::FindSect(int head, const SECT_ID &id, int erased, int skip, Sector *&sect, int &delay)
{
    crtHead = head;
    cpcTrack *track = CrtTrack();
    delay=10;
    if (!track || !track->Formatted()) {
        gMsg(CpcConsole::IMPORTANT,"Unformatted track [%02X-%02X-%02X-%02X]", id.track, id.head, id.id, id.size);
        throw Err(_WHERE);
    }
    return track->FindSect(id, erased, skip, sect, delay);
}

// Find a sector on the current track
// Returns an error if the track is not formatted
// Returns NULL sect if the sector is not found
// modifies delayBytes before throwing
bool cpcTrack::FindSect(const SECT_ID &id, int erased, int skip, Sector *&sect, int &delay)
{
    int idxCount=0;
    delay=10;
    static bool yes=false;
    
    do {
#if 1
        int eot;
        int nextSectDelay;
        NextSect(sect, eot, nextSectDelay);
        if (eot)
            idxCount++;
        if (yes)
            delay += nextSectDelay;
#else   
        
        if (++crtSect >= NbSect())
        {
            idxCount++;
        }
        sect = CrtSect();
#endif   
        if (sect->IsSameId(id))
        {
            //          if (!(!erased && skip && (sect->st2&0x40)) &&
            //              !(erased && skip && !(sect->st2&0x40)))
            
            //Mathematiques 3éme
            // un missing DAM sur le secteur &63 de la piste 40
            if (sect->St2()&St2MissingAddressMarkInDataField)
                return false;
            return true;
        }
    } while (idxCount != 2);
    //crtSect = CrtTrack()->NbSect()-1;
    sect=0;
    return false;
}

void cpcTrack::FirstSect(Sector *&sect, int &delay)
{
    crtSect = 0;
    sect = CrtSect();
}

void cpcTrack::NextSect(Sector *&sect, int &eot, int &delay)
{
    eot=0;
    // La Chose de Grotemburg wants this here...
    // Le Necromancien wants this here...
    if (AdvanceCrtSect())
        eot = 1;
    sect = CrtSect();
    
    //---Track prelude
    //  80  4E  Gap 4A
    //  12  00  Sync
    //  03  1A  Index AM
    //  01  FC
    //  50  4E  Gap 1
    int trackPrelude = 80+ 12+ 4+ 50;
    //---Sector intro
    //  12  00  Sync
    //  03  1A  ID AM
    //  01  FE
    //  04  XX  ID Field
    //  02  XX  IDF CRC
    int sectorIntro = 12+ 4+ 4+ 2;
    // Here, I use storedSize(). It is wrong if there is some gap data, but it is
    // even worst if we use logicalSize() as this comes from sect ID which can be anything
    //\todo use Simon's sector offset info
    if (crtSect==0)
    {
        // first sector of the track (after index)
        //---Track prelude
        //  80  4E  Gap 4A
        //  12  00  Sync
        //  03  1A  Index AM
        //  01  FC
        //  50  4E  Gap 1
        //---Sector intro
        //  12  00  Sync
        //  03  1A  ID AM
        //  01  FE
        //  04  XX  ID Field
        //  02  XX  IDF CRC
        
        // the delay is critical for "La Chose..." which uses it with ReadID
        delay = 22+ 12+ 4+ Sect(NbSect()-1).storedSize() + 2+ Gap3() + trackPrelude+ sectorIntro;
        
        // More bytes for Gap4B
        int fullDataSize = trackPrelude;
        for (int i=0; i<NbSect(); i++)
            fullDataSize += sectorIntro+ 22+ 12+ 4+ Sect(i).storedSize()+ 2+ Gap3();
        delay += totalTrackSize - fullDataSize;
        //delay = 210 + Gap3() + Sect(NbSect()-1).storedSize();
    }
    else
    {
        //---End of Previous Sector
        //  22  4D  Gap 2
        //  12  00  Sync
        //  03  1A  Data AM
        //  01  FB
        //
        //  xx  xx ... data Field
        //
        // Intersect data
        //  02  XX  CRC
        //  nn  4E  Gap 3 (82 by default)
        //---Sector intro
        //  12  00  Sync
        //  03  1A  ID AM
        //  01  FE
        //  04  XX  ID Field
        //  02  XX  CRC
        delay = 22+ 12+ 4+ Sect(crtSect-1).storedSize() + 2+ Gap3()+ sectorIntro;
        //delay = 62 + Gap3() +  Sect(crtSect-1).storedSize();
    }
}

void Drive::FirstSect(int head, Sector *&sect, int &delay)
{
    delay=10;
    crtHead = head;
    cpcTrack *track = CrtTrack();
    
    if (!track || !track->Formatted()) {
        gMsg(CpcConsole::IMPORTANT,"FirstSect: no data on track %d", crtTrack);
        throw Err(_WHERE);
    }
    track->FirstSect(sect, delay);
}

void Drive::NextSect(int head, Sector *&sect, int &eot, int &delay)
{
    delay=0;
    crtHead = head;
    cpcTrack *track = CrtTrack();
    eot=0;
    if (!track || !track->Formatted()) {
        throw Err(_WHERE);
    }
    track->NextSect(sect, eot, delay);
}

bool Drive::NextSectId(int head, int erased, int skip, Sector *&sect, int &eot, int &delay)
{
    eot=0;
    delay=10;
    crtHead = head;
    cpcTrack *track = CrtTrack();
    
    if (!track || !track->Formatted())
        throw Err(_WHERE);
    return track->NextSectId(erased, skip, sect, eot, delay);
}

bool cpcTrack::NextSectId(int erased, int skip, Sector *&sect, int &eot, int &delay)
{
    int searchId,idxCount=0;    
    eot=0;
    delay=10;
    sect = CrtSect();
    searchId = sect->Id()+1;
    do {
        if (AdvanceCrtSect())
            idxCount++;
        sect = CrtSect();
        if (sect->Id() == searchId)
        {
            if ( !(!erased && skip && sect->CtrlMark()) &&
                !(erased && skip && !sect->CtrlMark()))
                return false;
        }
    } while (idxCount!=2);
    eot = 1;
    gMsg(CpcConsole::IMPORTANT,"NextSectId: no sector %02X on track", searchId);
    return true;
}

static int highestOneBit(int i)
{
    i |= (i >> 1);
    i |= (i >> 2);
    i |= (i >> 4);
    i |= (i >> 8);
    i |= (i >> 16);
    return i - (i >> 1);
}

// Generate data for a read operation
// if nBytesToGenerate exceeds the current sector's size, we generate intersect data and then go into the next sectors etc.
void cpcTrack::GenerateSectorReadData(Sector &crtReadDataSect, char *dst, int nBytesToGenerate)
{
    int state=0;
    int crtSectPhysicalSize,crtSectAvail,crtSectIdx;
    int crtIntersectSize, crtIntersectIdx;
    int crtEndOfTrackSize, crtEndOfTrackIdx;    
    int syntheticIntersectDataSize;
    int eot, delay;
    std::vector<BYTE> crtIntersectData;
    int gap3 = Gap3();
    
    Sector *crtSectP = &crtReadDataSect;
    
    for (int crtIdx=0; crtIdx<nBytesToGenerate; crtIdx++)
    {
        switch (state)
        {
            case 0: // init
                crtSectAvail = crtSectP->storedSize();
                crtSectPhysicalSize = SectNBytes();
                crtSectPhysicalSize = highestOneBit(crtSectAvail);
                crtSectIdx=0;
                state=1;
            case 1: // in sector
                if (crtSectIdx<crtSectAvail && crtSectIdx<crtSectPhysicalSize)
                {
                    dst[crtIdx] = crtSectP->Data(crtSectIdx++);
                    break;
                }
                // trust the data that is there if there is more that the synthetic.
                // Disco: 8K sectors with 6238 bytes avail (>4096+144)
                syntheticIntersectDataSize=IntersectDataSize();
                crtIntersectSize = std::max(crtSectAvail-crtSectIdx, syntheticIntersectDataSize);
                crtIntersectIdx=0;
                state=2;
            case 2: // inter-sector from dsk
                if (crtSectIdx<crtSectAvail && crtIntersectIdx<crtIntersectSize)
                {
                    dst[crtIdx] = crtSectP->Data(crtSectIdx++);
                    crtIntersectIdx++;
                    break;
                }
                NextSect(crtSectP,eot,delay);
                crtSectP->IntersectData(gap3, crtIntersectData);
                state=3;
            case 3: // inter-sector synthesized
                if (crtIntersectIdx<crtIntersectSize)
                {
                    dst[crtIdx] = crtIntersectData[crtIntersectIdx++];
                    break;
                }
                
                if (!eot)
                {
                    state=0;
                    crtIdx--;
                    break;
                }
                crtEndOfTrackSize = cpcTrack::totalTrackSize-NbSect()*(SectNBytes()+crtIntersectSize);
                crtEndOfTrackIdx=0;
                state=4;
            case 4: // EOT Gap
                if (crtEndOfTrackIdx<crtEndOfTrackSize)
                {
                    dst[crtIdx] = 0x4E;
                    crtEndOfTrackIdx++;
                    break;
                }
                state=0;
                crtIdx--;
                break;
        }
    }
}

int cpcTrack::IntersectDataSize()
{
    int gap3 = Gap3();
    int crtIntersectSize=2+gap3+12+3+1+4+2+22+12+3+1;
    return crtIntersectSize;
}

void Sector::IntersectData(int gap3, std::vector<BYTE> &data)
{
    int crtIntersectSize=2+gap3+12+3+1+4+2+22+12+3+1;
    data.resize(crtIntersectSize);
    BYTE *pt = &data[0];
    
    // CRC (on data+ID, i.e. 516 bytes)
    *pt++ = 0x7C;
    *pt++ = 0x09;
    // GAP #3
    for (int i=0; i<gap3; i++)
        *pt++ = 0x4E;
    // Sync
    for (int i=0; i<12; i++)
        *pt++ = 0x00;
    // ID AM
    for (int i=0; i<3; i++)
        *pt++ = 0x1A;
    *pt++ = 0xFE;
    // ID
    *pt++ = Track();
    *pt++ = Head();
    *pt++ = Id();
    *pt++ = Size();
    // CRC on ID
    *pt++ = 0x45;
    *pt++ = 0xAC;
    // GAP #2
    for (int i=0; i<22; i++)
        *pt++ = 0x4E;
    // Sync
    for (int i=0; i<12; i++)
        *pt++ = 0x00;
    // DATA AM
    for (int i=0; i<3; i++)
        *pt++ = 0x1A;
    *pt++ = 0xFB;
}

void Drive::FormatTrack(int head, BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    crtHead = head;
    if (floppy==0)
        throw Err(_WHERE);
    return floppy->FormatTrack(crtHead,crtTrack,fmtSize,fmtNb,fmtGap,fmtData);
}

void Drive::FormatSect(int head, int sect, const SECT_ID &id)
{
    crtHead = head;
    if (floppy==0 || CrtTrack()==0)
        throw Err(_WHERE);
    CrtTrack()->FormatSect(sect,id);
}

//-----------------------------------
// Floppy disk object
//
Floppy::Floppy(bool randomDataError)
: writeProtect(0), format(UNKNOWN), nbSides(0), flipped(0), tracks(), randomDataError(randomDataError), randomDataErrorFlag(0)
{
}

Floppy::~Floppy()
{
    for (Tracks::iterator i=tracks.begin(); i!=tracks.end(); ++i)
        delete *i;
}

void Floppy::Flip()
{
    if (nbSides!=2)
        throw Err(_WHERE);
    flipped ^= 1;
}

cpcTrack * Floppy::GetTrack(int side, int track)
{
    if (side>=nbSides) side=nbSides-1;
    try {
        return tracks.at(track*nbSides+(side^flipped));
    } catch (...) {
        return 0;
    }
}

void Floppy::FormatTrack(int /*side*/, int track, BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    tracks.resize(track+1);
    tracks[track] = new cpcTrack(0,0,0,0,randomDataError);
    return tracks[track]->Format(fmtSize,fmtNb,fmtGap,fmtData);
}

void Floppy::Load(const CpcFile &file, bool protect)
{
    BYTE temp[8192];
    BYTE * const buf=temp;
    
    randomDataErrorFlag = 0;
    format = UNKNOWN;
    writeProtect = protect;
    flipped=0;
    
    // Get header
    file.Read(buf,256);
    // Identify format
    SetFormat(buf);
    file.Seek(-256);
    
    switch (format) {
        case DIF0: case DIF1:
            LoadDIF(file);
            break;
        case DIF2:
            LoadDIF2(file);
            break;
        case DSK:
            LoadDSK(file);
            break;
        case EXTENDED:
            LoadEXTDSK(file);
            break;
        case UNKNOWN:
        default:
            throw Err(_WHERE, ERR_DRV_NOFORMAT);
    }
    
    if (randomDataErrorFlag)
        RandomDataError(true);
}

void Floppy::SetFormat(const BYTE *buf)
{
    if (!strncmp(reinterpret_cast<const char *>(buf), "MV - CPC", 8)) {
        gMsg(CpcConsole::INTERESTING, "DSK format");
        format = DSK;
    } else if (!strncmp(reinterpret_cast<const char *>(buf), "EXTENDED", 8)) {
        gMsg(CpcConsole::INTERESTING, "EXTENDED DSK format");
        format = EXTENDED;
    } else if (*buf == 0) {
        gMsg(CpcConsole::INTERESTING, "DIF0 format");
        format = DIF0;
    } else if (*buf == 1) {
        gMsg(CpcConsole::INTERESTING, "DIF1 format");
        format = DIF1;
    } else if (*buf == 2) {
        gMsg(CpcConsole::INTERESTING, "DIF2 format");
        format = DIF2;
    } else
        throw Err(_WHERE, ERR_DRV_NOFORMAT);
}

void Floppy::LoadDIF(const CpcFile &file)
{
    BYTE buf[2048];
    file.Read(buf, 2048);
    const BYTE *pt = buf+1;
    int nbTracks = *pt++;
    if (!nbTracks)
        throw Err(_WHERE, ERR_DRV_NOTRACKS);
    nbSides = 1;
    tracks.clear();
    tracks.resize(nbTracks);
    for (int i=0; i<nbTracks; i++) {
        cpcTrack *track = tracks[i] = new cpcTrack(*pt++,0,0,0,randomDataError);
        for (int j=0; j<track->NbSect(); j++) {
            SECT_ID id;
            id.track = *pt++;
            id.head = *pt++;
            id.id = *pt++;
            id.size = *pt++;
            track->LoadSect(j,id,file);
        }
    }
}

void Floppy::LoadDIF2(const CpcFile &file)
{
    BYTE buf[8192];
    file.Read(buf, 8192);
    const BYTE *pt = buf+1;
    int nbTracks = *pt++;
    if (!nbTracks)
        throw Err(_WHERE, ERR_DRV_NOTRACKS);
    nbSides = 1;
    tracks.clear();
    tracks.resize(nbTracks);
    for (int i=0; i<nbTracks; i++) {
        cpcTrack *track = tracks[i] = new cpcTrack(*pt++,0,0,0,randomDataError);
        for (int j=0; j<track->NbSect(); j++) {
            SECT_ID id;
            id.track = *pt++;
            id.head = *pt++;
            id.id = *pt++;
            id.size = *pt++;
            pt++;
            BYTE st1 = *pt++;
            BYTE st2 = *pt++;
            BYTE cptVal = *pt++;
            if (st2 & 0x80) {
                st2 = st2&0x7F;
                track->LoadSect(j,id,cptVal);
            } else
                track->LoadSect(j,id,file);
            track->SetSectState(j,st1,st2);
        }
    }
}

struct DskHdr
{
    char idDsk[34];
    char idCreator[14];
    unsigned char trackCount;
    unsigned char sideCount;
    unsigned char trackLength[2]; // unused for EDSK
};

struct TrackHdr
{
    char idTrack[12];
    char idFree[4];
    unsigned char trackNumber;
    unsigned char sideNumber;
    unsigned char unused[2];
    unsigned char sectorSize;
    unsigned char sectorCount;
    unsigned char gap3length;
    unsigned char fillerByte;
};
struct SectHdr
{
    unsigned char C;
    unsigned char H;
    unsigned char R;
    unsigned char N;
    unsigned char st1;
    unsigned char st2;
    unsigned char dataLength[2];
};

void Floppy::LoadDSK(const CpcFile &file)
{
    BYTE buf[256];
    file.Read(buf,256);
    
    DskHdr &dskHdr(*reinterpret_cast<DskHdr *>(buf));
    
    int nbTracks = dskHdr.trackCount;
    if (!nbTracks)
        throw Err(_WHERE, ERR_DRV_NOTRACKS);
    if (nbTracks>80) gMsg(CpcConsole::IMPORTANT,"DSK: Tracks: %d", nbTracks);
    
    nbSides = dskHdr.sideCount;
    if (nbSides<1 || nbSides>2)
        throw Err(_WHERE, ERR_DRV_BADSIDENO);
    if (nbSides!=1) gMsg(CpcConsole::IMPORTANT,"DSK: Sides: %d", nbSides);
    
    tracks.clear();
    tracks.resize(nbTracks*nbSides);
    
    int trSize = dskHdr.trackLength[0] + 256*dskHdr.trackLength[1];
    if (trSize<1 || trSize>8192)
        throw Err(_WHERE, ERR_DRV_BADTRACKSIZE);
    
    for (int i=0; i<nbTracks; i++) {
        for (int j=0; j<nbSides; j++) {
            // Get track header
            
            // Normally we need to fseek, but BTL etc. have it wrong
            // Try without fseek first, then try with fseek
            // This will fail on DSKs without the proper track header
            // AND with incorrect handling of trSize
            file.Read(buf, 256);
            if (strncmp(reinterpret_cast<char *>(buf),"Track-Info\r\n",12)) {
                file.Reset();
                file.Seek(trSize*(i*nbSides+j)+0x100);
                file.Read(buf,256);
            }
            TrackHdr &trackHdr(*reinterpret_cast<TrackHdr *>(buf));
            
            // Sanity checks
            if (strncmp(trackHdr.idTrack,"Track-Info\r\n",12))
                gMsg(CpcConsole::INTERESTING,"DSK[%02X] bad track header",i);
            if (trackHdr.trackNumber!=i)
                gMsg(CpcConsole::INTERESTING,"DSK[%02X] track number mismatch: %d ~ %d",
                     i,i,trackHdr.trackNumber);
            if (trackHdr.sideNumber!=j)
                gMsg(CpcConsole::INTERESTING,"DSK[%02X] side number mismatch: %d ~ %d",
                     i,j,trackHdr.sideNumber);
            if (!trackHdr.sectorCount)
                gMsg(CpcConsole::INTERESTING,"DSK[%02X] track not formatted",i);
            
            // Create track
            cpcTrack *track = tracks[i*nbSides+j] =
            new cpcTrack(trackHdr.sectorCount,trackHdr.sectorSize,trackHdr.gap3length,trackHdr.fillerByte,randomDataError);
            
            for (int k=0; k<track->NbSect(); k++) {
                SectHdr &sectHdr(*reinterpret_cast<SectHdr *>(buf+sizeof(TrackHdr)+k*sizeof(SectHdr)));
                
                // Sanity checks
                if (sectHdr.N==0 || sectHdr.N>5)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] size= %02X",
                         i,k,sectHdr.N);
                if (sectHdr.st1&0x37)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] st1= %02X",
                         i,k,sectHdr.st1);
                if (sectHdr.st2&0x7F)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] st2= %02X",
                         i,k,sectHdr.st2);
                
                // Create sector
                SECT_ID id;
                id.track = sectHdr.C;
                id.head = sectHdr.H;
                id.id = sectHdr.R;
                id.size = sectHdr.N;
                track->LoadSect(k,id,file);
                const Sector *sector = &track->Sect(k);
                track->SetSectState(k,sectHdr.st1,sectHdr.st2);
                
                // Adjust position
                int offset = track->SectNBytes()-sector->storedSize();
                if (offset)
                    file.Seek(offset);
            }
        }
    }           
}

void Floppy::LoadEXTDSK(const CpcFile &file)
{
    BYTE temp[512];
    BYTE *buf=temp;
    file.Read(buf,256);
    DskHdr &dskHdr(*reinterpret_cast<DskHdr *>(buf));
    
    int nbTracks = dskHdr.trackCount;
    if (!nbTracks)
        throw Err(_WHERE, ERR_DRV_NOTRACKS);
    nbSides = dskHdr.sideCount;
    if (nbSides & 0x80) { // Data Error protection flag
        gMsg(CpcConsole::INTERESTING,"Data error protection flag ON");
        randomDataErrorFlag = 1;
        nbSides &= 0x7F;
    }
    if (nbSides!=1) {
        gMsg(CpcConsole::IMPORTANT,"EDSK: sides=%d", nbSides);
    }
    tracks.clear();
    tracks.resize(nbTracks*nbSides);
    const BYTE *trackSizePt = buf+0x34;
    buf += 256;
    for (int i=0; i<nbTracks; i++) {
        for (int j=0; j<nbSides; j++) {
            if (*trackSizePt) {
                // Get track header
                file.Read(buf, 256);
                TrackHdr &trackHdr(*reinterpret_cast<TrackHdr *>(buf));
                
                // Sanity checks
                if (strncmp(reinterpret_cast<char *>(buf),"Track-Info\r\n",13))
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X] bad track header",i);
                if (trackHdr.trackNumber!=i)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X] track number mismatch: %d ~ %d",i,trackHdr.trackNumber);
                if (trackHdr.sideNumber!=j)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X] side number mismatch: %d ~ %d",i,j,trackHdr.sideNumber);
                if (!trackHdr.sectorCount)
                    gMsg(CpcConsole::INTERESTING,"EDSK[%02X] empty track",i);
                
                // Create track
                cpcTrack *track = tracks[i*nbSides+j] =
                new cpcTrack(trackHdr.sectorCount,trackHdr.sectorSize,trackHdr.gap3length,trackHdr.fillerByte,randomDataError);
                int trSize = (*trackSizePt++)*256;
                int offset = 256;
                
                for (int k=0; k<track->NbSect(); k++) {
                    // get sector header
                    SectHdr &sectHdr(*reinterpret_cast<SectHdr *>(buf+sizeof(TrackHdr)+k*sizeof(SectHdr)));
                    
                    // Sanity checks
                    if (sectHdr.N==0 || sectHdr.N>5)
                        gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] size= %02X", i,k,sectHdr.N);
                    if (sectHdr.st1&0x37)
                        gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] st1= %02X", i,k,sectHdr.st1);
                    if (sectHdr.st2&0x7F)
                        gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] st2= %02X", i,k,sectHdr.st2);
                    
                    SECT_ID id;
                    id.track = sectHdr.C;
                    id.head = sectHdr.H;
                    id.id = sectHdr.R;
                    id.size = sectHdr.N;
                    
                    int dataSize;
                    if (sectHdr.dataLength[1]==0xFF)
                        dataSize = sectHdr.dataLength[0];
                    else
                        dataSize = sectHdr.dataLength[0]|(sectHdr.dataLength[1]<<8);
                    if (dataSize==0 || dataSize>6000)
                        gMsg(CpcConsole::INTERESTING,"EDSK[%02X/%02X] dataSize=%04X",i,k,dataSize);
                    
                    
                    // EDSK extensions - Simon
                    // Storing Multiple Versions of Weak/Random Sectors.
                    // Some copy protections have what is described as 'weak/random' data. Each time the sector is
                    // read one or more bytes will change, the value may be random between consecutive reads of the same sector.
                    // To support these formats the following extension has been proposed.
                    // Where a sector has weak/random data, there are multiple copies stored.
                    // The actual sector size field in the SECTOR INFORMATION LIST describes the size of all the copies.
                    // To determine if a sector has multiple copies then compare the actual sector size field to the size defined by the N parameter.
                    // For multiple copies the actual sector size field will have a value which is a multiple of the size defined by the N parameter.
                    // The emulator should then choose which copy of the sector it should return on each read. 
                    //----------
                    // I've made a change to add an extra byte (7B) to the data if
                    // it clashes with the multiple copies extension.  This extra byte should
                    // be removed on EDSK loading if it meets these conditions:
                    // 1) sector has data CRC error.
                    // 2) ((dataStored MOD sectorSize) == 1).
                    // 3) last byte of data is 7B. 
                    int sectSize = Size2Bytes(id.size);
                    int loadSize=dataSize, skipSize=0;
                    int nbCopies=1;
                    if (dataSize > sectSize)
                    {
                        if (sectHdr.st2 & St2DataErrorInDataField)
                        {
                            if (dataSize%sectSize==0)
                            {
                                // Weak sector
                                nbCopies = dataSize/sectSize;
                            }
                            else if (dataSize%sectSize==1)
                            {
                                // inter-sector data that has the same size as the sector itself
                                // (need the 7B check here)
                                loadSize = dataSize-1;
                                skipSize = 1;
                            }
                        }
                    }
                    
                    track->LoadSect(k,id,loadSize,file,nbCopies);
                    track->SetSectState(k,sectHdr.st1,sectHdr.st2);
                    if (skipSize)
                        file.Seek(skipSize);
                    offset += dataSize;
                }
                file.Seek(trSize - offset);
            } else {
                gMsg(CpcConsole::INTERESTING,"EDSK[%02X] not formated",i);
                tracks[i*nbSides+j] = new cpcTrack(0,0,0,0,randomDataError);
                trackSizePt++;
            }                   
        }
    }           
}

void Floppy::Save(CpcFile &file)
{
    BYTE buff[0x100];
    const Sector * sect;
    
    // Make up header
    sprintf(reinterpret_cast<char *>(buff)+0x00,"EXTENDED CPC DSK File\r\nDisk-Info\r\n");
    sprintf(reinterpret_cast<char *>(buff)+0x22,"CPC++         ");
    buff[0x30]=static_cast<BYTE>(tracks.size());
    buff[0x31]=nbSides|(randomDataError? 0x80: 0x00);
    buff[0x32]=0;
    buff[0x33]=0;
    BYTE *offsets = &buff[0x34];
    for (Tracks::iterator trackI=tracks.begin(); trackI!=tracks.end(); ++trackI) { // Track offset table
        cpcTrack *track = *trackI;
        if (!track->NbSect())
            *offsets=0; // Not formatted
        else {
            BYTE maxSz=0;
            int totSz=0;
            for (int j=0; j<track->NbSect(); j++) {
                if (maxSz < track->Sect(j).Size())
                    maxSz = track->Sect(j).Size();
                totSz += 0x80<<track->Sect(j).Size();
            }
            *offsets = static_cast<BYTE>(1 + ((totSz+0xFF)>>8));
            track->SectSize(maxSz); // Might not be set yet
        }
        offsets++;
    }
    // Write header
    file.Write(buff,256);
    
    // Make up track blocks
    BYTE trackIdx=0;
    for (Tracks::iterator trackI=tracks.begin(); trackI!=tracks.end(); ++trackI) { // Track offset table
        cpcTrack *track = *trackI;
        if (track->NbSect()) {
            // Make up track header
            sprintf(reinterpret_cast<char *>(buff),"Track-Info\r\n");
            buff[0x0D]=buff[0x0E]=buff[0x0F]=0;
            buff[0x10]=trackIdx;
            buff[0x11]=0; // Side
            buff[0x12]=buff[0x13]=0;
            buff[0x14]=track->SectSize();
            buff[0x15]=static_cast<BYTE>(track->NbSect());
            buff[0x16]=track->Gap3();
            buff[0x17]=track->Filler();
            for (int j=0; j<track->NbSect(); j++) { // Sector info list
                sect = &track->Sect(j);
                buff[0x18+8*j]=sect->Track();
                buff[0x19+8*j]=sect->Head();
                buff[0x1A+8*j]=sect->Id();
                buff[0x1B+8*j]=sect->Size();
                buff[0x1C+8*j]=sect->St1();
                buff[0x1D+8*j]=sect->St2();
                buff[0x1E +8*j]=static_cast<BYTE>(sect->storedSize()&0xFF);
                buff[0x1F+8*j]=static_cast<BYTE>(sect->storedSize()>>8);
            }
            // Write track header
            file.Write(buff,256);
            // Write sectors data
            for (int j=0; j<track->NbSect(); j++) {
                sect = &track->Sect(j);
                file.Write(sect->Data(),sect->storedSize());
            }
        }
        trackIdx++;
    }
}

static bool BadChar(char c) {return c<0x20 || c>0x7E;}

void Floppy::GetDir(int &nbEntries, std::vector<char> &dir)
{
    static const int nbSectorsPerDirectory=4;
    static const int nbEntriesPerSector=16;
    static const int nbBytesPerEntry=32;
    static const int nbCharPerEntry=11;
    
    dir.resize(nbCharPerEntry*nbEntriesPerSector*nbSectorsPerDirectory);    
    nbEntries = 0;
    
    if (!tracks[0] || !tracks[0]->Formatted()) throw Err(_WHERE);
    
    BYTE sectorId = tracks[0]->Sect(0).Id() & 0xC0;
    
    cpcTrack * track;
    switch(sectorId) {
        case 0x00: // IBM
            track = tracks[1];
            break;
        case 0x40: // CPM
            track = tracks[2];
            break;
        case 0xC0: // AMSDOS
            track = tracks[0];
            break;
        default:
            throw Err(_WHERE);
    }
    
    for (int currentSector=0; currentSector<nbSectorsPerDirectory; currentSector++) {
        sectorId++;
        
        Sector *sector=0;
        for (int sectorIndex=0; !sector && sectorIndex<track->NbSect(); sectorIndex++)
        {
            Sector &crtSect(track->Sect(sectorIndex));
            if (crtSect.Id() == sectorId && crtSect.Size()==2)
                sector = &crtSect;
        }
        if (sector==0) return; // not found

        for (int entryIndex=0; entryIndex<nbEntriesPerSector; entryIndex++) {
            int srcIdx=nbBytesPerEntry*entryIndex;
            char *dst = &dir[nbCharPerEntry * nbEntries];
            if (sector->Data(srcIdx)==0 && sector->Data(srcIdx+12)==0 && (sector->Data(srcIdx+10)&0x80)==0) { // User 0, First entry, visible
                srcIdx++;
                int charIndex;
                for (charIndex=0; charIndex<nbCharPerEntry;charIndex++) {
                    if (BadChar(sector->Data(srcIdx) & 0x7F)) charIndex=1000;
                    *dst++ = sector->Data(srcIdx)&0x7F;
                    srcIdx++;
                }
                if (charIndex==nbCharPerEntry)
                    (nbEntries)++;
            }
        }
    }	
}

bool Floppy::CanCpm()const
{
    if (!tracks[0] || !tracks[0]->Formatted())
        return false;
    for (int sectorIndex=0; sectorIndex<tracks[0]->NbSect(); sectorIndex++)
    {
        Sector &crtSect(tracks[0]->Sect(sectorIndex));
        if (crtSect.Id() == 0x41 && crtSect.Size()==2)
            return true;;
    }
    return false;
}

void Floppy::RandomDataError(bool val)
{
    randomDataError=val;
    for (Tracks::iterator i=tracks.begin(); i!=tracks.end(); ++i)
        (*i)->RandomDataError(val);
}

cpcTrack::cpcTrack(int nbSect_, BYTE sectSize_, BYTE myGap3, BYTE filler_, bool randomDataError)
: sectSize(sectSize_), gap3(myGap3), filler(filler_), sectors(nbSect_), randomDataError(randomDataError), crtSect(0)
{}

cpcTrack::~cpcTrack()
{
    for (Sectors::iterator i=sectors.begin(); i!=sectors.end(); ++i)
        delete *i;
}

void cpcTrack::Format(BYTE fmtSize, BYTE fmtNb, BYTE fmtGap, BYTE fmtData)
{
    for (Sectors::iterator i=sectors.begin(); i!=sectors.end(); ++i)
        delete *i;
    sectors.clear();
    sectSize = fmtSize;
    gap3 = fmtGap;
    filler = fmtData;
    sectors.resize(fmtNb);
}

void cpcTrack::FormatSect(int no, const SECT_ID &id)
{
    sectors[no] = new Sector(id, filler, randomDataError);
}

void cpcTrack::LoadSect(int no, const SECT_ID &id, const CpcFile &file)
{
    sectors[no] = new Sector(id, file, randomDataError);
}

void cpcTrack::LoadSect(int no, const SECT_ID &id, int fSize, const CpcFile &file, int nbCopies)
{
    sectors[no] = new Sector(id, fSize, file, randomDataError, nbCopies);
}

void cpcTrack::LoadSect(int no, const SECT_ID &id, int cptVal)
{
    sectors[no] = new Sector(id,cptVal, randomDataError);
}

void cpcTrack::SetSectState(int no, BYTE st1, BYTE st2)
{
    sectors[no]->St1(st1);
    sectors[no]->St2(st2);
}

void cpcTrack::RandomDataError(bool val)
{
    randomDataError=val;
    for (Sectors::iterator i=sectors.begin(); i!=sectors.end(); ++i)
        (*i)->RandomDataError(val);
}

Sector::Sector(const SECT_ID &myId, int filler, bool rde)
: id(myId), st1(St1EndOfCylinder), st2(St2None), nbCopies(1), crtCopy(0), data(nbCopies), randomDataError(rde? rdeRandom: noRde)
{
    int dataSize = logicalSize();
    data[0].resize(dataSize);
    memset(&data[0][0],filler,dataSize);
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

Sector::Sector(const SECT_ID &myId, const CpcFile &file, bool rde)
: id(myId), st1(St1EndOfCylinder), st2(St2None), nbCopies(1), crtCopy(0), data(nbCopies), randomDataError(rde? rdeRandom: noRde)
{
    int dataSize = logicalSize();
    data[0].resize(dataSize);
    file.Read(&data[0][0], dataSize);
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

Sector::Sector(const SECT_ID &myId, int fSize, const CpcFile &file, bool rde, int nbCopies_)
: id(myId), st1(St1EndOfCylinder), st2(St2None), nbCopies(nbCopies_), crtCopy(0), data(nbCopies), randomDataError(nbCopies>1? rdeFromDsk: rde? rdeRandom: noRde)
{
    int dataSize = fSize/nbCopies;
    for (int i=0; i<nbCopies; i++)
    {
        data[i].resize(dataSize);
        if (dataSize) file.Read(&data[i][0], dataSize);
    }
    if (myId.size > 5) {
        st1 = St1DataError;
        st2 = St2DataErrorInDataField;
    }
}

void Sector::RandomDataError(bool val)
{
    if (randomDataError==rdeFromDsk) return;
    randomDataError = val? rdeRandom: noRde;
}

bool Sector::IsSameId(const SECT_ID &id_)const
{
    return id == id_;
}

BYTE Sector::Data(int idx)const
{
    // DD error protection trick:
    // This protection has a damaged sector which, when repeatedly
    // read, returns random data.
    // We have no clue has to where the damage is, so we
    // arbitrarily poke some random bytes at random places
    if (idx==0 && randomDataError==rdeFromDsk)
    {
        crtCopy = rand()%nbCopies;
    }
    if (idx >= storedSize() 
        || ( randomDataError==rdeRandom && Size()<6 && (St2()&St2DataErrorInDataField) && (rand()&0x01)==0 ))
        return static_cast<BYTE>(rand()&0xFF);
    return data[crtCopy][idx];
}

void Sector::Data(int idx, BYTE val)
{
    if (idx >=storedSize())
        gMsg(CpcConsole::IMPORTANT,"Attempt to write after physical end of sector");
    else
        data[0][idx] = val;
}

