// CPC++  - An Amstrad CPC emulator.
// Copyright (C) 1996-2010 Brice Rive
// 
// Fdc.h - Floppy disk controller emulation
#ifndef Fdc_h
#define Fdc_h

#include "periph.h"
#include "drive.h"

// Main status register flags
static const BYTE FDC_RQM=0x80;
static const BYTE FDC_DIO=0x40;
static const BYTE FDC_EXM=0x20;
static const BYTE FDC_CBY=0x10;
static const BYTE FDC_DBY3=0x08;
static const BYTE FDC_DBY2=0x04;
static const BYTE FDC_DBY1=0x02;
static const BYTE FDC_DBY0=0x01;

// Commands
// These are the instruction codes for the 15 Fdc instructions
/*
 æPD765 Version	   D7  D6  D5  D4  D3  D2  D1  D0
 
 command byte 0:     ?	?   ?	1   0	0   0	0
 result byte 0:	   status register 0
 90h = æPD765B;  80h = æPD765A or æPD765A-2
 
 Invalid Command
 
 result byte 0:	   status register 0 (value of 80h)
 */
#define Cmd_ReadData 0x06
#define Cmd_WriteData 0x05
#define Cmd_ReadDataE 0x0C
#define Cmd_WriteDataE 0x09
#define Cmd_ReadTrack 0x02
#define Cmd_Format 0x0D
#define Cmd_ReadId 0x0A
#define Cmd_ScanEqual 0x11
#define Cmd_ScanLow 0x19
#define Cmd_ScanHigh 0x1D
#define Cmd_Recalibrate 0x07
#define Cmd_FindTrack 0x0F
#define Cmd_SenseInt 0x08
#define Cmd_SenseDrive 0x04
#define Cmd_SetDrive 0x03
#define Cmd_Version 0x10
#define Cmd_Invalid 0x00

static const int fullTrackBufferSize=8192;

class Fdc:public Peripheral {
public:
    Fdc();
    virtual ~Fdc();
    void Wire(Drive **drives, volatile int *trapFlag_=0);
    virtual void Reset();
    virtual void Put(Phase phase, WORD addr, BYTE val);
    virtual BYTE Get(Phase phase, WORD addr);
    
    // Options
    void RealTime(bool val) {realTime=val;}
    bool RealTime()const {return realTime;}
    
private:
    Fdc &operator=(const Fdc&);
    Fdc(const Fdc&);
    
    // options
    bool realTime;
    
    // Instruction execution
    BYTE Select(BYTE val, Phase phase);
    Phase crtElapsed;
    Phase crtDelay;
    Phase crtTimeout;
    void Instr0(BYTE val, int warnMask=0xE0);
    void Instr1(BYTE val);
    void SetDelay(int delay);
    void UpdateMainState();
    
    // The drives
    Drive **drives;
    
    struct DriveState
    {
        DriveState(): st0(St0ErrorCodeNone), crtTrack(0), seekState(NO) {}
        // Suppose there is one st0 for each drive
        BYTE st0;
        // the FDC has its own current track idea as there is no way to ask directly to the drive
        int crtTrack;
        typedef enum {NO,YES,DONE} SeekState;
        SeekState seekState;
    };
    DriveState driveStates[4];
    
    // Fdc state
    BYTE mainState;
    BYTE st0, st1, st2, st3;
    //    BYTE r[4];
    int motors;
    
    // read buffer for ReadTrack, ReadData, ReadDataErased
    char readBuffer[fullTrackBufferSize];
    
    // Readtrack emulation
    void SetupReadTrack();
    
    // ReadData emulation
    void SetupReadData();

    // Execution data
    typedef BYTE (Fdc::*Instruction)(BYTE, Phase);
    Instruction currentInstruction;
    
    // Data set by instruction phase
    bool multi; // from first  instruction byte
    bool mfm; // from first  instruction byte
    bool skip; // from first  instruction byte
    bool erased;  // from first  instruction byte
    bool track;  // from first  instruction byte
    int crtDrive; // from second instruction byte
    BYTE crtHead; // from second instruction byte
    
    SECT_ID id; // set by ReadData, ReadDataErased, WriteDat, WriteDataErased, ReadTrack
    BYTE lastSect; // set by ReadData, ReadDataErased, WriteDat, WriteDataErased, ReadTrack
    BYTE gap; // set by ReadData, ReadDataErased, WriteDat, WriteDataErased, ReadTrack
    BYTE length; // set by ReadData, ReadDataErased, WriteDat, WriteDataErased, ReadTrack
    
    BYTE fmtSize; // set by Format
    BYTE fmtNb; // set by Format
    BYTE fmtGap; // set by Format
    BYTE fmtData; // set by Format
    
    // Data used by execution phase
    int crtCnt;
    Phase lastReadPhase;
    BYTE fmtSect;
    BYTE step, lift, load, dma; // value defined by SetDrive
    SECT_ID fmtId;
    int dataIdx;
    
    Drive *CrtDrive()const {return drives[crtDrive];}
    int CrtTrack()const;
    Sector *crtSect;
    
    // Methods
    SECT_ID *FindID(BYTE *dsk, int track, int *sector);
    BYTE Error(BYTE val, Phase phase);
    BYTE ReadData(BYTE val, Phase phase);
    BYTE ReadDataE(BYTE val, Phase phase);
    BYTE WriteData(BYTE val, Phase phase);
    BYTE WriteDataE(BYTE val, Phase phase);
    BYTE ReadTrack(BYTE val, Phase phase);
    BYTE Format(BYTE val, Phase phase);
    BYTE ReadId(BYTE val, Phase phase);
    BYTE ScanEqual(BYTE val, Phase phase);
    BYTE ScanLow(BYTE val, Phase phase);
    BYTE ScanHigh(BYTE val, Phase phase);
    BYTE Recalibrate(BYTE val, Phase phase);
    BYTE FindTrack(BYTE val, Phase phase);
    BYTE SenseInt(BYTE val, Phase phase);
    BYTE SenseDrive(BYTE val, Phase phase);
    BYTE SetDrive(BYTE val, Phase phase);
    BYTE Version(BYTE val, Phase phase);
    BYTE Invalid(BYTE val, Phase phase);
    
	// Drive state
	bool DriveNotReady(Phase phase)const {return !CrtDrive() || !CrtDrive()->IsReady(phase);}
	bool DriveProtected()const {return CrtDrive()->IsProtected();}	
    void UpdateSeekState(Phase phase);
	// MainState register
	inline void StateReset() {mainState=FDC_RQM;}
	inline void StateInstr() {mainState=(mainState&0x0F)|FDC_CBY;}
	inline void StateExecIn() {mainState=(mainState&0x0F)|FDC_CBY|FDC_EXM;}
	inline void StateExecOut() {mainState=(mainState&0x0F)|FDC_CBY|FDC_EXM|FDC_DIO;}
	inline void StateResult() {mainState=(mainState&0x0F)|FDC_CBY|FDC_DIO;}
	inline void StateReady();
	inline int IsExecuting()const {return mainState & FDC_EXM;}
	inline int IsInputing()const {return !(mainState & FDC_DIO);}
	inline int IsOutputing()const {return mainState & FDC_DIO;}
	inline void SetReady(Phase phase);
    
  	// Monitor support
    volatile int *trapFlag;
};
#endif
