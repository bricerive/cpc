################################################################
231015 - Back to dev
----------------------------------------------------------------
Run EMS.sh to launch Visual Code, Bitbucket, sourceTree, a log terminal...
In Visual Code, Go to CMake panel and open the target and click the build icon
################################################################
220415
Upgrade wxWidgets to 3.1.6 (to see if they fixed the pasting issue in file dialog)

https://www.wxwidgets.org/downloads/

cd /Users/brice/Dev/ws/git/CPC++/CPC++/wxWidgets-3.1.6/build-release

../configure --disable-shared --enable-stl --with-cxx=14 --with-macosx-sdk=/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk --enable-debug --enable-debug_gdb --disable-optimise --prefix="`pwd`"

        Configured wxWidgets 3.1.6 for `x86_64-apple-darwin18.7.0'

          Which GUI toolkit should wxWidgets use?                 osx_cocoa
          Should wxWidgets be compiled into single library?       no
          Should wxWidgets be linked as a shared library?         no
          Should wxWidgets support Unicode?                       yes (using wchar_t)
          What level of wxWidgets compatibility should be enabled?
                                               wxWidgets 2.8      no
                                               wxWidgets 3.0      yes
          Which libraries should wxWidgets use?
                                               STL                yes
                                               jpeg               sys
                                               png                sys
                                               regex              sys
                                               tiff               sys
                                               lzma               no
                                               zlib               sys
                                               expat              sys
                                               libmspack          no
                                               sdl                no
make

I then had to kludge the Makefile and FindSDL2 so they find the right stuff
./src/sdl2/CMakeLists.txt:    set(wxWidgets_CONFIG_EXECUTABLE "/Users/brice/Dev/ws/git/CPC++/CPC++/wxWidgets-3.1.4/build-release/wx-config")
----------------------------------------------------------------
210206

Try upgrading wxWidgets

brew only has old version

install new one by hand:
    -Put Apple gcc in front of brew's:
export PATH=/usr/bin:$PATH
    -Configure:
cd /Users/brice/Dev/ws/git/CPC++/CPC++/wxWidgets-3.1.4/build-release
../configure --disable-shared --enable-stl --with-cxx=14 --with-macosx-sdk=/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk --enable-debug --enable-debug_gdb --disable-optimise --prefix="`pwd`"


Configured wxWidgets 3.1.4 for `x86_64-apple-darwin18.7.0'

  Which GUI toolkit should wxWidgets use?                 osx_cocoa
  Should wxWidgets be compiled into single library?       no
  Should wxWidgets be linked as a shared library?         no
  Should wxWidgets support Unicode?                       yes (using wchar_t)
  What level of wxWidgets compatibility should be enabled?
                                       wxWidgets 2.8      no
                                       wxWidgets 3.0      yes
  Which libraries should wxWidgets use?
                                       STL                yes
                                       jpeg               sys
                                       png                sys
                                       regex              builtin
                                       tiff               sys
                                       lzma               yes
                                       zlib               sys
                                       expat              sys
                                       libmspack          no
                                       sdl                no

-build
make

I then had to kludge the Makefile and FindSDL2 so they find the right stuff

----------------------------------------------------------------
200913
Bitbucket integrates nicely with Jira so use it instead of manual tracking
Finally got it to build on VSCode on OSX - bye-bye XCode!

----------------------------------------------------------------
Switching to SDL2/wxWidgets

----------------------------------------------------------------
OSX aliases are now huge (500kb). They are filled with icon data.
Use soft links instead
I think this is not true anymore in Mojave

----------------------------------------------------------------
I decided to regroup all the CPC++ stuff under git.
That is:
-the main source code repo
-the homePage repo
-the tools repo
-the Project document (but not the stuff that can be found online)

I use .gitignore to avoid putting in git:
-big stuff
-files that contain private info like passwords etc.

I also compress stuff:
Images/scans -> 100dpi jpg
.DSK >SNA -> ZIP

----------------------------------------------------------------
To avoid rebooting under SL, I'm thinking about using a VirtualBox.
See: 160702-VirtualBox-Snow Leopard

----------------------------------------------------------------
I modified the makefiles to support the dylibs moved under the bundle.
	It needs CMake >= 2.8.12
	see: https://blog.kitware.com/upcoming-in-cmake-2-8-12-osx-rpath-support/
The main lines are:
	set(CMAKE_MACOSX_RPATH 1)
	set_target_properties(${target}  PROPERTIES INSTALL_RPATH “@executable_path/../lib”)

When CPC++ (the exe) gets rebuilt, I still need to add a rpath by hand:
 	install_name_tool -add_rpath @executable_path/../lib CPC++
When I rebuild the dylibs, I need to move them into bundle/Contents/lib

----------------------------------------------------------------
otool -l CPC++  | grep LC_RPATH -A2
    path @executable_path/../lib (offset 12)
otool -D /Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/core/Debug/libcpcppCoreLib.dylib	
	@rpath/libcpcppCoreLib.dylib
otool -L CPC++
CPC++:
	@rpath/libcpcppOsLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	@rpath/libcpcppConsoleLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	@rpath/libcpcppCoreLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	/System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 152.0.0)
	/System/Library/Frameworks/Cocoa.framework/Versions/A/Cocoa (compatibility version 1.0.0, current version 15.0.0)
	/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 227.0.0)
	/usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.9.0)
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 125.2.11)
----------------------------------------------------------------
To view the install name of a shared library, use “otool -D <file>”
To view the install name of dependent shared libraries, use “otool -L <file>”
To view the RPATHs for locating dependent shared libraries using @rpath, use “otool -l <file> | grep LC_RPATH -A2”
To modify RPATHs, use install_name_tool’s -rpath, -add_rpath, and -delete_rpath options.
----------------------------------------------------------------
To modify the OSX part:
Boot under Snow Leopard (from Mobile Space)
open /Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/CPC++.xcodeproj
build
run otool -L CPC++
$ otool -L /Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/Debug/CPC++.app/Contents/MacOS/CPC++
/Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/Debug/CPC++.app/Contents/MacOS/CPC++:
	/Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/core/Debug/libcpcppCoreLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	/Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/console/Debug/libcpcppConsoleLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	/Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/osx/Debug/libcpcppOsLib.dylib (compatibility version 0.0.0, current version 0.0.0)
	/System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 152.0.0)
	/System/Library/Frameworks/Cocoa.framework/Versions/A/Cocoa (compatibility version 1.0.0, current version 15.0.0)
	/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 227.0.0)
	/usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.9.0)
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 125.2.11)
	
$ install_name_tool -change /Users/brice/MacDev/ws/git/CPC++/CPC++/CPC++_repo/build_XCode/src/core/Debug/libcpcppCoreLib.dylib @executable_path/libcpcppCoreLib.dylib CPC++

----------------------------------------------------------------
I cannot build Cabon but I can run it
-> Make a version under SnowLeopard that loads the CpcCore as a dynamic library
That way, I can patch the code lib and still make a runnable version with Carbon wrapper.

Under Snow Loepard:
Modify CMake to generate a SHARED instead of STATIC
Moved the dynamic lib into the bundle
used install_name_tool to patch the executable (see otool -L CPC++)

----------------------------------------------------------------
Moved to El Capitan -> no more Carbon!!! Apple sucks. Done developping code for their stupid OS!!!
----------------------------------------------------------------
use SSH key to allow pushing without entering password

$ pbcopy < ~/.ssh/id_rsa.pub

ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArL3xcCdyMXhumIMA87ScxugFQ3EQvRXa5fwnl+3ThsibKjXAnEp4bHdWEPTbxnxnpmzyNEjCrhlvYkPGbR8YmzBCYfRN5+QO2Mc1wh60Md8Mn4VFUGYNmMH98vnjBBpzXOeYblO05hlXlE0P+f7PdotZLgcLfnY5kI0ArILASENFZ7p/KjBbNZswiw3V+gcaVYXjFBsI4C/R6YbtRPD4ezTs5cKnDwi5RZTx8TYOHtjLmL/pzwy3XN1hkW5DXjJIUBdOAo8/IUKDIRWRqtg2T4UdCd5FRXU2x4BrNtkd8MKerOYMGf5UsWZ1eWA53nQAGUJm+FExC04HxP/B1aUdAw== brice@brice-rives-macbook-pro.local

Add SSH Key to BitBucket: https://bitbucket.org/account/user/bricerive/ssh-keys/

----------------------------------------------------------------
Clone from the origin

$ git clone git@bitbucket.org:bricerive/cpc.git

----------------------------------------------------------------
