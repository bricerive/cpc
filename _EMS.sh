#!/bin/sh

# Visual code
code ~/Dev/ws/git/CPC++/CPC++/cpcpp

# Bitbucket
open https://bitbucket.org/bricerive/cpc/src/master/

# Jira
# Open issues
open https://bricerive.atlassian.net/jira/software/c/projects/CPCPP/issues/?filter=allopenissues
# Kanban
open https://bricerive.atlassian.net/jira/software/c/projects/CPCPP/boards/1
# Backlog
open https://bricerive.atlassian.net/jira/software/c/projects/CPCPP/boards/1/backlog?view=detail&issueLimit=100

# SourceTree
stree ~/Dev/ws/git/CPC++

# log file navigator
osascript -e 'tell app "Terminal" to do script "lnav /Users/brice/Dev/ws/git/CPC++/CPC++/build-VS/src/sdl2/CPC++.app/Contents/MacOS/CPC++_log.txt"'
